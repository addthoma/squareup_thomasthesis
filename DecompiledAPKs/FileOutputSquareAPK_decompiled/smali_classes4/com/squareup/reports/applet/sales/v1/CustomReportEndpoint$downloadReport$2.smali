.class final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;
.super Ljava/lang/Object;
.source "CustomReportEndpoint.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->downloadReport(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Single<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u001a\u0010\u0004\u001a\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
        "salesSummarySuccessOrFailure",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $filters:Ljava/util/List;

.field final synthetic $reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->$reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->$filters:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
            ">;>;"
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->$reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadReport$2;->$filters:Ljava/util/List;

    const-string v3, "salesSummarySuccessOrFailure"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$downloadSubReports(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method
