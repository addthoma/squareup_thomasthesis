.class final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;
.super Ljava/lang/Object;
.source "CustomReportEndpoint.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->downloadSubReports(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Ljava/util/List;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "TT1;TT2;TT3;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00010\u00012\u001a\u0010\u0006\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00010\u00012&\u0010\u0007\u001a\"\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0001 \u0005*\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u0001\u0018\u00010\u00080\u0008H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
        "discountsSuccessOrFailure",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "kotlin.jvm.PlatformType",
        "paymentMethodSuccessOrFailure",
        "optionalChartDataSuccessOrFailure",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

.field final synthetic $salesSummaryResponse:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->$salesSummaryResponse:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->$reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/util/Optional;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;",
            ">;>;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;",
            ">;"
        }
    .end annotation

    .line 208
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    move-object v2, p1

    check-cast v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 214
    instance-of p1, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_3

    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    move-object v3, p1

    check-cast v3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    const/4 p1, 0x0

    .line 221
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

    .line 222
    invoke-virtual {p3}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 223
    invoke-virtual {p3}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    .line 226
    instance-of p2, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p2, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;

    goto :goto_0

    .line 227
    :cond_0
    instance-of p2, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p2, :cond_1

    .line 229
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {p2, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$mapFailure(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    :goto_0
    move-object v8, p1

    .line 233
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    invoke-virtual {p1, v2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->createDiscountRows(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Ljava/util/List;

    move-result-object v6

    .line 238
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->$salesSummaryResponse:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const-string p2, "salesSummaryResponse.custom_report"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 239
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iget-object p3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->sub_report:Ljava/util/List;

    const-string v0, "rootCustomReport.sub_report"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$createItemCategoryRows(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 240
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iget-object p3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    const-string v0, "rootCustomReport.aggregate"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$createSalesRows(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Ljava/util/List;

    move-result-object v4

    .line 242
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1, v3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$createPaymentsRows(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Ljava/util/List;

    move-result-object v5

    .line 244
    new-instance p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 246
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->$reportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->$salesSummaryResponse:Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;

    .line 245
    invoke-static/range {v0 .. v8}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;->withTransactions(Lcom/squareup/reports/applet/sales/v1/ReportConfig;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport$ChartData;)Lcom/squareup/reports/applet/sales/v1/SalesSummaryReport;

    move-result-object p2

    .line 244
    invoke-direct {p1, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object p1

    .line 215
    :cond_3
    instance-of p1, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_4

    .line 216
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 217
    invoke-static {p1, p2}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$mapFailure(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 209
    :cond_5
    instance-of p2, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p2, :cond_6

    .line 210
    iget-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->this$0:Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {p2, p1}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;->access$mapFailure(Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 70
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    check-cast p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    check-cast p3, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint$downloadSubReports$3;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/util/Optional;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    return-object p1
.end method
