.class public Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;
.super Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;
.source "TimeDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final isStartTime:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 98
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$ao6bCn3DNqCxdrpp37IggZoVxzg;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$ao6bCn3DNqCxdrpp37IggZoVxzg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;-><init>()V

    .line 39
    iput-boolean p1, p0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->isStartTime:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;)Z
    .locals 0

    .line 26
    iget-boolean p0, p0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->isStartTime:Z

    return p0
.end method

.method public static forEndTime()Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;
    .locals 2

    .line 29
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;-><init>(Z)V

    return-object v0
.end method

.method public static forStartTime()Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;-><init>(Z)V

    return-object v0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;
    .locals 1

    .line 99
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 100
    :goto_0
    new-instance p0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;-><init>(Z)V

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 94
    invoke-super {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 95
    iget-boolean p2, p0, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;->isStartTime:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
