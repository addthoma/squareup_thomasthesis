.class public final enum Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;
.super Ljava/lang/Enum;
.source "CategorySalesRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/CategorySalesRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CategorySalesRowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

.field public static final enum CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

.field public static final enum ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

.field public static final enum ITEM_VARIATION:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 8
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    const/4 v1, 0x0

    const-string v2, "CATEGORY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    const/4 v2, 0x1

    const-string v3, "ITEM"

    invoke-direct {v0, v3, v2}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    new-instance v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    const/4 v3, 0x2

    const-string v4, "ITEM_VARIATION"

    invoke-direct {v0, v4, v3}, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM_VARIATION:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    .line 7
    sget-object v4, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->CATEGORY:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->ITEM_VARIATION:Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->$VALUES:[Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->$VALUES:[Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    invoke-virtual {v0}, [Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/reports/applet/sales/v1/CategorySalesRow$CategorySalesRowType;

    return-object v0
.end method
