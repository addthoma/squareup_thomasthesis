.class public interface abstract Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen$Component;
.super Ljava/lang/Object;
.source "ReportConfigEmployeeFilterScreen.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract reportConfigEmployeeFilterCoordinator()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator;
.end method

.method public abstract reportConfigEmployeeFilterRunner()Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterRunner;
.end method
