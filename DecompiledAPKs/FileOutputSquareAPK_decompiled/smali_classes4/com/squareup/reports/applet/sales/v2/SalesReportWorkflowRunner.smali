.class public interface abstract Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;
.super Ljava/lang/Object;
.source "SalesReportWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00072\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "startWorkflow",
        "",
        "salesReportProps",
        "Lcom/squareup/salesreport/SalesReportProps;",
        "Companion",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner;->Companion:Lcom/squareup/reports/applet/sales/v2/SalesReportWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startWorkflow(Lcom/squareup/salesreport/SalesReportProps;)V
.end method
