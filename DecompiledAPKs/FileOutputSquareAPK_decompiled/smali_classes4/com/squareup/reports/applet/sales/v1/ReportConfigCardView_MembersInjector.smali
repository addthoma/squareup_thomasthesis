.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;
.super Ljava/lang/Object;
.source "ReportConfigCardView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;",
            ">;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 45
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;Ljava/lang/Object;)V
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->injectPresenter(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->injectDevice(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView_MembersInjector;->injectMembers(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    return-void
.end method
