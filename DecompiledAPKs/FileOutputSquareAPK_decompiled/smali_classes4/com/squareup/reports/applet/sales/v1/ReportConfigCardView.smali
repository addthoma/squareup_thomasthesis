.class public Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;
.super Landroid/widget/LinearLayout;
.source "ReportConfigCardView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# static fields
.field private static final SQUARE_START_YEAR:I = 0x7d9


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private allDayToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private datePicker:Lcom/squareup/register/widgets/date/DatePicker;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private deviceFilterToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

.field private endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field private itemDetailsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field private final timeFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const-class p2, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Component;->inject(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    .line 56
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->timeFormat:Ljava/text/DateFormat;

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 183
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public synthetic lambda$onFinishInflate$0$ReportConfigCardView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 90
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->setAllDayChecked(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$ReportConfigCardView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 92
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->setItemDetailsChecked(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$ReportConfigCardView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 94
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->setDeviceFilterChecked(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$3$ReportConfigCardView()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->onClickEmployeeFilter()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 187
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 116
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 62
    sget v0, Lcom/squareup/reports/applet/R$id;->date_picker:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/date/DatePicker;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    .line 63
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    new-instance v1, Ljava/util/GregorianCalendar;

    const/4 v2, 0x1

    const/16 v3, 0x7d9

    invoke-direct {v1, v3, v2, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/squareup/register/widgets/date/DatePicker;->setDateRange(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$1;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/date/DatePicker;->setListener(Lcom/squareup/register/widgets/date/DatePickerListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    sget v3, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    .line 76
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 75
    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/date/DatePicker;->setPageMargin(I)V

    .line 79
    :cond_0
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 80
    sget v0, Lcom/squareup/reports/applet/R$id;->config_item_details:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->itemDetailsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 81
    sget v0, Lcom/squareup/reports/applet/R$id;->config_device_filter:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->deviceFilterToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 82
    sget v0, Lcom/squareup/reports/applet/R$id;->config_employee_filter:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 83
    sget v0, Lcom/squareup/reports/applet/R$id;->config_all_day:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->allDayToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 84
    sget v0, Lcom/squareup/reports/applet/R$id;->config_start_time_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 85
    sget v0, Lcom/squareup/reports/applet/R$id;->config_end_time_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 86
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/reports/applet/R$string;->report_config_start_time:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    .line 87
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/reports/applet/R$string;->report_config_end_time:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->allDayToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$HtbS9kqie4I1-MXrl_elOOzMmt4;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$HtbS9kqie4I1-MXrl_elOOzMmt4;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->itemDetailsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$NikS2LThc-W2C2DKw5eM9FPTjl0;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$NikS2LThc-W2C2DKw5eM9FPTjl0;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->deviceFilterToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$x9n1ccZBXOrrXddm7zA30oqsegs;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$x9n1ccZBXOrrXddm7zA30oqsegs;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$qe7FWBZme6bRGaOvtvJhGZX7K8s;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$ReportConfigCardView$qe7FWBZme6bRGaOvtvJhGZX7K8s;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$2;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$2;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 104
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v1, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$3;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView$3;-><init>(Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 111
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->presenter:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setEmployeeFilterVisibility(Z)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 153
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    return-void
.end method

.method public showCalendarMonth(Ljava/util/Calendar;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->setDate(Ljava/util/Calendar;)V

    return-void
.end method

.method public updateConfigButtons(ZZ)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->itemDetailsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    .line 148
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->deviceFilterToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    return-void
.end method

.method public updateEmployeeFilterValueText(I)V
    .locals 3

    .line 163
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v2, Lcom/squareup/reports/applet/R$string;->report_config_employee_selection_multiple_employees:I

    .line 175
    invoke-static {v0, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v2, "number"

    .line 176
    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 178
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 174
    invoke-virtual {v1, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 170
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/reports/applet/R$string;->report_config_employee_selection_one_employee:I

    .line 171
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 166
    :cond_1
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->employeeFilter:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/reports/applet/R$string;->report_config_employee_selection_all_employees:I

    .line 167
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public updateTime(Ljava/util/Date;Ljava/util/Date;Z)V
    .locals 4

    .line 128
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->datePicker:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/register/widgets/date/DatePicker;->setSelectedRange(Ljava/util/Date;Ljava/util/Date;)V

    .line 130
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->allDayToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v1, 0x0

    invoke-virtual {v0, p3, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    .line 132
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/16 v2, 0x8

    if-eqz p3, :cond_0

    const/16 v3, 0x8

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    if-eqz p3, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    .line 135
    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->startTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->endTimeRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportConfigCardView;->timeFormat:Ljava/text/DateFormat;

    invoke-virtual {p3, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method
