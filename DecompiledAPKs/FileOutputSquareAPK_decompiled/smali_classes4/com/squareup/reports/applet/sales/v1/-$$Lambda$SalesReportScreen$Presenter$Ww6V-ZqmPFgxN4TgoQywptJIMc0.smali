.class public final synthetic Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/github/mikephil/charting/utils/ValueFormatter;


# instance fields
.field private final synthetic f$0:Lcom/squareup/protos/common/CurrencyCode;

.field private final synthetic f$1:Lcom/squareup/text/Formatter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;->f$0:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;->f$1:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public final getFormattedValue(F)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;->f$0:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$SalesReportScreen$Presenter$Ww6V-ZqmPFgxN4TgoQywptJIMc0;->f$1:Lcom/squareup/text/Formatter;

    invoke-static {v0, v1, p1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->lambda$new$0(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;F)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
