.class public final Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;
.super Ljava/lang/Object;
.source "LoyaltyReportSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;)",
            "Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/loyalty/LoyaltySettings;

    invoke-static {v0, v1}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/loyalty/LoyaltySettings;)Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection_Factory;->get()Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    move-result-object v0

    return-object v0
.end method
