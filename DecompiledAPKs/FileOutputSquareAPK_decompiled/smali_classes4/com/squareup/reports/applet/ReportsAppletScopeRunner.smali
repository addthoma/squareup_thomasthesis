.class public Lcom/squareup/reports/applet/ReportsAppletScopeRunner;
.super Ljava/lang/Object;
.source "ReportsAppletScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;
.implements Lmortar/bundler/Bundler;


# static fields
.field private static final CONFIG_KEY:Ljava/lang/String; = "config"


# instance fields
.field private lastReportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

.field private final reportConfigBuilder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

.field private final reportConfigSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final reports:Lcom/squareup/reports/applet/ReportsApplet;

.field private final salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/ReportsApplet;Lrx/subjects/BehaviorSubject;Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;",
            "Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reports:Lcom/squareup/reports/applet/ReportsApplet;

    .line 32
    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reportConfigSubject:Lrx/subjects/BehaviorSubject;

    .line 33
    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reportConfigBuilder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    .line 34
    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$ReportsAppletScopeRunner(Lcom/squareup/reports/applet/sales/v1/ReportConfig;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->lastReportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 38
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    invoke-virtual {v0, p0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reportConfigSubject:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/reports/applet/-$$Lambda$ReportsAppletScopeRunner$7Bs6CJWm8hL5yHb9gcLvkwRpPSc;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/-$$Lambda$ReportsAppletScopeRunner$7Bs6CJWm8hL5yHb9gcLvkwRpPSc;-><init>(Lcom/squareup/reports/applet/ReportsAppletScopeRunner;)V

    .line 42
    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 41
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reports:Lcom/squareup/reports/applet/ReportsApplet;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/ReportsApplet;->select()V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    invoke-interface {v0}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->revokePermissionForPinnedEmployee()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->lastReportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    const-string v0, "config"

    .line 59
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    .line 60
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reportConfigSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reportConfigSubject:Lrx/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->reportConfigBuilder:Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigBuilder;->defaultReport()Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletScopeRunner;->lastReportConfig:Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    const-string v1, "config"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
