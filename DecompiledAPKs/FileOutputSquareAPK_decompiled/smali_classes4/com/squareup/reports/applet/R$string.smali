.class public final Lcom/squareup/reports/applet/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final active_disputes_badge:I = 0x7f120053

.field public static final cash_management_email_address_hint:I = 0x7f120381

.field public static final confirm_save:I = 0x7f120484

.field public static final current_drawer_actual_in_drawer_hint:I = 0x7f120789

.field public static final current_drawer_confirm_end_drawer:I = 0x7f12078c

.field public static final current_drawer_confirm_start_drawer:I = 0x7f12078d

.field public static final current_drawer_drawer_description_hint:I = 0x7f12078e

.field public static final current_drawer_drawer_description_label_uppercase:I = 0x7f12078f

.field public static final current_drawer_end_drawer:I = 0x7f120790

.field public static final current_drawer_open:I = 0x7f120791

.field public static final current_drawer_report_saved:I = 0x7f120792

.field public static final current_drawer_start_drawer:I = 0x7f120793

.field public static final current_drawer_starting_cash_label_uppercase:I = 0x7f120794

.field public static final current_drawer_turn_on_cash_management:I = 0x7f120795

.field public static final date_format:I = 0x7f1207c9

.field public static final drawer_history_drawer_report:I = 0x7f1208da

.field public static final drawer_history_email_drawer_report:I = 0x7f1208db

.field public static final drawer_history_email_message:I = 0x7f1208dc

.field public static final drawer_history_email_message_offline:I = 0x7f1208dd

.field public static final drawer_history_null_state_subtitle:I = 0x7f1208de

.field public static final drawer_history_null_state_title:I = 0x7f1208df

.field public static final drawer_history_previous_drawers_uppercase:I = 0x7f1208e0

.field public static final drawer_history_send_drawer_report:I = 0x7f1208e1

.field public static final drawer_history_unclosed_drawers_uppercase:I = 0x7f1208e2

.field public static final new_badge:I = 0x7f121063

.field public static final paid_in_out:I = 0x7f1212fd

.field public static final paid_in_out_amount_hint:I = 0x7f1212fe

.field public static final paid_in_out_amount_uppercase:I = 0x7f1212ff

.field public static final paid_in_out_description_hint:I = 0x7f121300

.field public static final paid_in_out_description_uppercase:I = 0x7f121301

.field public static final paid_in_out_employee_full_name:I = 0x7f121302

.field public static final paid_in_out_employee_name:I = 0x7f121303

.field public static final paid_in_out_paid_in_button:I = 0x7f121304

.field public static final paid_in_out_paid_in_button_confirm:I = 0x7f121305

.field public static final paid_in_out_paid_out_button:I = 0x7f121306

.field public static final paid_in_out_paid_out_button_confirm:I = 0x7f121307

.field public static final report_calendar_date_range:I = 0x7f121677

.field public static final report_calendar_date_range_with_times:I = 0x7f121678

.field public static final report_calendar_single_day_time_range:I = 0x7f121679

.field public static final report_config_all_day:I = 0x7f12167a

.field public static final report_config_apply:I = 0x7f12167b

.field public static final report_config_customize:I = 0x7f12167c

.field public static final report_config_device_filter:I = 0x7f12167d

.field public static final report_config_employee_selection:I = 0x7f12167e

.field public static final report_config_employee_selection_all_employees:I = 0x7f12167f

.field public static final report_config_employee_selection_filter:I = 0x7f121680

.field public static final report_config_employee_selection_header_upercase:I = 0x7f121681

.field public static final report_config_employee_selection_loading:I = 0x7f121682

.field public static final report_config_employee_selection_multiple_employees:I = 0x7f121683

.field public static final report_config_employee_selection_name:I = 0x7f121684

.field public static final report_config_employee_selection_one_employee:I = 0x7f121685

.field public static final report_config_end_time:I = 0x7f121686

.field public static final report_config_hour_format:I = 0x7f121687

.field public static final report_config_items_details:I = 0x7f121688

.field public static final report_config_month_format:I = 0x7f121689

.field public static final report_config_month_short_format:I = 0x7f12168a

.field public static final report_config_start_time:I = 0x7f12168b

.field public static final reports_current_drawer:I = 0x7f12168c

.field public static final reports_drawer_history:I = 0x7f12168e

.field public static final reports_loyalty:I = 0x7f121692

.field public static final reports_sales:I = 0x7f121694

.field public static final sales_report:I = 0x7f1216af

.field public static final sales_report_average_sales:I = 0x7f1216b1

.field public static final sales_report_card:I = 0x7f1216b2

.field public static final sales_report_cash:I = 0x7f1216b3

.field public static final sales_report_cash_rounding:I = 0x7f1216b4

.field public static final sales_report_chart_highlight_plural:I = 0x7f1216b5

.field public static final sales_report_chart_highlight_single:I = 0x7f1216b6

.field public static final sales_report_chart_highlight_zero:I = 0x7f1216b7

.field public static final sales_report_cover_count:I = 0x7f1216d5

.field public static final sales_report_discounts:I = 0x7f1216f3

.field public static final sales_report_email_offline:I = 0x7f1216f4

.field public static final sales_report_email_report:I = 0x7f1216f5

.field public static final sales_report_email_sent:I = 0x7f1216f6

.field public static final sales_report_fees:I = 0x7f1216ff

.field public static final sales_report_gift_card:I = 0x7f121700

.field public static final sales_report_gift_card_sales:I = 0x7f121701

.field public static final sales_report_gross_sales:I = 0x7f121702

.field public static final sales_report_hint_customize:I = 0x7f121709

.field public static final sales_report_item_gross_sales:I = 0x7f12170a

.field public static final sales_report_name_with_quantity:I = 0x7f12170c

.field public static final sales_report_net_sales:I = 0x7f12170d

.field public static final sales_report_no_transactions_for_time_period:I = 0x7f121711

.field public static final sales_report_no_transactions_message:I = 0x7f121712

.field public static final sales_report_other:I = 0x7f121713

.field public static final sales_report_partial_refund:I = 0x7f12171d

.field public static final sales_report_print_report:I = 0x7f121732

.field public static final sales_report_product_sales:I = 0x7f121737

.field public static final sales_report_refund_count:I = 0x7f121738

.field public static final sales_report_refunds:I = 0x7f121739

.field public static final sales_report_returns:I = 0x7f12173a

.field public static final sales_report_sales_count:I = 0x7f12173c

.field public static final sales_report_select_time_frame:I = 0x7f12173d

.field public static final sales_report_service_charges:I = 0x7f12173e

.field public static final sales_report_tax:I = 0x7f121740

.field public static final sales_report_third_party_card:I = 0x7f121741

.field public static final sales_report_tip:I = 0x7f121755

.field public static final sales_report_total:I = 0x7f12175f

.field public static final sales_report_total_collected:I = 0x7f121760

.field public static final titlecase_reports:I = 0x7f1219e1

.field public static final uppercase_sales_report:I = 0x7f121b77

.field public static final uppercase_sales_report_amount:I = 0x7f121b78

.field public static final uppercase_sales_report_category_sales:I = 0x7f121b79

.field public static final uppercase_sales_report_daily_sales:I = 0x7f121b7a

.field public static final uppercase_sales_report_discounts_applied:I = 0x7f121b7b

.field public static final uppercase_sales_report_gross_sales:I = 0x7f121b7c

.field public static final uppercase_sales_report_hourly_sales:I = 0x7f121b7d

.field public static final uppercase_sales_report_items_sales:I = 0x7f121b7e

.field public static final uppercase_sales_report_items_sold:I = 0x7f121b7f

.field public static final uppercase_sales_report_monthly_sales:I = 0x7f121b80

.field public static final uppercase_sales_report_overview:I = 0x7f121b81

.field public static final uppercase_sales_report_quantity:I = 0x7f121b82

.field public static final uppercase_sales_report_sales_summary:I = 0x7f121b83

.field public static final uppercase_sales_report_sales_summary_payments_table:I = 0x7f121b84

.field public static final uppercase_sales_report_sales_summary_sales_table:I = 0x7f121b85

.field public static final uppercase_sales_report_summary:I = 0x7f121b86


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
