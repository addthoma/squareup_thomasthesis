.class public abstract Lcom/squareup/reports/applet/BaseEmailCardView;
.super Lcom/squareup/widgets/PairLayout;
.source "BaseEmailCardView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private contentFrame:Landroid/view/ViewGroup;

.field private emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private emailRecipient:Lcom/squareup/ui/XableEditText;

.field private presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

.field private sendEmailButton:Landroid/view/View;

.field private sendEmailContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/PairLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/BaseEmailCardView;)Z
    .locals 0

    .line 34
    invoke-direct {p0}, Lcom/squareup/reports/applet/BaseEmailCardView;->onSendEmail()Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/reports/applet/BaseEmailCardView;)Lcom/squareup/reports/applet/BaseEmailCardPresenter;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/reports/applet/BaseEmailCardView;)Landroid/view/View;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->sendEmailButton:Landroid/view/View;

    return-object p0
.end method

.method private onSendEmail()Z
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailRecipient:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->isValidEmail(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    iget-object v1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailRecipient:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->sendEmail(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract getPresenter()Lcom/squareup/reports/applet/BaseEmailCardPresenter;
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 104
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public onBackPressed()Z
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->dropView(Ljava/lang/Object;)V

    .line 96
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 51
    invoke-super {p0}, Lcom/squareup/widgets/PairLayout;->onFinishInflate()V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/reports/applet/BaseEmailCardView;->getPresenter()Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    .line 54
    sget v0, Lcom/squareup/reports/applet/R$id;->report_content_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->contentFrame:Landroid/view/ViewGroup;

    .line 55
    sget v0, Lcom/squareup/reports/applet/R$id;->send_email_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->sendEmailContainer:Landroid/view/ViewGroup;

    .line 56
    sget v0, Lcom/squareup/reports/applet/R$id;->email_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 58
    sget v0, Lcom/squareup/reports/applet/R$id;->email_recipient:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailRecipient:Lcom/squareup/ui/XableEditText;

    .line 59
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailRecipient:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->getDefaultEmailRecipient()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailRecipient:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/reports/applet/BaseEmailCardView$1;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/BaseEmailCardView$1;-><init>(Lcom/squareup/reports/applet/BaseEmailCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailRecipient:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/reports/applet/BaseEmailCardView$2;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/BaseEmailCardView$2;-><init>(Lcom/squareup/reports/applet/BaseEmailCardView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 73
    sget v0, Lcom/squareup/reports/applet/R$id;->send_email_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->sendEmailButton:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->sendEmailButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/reports/applet/BaseEmailCardView$3;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/BaseEmailCardView$3;-><init>(Lcom/squareup/reports/applet/BaseEmailCardView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 80
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->presenter:Lcom/squareup/reports/applet/BaseEmailCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method public showEmailMessage(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;Z)V
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(I)V

    const/4 p1, -0x1

    if-eq p2, p1, :cond_0

    .line 115
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(I)V

    .line 117
    :cond_0
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, p4}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    if-eqz p3, :cond_1

    .line 119
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget p2, Lcom/squareup/common/strings/R$string;->ok:I

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonText(I)V

    .line 120
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    new-instance p2, Lcom/squareup/reports/applet/BaseEmailCardView$4;

    invoke-direct {p2, p0}, Lcom/squareup/reports/applet/BaseEmailCardView$4;-><init>(Lcom/squareup/reports/applet/BaseEmailCardView;)V

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    if-eqz p5, :cond_2

    .line 127
    invoke-static {}, Lcom/squareup/register/widgets/Animations;->buildPulseAnimation()Landroid/view/animation/Animation;

    move-result-object p1

    .line 128
    iget-object p2, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->contentFrame:Landroid/view/ViewGroup;

    invoke-virtual {p2, p1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 129
    new-instance p2, Lcom/squareup/reports/applet/BaseEmailCardView$5;

    invoke-direct {p2, p0, p3}, Lcom/squareup/reports/applet/BaseEmailCardView$5;-><init>(Lcom/squareup/reports/applet/BaseEmailCardView;Z)V

    invoke-virtual {p1, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->sendEmailContainer:Landroid/view/ViewGroup;

    iget-object p2, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const-wide/16 p3, 0x14d

    invoke-static {p1, p2, p3, p4}, Lcom/squareup/util/Views;->crossFade(Landroid/view/View;Landroid/view/View;J)V

    goto :goto_0

    .line 136
    :cond_2
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->emailMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 138
    :goto_0
    iget-object p1, p0, Lcom/squareup/reports/applet/BaseEmailCardView;->sendEmailContainer:Landroid/view/ViewGroup;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public showEmailMessage(ILcom/squareup/glyph/GlyphTypeface$Glyph;Z)V
    .locals 6

    const/4 v2, -0x1

    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v4, p2

    move v5, p3

    .line 108
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/reports/applet/BaseEmailCardView;->showEmailMessage(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;Z)V

    return-void
.end method
