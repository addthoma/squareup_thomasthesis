.class public final Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;
.super Ljava/lang/Object;
.source "RealReportsAppletGateway_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/RealReportsAppletGateway;",
        ">;"
    }
.end annotation


# instance fields
.field private final reportsAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;->reportsAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsApplet;",
            ">;)",
            "Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/reports/applet/ReportsApplet;)Lcom/squareup/reports/applet/RealReportsAppletGateway;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/reports/applet/RealReportsAppletGateway;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/RealReportsAppletGateway;-><init>(Lcom/squareup/reports/applet/ReportsApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/RealReportsAppletGateway;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;->reportsAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/ReportsApplet;

    invoke-static {v0}, Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;->newInstance(Lcom/squareup/reports/applet/ReportsApplet;)Lcom/squareup/reports/applet/RealReportsAppletGateway;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/reports/applet/RealReportsAppletGateway_Factory;->get()Lcom/squareup/reports/applet/RealReportsAppletGateway;

    move-result-object v0

    return-object v0
.end method
