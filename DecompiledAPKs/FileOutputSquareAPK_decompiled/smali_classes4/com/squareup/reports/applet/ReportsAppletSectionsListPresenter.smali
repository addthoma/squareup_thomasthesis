.class public Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;
.super Lcom/squareup/applet/AppletSectionsListPresenter;
.source "ReportsAppletSectionsListPresenter.java"


# instance fields
.field private final cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;


# direct methods
.method public constructor <init>(Lcom/squareup/reports/applet/ReportsAppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/applet/AppletSectionsListPresenter;-><init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;)V

    .line 39
    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    .line 40
    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 41
    iput-object p5, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 42
    iput-object p6, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    .line 43
    iput-object p7, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    .line 44
    iput-object p8, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic access$001(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/permissions/EmployeeManagement;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    return-object p0
.end method

.method static synthetic access$301(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method static synthetic access$401(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    return-object p0
.end method

.method static synthetic access$601(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V
    .locals 0

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method private handleSalesReportSection(Ljava/util/Set;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;I)V"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->hasPermission(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-static {p0, p2}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$401(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V

    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v1, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$2;-><init>(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessExplicitlyGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method private handleSelectionWithPermission(Lcom/squareup/applet/AppletSection;Ljava/util/Set;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/applet/AppletSection;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;I)V"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v1, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;

    invoke-direct {v1, p0, p1, p3}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;-><init>(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/applet/AppletSection;I)V

    invoke-virtual {v0, p2, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method


# virtual methods
.method public onSectionClicked(I)V
    .locals 4

    .line 49
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;->revokeHasViewAmountInCashDrawerPermission()V

    .line 50
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->salesReportDetailLevelHolder:Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;

    invoke-interface {v0}, Lcom/squareup/api/salesreport/SalesReportDetailLevelHolder;->revokePermissionForPinnedEmployee()V

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    .line 53
    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v1

    .line 54
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    invoke-static {p0, p1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$001(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V

    goto :goto_0

    .line 57
    :cond_0
    iget-object v2, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_1

    instance-of v2, v0, Lcom/squareup/reports/applet/sales/SalesReportSection;

    if-eqz v2, :cond_1

    .line 59
    invoke-direct {p0, v1, p1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->handleSalesReportSection(Ljava/util/Set;I)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->handleSelectionWithPermission(Lcom/squareup/applet/AppletSection;Ljava/util/Set;I)V

    :goto_0
    return-void
.end method

.method public refreshIfSidebar()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListView;->refreshAdapter()V

    :cond_0
    return-void
.end method
