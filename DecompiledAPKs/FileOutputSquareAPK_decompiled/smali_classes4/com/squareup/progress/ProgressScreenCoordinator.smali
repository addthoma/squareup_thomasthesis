.class public final Lcom/squareup/progress/ProgressScreenCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ProgressScreenCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B+\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J \u0010\u0010\u001a\u00020\u00112\u0016\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/progress/ProgressScreenCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/progress/Progress$ScreenData;",
        "Lcom/squareup/progress/Progress$ProgressComplete;",
        "Lcom/squareup/progress/ProgressScreen;",
        "wrappedCoordinator",
        "Lcom/squareup/caller/ProgressDialogCoordinator;",
        "(Lio/reactivex/Observable;Lcom/squareup/caller/ProgressDialogCoordinator;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "detach",
        "render",
        "Lio/reactivex/Completable;",
        "screen",
        "progress_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;>;"
        }
    .end annotation
.end field

.field private final wrappedCoordinator:Lcom/squareup/caller/ProgressDialogCoordinator;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/caller/ProgressDialogCoordinator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;>;",
            "Lcom/squareup/caller/ProgressDialogCoordinator;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "wrappedCoordinator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->wrappedCoordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    return-void
.end method

.method public static final synthetic access$render(Lcom/squareup/progress/ProgressScreenCoordinator;Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/Completable;
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/progress/ProgressScreenCoordinator;->render(Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/Completable;

    move-result-object p0

    return-object p0
.end method

.method private final render(Lcom/squareup/workflow/legacy/Screen;)Lio/reactivex/Completable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/progress/Progress$ScreenData;",
            "Lcom/squareup/progress/Progress$ProgressComplete;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .line 42
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/progress/Progress$ScreenData;

    .line 43
    instance-of v1, v0, Lcom/squareup/progress/Progress$ScreenData$Progress;

    if-eqz v1, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->wrappedCoordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/progress/Progress$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/progress/Progress$ScreenData;->getMessage()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showProgress(I)V

    .line 45
    invoke-static {}, Lio/reactivex/Completable;->never()Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "Completable.never()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_0
    instance-of v0, v0, Lcom/squareup/progress/Progress$ScreenData$Complete;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->wrappedCoordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/progress/Progress$ScreenData;

    invoke-virtual {v1}, Lcom/squareup/progress/Progress$ScreenData;->getMessage()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showComplete(I)Lrx/Completable;

    move-result-object v0

    const-string/jumbo v1, "wrappedCoordinator.showC\u2026lete(screen.data.message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object v0

    .line 51
    new-instance v1, Lcom/squareup/progress/ProgressScreenCoordinator$render$1;

    invoke-direct {v1, p1}, Lcom/squareup/progress/ProgressScreenCoordinator$render$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Lio/reactivex/functions/Action;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->doOnComplete(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p1

    const-string/jumbo v0, "wrappedCoordinator.showC\u2026Event(ProgressComplete) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->wrappedCoordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->attach(Landroid/view/View;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/progress/ProgressScreenCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/progress/ProgressScreenCoordinator$attach$1;-><init>(Lcom/squareup/progress/ProgressScreenCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens.switchMap { scre\u2026n).toObservable<Unit>() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object v1, Lcom/squareup/progress/ProgressScreenCoordinator$attach$2;->INSTANCE:Lcom/squareup/progress/ProgressScreenCoordinator$attach$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/progress/ProgressScreenCoordinator;->wrappedCoordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->detach(Landroid/view/View;)V

    .line 33
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method
