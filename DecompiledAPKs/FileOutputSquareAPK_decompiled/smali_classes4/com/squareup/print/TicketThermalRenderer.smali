.class public Lcom/squareup/print/TicketThermalRenderer;
.super Ljava/lang/Object;
.source "TicketThermalRenderer.java"


# instance fields
.field private final builder:Lcom/squareup/print/ThermalBitmapBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method private renderItems(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ThermalBitmapBuilder;",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemBlockSection;",
            ">;)V"
        }
    .end annotation

    .line 74
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/sections/TicketItemBlockSection;

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/print/sections/TicketItemBlockSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 76
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private renderNoteAndDeviceName(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;)V
    .locals 3

    .line 81
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 82
    iget-object v1, p2, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 85
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 86
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_1
    if-eqz v0, :cond_2

    .line 90
    iget-object v2, p2, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_2
    if-eqz v1, :cond_4

    if-eqz v0, :cond_3

    .line 95
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 96
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 97
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 99
    :cond_3
    iget-object p2, p2, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    const-string v0, ""

    invoke-virtual {p1, v0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_4
    return-void
.end method

.method private renderReceiptNumbersAndPrinterStationName(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;)V
    .locals 3

    .line 105
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 106
    iget-object v1, p2, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    return-void

    .line 112
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 113
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 114
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    const-string v2, ""

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v2

    :goto_0
    if-eqz v1, :cond_2

    .line 117
    iget-object v2, p2, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    .line 119
    :cond_2
    invoke-virtual {p1, v0, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapseMediumText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/payload/TicketPayload;Z)Landroid/graphics/Bitmap;
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/print/TicketThermalRenderer;->renderHeader(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;)V

    .line 24
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    iget-object v1, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/FulfillmentSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 28
    :cond_0
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 30
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 33
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/squareup/print/TicketThermalRenderer;->renderItems(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/util/List;)V

    .line 36
    :goto_0
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/TicketThermalRenderer;->renderNoteAndDeviceName(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-direct {p0, v0, p1}, Lcom/squareup/print/TicketThermalRenderer;->renderReceiptNumbersAndPrinterStationName(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/print/TicketThermalRenderer;->renderReprint(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;Z)V

    .line 41
    iget-object p1, p0, Lcom/squareup/print/TicketThermalRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method renderHeader(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;)V
    .locals 2

    .line 45
    invoke-virtual {p2}, Lcom/squareup/print/payload/TicketPayload;->isCompactTicket()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->ticketTopPadding(Z)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 47
    invoke-virtual {p2}, Lcom/squareup/print/payload/TicketPayload;->isVoidTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->voidLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->voidBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 49
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 52
    :cond_0
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    .line 53
    invoke-static {v0}, Lcom/squareup/util/Strings;->isNumeric(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    .line 52
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->ticketHeaderSingleLine(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 55
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 57
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 59
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_1

    .line 62
    :cond_2
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    iget-object v1, p2, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 63
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 64
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 67
    :goto_1
    iget-object v0, p2, Lcom/squareup/print/payload/TicketPayload;->covers:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 68
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 69
    iget-object p2, p2, Lcom/squareup/print/payload/TicketPayload;->covers:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->singleColumn(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_3
    return-void
.end method

.method renderReprint(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/payload/TicketPayload;Z)V
    .locals 0

    if-eqz p3, :cond_0

    .line 125
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 126
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 127
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 128
    iget-object p2, p2, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->reprintBlock(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_0
    return-void
.end method
