.class public abstract Lcom/squareup/print/payload/ReceiptPayloadFactory;
.super Ljava/lang/Object;
.source "ReceiptPayloadFactory.java"


# instance fields
.field protected final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field protected final features:Lcom/squareup/settings/server/Features;

.field protected final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field protected final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field protected final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field protected final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field protected final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field protected final receiptInfoProvider:Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

.field protected final res:Lcom/squareup/util/Res;

.field protected final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field protected final taxFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field protected final tipSectionFactory:Lcom/squareup/print/papersig/TipSectionFactory;

.field protected final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/print/papersig/TipSectionFactory;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;)V
    .locals 0
    .param p8    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/ForTaxPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/print/papersig/TipSectionFactory;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;",
            ")V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 47
    iput-object p2, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    .line 48
    iput-object p3, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->tipSectionFactory:Lcom/squareup/print/papersig/TipSectionFactory;

    .line 49
    iput-object p4, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 50
    iput-object p5, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 51
    iput-object p6, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 52
    iput-object p7, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 53
    iput-object p8, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->taxFormatter:Lcom/squareup/text/Formatter;

    .line 54
    iput-object p9, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->res:Lcom/squareup/util/Res;

    .line 55
    iput-object p10, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 56
    iput-object p11, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->localeProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p12, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 58
    iput-object p13, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->receiptInfoProvider:Lcom/squareup/print/receiptinfoprovider/ReceiptInfoProvider;

    return-void
.end method


# virtual methods
.method protected buildReceiptFormatter(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/print/ReceiptFormatter;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/print/ReceiptFormatter;

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v3, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->taxFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->features:Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayloadFactory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 63
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v6

    move-object v0, v7

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/ReceiptFormatter;-><init>(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/CountryCode;)V

    return-object v7
.end method
