.class public Lcom/squareup/print/payload/StubPayload$Factory;
.super Ljava/lang/Object;
.source "StubPayload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/payload/StubPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final formatter:Lcom/squareup/print/ReceiptFormatter;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/print/payload/StubPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    .line 27
    iput-object p2, p0, Lcom/squareup/print/payload/StubPayload$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 28
    iput-object p3, p0, Lcom/squareup/print/payload/StubPayload$Factory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method public fromOrder(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;)Lcom/squareup/print/payload/StubPayload;
    .locals 8

    .line 32
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrder;->getTimestamp()Ljava/util/Date;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/squareup/print/payload/StubPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v1, v0}, Lcom/squareup/print/ReceiptFormatter;->getDateDetailString(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 34
    iget-object v1, p0, Lcom/squareup/print/payload/StubPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v1, v0}, Lcom/squareup/print/ReceiptFormatter;->getTimeDetailStringWithoutSeconds(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 35
    iget-object v0, p0, Lcom/squareup/print/payload/StubPayload$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v4

    .line 36
    iget-object v0, p0, Lcom/squareup/print/payload/StubPayload$Factory;->formatter:Lcom/squareup/print/ReceiptFormatter;

    invoke-virtual {v0, p2}, Lcom/squareup/print/ReceiptFormatter;->ticketNameOrBlank(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 39
    iget-object p2, p0, Lcom/squareup/print/payload/StubPayload$Factory;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p1}, Lcom/squareup/print/PrintableOrder;->getEmployeeToken()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 40
    :cond_0
    iget-object p1, p1, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    :goto_0
    move-object v7, p1

    .line 42
    new-instance p1, Lcom/squareup/print/payload/StubPayload;

    move-object v2, p1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/print/payload/StubPayload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object p1
.end method
