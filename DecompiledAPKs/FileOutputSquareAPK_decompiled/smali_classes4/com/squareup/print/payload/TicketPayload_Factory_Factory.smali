.class public final Lcom/squareup/print/payload/TicketPayload_Factory_Factory;
.super Ljava/lang/Object;
.source "TicketPayload_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/payload/TicketPayload$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceNameSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/ReceiptFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/ReceiptFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->formatterProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->deviceNameSettingProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/payload/TicketPayload_Factory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/ReceiptFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/print/payload/TicketPayload_Factory_Factory;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)Lcom/squareup/print/payload/TicketPayload$Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/print/payload/TicketPayload$Factory;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/print/payload/TicketPayload$Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/print/payload/TicketPayload$Factory;-><init>(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/print/payload/TicketPayload$Factory;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/print/ReceiptFormatter;

    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->deviceNameSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v4, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v7}, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->newInstance(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;)Lcom/squareup/print/payload/TicketPayload$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/print/payload/TicketPayload_Factory_Factory;->get()Lcom/squareup/print/payload/TicketPayload$Factory;

    move-result-object v0

    return-object v0
.end method
