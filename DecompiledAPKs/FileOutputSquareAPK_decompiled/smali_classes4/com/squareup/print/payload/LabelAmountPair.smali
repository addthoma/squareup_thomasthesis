.class public Lcom/squareup/print/payload/LabelAmountPair;
.super Ljava/lang/Object;
.source "LabelAmountPair.java"


# instance fields
.field public final amount:Ljava/lang/String;

.field public final label:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    return-void
.end method

.method public static empty()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 2

    .line 13
    new-instance v0, Lcom/squareup/print/payload/LabelAmountPair;

    const-string v1, ""

    invoke-direct {v0, v1, v1}, Lcom/squareup/print/payload/LabelAmountPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 29
    instance-of v0, p1, Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    check-cast p1, Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p1, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    .line 30
    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    .line 31
    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 25
    iget-object v1, p0, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 35
    iget-object v1, p0, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "LabelAmountPair(\"%s\", %s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
