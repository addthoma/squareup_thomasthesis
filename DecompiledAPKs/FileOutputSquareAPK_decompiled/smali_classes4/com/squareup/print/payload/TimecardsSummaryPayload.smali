.class public Lcom/squareup/print/payload/TimecardsSummaryPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "TimecardsSummaryPayload.java"


# instance fields
.field public final employeeSummarySection:Lcom/squareup/print/sections/EmployeeSummarySection;

.field public final printedAtSection:Lcom/squareup/print/sections/PrintedAtSection;

.field public final reportHeaderText:Ljava/lang/String;

.field public final shiftHeaderText:Ljava/lang/String;

.field public final shiftSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ShiftSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/print/sections/EmployeeSummarySection;Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/sections/PrintedAtSection;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/print/sections/EmployeeSummarySection;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ShiftSection;",
            ">;",
            "Lcom/squareup/print/sections/PrintedAtSection;",
            ")V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/print/payload/TimecardsSummaryPayload;->reportHeaderText:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/squareup/print/payload/TimecardsSummaryPayload;->employeeSummarySection:Lcom/squareup/print/sections/EmployeeSummarySection;

    .line 27
    iput-object p3, p0, Lcom/squareup/print/payload/TimecardsSummaryPayload;->shiftHeaderText:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/squareup/print/payload/TimecardsSummaryPayload;->shiftSections:Ljava/util/List;

    .line 29
    iput-object p5, p0, Lcom/squareup/print/payload/TimecardsSummaryPayload;->printedAtSection:Lcom/squareup/print/sections/PrintedAtSection;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_TIMECARDS_SUMMARY:Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 0

    .line 33
    new-instance p2, Lcom/squareup/print/TimecardsSummaryRenderer;

    invoke-direct {p2, p1}, Lcom/squareup/print/TimecardsSummaryRenderer;-><init>(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 34
    invoke-virtual {p2, p0}, Lcom/squareup/print/TimecardsSummaryRenderer;->renderBitmap(Lcom/squareup/print/payload/TimecardsSummaryPayload;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
