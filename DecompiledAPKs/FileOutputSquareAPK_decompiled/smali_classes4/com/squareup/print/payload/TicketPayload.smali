.class public Lcom/squareup/print/payload/TicketPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "TicketPayload.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/payload/TicketPayload$Factory;
    }
.end annotation


# instance fields
.field public final covers:Ljava/lang/String;

.field public final date:Ljava/lang/String;

.field public final deviceName:Ljava/lang/String;

.field public final employeeFirstName:Ljava/lang/String;

.field public final fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

.field private isCompactTicket:Z

.field isManualReprint:Z

.field public final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemBlockSection;",
            ">;"
        }
    .end annotation
.end field

.field public final note:Ljava/lang/String;

.field public final printerStation:Ljava/lang/String;

.field public final receiptNumbers:Ljava/lang/String;

.field public final reprintLabel:Ljava/lang/String;

.field public final ticketName:Ljava/lang/String;

.field public final time:Ljava/lang/String;

.field public final voidLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/sections/FulfillmentSection;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/print/sections/FulfillmentSection;",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/TicketItemBlockSection;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 227
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    .line 228
    iput-object p1, p0, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    .line 229
    iput-object p2, p0, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    .line 230
    iput-object p3, p0, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    .line 231
    iput-object p4, p0, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    .line 232
    iput-object p5, p0, Lcom/squareup/print/payload/TicketPayload;->covers:Ljava/lang/String;

    .line 233
    iput-object p6, p0, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    .line 234
    iput-object p7, p0, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    .line 235
    iput-object p8, p0, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    .line 236
    iput-object p9, p0, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    .line 237
    iput-object p10, p0, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    .line 238
    iput-object p11, p0, Lcom/squareup/print/payload/TicketPayload;->voidLabel:Ljava/lang/String;

    .line 239
    iput-object p12, p0, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    .line 240
    iput-object p13, p0, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    const/4 p1, 0x0

    .line 241
    iput-boolean p1, p0, Lcom/squareup/print/payload/TicketPayload;->isManualReprint:Z

    .line 242
    iput-boolean p1, p0, Lcom/squareup/print/payload/TicketPayload;->isCompactTicket:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_16

    .line 264
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_9

    .line 266
    :cond_1
    check-cast p1, Lcom/squareup/print/payload/TicketPayload;

    .line 268
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 271
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 274
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    invoke-virtual {v2, v3}, Lcom/squareup/print/sections/FulfillmentSection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 278
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 281
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_4

    :cond_a
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    if-eqz v2, :cond_b

    :goto_4
    return v1

    .line 284
    :cond_b
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    goto :goto_5

    :cond_c
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    if-eqz v2, :cond_d

    :goto_5
    return v1

    .line 288
    :cond_d
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    goto :goto_6

    :cond_e
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    if-eqz v2, :cond_f

    :goto_6
    return v1

    .line 291
    :cond_f
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    goto :goto_7

    :cond_10
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    if-eqz v2, :cond_11

    :goto_7
    return v1

    .line 294
    :cond_11
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v2, :cond_12

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    .line 295
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    goto :goto_8

    :cond_12
    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v2, :cond_13

    :goto_8
    return v1

    .line 300
    :cond_13
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    return v1

    .line 304
    :cond_14
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_15

    return v1

    :cond_15
    return v0

    :cond_16
    :goto_9
    return v1
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 1

    .line 351
    invoke-virtual {p0}, Lcom/squareup/print/payload/TicketPayload;->isVoidTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_VOID_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

    return-object v0

    .line 354
    :cond_0
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_TICKET_PAPER:Lcom/squareup/analytics/RegisterActionName;

    return-object v0
.end method

.method public getAnalyticsPrintJobType()Ljava/lang/String;
    .locals 1

    .line 343
    invoke-virtual {p0}, Lcom/squareup/print/payload/TicketPayload;->isVoidTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "void_ticket"

    return-object v0

    :cond_0
    const-string v0, "ticket"

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .line 312
    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 313
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->date:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 314
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 315
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 316
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/squareup/print/sections/FulfillmentSection;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 317
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    if-eqz v2, :cond_5

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 318
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 319
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 320
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 321
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 322
    iget-object v2, p0, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_a
    add-int/2addr v0, v1

    return v0
.end method

.method public isCompactTicket()Z
    .locals 1

    .line 251
    iget-boolean v0, p0, Lcom/squareup/print/payload/TicketPayload;->isCompactTicket:Z

    return v0
.end method

.method public isVoidTicket()Z
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/print/payload/TicketPayload;->voidLabel:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 1

    .line 328
    new-instance v0, Lcom/squareup/print/TicketThermalRenderer;

    invoke-direct {v0, p1}, Lcom/squareup/print/TicketThermalRenderer;-><init>(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 331
    iget-boolean p1, p0, Lcom/squareup/print/payload/TicketPayload;->isManualReprint:Z

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    invoke-virtual {v0, p0, p1}, Lcom/squareup/print/TicketThermalRenderer;->renderBitmap(Lcom/squareup/print/payload/TicketPayload;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public renderText(Lcom/squareup/print/text/ImpactTextBuilder;Z)Lcom/squareup/print/text/RenderedRows;
    .locals 1

    .line 336
    new-instance v0, Lcom/squareup/print/TicketImpactRenderer;

    invoke-direct {v0, p1}, Lcom/squareup/print/TicketImpactRenderer;-><init>(Lcom/squareup/print/text/ImpactTextBuilder;)V

    .line 339
    iget-boolean p1, p0, Lcom/squareup/print/payload/TicketPayload;->isManualReprint:Z

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    invoke-virtual {v0, p0, p1}, Lcom/squareup/print/TicketImpactRenderer;->renderText(Lcom/squareup/print/payload/TicketPayload;Z)Lcom/squareup/print/text/RenderedRows;

    move-result-object p1

    return-object p1
.end method

.method public setIsCompactTicket(Z)V
    .locals 0

    .line 255
    iput-boolean p1, p0, Lcom/squareup/print/payload/TicketPayload;->isCompactTicket:Z

    return-void
.end method

.method public setManualReprint(Z)V
    .locals 0

    .line 259
    iput-boolean p1, p0, Lcom/squareup/print/payload/TicketPayload;->isManualReprint:Z

    return-void
.end method
