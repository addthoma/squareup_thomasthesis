.class public interface abstract Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;
.super Ljava/lang/Object;
.source "PrintSpooler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintSpooler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PrintJobStatusListener"
.end annotation


# virtual methods
.method public abstract onPrintJobAttempted(Lcom/squareup/print/PrintJob;)V
.end method

.method public abstract onPrintJobEnqueued(Lcom/squareup/print/PrintJob;)V
.end method
