.class public Lcom/squareup/print/OrderPrintingDispatcher;
.super Ljava/lang/Object;
.source "OrderPrintingDispatcher.java"


# static fields
.field static final PRINTER_STATION_PRINTS_CATEGORY:Lcom/squareup/util/SquareCollections$Classifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/SquareCollections$Classifier<",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final UNKNOWN_ORDER_ID:Ljava/lang/String; = "UnknownOrder"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final billPayloadFactory:Lcom/squareup/print/payload/TicketBillPayloadFactory;

.field private final context:Landroid/content/Context;

.field private final historicalReceiptPayloadFactory:Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final printSettings:Lcom/squareup/print/PrintSettings;

.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final receiptPayloadFactory:Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final stubPayloadFactory:Lcom/squareup/print/payload/StubPayload$Factory;

.field private final ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/SquareCollections$Classifier<",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 76
    sget-object v0, Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;->INSTANCE:Lcom/squareup/print/-$$Lambda$OrderPrintingDispatcher$SiDQxu_HJAAShE9oJLhsrsVfPHQ;

    sput-object v0, Lcom/squareup/print/OrderPrintingDispatcher;->PRINTER_STATION_PRINTS_CATEGORY:Lcom/squareup/util/SquareCollections$Classifier;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TicketBillPayloadFactory;Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;Lcom/squareup/print/payload/StubPayload$Factory;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/print/PrintSettings;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintSpooler;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/print/payload/TicketBillPayloadFactory;",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
            "Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;",
            "Lcom/squareup/print/payload/StubPayload$Factory;",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Landroid/app/Application;",
            "Lcom/squareup/print/PrintSettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 100
    sget-object v2, Lcom/squareup/print/OrderPrintingDispatcher;->PRINTER_STATION_PRINTS_CATEGORY:Lcom/squareup/util/SquareCollections$Classifier;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/print/OrderPrintingDispatcher;-><init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/SquareCollections$Classifier;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TicketBillPayloadFactory;Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;Lcom/squareup/print/payload/StubPayload$Factory;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Landroid/content/Context;Lcom/squareup/print/PrintSettings;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/SquareCollections$Classifier;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/payload/TicketBillPayloadFactory;Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;Lcom/squareup/print/payload/StubPayload$Factory;Lcom/squareup/print/payload/TicketPayload$Factory;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Ljavax/inject/Provider;Landroid/content/Context;Lcom/squareup/print/PrintSettings;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintSpooler;",
            "Lcom/squareup/util/SquareCollections$Classifier<",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/print/payload/TicketBillPayloadFactory;",
            "Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;",
            "Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;",
            "Lcom/squareup/print/payload/StubPayload$Factory;",
            "Lcom/squareup/print/payload/TicketPayload$Factory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Landroid/content/Context;",
            "Lcom/squareup/print/PrintSettings;",
            ")V"
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p5, p0, Lcom/squareup/print/OrderPrintingDispatcher;->receiptPayloadFactory:Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    .line 114
    iput-object p6, p0, Lcom/squareup/print/OrderPrintingDispatcher;->historicalReceiptPayloadFactory:Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;

    .line 115
    iput-object p7, p0, Lcom/squareup/print/OrderPrintingDispatcher;->stubPayloadFactory:Lcom/squareup/print/payload/StubPayload$Factory;

    .line 116
    iput-object p8, p0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;

    .line 117
    iput-object p1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    .line 118
    iput-object p3, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 119
    iput-object p2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    .line 120
    iput-object p4, p0, Lcom/squareup/print/OrderPrintingDispatcher;->billPayloadFactory:Lcom/squareup/print/payload/TicketBillPayloadFactory;

    .line 121
    iput-object p9, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    .line 122
    iput-object p10, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    .line 123
    iput-object p11, p0, Lcom/squareup/print/OrderPrintingDispatcher;->localeProvider:Ljavax/inject/Provider;

    .line 124
    iput-object p12, p0, Lcom/squareup/print/OrderPrintingDispatcher;->context:Landroid/content/Context;

    .line 125
    iput-object p13, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSettings:Lcom/squareup/print/PrintSettings;

    return-void
.end method

.method private breakNonUnitPricedItemIntoSingleQuantityItems(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .line 656
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 657
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintableOrderItem;

    .line 658
    invoke-interface {v1}, Lcom/squareup/print/PrintableOrderItem;->isUnitPriced()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 659
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 665
    :goto_1
    invoke-interface {v1}, Lcom/squareup/print/PrintableOrderItem;->getQuantityAsInt()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 666
    new-instance v3, Lcom/squareup/print/SingleQuantityPrintableOrderItem;

    invoke-direct {v3, v1}, Lcom/squareup/print/SingleQuantityPrintableOrderItem;-><init>(Lcom/squareup/print/PrintableOrderItem;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private dispatchStubTo(Lcom/squareup/print/payload/StubPayload;Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/payload/StubPayload;",
            "Lcom/squareup/print/PrintableOrder;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;Z)V"
        }
    .end annotation

    .line 397
    invoke-static {p2, p5}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableItems(Lcom/squareup/print/PrintableOrder;Z)Ljava/util/List;

    move-result-object p5

    .line 399
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {p0, p3, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Ljava/lang/String;Lcom/squareup/print/PrinterStation$Role;)Ljava/lang/String;

    move-result-object p3

    .line 400
    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p4

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 401
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrintableOrderItem;

    .line 402
    iget-object v4, p0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    invoke-interface {v4, v1, v3}, Lcom/squareup/util/SquareCollections$Classifier;->classContains(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 403
    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-direct {p0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, p1, p3, v3}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 410
    :cond_2
    iget-object p1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forStubDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private dispatchTicketsTo(Ljava/lang/String;ZLjava/util/Map;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map<",
            "Lcom/squareup/print/PrinterStation;",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/TicketPayload;",
            ">;>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 440
    iget-object p2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forReprintTicketDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 442
    :cond_0
    iget-object p2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3}, Ljava/util/Map;->size()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forTicketDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 445
    :goto_0
    sget-object p2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Ljava/lang/String;Lcom/squareup/print/PrinterStation$Role;)Ljava/lang/String;

    move-result-object p1

    .line 446
    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Map$Entry;

    .line 447
    invoke-interface {p3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/payload/TicketPayload;

    .line 448
    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-interface {p3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/print/PrintTarget;

    invoke-virtual {v2, v3, v1, p1, p4}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method private getBuyerLocaleOverrideFactory(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/locale/LocaleOverrideFactory;
    .locals 3

    .line 524
    new-instance v0, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-virtual {p1, v2}, Lcom/squareup/billhistory/model/TenderHistory;->getBuyerSelectedLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    return-object v0
.end method

.method private getBuyerLocaleOverrideFactory(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/locale/LocaleOverrideFactory;
    .locals 3

    .line 520
    new-instance v0, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-virtual {p1, v2}, Lcom/squareup/payment/tender/BaseTender;->getBuyerSelectedLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    return-object v0
.end method

.method private getExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    .line 642
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrinterStations;->getEnabledExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private getJobTitle(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;
    .locals 1

    .line 532
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getDisplayName()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 536
    :cond_0
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 537
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 540
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getJobTitle(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;
    .locals 1

    .line 508
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getTicketName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getTicketName()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 512
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getReceiptNumber()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 513
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Payment;->getReceiptNumber()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 516
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/print/R$string;->buyer_printed_receipt_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getOrderId(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;
    .locals 0

    .line 553
    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getOrderId(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string p1, "UnknownOrder"

    return-object p1

    .line 564
    :cond_0
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getOrderId(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;
    .locals 0

    .line 557
    invoke-virtual {p1}, Lcom/squareup/payment/PaymentReceipt;->getOrderSnapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getOrderId(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    .line 568
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrder;->getOrderIdentifier()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 569
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrder;->getOrderIdentifier()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "UnknownOrder"

    return-object p1
.end method

.method private static getPrintableItems(Lcom/squareup/print/PrintableOrder;Z)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintableOrder;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 418
    invoke-interface {p0}, Lcom/squareup/print/PrintableOrder;->getNotVoidedLockedItems()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 419
    :cond_0
    invoke-interface {p0}, Lcom/squareup/print/PrintableOrder;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;
    .locals 0

    .line 373
    invoke-static {p0}, Lcom/squareup/print/OrderPrintingDispatcher;->getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromSnapshot(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/print/PrintableOrder;

    move-result-object p0

    return-object p0
.end method

.method private static getPrintableOrderFromSnapshot(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/print/PrintableOrder;
    .locals 1

    .line 377
    new-instance v0, Lcom/squareup/print/PrintablePaymentOrder;

    invoke-direct {v0, p0}, Lcom/squareup/print/PrintablePaymentOrder;-><init>(Lcom/squareup/payment/OrderSnapshot;)V

    return-object v0
.end method

.method private getSellerLocaleOverrideFactory()Lcom/squareup/locale/LocaleOverrideFactory;
    .locals 3

    .line 528
    new-instance v0, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    return-object v0
.end method

.method public static getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    .line 367
    instance-of v0, p0, Lcom/squareup/payment/OrderSnapshot;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/payment/OrderSnapshot;

    goto :goto_0

    .line 369
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->snapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStation$Role;",
            ")",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    .line 638
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrinterStations;->getEnabledStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$static$0(Lcom/squareup/print/PrinterStation;Lcom/squareup/print/PrintableOrderItem;)Z
    .locals 1

    .line 76
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->isUncategorized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/squareup/print/PrinterStation;->printsUncategorizedItems()Z

    move-result p0

    goto :goto_0

    .line 78
    :cond_0
    invoke-interface {p1}, Lcom/squareup/print/PrintableOrderItem;->getCategoryId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/print/PrinterStation;->isEnabledForCategoryId(Ljava/lang/String;)Z

    move-result p0

    :goto_0
    return p0
.end method


# virtual methods
.method public autoPrintItemizedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/payment/tender/BaseTender;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 350
    invoke-virtual {p0, p3}, Lcom/squareup/print/OrderPrintingDispatcher;->getReceiptStationsForAutoPrint(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v3

    .line 351
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_0

    return-void

    .line 354
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getBuyerLocaleOverrideFactory(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object p3

    .line 355
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->receiptPayloadFactory:Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    sget-object v1, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    .line 356
    invoke-virtual {v0, p3, p1, p2, v1}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createItemizedReceiptPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object v1

    .line 358
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;

    move-result-object v2

    .line 359
    sget-object v4, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchReceipt(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/lang/String;)V

    return-void
.end method

.method createPayloadByPrinterStation(Ljava/util/Collection;Lcom/squareup/print/PrintableOrder;Ljava/lang/String;ZZ)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/print/PrintableOrder;",
            "Ljava/lang/String;",
            "ZZ)",
            "Ljava/util/Map<",
            "Lcom/squareup/print/PrinterStation;",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/TicketPayload;",
            ">;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v8, p2

    move/from16 v1, p4

    .line 586
    invoke-static {v8, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableItems(Lcom/squareup/print/PrintableOrder;Z)Ljava/util/List;

    move-result-object v1

    .line 588
    iget-object v2, v0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    move-object/from16 v3, p1

    .line 589
    invoke-static {v3, v1, v2}, Lcom/squareup/util/SquareCollections;->classify(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/util/SquareCollections$Classifier;)Ljava/util/Map;

    move-result-object v1

    .line 590
    new-instance v9, Ljava/util/LinkedHashMap;

    invoke-direct {v9}, Ljava/util/LinkedHashMap;-><init>()V

    .line 592
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 593
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/squareup/print/PrinterStation;

    if-eqz v11, :cond_0

    .line 595
    invoke-virtual {v11}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v12

    .line 597
    invoke-virtual {v11}, Lcom/squareup/print/PrinterStation;->isPrintCompactTicketsEnabled()Z

    move-result v13

    .line 598
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 599
    iget-object v2, v0, Lcom/squareup/print/OrderPrintingDispatcher;->printSettings:Lcom/squareup/print/PrintSettings;

    invoke-interface {v2, v11}, Lcom/squareup/print/PrintSettings;->shouldPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 601
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 600
    invoke-direct {v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->breakNonUnitPricedItemIntoSingleQuantityItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintableOrderItem;

    .line 604
    iget-object v2, v0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;

    .line 605
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    move-object v1, v2

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v5, p5

    move v6, v13

    move-object v7, v12

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/print/payload/TicketPayload$Factory;->fromOrder(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/List;ZZLjava/lang/String;)Lcom/squareup/print/payload/TicketPayload;

    move-result-object v1

    .line 604
    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 609
    :cond_1
    iget-object v2, v0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Ljava/util/List;

    move-object v1, v2

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v5, p5

    move v6, v13

    move-object v7, v12

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/print/payload/TicketPayload$Factory;->fromOrder(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/List;ZZLjava/lang/String;)Lcom/squareup/print/payload/TicketPayload;

    move-result-object v1

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 613
    :cond_2
    invoke-interface {v9, v11, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 617
    :cond_3
    invoke-static {v9}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method dispatchAuthSlip(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;ZLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintablePayload;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-eqz p4, :cond_0

    .line 497
    iget-object p4, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forAuthSlipCopyDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 499
    :cond_0
    iget-object p4, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forAuthSlipDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 502
    :goto_0
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/print/PrinterStation;

    .line 503
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v0, p4, p1, p2, p5}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method dispatchBill(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;)V
    .locals 5

    .line 381
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->billPayloadFactory:Lcom/squareup/print/payload/TicketBillPayloadFactory;

    invoke-virtual {v0, p1}, Lcom/squareup/print/payload/TicketBillPayloadFactory;->fromOrder(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/print/payload/TicketBillPayload;

    move-result-object v0

    .line 383
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {p0, p2, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Ljava/lang/String;Lcom/squareup/print/PrinterStation$Role;)Ljava/lang/String;

    move-result-object p2

    .line 384
    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/PrinterStation;

    .line 385
    iget-object v3, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/payment/Order;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v0, p2, v4}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method dispatchReceipt(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintablePayload;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 483
    sget-object v0, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-ne p4, v0, :cond_0

    .line 484
    iget-object p4, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forGiftReceiptDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 486
    :cond_0
    iget-object p4, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p3}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/print/PrintDispatchedEvent;->forReceiptDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v0

    invoke-interface {p4, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 489
    :goto_0
    invoke-interface {p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/print/PrinterStation;

    .line 490
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v0, p4, p1, p2, p5}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method dispatchStub(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintableOrder;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;Z)V"
        }
    .end annotation

    .line 391
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->stubPayloadFactory:Lcom/squareup/print/payload/StubPayload$Factory;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/print/payload/StubPayload$Factory;->fromOrder(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;)Lcom/squareup/print/payload/StubPayload;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    .line 392
    invoke-direct/range {v1 .. v6}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchStubTo(Lcom/squareup/print/payload/StubPayload;Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    return-void
.end method

.method dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintableOrder;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;Z)V"
        }
    .end annotation

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p4

    move-object v5, p3

    .line 425
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;ZZLjava/util/Collection;)V

    return-void
.end method

.method dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;ZZLjava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintableOrder;",
            "Ljava/lang/String;",
            "ZZ",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    .line 432
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->createPayloadByPrinterStation(Ljava/util/Collection;Lcom/squareup/print/PrintableOrder;Ljava/lang/String;ZZ)Ljava/util/Map;

    move-result-object p3

    .line 434
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p4, p3, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTicketsTo(Ljava/lang/String;ZLjava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method dispatchVoidTickets(Lcom/squareup/print/PrintableOrder;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/util/Collection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintableOrder;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 456
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketItemClassifier:Lcom/squareup/util/SquareCollections$Classifier;

    .line 457
    invoke-static {p4, p3, v0}, Lcom/squareup/util/SquareCollections;->classify(Ljava/util/Collection;Ljava/util/Collection;Lcom/squareup/util/SquareCollections$Classifier;)Ljava/util/Map;

    move-result-object p4

    .line 458
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 460
    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lcom/squareup/print/PrintDispatchedEvent;->forVoidTicketDispatched(I)Lcom/squareup/print/PrintDispatchedEvent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 462
    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :cond_0
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 463
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 464
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    if-eqz v1, :cond_0

    .line 466
    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->ticketPayloadFactory:Lcom/squareup/print/payload/TicketPayload$Factory;

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    iget-object v3, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    .line 467
    invoke-interface {p1, v3}, Lcom/squareup/print/PrintableOrder;->getDiningOption(Lcom/squareup/util/Res;)Lcom/squareup/checkout/DiningOption;

    move-result-object v6

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->isPrintCompactTicketsEnabled()Z

    move-result v7

    .line 468
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p1}, Lcom/squareup/print/PrintableOrder;->getReceiptNumbers()Ljava/util/List;

    move-result-object v9

    move-object v3, p3

    move-object v4, p2

    .line 466
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/print/payload/TicketPayload$Factory;->fromVoidItems(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Ljava/util/Date;Lcom/squareup/checkout/DiningOption;ZLjava/lang/String;Ljava/util/List;)Lcom/squareup/print/payload/TicketPayload;

    move-result-object v2

    .line 469
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 474
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/tickets/OpenTicket;->getName()Ljava/lang/String;

    move-result-object p2

    sget-object p3, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-virtual {p0, p2, p3}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Ljava/lang/String;Lcom/squareup/print/PrinterStation$Role;)Ljava/lang/String;

    move-result-object p2

    .line 475
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p3

    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/util/Map$Entry;

    .line 476
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-interface {p4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrintTarget;

    invoke-interface {p4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/print/PrintablePayload;

    .line 477
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/print/PrintableOrder;)Ljava/lang/String;

    move-result-object v2

    .line 476
    invoke-virtual {v0, v1, p4, p2, v2}, Lcom/squareup/print/PrintSpooler;->enqueueForPrint(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public getJobTitle(Ljava/lang/String;Lcom/squareup/print/PrinterStation$Role;)Ljava/lang/String;
    .locals 2

    .line 545
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/print/R$string;->print_job_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "ticket_name"

    .line 546
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    iget p2, p2, Lcom/squareup/print/PrinterStation$Role;->stringResId:I

    .line 547
    invoke-interface {v0, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "label"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 548
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 549
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getReceiptStationsForAutoPrint(Ljava/util/Collection;)Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation

    .line 622
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 624
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 625
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 626
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterStation;

    .line 627
    invoke-virtual {v1}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v1

    iget-boolean v1, v1, Lcom/squareup/print/PrinterStationConfiguration;->autoPrintItemizedReceipts:Z

    if-nez v1, :cond_0

    .line 630
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public printAuthSlip(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;ZZLjava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/payment/tender/BaseTender;",
            "ZZ",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 301
    invoke-interface {p5}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p4, :cond_1

    .line 307
    invoke-direct {p0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getBuyerLocaleOverrideFactory(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/squareup/print/OrderPrintingDispatcher;->getSellerLocaleOverrideFactory()Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    :goto_0
    move-object v2, v0

    .line 308
    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->receiptPayloadFactory:Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    .line 309
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createAuthSlipPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;ZZ)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object v4

    .line 311
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;

    move-result-object v5

    .line 312
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p0

    move-object v6, p5

    move v7, p4

    invoke-virtual/range {v3 .. v8}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchAuthSlip(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;ZLjava/lang/String;)V

    return-void
.end method

.method public printBill(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 4

    .line 130
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    const-string v0, "ticketName"

    .line 133
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 134
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchBill(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;)V

    return-void
.end method

.method public printBillStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 1

    .line 138
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getSnapshotOfOrder(Lcom/squareup/payment/Order;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object p1

    .line 139
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromSnapshot(Lcom/squareup/payment/OrderSnapshot;)Lcom/squareup/print/PrintableOrder;

    move-result-object v0

    .line 140
    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printBillStubAndTicket(Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/print/PrintableOrder;Ljava/lang/String;)V

    return-void
.end method

.method printBillStubAndTicket(Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/print/PrintableOrder;Ljava/lang/String;)V
    .locals 6

    .line 145
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-interface {v0, v2}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    .line 146
    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    new-array v3, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v5, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v5, v3, v4

    invoke-interface {v2, v3}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v2

    .line 147
    iget-object v3, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v5, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v5, v1, v4

    invoke-interface {v3, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v3, "ticketName"

    .line 154
    invoke-static {p3, v3}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p0, p1, p3}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchBill(Lcom/squareup/payment/OrderSnapshot;Ljava/lang/String;)V

    :cond_1
    if-eqz v2, :cond_2

    .line 162
    sget-object p1, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object p1

    invoke-virtual {p0, p2, p3, p1, v4}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchStub(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_2
    if-eqz v1, :cond_3

    .line 166
    sget-object p1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object p1

    invoke-virtual {p0, p2, p3, p1, v4}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_3
    return-void
.end method

.method public printItemizedReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/PaymentReceipt;",
            "Lcom/squareup/payment/tender/BaseTender;",
            "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 317
    invoke-interface {p4}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 322
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getBuyerLocaleOverrideFactory(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    .line 323
    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->receiptPayloadFactory:Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;

    .line 324
    invoke-virtual {v1, v0, p1, p2, p3}, Lcom/squareup/print/payload/PaymentReceiptPayloadFactory;->createItemizedReceiptPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/payment/tender/BaseTender;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object v3

    .line 326
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;

    move-result-object v4

    .line 327
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/payment/PaymentReceipt;)Ljava/lang/String;

    move-result-object v7

    move-object v2, p0

    move-object v5, p4

    move-object v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchReceipt(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/lang/String;)V

    return-void
.end method

.method public printReceipt(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V
    .locals 7

    .line 332
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->RECEIPTS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v4

    .line 333
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p2, :cond_1

    .line 339
    invoke-direct {p0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getBuyerLocaleOverrideFactory(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    goto :goto_0

    .line 340
    :cond_1
    invoke-direct {p0}, Lcom/squareup/print/OrderPrintingDispatcher;->getSellerLocaleOverrideFactory()Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    .line 341
    :goto_0
    iget-object v1, p0, Lcom/squareup/print/OrderPrintingDispatcher;->historicalReceiptPayloadFactory:Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;

    invoke-virtual {v1, v0, p1, p2, p3}, Lcom/squareup/print/payload/HistoricalReceiptPayloadFactory;->createPayload(Lcom/squareup/locale/LocaleOverrideFactory;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object v2

    .line 343
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getJobTitle(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v3

    .line 344
    invoke-direct {p0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getOrderId(Lcom/squareup/billhistory/model/BillHistory;)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchReceipt(Lcom/squareup/print/PrintablePayload;Ljava/lang/String;Ljava/util/Collection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/lang/String;)V

    return-void
.end method

.method public printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 1

    .line 189
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Z)V

    return-void
.end method

.method public printStubAndTicket(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Z)V
    .locals 5

    .line 194
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-interface {v0, v2}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    .line 195
    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v3, v1, v4

    invoke-interface {v2, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "ticketName"

    .line 202
    invoke-static {p2, v2}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 205
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchStub(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_1
    if-eqz v1, :cond_2

    .line 209
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_2
    return-void
.end method

.method public printStubAndTicketExternalOnly(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 0

    .line 214
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicketExternalOnly(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;)V

    return-void
.end method

.method printStubAndTicketExternalOnly(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;)V
    .locals 5

    .line 219
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v2, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-interface {v0, v2}, Lcom/squareup/print/PrinterStations;->hasEnabledExternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    .line 220
    iget-object v2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v3, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v3, v1, v4

    invoke-interface {v2, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledExternalStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v1

    if-nez v0, :cond_0

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-string v2, "ticketName"

    .line 227
    invoke-static {p2, v2}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 230
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, v4}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchStub(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_1
    if-eqz v1, :cond_2

    .line 234
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getExternalStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, v4}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_2
    return-void
.end method

.method public printTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 1

    .line 239
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->printTicket(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method

.method public printTicket(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ticketName"

    .line 244
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 245
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3, v3}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_0
    return-void
.end method

.method public printTicketStub(Lcom/squareup/payment/Order;Ljava/lang/String;)V
    .locals 1

    .line 251
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->STUBS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->printTicketStub(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)V

    return-void
.end method

.method public printTicketStub(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/lang/String;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    const-string v0, "ticketName"

    .line 256
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 257
    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchStub(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;Ljava/util/Collection;Z)V

    :cond_0
    return-void
.end method

.method public printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)V"
        }
    .end annotation

    .line 171
    sget-object v0, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/print/OrderPrintingDispatcher;->printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/util/Collection;)V

    return-void
.end method

.method public printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/tickets/OpenTicket;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/PrinterStation;",
            ">;)V"
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/print/PrinterStation$Role;

    sget-object v2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result v0

    const-string v1, "openTicket"

    .line 178
    invoke-static {p2, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 179
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    if-eqz v0, :cond_0

    .line 182
    sget-object v0, Lcom/squareup/print/PrintablePaymentOrder;->Companion:Lcom/squareup/print/PrintablePaymentOrder$Companion;

    .line 183
    invoke-virtual {v0, p3}, Lcom/squareup/print/PrintablePaymentOrder$Companion;->convertCartItemsToItems(Ljava/util/List;)Ljava/util/List;

    move-result-object p3

    .line 182
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchVoidTickets(Lcom/squareup/print/PrintableOrder;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;Ljava/util/Collection;)V

    :cond_0
    return-void
.end method

.method public reprintSavedTicketFromTransaction(Lcom/squareup/payment/Transaction;)V
    .locals 3

    .line 267
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->timestampAsDate()Ljava/util/Date;

    move-result-object v1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x1

    .line 267
    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/payment/Order;Ljava/util/Date;Ljava/lang/String;Z)V

    return-void
.end method

.method public reprintTicket(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 276
    iget-object v0, p1, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->mostRecentTenderTimestamp()Ljava/util/Date;

    move-result-object v1

    .line 277
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory;->getTicketName()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    .line 276
    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/payment/Order;Ljava/util/Date;Ljava/lang/String;Z)V

    return-void
.end method

.method public reprintTicket(Lcom/squareup/payment/Order;Ljava/util/Date;Ljava/lang/String;Z)V
    .locals 0

    .line 283
    invoke-static {p1}, Lcom/squareup/print/OrderPrintingDispatcher;->getPrintableOrderFromOrder(Lcom/squareup/payment/Order;)Lcom/squareup/print/PrintableOrder;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/print/PrintableOrder;Ljava/util/Date;Ljava/lang/String;Z)V

    return-void
.end method

.method reprintTicket(Lcom/squareup/print/PrintableOrder;Ljava/util/Date;Ljava/lang/String;Z)V
    .locals 6

    .line 289
    invoke-interface {p1, p2}, Lcom/squareup/print/PrintableOrder;->setTimestampForReprint(Ljava/util/Date;)V

    .line 291
    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/transaction/R$string;->kitchen_printing_order:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    :cond_0
    move-object v2, p3

    .line 293
    iget-object p2, p0, Lcom/squareup/print/OrderPrintingDispatcher;->printerStations:Lcom/squareup/print/PrinterStations;

    const/4 p3, 0x1

    new-array p3, p3, [Lcom/squareup/print/PrinterStation$Role;

    const/4 v0, 0x0

    sget-object v1, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    aput-object v1, p3, v0

    invoke-interface {p2, p3}, Lcom/squareup/print/PrinterStations;->hasEnabledStationsForAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 v4, 0x1

    .line 295
    sget-object p2, Lcom/squareup/print/PrinterStation$Role;->TICKETS:Lcom/squareup/print/PrinterStation$Role;

    invoke-direct {p0, p2}, Lcom/squareup/print/OrderPrintingDispatcher;->getStationsFor(Lcom/squareup/print/PrinterStation$Role;)Ljava/util/Collection;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/print/OrderPrintingDispatcher;->dispatchTickets(Lcom/squareup/print/PrintableOrder;Ljava/lang/String;ZZLjava/util/Collection;)V

    :cond_1
    return-void
.end method
