.class public final Lcom/squareup/print/PrintErrorReprintEvent;
.super Lcom/squareup/print/PrinterEvent;
.source "PrintErrorReprintEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/print/PrintErrorReprintEvent;",
        "Lcom/squareup/print/PrinterEvent;",
        "info",
        "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
        "error",
        "",
        "jobCount",
        "",
        "(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;I)V",
        "getJobCount",
        "()I",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final jobCount:I


# direct methods
.method public constructor <init>(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;I)V
    .locals 12

    const-string v0, "info"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "error"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_ERROR_REPRINT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v0, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v0, "PRINT_ERROR_REPRINT.value"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xf0

    const/4 v11, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v11}, Lcom/squareup/print/PrinterEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p3, p0, Lcom/squareup/print/PrintErrorReprintEvent;->jobCount:I

    return-void
.end method


# virtual methods
.method public final getJobCount()I
    .locals 1

    .line 12
    iget v0, p0, Lcom/squareup/print/PrintErrorReprintEvent;->jobCount:I

    return v0
.end method
