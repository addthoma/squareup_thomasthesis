.class public Lcom/squareup/print/papersig/QuickTipSection;
.super Ljava/lang/Object;
.source "QuickTipSection.java"


# instance fields
.field public final calculatedOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final customTipLabel:Ljava/lang/String;

.field public final options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final title:Ljava/lang/String;

.field public final totalLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "title"

    .line 22
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/print/papersig/QuickTipSection;->title:Ljava/lang/String;

    const-string p1, "options"

    .line 23
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/print/papersig/QuickTipSection;->options:Ljava/util/List;

    const-string p1, "calculatedOptions"

    .line 24
    invoke-static {p3, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/print/papersig/QuickTipSection;->calculatedOptions:Ljava/util/List;

    .line 25
    iput-object p4, p0, Lcom/squareup/print/papersig/QuickTipSection;->customTipLabel:Ljava/lang/String;

    .line 26
    iput-object p5, p0, Lcom/squareup/print/papersig/QuickTipSection;->totalLabel:Ljava/lang/String;

    .line 28
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p2

    if-ne p1, p2, :cond_0

    return-void

    .line 29
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Number of options and calculated options must be equal."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_9

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 42
    :cond_1
    check-cast p1, Lcom/squareup/print/papersig/QuickTipSection;

    .line 44
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->customTipLabel:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/papersig/QuickTipSection;->customTipLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/papersig/QuickTipSection;->customTipLabel:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 48
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/papersig/QuickTipSection;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    return v1

    .line 49
    :cond_4
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->options:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/papersig/QuickTipSection;->options:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    return v1

    .line 50
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->calculatedOptions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/papersig/QuickTipSection;->calculatedOptions:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    return v1

    .line 51
    :cond_6
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->totalLabel:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/papersig/QuickTipSection;->totalLabel:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_8

    goto :goto_1

    :cond_7
    if-eqz p1, :cond_8

    :goto_1
    return v1

    :cond_8
    return v0

    :cond_9
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 59
    iget-object v0, p0, Lcom/squareup/print/papersig/QuickTipSection;->customTipLabel:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 60
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->title:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 61
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->options:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 62
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->calculatedOptions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 63
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->totalLabel:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 7

    .line 34
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->EXTRA_LARGE:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 35
    iget-object v2, p0, Lcom/squareup/print/papersig/QuickTipSection;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/print/papersig/QuickTipSection;->options:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/print/papersig/QuickTipSection;->calculatedOptions:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/print/papersig/QuickTipSection;->customTipLabel:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/print/papersig/QuickTipSection;->totalLabel:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/print/ThermalBitmapBuilder;->quickTipOptions(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
