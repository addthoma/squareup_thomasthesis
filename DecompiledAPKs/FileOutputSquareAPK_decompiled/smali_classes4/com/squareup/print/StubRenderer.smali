.class public Lcom/squareup/print/StubRenderer;
.super Ljava/lang/Object;
.source "StubRenderer.java"


# instance fields
.field private final builder:Lcom/squareup/print/ThermalBitmapBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/payload/StubPayload;)Landroid/graphics/Bitmap;
    .locals 3

    .line 16
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/StubPayload;->ticketName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->stubHeaderSingleLine(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 18
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 19
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 20
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 22
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->canFitIntoTwoColumnsOnSingleLine(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 24
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 25
    iget-object v0, p1, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object p1, p1, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->date:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/print/payload/StubPayload;->time:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 34
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 35
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/StubPayload;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 36
    iget-object v0, p1, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 37
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;->TINY:Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;

    invoke-virtual {v0, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->space(Lcom/squareup/print/ThermalBitmapBuilder$SpaceSize;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 38
    iget-object v0, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object p1, p1, Lcom/squareup/print/payload/StubPayload;->employeeFirstName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 42
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 43
    iget-object p1, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 45
    iget-object p1, p0, Lcom/squareup/print/StubRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
