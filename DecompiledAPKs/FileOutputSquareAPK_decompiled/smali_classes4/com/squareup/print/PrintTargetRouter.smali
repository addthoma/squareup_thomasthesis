.class public interface abstract Lcom/squareup/print/PrintTargetRouter;
.super Ljava/lang/Object;
.source "PrintTargetRouter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintTargetRouter$RouteResult;
    }
.end annotation


# virtual methods
.method public abstract retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation
.end method
