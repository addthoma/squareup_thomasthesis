.class public Lcom/squareup/print/GiftReceiptPayloadRenderer;
.super Ljava/lang/Object;
.source "GiftReceiptPayloadRenderer.java"

# interfaces
.implements Lcom/squareup/print/ReceiptPayloadRenderer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/payload/ReceiptPayload;Lcom/squareup/print/ThermalBitmapBuilder;)Landroid/graphics/Bitmap;
    .locals 1

    .line 8
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getHeader()Lcom/squareup/print/sections/HeaderSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/HeaderSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 9
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getCodes()Lcom/squareup/print/sections/CodesSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/CodesSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 11
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 13
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getFooter()Lcom/squareup/print/sections/FooterSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/FooterSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 16
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getBarcode()Lcom/squareup/print/sections/BarcodeSection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 17
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getBarcode()Lcom/squareup/print/sections/BarcodeSection;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/print/sections/BarcodeSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 20
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
