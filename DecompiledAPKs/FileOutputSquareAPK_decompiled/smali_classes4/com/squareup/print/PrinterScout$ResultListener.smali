.class public interface abstract Lcom/squareup/print/PrinterScout$ResultListener;
.super Ljava/lang/Object;
.source "PrinterScout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrinterScout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ResultListener"
.end annotation


# virtual methods
.method public abstract onResult(Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;)V"
        }
    .end annotation
.end method
