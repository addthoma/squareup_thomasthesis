.class public abstract Lcom/squareup/print/PrinterStation;
.super Ljava/lang/Object;
.source "PrinterStation.java"

# interfaces
.implements Lcom/squareup/print/PrintTarget;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrinterStation$Role;
    }
.end annotation


# instance fields
.field private final stationUuid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/print/PrinterStation;->stationUuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract commit(Lcom/squareup/print/PrinterStationConfiguration;)V
.end method

.method public abstract getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;
.end method

.method public abstract getDisabledCategoryIds()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/print/PrinterStation;->stationUuid:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSelectedHardwarePrinterId()Ljava/lang/String;
.end method

.method public varargs abstract hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
.end method

.method public hasAutomaticPaperCutter()Z
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/print/PrinterStation;->isInternal()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public abstract hasHardwarePrinterSelected()Z
.end method

.method public abstract isAutoPrintItemizedReceiptsEnabled()Z
.end method

.method public abstract isEnabled()Z
.end method

.method public abstract isEnabledForCategoryId(Ljava/lang/String;)Z
.end method

.method public abstract isInternal()Z
.end method

.method public abstract isPrintATicketForEachItemEnabled()Z
.end method

.method public abstract isPrintCompactTicketsEnabled()Z
.end method

.method public abstract printsUncategorizedItems()Z
.end method
