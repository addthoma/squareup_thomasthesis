.class public interface abstract Lcom/squareup/print/PrintableOrderItemModifier;
.super Ljava/lang/Object;
.source "PrintableOrderItemModifier.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/print/PrintableOrderItemModifier;",
        "",
        "basePriceTimesModifierQuantity",
        "Lcom/squareup/protos/common/Money;",
        "getBasePriceTimesModifierQuantity",
        "()Lcom/squareup/protos/common/Money;",
        "hideFromCustomer",
        "",
        "getHideFromCustomer",
        "()Z",
        "getDisplayName",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getBasePriceTimesModifierQuantity()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getDisplayName(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method

.method public abstract getHideFromCustomer()Z
.end method
