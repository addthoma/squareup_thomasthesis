.class public Lcom/squareup/print/HardwarePrinterTracker;
.super Ljava/lang/Object;
.source "HardwarePrinterTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/HardwarePrinterTracker$Listener;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final availablePrinters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation
.end field

.field private cachedHardwareInfos:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
            ">;>;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/print/HardwarePrinterTracker;->cachedHardwareInfos:Lcom/squareup/settings/LocalSetting;

    .line 44
    iput-object p2, p0, Lcom/squareup/print/HardwarePrinterTracker;->analytics:Lcom/squareup/analytics/Analytics;

    .line 46
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    .line 47
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/HardwarePrinterTracker;->listeners:Ljava/util/List;

    return-void
.end method

.method private notifyListenersForConnectedPrinter(Lcom/squareup/print/HardwarePrinter;)V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/HardwarePrinterTracker$Listener;

    .line 142
    invoke-interface {v1, p1}, Lcom/squareup/print/HardwarePrinterTracker$Listener;->printerConnected(Lcom/squareup/print/HardwarePrinter;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private notifyListenersForDisconnectedPrinter(Lcom/squareup/print/HardwarePrinter;)V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->listeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/HardwarePrinterTracker$Listener;

    .line 148
    invoke-interface {v1, p1}, Lcom/squareup/print/HardwarePrinterTracker$Listener;->printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public debugForgetAllPrinters()V
    .locals 1

    .line 90
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/print/HardwarePrinterTracker;->setAvailableHardwarePrinters(Ljava/util/Map;)V

    return-void
.end method

.method public getAllAvailableHardwarePrinters()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->cachedHardwareInfos:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    return-object p1
.end method

.method public getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/HardwarePrinter;

    return-object p1
.end method

.method public removeListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setAvailableHardwarePrinters(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;)V"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 103
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 104
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 105
    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    iget-object v2, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/HardwarePrinter;

    .line 107
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 108
    invoke-direct {p0, v1}, Lcom/squareup/print/HardwarePrinterTracker;->notifyListenersForDisconnectedPrinter(Lcom/squareup/print/HardwarePrinter;)V

    goto :goto_0

    .line 113
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 114
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 115
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/HardwarePrinter;

    .line 117
    iget-object v2, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    .line 118
    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v3

    .line 119
    invoke-virtual {v3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasAllRequiredFields()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->isSupported()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 121
    iget-object v4, p0, Lcom/squareup/print/HardwarePrinterTracker;->cachedHardwareInfos:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v4}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    .line 122
    invoke-interface {v4, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v3, p0, Lcom/squareup/print/HardwarePrinterTracker;->availablePrinters:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v2, :cond_2

    .line 130
    invoke-direct {p0, v0}, Lcom/squareup/print/HardwarePrinterTracker;->notifyListenersForConnectedPrinter(Lcom/squareup/print/HardwarePrinter;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->cachedHardwareInfos:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0, v4}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 135
    :cond_3
    iget-object v1, p0, Lcom/squareup/print/HardwarePrinterTracker;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/print/PrinterEventKt;->forUnsupportedPrinterConnect(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Lcom/squareup/print/PrinterEvent;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method public setHardwareInfoForPrinter(Ljava/lang/String;Lcom/squareup/print/HardwarePrinter$HardwareInfo;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinterTracker;->cachedHardwareInfos:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
