.class public Lcom/squareup/print/sections/TenderSection;
.super Ljava/lang/Object;
.source "TenderSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/sections/TenderSection$Builder;
    }
.end annotation


# instance fields
.field public final amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

.field public final amountActuallyTenderedShort:Lcom/squareup/print/payload/LabelAmountPair;

.field public final amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

.field public final buyerSelectedAccountName:Ljava/lang/String;

.field public final cardHolderName:Ljava/lang/String;

.field public final change:Lcom/squareup/print/payload/LabelAmountPair;

.field public final felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

.field public final idTransactionTypeLocation:Ljava/lang/String;

.field public final remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

.field public final tip:Lcom/squareup/print/payload/LabelAmountPair;


# direct methods
.method public constructor <init>(Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)V
    .locals 0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    .line 110
    iput-object p2, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTenderedShort:Lcom/squareup/print/payload/LabelAmountPair;

    .line 111
    iput-object p3, p0, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    .line 112
    iput-object p4, p0, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    .line 113
    iput-object p5, p0, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    .line 114
    iput-object p6, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    .line 115
    iput-object p7, p0, Lcom/squareup/print/sections/TenderSection;->cardHolderName:Ljava/lang/String;

    .line 116
    iput-object p8, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    .line 117
    iput-object p9, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    .line 118
    iput-object p10, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    return-void
.end method

.method public static fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/sections/TenderSection;
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p8

    const/4 v2, 0x0

    if-eqz p4, :cond_0

    .line 28
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    move-object/from16 v4, p5

    invoke-virtual {p0, v4, v3}, Lcom/squareup/print/ReceiptFormatter;->changeOrZero(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v3

    move-object v8, v3

    goto :goto_0

    :cond_0
    move-object v8, v2

    .line 33
    :goto_0
    invoke-static/range {p6 .. p6}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v1, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_2

    move-object/from16 v4, p6

    .line 35
    invoke-virtual {p0, v4, v1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v4

    move-object v5, v4

    goto :goto_2

    :cond_2
    move-object v5, v2

    :goto_2
    if-eqz v3, :cond_3

    move-object/from16 v3, p7

    .line 38
    invoke-virtual {p0, v3, v1}, Lcom/squareup/print/ReceiptFormatter;->formatMoney(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v2

    :cond_3
    move-object v6, v2

    .line 41
    new-instance v1, Lcom/squareup/print/sections/TenderSection;

    move-object/from16 v2, p9

    .line 44
    invoke-virtual {p0, v2}, Lcom/squareup/print/ReceiptFormatter;->amountToTender(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v7

    move-object/from16 v2, p3

    .line 46
    invoke-virtual {p0, v2}, Lcom/squareup/print/ReceiptFormatter;->tipOrNullIfZero(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v9

    move-object/from16 v2, p2

    .line 47
    invoke-virtual {p0, v2}, Lcom/squareup/print/ReceiptFormatter;->remainingBalance(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v10

    move-object v4, v1

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v4 .. v14}, Lcom/squareup/print/sections/TenderSection;-><init>(Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/payload/LabelAmountPair;)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_12

    .line 123
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_8

    .line 125
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/TenderSection;

    .line 127
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 132
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 137
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 141
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 142
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_a

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    goto :goto_4

    :cond_a
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_b

    :goto_4
    return v1

    .line 146
    :cond_b
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_c

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v2, v3}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    goto :goto_5

    :cond_c
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_d

    :goto_5
    return v1

    .line 147
    :cond_d
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v3, p1, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    .line 148
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    goto :goto_6

    :cond_e
    iget-object v2, p1, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    if-eqz v2, :cond_f

    :goto_6
    return v1

    .line 152
    :cond_f
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object p1, p1, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_10

    invoke-virtual {v2, p1}, Lcom/squareup/print/payload/LabelAmountPair;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_11

    goto :goto_7

    :cond_10
    if-eqz p1, :cond_11

    :goto_7
    return v1

    :cond_11
    return v0

    :cond_12
    :goto_8
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 162
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 163
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 164
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 165
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 166
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 167
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 168
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 169
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 171
    iget-object v2, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/squareup/print/payload/LabelAmountPair;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    if-nez v0, :cond_0

    return-void

    .line 181
    :cond_0
    iget-object v1, p0, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    if-nez v1, :cond_4

    .line 183
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 185
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 190
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 193
    :cond_2
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_3

    .line 194
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_9

    .line 201
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 202
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 206
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 207
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 208
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 210
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_5

    .line 212
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 213
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 214
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->change:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 217
    :cond_5
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_6

    .line 218
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->amountToTender:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 219
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 222
    :cond_6
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->amountActuallyTendered:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 224
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 225
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->buyerSelectedAccountName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 228
    :cond_7
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 229
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->idTransactionTypeLocation:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 235
    :cond_8
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_9

    .line 236
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 237
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 242
    :cond_9
    :goto_0
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->cardHolderName:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 243
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 247
    :cond_a
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_c

    .line 248
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 249
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 250
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 251
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->remainingBalance:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_b

    .line 252
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 254
    :cond_b
    iget-object v0, p0, Lcom/squareup/print/sections/TenderSection;->felicaTerminalId:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_c
    return-void
.end method
