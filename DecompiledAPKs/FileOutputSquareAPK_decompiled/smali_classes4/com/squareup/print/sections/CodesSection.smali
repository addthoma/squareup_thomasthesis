.class public Lcom/squareup/print/sections/CodesSection;
.super Ljava/lang/Object;
.source "CodesSection.java"


# static fields
.field private static final GST_TAX_ID_TYPE:Ljava/lang/String; = "ca_gst"

.field private static final HST_TAX_ID_TYPE:Ljava/lang/String; = "ca_hst"


# instance fields
.field public final authorizationCode:Ljava/lang/String;

.field public final receiptNumber:Ljava/lang/String;

.field public final sequentialTenderNumber:Ljava/lang/String;

.field public final taxIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final ticketName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/squareup/print/sections/CodesSection;->ticketName:Ljava/lang/String;

    .line 147
    iput-object p2, p0, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    .line 148
    iput-object p3, p0, Lcom/squareup/print/sections/CodesSection;->authorizationCode:Ljava/lang/String;

    .line 149
    iput-object p4, p0, Lcom/squareup/print/sections/CodesSection;->sequentialTenderNumber:Ljava/lang/String;

    .line 150
    iput-object p5, p0, Lcom/squareup/print/sections/CodesSection;->taxIds:Ljava/util/List;

    return-void
.end method

.method private static buildTaxIds(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 48
    invoke-static {p0}, Lcom/squareup/print/sections/CodesSection;->getTaxIdsByType(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/Map;

    move-result-object p0

    .line 49
    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    return-object p0

    .line 54
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 55
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/OrderAdjustment;

    .line 56
    sget-object v2, Lcom/squareup/print/sections/CodesSection$1;->$SwitchMap$com$squareup$checkout$SubtotalType:[I

    iget-object v3, v1, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    invoke-virtual {v3}, Lcom/squareup/checkout/SubtotalType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x1

    if-eq v2, v3, :cond_2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    goto :goto_0

    .line 59
    :cond_2
    iget-object v2, v1, Lcom/squareup/payment/OrderAdjustment;->taxTypeId:Ljava/lang/String;

    invoke-interface {p0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/TaxId;

    .line 60
    iget-object v3, v1, Lcom/squareup/payment/OrderAdjustment;->historicalTaxTypeName:Ljava/lang/String;

    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/squareup/payment/OrderAdjustment;->historicalTaxTypeNumber:Ljava/lang/String;

    .line 61
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 62
    :cond_3
    new-instance v2, Lcom/squareup/server/account/protos/TaxId$Builder;

    invoke-direct {v2}, Lcom/squareup/server/account/protos/TaxId$Builder;-><init>()V

    iget-object v3, v1, Lcom/squareup/payment/OrderAdjustment;->historicalTaxTypeName:Ljava/lang/String;

    .line 63
    invoke-virtual {v2, v3}, Lcom/squareup/server/account/protos/TaxId$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/TaxId$Builder;

    move-result-object v2

    iget-object v1, v1, Lcom/squareup/payment/OrderAdjustment;->historicalTaxTypeNumber:Ljava/lang/String;

    .line 64
    invoke-virtual {v2, v1}, Lcom/squareup/server/account/protos/TaxId$Builder;->number(Ljava/lang/String;)Lcom/squareup/server/account/protos/TaxId$Builder;

    move-result-object v1

    .line 65
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/TaxId$Builder;->build()Lcom/squareup/server/account/protos/TaxId;

    move-result-object v2

    :cond_4
    if-eqz v2, :cond_1

    .line 68
    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_5
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_6

    .line 75
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    return-object p0

    .line 79
    :cond_6
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 81
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_7
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/TaxId;

    .line 82
    iget-object v1, v0, Lcom/squareup/server/account/protos/TaxId;->number:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 84
    iget-object v1, v0, Lcom/squareup/server/account/protos/TaxId;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/server/account/protos/TaxId;->number:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/print/ReceiptFormatter;->taxId(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_8
    return-object p0
.end method

.method public static fromOrder(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/sections/CodesSection;
    .locals 6

    .line 35
    invoke-static {p6}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getDisplayName()Ljava/lang/String;

    move-result-object p6

    .line 38
    :cond_0
    invoke-virtual {p1, p6}, Lcom/squareup/print/ReceiptFormatter;->ticketNameOrNull(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object p2

    invoke-static {p0, p1, p2}, Lcom/squareup/print/sections/CodesSection;->buildTaxIds(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 42
    new-instance p0, Lcom/squareup/print/sections/CodesSection;

    move-object v0, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/print/sections/CodesSection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-object p0
.end method

.method private getNonBlankCodesAsList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CodesSection;->ticketName:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CodesSection;->authorizationCode:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/CodesSection;->sequentialTenderNumber:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/print/util/PrintRendererUtils;->removeBlanks([Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static getTaxIdForType(Lcom/squareup/server/account/protos/TaxId;Ljava/lang/String;Lcom/squareup/server/account/FeeTypes;)Lcom/squareup/server/account/protos/TaxId;
    .locals 1

    const-string v0, "ca_gst"

    .line 122
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ca_hst"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    :cond_0
    invoke-virtual {p2, p1}, Lcom/squareup/server/account/FeeTypes;->withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 125
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/TaxId;->newBuilder()Lcom/squareup/server/account/protos/TaxId$Builder;

    move-result-object p0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FeeType;->name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/TaxId$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/TaxId$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/server/account/protos/TaxId$Builder;->build()Lcom/squareup/server/account/protos/TaxId;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method private static getTaxIdsByType(Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/server/account/protos/TaxId;",
            ">;"
        }
    .end annotation

    .line 98
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTaxIds()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    .line 100
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p0

    return-object p0

    .line 103
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 104
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getFeeTypes()Lcom/squareup/server/account/FeeTypes;

    move-result-object p0

    .line 105
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/TaxId;

    .line 106
    iget-object v3, v2, Lcom/squareup/server/account/protos/TaxId;->types:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 107
    invoke-static {v2, v4, p0}, Lcom/squareup/print/sections/CodesSection;->getTaxIdForType(Lcom/squareup/server/account/protos/TaxId;Ljava/lang/String;Lcom/squareup/server/account/FeeTypes;)Lcom/squareup/server/account/protos/TaxId;

    move-result-object v5

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_c

    .line 155
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_5

    .line 157
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/CodesSection;

    .line 159
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->ticketName:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/print/sections/CodesSection;->ticketName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/print/sections/CodesSection;->ticketName:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 162
    :cond_3
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v3, p1, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_4
    iget-object v2, p1, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    if-eqz v2, :cond_5

    :goto_1
    return v1

    .line 166
    :cond_5
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->authorizationCode:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v3, p1, Lcom/squareup/print/sections/CodesSection;->authorizationCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    goto :goto_2

    :cond_6
    iget-object v2, p1, Lcom/squareup/print/sections/CodesSection;->authorizationCode:Ljava/lang/String;

    if-eqz v2, :cond_7

    :goto_2
    return v1

    .line 170
    :cond_7
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->sequentialTenderNumber:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v3, p1, Lcom/squareup/print/sections/CodesSection;->sequentialTenderNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    goto :goto_3

    :cond_8
    iget-object v2, p1, Lcom/squareup/print/sections/CodesSection;->sequentialTenderNumber:Ljava/lang/String;

    if-eqz v2, :cond_9

    :goto_3
    return v1

    .line 174
    :cond_9
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->taxIds:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/print/sections/CodesSection;->taxIds:Ljava/util/List;

    if-eqz v2, :cond_a

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_b

    goto :goto_4

    :cond_a
    if-eqz p1, :cond_b

    :goto_4
    return v1

    :cond_b
    return v0

    :cond_c
    :goto_5
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 180
    iget-object v0, p0, Lcom/squareup/print/sections/CodesSection;->ticketName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 181
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->receiptNumber:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 182
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->authorizationCode:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 183
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->sequentialTenderNumber:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    .line 184
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->taxIds:Ljava/util/List;

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 3

    .line 189
    invoke-direct {p0}, Lcom/squareup/print/sections/CodesSection;->getNonBlankCodesAsList()Ljava/util/List;

    move-result-object v0

    .line 190
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/print/sections/CodesSection;->taxIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 194
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 196
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 197
    invoke-static {v1, v0}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlankList(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 199
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 200
    iget-object v2, p0, Lcom/squareup/print/sections/CodesSection;->taxIds:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlankList(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 202
    invoke-virtual {p1, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsOrCollapse(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 204
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
