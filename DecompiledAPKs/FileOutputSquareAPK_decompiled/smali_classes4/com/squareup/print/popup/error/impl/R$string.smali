.class public final Lcom/squareup/print/popup/error/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/popup/error/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final kitchen_printing_confirm_discard_print_jobs_cancel:I = 0x7f120e8b

.field public static final kitchen_printing_confirm_discard_print_jobs_confirm:I = 0x7f120e8c

.field public static final kitchen_printing_confirm_discard_print_jobs_message:I = 0x7f120e8d

.field public static final kitchen_printing_confirm_discard_print_jobs_title:I = 0x7f120e8e

.field public static final kitchen_printing_discarded_prints_many:I = 0x7f120e8f

.field public static final kitchen_printing_discarded_prints_one:I = 0x7f120e90

.field public static final kitchen_printing_error_affected_prints:I = 0x7f120e91

.field public static final kitchen_printing_error_cover_open_title:I = 0x7f120e92

.field public static final kitchen_printing_error_cutter_body:I = 0x7f120e93

.field public static final kitchen_printing_error_cutter_title:I = 0x7f120e94

.field public static final kitchen_printing_error_issue_body_network:I = 0x7f120e95

.field public static final kitchen_printing_error_issue_body_reboot:I = 0x7f120e96

.field public static final kitchen_printing_error_issue_body_support_center:I = 0x7f120e97

.field public static final kitchen_printing_error_issue_title:I = 0x7f120e98

.field public static final kitchen_printing_error_jam_body:I = 0x7f120e99

.field public static final kitchen_printing_error_jam_title:I = 0x7f120e9a

.field public static final kitchen_printing_error_mechanical_body:I = 0x7f120e9b

.field public static final kitchen_printing_error_mechanical_title:I = 0x7f120e9c

.field public static final kitchen_printing_error_out_of_paper_body:I = 0x7f120e9d

.field public static final kitchen_printing_error_out_of_paper_title:I = 0x7f120e9e

.field public static final kitchen_printing_error_printer_jobs_phone:I = 0x7f120e9f

.field public static final kitchen_printing_error_printer_jobs_tablet:I = 0x7f120ea0

.field public static final kitchen_printing_error_printer_stations_jobs_phone:I = 0x7f120ea1

.field public static final kitchen_printing_error_printer_stations_jobs_tablet:I = 0x7f120ea2

.field public static final kitchen_printing_error_reprint_failed:I = 0x7f120ea3

.field public static final kitchen_printing_error_title:I = 0x7f120ea4

.field public static final kitchen_printing_error_title_message:I = 0x7f120ea5

.field public static final kitchen_printing_error_unavailable_title:I = 0x7f120ea6

.field public static final kitchen_printing_printer_errors:I = 0x7f120eaa

.field public static final kitchen_printing_reprint:I = 0x7f120eab

.field public static final kitchen_printing_try_again:I = 0x7f120eac

.field public static final printer_troubleshooting:I = 0x7f1214e3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
