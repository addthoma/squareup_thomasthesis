.class public Lcom/squareup/print/HardwarePrinter$HardwareInfo;
.super Ljava/lang/Object;
.source "HardwarePrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/HardwarePrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HardwareInfo"
.end annotation


# static fields
.field static final NO_PRINTER:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

.field static final UNKNOWN_PRINTER:Ljava/lang/String; = "Unknown Printer"


# instance fields
.field public final connectionType:Lcom/squareup/print/ConnectionType;

.field public final manufacturer:Ljava/lang/String;

.field public final model:Ljava/lang/String;

.field public final supportsRasterMode:Z

.field public final supportsTextMode:Z

.field public final uniqueAttribute:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 51
    new-instance v7, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;)V

    sput-object v7, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->NO_PRINTER:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/print/ConnectionType;ZZLjava/lang/String;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    .line 74
    iput-boolean p4, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    .line 75
    iput-boolean p5, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsTextMode:Z

    .line 76
    iput-object p6, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method connectionTypeName()Ljava/lang/String;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/print/ConnectionType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 159
    :cond_0
    instance-of v1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 160
    :cond_1
    check-cast p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 161
    iget-boolean v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    iget-boolean v3, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsTextMode:Z

    iget-boolean v3, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsTextMode:Z

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    .line 163
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    .line 164
    invoke-static {v1, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    iget-object v3, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    .line 166
    invoke-static {v1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAnalyticsName()Ljava/lang/String;
    .locals 3

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayableModelName()Ljava/lang/String;
    .locals 4

    .line 86
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getShortSuffix()Ljava/lang/String;

    move-result-object v0

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 89
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public getId()Ljava/lang/String;
    .locals 3

    .line 99
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasAllRequiredFields()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Unknown Printer"

    return-object v0

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasUniqueAttribute()Z

    move-result v0

    if-nez v0, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getNonUniqueId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 104
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNonUniqueId()Ljava/lang/String;
    .locals 3

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionTypeName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getShortSuffix()Ljava/lang/String;
    .locals 4

    .line 117
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasUniqueAttribute()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_1

    const-string v1, ":"

    const-string v3, ""

    .line 121
    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v2

    if-gez v1, :cond_0

    const/4 v1, 0x0

    .line 126
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method hasAllRequiredFields()Z
    .locals 3

    .line 139
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 142
    :goto_0
    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v2, Lcom/squareup/print/ConnectionType;->USB:Lcom/squareup/print/ConnectionType;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v2, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v2, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    if-eq v1, v2, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->hasUniqueAttribute()Z

    move-result v1

    and-int/2addr v0, v1

    :cond_1
    return v0
.end method

.method public hasUniqueAttribute()Z
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 170
    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsTextMode:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->uniqueAttribute:Ljava/lang/String;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method isSupported()Z
    .locals 1

    .line 150
    iget-boolean v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsRasterMode:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->supportsTextMode:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
