.class public final enum Lcom/squareup/print/util/RasterDocument$RasTopMargin;
.super Ljava/lang/Enum;
.source "RasterDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/util/RasterDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RasTopMargin"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/util/RasterDocument$RasTopMargin;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/util/RasterDocument$RasTopMargin;

.field public static final enum DEFAULT:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

.field public static final enum SMALL:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

.field public static final enum STANDARD:Lcom/squareup/print/util/RasterDocument$RasTopMargin;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 12
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    const/4 v1, 0x0

    const-string v2, "DEFAULT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/print/util/RasterDocument$RasTopMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    .line 13
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    const/4 v2, 0x1

    const-string v3, "SMALL"

    invoke-direct {v0, v3, v2}, Lcom/squareup/print/util/RasterDocument$RasTopMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->SMALL:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    .line 14
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    const/4 v3, 0x2

    const-string v4, "STANDARD"

    invoke-direct {v0, v4, v3}, Lcom/squareup/print/util/RasterDocument$RasTopMargin;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->STANDARD:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    .line 11
    sget-object v4, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->SMALL:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->STANDARD:Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->$VALUES:[Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/util/RasterDocument$RasTopMargin;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/util/RasterDocument$RasTopMargin;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasTopMargin;->$VALUES:[Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    invoke-virtual {v0}, [Lcom/squareup/print/util/RasterDocument$RasTopMargin;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/util/RasterDocument$RasTopMargin;

    return-object v0
.end method
