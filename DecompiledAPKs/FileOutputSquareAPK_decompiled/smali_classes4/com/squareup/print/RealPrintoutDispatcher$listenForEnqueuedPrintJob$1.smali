.class final Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintoutDispatcher;->listenForEnqueuedPrintJob()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/print/PrintJob;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/print/RealPrintoutDispatcher;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealPrintoutDispatcher;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/print/PrintJob;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 159
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1$printJobStatusListener$1;

    invoke-direct {v0, p1}, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1$printJobStatusListener$1;-><init>(Lio/reactivex/SingleEmitter;)V

    .line 167
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1;Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1$printJobStatusListener$1;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/SingleEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 168
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {p1}, Lcom/squareup/print/RealPrintoutDispatcher;->access$getPrintSpooler$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/PrintSpooler;

    move-result-object p1

    check-cast v0, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;

    invoke-virtual {p1, v0}, Lcom/squareup/print/PrintSpooler;->addPrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V

    return-void
.end method
