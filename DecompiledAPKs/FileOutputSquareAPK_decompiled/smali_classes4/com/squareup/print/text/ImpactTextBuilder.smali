.class public Lcom/squareup/print/text/ImpactTextBuilder;
.super Ljava/lang/Object;
.source "ImpactTextBuilder.java"


# static fields
.field private static final DIVIDER_CHAR:C = '-'

.field private static final MINIMUM_COLUMN_PADDING:I = 0x1

.field private static final SPACE_CHAR:C = ' '

.field private static final STAR_SP_LINE_HEIGHT_INCHES_VERTICAL:F = 0.16666667f

.field private static final TICKET_ITEM_COLUMN_PADDING:I = 0x1

.field private static final TICKET_ITEM_MARGIN:I = 0x1

.field private static final TICKET_TOP_PADDING_LINES:I = 0x5


# instance fields
.field private defaultStyle:Lcom/squareup/print/text/Style;

.field private final maxLineLength:I

.field private final renderedRowsBuilder:Lcom/squareup/print/text/RenderedRowsBuilder;


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    .line 54
    new-instance p1, Lcom/squareup/print/text/RenderedRowsBuilder;

    invoke-direct {p1}, Lcom/squareup/print/text/RenderedRowsBuilder;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/text/ImpactTextBuilder;->renderedRowsBuilder:Lcom/squareup/print/text/RenderedRowsBuilder;

    return-void
.end method

.method private appendBoldRedText(Ljava/lang/String;)V
    .locals 2

    .line 269
    sget-object v0, Lcom/squareup/print/text/Style;->BOLD:Lcom/squareup/print/text/Style;

    sget-object v1, Lcom/squareup/print/text/Style;->RED:Lcom/squareup/print/text/Style;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method private appendBoldText(Ljava/lang/String;)V
    .locals 1

    .line 265
    sget-object v0, Lcom/squareup/print/text/Style;->BOLD:Lcom/squareup/print/text/Style;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method private appendControlSequence(Ljava/lang/String;)V
    .locals 1

    .line 289
    iget-object v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->renderedRowsBuilder:Lcom/squareup/print/text/RenderedRowsBuilder;

    invoke-virtual {v0, p1}, Lcom/squareup/print/text/RenderedRowsBuilder;->addRawChars(Ljava/lang/String;)Z

    return-void
.end method

.method private appendText(Ljava/lang/String;)V
    .locals 1

    .line 273
    const-class v0, Lcom/squareup/print/text/Style;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;Ljava/util/Set;)V

    return-void
.end method

.method private appendText(Ljava/lang/String;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/text/Style;",
            ">;)V"
        }
    .end annotation

    .line 277
    iget-object v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->defaultStyle:Lcom/squareup/print/text/Style;

    if-eqz v0, :cond_0

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->defaultStyle:Lcom/squareup/print/text/Style;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->renderedRowsBuilder:Lcom/squareup/print/text/RenderedRowsBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/print/text/RenderedRowsBuilder;->addStyledText(Ljava/lang/String;Ljava/util/Set;)Z

    return-void
.end method

.method private fullWidthWrappedText(Ljava/lang/String;Landroid/text/Layout$Alignment;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/text/Layout$Alignment;",
            "Ljava/util/Set<",
            "Lcom/squareup/print/text/Style;",
            ">;)V"
        }
    .end annotation

    .line 222
    iget v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    invoke-static {v0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->wrapToLines(ILjava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 224
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 225
    iget v1, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v1, v2

    if-lez v1, :cond_1

    .line 227
    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    if-ne p2, v2, :cond_0

    .line 228
    invoke-direct {p0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->padding(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    goto :goto_1

    .line 229
    :cond_0
    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    if-ne p2, v2, :cond_1

    .line 230
    div-int/lit8 v1, v1, 0x2

    invoke-direct {p0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->padding(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 233
    :cond_1
    :goto_1
    invoke-direct {p0, v0, p3}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;Ljava/util/Set;)V

    .line 234
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static linesToInches(I)F
    .locals 1

    int-to-float p0, p0

    const v0, 0x3e2aaaab

    mul-float p0, p0, v0

    return p0
.end method

.method private padLeft(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .line 247
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, p2, :cond_0

    return-object p1

    .line 251
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->padding(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private padding(I)Ljava/lang/String;
    .locals 1

    const/16 v0, 0x20

    .line 255
    invoke-direct {p0, v0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->repeating(CI)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private repeating(CI)Ljava/lang/String;
    .locals 0

    .line 259
    new-array p2, p2, [C

    .line 260
    invoke-static {p2, p1}, Ljava/util/Arrays;->fill([CC)V

    .line 261
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/String;-><init>([C)V

    return-object p1
.end method

.method private twoColumnsWithCenterPadding(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 240
    invoke-direct {p0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;)V

    .line 241
    invoke-direct {p0, p3}, Lcom/squareup/print/text/ImpactTextBuilder;->padding(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 242
    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    return-void
.end method

.method private static wrapSingleLineToLines(ILjava/lang/String;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    if-lt p0, v0, :cond_9

    .line 318
    new-instance v1, Ljava/util/ArrayList;

    const-string v2, " "

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 319
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 320
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 322
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x0

    .line 323
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 327
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, p0, :cond_2

    .line 329
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "(?<=\\G.{"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v8, "})"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 330
    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    .line 332
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    goto :goto_1

    .line 334
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Infinite loop processing word \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' from input \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' with maxLineLength = "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 339
    :cond_1
    :goto_1
    invoke-virtual {v1, v5, v7}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    goto :goto_0

    .line 341
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-nez v8, :cond_3

    goto :goto_2

    :cond_3
    const/4 v5, 0x1

    :goto_2
    add-int/2addr v7, v5

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/2addr v7, v5

    if-gt v7, p0, :cond_5

    .line 343
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_4

    .line 344
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_4
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 349
    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_6

    .line 351
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 359
    :cond_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result p0

    if-lez p0, :cond_8

    .line 360
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    return-object v3

    .line 315
    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "maxLineLength must be at least 1: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static wrapToLines(ILjava/lang/String;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "\n"

    .line 297
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 298
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 302
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 303
    invoke-static {p0, v3}, Lcom/squareup/print/text/ImpactTextBuilder;->wrapSingleLineToLines(ILjava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public appendNewline()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->renderedRowsBuilder:Lcom/squareup/print/text/RenderedRowsBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/RenderedRowsBuilder;->addNewLine()Z

    return-void
.end method

.method public centeredBlock(Ljava/lang/String;)V
    .locals 2

    .line 191
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    sget-object v1, Lcom/squareup/print/text/Style;->BOLD:Lcom/squareup/print/text/Style;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthWrappedText(Ljava/lang/String;Landroid/text/Layout$Alignment;Ljava/util/Set;)V

    return-void
.end method

.method public diningOptionHeader(Ljava/lang/String;)V
    .locals 2

    .line 203
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 204
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    sget-object v1, Lcom/squareup/print/text/Style;->BOLD:Lcom/squareup/print/text/Style;

    invoke-static {v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthWrappedText(Ljava/lang/String;Landroid/text/Layout$Alignment;Ljava/util/Set;)V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    return-void
.end method

.method public divider()V
    .locals 2

    .line 209
    iget v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    const/16 v1, 0x2d

    invoke-direct {p0, v1, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->repeating(CI)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    return-void
.end method

.method public fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V
    .locals 2

    .line 195
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const-class v1, Lcom/squareup/print/text/Style;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthWrappedText(Ljava/lang/String;Landroid/text/Layout$Alignment;Ljava/util/Set;)V

    return-void
.end method

.method public fullWidthRightAlignedWrappedText(Ljava/lang/String;)V
    .locals 2

    .line 199
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    const-class v1, Lcom/squareup/print/text/Style;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthWrappedText(Ljava/lang/String;Landroid/text/Layout$Alignment;Ljava/util/Set;)V

    return-void
.end method

.method public render()Lcom/squareup/print/text/RenderedRows;
    .locals 1

    .line 218
    iget-object v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->renderedRowsBuilder:Lcom/squareup/print/text/RenderedRowsBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/RenderedRowsBuilder;->getRenderedRows()Lcom/squareup/print/text/RenderedRows;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultStyle(Lcom/squareup/print/text/Style;)V
    .locals 0

    .line 58
    iput-object p1, p0, Lcom/squareup/print/text/ImpactTextBuilder;->defaultStyle:Lcom/squareup/print/text/Style;

    return-void
.end method

.method public spacing()V
    .locals 0

    .line 214
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    return-void
.end method

.method public ticketQuantityAndItem(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "quantity"

    .line 115
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    const-string v0, "timesSign"

    .line 116
    invoke-static {p4, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    const-string v0, "name"

    .line 117
    invoke-static {p5, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 118
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-gt v0, p2, :cond_5

    add-int/2addr p2, v2

    .line 125
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr p2, v0

    .line 126
    iget v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    sub-int/2addr v0, p2

    sub-int/2addr v0, v2

    sub-int/2addr v0, v2

    add-int/lit8 v3, p2, 0x1

    .line 128
    invoke-direct {p0, v3}, Lcom/squareup/print/text/ImpactTextBuilder;->padding(I)Ljava/lang/String;

    move-result-object v3

    .line 131
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->padLeft(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    .line 132
    invoke-direct {p0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendBoldText(Ljava/lang/String;)V

    .line 135
    invoke-direct {p0, v2}, Lcom/squareup/print/text/ImpactTextBuilder;->padding(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 137
    invoke-static {v0, p5}, Lcom/squareup/print/text/ImpactTextBuilder;->wrapToLines(ILjava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 140
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->appendBoldText(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    .line 144
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    invoke-interface {p1, v2, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 145
    invoke-direct {p0, v3}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 146
    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->appendBoldText(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 155
    invoke-static {p3}, Lcom/squareup/util/Strings;->replaceNBSPWithSimpleSpace(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 154
    invoke-static {v0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->wrapToLines(ILjava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 156
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 157
    invoke-direct {p0, v3}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->appendBoldText(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    goto :goto_1

    :cond_1
    if-eqz p6, :cond_3

    .line 166
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 167
    invoke-interface {p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    .line 168
    invoke-static {v0, p3}, Lcom/squareup/print/text/ImpactTextBuilder;->wrapToLines(ILjava/lang/String;)Ljava/util/List;

    move-result-object p3

    invoke-interface {p1, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 171
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 172
    invoke-direct {p0, v3}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 173
    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->appendBoldRedText(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    goto :goto_3

    .line 178
    :cond_3
    invoke-static {p7}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 181
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "- "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 182
    invoke-static {v0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->wrapToLines(ILjava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 183
    invoke-direct {p0, v3}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    .line 184
    invoke-direct {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->appendText(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    goto :goto_4

    :cond_4
    return-void

    .line 119
    :cond_5
    new-instance p3, Ljava/lang/IllegalArgumentException;

    sget-object p4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 p5, 0x2

    new-array p5, p5, [Ljava/lang/Object;

    aput-object p1, p5, v1

    .line 121
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, p5, v2

    const-string p1, "Quantity string cannot be longer than quantityWidth: len(%s) > %d"

    .line 120
    invoke-static {p4, p1, p5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3
.end method

.method public ticketTopPadding()V
    .locals 2

    const/16 v0, 0xa

    const/4 v1, 0x5

    .line 66
    invoke-direct {p0, v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->repeating(CI)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendControlSequence(Ljava/lang/String;)V

    return-void
.end method

.method public twoColumnsLeftExpandingText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 87
    iget v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 89
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gt v1, v0, :cond_0

    .line 91
    iget v0, p0, Lcom/squareup/print/text/ImpactTextBuilder;->maxLineLength:I

    .line 92
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 91
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->twoColumnsWithCenterPadding(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0, p2}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
