.class public final Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;
.super Ljava/lang/Object;
.source "StarMicronicsRenderRowsUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0008\u001a\u00020\u0004H\u0002J&\u0010\t\u001a\u00020\n*\u00060\u000bj\u0002`\u000c2\u0006\u0010\u0008\u001a\u00020\u00042\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;",
        "",
        "()V",
        "mapRenderedRowsToString",
        "",
        "renderedRows",
        "Lcom/squareup/print/text/RenderedRows;",
        "sanitizeText",
        "text",
        "appendStyledText",
        "",
        "Ljava/lang/StringBuilder;",
        "Lkotlin/text/StringBuilder;",
        "styles",
        "",
        "Lcom/squareup/print/text/Style;",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;

    invoke-direct {v0}, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;-><init>()V

    sput-object v0, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;->INSTANCE:Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final appendStyledText(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/print/text/Style;",
            ">;)V"
        }
    .end annotation

    .line 46
    check-cast p3, Ljava/lang/Iterable;

    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p3

    .line 47
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/text/Style;

    .line 48
    sget-object v4, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/print/text/Style;->ordinal()I

    move-result v1

    aget v1, v4, v1

    if-eq v1, v3, :cond_1

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v1, "\u001b4"

    .line 50
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string v1, "\u001bE"

    .line 49
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 54
    :cond_2
    invoke-direct {p0, p2}, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;->sanitizeText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    check-cast p3, Ljava/lang/Iterable;

    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->reversed(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/print/text/Style;

    .line 57
    sget-object v0, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p3}, Lcom/squareup/print/text/Style;->ordinal()I

    move-result p3

    aget p3, v0, p3

    if-eq p3, v3, :cond_4

    if-eq p3, v2, :cond_3

    goto :goto_1

    :cond_3
    const-string p3, "\u001b5"

    .line 59
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string p3, "\u001bF"

    .line 58
    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    return-void
.end method

.method private final sanitizeText(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string/jumbo v1, "\u00d7"

    const-string/jumbo v2, "x"

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    .line 66
    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "?"

    .line 67
    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->sanitizeTextToAsciiLetters(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final mapRenderedRowsToString(Lcom/squareup/print/text/RenderedRows;)Ljava/lang/String;
    .locals 3

    const-string v0, "renderedRows"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u001bW1"

    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/print/text/RenderedRows;->getRows()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/text/Row;

    .line 28
    instance-of v2, v1, Lcom/squareup/print/text/Row$StyledRow;

    if-eqz v2, :cond_1

    check-cast v1, Lcom/squareup/print/text/Row$StyledRow;

    invoke-virtual {v1}, Lcom/squareup/print/text/Row$StyledRow;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/print/text/Row$StyledRow;->getStyles()Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v2, v1}, Lcom/squareup/print/text/StarMicronicsRenderRowsUtil;->appendStyledText(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/util/Set;)V

    goto :goto_0

    .line 29
    :cond_1
    instance-of v2, v1, Lcom/squareup/print/text/Row$RawChars;

    if-eqz v2, :cond_2

    check-cast v1, Lcom/squareup/print/text/Row$RawChars;

    invoke-virtual {v1}, Lcom/squareup/print/text/Row$RawChars;->getChars()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "sb.append(row.chars)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 30
    :cond_2
    instance-of v1, v1, Lcom/squareup/print/text/Row$NewLine;

    if-eqz v1, :cond_0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "sb.append(\"\\n\")"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string p1, "\u001bW0"

    .line 36
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\u001bd\u0002"

    .line 37
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "sb.toString()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
