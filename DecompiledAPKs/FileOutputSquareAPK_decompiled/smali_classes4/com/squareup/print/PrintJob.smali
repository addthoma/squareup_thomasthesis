.class public final Lcom/squareup/print/PrintJob;
.super Ljava/lang/Object;
.source "PrintJob.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintJob$PrintAttempt;
    }
.end annotation


# static fields
.field private static final INITIAL_PRINT_ATTEMPTS_COUNT:I


# instance fields
.field public final forTestPrint:Z

.field private hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

.field private final jobUuid:Ljava/lang/String;

.field private latestPrintAttempt:Lcom/squareup/print/PrintJob$PrintAttempt;

.field private printAttemptsCount:I

.field private final printTargetId:Ljava/lang/String;

.field final printTargetName:Ljava/lang/String;

.field private printTimingData:Lcom/squareup/print/PrintTimingData;

.field private final printablePayload:Lcom/squareup/print/PrintablePayload;

.field final sourceIdentifier:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/print/PrintTarget;Lcom/squareup/print/PrintablePayload;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 140
    iput v0, p0, Lcom/squareup/print/PrintJob;->printAttemptsCount:I

    .line 141
    invoke-static {}, Lcom/squareup/print/PrintJob$PrintAttempt;->access$000()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/print/PrintJob;->latestPrintAttempt:Lcom/squareup/print/PrintJob$PrintAttempt;

    .line 146
    new-instance v0, Lcom/squareup/print/PrintTimingData;

    invoke-direct {v0}, Lcom/squareup/print/PrintTimingData;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrintJob;->printTimingData:Lcom/squareup/print/PrintTimingData;

    const/4 v0, 0x0

    .line 148
    iput-object v0, p0, Lcom/squareup/print/PrintJob;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    const-string v0, "target"

    .line 152
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "printablePayload"

    .line 153
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "title"

    .line 154
    invoke-static {p3, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 155
    invoke-interface {p1}, Lcom/squareup/print/PrintTarget;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/print/PrintJob;->printTargetId:Ljava/lang/String;

    .line 156
    invoke-interface {p1}, Lcom/squareup/print/PrintTarget;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintJob;->printTargetName:Ljava/lang/String;

    .line 157
    iput-object p2, p0, Lcom/squareup/print/PrintJob;->printablePayload:Lcom/squareup/print/PrintablePayload;

    .line 158
    iput-object p3, p0, Lcom/squareup/print/PrintJob;->title:Ljava/lang/String;

    .line 159
    iput-boolean p4, p0, Lcom/squareup/print/PrintJob;->forTestPrint:Z

    .line 160
    iput-object p5, p0, Lcom/squareup/print/PrintJob;->sourceIdentifier:Ljava/lang/String;

    .line 161
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    .line 247
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 248
    :cond_1
    check-cast p1, Lcom/squareup/print/PrintJob;

    .line 249
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method generateStatusDebugText()Ljava/lang/String;
    .locals 4

    .line 231
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->printablePayload:Lcom/squareup/print/PrintablePayload;

    .line 233
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->title:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->printTargetId:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    iget v2, p0, Lcom/squareup/print/PrintJob;->printAttemptsCount:I

    .line 234
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->latestPrintAttempt:Lcom/squareup/print/PrintJob$PrintAttempt;

    iget-object v2, v2, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    const/4 v3, 0x5

    aput-object v2, v1, v3

    const-string v2, "PrintJob %s \u201c%s\u201d %s for target %s. | Attempt count: %d | Latest result: %s"

    .line 231
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getJobId()Ljava/lang/String;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    return-object v0
.end method

.method public getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->latestPrintAttempt:Lcom/squareup/print/PrintJob$PrintAttempt;

    return-object v0
.end method

.method getPayload()Lcom/squareup/print/PrintablePayload;
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->printablePayload:Lcom/squareup/print/PrintablePayload;

    return-object v0
.end method

.method public getPrintAttemptsCount()I
    .locals 1

    .line 173
    iget v0, p0, Lcom/squareup/print/PrintJob;->printAttemptsCount:I

    return v0
.end method

.method public getPrintTimingData()Lcom/squareup/print/PrintTimingData;
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->printTimingData:Lcom/squareup/print/PrintTimingData;

    return-object v0
.end method

.method public getSourceIdentifier()Ljava/lang/String;
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->sourceIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetId()Ljava/lang/String;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->printTargetId:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->title:Ljava/lang/String;

    return-object v0
.end method

.method hardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method incrementPrintAttempts()I
    .locals 1

    .line 221
    iget v0, p0, Lcom/squareup/print/PrintJob;->printAttemptsCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/print/PrintJob;->printAttemptsCount:I

    .line 222
    iget v0, p0, Lcom/squareup/print/PrintJob;->printAttemptsCount:I

    return v0
.end method

.method isReprint()Z
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/print/PrintJob;->latestPrintAttempt:Lcom/squareup/print/PrintJob$PrintAttempt;

    iget-object v0, v0, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v1, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public resetPrintTimingData()V
    .locals 1

    .line 213
    new-instance v0, Lcom/squareup/print/PrintTimingData;

    invoke-direct {v0}, Lcom/squareup/print/PrintTimingData;-><init>()V

    iput-object v0, p0, Lcom/squareup/print/PrintJob;->printTimingData:Lcom/squareup/print/PrintTimingData;

    return-void
.end method

.method setHardwareInfo(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)V
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/print/PrintJob;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    return-void
.end method

.method public setPrintAttempt(Lcom/squareup/print/PrintJob$PrintAttempt;)V
    .locals 1

    const-string v0, "latestPrintAttempt"

    .line 205
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/print/PrintJob$PrintAttempt;

    iput-object p1, p0, Lcom/squareup/print/PrintJob;->latestPrintAttempt:Lcom/squareup/print/PrintJob$PrintAttempt;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PrintJob{title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/PrintJob;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", jobUuid=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->jobUuid:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", printTargetId=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/PrintJob;->printTargetId:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
