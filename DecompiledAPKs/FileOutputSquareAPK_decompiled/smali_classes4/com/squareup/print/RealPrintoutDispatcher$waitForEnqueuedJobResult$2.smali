.class final Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$2;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintoutDispatcher;->waitForEnqueuedJobResult()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/print/PrintJob;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/print/RealPrintoutDispatcher;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealPrintoutDispatcher;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$2;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/print/PrintJob;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintJob;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$2;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {v0, p1}, Lcom/squareup/print/RealPrintoutDispatcher;->access$getPrinter(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/PrintJob;)Lcom/squareup/print/HardwarePrinter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter;->resetPrinterAndGetStatus()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/print/PrintJob;

    invoke-virtual {p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$2;->apply(Lcom/squareup/print/PrintJob;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
