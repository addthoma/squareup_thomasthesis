.class public final Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;
.super Ljava/lang/Object;
.source "EpsonPrinterConnection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/printer/epson/EpsonPrinterConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0016\n\u0002\u0008\n\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\t\u0010\u0002\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;",
        "",
        "()V",
        "ALREADY_CONNECTED",
        "",
        "CLEANUP_TIMEOUT_MS",
        "",
        "CONNECT_RETRY_DELAYS_MS",
        "",
        "CONNECT_RETRY_DELAYS_MS$annotations",
        "getCONNECT_RETRY_DELAYS_MS",
        "()[J",
        "CONNECT_TIMEOUT_MS",
        "EPSON_SDK_TIMEOUT_EXCEPTION",
        "NO_CALLBACK_CODE",
        "PENDING_DISCONNECT_PRINTER_DELAY_MS",
        "PRINT_RESULT_TIMEOUT_MS",
        "SEND_DATA_TIMEOUT_MS",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 248
    invoke-direct {p0}, Lcom/squareup/printer/epson/EpsonPrinterConnection$Companion;-><init>()V

    return-void
.end method

.method public static synthetic CONNECT_RETRY_DELAYS_MS$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getCONNECT_RETRY_DELAYS_MS()[J
    .locals 1

    .line 303
    invoke-static {}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->access$getCONNECT_RETRY_DELAYS_MS$cp()[J

    move-result-object v0

    return-object v0
.end method
