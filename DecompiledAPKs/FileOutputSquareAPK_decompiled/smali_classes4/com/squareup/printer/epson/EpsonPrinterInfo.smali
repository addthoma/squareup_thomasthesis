.class public final Lcom/squareup/printer/epson/EpsonPrinterInfo;
.super Ljava/lang/Object;
.source "EpsonPrinterInfo.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0008H\u00c6\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003JI\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\t\u0010!\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000fR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000f\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterInfo;",
        "",
        "target",
        "",
        "deviceName",
        "epsonPrinterAttributes",
        "Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
        "connectionType",
        "Lcom/squareup/print/ConnectionType;",
        "macAddress",
        "serialNumber",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;)V",
        "getConnectionType",
        "()Lcom/squareup/print/ConnectionType;",
        "getDeviceName",
        "()Ljava/lang/String;",
        "getEpsonPrinterAttributes",
        "()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;",
        "getMacAddress",
        "getSerialNumber",
        "getTarget",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectionType:Lcom/squareup/print/ConnectionType;

.field private final deviceName:Ljava/lang/String;

.field private final epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

.field private final macAddress:Ljava/lang/String;

.field private final serialNumber:Ljava/lang/String;

.field private final target:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "target"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterAttributes"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionType"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    iput-object p5, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    and-int/lit8 v0, p7, 0x10

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 11
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v7, v0

    goto :goto_0

    :cond_0
    move-object v7, p5

    :goto_0
    and-int/lit8 v0, p7, 0x20

    if-eqz v0, :cond_1

    .line 12
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v8, v0

    goto :goto_1

    :cond_1
    move-object v8, p6

    :goto_1
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v2 .. v8}, Lcom/squareup/printer/epson/EpsonPrinterInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/printer/epson/EpsonPrinterInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/printer/epson/EpsonPrinterInfo;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/printer/epson/EpsonPrinterInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    return-object v0
.end method

.method public final component4()Lcom/squareup/print/ConnectionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/printer/epson/EpsonPrinterInfo;
    .locals 8

    const-string v0, "target"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "epsonPrinterAttributes"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectionType"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/printer/epson/EpsonPrinterInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConnectionType()Lcom/squareup/print/ConnectionType;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    return-object v0
.end method

.method public final getDeviceName()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public final getEpsonPrinterAttributes()Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    return-object v0
.end method

.method public final getMacAddress()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getSerialNumber()Ljava/lang/String;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getTarget()Ljava/lang/String;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EpsonPrinterInfo(target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->target:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", deviceName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", epsonPrinterAttributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->epsonPrinterAttributes:Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", connectionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", macAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->macAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", serialNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterInfo;->serialNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
