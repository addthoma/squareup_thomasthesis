.class public abstract Lcom/squareup/printer/epson/EpsonPrinterModule;
.super Ljava/lang/Object;
.source "EpsonPrinterModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H!\u00a2\u0006\u0002\u0008\u0011\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonPrinterModule;",
        "",
        "()V",
        "bindEpsonDiscoverer",
        "Lcom/squareup/printer/epson/EpsonDiscoverer;",
        "epsonDiscoverer",
        "Lcom/squareup/printer/epson/RealEpsonDiscoverer;",
        "bindEpsonDiscoverer$impl_wiring_release",
        "bindEpsonPrinterScouts",
        "Lcom/squareup/print/EpsonPrinterScouts;",
        "epsonPrinterScouts",
        "Lcom/squareup/printer/epson/RealEpsonPrinterScouts;",
        "bindEpsonPrinterScouts$impl_wiring_release",
        "bindEpsonPrinterSdkFactory",
        "Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;",
        "epsonPrinterSdkFactory",
        "Lcom/squareup/printer/epson/RealEpsonPrinterSdkFactory;",
        "bindEpsonPrinterSdkFactory$impl_wiring_release",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/printer/epson/EpsonPrinterModule;->Companion:Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideEpsonPrinterConnection(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)Lcom/squareup/printer/epson/EpsonPrinterConnection;
    .locals 1
    .param p1    # Lkotlinx/coroutines/CoroutineScope;
        .annotation runtime Lcom/squareup/print/PrinterThread;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinterModule;->Companion:Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;->provideEpsonPrinterConnection(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)Lcom/squareup/printer/epson/EpsonPrinterConnection;

    move-result-object p0

    return-object p0
.end method

.method public static final provideEpsonTcpPrinterScout(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonTcpPrinterScout;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/LoggedInScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinterModule;->Companion:Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;->provideEpsonTcpPrinterScout(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/printer/epson/EpsonDiscoverer;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonTcpPrinterScout;

    move-result-object p0

    return-object p0
.end method

.method public static final provideEpsonUsbPrinterScout(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonUsbPrinterScout;
    .locals 1
    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/LoggedInScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinterModule;->Companion:Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;->provideEpsonUsbPrinterScout(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonUsbPrinterScout;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindEpsonDiscoverer$impl_wiring_release(Lcom/squareup/printer/epson/RealEpsonDiscoverer;)Lcom/squareup/printer/epson/EpsonDiscoverer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindEpsonPrinterScouts$impl_wiring_release(Lcom/squareup/printer/epson/RealEpsonPrinterScouts;)Lcom/squareup/print/EpsonPrinterScouts;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindEpsonPrinterSdkFactory$impl_wiring_release(Lcom/squareup/printer/epson/RealEpsonPrinterSdkFactory;)Lcom/squareup/printer/epson/EpsonPrinterSdkFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
