.class final Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;
.super Ljava/lang/Object;
.source "RealEpsonDiscoverer.kt"

# interfaces
.implements Lcom/epson/epos2/discovery/DiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/RealEpsonDiscoverer;->startEpsonDiscovery(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "deviceInfo",
        "Lcom/epson/epos2/discovery/DeviceInfo;",
        "kotlin.jvm.PlatformType",
        "onDiscovery"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $discoveredPrinters:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/printer/epson/RealEpsonDiscoverer;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;->this$0:Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    iput-object p2, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;->$discoveredPrinters:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDiscovery(Lcom/epson/epos2/discovery/DeviceInfo;)V
    .locals 10

    .line 34
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinters;->INSTANCE:Lcom/squareup/printer/epson/EpsonPrinters;

    invoke-virtual {v0}, Lcom/squareup/printer/epson/EpsonPrinters;->getEpsonTcpDeviceNameMapping()Ljava/util/Map;

    move-result-object v0

    const-string v1, "deviceInfo"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;

    if-nez v4, :cond_0

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported epson printer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 40
    :cond_0
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterInfo;

    .line 41
    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;->this$0:Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    invoke-static {v1, p1}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->access$getTarget(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Lcom/epson/epos2/discovery/DeviceInfo;)Ljava/lang/String;

    move-result-object v2

    .line 42
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    const-string v1, "deviceInfo.deviceName"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;->this$0:Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getTarget()Ljava/lang/String;

    move-result-object v5

    const-string v6, "deviceInfo.target"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v5}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->access$getConnectionType(Lcom/squareup/printer/epson/RealEpsonDiscoverer;Ljava/lang/String;)Lcom/squareup/print/ConnectionType;

    move-result-object v5

    .line 45
    invoke-virtual {p1}, Lcom/epson/epos2/discovery/DeviceInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, v0

    .line 40
    invoke-direct/range {v1 .. v9}, Lcom/squareup/printer/epson/EpsonPrinterInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/printer/epson/EpsonPrinters$EpsonPrinterAttributes;Lcom/squareup/print/ConnectionType;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 47
    iget-object p1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;->$discoveredPrinters:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/printer/epson/RealEpsonDiscoverer$startEpsonDiscovery$2;->this$0:Lcom/squareup/printer/epson/RealEpsonDiscoverer;

    invoke-static {v1}, Lcom/squareup/printer/epson/RealEpsonDiscoverer;->access$getEpsonPrinterFactory$p(Lcom/squareup/printer/epson/RealEpsonDiscoverer;)Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/printer/epson/EpsonPrinter$Factory;->create(Lcom/squareup/printer/epson/EpsonPrinterInfo;)Lcom/squareup/printer/epson/EpsonPrinter;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
