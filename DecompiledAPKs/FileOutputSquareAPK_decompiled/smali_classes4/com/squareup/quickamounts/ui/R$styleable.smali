.class public final Lcom/squareup/quickamounts/ui/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final QuickAmountsView:[I

.field public static final QuickAmountsView_amountCount:I = 0x1

.field public static final QuickAmountsView_android_textColor:I = 0x0

.field public static final QuickAmountsView_buttonSelector:I = 0x2

.field public static final QuickAmountsView_drawBottomLine:I = 0x3

.field public static final QuickAmountsView_drawTopLine:I = 0x4

.field public static final QuickAmountsView_lineColor:I = 0x5

.field public static final QuickAmountsView_lineWidth:I = 0x6


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    new-array v0, v0, [I

    .line 43
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/quickamounts/ui/R$styleable;->QuickAmountsView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010098
        0x7f040032
        0x7f040086
        0x7f040138
        0x7f04013c
        0x7f04027d
        0x7f040282
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
