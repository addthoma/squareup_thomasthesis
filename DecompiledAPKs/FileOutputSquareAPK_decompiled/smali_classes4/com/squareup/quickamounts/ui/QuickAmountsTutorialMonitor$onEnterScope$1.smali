.class final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "QuickAmountsTutorialMonitor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;",
        "+",
        "Ljava/lang/Boolean;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 57
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 58
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getQuickAmountsTutorialCreator$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;->deactivate()V

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;->getShowEligibilityTutorial()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 60
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getQuickAmountsTutorialCreator$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    move-result-object p1

    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$AutomaticEligibilityStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$AutomaticEligibilityStart;

    check-cast v0, Lcom/squareup/quickamounts/ui/StartMode;

    invoke-virtual {p1, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;->activate(Lcom/squareup/quickamounts/ui/StartMode;)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$TutorialVisibilities;->getShowUpdatedAmountsTutorial()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 62
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getQuickAmountsTutorialCreator$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    move-result-object p1

    sget-object v0, Lcom/squareup/quickamounts/ui/StartMode$AutomaticUpdatedAmountsStart;->INSTANCE:Lcom/squareup/quickamounts/ui/StartMode$AutomaticUpdatedAmountsStart;

    check-cast v0, Lcom/squareup/quickamounts/ui/StartMode;

    invoke-virtual {p1, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;->activate(Lcom/squareup/quickamounts/ui/StartMode;)V

    goto :goto_0

    .line 64
    :cond_2
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor$onEnterScope$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;->access$getQuickAmountsTutorialCreator$p(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;->deactivate()V

    :goto_0
    return-void
.end method
