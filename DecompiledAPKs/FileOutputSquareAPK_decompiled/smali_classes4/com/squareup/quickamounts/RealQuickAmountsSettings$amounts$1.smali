.class final Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings;->amounts()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "kotlin.jvm.PlatformType",
        "source",
        "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-static {v0}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$getCogs$p(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Lcom/squareup/cogs/Cogs;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$quickAmountsForCurrentUnit(Lcom/squareup/quickamounts/RealQuickAmountsSettings;Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1$1;-><init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$amounts$1;->apply(Lcom/squareup/quickamounts/RealQuickAmountsSettings$DataSource;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
