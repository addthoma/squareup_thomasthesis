.class final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$2;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsLayoutRunner.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->invoke(ILcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "focused",
        "",
        "onFocusChange",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$1$2$1$1$2",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$row$lambda$1$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $item:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$2;->$item:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 0

    .line 106
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$2;->$item:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    invoke-virtual {p1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->getOnFocused()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
