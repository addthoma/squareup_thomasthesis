.class public final Lcom/squareup/quickamounts/settings/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/settings/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final quick_amounts_settings_add:I = 0x7f121504

.field public static final quick_amounts_settings_clear_value_accessibility:I = 0x7f121505

.field public static final quick_amounts_settings_description:I = 0x7f121506

.field public static final quick_amounts_settings_items_prompt_create:I = 0x7f121507

.field public static final quick_amounts_settings_items_prompt_description:I = 0x7f121508

.field public static final quick_amounts_settings_items_prompt_no_thanks:I = 0x7f121509

.field public static final quick_amounts_settings_items_prompt_title:I = 0x7f12150a

.field public static final quick_amounts_settings_preview_heading:I = 0x7f12150b

.field public static final quick_amounts_settings_radio_auto:I = 0x7f12150c

.field public static final quick_amounts_settings_radio_off:I = 0x7f12150d

.field public static final quick_amounts_settings_radio_set:I = 0x7f12150e

.field public static final quick_amounts_settings_status_auto:I = 0x7f12150f

.field public static final quick_amounts_settings_status_off:I = 0x7f121510

.field public static final quick_amounts_settings_status_set:I = 0x7f121511

.field public static final quick_amounts_settings_status_use_quick_amounts:I = 0x7f121512

.field public static final quick_amounts_settings_switch:I = 0x7f121513

.field public static final quick_amounts_settings_title:I = 0x7f121514


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
