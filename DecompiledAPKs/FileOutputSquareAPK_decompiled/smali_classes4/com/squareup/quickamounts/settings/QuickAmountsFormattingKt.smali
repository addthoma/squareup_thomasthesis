.class public final Lcom/squareup/quickamounts/settings/QuickAmountsFormattingKt;
.super Ljava/lang/Object;
.source "QuickAmountsFormatting.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmountsFormatting.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmountsFormatting.kt\ncom/squareup/quickamounts/settings/QuickAmountsFormattingKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,38:1\n747#2:39\n769#2,2:40\n1360#2:42\n1429#2,3:43\n*E\n*S KotlinDebug\n*F\n+ 1 QuickAmountsFormatting.kt\ncom/squareup/quickamounts/settings/QuickAmountsFormattingKt\n*L\n16#1:39\n16#1,2:40\n33#1:42\n33#1,3:43\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\u0018\u0010\u0002\u001a\u00020\u0003*\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u001a\u0012\u0010\u0007\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "filterZeroes",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "formatForPreview",
        "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "maybeAddZeroAmount",
        "currency",
        "Lcom/squareup/protos/connect/v2/common/Currency;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final filterZeroes(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 5

    const-string v0, "$this$filterZeroes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {p0}, Lcom/squareup/quickamounts/QuickAmounts;->getSetAmounts()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 39
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 40
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/connect/v2/common/Money;

    .line 16
    invoke-static {v4}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 41
    :cond_1
    check-cast v2, Ljava/util/List;

    .line 16
    invoke-virtual {p0, v0, v2}, Lcom/squareup/quickamounts/QuickAmounts;->replaceAmounts(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p0

    return-object p0
.end method

.method public static final formatForPreview(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/text/Formatter;)Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;"
        }
    .end annotation

    const-string v0, "$this$formatForPreview"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 43
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 44
    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    .line 34
    new-instance v2, Lcom/squareup/quickamounts/ui/UiQuickAmount;

    invoke-static {v1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "moneyFormatter.format(quickAmount.toMoneyV1())"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1, v3}, Lcom/squareup/quickamounts/ui/UiQuickAmount;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/CharSequence;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 36
    new-instance p0, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;-><init>(Ljava/util/List;)V

    return-object p0
.end method

.method public static final maybeAddZeroAmount(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 4

    const-string v0, "$this$maybeAddZeroAmount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currency"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/quickamounts/QuickAmounts;->getSetAmounts()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    sget-object v0, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    new-instance v1, Lcom/squareup/protos/connect/v2/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/protos/connect/v2/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/connect/v2/common/Currency;)V

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/quickamounts/QuickAmounts;->replaceAmounts(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p0

    :cond_0
    return-object p0
.end method
