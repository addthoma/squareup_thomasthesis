.class final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "QuickAmountsSettingsLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "row",
        "",
        "item",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "invoke",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$1$2$1$1",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$row$lambda$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $clearPlugin:Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;

.field final synthetic $this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;


# direct methods
.method constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$clearPlugin:Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->invoke(ILcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;)V
    .locals 2

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->getAmountEditText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->getEditable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setEnabled(Z)V

    .line 94
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$clearPlugin:Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;

    check-cast v1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->removePlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 95
    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->getEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->getClearable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$clearPlugin:Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;

    check-cast v1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$clearPlugin:Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;

    new-instance v1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$1;

    invoke-direct {v1, p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$1;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;->onClick(Lkotlin/jvm/functions/Function0;)V

    .line 100
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;->getFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->requestFocus()Z

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_2

    sget-object p1, Lcom/squareup/noho/NohoEditRow$PositionInList;->FIRST:Lcom/squareup/noho/NohoEditRow$PositionInList;

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/squareup/noho/NohoEditRow$PositionInList;->MIDDLE:Lcom/squareup/noho/NohoEditRow$PositionInList;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->setPositionInList(Lcom/squareup/noho/NohoEditRow$PositionInList;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;->$this_create:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$2;

    invoke-direct {v0, p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1$2;-><init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;)V

    check-cast v0, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method
