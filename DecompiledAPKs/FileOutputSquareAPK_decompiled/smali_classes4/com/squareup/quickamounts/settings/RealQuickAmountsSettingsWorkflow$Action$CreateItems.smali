.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;
.super Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.source "RealQuickAmountsSettingsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateItems"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0018\u0010\u0011\u001a\u00020\u0012*\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/analytics/Analytics;)V",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 226
    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;Lcom/squareup/analytics/Analytics;ILjava/lang/Object;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->copy(Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->onClickedCreateItem(Lcom/squareup/analytics/Analytics;)V

    .line 229
    sget-object v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$StartItemsTutorial;->INSTANCE:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$StartItemsTutorial;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/analytics/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final copy(Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;

    invoke-direct {v0, p1}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;-><init>(Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object p1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateItems(analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$CreateItems;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
