.class public abstract Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule;
.super Ljava/lang/Object;
.source "RequestingBusinessAddressModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule;",
        "",
        "()V",
        "bindRequestingBusinessAddress",
        "Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;",
        "realRequestBusinessAddressMonitor",
        "Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule;->Companion:Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideHasUserResolvedBusinessAddress(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/requestingbusinessaddress/HasUserResolvedBusinessAddress;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule;->Companion:Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/requestingbusinessaddress/RequestingBusinessAddressModule$Companion;->provideHasUserResolvedBusinessAddress(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindRequestingBusinessAddress(Lcom/squareup/requestingbusinessaddress/RealRequestBusinessAddressMonitor;)Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
