.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getAllItemsInCategory(Ljava/util/List;)Lrx/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/util/Set<",
        "+",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,607:1\n1360#2:608\n1429#2,3:609\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1\n*L\n581#1:608\n581#1,3:609\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u00032\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/HashSet;",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "Lkotlin/collections/HashSet;",
        "cogsLocal",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "kotlin.jvm.PlatformType",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $categoryIds:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;->$categoryIds:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 95
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/HashSet;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/HashSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/HashSet<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation

    .line 577
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 580
    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;->$categoryIds:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 608
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 609
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 610
    check-cast v3, Ljava/lang/String;

    .line 583
    const-class v4, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    invoke-interface {p1, v4}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v4

    check-cast v4, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 584
    iget-object v5, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$getAllItemsInCategory$1;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {v5}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getSupportedCatalogTypes$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->findCatalogItemsForCategoryId(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v3

    const-string v4, "cursor"

    .line 587
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 588
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToFirst()Z

    .line 590
    :cond_0
    move-object v4, v0

    check-cast v4, Ljava/util/Collection;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 591
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 593
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    .line 594
    sget-object v3, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 611
    :cond_2
    check-cast v2, Ljava/util/List;

    return-object v0
.end method
