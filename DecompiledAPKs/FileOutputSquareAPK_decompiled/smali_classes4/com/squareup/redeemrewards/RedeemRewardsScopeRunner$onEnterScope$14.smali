.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;
.super Lkotlin/jvm/internal/Lambda;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 2

    .line 360
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getFlow$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lflow/Flow;

    move-result-object p1

    new-instance v0, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;

    iget-object v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {v1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getRedeemScope$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/RedeemRewardsScope;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method
