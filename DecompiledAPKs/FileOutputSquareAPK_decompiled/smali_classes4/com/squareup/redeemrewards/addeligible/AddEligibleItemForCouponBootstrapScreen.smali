.class public final Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponBootstrapScreen;
.super Lcom/squareup/container/BlockingBootstrapTreeKey;
.source "AddEligibleItemForCouponBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponBootstrapScreen;",
        "Lcom/squareup/container/BlockingBootstrapTreeKey;",
        "props",
        "Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;",
        "(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Lcom/squareup/container/BlockingBootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponBootstrapScreen;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    sget-object v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflowRunner;->Companion:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflowRunner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponBootstrapScreen;->props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-interface {p1, v0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponWorkflowRunner;->start(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponScope;->INSTANCE:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponScope;

    return-object v0
.end method
