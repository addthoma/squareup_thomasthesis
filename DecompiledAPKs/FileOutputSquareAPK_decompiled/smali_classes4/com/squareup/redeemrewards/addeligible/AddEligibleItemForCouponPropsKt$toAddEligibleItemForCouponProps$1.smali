.class final Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AddEligibleItemForCouponProps.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt;->toAddEligibleItemForCouponProps(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddEligibleItemForCouponProps.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddEligibleItemForCouponProps.kt\ncom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,75:1\n1265#2,12:76\n*E\n*S KotlinDebug\n*F\n+ 1 AddEligibleItemForCouponProps.kt\ncom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1\n*L\n35#1,12:76\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "constraintIds",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toAddEligibleItemForCouponProps:Lcom/squareup/protos/client/coupons/Coupon;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;->$this_toAddEligibleItemForCouponProps:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;->invoke()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponPropsKt$toAddEligibleItemForCouponProps$1;->$this_toAddEligibleItemForCouponProps:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v0, v0, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    const-string v1, "reward.item_constraint"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 76
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 83
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 84
    check-cast v2, Lcom/squareup/protos/client/coupons/ItemConstraint;

    .line 35
    iget-object v2, v2, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 85
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_0

    .line 87
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method
