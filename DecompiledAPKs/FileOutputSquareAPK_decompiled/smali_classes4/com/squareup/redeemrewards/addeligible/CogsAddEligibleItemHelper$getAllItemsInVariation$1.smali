.class final Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;
.super Ljava/lang/Object;
.source "CogsAddEligibleItemHelper.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->getAllItemsInVariation(Ljava/util/List;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/util/HashSet<",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCogsAddEligibleItemHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CogsAddEligibleItemHelper.kt\ncom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,88:1\n1412#2,9:89\n1642#2,2:98\n1421#2:100\n1360#2:101\n1429#2,3:102\n*E\n*S KotlinDebug\n*F\n+ 1 CogsAddEligibleItemHelper.kt\ncom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1\n*L\n31#1,9:89\n31#1,2:98\n31#1:100\n34#1:101\n34#1,3:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u00032\u000e\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/HashSet;",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "Lkotlin/collections/HashSet;",
        "cogsLocal",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "kotlin.jvm.PlatformType",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $variationIds:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;->this$0:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;

    iput-object p2, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;->$variationIds:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/HashSet;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/HashSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/HashSet<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 30
    iget-object v1, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;->$variationIds:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 89
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 98
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 97
    check-cast v3, Ljava/lang/String;

    .line 32
    const-class v4, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-interface {p1, v4, v3}, Lcom/squareup/shared/catalog/Catalog$Local;->findByIdOrNull(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v3

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    if-eqz v3, :cond_0

    .line 97
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_1
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 102
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 103
    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 34
    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getItemId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 104
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 35
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v1

    .line 39
    const-class v2, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    invoke-interface {p1, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 40
    iget-object v2, p0, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper$getAllItemsInVariation$1;->this$0:Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;

    invoke-static {v2}, Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;->access$getSupportedCatalogTypes$p(Lcom/squareup/redeemrewards/addeligible/CogsAddEligibleItemHelper;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    const-string v2, "cursor"

    .line 43
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getCount()I

    move-result v2

    if-lez v2, :cond_6

    .line 44
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToFirst()Z

    .line 46
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 47
    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 49
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    .line 51
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    return-object v0
.end method
