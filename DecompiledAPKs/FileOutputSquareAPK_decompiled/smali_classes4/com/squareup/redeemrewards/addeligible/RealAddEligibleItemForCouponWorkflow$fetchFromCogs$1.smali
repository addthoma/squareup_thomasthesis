.class final Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$1;
.super Ljava/lang/Object;
.source "RealAddEligibleItemForCouponWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow;->fetchFromCogs(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;",
        "it",
        "Ljava/util/HashSet;",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "Lkotlin/collections/HashSet;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$1;->$props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/HashSet;)Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)",
            "Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;

    iget-object v1, p0, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$1;->$props:Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;

    check-cast p1, Ljava/util/Set;

    invoke-direct {v0, v1, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;-><init>(Lcom/squareup/redeemrewards/addeligible/AddEligibleItemForCouponProps;Ljava/util/Set;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$fetchFromCogs$1;->apply(Ljava/util/HashSet;)Lcom/squareup/redeemrewards/addeligible/RealAddEligibleItemForCouponWorkflow$Action$ItemsFetched;

    move-result-object p1

    return-object p1
.end method
