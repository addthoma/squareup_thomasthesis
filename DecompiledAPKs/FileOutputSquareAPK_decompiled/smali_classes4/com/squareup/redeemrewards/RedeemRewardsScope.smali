.class public final Lcom/squareup/redeemrewards/RedeemRewardsScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "RedeemRewardsScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/cart/util/PartOfCartScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardsScope$ParentComponent;,
        Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;,
        Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;,
        Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \u00142\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u0014\u0015\u0016\u0017B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0014J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "Lcom/squareup/cart/util/PartOfCartScope;",
        "firstScreenIsCustomerList",
        "",
        "(Z)V",
        "getFirstScreenIsCustomerList$redeem_rewards_release",
        "()Z",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getName",
        "",
        "register",
        "scope",
        "Lmortar/MortarScope;",
        "Companion",
        "Component",
        "Module",
        "ParentComponent",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion;


# instance fields
.field private final firstScreenIsCustomerList:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScope;->Companion:Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion;

    .line 151
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardsScope$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { it ->\n     \u2026(it.readInt() != 0)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope;->firstScreenIsCustomerList:Z

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 147
    iget-boolean p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope;->firstScreenIsCustomerList:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public final getFirstScreenIsCustomerList$redeem_rewards_release()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope;->firstScreenIsCustomerList:Z

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/InMainActivityScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope;->firstScreenIsCustomerList:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    .line 133
    invoke-interface {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;->redeemRewardsScopeRunner()Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
