.class public abstract Lcom/squareup/redeemrewards/RedeemRewardState;
.super Ljava/lang/Object;
.source "RedeemRewardState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;,
        Lcom/squareup/redeemrewards/RedeemRewardState$UsingRewardCode;,
        Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;,
        Lcom/squareup/redeemrewards/RedeemRewardState$ErrorLookingUpContact;,
        Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;,
        Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;,
        Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0007\u0008\t\n\u000b\u000c\rB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0007\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "Landroid/os/Parcelable;",
        "searchTerm",
        "",
        "(Ljava/lang/String;)V",
        "getSearchTerm",
        "()Ljava/lang/String;",
        "AddingEligibleItemFromCode",
        "AddingEligibleItemFromContact",
        "ChooseContact",
        "ErrorLookingUpContact",
        "LookingUpContact",
        "UsingRewardCode",
        "ViewingRewards",
        "Lcom/squareup/redeemrewards/RedeemRewardState$ChooseContact;",
        "Lcom/squareup/redeemrewards/RedeemRewardState$UsingRewardCode;",
        "Lcom/squareup/redeemrewards/RedeemRewardState$LookingUpContact;",
        "Lcom/squareup/redeemrewards/RedeemRewardState$ErrorLookingUpContact;",
        "Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromCode;",
        "Lcom/squareup/redeemrewards/RedeemRewardState$AddingEligibleItemFromContact;",
        "Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final searchTerm:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardState;->searchTerm:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardState;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getSearchTerm()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardState;->searchTerm:Ljava/lang/String;

    return-object v0
.end method
