.class public final Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseCustomerCoordinatorV2.kt"


# annotations
.annotation runtime Lkotlin/Deprecated;
    message = "TODO: CRM-4708 - should use new ChooseCustomerFlow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
        "contactLoader",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "(Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;Lcom/squareup/crm/RolodexContactLoader;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final runner:Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;Lcom/squareup/crm/RolodexContactLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactLoader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->runner:Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    return-void
.end method

.method public static final synthetic access$getContactLoader$p(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->runner:Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 45
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    .line 46
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/redeemrewards/R$string;->coupon_redeem_rewards:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 47
    new-instance v2, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$1;-><init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 48
    sget v2, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_use_code:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    .line 49
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    .line 50
    new-instance v2, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$2;

    invoke-direct {v2, p0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$2;-><init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    .line 52
    sget v1, Lcom/squareup/redeemrewards/R$id;->crm_search_box:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    .line 53
    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->runner:Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    invoke-interface {v2}, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;->getContactLoaderSearchTerm()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 54
    new-instance v2, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3;

    invoke-direct {v2, p0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$3;-><init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 62
    new-instance v2, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$4;

    invoke-direct {v2, v1}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$4;-><init>(Lcom/squareup/ui/XableEditText;)V

    check-cast v2, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 74
    sget v1, Lcom/squareup/redeemrewards/R$id;->crm_contact_list:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    .line 75
    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v3, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->runner:Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    invoke-interface {v3}, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;->getContactListScrollPosition()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->init(Lcom/squareup/crm/RolodexContactLoader;I)V

    .line 77
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->scrollPosition()Lrx/Observable;

    move-result-object v2

    const-string v3, "contactList.scrollPosition()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v3, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$5;

    invoke-direct {v3, p0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$5;-><init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v3}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 80
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked()Lrx/Observable;

    move-result-object v2

    const-string v3, "contactList.onContactClicked()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v3, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$6;

    invoke-direct {v3, p0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$6;-><init>(Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v3}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 83
    sget v2, Lcom/squareup/redeemrewards/R$id;->crm_search_progress_bar:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    .line 84
    sget v3, Lcom/squareup/redeemrewards/R$id;->crm_search_message:I

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/widgets/MessageView;

    .line 86
    iget-object v4, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object v4

    const-string/jumbo v5, "visualStateOf(contactLoader, null)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    new-instance v5, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;

    invoke-direct {v5, v2, v1, v3, v0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;-><init>(Landroid/widget/ProgressBar;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/widgets/MessageView;Landroid/content/res/Resources;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v4, p1, v5}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method
