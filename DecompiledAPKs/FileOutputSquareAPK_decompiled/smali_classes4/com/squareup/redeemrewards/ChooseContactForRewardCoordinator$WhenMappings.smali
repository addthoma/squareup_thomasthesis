.class public final synthetic Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->values()[Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_AT_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {v1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    return-void
.end method
