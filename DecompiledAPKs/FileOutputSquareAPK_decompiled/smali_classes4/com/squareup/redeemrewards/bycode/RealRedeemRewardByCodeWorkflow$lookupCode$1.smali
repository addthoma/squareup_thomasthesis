.class final Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;
.super Ljava/lang/Object;
.source "RealRedeemRewardByCodeWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->lookupCode(Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/redeemrewards/bycode/Action;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

.field final synthetic this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;->this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;->$state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/redeemrewards/bycode/Action;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
            ">;)",
            "Lcom/squareup/redeemrewards/bycode/Action;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;->this$0:Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;

    iget-object v1, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;->$state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    invoke-static {v0, v1, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;->access$handleSuccess(Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;Lcom/squareup/protos/client/coupons/LookupCouponsResponse;)Lcom/squareup/redeemrewards/bycode/Action;

    move-result-object p1

    goto :goto_0

    .line 144
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;

    iget-object v0, p0, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;->$state:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;->getCode()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Error;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward$Error;

    check-cast v1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    invoke-direct {p1, v0, v1}, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;-><init>(Ljava/lang/String;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;)V

    check-cast p1, Lcom/squareup/redeemrewards/bycode/Action;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/bycode/RealRedeemRewardByCodeWorkflow$lookupCode$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/redeemrewards/bycode/Action;

    move-result-object p1

    return-object p1
.end method
