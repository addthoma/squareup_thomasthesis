.class abstract Lcom/squareup/redeemrewards/bycode/Action;
.super Ljava/lang/Object;
.source "RealRedeemRewardByCodeWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/bycode/Action$Close;,
        Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;,
        Lcom/squareup/redeemrewards/bycode/Action$LookupCode;,
        Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;,
        Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;,
        Lcom/squareup/redeemrewards/bycode/Action$ShowEnterCode;,
        Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0007\u0007\u0008\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0007\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/bycode/Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
        "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "ApplyCoupon",
        "Close",
        "CodeEntered",
        "LookupCode",
        "ShowEnterCode",
        "ShowNoRewardScreen",
        "ShowRewardScreen",
        "Lcom/squareup/redeemrewards/bycode/Action$Close;",
        "Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;",
        "Lcom/squareup/redeemrewards/bycode/Action$LookupCode;",
        "Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;",
        "Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;",
        "Lcom/squareup/redeemrewards/bycode/Action$ShowEnterCode;",
        "Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 207
    invoke-direct {p0}, Lcom/squareup/redeemrewards/bycode/Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
            ">;)",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    instance-of v0, p0, Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;

    move-object v1, p0

    check-cast v1, Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/Action$CodeEntered;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 234
    :cond_0
    instance-of v0, p0, Lcom/squareup/redeemrewards/bycode/Action$LookupCode;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;

    move-object v1, p0

    check-cast v1, Lcom/squareup/redeemrewards/bycode/Action$LookupCode;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/Action$LookupCode;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$LookingUpRewardCode;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 236
    :cond_1
    instance-of v0, p0, Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;

    move-object v1, p0

    check-cast v1, Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/Action$ShowRewardScreen;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$DisplayingReward;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/coupons/Coupon;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 238
    :cond_2
    instance-of v0, p0, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound;

    move-object v1, p0

    check-cast v1, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/bycode/Action$ShowNoRewardScreen;->getReason()Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound;-><init>(Ljava/lang/String;Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$NoRewardFound$ReasonForNoReward;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 240
    :cond_3
    sget-object v0, Lcom/squareup/redeemrewards/bycode/Action$ShowEnterCode;->INSTANCE:Lcom/squareup/redeemrewards/bycode/Action$ShowEnterCode;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState$EnterRewardCode;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 242
    :cond_4
    sget-object p1, Lcom/squareup/redeemrewards/bycode/Action$Close;->INSTANCE:Lcom/squareup/redeemrewards/bycode/Action$Close;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$Close;->INSTANCE:Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$Close;

    check-cast p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;

    return-object p1

    .line 244
    :cond_5
    instance-of p1, p0, Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;

    if-eqz p1, :cond_6

    new-instance p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$ApplyCoupon;

    move-object v0, p0

    check-cast v0, Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;

    invoke-virtual {v0}, Lcom/squareup/redeemrewards/bycode/Action$ApplyCoupon;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput$ApplyCoupon;-><init>(Lcom/squareup/protos/client/coupons/Coupon;)V

    check-cast p1, Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;

    return-object p1

    :cond_6
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 207
    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/bycode/Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeState;",
            "-",
            "Lcom/squareup/redeemrewards/bycode/RedeemRewardByCodeOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
