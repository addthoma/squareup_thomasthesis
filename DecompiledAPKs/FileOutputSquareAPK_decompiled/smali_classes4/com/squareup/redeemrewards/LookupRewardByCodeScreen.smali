.class public final Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;
.super Lcom/squareup/redeemrewards/InRedeemRewardScope;
.source "LookupRewardByCodeScreen.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;,
        Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLookupRewardByCodeScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LookupRewardByCodeScreen.kt\ncom/squareup/redeemrewards/LookupRewardByCodeScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,43:1\n43#2:44\n*E\n*S KotlinDebug\n*F\n+ 1 LookupRewardByCodeScreen.kt\ncom/squareup/redeemrewards/LookupRewardByCodeScreen\n*L\n30#1:44\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0002\u0012\u0013B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0014J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000cH\u0016\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;",
        "Lcom/squareup/redeemrewards/InRedeemRewardScope;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/LayoutScreen;",
        "redeemRewardsScope",
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "Runner",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;->Companion:Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion;

    .line 38
    sget-object v0, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026ava.classLoader)!!)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V
    .locals 1

    const-string v0, "redeemRewardsScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/InRedeemRewardScope;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-super {p0, p1, p2}, Lcom/squareup/redeemrewards/InRedeemRewardScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/LookupRewardByCodeScreen;->getRedeemRewardsScope$redeem_rewards_release()Lcom/squareup/redeemrewards/RedeemRewardsScope;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    .line 30
    invoke-interface {p1}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;->lookupRewardByCodeCoordinator()Lcom/squareup/redeemrewards/LookupRewardByCodeCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 27
    sget v0, Lcom/squareup/redeemrewards/R$layout;->crm_lookup_reward_by_code:I

    return v0
.end method
