.class final Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ViewRewardsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ViewRewardsWorkflow;->render(Lcom/squareup/redeemrewards/ViewRewardsProps;Lcom/squareup/redeemrewards/ViewRewardsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "+",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "+",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/redeemrewards/ViewRewardsState;",
        "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
        "Lcom/squareup/redeemrewards/Action0;",
        "action",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;->INSTANCE:Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/WorkflowAction;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "+",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/redeemrewards/ViewRewardsState;",
            "Lcom/squareup/redeemrewards/ViewRewardsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ViewRewardsWorkflow$render$2;->invoke(Lcom/squareup/workflow/WorkflowAction;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
