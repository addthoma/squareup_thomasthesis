.class public interface abstract Lcom/squareup/redeemrewards/RedeemRewardWorkflow;
.super Ljava/lang/Object;
.source "RedeemRewardWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lcom/squareup/redeemrewards/RedeemRewardProps;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002:\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0001\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/redeemrewards/RedeemRewardProps;",
        "Lcom/squareup/redeemrewards/RedeemRewardOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
