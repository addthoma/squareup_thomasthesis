.class final Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;
.super Ljava/lang/Object;
.source "RealSafetyNetRecaptchaVerifier.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->verifyWithRecaptcha(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/safetynetrecaptcha/CaptchaResult;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $apiKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;


# direct methods
.method constructor <init>(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    iput-object p2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->$apiKey:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/safetynetrecaptcha/CaptchaResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    invoke-static {v0}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->access$getSafetyNetClient$p(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;)Lcom/google/android/gms/safetynet/SafetyNetClient;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->$apiKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/safetynet/SafetyNetClient;->verifyWithRecaptcha(Ljava/lang/String;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$1;-><init>(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;Lio/reactivex/SingleEmitter;)V

    check-cast v1, Lcom/google/android/gms/tasks/OnSuccessListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    .line 52
    new-instance v1, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;-><init>(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;Lio/reactivex/SingleEmitter;)V

    check-cast v1, Lcom/google/android/gms/tasks/OnFailureListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method
