.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BuyerCheckoutReactor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00be\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\n\u001a\u00020\u000bH\u0002J*\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00022\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0002J6\u0010\u0013\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020\u00182\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001aH\u0002J>\u0010\u001b\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020\u001c2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001a2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J6\u0010\u001f\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020 2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001aH\u0002J>\u0010!\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020\"2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001a2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J(\u0010#\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020$H\u0002J(\u0010%\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020&H\u0002J(\u0010\'\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020\u00042\u0006\u0010*\u001a\u00020+H\u0002J(\u0010,\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020\u000eH\u0002J\u0016\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0\u00142\u0006\u0010/\u001a\u000200H\u0002J\u0008\u00101\u001a\u000202H\u0002J\u0008\u00103\u001a\u000202H\u0002J$\u00104\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u00152\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u00105\u001a\u000206H\u0002J>\u00107\u001a\u001a\u0012\u0016\u0008\u0001\u0012\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0015j\u0002`\u00160\u00142\u0006\u0010\u0017\u001a\u00020\u00022\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u001a2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u001e\u00108\u001a\u0008\u0012\u0004\u0012\u00020\u00020\r2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u00109\u001a\u00020+H\u0002J\u0008\u0010:\u001a\u000200H\u0002J\u0010\u0010;\u001a\u0002002\u0006\u0010\u0017\u001a\u00020&H\u0002J\u0016\u0010<\u001a\u0008\u0012\u0004\u0012\u00020(0\r2\u0006\u0010=\u001a\u00020>H\u0002J\u0016\u0010?\u001a\u0008\u0012\u0004\u0012\u00020(0\r2\u0006\u0010@\u001a\u00020AH\u0002J\u0016\u0010B\u001a\u0008\u0012\u0004\u0012\u00020(0\r2\u0006\u0010C\u001a\u00020DH\u0002J\u0008\u0010E\u001a\u000200H\u0002J\u001e\u0010F\u001a\u0008\u0012\u0004\u0012\u00020\u00020\r2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010G\u001a\u00020HH\u0002R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006I"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "paymentInputHandler",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;Lcom/squareup/ui/main/errors/PaymentInputHandler;)V",
        "getPaymentInputHandler",
        "()Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "amountDue",
        "Lcom/squareup/protos/common/Money;",
        "enterStateWaitingForCancelPermission",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
        "from",
        "ifGranted",
        "log",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "handleInBuyerCart",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutWorkflowReaction;",
        "state",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "handleInLanguageSelection",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "handleInPaymentPrompt",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;",
        "handleInTipping",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;",
        "handleStartTipping",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;",
        "handleStarting",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;",
        "handleStopping",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;",
        "handleSwipeSuccess",
        "successfulSwipe",
        "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
        "handleWaitingForCancelPermission",
        "initializePaymentEventHandler",
        "Lcom/squareup/buyercheckout/PaymentInputHandlerResult;",
        "enableWithoutNfc",
        "",
        "initializePaymentEventHandlerWithoutNfc",
        "",
        "maybeDropPaymentForPreAuthTipping",
        "onPaymentEvent",
        "paymentEvent",
        "Lcom/squareup/ui/main/errors/PaymentEvent;",
        "onReact",
        "processSwipeAndChangeState",
        "swipe",
        "shouldPromptForTip",
        "shouldShowBuyerCart",
        "stopWithProcessContactless",
        "result",
        "Lcom/squareup/ui/main/SmartPaymentResult;",
        "stopWithProcessEmvDip",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "stopWithReaderIssue",
        "issue",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "tipScreenEnabled",
        "updateToast",
        "failureReason",
        "Lcom/squareup/ui/main/errors/PaymentError;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;


# direct methods
.method public constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;Lcom/squareup/ui/main/errors/PaymentInputHandler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ")V"
        }
    .end annotation

    const-string v0, "paymentInputHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    return-void
.end method

.method public static final synthetic access$enterStateWaitingForCancelPermission(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 0

    .line 176
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->enterStateWaitingForCancelPermission(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onPaymentEvent(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/ui/main/errors/PaymentEvent;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 0

    .line 176
    invoke-direct {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->onPaymentEvent(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/ui/main/errors/PaymentEvent;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$shouldPromptForTip(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;)Z
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->shouldPromptForTip()Z

    move-result p0

    return p0
.end method

.method private final amountDue()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 558
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    const-string v1, "transaction.requireBillPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    const-string v1, "if (tenderInEdit.isEditi\u2026ainingAmountDue\n        }"

    .line 559
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 566
    :cond_1
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const-string v1, "transaction.amountDue"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object v0
.end method

.method private final enterStateWaitingForCancelPermission(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/eventstream/v1/EventStreamEvent;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
            ">;"
        }
    .end annotation

    .line 513
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 514
    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 513
    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method static synthetic enterStateWaitingForCancelPermission$default(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 512
    check-cast p3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->enterStateWaitingForCancelPermission(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p0

    return-object p0
.end method

.method private final handleInBuyerCart(Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 270
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->maybeDropPaymentForPreAuthTipping()V

    .line 272
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->initializePaymentEventHandlerWithoutNfc()V

    .line 273
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getAnalytics$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewBuyerCart;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewBuyerCart;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTutorialCore$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object v0

    const-string v1, "Shown BuyerCartScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 276
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final handleInLanguageSelection(Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 405
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;

    invoke-direct {v0, p3, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInLanguageSelection$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final handleInPaymentPrompt(Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;)",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 314
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;->getInOfflineMode()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->initializePaymentEventHandler(Z)Lio/reactivex/Single;

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getAnalytics$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    sget-object v2, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 318
    new-instance v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;Lio/reactivex/Single;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final handleInTipping(Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 374
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;

    invoke-direct {v0, p0, p3, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInTipping$1;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private final handleStartTipping(Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 353
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cancelPaymentAndDestroy()V

    .line 355
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    .line 357
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderFactory$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/tender/TenderFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/tender/TenderFactory;->createSmartCard()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v0, v1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 358
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    const-string v1, "tenderInEdit.requireSmartCardTender()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    const-string v2, "transaction.asBillPayment()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->getRemainingAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 366
    :cond_0
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object p1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v1, p1, v3, v2, v3}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(EnterState(InTipping(state.data)))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final handleStarting(Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getAnalytics$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    .line 216
    sget-object v1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;

    .line 217
    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getCustomerCheckoutSettings$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getBuyerCartFormatter$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/ui/cart/BuyerCartFormatter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/cart/BuyerCartFormatter;->getDisplayItems()Ljava/util/List;

    move-result-object v3

    .line 216
    invoke-virtual {v1, v2, v3}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;->of(Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Ljava/util/List;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 215
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 221
    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    .line 222
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getOfflineModeMonitor$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/OfflineModeMonitor;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/payment/OfflineModeMonitor;->isInOfflineMode()Z

    move-result v1

    .line 223
    iget-object v2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getFeatures$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/settings/server/Features;

    move-result-object v2

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 221
    invoke-direct {v0, v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;-><init>(ZZ)V

    .line 227
    new-instance v1, Lcom/squareup/workflow/legacy/EnterState;

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->shouldShowBuyerCart(Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;)Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-direct {p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto :goto_0

    .line 230
    :cond_0
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->shouldPromptForTip()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    invoke-direct {p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto :goto_0

    .line 231
    :cond_1
    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    invoke-direct {p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 227
    :goto_0
    invoke-direct {v1, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    .line 226
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(\n          EnterSta\u2026    }\n          )\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final handleStopping(Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 239
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;->getResult()Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cancelPayment()V

    .line 243
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->maybeDropPaymentForPreAuthTipping()V

    .line 246
    :cond_0
    new-instance v0, Lcom/squareup/workflow/legacy/FinishWith;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;->getResult()Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "just(FinishWith(state.result))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final handleSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Lcom/squareup/tenderpayment/TenderPaymentResult;
    .locals 4

    .line 597
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 598
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->amountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 599
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/payment/AcceptsTips;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    move-object v1, v3

    :cond_0
    check-cast v1, Lcom/squareup/payment/AcceptsTips;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/squareup/payment/AcceptsTips;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 601
    :cond_1
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderCompleter$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/tenderpayment/TenderCompleter;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;->card:Lcom/squareup/Card;

    invoke-interface {v1, p1, v0, v3}, Lcom/squareup/tenderpayment/TenderCompleter;->payCardTender(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object p1

    .line 603
    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    if-ne p1, v0, :cond_2

    .line 604
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 606
    :cond_2
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 609
    :cond_3
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;-><init>(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    :goto_0
    return-object p1
.end method

.method private final handleWaitingForCancelPermission(Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    .line 251
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getPermissionGatekeeper$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object v0

    .line 252
    sget-object v1, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    invoke-static {v0, v1}, Lcom/squareup/permissions/PermissionGatekeepersKt;->seekPermission(Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/Permission;)Lio/reactivex/Single;

    move-result-object v0

    .line 253
    new-instance v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleWaitingForCancelPermission$1;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "permissionGatekeeper\n   \u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final initializePaymentEventHandler(Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/buyercheckout/PaymentInputHandlerResult;",
            ">;"
        }
    .end annotation

    .line 531
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->cancelPaymentAndDestroy()V

    if-eqz p1, :cond_0

    .line 534
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithoutNfc()V

    .line 535
    invoke-static {}, Lio/reactivex/Single;->never()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.never()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 537
    :cond_0
    new-instance p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1;

    invoke-direct {p1, p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$initializePaymentEventHandler$1;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;)V

    check-cast p1, Lio/reactivex/SingleOnSubscribe;

    invoke-static {p1}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.create<PaymentInp\u2026      }\n        )\n      }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final initializePaymentEventHandlerWithoutNfc()V
    .locals 1

    const/4 v0, 0x1

    .line 522
    invoke-direct {p0, v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->initializePaymentEventHandler(Z)Lio/reactivex/Single;

    return-void
.end method

.method private final maybeDropPaymentForPreAuthTipping()V
    .locals 2

    .line 453
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->tipScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_OTHER:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    :cond_0
    return-void
.end method

.method private final onPaymentEvent(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/ui/main/errors/PaymentEvent;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/ui/main/errors/PaymentEvent;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    .line 464
    instance-of v0, p2, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/TakeSwipePayment;->getSwipe()Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->processSwipeAndChangeState(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 465
    :cond_0
    instance-of v0, p2, Lcom/squareup/ui/main/errors/TakeDipPayment;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/squareup/ui/main/errors/TakeDipPayment;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/TakeDipPayment;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->stopWithProcessEmvDip(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 466
    :cond_1
    instance-of v0, p2, Lcom/squareup/ui/main/errors/TakeTapPayment;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/squareup/ui/main/errors/TakeTapPayment;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/TakeTapPayment;->getSmartPaymentResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->stopWithProcessContactless(Lcom/squareup/ui/main/SmartPaymentResult;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 467
    :cond_2
    instance-of v0, p2, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    if-eqz v0, :cond_3

    check-cast p2, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/ReportReaderIssue;->getIssue()Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->stopWithReaderIssue(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 468
    :cond_3
    instance-of v0, p2, Lcom/squareup/ui/main/errors/CardFailed;

    if-eqz v0, :cond_4

    check-cast p2, Lcom/squareup/ui/main/errors/CardFailed;

    invoke-virtual {p2}, Lcom/squareup/ui/main/errors/CardFailed;->getFailureReason()Lcom/squareup/ui/main/errors/PaymentError;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->updateToast(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/ui/main/errors/PaymentError;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1

    .line 469
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot process event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 469
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final processSwipeAndChangeState(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            ">;"
        }
    .end annotation

    .line 478
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getSwipeValidator$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/swipe/SwipeValidator;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/swipe/SwipeValidator;->swipeHasEnoughData(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    invoke-direct {p0, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)Lcom/squareup/tenderpayment/TenderPaymentResult;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    goto :goto_0

    .line 481
    :cond_0
    sget-object p2, Lcom/squareup/ui/main/errors/SwipeStraight;->INSTANCE:Lcom/squareup/ui/main/errors/SwipeStraight;

    check-cast p2, Lcom/squareup/ui/main/errors/PaymentError;

    invoke-direct {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->updateToast(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/ui/main/errors/PaymentError;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final shouldPromptForTip()Z
    .locals 4

    .line 446
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    const-string v3, "transaction.requireTippingPayment()"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 449
    :goto_1
    invoke-direct {p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->tipScreenEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method private final shouldShowBuyerCart(Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;)Z
    .locals 1

    .line 435
    invoke-virtual {p1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;->getSkipCart()Z

    move-result p1

    if-nez p1, :cond_1

    .line 434
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getCustomerCheckoutSettings$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->isSkipCartScreenEnabled()Z

    move-result p1

    if-nez p1, :cond_1

    .line 435
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getFeatures$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/settings/server/Features;

    move-result-object p1

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->ALWAYS_SHOW_ITEMIZED_CART:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-virtual {p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->onlyHaveOneCustomItemInCart()Z

    move-result p1

    if-nez p1, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final stopWithProcessContactless(Lcom/squareup/ui/main/SmartPaymentResult;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/SmartPaymentResult;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;",
            ">;"
        }
    .end annotation

    .line 589
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    new-instance v2, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;

    invoke-direct {v2, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;-><init>(Lcom/squareup/ui/main/SmartPaymentResult;)V

    check-cast v2, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-direct {v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private final stopWithProcessEmvDip(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;",
            ">;"
        }
    .end annotation

    .line 571
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    const-string v1, "tenderInEdit.requireSmartCardTender()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->setCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 577
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTenderInEdit$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/TenderInEdit;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 578
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 580
    :cond_0
    sget-object p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;

    move-object v0, p1

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    goto :goto_0

    .line 583
    :cond_1
    new-instance v0, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;

    invoke-direct {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResult;

    .line 585
    :goto_0
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    invoke-direct {v1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    invoke-direct {p1, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method private final stopWithReaderIssue(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;",
            ">;"
        }
    .end annotation

    .line 593
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    new-instance v2, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;

    invoke-direct {v2, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;-><init>(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    check-cast v2, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-direct {v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method private final tipScreenEnabled()Z
    .locals 2

    .line 439
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getFeatures$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/settings/server/Features;

    move-result-object v0

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 440
    iget-object v1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final updateToast(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/ui/main/errors/PaymentError;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/ui/main/errors/PaymentError;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            ">;"
        }
    .end annotation

    .line 490
    instance-of v0, p2, Lcom/squareup/ui/main/errors/PaymentOutOfRange;

    if-eqz v0, :cond_0

    .line 491
    iget-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getPaymentHudToaster$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/PaymentHudToaster;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getTransaction$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastPaymentOutOfRange(Lcom/squareup/payment/Transaction;)Z

    goto :goto_0

    .line 493
    :cond_0
    instance-of v0, p2, Lcom/squareup/ui/main/errors/PaymentOutOfRangeGiftCard;

    if-eqz v0, :cond_1

    .line 494
    iget-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getPaymentHudToaster$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/payment/PaymentHudToaster;

    move-result-object p2

    .line 495
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {v0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getSettings$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v0

    const-string v1, "settings.giftCardSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/GiftCardSettings;->getGiftCardTransactionMinimum()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 494
    invoke-virtual {p2, v0}, Lcom/squareup/payment/PaymentHudToaster;->toastOutOfRangeGiftCard(Ljava/lang/Long;)Z

    goto :goto_0

    .line 498
    :cond_1
    instance-of v0, p2, Lcom/squareup/ui/main/errors/SwipeStraight;

    if-eqz v0, :cond_2

    .line 499
    iget-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getHudToaster$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/hudtoaster/HudToaster;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    check-cast v0, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    invoke-interface {p2, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    goto :goto_0

    .line 501
    :cond_2
    instance-of p2, p2, Lcom/squareup/ui/main/errors/TryAgain;

    if-eqz p2, :cond_3

    .line 502
    iget-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getHudToaster$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/hudtoaster/HudToaster;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    check-cast v0, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;

    invoke-interface {p2, v0}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    .line 506
    :cond_3
    :goto_0
    new-instance p2, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {p2, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p2
.end method


# virtual methods
.method public final getPaymentInputHandler()Lcom/squareup/ui/main/errors/PaymentInputHandler;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    return-object v0
.end method

.method public launch(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/Reactor$DefaultImpls;->launch(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->launch(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    if-eqz v0, :cond_0

    move-object p2, p1

    check-cast p2, Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;

    invoke-direct {p0, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleStarting(Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 190
    :cond_0
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    if-eqz v0, :cond_1

    move-object p2, p1

    check-cast p2, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    invoke-direct {p0, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleStopping(Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 192
    :cond_1
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    if-eqz v0, :cond_2

    move-object p2, p1

    check-cast p2, Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;

    invoke-direct {p0, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleWaitingForCancelPermission(Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 194
    :cond_2
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    if-eqz v0, :cond_3

    move-object p3, p1

    check-cast p3, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-direct {p0, p3, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleInBuyerCart(Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 196
    :cond_3
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    if-eqz v0, :cond_4

    move-object p3, p1

    check-cast p3, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    invoke-direct {p0, p3, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleInPaymentPrompt(Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 198
    :cond_4
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    if-eqz v0, :cond_5

    move-object p2, p1

    check-cast p2, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    invoke-direct {p0, p2}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleStartTipping(Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 200
    :cond_5
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;

    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleInTipping(Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p2

    goto :goto_0

    .line 202
    :cond_6
    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    if-eqz v0, :cond_7

    move-object v0, p1

    check-cast v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;

    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleInLanguageSelection(Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p2

    .line 205
    :goto_0
    new-instance p3, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$onReact$1;

    invoke-direct {p3, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$onReact$1;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState;)V

    check-cast p3, Lio/reactivex/functions/Consumer;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "nextReaction.doOnSuccess\u2026      }\n        }\n      }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 202
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->onReact(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
