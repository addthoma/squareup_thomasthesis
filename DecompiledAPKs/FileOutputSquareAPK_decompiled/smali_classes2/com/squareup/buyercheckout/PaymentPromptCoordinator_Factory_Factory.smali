.class public final Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "PaymentPromptCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final formattedTotalProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionTypeDisplayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/TransactionTypeDisplay;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/TransactionTypeDisplay;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->formattedTotalProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->transactionTypeDisplayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/FormattedTotalProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/TransactionTypeDisplay;",
            ">;)",
            "Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;
    .locals 1

    .line 54
    new-instance v0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;-><init>(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->formattedTotalProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/FormattedTotalProvider;

    iget-object v1, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v2, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v3, p0, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->transactionTypeDisplayProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/buyercheckout/TransactionTypeDisplay;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->newInstance(Lcom/squareup/ui/buyer/FormattedTotalProvider;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/buyercheckout/TransactionTypeDisplay;)Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/buyercheckout/PaymentPromptCoordinator_Factory_Factory;->get()Lcom/squareup/buyercheckout/PaymentPromptCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
