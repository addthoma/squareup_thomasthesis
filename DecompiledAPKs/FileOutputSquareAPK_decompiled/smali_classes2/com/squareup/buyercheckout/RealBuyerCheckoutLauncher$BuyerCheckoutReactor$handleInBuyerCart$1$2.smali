.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/buyercheckout/OnConfirmAndPayClicked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "it",
        "Lcom/squareup/buyercheckout/OnConfirmAndPayClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/buyercheckout/OnConfirmAndPayClicked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/OnConfirmAndPayClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    iget-object p1, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    iget-object p1, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    invoke-static {p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->access$getAnalytics$p(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ConfirmAndPayClicked;->INSTANCE:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ConfirmAndPayClicked;

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 282
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    iget-object p1, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    invoke-static {p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->access$shouldPromptForTip(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 283
    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    goto :goto_0

    .line 285
    :cond_0
    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    iget-object v0, v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    invoke-virtual {v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;->getData()Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;-><init>(Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;)V

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 287
    :goto_0
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/buyercheckout/OnConfirmAndPayClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$2;->invoke(Lcom/squareup/buyercheckout/OnConfirmAndPayClicked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
