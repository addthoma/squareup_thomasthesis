.class public abstract Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "BuyerCheckoutEventStreamEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ConfirmAndPayClicked;,
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ExitBuyerCart;,
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ExitPaymentPrompt;,
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewBuyerCart;,
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;,
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u0007\u0008\t\n\u000b\u000cB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u0082\u0001\u0006\r\u000e\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "name",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "value",
        "",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V",
        "ConfirmAndPayClicked",
        "EnterBuyerCheckout",
        "ExitBuyerCart",
        "ExitPaymentPrompt",
        "ViewBuyerCart",
        "ViewPaymentPrompt",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ConfirmAndPayClicked;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ExitBuyerCart;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ExitPaymentPrompt;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewBuyerCart;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$ViewPaymentPrompt;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
