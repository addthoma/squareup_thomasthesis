.class public final Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
.super Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;
.source "BuyerCheckoutEventStreamEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EnterBuyerCheckout"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0008\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\t\u001a\u00020\u0003H\u00c2\u0003J\t\u0010\n\u001a\u00020\u0006H\u00c2\u0003J\'\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\u00032\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0006H\u00d6\u0001R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;",
        "is_default_to_customer_checkout_enabled",
        "",
        "is_skip_cart_screen_enabled",
        "cart_items",
        "",
        "(ZZLjava/lang/String;)V",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;


# instance fields
.field private final cart_items:Ljava/lang/String;

.field private final is_default_to_customer_checkout_enabled:Z

.field private final is_skip_cart_screen_enabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;

    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;)V
    .locals 3

    const-string v0, "cart_items"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CUSTOMER_CHECKOUT_ENTER:Lcom/squareup/analytics/RegisterActionName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    const-string v2, "CUSTOMER_CHECKOUT_ENTER.value"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    iput-boolean p2, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    iput-object p3, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    return-void
.end method

.method private final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    return v0
.end method

.method private final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    return v0
.end method

.method private final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;ZZLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->copy(ZZLjava/lang/String;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Ljava/util/List;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/DisplayItem;",
            ">;)",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout$Companion;->of(Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Ljava/util/List;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZZLjava/lang/String;)Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;
    .locals 1

    const-string v0, "cart_items"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;-><init>(ZZLjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;

    iget-boolean v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    iget-boolean v1, p1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    iget-boolean v1, p1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    move v1, v2

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnterBuyerCheckout(is_default_to_customer_checkout_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_default_to_customer_checkout_enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", is_skip_cart_screen_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->is_skip_cart_screen_enabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cart_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/buyercheckout/BuyerCheckoutEventStreamEvent$EnterBuyerCheckout;->cart_items:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
