.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/buyercheckout/OnBackPressed;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
        "it",
        "Lcom/squareup/buyercheckout/OnBackPressed;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$4;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/buyercheckout/OnBackPressed;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/OnBackPressed;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 297
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$4;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    iget-object v0, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    .line 298
    iget-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$4;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;

    iget-object p1, p1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;

    move-object v1, p1

    check-cast v1, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    .line 299
    new-instance p1, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;

    sget-object v2, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    check-cast v2, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-direct {p1, v2}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;-><init>(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/buyercheckout/BuyerCheckoutState;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 297
    invoke-static/range {v0 .. v5}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->enterStateWaitingForCancelPermission$default(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/eventstream/v1/EventStreamEvent;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/buyercheckout/OnBackPressed;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInBuyerCart$1$4;->invoke(Lcom/squareup/buyercheckout/OnBackPressed;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
