.class public final Lcom/squareup/chartography/util/D3Util;
.super Ljava/lang/Object;
.source "D3Util.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nD3Util.kt\nKotlin\n*S Kotlin\n*F\n+ 1 D3Util.kt\ncom/squareup/chartography/util/D3Util\n+ 2 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,86:1\n37#2,2:87\n*E\n*S KotlinDebug\n*F\n+ 1 D3Util.kt\ncom/squareup/chartography/util/D3Util\n*L\n40#1,2:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0003\u001a\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J1\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000c2\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/chartography/util/D3Util;",
        "",
        "()V",
        "step",
        "",
        "range",
        "Lkotlin/ranges/ClosedRange;",
        "count",
        "",
        "allowFractionalSteps",
        "",
        "steps",
        "",
        "(Lkotlin/ranges/ClosedRange;IZ)[Ljava/lang/Double;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/chartography/util/D3Util;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/chartography/util/D3Util;

    invoke-direct {v0}, Lcom/squareup/chartography/util/D3Util;-><init>()V

    sput-object v0, Lcom/squareup/chartography/util/D3Util;->INSTANCE:Lcom/squareup/chartography/util/D3Util;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final step(Lkotlin/ranges/ClosedRange;IZ)D
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/ranges/ClosedRange<",
            "Ljava/lang/Double;",
            ">;IZ)D"
        }
    .end annotation

    .line 48
    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getStart()Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getEndInclusive()Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    cmpg-double v4, v0, v2

    if-nez v4, :cond_0

    const-wide/16 p1, 0x0

    return-wide p1

    :cond_0
    const/4 v0, 0x1

    .line 52
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 54
    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getEndInclusive()Ljava/lang/Comparable;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v1

    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getStart()Ljava/lang/Comparable;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v3

    sub-double/2addr v1, v3

    int-to-double p1, p2

    div-double/2addr v1, p1

    if-nez p3, :cond_1

    int-to-double p1, v0

    rem-double p1, v1, p1

    const/4 p3, 0x0

    int-to-double v3, p3

    cmpl-double p3, p1, v3

    if-lez p3, :cond_1

    .line 56
    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    .line 59
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Math;->log10(D)D

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide p1

    double-to-int p1, p1

    const-wide/high16 p2, 0x4024000000000000L    # 10.0

    if-lez p1, :cond_2

    int-to-double v3, p1

    .line 63
    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    goto :goto_0

    :cond_2
    int-to-double v3, p1

    neg-double v3, v3

    .line 68
    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    int-to-double v5, v0

    div-double v3, v5, v3

    :goto_0
    div-double/2addr v1, v3

    const-wide/high16 v5, 0x4049000000000000L    # 50.0

    .line 79
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    cmpl-double p1, v1, v5

    if-ltz p1, :cond_3

    const/16 p1, 0xa

    :goto_1
    int-to-double p1, p1

    mul-double v3, v3, p1

    goto :goto_2

    .line 80
    :cond_3
    invoke-static {p2, p3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    cmpl-double p3, v1, p1

    if-ltz p3, :cond_4

    const/4 p1, 0x5

    goto :goto_1

    :cond_4
    const-wide/high16 p1, 0x4000000000000000L    # 2.0

    .line 81
    invoke-static {p1, p2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide p1

    cmpl-double p3, v1, p1

    if-lez p3, :cond_5

    const/4 p1, 0x2

    goto :goto_1

    :cond_5
    :goto_2
    return-wide v3
.end method

.method public static synthetic steps$default(Lcom/squareup/chartography/util/D3Util;Lkotlin/ranges/ClosedRange;IZILjava/lang/Object;)[Ljava/lang/Double;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 23
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/chartography/util/D3Util;->steps(Lkotlin/ranges/ClosedRange;IZ)[Ljava/lang/Double;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final steps(Lkotlin/ranges/ClosedRange;IZ)[Ljava/lang/Double;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/ranges/ClosedRange<",
            "Ljava/lang/Double;",
            ">;IZ)[",
            "Ljava/lang/Double;"
        }
    .end annotation

    const-string v0, "range"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getStart()Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getEndInclusive()Ljava/lang/Comparable;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    const/4 v4, 0x0

    cmpg-double v5, v0, v2

    if-nez v5, :cond_0

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Double;

    .line 26
    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getStart()Ljava/lang/Comparable;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    aput-object p1, p2, v4

    return-object p2

    .line 29
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/chartography/util/D3Util;->step(Lkotlin/ranges/ClosedRange;IZ)D

    move-result-wide p2

    .line 30
    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getStart()Ljava/lang/Comparable;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    div-double/2addr v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    mul-double v0, v0, p2

    .line 31
    invoke-interface {p1}, Lkotlin/ranges/ClosedRange;->getEndInclusive()Ljava/lang/Comparable;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    div-double/2addr v2, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    mul-double v2, v2, p2

    const/4 p1, 0x2

    int-to-double v5, p1

    div-double v5, p2, v5

    add-double/2addr v2, v5

    .line 33
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    :goto_0
    cmpg-double v5, v0, v2

    if-gtz v5, :cond_1

    .line 36
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-double/2addr v0, p2

    goto :goto_0

    .line 40
    :cond_1
    check-cast p1, Ljava/util/Collection;

    new-array p2, v4, [Ljava/lang/Double;

    .line 88
    invoke-interface {p1, p2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, [Ljava/lang/Double;

    return-object p1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
