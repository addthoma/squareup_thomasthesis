.class public final Lcom/squareup/chartography/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/chartography/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final axisColor:I = 0x7f040040

.field public static final axisStrokeWidth:I = 0x7f040041

.field public static final barSpacing:I = 0x7f040055

.field public static final dataColors:I = 0x7f040114

.field public static final dataSelectedColors:I = 0x7f040115

.field public static final dotSize:I = 0x7f040133

.field public static final hideFirstYLabel:I = 0x7f0401b8

.field public static final lineSize:I = 0x7f040280

.field public static final stepSpacing:I = 0x7f0403f6

.field public static final textColor:I = 0x7f040446

.field public static final tickSize:I = 0x7f04045a

.field public static final xLabelFrequency:I = 0x7f0404a4

.field public static final xLabelMinGap:I = 0x7f0404a5

.field public static final xLabelSize:I = 0x7f0404a6

.field public static final xLabelTickSpacing:I = 0x7f0404a7

.field public static final yLabelSize:I = 0x7f0404a9

.field public static final yLabelSpacing:I = 0x7f0404aa


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
