.class public final Lcom/squareup/accountfreeze/FreezeAnalyticsKt;
.super Ljava/lang/Object;
.source "FreezeAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u001a\u000c\u0010\u0008\u001a\u00020\t*\u00020\nH\u0000\u001a\u000c\u0010\u000b\u001a\u00020\t*\u00020\nH\u0000\u001a\u000c\u0010\u000c\u001a\u00020\t*\u00020\nH\u0000\u001a\u000c\u0010\r\u001a\u00020\t*\u00020\nH\u0000\u001a\u000c\u0010\u000e\u001a\u00020\t*\u00020\nH\u0000\u001a\u000c\u0010\u000f\u001a\u00020\t*\u00020\nH\u0000\u001a\u000c\u0010\u0010\u001a\u00020\t*\u00020\nH\u0000\u001a\u0014\u0010\u0011\u001a\u00020\t*\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u0013\u001a\u00020\t*\u00020\n2\u0006\u0010\u0014\u001a\u00020\u0001H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "CLICK_BANNER",
        "",
        "CLICK_CLOSE_BANNER",
        "CLICK_MODAL_LATER",
        "CLICK_MODAL_VERIFY",
        "CLICK_NOTIFICATION",
        "VIEW_BANNER",
        "VIEW_NOTIFICATION",
        "onClickedBanner",
        "",
        "Lcom/squareup/analytics/Analytics;",
        "onClickedNotification",
        "onClosedBanner",
        "onCreatedNotification",
        "onModalLater",
        "onModalVerify",
        "onViewedBanner",
        "sendClick",
        "description",
        "sendView",
        "to",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CLICK_BANNER:Ljava/lang/String; = "Account Freeze Banner: Click Banner"

.field private static final CLICK_CLOSE_BANNER:Ljava/lang/String; = "Account Freeze Banner: Close Banner"

.field private static final CLICK_MODAL_LATER:Ljava/lang/String; = "Account Freeze Deposits Suspended Modal: Later"

.field private static final CLICK_MODAL_VERIFY:Ljava/lang/String; = "Account Freeze Deposits Suspended Modal: Verify Account"

.field private static final CLICK_NOTIFICATION:Ljava/lang/String; = "Account Freeze Push Notification: Click"

.field private static final VIEW_BANNER:Ljava/lang/String; = "Account Freeze Banner: View Banner"

.field private static final VIEW_NOTIFICATION:Ljava/lang/String; = "Account Freeze Push Notification: Create"


# direct methods
.method public static final onClickedBanner(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedBanner"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Banner: Click Banner"

    .line 33
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClickedNotification(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClickedNotification"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Push Notification: Click"

    .line 25
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onClosedBanner(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onClosedBanner"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Banner: Close Banner"

    .line 29
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onCreatedNotification(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onCreatedNotification"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Push Notification: Create"

    .line 17
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onModalLater(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onModalLater"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Deposits Suspended Modal: Later"

    .line 37
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onModalVerify(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onModalVerify"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Deposits Suspended Modal: Verify Account"

    .line 41
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method public static final onViewedBanner(Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "$this$onViewedBanner"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Account Freeze Banner: View Banner"

    .line 21
    invoke-static {p0, v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V

    return-void
.end method

.method private static final sendClick(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/analytics/event/ClickEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method private static final sendView(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/analytics/event/ViewEvent;

    invoke-direct {v0, p1}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
