.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "AccountFreezeTutorialCreator.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\u0013B\u001d\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0006\u0010\u0010\u001a\u00020\rJ\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0012H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0008\u001a\u0010\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\n0\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "accountFreeze",
        "Lcom/squareup/accountfreeze/AccountFreeze;",
        "tutorialProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
        "(Lcom/squareup/accountfreeze/AccountFreeze;Ljavax/inject/Provider;)V",
        "seeds",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "kotlin.jvm.PlatformType",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onFreezeTutorialAborted",
        "triggeredTutorial",
        "Lio/reactivex/Observable;",
        "Seed",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

.field private final seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/accountfreeze/AccountFreeze;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountFreeze"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->tutorialProvider:Ljavax/inject/Provider;

    .line 21
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<TutorialSeed>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getSeeds$p(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getTutorialProvider$p(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;)Ljavax/inject/Provider;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->tutorialProvider:Ljavax/inject/Provider;

    return-object p0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->accountFreeze:Lcom/squareup/accountfreeze/AccountFreeze;

    invoke-interface {v0}, Lcom/squareup/accountfreeze/AccountFreeze;->canShowBanner()Lio/reactivex/Observable;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "accountFreeze.canShowBan\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$onEnterScope$1;-><init>(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final onFreezeTutorialAborted()V
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;

    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->tutorialProvider:Ljavax/inject/Provider;

    invoke-direct {v1, v2}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;-><init>(Ljavax/inject/Provider;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public triggeredTutorial()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialSeed;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;->seeds:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
