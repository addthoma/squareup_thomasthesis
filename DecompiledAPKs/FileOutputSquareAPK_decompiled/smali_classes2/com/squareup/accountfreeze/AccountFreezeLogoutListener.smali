.class public final Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;
.super Ljava/lang/Object;
.source "AccountFreezeLogoutListener.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;",
        "Lmortar/Scoped;",
        "accountFreezeNotificationScheduler",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
        "lastDismissedBanner",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "lastDismissedNotification",
        "(Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

.field private final lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 1
    .param p2    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeBanner;
        .end annotation
    .end param
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeNotification;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountFreezeNotificationScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastDismissedBanner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastDismissedNotification"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;->accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;->lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;->lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;->lastDismissedBanner:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->delete()V

    .line 25
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;->lastDismissedNotification:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->delete()V

    .line 26
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;->accountFreezeNotificationScheduler:Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;

    invoke-virtual {v0}, Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;->unscheduleNotification()I

    return-void
.end method
