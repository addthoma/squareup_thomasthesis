.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;
.super Lcom/squareup/tutorialv2/TutorialSeed;
.source "AccountFreezeTutorialCreator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Seed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0007H\u0014R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;",
        "Lcom/squareup/tutorialv2/TutorialSeed;",
        "tutorialProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
        "(Ljavax/inject/Provider;)V",
        "doCreate",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;)V"
        }
    .end annotation

    const-string v0, "tutorialProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lcom/squareup/tutorialv2/TutorialSeed$Priority;->AUTO_STARTED:Lcom/squareup/tutorialv2/TutorialSeed$Priority;

    invoke-direct {p0, v0}, Lcom/squareup/tutorialv2/TutorialSeed;-><init>(Lcom/squareup/tutorialv2/TutorialSeed$Priority;)V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;->tutorialProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method protected doCreate()Lcom/squareup/tutorialv2/Tutorial;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator$Seed;->tutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "tutorialProvider.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/tutorialv2/Tutorial;

    return-object v0
.end method
