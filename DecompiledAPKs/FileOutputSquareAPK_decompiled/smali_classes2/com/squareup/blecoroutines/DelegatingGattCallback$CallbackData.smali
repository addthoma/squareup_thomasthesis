.class public abstract Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;
.super Ljava/lang/Object;
.source "DelegatingGattCallback.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blecoroutines/DelegatingGattCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CallbackData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;,
        Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnMtuChanged;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0008\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
        "",
        "()V",
        "gatt",
        "Landroid/bluetooth/BluetoothGatt;",
        "getGatt",
        "()Landroid/bluetooth/BluetoothGatt;",
        "OnCharacteristicChanged",
        "OnCharacteristicRead",
        "OnCharacteristicWrite",
        "OnConnectionStateChanged",
        "OnDescriptorWrite",
        "OnMtuChanged",
        "OnReadRemoteRssi",
        "OnServicesDiscovered",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnServicesDiscovered;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicRead;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicWrite;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnCharacteristicChanged;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnDescriptorWrite;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnReadRemoteRssi;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnMtuChanged;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getGatt()Landroid/bluetooth/BluetoothGatt;
.end method
