.class final Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;
.super Lkotlin/jvm/internal/Lambda;
.source "GattCallbackBroker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blecoroutines/GattCallbackBroker;->runAndWaitForCallback(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "T",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
        "action",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $clazz:Lkotlin/reflect/KClass;

.field final synthetic $deferredGattCallback:Lkotlinx/coroutines/CompletableDeferred;

.field final synthetic this$0:Lcom/squareup/blecoroutines/GattCallbackBroker;


# direct methods
.method constructor <init>(Lcom/squareup/blecoroutines/GattCallbackBroker;Lkotlin/reflect/KClass;Lkotlinx/coroutines/CompletableDeferred;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->this$0:Lcom/squareup/blecoroutines/GattCallbackBroker;

    iput-object p2, p0, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->$clazz:Lkotlin/reflect/KClass;

    iput-object p3, p0, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->$deferredGattCallback:Lkotlinx/coroutines/CompletableDeferred;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;

    invoke-virtual {p0, p1}, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->invoke(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;)V
    .locals 2

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->$clazz:Lkotlin/reflect/KClass;

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getJavaClass(Lkotlin/reflect/KClass;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->this$0:Lcom/squareup/blecoroutines/GattCallbackBroker;

    invoke-static {v0}, Lcom/squareup/blecoroutines/GattCallbackBroker;->access$unsetDelegate(Lcom/squareup/blecoroutines/GattCallbackBroker;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/blecoroutines/GattCallbackBroker$runAndWaitForCallback$3;->$deferredGattCallback:Lkotlinx/coroutines/CompletableDeferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/CompletableDeferred;->complete(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "unhandled: %s"

    .line 77
    invoke-static {p1, v0}, Ltimber/log/Timber;->i(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
