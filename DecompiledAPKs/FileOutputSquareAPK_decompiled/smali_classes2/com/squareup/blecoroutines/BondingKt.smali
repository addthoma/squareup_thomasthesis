.class public final Lcom/squareup/blecoroutines/BondingKt;
.super Ljava/lang/Object;
.source "Bonding.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBonding.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Bonding.kt\ncom/squareup/blecoroutines/BondingKt\n*L\n1#1,118:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u001a%\u0010\u0002\u001a\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\t\u001a%\u0010\n\u001a\u00020\u0003*\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\t\u001a\n\u0010\u000b\u001a\u00020\u000c*\u00020\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\r"
    }
    d2 = {
        "EXTRA_REASON",
        "",
        "createBond",
        "",
        "Landroid/bluetooth/BluetoothDevice;",
        "timeoutMs",
        "",
        "context",
        "Landroid/content/Context;",
        "(Landroid/bluetooth/BluetoothDevice;JLandroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "removeBond",
        "removeBondUsingReflection",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final EXTRA_REASON:Ljava/lang/String; = "android.bluetooth.device.extra.REASON"


# direct methods
.method public static final createBond(Landroid/bluetooth/BluetoothDevice;JLandroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "J",
            "Landroid/content/Context;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-wide/from16 v9, p1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    instance-of v2, v1, Lcom/squareup/blecoroutines/BondingKt$createBond$1;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/squareup/blecoroutines/BondingKt$createBond$1;

    iget v3, v2, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->label:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget v1, v2, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->label:I

    sub-int/2addr v1, v4

    iput v1, v2, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/blecoroutines/BondingKt$createBond$1;

    invoke-direct {v2, v1}, Lcom/squareup/blecoroutines/BondingKt$createBond$1;-><init>(Lkotlin/coroutines/Continuation;)V

    :goto_0
    move-object v11, v2

    iget-object v1, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v12

    .line 28
    iget v2, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->label:I

    const/4 v13, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v13, :cond_1

    iget-object v0, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$6:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/BondingReceiver;

    iget-object v0, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$5:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Throwable;

    iget-object v2, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$4:Ljava/lang/Object;

    check-cast v2, Ljava/io/Closeable;

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$3:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/blecoroutines/BondingReceiver;

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$2:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/CompletableDeferred;

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$1:Ljava/lang/Object;

    check-cast v3, Landroid/content/Context;

    iget-wide v3, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->J$0:J

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$0:Ljava/lang/Object;

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    :try_start_0
    invoke-static {v1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v13, v0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v12, v2

    goto/16 :goto_3

    .line 41
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_2
    invoke-static {v1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 32
    invoke-static {v1, v13, v1}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v14

    .line 33
    new-instance v15, Lcom/squareup/blecoroutines/BondingReceiver;

    invoke-virtual/range {p0 .. p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    const-string v3, "address"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v15, v14, v0, v2}, Lcom/squareup/blecoroutines/BondingReceiver;-><init>(Lkotlinx/coroutines/CompletableDeferred;Landroid/content/Context;Ljava/lang/String;)V

    .line 34
    move-object v8, v15

    check-cast v8, Ljava/io/Closeable;

    move-object v6, v1

    check-cast v6, Ljava/lang/Throwable;

    :try_start_1
    move-object v7, v8

    check-cast v7, Lcom/squareup/blecoroutines/BondingReceiver;

    .line 35
    invoke-virtual {v15}, Lcom/squareup/blecoroutines/BondingReceiver;->register()V

    .line 36
    invoke-virtual/range {p0 .. p0}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 38
    new-instance v16, Lcom/squareup/blecoroutines/BondingKt$createBond$$inlined$use$lambda$1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const/4 v2, 0x0

    move-object/from16 v1, v16

    move-object/from16 v3, p0

    move-object v4, v15

    move-object v5, v11

    move-object v13, v6

    move-object/from16 v17, v7

    move-wide/from16 v6, p1

    move-object/from16 v18, v12

    move-object v12, v8

    move-object v8, v14

    :try_start_2
    invoke-direct/range {v1 .. v8}, Lcom/squareup/blecoroutines/BondingKt$createBond$$inlined$use$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/blecoroutines/BondingReceiver;Lkotlin/coroutines/Continuation;JLkotlinx/coroutines/CompletableDeferred;)V

    move-object/from16 v1, v16

    check-cast v1, Lkotlin/jvm/functions/Function2;

    move-object/from16 v2, p0

    iput-object v2, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$0:Ljava/lang/Object;

    iput-wide v9, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->J$0:J

    iput-object v0, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$1:Ljava/lang/Object;

    iput-object v14, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$2:Ljava/lang/Object;

    iput-object v15, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$3:Ljava/lang/Object;

    iput-object v12, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$4:Ljava/lang/Object;

    iput-object v13, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$5:Ljava/lang/Object;

    move-object/from16 v8, v17

    iput-object v8, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->L$6:Ljava/lang/Object;

    const/4 v0, 0x1

    iput v0, v11, Lcom/squareup/blecoroutines/BondingKt$createBond$1;->label:I

    invoke-static {v9, v10, v1, v11}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, v18

    if-ne v1, v0, :cond_3

    return-object v0

    :cond_3
    move-object v2, v12

    .line 28
    :goto_1
    :try_start_3
    move-object v9, v1

    check-cast v9, Lcom/squareup/blecoroutines/BondResult;

    .line 39
    invoke-virtual {v9}, Lcom/squareup/blecoroutines/BondResult;->getNewState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_4

    .line 40
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 34
    invoke-static {v2, v13}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 41
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0

    .line 39
    :cond_4
    :try_start_4
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    sget-object v4, Lcom/squareup/blecoroutines/Event;->CREATE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x5e

    const/4 v12, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v12}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_5
    move-object v12, v8

    .line 36
    :try_start_5
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    sget-object v14, Lcom/squareup/blecoroutines/Event;->CREATE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x7e

    const/16 v22, 0x0

    move-object v13, v0

    invoke-direct/range {v13 .. v22}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v12, v8

    :goto_2
    move-object v1, v0

    .line 34
    :goto_3
    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception v0

    move-object v2, v0

    invoke-static {v12, v1}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static final removeBond(Landroid/bluetooth/BluetoothDevice;JLandroid/content/Context;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "J",
            "Landroid/content/Context;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    move-wide/from16 v9, p1

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    instance-of v2, v1, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;

    iget v3, v2, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->label:I

    const/high16 v4, -0x80000000

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    iget v1, v2, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->label:I

    sub-int/2addr v1, v4

    iput v1, v2, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v2, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;

    invoke-direct {v2, v1}, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;-><init>(Lkotlin/coroutines/Continuation;)V

    :goto_0
    move-object v11, v2

    iget-object v1, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v12

    .line 48
    iget v2, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->label:I

    const/4 v13, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v13, :cond_1

    iget-object v0, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$6:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/blecoroutines/BondingReceiver;

    iget-object v0, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$5:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Throwable;

    iget-object v2, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$4:Ljava/lang/Object;

    check-cast v2, Ljava/io/Closeable;

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$3:Ljava/lang/Object;

    check-cast v3, Lcom/squareup/blecoroutines/BondingReceiver;

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$2:Ljava/lang/Object;

    check-cast v3, Lkotlinx/coroutines/CompletableDeferred;

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$1:Ljava/lang/Object;

    check-cast v3, Landroid/content/Context;

    iget-wide v3, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->J$0:J

    iget-object v3, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$0:Ljava/lang/Object;

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    :try_start_0
    invoke-static {v1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v13, v0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v0

    move-object v12, v2

    goto/16 :goto_3

    .line 61
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_2
    invoke-static {v1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 52
    invoke-static {v1, v13, v1}, Lkotlinx/coroutines/CompletableDeferredKt;->CompletableDeferred$default(Lkotlinx/coroutines/Job;ILjava/lang/Object;)Lkotlinx/coroutines/CompletableDeferred;

    move-result-object v14

    .line 53
    new-instance v15, Lcom/squareup/blecoroutines/BondingReceiver;

    invoke-virtual/range {p0 .. p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    const-string v3, "address"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v15, v14, v0, v2}, Lcom/squareup/blecoroutines/BondingReceiver;-><init>(Lkotlinx/coroutines/CompletableDeferred;Landroid/content/Context;Ljava/lang/String;)V

    .line 54
    move-object v8, v15

    check-cast v8, Ljava/io/Closeable;

    move-object v6, v1

    check-cast v6, Ljava/lang/Throwable;

    :try_start_1
    move-object v7, v8

    check-cast v7, Lcom/squareup/blecoroutines/BondingReceiver;

    .line 55
    invoke-virtual {v15}, Lcom/squareup/blecoroutines/BondingReceiver;->register()V

    .line 56
    invoke-static/range {p0 .. p0}, Lcom/squareup/blecoroutines/BondingKt;->removeBondUsingReflection(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 58
    new-instance v16, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    const/4 v2, 0x0

    move-object/from16 v1, v16

    move-object/from16 v3, p0

    move-object v4, v15

    move-object v5, v11

    move-object v13, v6

    move-object/from16 v17, v7

    move-wide/from16 v6, p1

    move-object/from16 v18, v12

    move-object v12, v8

    move-object v8, v14

    :try_start_2
    invoke-direct/range {v1 .. v8}, Lcom/squareup/blecoroutines/BondingKt$removeBond$$inlined$use$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/blecoroutines/BondingReceiver;Lkotlin/coroutines/Continuation;JLkotlinx/coroutines/CompletableDeferred;)V

    move-object/from16 v1, v16

    check-cast v1, Lkotlin/jvm/functions/Function2;

    move-object/from16 v2, p0

    iput-object v2, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$0:Ljava/lang/Object;

    iput-wide v9, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->J$0:J

    iput-object v0, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$1:Ljava/lang/Object;

    iput-object v14, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$2:Ljava/lang/Object;

    iput-object v15, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$3:Ljava/lang/Object;

    iput-object v12, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$4:Ljava/lang/Object;

    iput-object v13, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$5:Ljava/lang/Object;

    move-object/from16 v8, v17

    iput-object v8, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->L$6:Ljava/lang/Object;

    const/4 v0, 0x1

    iput v0, v11, Lcom/squareup/blecoroutines/BondingKt$removeBond$1;->label:I

    invoke-static {v9, v10, v1, v11}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, v18

    if-ne v1, v0, :cond_3

    return-object v0

    :cond_3
    move-object v2, v12

    .line 48
    :goto_1
    :try_start_3
    move-object v9, v1

    check-cast v9, Lcom/squareup/blecoroutines/BondResult;

    .line 59
    invoke-virtual {v9}, Lcom/squareup/blecoroutines/BondResult;->getNewState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_4

    .line 60
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 54
    invoke-static {v2, v13}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 61
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0

    .line 59
    :cond_4
    :try_start_4
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    sget-object v4, Lcom/squareup/blecoroutines/Event;->REMOVE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x5e

    const/4 v12, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v12}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_5
    move-object v12, v8

    .line 56
    :try_start_5
    new-instance v0, Lcom/squareup/blecoroutines/BleError;

    sget-object v14, Lcom/squareup/blecoroutines/Event;->REMOVE_BOND_FAILED:Lcom/squareup/blecoroutines/Event;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x7e

    const/16 v22, 0x0

    move-object v13, v0

    invoke-direct/range {v13 .. v22}, Lcom/squareup/blecoroutines/BleError;-><init>(Lcom/squareup/blecoroutines/Event;Ljava/util/UUID;Ljava/util/UUID;Ljava/util/UUID;Ljava/lang/Integer;Lcom/squareup/blecoroutines/BondResult;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_2

    :catchall_2
    move-exception v0

    move-object v12, v8

    :goto_2
    move-object v1, v0

    .line 54
    :goto_3
    :try_start_6
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    :catchall_3
    move-exception v0

    move-object v2, v0

    invoke-static {v12, v1}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public static final removeBondUsingReflection(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 4

    const-string v0, "$this$removeBondUsingReflection"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 65
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "removeBond"

    new-array v3, v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const-string v2, "javaClass.getMethod(\"removeBond\")"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v2, v0, [Ljava/lang/Object;

    .line 66
    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Boolean"

    invoke-direct {p0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return v0
.end method
