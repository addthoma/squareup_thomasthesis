.class public final Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;
.super Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;
.source "DelegatingGattCallback.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnConnectionStateChanged"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\t2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;",
        "Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;",
        "gatt",
        "Landroid/bluetooth/BluetoothGatt;",
        "status",
        "",
        "newState",
        "(Landroid/bluetooth/BluetoothGatt;II)V",
        "connected",
        "",
        "getConnected",
        "()Z",
        "getGatt",
        "()Landroid/bluetooth/BluetoothGatt;",
        "getNewState",
        "()I",
        "getStatus",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connected:Z

.field private final gatt:Landroid/bluetooth/BluetoothGatt;

.field private final newState:I

.field private final status:I


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 1

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, v0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->gatt:Landroid/bluetooth/BluetoothGatt;

    iput p2, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    iput p3, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    .line 46
    iget p1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    const/4 p2, 0x2

    if-ne p1, p2, :cond_0

    iget p1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->connected:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;Landroid/bluetooth/BluetoothGatt;IIILjava/lang/Object;)Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->copy(Landroid/bluetooth/BluetoothGatt;II)Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Landroid/bluetooth/BluetoothGatt;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    return v0
.end method

.method public final copy(Landroid/bluetooth/BluetoothGatt;II)Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;
    .locals 1

    const-string v0, "gatt"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;-><init>(Landroid/bluetooth/BluetoothGatt;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;

    invoke-virtual {p0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    iget v1, p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    iget p1, p1, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConnected()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->connected:Z

    return v0
.end method

.method public getGatt()Landroid/bluetooth/BluetoothGatt;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->gatt:Landroid/bluetooth/BluetoothGatt;

    return-object v0
.end method

.method public final getNewState()I
    .locals 1

    .line 44
    iget v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    return v0
.end method

.method public final getStatus()I
    .locals 1

    .line 43
    iget v0, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnConnectionStateChanged(gatt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->getGatt()Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->status:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", newState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blecoroutines/DelegatingGattCallback$CallbackData$OnConnectionStateChanged;->newState:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
