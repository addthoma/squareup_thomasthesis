.class public final Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;
.super Ljava/lang/Object;
.source "AnrChaperone.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/anrchaperone/AnrChaperone;->supervise(Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1",
        "Ljava/lang/Runnable;",
        "run",
        "",
        "anr-chaperone_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/anrchaperone/AnrChaperone;


# direct methods
.method constructor <init>(Lcom/squareup/anrchaperone/AnrChaperone;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 51
    iput-object p1, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 55
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getMainThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Landroid/os/Handler;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getClock$p(Lcom/squareup/anrchaperone/AnrChaperone;)Lcom/squareup/util/Clock;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/squareup/anrchaperone/AnrChaperone;->setMainThreadLastUpdate(J)V

    .line 57
    iget-object v0, p0, Lcom/squareup/anrchaperone/AnrChaperone$supervise$mainThreadTick$1;->this$0:Lcom/squareup/anrchaperone/AnrChaperone;

    invoke-static {v0}, Lcom/squareup/anrchaperone/AnrChaperone;->access$getMainThread$p(Lcom/squareup/anrchaperone/AnrChaperone;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {}, Lcom/squareup/anrchaperone/AnrChaperone;->access$Companion()Lcom/squareup/anrchaperone/AnrChaperone$Companion;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
