.class public Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;
.super Ljava/lang/Object;
.source "TenderStatusCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/papersignature/TenderStatusCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CacheEntry"
.end annotation


# instance fields
.field public final billId:Ljava/lang/String;

.field public final tipAmount:Ljava/lang/Long;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->tipAmount:Ljava/lang/Long;

    return-void
.end method

.method public static fromTenderHistory(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->billId:Lcom/squareup/protos/client/IdPair;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistory;->billId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 58
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_1

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object p0

    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 61
    :goto_1
    new-instance p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;

    invoke-direct {p0, v0, v1}, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_2

    .line 88
    :cond_1
    check-cast p1, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;

    .line 90
    iget-object v2, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v3, p1, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    if-eqz v2, :cond_3

    :goto_0
    return v1

    .line 91
    :cond_3
    iget-object v2, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->tipAmount:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->tipAmount:Ljava/lang/Long;

    if-eqz v2, :cond_4

    invoke-virtual {v2, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    goto :goto_1

    :cond_4
    if-eqz p1, :cond_5

    :goto_1
    return v1

    :cond_5
    return v0

    :cond_6
    :goto_2
    return v1
.end method

.method public hashCode()I
    .locals 3

    .line 99
    iget-object v0, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 100
    iget-object v2, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->tipAmount:Ljava/lang/Long;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CacheEntry{billId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->billId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", tipAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/papersignature/TenderStatusCache$CacheEntry;->tipAmount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
