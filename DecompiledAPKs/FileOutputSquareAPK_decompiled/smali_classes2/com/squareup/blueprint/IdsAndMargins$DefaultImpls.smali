.class public final Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;
.super Ljava/lang/Object;
.source "IdsAndMargins.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blueprint/IdsAndMargins;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static connectTo(Lcom/squareup/blueprint/IdsAndMargins;IIILcom/squareup/blueprint/UpdateContext;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    new-instance v0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p4

    move v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;-><init>(Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/UpdateContext;III)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-interface {p0, v0}, Lcom/squareup/blueprint/IdsAndMargins;->forEach(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public static connectTo(Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V
    .locals 2

    const-string v0, "hook"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p1}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result p1

    invoke-interface {p0, v0, v1, p1, p2}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(IIILcom/squareup/blueprint/UpdateContext;)V

    return-void
.end method

.method public static isNotEmpty(Lcom/squareup/blueprint/IdsAndMargins;)Z
    .locals 0

    .line 140
    invoke-interface {p0}, Lcom/squareup/blueprint/IdsAndMargins;->isEmpty()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method
