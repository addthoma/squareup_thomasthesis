.class final Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;
.super Lkotlin/jvm/internal/Lambda;
.source "IdsAndMargins.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->connectTo(Lcom/squareup/blueprint/IdsAndMargins;IIILcom/squareup/blueprint/UpdateContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "id",
        "",
        "margin",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/blueprint/UpdateContext;

.field final synthetic $hookId:I

.field final synthetic $hookMargin:I

.field final synthetic $hookSide:I

.field final synthetic this$0:Lcom/squareup/blueprint/IdsAndMargins;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/UpdateContext;III)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->this$0:Lcom/squareup/blueprint/IdsAndMargins;

    iput-object p2, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$context:Lcom/squareup/blueprint/UpdateContext;

    iput p3, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$hookId:I

    iput p4, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$hookSide:I

    iput p5, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$hookMargin:I

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->invoke(II)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(II)V
    .locals 7

    .line 159
    iget-object v0, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$context:Lcom/squareup/blueprint/UpdateContext;

    invoke-virtual {v0}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->this$0:Lcom/squareup/blueprint/IdsAndMargins;

    invoke-interface {v0}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v3

    iget v4, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$hookId:I

    iget v5, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$hookSide:I

    iget v0, p0, Lcom/squareup/blueprint/IdsAndMargins$connectTo$1;->$hookMargin:I

    add-int v6, p2, v0

    move v2, p1

    invoke-static/range {v1 .. v6}, Lcom/squareup/blueprint/UpdateContextKt;->connectAndLog(Landroidx/constraintlayout/widget/ConstraintSet;IIIII)V

    return-void
.end method
