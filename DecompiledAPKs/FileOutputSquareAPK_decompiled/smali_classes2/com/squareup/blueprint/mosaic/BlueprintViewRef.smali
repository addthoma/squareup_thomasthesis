.class public final Lcom/squareup/blueprint/mosaic/BlueprintViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "BlueprintViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/blueprint/mosaic/BlueprintUiModel<",
        "*>;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBlueprintViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BlueprintViewRef.kt\ncom/squareup/blueprint/mosaic/BlueprintViewRef\n*L\n1#1,46:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0011\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u0015\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0018H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\"\u0010\t\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000b0\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/blueprint/mosaic/BlueprintViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/blueprint/mosaic/BlueprintUiModel;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "blueprint",
        "Lcom/squareup/blueprint/mosaic/MosaicBlueprint;",
        "children",
        "Lkotlin/sequences/Sequence;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "getChildren",
        "()Lkotlin/sequences/Sequence;",
        "updateContext",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private blueprint:Lcom/squareup/blueprint/mosaic/MosaicBlueprint;

.field private updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 13
    check-cast p2, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->createView(Landroid/content/Context;Lcom/squareup/blueprint/mosaic/BlueprintUiModel;)Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/blueprint/mosaic/BlueprintUiModel;)Landroidx/constraintlayout/widget/ConstraintLayout;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/blueprint/mosaic/BlueprintUiModel<",
            "*>;)",
            "Landroidx/constraintlayout/widget/ConstraintLayout;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    new-instance p2, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-direct {p2, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance p1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-direct {p1, p2}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    .line 26
    new-instance p1, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;

    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    if-nez v0, :cond_0

    const-string v1, "updateContext"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p1, v0}, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;-><init>(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;)V

    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->blueprint:Lcom/squareup/blueprint/mosaic/MosaicBlueprint;

    return-object p2
.end method

.method public doBind(Lcom/squareup/blueprint/mosaic/BlueprintUiModel;Lcom/squareup/blueprint/mosaic/BlueprintUiModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/mosaic/BlueprintUiModel<",
            "*>;",
            "Lcom/squareup/blueprint/mosaic/BlueprintUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v0, p2

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, v0}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 32
    iget-object p1, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->blueprint:Lcom/squareup/blueprint/mosaic/MosaicBlueprint;

    if-nez p1, :cond_0

    const-string v0, "blueprint"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 33
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->getBlueprintModel()Lcom/squareup/blueprint/Block;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 34
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->getExtendHorizontally()Z

    move-result v1

    .line 35
    invoke-virtual {p2}, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;->getExtendVertically()Z

    move-result p2

    .line 32
    invoke-virtual {p1, v0, v1, p2}, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->update(Lcom/squareup/blueprint/Block;ZZ)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    check-cast p2, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->doBind(Lcom/squareup/blueprint/mosaic/BlueprintUiModel;Lcom/squareup/blueprint/mosaic/BlueprintUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    check-cast p2, Lcom/squareup/blueprint/mosaic/BlueprintUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->doBind(Lcom/squareup/blueprint/mosaic/BlueprintUiModel;Lcom/squareup/blueprint/mosaic/BlueprintUiModel;)V

    return-void
.end method

.method public getChildren()Lkotlin/sequences/Sequence;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/sequences/Sequence<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    if-nez v0, :cond_0

    const-string v1, "updateContext"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->getViewList$blueprint_mosaic_release()Lcom/squareup/mosaic/lists/ModelsList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->getSubViews()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    return-object v0
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->blueprint:Lcom/squareup/blueprint/mosaic/MosaicBlueprint;

    if-nez v0, :cond_0

    const-string v1, "blueprint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->getUpdateContext$blueprint_mosaic_release()Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->restoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public saveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/BlueprintViewRef;->blueprint:Lcom/squareup/blueprint/mosaic/MosaicBlueprint;

    if-nez v0, :cond_0

    const-string v1, "blueprint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->getUpdateContext$blueprint_mosaic_release()Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->saveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method
