.class public final Lcom/squareup/blueprint/IdAndMargin;
.super Ljava/lang/Object;
.source "IdsAndMargins.kt"

# interfaces
.implements Lcom/squareup/blueprint/IdsAndMargins;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nIdsAndMargins.kt\nKotlin\n*S Kotlin\n*F\n+ 1 IdsAndMargins.kt\ncom/squareup/blueprint/IdAndMargin\n*L\n1#1,286:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\n2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\"\u0010\u0018\u001a\u00020\u00192\u0018\u0010\u001a\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00190\u001bH\u0016J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001J\u0016\u0010\u001d\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\u0010\u0010!\u001a\u00020\u00002\u0006\u0010\"\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\nX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u0008\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/blueprint/IdAndMargin;",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "id",
        "",
        "side",
        "margin",
        "(III)V",
        "getId",
        "()I",
        "isEmpty",
        "",
        "()Z",
        "getMargin",
        "getSide",
        "asIdAndMargin",
        "context",
        "Lcom/squareup/blueprint/UpdateContext;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "forEach",
        "",
        "block",
        "Lkotlin/Function2;",
        "hashCode",
        "hookForTheSameSide",
        "dependentIds",
        "toString",
        "",
        "withExtraMargin",
        "delta",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:I

.field private final isEmpty:Z

.field private final margin:I

.field private final side:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    iput p2, p0, Lcom/squareup/blueprint/IdAndMargin;->side:I

    iput p3, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    .line 30
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/blueprint/SideExtensionsKt;->isAValidSide(I)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Side: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " is not valid."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/IdAndMargin;IIIILjava/lang/Object;)Lcom/squareup/blueprint/IdAndMargin;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/IdAndMargin;->copy(III)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    return v0
.end method

.method public final component2()I
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v0

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    return v0
.end method

.method public connectTo(IIILcom/squareup/blueprint/UpdateContext;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->connectTo(Lcom/squareup/blueprint/IdsAndMargins;IIILcom/squareup/blueprint/UpdateContext;)V

    return-void
.end method

.method public connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V
    .locals 1

    const-string v0, "hook"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p0, p1, p2}, Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->connectTo(Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    return-void
.end method

.method public final copy(III)Lcom/squareup/blueprint/IdAndMargin;
    .locals 1

    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/IdAndMargin;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/IdAndMargin;

    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    iget v1, p1, Lcom/squareup/blueprint/IdAndMargin;->id:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    iget p1, p1, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public forEach(Lkotlin/jvm/functions/Function2;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final getId()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    return v0
.end method

.method public final getMargin()I
    .locals 1

    .line 27
    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    return v0
.end method

.method public getSide()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->side:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;
    .locals 10

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dependentIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v0

    invoke-interface {p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    .line 84
    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    if-nez v0, :cond_1

    return-object p0

    .line 88
    :cond_1
    new-instance v0, Lkotlin/jvm/internal/Ref$BooleanRef;

    invoke-direct {v0}, Lkotlin/jvm/internal/Ref$BooleanRef;-><init>()V

    iput-boolean v2, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    .line 89
    new-instance v1, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/blueprint/IdAndMargin$hookForTheSameSide$2;-><init>(Lcom/squareup/blueprint/IdAndMargin;Lkotlin/jvm/internal/Ref$BooleanRef;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-interface {p2, v1}, Lcom/squareup/blueprint/IdsAndMargins;->forEach(Lkotlin/jvm/functions/Function2;)V

    .line 92
    iget-boolean p2, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    if-eqz p2, :cond_2

    .line 93
    new-instance p1, Lcom/squareup/blueprint/IdAndMargin;

    iget p2, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v0

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    neg-int v1, v1

    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    return-object p1

    .line 96
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->generateViewId()I

    move-result p2

    .line 97
    invoke-virtual {p1, p2}, Lcom/squareup/blueprint/UpdateContext;->createView(I)V

    .line 99
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/blueprint/SideExtensionsKt;->isHorizontal(I)Z

    move-result v0

    const/16 v1, 0x14

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    goto :goto_1

    :cond_3
    const/16 v0, 0x14

    .line 100
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v2

    invoke-static {v2}, Lcom/squareup/blueprint/SideExtensionsKt;->isVertical(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    .line 101
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v2

    invoke-virtual {v2, p2, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 102
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v0

    invoke-virtual {v0, p2, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    .line 103
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v4

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/blueprint/SideExtensionsKt;->getOppositeSide(I)I

    move-result v6

    iget v7, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v8

    const/4 v9, 0x0

    move v5, p2

    invoke-static/range {v4 .. v9}, Lcom/squareup/blueprint/UpdateContextKt;->connectAndLog(Landroidx/constraintlayout/widget/ConstraintSet;IIIII)V

    .line 104
    new-instance p1, Lcom/squareup/blueprint/IdAndMargin;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v0

    invoke-direct {p1, p2, v0, v3}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    return-object p1

    .line 80
    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Cannot `hookForTheSameSide` if dependents are not the same side."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public isEmpty()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/blueprint/IdAndMargin;->isEmpty:Z

    return v0
.end method

.method public isNotEmpty()Z
    .locals 1

    .line 24
    invoke-static {p0}, Lcom/squareup/blueprint/IdsAndMargins$DefaultImpls;->isNotEmpty(Lcom/squareup/blueprint/IdsAndMargins;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "IdAndMargin(id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", side="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", margin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withExtraMargin(I)Lcom/squareup/blueprint/IdAndMargin;
    .locals 4

    .line 33
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    iget v1, p0, Lcom/squareup/blueprint/IdAndMargin;->id:I

    invoke-virtual {p0}, Lcom/squareup/blueprint/IdAndMargin;->getSide()I

    move-result v2

    iget v3, p0, Lcom/squareup/blueprint/IdAndMargin;->margin:I

    add-int/2addr v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    return-object v0
.end method

.method public bridge synthetic withExtraMargin(I)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/IdAndMargin;->withExtraMargin(I)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method
