.class public final Lcom/squareup/blueprint/SpreadHorizontalBlock;
.super Lcom/squareup/blueprint/LinearBlock;
.source "SpreadHorizontalBlock.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/blueprint/LinearBlock<",
        "TC;TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSpreadHorizontalBlock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SpreadHorizontalBlock.kt\ncom/squareup/blueprint/SpreadHorizontalBlock\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 4 Block.kt\ncom/squareup/blueprint/ChainInfo\n*L\n1#1,182:1\n1499#2,3:183\n1313#2:186\n1382#2,3:187\n1600#2,3:190\n1591#2,2:196\n1591#2,2:199\n1591#2,2:201\n1591#2,2:203\n1591#2,2:205\n1591#2,2:207\n1084#3,2:193\n204#4:195\n205#4:198\n*E\n*S KotlinDebug\n*F\n+ 1 SpreadHorizontalBlock.kt\ncom/squareup/blueprint/SpreadHorizontalBlock\n*L\n25#1,3:183\n33#1:186\n33#1,3:187\n35#1,3:190\n72#1,2:196\n100#1,2:199\n121#1,2:201\n129#1,2:203\n144#1,2:205\n176#1,2:207\n53#1,2:193\n72#1:195\n72#1:198\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0010 \n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0005B)\u0012\u0006\u0010\u0006\u001a\u00028\u0001\u0012\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008\u00a2\u0006\u0002\u0010\u000bJ%\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00028\u00002\u0006\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u001e\u001a\u00020\u0019H\u0016\u00a2\u0006\u0002\u0010\u001fJ \u0010 \u001a\u00020\u00192\u0006\u0010!\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0019H\u0016J \u0010%\u001a\u00020\u00192\u0006\u0010!\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0019H\u0016J\u000e\u0010&\u001a\u00028\u0001H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0015J\u001b\u0010\'\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008H\u00c4\u0003J \u0010(\u001a\u00020)2\u0006\u0010!\u001a\u00020\u00022\u0006\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020,H\u0016J \u0010-\u001a\u00020)2\u0006\u0010!\u001a\u00020\u00022\u0006\u0010*\u001a\u00020)2\u0006\u0010+\u001a\u00020.H\u0016J@\u0010/\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00028\u00012\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008H\u00c6\u0001\u00a2\u0006\u0002\u00100J\u0013\u00101\u001a\u00020\r2\u0008\u00102\u001a\u0004\u0018\u00010\u0004H\u00d6\u0003J\t\u00103\u001a\u00020\u0019H\u00d6\u0001J\u0010\u00104\u001a\u00020)2\u0006\u0010!\u001a\u00020\u0002H\u0016J\t\u00105\u001a\u000206H\u00d6\u0001J\u0010\u00107\u001a\u0002082\u0006\u0010!\u001a\u00020\u0002H\u0016R\u0014\u0010\u000c\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\r8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000fR&\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\n0\t0\u0008X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0016\u0010\u0006\u001a\u00028\u0001X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/blueprint/SpreadHorizontalBlock;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "Lcom/squareup/blueprint/LinearBlock;",
        "params",
        "elements",
        "",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "(Ljava/lang/Object;Ljava/util/List;)V",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "getElements",
        "()Ljava/util/List;",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "spacing",
        "",
        "",
        "buildViews",
        "",
        "updateContext",
        "width",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "component1",
        "component2",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "copy",
        "(Ljava/lang/Object;Ljava/util/List;)Lcom/squareup/blueprint/SpreadHorizontalBlock;",
        "equals",
        "other",
        "hashCode",
        "startIds",
        "toString",
        "",
        "topIds",
        "Lcom/squareup/blueprint/IdAndMarginCollection;",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private spacing:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/blueprint/LinearBlock;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/SpreadHorizontalBlock;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/blueprint/SpreadHorizontalBlock;->elements:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 13
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/List;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/blueprint/SpreadHorizontalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/blueprint/SpreadHorizontalBlock;Ljava/lang/Object;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/blueprint/SpreadHorizontalBlock;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->copy(Ljava/lang/Object;Ljava/util/List;)Lcom/squareup/blueprint/SpreadHorizontalBlock;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintLayout()Landroidx/constraintlayout/widget/ConstraintLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 186
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 187
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 188
    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 33
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {v3}, Lcom/squareup/blueprint/LinearBlock$Params;->getSpacing$blueprint_core_release()Lcom/squareup/resources/DimenModel;

    move-result-object v3

    const-string v4, "context"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 189
    :cond_0
    check-cast v2, Ljava/util/List;

    iput-object v2, p0, Lcom/squareup/blueprint/SpreadHorizontalBlock;->spacing:Ljava/util/List;

    .line 35
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    const/4 v1, 0x0

    .line 191
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 36
    invoke-virtual {v2, p1, p2, p3}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    move v1, v3

    goto :goto_1

    :cond_2
    return-void
.end method

.method public chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 0

    const-string p3, "context"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "chainInfo"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Spread horizontally cannot participate into an horizontal chain"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
    .locals 0

    const-string p3, "context"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "chainInfo"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "An operation is not implemented: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "I owe you this one... probably need to copy from HorizontalBlock"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 17

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "context"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "previousIds"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "align"

    move-object/from16 v3, p3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v2, Lcom/squareup/blueprint/ChainInfo;

    invoke-direct {v2}, Lcom/squareup/blueprint/ChainInfo;-><init>()V

    .line 49
    invoke-interface {v1, v0}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object v3

    .line 52
    invoke-virtual {v3}, Lcom/squareup/blueprint/IdAndMargin;->getMargin()I

    move-result v4

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v5

    move-object/from16 v6, p0

    iget-object v7, v6, Lcom/squareup/blueprint/SpreadHorizontalBlock;->spacing:Ljava/util/List;

    if-nez v7, :cond_0

    const-string v8, "spacing"

    invoke-static {v8}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v7, Ljava/lang/Iterable;

    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v7

    invoke-static {v5, v7}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v5

    .line 193
    invoke-interface {v5}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lkotlin/Pair;

    invoke-virtual {v7}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/blueprint/Block;

    invoke-virtual {v7}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    add-int/2addr v7, v4

    .line 54
    invoke-virtual {v8, v0, v2, v7}, Lcom/squareup/blueprint/Block;->chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I

    move-result v4

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v5

    const/4 v7, 0x1

    const/4 v8, 0x0

    if-le v5, v7, :cond_3

    const/4 v12, 0x0

    const/4 v13, 0x7

    .line 62
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v9

    .line 63
    invoke-virtual {v3}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v10

    .line 64
    invoke-interface/range {p2 .. p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v11

    .line 67
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->ids()[I

    move-result-object v14

    .line 68
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v1

    new-array v15, v1, [F

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_2

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v15, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/16 v16, 0x2

    .line 62
    invoke-virtual/range {v9 .. v16}, Landroidx/constraintlayout/widget/ConstraintSet;->createHorizontalChainRtl(IIII[I[FI)V

    .line 195
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->getSize()I

    move-result v1

    invoke-static {v8, v1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 196
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object v3, v1

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v3

    .line 195
    invoke-virtual {v2, v3}, Lcom/squareup/blueprint/ChainInfo;->idAt(I)I

    move-result v5

    invoke-virtual {v2, v3}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v3

    .line 73
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v7

    const/4 v8, 0x6

    invoke-virtual {v7, v5, v8, v3}, Landroidx/constraintlayout/widget/ConstraintSet;->setMargin(III)V

    goto :goto_2

    .line 77
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/blueprint/UpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v9

    .line 78
    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->firstId()I

    move-result v10

    const/4 v11, 0x6

    .line 80
    invoke-virtual {v3}, Lcom/squareup/blueprint/IdAndMargin;->getId()I

    move-result v12

    .line 81
    invoke-interface/range {p2 .. p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result v13

    .line 82
    invoke-virtual {v2, v8}, Lcom/squareup/blueprint/ChainInfo;->marginAt(I)I

    move-result v14

    .line 77
    invoke-virtual/range {v9 .. v14}, Landroidx/constraintlayout/widget/ConstraintSet;->connect(IIIII)V

    .line 86
    :cond_4
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v1, 0x7

    invoke-virtual {v2}, Lcom/squareup/blueprint/ChainInfo;->lastId()I

    move-result v2

    invoke-direct {v0, v1, v2, v4}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(III)V

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    return-object v0
.end method

.method public connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 5

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousIds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "align"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/squareup/blueprint/SpreadHorizontalBlock$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/blueprint/VerticalAlign;->ordinal()I

    move-result p3

    aget p3, v0, p3

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-eq p3, v0, :cond_8

    const/4 v0, 0x2

    if-eq p3, v0, :cond_6

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    .line 127
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v0}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 128
    new-instance v2, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v2, v0}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 129
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 203
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/Block;

    .line 130
    invoke-virtual {v3}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 131
    invoke-virtual {v3, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    invoke-virtual {p3, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {v3, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    .line 136
    :cond_1
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 139
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 140
    invoke-virtual {p3, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 141
    invoke-interface {p2, p3, p1}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 142
    check-cast v2, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p3, p1, v2}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p3

    .line 143
    new-instance v1, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-interface {p2}, Lcom/squareup/blueprint/IdsAndMargins;->getSide()I

    move-result p2

    invoke-direct {v1, p2}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 205
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 145
    invoke-virtual {v2}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 147
    move-object v3, v1

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->BOTTOM:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    goto :goto_2

    .line 149
    :cond_2
    move-object v3, p3

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    .line 145
    :goto_2
    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_1

    .line 152
    :cond_3
    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    return-object v0

    .line 136
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "HorizontalBlock aligned BOTTOM need to have at least one dependable item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 152
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 119
    :cond_6
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 120
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 121
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 201
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    .line 122
    move-object v2, p2

    check-cast v2, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v3, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v1, p1, v2, v3}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_3

    .line 124
    :cond_7
    check-cast p3, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p3

    .line 97
    :cond_8
    invoke-interface {p2, p1}, Lcom/squareup/blueprint/IdsAndMargins;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 98
    new-instance p3, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {p3, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 99
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 199
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 101
    invoke-virtual {v2}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 102
    move-object v3, p2

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_4

    .line 104
    :cond_9
    move-object v3, p2

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    sget-object v4, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    invoke-virtual {v2, p1, v3, v4}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_4

    .line 107
    :cond_a
    invoke-virtual {p3}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_c

    .line 110
    invoke-virtual {v0}, Lcom/squareup/blueprint/IdAndMarginCollection;->isNotEmpty()Z

    move-result p2

    if-eqz p2, :cond_b

    .line 111
    invoke-virtual {p3, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->asIdAndMargin(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 113
    move-object v1, v0

    check-cast v1, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p2, p1, v1}, Lcom/squareup/blueprint/IdAndMargin;->hookForTheSameSide(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;)Lcom/squareup/blueprint/IdAndMargin;

    move-result-object p2

    .line 114
    invoke-virtual {v0, p2, p1}, Lcom/squareup/blueprint/IdAndMarginCollection;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 116
    :cond_b
    check-cast p3, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p3

    .line 107
    :cond_c
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "HorizontalBlock aligned TOP need to have at least one dependable item."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final copy(Ljava/lang/Object;Ljava/util/List;)Lcom/squareup/blueprint/SpreadHorizontalBlock;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;)",
            "Lcom/squareup/blueprint/SpreadHorizontalBlock<",
            "TC;TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "elements"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/blueprint/SpreadHorizontalBlock;

    invoke-direct {v0, p1, p2}, Lcom/squareup/blueprint/SpreadHorizontalBlock;-><init>(Ljava/lang/Object;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/blueprint/SpreadHorizontalBlock;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/blueprint/SpreadHorizontalBlock;

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDependableHorizontally()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDependableVertically()Z
    .locals 3

    .line 25
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 183
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 184
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/Block;

    .line 25
    invoke-virtual {v1}, Lcom/squareup/blueprint/Block;->getDependableVertically()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    return v2
.end method

.method protected getElements()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/blueprint/Block<",
            "TC;",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;>;"
        }
    .end annotation

    .line 13
    iget-object v0, p0, Lcom/squareup/blueprint/SpreadHorizontalBlock;->elements:Ljava/util/List;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/blueprint/SpreadHorizontalBlock;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/Block;

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SpreadHorizontalBlock(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", elements="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    new-instance v0, Lcom/squareup/blueprint/IdAndMarginCollection;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/squareup/blueprint/IdAndMarginCollection;-><init>(I)V

    .line 177
    invoke-virtual {p0}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->getElements()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 207
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/blueprint/Block;

    .line 177
    invoke-virtual {v2, p1}, Lcom/squareup/blueprint/Block;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/blueprint/IdAndMarginCollection;->plusAssign(Lcom/squareup/blueprint/IdsAndMargins;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/SpreadHorizontalBlock;->topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdAndMarginCollection;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/IdsAndMargins;

    return-object p1
.end method
