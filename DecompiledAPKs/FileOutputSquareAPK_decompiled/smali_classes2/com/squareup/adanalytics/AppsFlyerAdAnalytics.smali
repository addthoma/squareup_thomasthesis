.class public final Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;
.super Ljava/lang/Object;
.source "AppsFlyerAdAnalytics.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/adanalytics/AdAnalytics;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0007\u0018\u0000 \'2\u00020\u00012\u00020\u0002:\u0001\'B\u007f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u000e\u0008\u0001\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J \u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0013H\u0016J\"\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 2\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u0012\u0010!\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\"\u0010\"\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0012\u0010#\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\"\u0010$\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 2\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\u0012\u0010%\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\"\u0010&\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;",
        "Lmortar/Scoped;",
        "Lcom/squareup/adanalytics/AdAnalytics;",
        "context",
        "Landroid/app/Application;",
        "appsFlyerLib",
        "Lcom/appsflyer/AppsFlyerLib;",
        "hasFinishedIdVerificationBefore",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "hasSignedUpBefore",
        "hasSignedInBefore",
        "hasSentInvoiceOrTakenPaymentBefore",
        "hasTakenItemizedPaymentBefore",
        "hasCreatedItemBefore",
        "accountStatusProvider",
        "Lcom/squareup/accountstatus/AccountStatusProvider;",
        "(Landroid/app/Application;Lcom/appsflyer/AppsFlyerLib;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/accountstatus/AccountStatusProvider;)V",
        "measureSendInvoiceOrTakePayment",
        "",
        "merchantToken",
        "",
        "serverId",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "recordActivation",
        "token",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "recordCreateItem",
        "recordSendInvoice",
        "recordSignIn",
        "recordSignUp",
        "recordTakeItemizedPayment",
        "recordTakePayment",
        "Companion",
        "ad-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$Companion;

.field public static final MERCHANT_ID_KEY_NAME:Ljava/lang/String; = "af_content_id"


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

.field private final context:Landroid/app/Application;

.field private final hasCreatedItemBefore:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final hasFinishedIdVerificationBefore:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSentInvoiceOrTakenPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSignedInBefore:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final hasSignedUpBefore:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final hasTakenItemizedPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->Companion:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/appsflyer/AppsFlyerLib;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/accountstatus/AccountStatusProvider;)V
    .locals 1
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/adanalytics/AdAnalyticsModule$HasFinishedIdVerificationBeforeAppsFlyer;
        .end annotation
    .end param
    .param p4    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/adanalytics/AdAnalyticsModule$HasSignedUpBeforeAppsFlyer;
        .end annotation
    .end param
    .param p5    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/adanalytics/AdAnalyticsModule$HasSignedInBeforeAppsFlyer;
        .end annotation
    .end param
    .param p6    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/adanalytics/AdAnalyticsModule$HasSentInvoiceOrTakenPaymentBeforeAppsFlyer;
        .end annotation
    .end param
    .param p7    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/adanalytics/AdAnalyticsModule$HasTakenItemizedPaymentBeforeAppsFlyer;
        .end annotation
    .end param
    .param p8    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/adanalytics/AdAnalyticsModule$HasCreatedItemBeforeAppsFlyer;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/appsflyer/AppsFlyerLib;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appsFlyerLib"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hasFinishedIdVerificationBefore"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hasSignedUpBefore"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hasSignedInBefore"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hasSentInvoiceOrTakenPaymentBefore"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hasTakenItemizedPaymentBefore"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hasCreatedItemBefore"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusProvider"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iput-object p3, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasFinishedIdVerificationBefore:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSignedUpBefore:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p5, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSignedInBefore:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p6, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSentInvoiceOrTakenPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p7, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasTakenItemizedPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p8, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasCreatedItemBefore:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p9, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    return-void
.end method

.method public static final synthetic access$getAppsFlyerLib$p(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)Lcom/appsflyer/AppsFlyerLib;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    return-object p0
.end method

.method public static final synthetic access$getContext$p(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)Landroid/app/Application;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    return-object p0
.end method

.method private final measureSendInvoiceOrTakePayment(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 4

    .line 167
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSentInvoiceOrTakenPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "hasSentInvoiceOrTakenPaymentBefore.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSentInvoiceOrTakenPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lkotlin/Pair;

    const/4 v2, 0x0

    const-string v3, "af_content_id"

    .line 173
    invoke-static {v3, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    aput-object p1, v0, v2

    const-string p1, "af_order_id"

    .line 174
    invoke-static {p1, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    aput-object p1, v0, v1

    const/4 p1, 0x2

    .line 175
    invoke-virtual {p3}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p2

    const-string p3, "af_currency"

    invoke-static {p3, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, v0, p1

    .line 172
    invoke-static {v0}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 177
    iget-object p2, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iget-object p3, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    check-cast p3, Landroid/content/Context;

    const-string v0, "sq_first_app_payment_or_invoice_sent"

    invoke-virtual {p2, p3, v0, p1}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 45
    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    invoke-static {v0}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresent(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$1;->INSTANCE:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$2;-><init>(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "accountStatusProvider\n  \u2026b.setCustomerUserId(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 53
    invoke-interface {v0}, Lcom/squareup/accountstatus/AccountStatusProvider;->latest()Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresent(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    sget-object v1, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;->INSTANCE:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 57
    new-instance v1, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$4;-><init>(Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "accountStatusProvider\n  \u2026opTracking(it, context) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public recordActivation(Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/lang/String;)V
    .locals 4

    const-string v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "countryCode"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_1

    .line 88
    iget-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasFinishedIdVerificationBefore:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "hasFinishedIdVerificationBefore.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 91
    :cond_0
    iget-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasFinishedIdVerificationBefore:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const/4 p1, 0x3

    new-array p1, p1, [Lkotlin/Pair;

    const/4 v1, 0x0

    const-string v2, "af_success"

    const-string v3, "TRUE"

    .line 94
    invoke-static {v2, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v2

    aput-object v2, p1, v1

    const-string v1, "af_content_id"

    .line 95
    invoke-static {v1, p3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p3

    aput-object p3, p1, v0

    const/4 p3, 0x2

    .line 96
    invoke-virtual {p2}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object p2

    const-string v0, "af_country"

    invoke-static {v0, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p2

    aput-object p2, p1, p3

    .line 93
    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 98
    iget-object p2, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iget-object p3, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    check-cast p3, Landroid/content/Context;

    const-string v0, "sq_first_successful_app_activation"

    invoke-virtual {p2, p3, v0, p1}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public recordCreateItem(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 149
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasCreatedItemBefore:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "hasCreatedItemBefore.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasCreatedItemBefore:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const-string v0, "af_content_id"

    .line 156
    invoke-static {v0, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    .line 155
    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 159
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iget-object v1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    const-string v2, "sq_first_app_item_created"

    invoke-virtual {v0, v1, v2, p1}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public recordSendInvoice(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1

    const-string v0, "serverId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 121
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->measureSendInvoiceOrTakePayment(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V

    return-void
.end method

.method public recordSignIn(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 102
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSignedInBefore:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "hasSignedInBefore.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSignedInBefore:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const-string v0, "af_content_id"

    .line 108
    invoke-static {v0, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    .line 107
    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 110
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iget-object v1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    const-string v2, "sq_first_sign_in"

    invoke-virtual {v0, v1, v2, p1}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public recordSignUp(Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/lang/String;)V
    .locals 3

    const-string v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "countryCode"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p3, :cond_1

    .line 71
    iget-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSignedUpBefore:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {p1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "hasSignedUpBefore.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 74
    :cond_0
    iget-object p1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasSignedUpBefore:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const/4 p1, 0x2

    new-array p1, p1, [Lkotlin/Pair;

    const/4 v0, 0x0

    const-string v1, "af_registration_method"

    const-string v2, "email"

    .line 77
    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v1

    aput-object v1, p1, v0

    const-string v0, "af_content_id"

    .line 78
    invoke-static {v0, p3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p3

    aput-object p3, p1, p2

    .line 76
    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 80
    iget-object p2, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iget-object p3, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    check-cast p3, Landroid/content/Context;

    const-string v0, "sq_first_complete_app_signup"

    invoke-virtual {p2, p3, v0, p1}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public recordTakeItemizedPayment(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_1

    .line 136
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasTakenItemizedPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "hasTakenItemizedPaymentBefore.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->hasTakenItemizedPaymentBefore:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    const-string v0, "af_content_id"

    .line 143
    invoke-static {v0, p1}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    .line 142
    invoke-static {p1}, Lkotlin/collections/MapsKt;->mapOf(Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    .line 145
    iget-object v0, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->appsFlyerLib:Lcom/appsflyer/AppsFlyerLib;

    iget-object v1, p0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->context:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    const-string v2, "sq_first_app_itemized_payment"

    invoke-virtual {v0, v1, v2, p1}, Lcom/appsflyer/AppsFlyerLib;->trackEvent(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public recordTakePayment(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1

    const-string v0, "serverId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    return-void

    .line 132
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->measureSendInvoiceOrTakePayment(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V

    return-void
.end method
