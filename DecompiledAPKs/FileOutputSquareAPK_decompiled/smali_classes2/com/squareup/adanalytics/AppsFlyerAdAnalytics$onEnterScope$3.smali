.class final Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;
.super Ljava/lang/Object;
.source "AppsFlyerAdAnalytics.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/adanalytics/AppsFlyerAdAnalytics;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "apply",
        "(Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/lang/Boolean;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;

    invoke-direct {v0}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;-><init>()V

    sput-object v0, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;->INSTANCE:Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/lang/Boolean;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/server/account/protos/PrivacyFeatures;->opted_out_of_tracking:Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/adanalytics/AppsFlyerAdAnalytics$onEnterScope$3;->apply(Lcom/squareup/server/account/protos/AccountStatusResponse;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
