.class public abstract Lcom/squareup/pauses/PausesModule;
.super Ljava/lang/Object;
.source "PausesModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideRegistry(Lcom/squareup/pauses/PauseAndResumePresenter;)Lcom/squareup/pauses/PauseAndResumeRegistrar;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
