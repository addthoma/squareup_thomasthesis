.class Lcom/squareup/autocapture/AutoCaptureJob$LoggedOut;
.super Ljava/lang/RuntimeException;
.source "AutoCaptureJob.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/autocapture/AutoCaptureJob;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LoggedOut"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/autocapture/AutoCaptureJob;


# direct methods
.method private constructor <init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;)V
    .locals 2

    .line 165
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureJob$LoggedOut;->this$0:Lcom/squareup/autocapture/AutoCaptureJob;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 167
    invoke-static {p1}, Lcom/squareup/autocapture/AutoCaptureJob;->access$200(Lcom/squareup/autocapture/AutoCaptureJob;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 p1, 0x1

    aput-object p2, v0, p1

    const-string p1, "%s logged out but received AutoCapture broadcast for %s"

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 166
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureJob$1;)V
    .locals 0

    .line 164
    invoke-direct {p0, p1, p2}, Lcom/squareup/autocapture/AutoCaptureJob$LoggedOut;-><init>(Lcom/squareup/autocapture/AutoCaptureJob;Ljava/lang/String;)V

    return-void
.end method
