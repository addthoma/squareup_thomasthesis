.class public interface abstract Lcom/squareup/autocapture/AutoCaptureControlAlarm;
.super Ljava/lang/Object;
.source "AutoCaptureControlAlarm.java"


# static fields
.field public static final REAL_AUTO_CAPTURE_CONTROL_ALARM:Lcom/squareup/autocapture/AutoCaptureControlAlarm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/autocapture/AutoCaptureControlAlarm$1;

    invoke-direct {v0}, Lcom/squareup/autocapture/AutoCaptureControlAlarm$1;-><init>()V

    sput-object v0, Lcom/squareup/autocapture/AutoCaptureControlAlarm;->REAL_AUTO_CAPTURE_CONTROL_ALARM:Lcom/squareup/autocapture/AutoCaptureControlAlarm;

    return-void
.end method


# virtual methods
.method public abstract startQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V
.end method

.method public abstract startSlowAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;Lcom/squareup/autocapture/AutoCaptureControlTimer;Lcom/squareup/settings/server/AccountStatusSettings;)V
.end method

.method public abstract stopQuickAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V
.end method

.method public abstract stopSlowAlarm(Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/log/OhSnapLogger;)V
.end method
