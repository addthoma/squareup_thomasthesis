.class public final synthetic Lcom/squareup/autocapture/AutoCaptureControlTimer$-CC;
.super Ljava/lang/Object;
.source "AutoCaptureControlTimer.java"


# direct methods
.method public static synthetic lambda$static$0(Lcom/squareup/autocapture/AutoCaptureControlAlarmType;)I
    .locals 3

    .line 7
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlTimer$1;->$SwitchMap$com$squareup$autocapture$AutoCaptureControlAlarmType:[I

    invoke-virtual {p0}, Lcom/squareup/autocapture/AutoCaptureControlAlarmType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const p0, 0x36ee80

    return p0

    .line 13
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized alarm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_1
    const p0, 0x1d4c0

    return p0
.end method
