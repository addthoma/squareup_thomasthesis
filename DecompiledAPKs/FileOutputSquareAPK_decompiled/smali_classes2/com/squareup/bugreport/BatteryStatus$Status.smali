.class public final enum Lcom/squareup/bugreport/BatteryStatus$Status;
.super Ljava/lang/Enum;
.source "BatteryStatus.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/bugreport/BatteryStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/bugreport/BatteryStatus$Status$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/bugreport/BatteryStatus$Status;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u0000 \u00082\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0008B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/bugreport/BatteryStatus$Status;",
        "",
        "(Ljava/lang/String;I)V",
        "CHARGING",
        "DISCHARGING",
        "FULL",
        "NOT_CHARGING",
        "UNKNOWN",
        "Companion",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/bugreport/BatteryStatus$Status;

.field public static final enum CHARGING:Lcom/squareup/bugreport/BatteryStatus$Status;

.field public static final Companion:Lcom/squareup/bugreport/BatteryStatus$Status$Companion;

.field public static final enum DISCHARGING:Lcom/squareup/bugreport/BatteryStatus$Status;

.field public static final enum FULL:Lcom/squareup/bugreport/BatteryStatus$Status;

.field public static final enum NOT_CHARGING:Lcom/squareup/bugreport/BatteryStatus$Status;

.field public static final enum UNKNOWN:Lcom/squareup/bugreport/BatteryStatus$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/bugreport/BatteryStatus$Status;

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Status;

    const/4 v2, 0x0

    const-string v3, "CHARGING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Status;->CHARGING:Lcom/squareup/bugreport/BatteryStatus$Status;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Status;

    const/4 v2, 0x1

    const-string v3, "DISCHARGING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Status;->DISCHARGING:Lcom/squareup/bugreport/BatteryStatus$Status;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Status;

    const/4 v2, 0x2

    const-string v3, "FULL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Status;->FULL:Lcom/squareup/bugreport/BatteryStatus$Status;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Status;

    const/4 v2, 0x3

    const-string v3, "NOT_CHARGING"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Status;->NOT_CHARGING:Lcom/squareup/bugreport/BatteryStatus$Status;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/bugreport/BatteryStatus$Status;

    const/4 v2, 0x4

    const-string v3, "UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/bugreport/BatteryStatus$Status;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/bugreport/BatteryStatus$Status;->UNKNOWN:Lcom/squareup/bugreport/BatteryStatus$Status;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/bugreport/BatteryStatus$Status;->$VALUES:[Lcom/squareup/bugreport/BatteryStatus$Status;

    new-instance v0, Lcom/squareup/bugreport/BatteryStatus$Status$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/bugreport/BatteryStatus$Status$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/bugreport/BatteryStatus$Status;->Companion:Lcom/squareup/bugreport/BatteryStatus$Status$Companion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/bugreport/BatteryStatus$Status;
    .locals 1

    const-class v0, Lcom/squareup/bugreport/BatteryStatus$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/bugreport/BatteryStatus$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/bugreport/BatteryStatus$Status;
    .locals 1

    sget-object v0, Lcom/squareup/bugreport/BatteryStatus$Status;->$VALUES:[Lcom/squareup/bugreport/BatteryStatus$Status;

    invoke-virtual {v0}, [Lcom/squareup/bugreport/BatteryStatus$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/bugreport/BatteryStatus$Status;

    return-object v0
.end method
