.class public Lcom/squareup/bugreport/AttachmentsBuilder$Factory;
.super Ljava/lang/Object;
.source "AttachmentsBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/bugreport/AttachmentsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final externalFilesDir:Ljava/io/File;


# direct methods
.method constructor <init>(Landroid/app/Application;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 46
    invoke-virtual {p1, v0}, Landroid/app/Application;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/bugreport/AttachmentsBuilder$Factory;->externalFilesDir:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/bugreport/AttachmentsBuilder;
    .locals 3

    .line 53
    new-instance v0, Lcom/squareup/bugreport/AttachmentsBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/AttachmentsBuilder$Factory;->externalFilesDir:Ljava/io/File;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/bugreport/AttachmentsBuilder;-><init>(Ljava/io/File;Lcom/squareup/bugreport/AttachmentsBuilder$1;)V

    return-object v0
.end method
