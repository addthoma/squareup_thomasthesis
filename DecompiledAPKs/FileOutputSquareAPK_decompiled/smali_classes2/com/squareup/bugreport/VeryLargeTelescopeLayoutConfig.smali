.class public interface abstract Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;
.super Ljava/lang/Object;
.source "VeryLargeTelescopeLayoutConfig.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u001a\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH&R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/bugreport/VeryLargeTelescopeLayoutConfig;",
        "",
        "mode",
        "Lcom/mattprecious/telescope/ScreenshotMode;",
        "getMode",
        "()Lcom/mattprecious/telescope/ScreenshotMode;",
        "onCapture",
        "",
        "activity",
        "Landroid/app/Activity;",
        "screenshot",
        "Ljava/io/File;",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getMode()Lcom/mattprecious/telescope/ScreenshotMode;
.end method

.method public abstract onCapture(Landroid/app/Activity;Ljava/io/File;)V
.end method
