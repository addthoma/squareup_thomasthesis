.class final Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;
.super Ljava/lang/Object;
.source "BugReporter.kt"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/bugreport/BugReporter;->showBugReportDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/content/DialogInterface;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activity:Landroid/app/Activity;

.field final synthetic $attachments:Ljava/util/List;

.field final synthetic $bugReport:Ljava/lang/String;

.field final synthetic $isTwoFinger:Z

.field final synthetic $reporterDescriptionView:Landroid/widget/TextView;

.field final synthetic $reporterNameView:Landroid/widget/TextView;

.field final synthetic $reporterSummaryView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/bugreport/BugReporter;


# direct methods
.method constructor <init>(Lcom/squareup/bugreport/BugReporter;ZLandroid/app/Activity;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->this$0:Lcom/squareup/bugreport/BugReporter;

    iput-boolean p2, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$isTwoFinger:Z

    iput-object p3, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$activity:Landroid/app/Activity;

    iput-object p4, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$reporterNameView:Landroid/widget/TextView;

    iput-object p5, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$reporterSummaryView:Landroid/widget/TextView;

    iput-object p6, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$reporterDescriptionView:Landroid/widget/TextView;

    iput-object p7, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$bugReport:Ljava/lang/String;

    iput-object p8, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$attachments:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .line 63
    iget-object v0, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->this$0:Lcom/squareup/bugreport/BugReporter;

    .line 64
    iget-boolean v1, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$isTwoFinger:Z

    .line 65
    iget-object v2, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$activity:Landroid/app/Activity;

    .line 66
    iget-object p1, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$reporterNameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 67
    iget-object p1, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$reporterSummaryView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 68
    iget-object p1, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$reporterDescriptionView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 69
    iget-object v6, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$bugReport:Ljava/lang/String;

    .line 70
    iget-object v7, p0, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1;->$attachments:Ljava/util/List;

    .line 63
    invoke-static/range {v0 .. v7}, Lcom/squareup/bugreport/BugReporter;->access$sendReport(Lcom/squareup/bugreport/BugReporter;ZLandroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    .line 71
    sget-object p2, Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;->INSTANCE:Lcom/squareup/bugreport/BugReporter$showBugReportDialog$1$1;

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
