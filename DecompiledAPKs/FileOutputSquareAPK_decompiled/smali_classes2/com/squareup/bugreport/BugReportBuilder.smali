.class public Lcom/squareup/bugreport/BugReportBuilder;
.super Ljava/lang/Object;
.source "BugReportBuilder.java"


# instance fields
.field private final application:Landroid/app/Application;

.field private final attachmentsBuilderFactory:Lcom/squareup/bugreport/AttachmentsBuilder$Factory;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final device:Lcom/squareup/util/Device;

.field private final featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

.field private final headsetConnection:Lcom/squareup/wavpool/swipe/HeadsetConnection;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mortarScopeHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final posBuild:Lcom/squareup/util/PosBuild;

.field private final resources:Landroid/content/res/Resources;

.field private final server:Lcom/squareup/http/Server;

.field private final userAgent:Ljava/lang/String;

.field private final userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

.field private final versionCode:I

.field private final versionName:Ljava/lang/String;

.field private final viewHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/wavpool/swipe/HeadsetConnection;Landroid/content/res/Resources;Lcom/squareup/http/Server;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/util/Device;Ljava/lang/String;Ljava/lang/String;ILcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/bugreport/AttachmentsBuilder$Factory;Lcom/squareup/util/PosBuild;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/http/Server;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            "Lcom/squareup/bugreport/AttachmentsBuilder$Factory;",
            "Lcom/squareup/util/PosBuild;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p7

    .line 78
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->resources:Landroid/content/res/Resources;

    move-object v1, p12

    .line 79
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->device:Lcom/squareup/util/Device;

    move-object v1, p1

    .line 80
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->application:Landroid/app/Application;

    move-object v1, p13

    .line 81
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->userAgent:Ljava/lang/String;

    move-object v1, p10

    .line 82
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->viewHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 83
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 84
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->versionName:Ljava/lang/String;

    move/from16 v1, p15

    .line 85
    iput v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->versionCode:I

    move-object v1, p8

    .line 86
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->server:Lcom/squareup/http/Server;

    move-object v1, p11

    .line 87
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 88
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    move-object v1, p5

    .line 89
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

    move-object v1, p6

    .line 90
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->headsetConnection:Lcom/squareup/wavpool/swipe/HeadsetConnection;

    move-object v1, p3

    .line 91
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    move-object v1, p2

    .line 92
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    move-object/from16 v1, p16

    .line 93
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    move-object/from16 v1, p17

    .line 94
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->attachmentsBuilderFactory:Lcom/squareup/bugreport/AttachmentsBuilder$Factory;

    move-object/from16 v1, p18

    .line 95
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder;->posBuild:Lcom/squareup/util/PosBuild;

    return-void
.end method

.method private appendDevice(Ljava/lang/StringBuilder;)V
    .locals 4

    .line 157
    iget-object v0, p0, Lcom/squareup/bugreport/BugReportBuilder;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 158
    invoke-static {v0}, Lcom/mattprecious/telescope/EmailDeviceInfoLens;->getDensityString(Landroid/util/DisplayMetrics;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "\n\nh1. Device\n"

    .line 160
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n{code}"

    .line 161
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nVersion: "

    .line 162
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/bugreport/BugReportBuilder;->versionName:Ljava/lang/String;

    .line 163
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nVersion code: "

    .line 164
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/squareup/bugreport/BugReportBuilder;->versionCode:I

    .line 165
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "\nBuild SHA: "

    .line 166
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/bugreport/BugReportBuilder;->posBuild:Lcom/squareup/util/PosBuild;

    .line 167
    invoke-interface {v3}, Lcom/squareup/util/PosBuild;->getGitSha()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nMake: "

    .line 168
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 169
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nModel: "

    .line 170
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 171
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nBrand: "

    .line 172
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 173
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nCpu: "

    .line 174
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    invoke-static {}, Lcom/squareup/util/Abis;->cpuAbi()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nAndroid Release: "

    .line 176
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 177
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\nAndroid API version: "

    .line 178
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 179
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "\nResolution: "

    .line 180
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 181
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v3, 0x78

    .line 182
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 183
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "\nDensity: "

    .line 184
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 185
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "dpi ("

    .line 186
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    .line 188
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\nDiagonal Bucket: "

    .line 189
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/bugreport/BugReportBuilder;->application:Landroid/app/Application;

    .line 190
    invoke-static {v0}, Lcom/squareup/bugreport/BugReportBuilder;->diagonalBucket(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\nConsidered tablet: "

    .line 191
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/bugreport/BugReportBuilder;->device:Lcom/squareup/util/Device;

    .line 192
    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendFeatures(Ljava/lang/StringBuilder;)V
    .locals 2

    const-string v0, "\n\nh1. Features\n"

    .line 197
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n{code}"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

    invoke-virtual {v1, p1}, Lcom/squareup/log/FeatureFlagsForLogs;->allFeatures(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    .line 199
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendMisc(Ljava/lang/StringBuilder;)V
    .locals 4

    const-string v0, "\n\nh1. Misc\n"

    .line 203
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n{code}"

    .line 204
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nconfigurationHardKeyboardHidden: "

    .line 205
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->resources:Landroid/content/res/Resources;

    .line 206
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/bugreport/BugReportBuilder;->hardKeyboardHidden(Landroid/content/res/Configuration;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nHeadset Connected: "

    .line 207
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->headsetConnection:Lcom/squareup/wavpool/swipe/HeadsetConnection;

    .line 208
    invoke-interface {v1}, Lcom/squareup/wavpool/swipe/HeadsetConnection;->isReaderConnected()Z

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\nBLE (supported/enabled): "

    .line 209
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/squareup/bugreport/BugReportBuilder;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 210
    invoke-interface {v2}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/bugreport/BugReportBuilder;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v2}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const-string v2, "%s/%s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nCardReaders: "

    .line 211
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 212
    invoke-direct {p0}, Lcom/squareup/bugreport/BugReportBuilder;->getCardReaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendRuntime(Ljava/lang/StringBuilder;)V
    .locals 2

    const-string v0, "\n\nh1. Runtime\n"

    .line 139
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n{code}"

    .line 140
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nDate: "

    .line 141
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 142
    invoke-static {v1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nEndpoint: "

    .line 143
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->server:Lcom/squareup/http/Server;

    .line 144
    invoke-virtual {v1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nDevice locale: "

    .line 145
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->localeProvider:Ljavax/inject/Provider;

    .line 146
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nApp package: "

    .line 147
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->application:Landroid/app/Application;

    .line 148
    invoke-virtual {v1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\nOrientation: "

    .line 149
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-direct {p0}, Lcom/squareup/bugreport/BugReportBuilder;->getOrientation()Lcom/squareup/log/OhSnapEvent$Orientation;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\nUser Agent: "

    .line 151
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->userAgent:Ljava/lang/String;

    .line 152
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private appendStacktrace(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    if-eqz p2, :cond_0

    const-string v0, "\n\nh1. Stacktrace\n"

    .line 117
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n{code}\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\n{code}"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private appendUser(Ljava/lang/StringBuilder;)V
    .locals 2

    const-string v0, "\n\nh1. User\n"

    .line 122
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n{code}"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->hasUserSettings()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "\nLogged in as: "

    .line 124
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    .line 125
    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n Unit token: "

    .line 126
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    .line 127
    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n Country code: "

    .line 128
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    .line 129
    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\n Currency: "

    .line 130
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    .line 131
    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v1, "\nUser not logged in."

    .line 133
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static diagonalBucket(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const-string/jumbo v0, "window"

    .line 298
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/WindowManager;

    .line 299
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 300
    invoke-interface {p0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object p0

    .line 301
    invoke-virtual {p0, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 302
    iget p0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int p0, p0, v1

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int v1, v1, v2

    add-int/2addr p0, v1

    int-to-double v1, p0

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    iget p0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-double v3, p0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getCardReaderString()Ljava/lang/String;
    .locals 3

    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 284
    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 285
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReader;

    .line 287
    invoke-interface {v2}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 289
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ", "

    .line 290
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string v1, "]"

    .line 294
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getOrientation()Lcom/squareup/log/OhSnapEvent$Orientation;
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/bugreport/BugReportBuilder;->resources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 278
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0

    .line 274
    :cond_0
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->LANDSCAPE:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0

    .line 276
    :cond_1
    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->PORTRAIT:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-object v0
.end method

.method private static hardKeyboardHidden(Landroid/content/res/Configuration;)Ljava/lang/String;
    .locals 1

    .line 259
    iget p0, p0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const-string p0, "OTHER"

    return-object p0

    :cond_0
    const-string p0, "YES"

    return-object p0

    :cond_1
    const-string p0, "NO"

    return-object p0

    :cond_2
    const-string p0, "UNDEFINED"

    return-object p0
.end method

.method private listOfRunningApps()Ljava/lang/String;
    .locals 3

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->application:Landroid/app/Application;

    const-string v2, "activity"

    .line 243
    invoke-virtual {v1, v2}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 245
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 247
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 248
    iget-object v2, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 251
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildBugReport()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 99
    invoke-virtual {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->buildBugReport(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public buildBugReport(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    invoke-direct {p0, v0, p1}, Lcom/squareup/bugreport/BugReportBuilder;->appendStacktrace(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 105
    invoke-direct {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->appendUser(Ljava/lang/StringBuilder;)V

    .line 106
    invoke-direct {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->appendRuntime(Ljava/lang/StringBuilder;)V

    .line 107
    invoke-direct {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->appendDevice(Ljava/lang/StringBuilder;)V

    .line 108
    invoke-direct {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->appendFeatures(Ljava/lang/StringBuilder;)V

    .line 109
    invoke-direct {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->appendMisc(Ljava/lang/StringBuilder;)V

    .line 110
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    .line 111
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object p1
.end method

.method public createAttachments()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/bugreport/Attachment;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 217
    invoke-virtual {p0, v0}, Lcom/squareup/bugreport/BugReportBuilder;->createAttachments(Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public createAttachments(Ljava/io/File;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/bugreport/Attachment;",
            ">;"
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/squareup/bugreport/BugReportBuilder;->attachmentsBuilderFactory:Lcom/squareup/bugreport/AttachmentsBuilder$Factory;

    invoke-virtual {v0}, Lcom/squareup/bugreport/AttachmentsBuilder$Factory;->get()Lcom/squareup/bugreport/AttachmentsBuilder;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "screenshot.png"

    .line 224
    invoke-virtual {v0, v1, p1}, Lcom/squareup/bugreport/AttachmentsBuilder;->add(Ljava/lang/String;Ljava/io/File;)Lcom/squareup/bugreport/AttachmentsBuilder;

    .line 227
    :cond_0
    invoke-direct {p0}, Lcom/squareup/bugreport/BugReportBuilder;->listOfRunningApps()Ljava/lang/String;

    move-result-object p1

    const-string v1, "running-apps.txt"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/bugreport/AttachmentsBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/bugreport/AttachmentsBuilder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 228
    invoke-interface {v1}, Lcom/squareup/log/OhSnapLogger;->getLastEventsAsString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "oh-snap-log.txt"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/bugreport/AttachmentsBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/bugreport/AttachmentsBuilder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->viewHierarchyProvider:Ljavax/inject/Provider;

    .line 229
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v2, "view-hierarchy.txt"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/bugreport/AttachmentsBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/bugreport/AttachmentsBuilder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/bugreport/BugReportBuilder;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    .line 230
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "mortar-scope-hierarchy.txt"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/bugreport/AttachmentsBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/bugreport/AttachmentsBuilder;

    .line 233
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result p1

    if-nez p1, :cond_1

    .line 234
    invoke-virtual {v0}, Lcom/squareup/bugreport/AttachmentsBuilder;->addLogcat()V

    .line 237
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/bugreport/AttachmentsBuilder;->create()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
