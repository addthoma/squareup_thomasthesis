.class public final Lcom/squareup/bugreport/TwoFingerBugReporter;
.super Ljava/lang/Object;
.source "TwoFingerBugReporter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/bugreport/TwoFingerBugReporter;",
        "",
        "bugReporter",
        "Lcom/squareup/bugreport/BugReporter;",
        "bugReportBuilder",
        "Lcom/squareup/bugreport/BugReportBuilder;",
        "(Lcom/squareup/bugreport/BugReporter;Lcom/squareup/bugreport/BugReportBuilder;)V",
        "showDialogAndReport",
        "",
        "activity",
        "Landroid/app/Activity;",
        "screenshot",
        "Ljava/io/File;",
        "bugreport_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bugReportBuilder:Lcom/squareup/bugreport/BugReportBuilder;

.field private final bugReporter:Lcom/squareup/bugreport/BugReporter;


# direct methods
.method public constructor <init>(Lcom/squareup/bugreport/BugReporter;Lcom/squareup/bugreport/BugReportBuilder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bugReporter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bugReportBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/bugreport/TwoFingerBugReporter;->bugReporter:Lcom/squareup/bugreport/BugReporter;

    iput-object p2, p0, Lcom/squareup/bugreport/TwoFingerBugReporter;->bugReportBuilder:Lcom/squareup/bugreport/BugReportBuilder;

    return-void
.end method

.method public static synthetic showDialogAndReport$default(Lcom/squareup/bugreport/TwoFingerBugReporter;Landroid/app/Activity;Ljava/io/File;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 13
    check-cast p2, Ljava/io/File;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/bugreport/TwoFingerBugReporter;->showDialogAndReport(Landroid/app/Activity;Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method public final showDialogAndReport(Landroid/app/Activity;Ljava/io/File;)V
    .locals 3

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/squareup/bugreport/TwoFingerBugReporter;->bugReporter:Lcom/squareup/bugreport/BugReporter;

    .line 16
    iget-object v1, p0, Lcom/squareup/bugreport/TwoFingerBugReporter;->bugReportBuilder:Lcom/squareup/bugreport/BugReportBuilder;

    invoke-virtual {v1}, Lcom/squareup/bugreport/BugReportBuilder;->buildBugReport()Ljava/lang/String;

    move-result-object v1

    const-string v2, "bugReportBuilder.buildBugReport()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/bugreport/TwoFingerBugReporter;->bugReportBuilder:Lcom/squareup/bugreport/BugReportBuilder;

    invoke-virtual {v2, p2}, Lcom/squareup/bugreport/BugReportBuilder;->createAttachments(Ljava/io/File;)Ljava/util/List;

    move-result-object p2

    const-string v2, "bugReportBuilder.createAttachments(screenshot)"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/bugreport/BugReporter;->showTwoFingerBugReportDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method
