.class public final Lcom/squareup/bugreport/BugReportBuilder_Factory;
.super Ljava/lang/Object;
.source "BugReportBuilder_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/bugreport/BugReportBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final attachmentsBuilderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/bugreport/AttachmentsBuilder$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featureFlagsForLogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetConnectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mortarScopeHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final userAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final userSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final versionCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final versionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final viewHierarchyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/bugreport/AttachmentsBuilder$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 76
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->applicationProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 77
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 78
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 79
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 80
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 81
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->headsetConnectionProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 82
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->resourcesProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 83
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->serverProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 84
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 85
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->viewHierarchyProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 86
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 87
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 88
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->userAgentProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 89
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->versionNameProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 90
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->versionCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 91
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->userSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 92
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->attachmentsBuilderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 93
    iput-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->posBuildProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/bugreport/BugReportBuilder_Factory;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/bugreport/AttachmentsBuilder$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;)",
            "Lcom/squareup/bugreport/BugReportBuilder_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 113
    new-instance v19, Lcom/squareup/bugreport/BugReportBuilder_Factory;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/bugreport/BugReportBuilder_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v19
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/wavpool/swipe/HeadsetConnection;Landroid/content/res/Resources;Lcom/squareup/http/Server;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/util/Device;Ljava/lang/String;Ljava/lang/String;ILcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/bugreport/AttachmentsBuilder$Factory;Lcom/squareup/util/PosBuild;)Lcom/squareup/bugreport/BugReportBuilder;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnection;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/http/Server;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            "Lcom/squareup/bugreport/AttachmentsBuilder$Factory;",
            "Lcom/squareup/util/PosBuild;",
            ")",
            "Lcom/squareup/bugreport/BugReportBuilder;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 124
    new-instance v19, Lcom/squareup/bugreport/BugReportBuilder;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/bugreport/BugReportBuilder;-><init>(Landroid/app/Application;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/wavpool/swipe/HeadsetConnection;Landroid/content/res/Resources;Lcom/squareup/http/Server;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/util/Device;Ljava/lang/String;Ljava/lang/String;ILcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/bugreport/AttachmentsBuilder$Factory;Lcom/squareup/util/PosBuild;)V

    return-object v19
.end method


# virtual methods
.method public get()Lcom/squareup/bugreport/BugReportBuilder;
    .locals 20

    move-object/from16 v0, p0

    .line 98
    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/log/OhSnapLogger;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->featureFlagsForLogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/log/FeatureFlagsForLogs;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->headsetConnectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/wavpool/swipe/HeadsetConnection;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/content/res/Resources;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/http/Server;

    iget-object v10, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->mortarScopeHierarchyProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->viewHierarchyProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->userAgentProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->versionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Ljava/lang/String;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->versionCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v16

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->userSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/settings/server/UserSettingsProvider;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->attachmentsBuilderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/bugreport/AttachmentsBuilder$Factory;

    iget-object v1, v0, Lcom/squareup/bugreport/BugReportBuilder_Factory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/util/PosBuild;

    invoke-static/range {v2 .. v19}, Lcom/squareup/bugreport/BugReportBuilder_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/wavpool/swipe/HeadsetConnection;Landroid/content/res/Resources;Lcom/squareup/http/Server;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/util/Device;Ljava/lang/String;Ljava/lang/String;ILcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/bugreport/AttachmentsBuilder$Factory;Lcom/squareup/util/PosBuild;)Lcom/squareup/bugreport/BugReportBuilder;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/bugreport/BugReportBuilder_Factory;->get()Lcom/squareup/bugreport/BugReportBuilder;

    move-result-object v0

    return-object v0
.end method
