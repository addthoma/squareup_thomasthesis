.class public Lcom/squareup/padlock/PadlockAccessibilityDelegate;
.super Landroidx/customview/widget/ExploreByTouchHelper;
.source "PadlockAccessibilityDelegate.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPadlockAccessibilityDelegate.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PadlockAccessibilityDelegate.kt\ncom/squareup/padlock/PadlockAccessibilityDelegate\n*L\n1#1,104:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0010\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0008\u0012\u00060\u0007R\u00020\u00080\u0005\u00a2\u0006\u0002\u0010\tJ\u0016\u0010\n\u001a\u0008\u0018\u00010\u0007R\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0018\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0014J\u0016\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u0014H\u0014J\"\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0017\u001a\u00020\u000c2\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0014J\u0018\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u001b\u001a\u00020\u001cH\u0014J\u0018\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u001fH\u0014R\u001e\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0008\u0012\u00060\u0007R\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/padlock/PadlockAccessibilityDelegate;",
        "Landroidx/customview/widget/ExploreByTouchHelper;",
        "parent",
        "Landroid/view/View;",
        "keypadConfig",
        "",
        "Lcom/squareup/padlock/Padlock$Key;",
        "Lcom/squareup/padlock/Padlock$ButtonInfo;",
        "Lcom/squareup/padlock/Padlock;",
        "(Landroid/view/View;Ljava/util/Map;)V",
        "findButtonInfoForVirtualView",
        "virtualViewId",
        "",
        "getVirtualViewAt",
        "x",
        "",
        "y",
        "getVisibleVirtualViews",
        "",
        "virtualViewIds",
        "",
        "onPerformActionForVirtualView",
        "",
        "action",
        "bundle",
        "Landroid/os/Bundle;",
        "onPopulateEventForVirtualView",
        "event",
        "Landroid/view/accessibility/AccessibilityEvent;",
        "onPopulateNodeForVirtualView",
        "node",
        "Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;",
        "padlock_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final keypadConfig:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/padlock/Padlock$Key;",
            "Lcom/squareup/padlock/Padlock$ButtonInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/Map<",
            "Lcom/squareup/padlock/Padlock$Key;",
            "+",
            "Lcom/squareup/padlock/Padlock$ButtonInfo;",
            ">;)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "keypadConfig"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1}, Landroidx/customview/widget/ExploreByTouchHelper;-><init>(Landroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->keypadConfig:Ljava/util/Map;

    return-void
.end method

.method private final findButtonInfoForVirtualView(I)Lcom/squareup/padlock/Padlock$ButtonInfo;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->keypadConfig:Ljava/util/Map;

    invoke-static {p1}, Lcom/squareup/padlock/PadlockAccessibilityDelegateKt;->access$toKey(I)Lcom/squareup/padlock/Padlock$Key;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    return-object p1
.end method


# virtual methods
.method protected getVirtualViewAt(FF)I
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->keypadConfig:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 37
    sget-object v3, Lcom/squareup/padlock/Padlock$Key;->UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

    if-ne v2, v3, :cond_1

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    invoke-static {v2}, Lcom/squareup/padlock/PadlockAccessibilityDelegateKt;->toVirtualViewId(Lcom/squareup/padlock/Padlock$Key;)I

    move-result p1

    return p1

    :cond_2
    const/high16 p1, -0x80000000

    return p1
.end method

.method protected getVisibleVirtualViews(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "virtualViewIds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->keypadConfig:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 22
    sget-object v3, Lcom/squareup/padlock/Padlock$Key;->UNKNOWN:Lcom/squareup/padlock/Padlock$Key;

    if-ne v2, v3, :cond_1

    goto :goto_0

    .line 25
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26
    invoke-static {v2}, Lcom/squareup/padlock/PadlockAccessibilityDelegateKt;->toVirtualViewId(Lcom/squareup/padlock/Padlock$Key;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
    .locals 1

    const/4 p3, 0x0

    const/16 v0, 0x10

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 78
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->findButtonInfoForVirtualView(I)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object p2

    if-eqz p2, :cond_1

    .line 80
    invoke-virtual {p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->centerX()F

    move-result p3

    invoke-virtual {p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->centerY()F

    move-result v0

    invoke-virtual {p2, p3, v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->click(FF)V

    .line 83
    invoke-virtual {p0, p1}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->invalidateVirtualView(I)V

    const/4 p2, 0x1

    .line 84
    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->sendEventForVirtualView(II)Z

    return p2

    .line 86
    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "ACTION_CLICK delivered to unmapped virtualViewId: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array p2, p3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return p3
.end method

.method protected onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->findButtonInfoForVirtualView(I)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getDescription()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 54
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No button found corresponding to virtualViewId: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method protected onPopulateNodeForVirtualView(ILandroidx/core/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1

    const-string v0, "node"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0, p1}, Lcom/squareup/padlock/PadlockAccessibilityDelegate;->findButtonInfoForVirtualView(I)Lcom/squareup/padlock/Padlock$ButtonInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getDescription()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 65
    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getLocation()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    const/16 p1, 0x10

    .line 66
    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    goto :goto_0

    .line 68
    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "No button found corresponding to virtualViewId: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ltimber/log/Timber;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
