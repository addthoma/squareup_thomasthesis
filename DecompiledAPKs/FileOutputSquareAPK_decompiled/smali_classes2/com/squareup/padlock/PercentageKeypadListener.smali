.class public abstract Lcom/squareup/padlock/PercentageKeypadListener;
.super Lcom/squareup/padlock/BaseUpdatingKeypadListener;
.source "PercentageKeypadListener.java"


# static fields
.field private static final DECIMAL:C = '.'

.field private static final ONE_HUNDRED:Ljava/lang/String; = "100"

.field private static final ZERO:C = '0'


# instance fields
.field private final percentBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/os/Vibrator;)V
    .locals 0

    .line 18
    invoke-direct {p0, p2, p1}, Lcom/squareup/padlock/BaseUpdatingKeypadListener;-><init>(Landroid/os/Vibrator;Lcom/squareup/padlock/Padlock;)V

    .line 19
    new-instance p1, Ljava/lang/StringBuilder;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object p1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    return-void
.end method


# virtual methods
.method protected backspaceEnabled()Z
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected decimalIndex()I
    .locals 3

    const/4 v0, 0x0

    .line 107
    :goto_0
    iget-object v1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 108
    iget-object v1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    const/16 v2, 0x2e

    if-ne v1, v2, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method protected digitsEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected getPercentage()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackspaceClicked()V
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-gtz v0, :cond_0

    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 75
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->update()V

    .line 76
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/squareup/padlock/PercentageKeypadListener;->onPercentUpdated(Ljava/lang/String;Z)V

    return-void
.end method

.method public onCancelClicked()V
    .locals 0

    return-void
.end method

.method public onClearClicked()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->update()V

    .line 69
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/PercentageKeypadListener;->onPercentUpdated(Ljava/lang/String;Z)V

    return-void
.end method

.method public onClearLongpressed()V
    .locals 2

    const-wide/16 v0, 0x32

    .line 58
    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/PercentageKeypadListener;->vibrate(J)V

    .line 59
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 60
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->update()V

    .line 62
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/PercentageKeypadListener;->onPercentUpdated(Ljava/lang/String;Z)V

    return-void
.end method

.method public onDecimalClicked()V
    .locals 2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->decimalIndex()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->update()V

    .line 103
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/padlock/PercentageKeypadListener;->onPercentUpdated(Ljava/lang/String;Z)V

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 6

    .line 25
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->decimalIndex()I

    move-result v0

    .line 26
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->getPercentage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    .line 28
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_0

    .line 30
    iget-object p1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 31
    iget-object p1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    const-string v0, "100"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    if-eq v0, v2, :cond_1

    .line 34
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_4

    .line 38
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 42
    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    invoke-virtual {v1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_3

    if-nez p1, :cond_2

    .line 45
    iget-object p1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v3}, Lcom/squareup/padlock/PercentageKeypadListener;->onPercentUpdated(Ljava/lang/String;Z)V

    return-void

    .line 49
    :cond_2
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 51
    :cond_3
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    :cond_4
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->update()V

    .line 54
    iget-object p1, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v3}, Lcom/squareup/padlock/PercentageKeypadListener;->onPercentUpdated(Ljava/lang/String;Z)V

    return-void
.end method

.method public abstract onPercentUpdated(Ljava/lang/String;Z)V
.end method

.method public onPinDigitEntered(FF)V
    .locals 0

    return-void
.end method

.method public onSkipClicked()V
    .locals 0

    return-void
.end method

.method protected submitEnabled()Z
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/padlock/PercentageKeypadListener;->percentBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected update()V
    .locals 0

    .line 130
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->updateBackspaceState()V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->updateSubmitState()V

    .line 132
    invoke-virtual {p0}, Lcom/squareup/padlock/PercentageKeypadListener;->updateDigitsState()V

    return-void
.end method
