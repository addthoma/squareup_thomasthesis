.class public final Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;
.super Ljava/lang/Object;
.source "CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;->contextProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideLocaleOverrideFactory(Landroid/app/Application;Ljava/util/Locale;)Lcom/squareup/locale/LocaleOverrideFactory;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/CommonLoggedInModule;->provideLocaleOverrideFactory(Landroid/app/Application;Ljava/util/Locale;)Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/locale/LocaleOverrideFactory;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/locale/LocaleOverrideFactory;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;->provideLocaleOverrideFactory(Landroid/app/Application;Ljava/util/Locale;)Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/CommonLoggedInModule_ProvideLocaleOverrideFactoryFactory;->get()Lcom/squareup/locale/LocaleOverrideFactory;

    move-result-object v0

    return-object v0
.end method
