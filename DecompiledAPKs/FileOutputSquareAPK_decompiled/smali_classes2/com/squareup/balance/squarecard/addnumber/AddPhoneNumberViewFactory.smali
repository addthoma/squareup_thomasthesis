.class public final Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "AddPhoneNumberViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "enterPhoneNumberCoordinator",
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;",
        "(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enterPhoneNumberCoordinator:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "enterPhoneNumberCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 12
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 13
    sget-object v2, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;->Companion:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 14
    sget v3, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_enter_phone_number_view:I

    .line 15
    new-instance v4, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 12
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 11
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;->enterPhoneNumberCoordinator:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;

    return-void
.end method
