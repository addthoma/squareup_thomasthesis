.class public final Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "SquareCardCreatePinCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->watchInputs(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;",
            ")V"
        }
    .end annotation

    .line 46
    iput-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;->this$0:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;->$data:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;->this$0:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator$watchInputs$textWatcher$1;->$data:Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;->access$updateFinishButton(Lcom/squareup/balance/squarecard/common/SquareCardCreatePinCoordinator;Lcom/squareup/balance/squarecard/common/SquareCardCreatePinScreen$ScreenData;)V

    return-void
.end method
