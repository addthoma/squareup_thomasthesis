.class public final Lcom/squareup/balance/squarecard/common/SquareCardProgress;
.super Ljava/lang/Object;
.source "SquareCardProgressScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;,
        Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0007\u001a\u00020\u0001H\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress;",
        "",
        "()V",
        "createKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "uniquifier",
        "Event",
        "ScreenData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardProgress;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/common/SquareCardProgress;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardProgress;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createKey(Ljava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use BizBankProgressScreen"
    .end annotation

    const-string v0, "uniquifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lcom/squareup/balance/squarecard/common/SquareCardProgress;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x2d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p1, v1, v2, v1}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
