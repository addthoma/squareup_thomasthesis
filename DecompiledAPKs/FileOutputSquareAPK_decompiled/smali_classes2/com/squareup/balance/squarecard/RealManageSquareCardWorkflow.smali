.class public final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealManageSquareCardWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$ManageSquareCardEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealManageSquareCardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealManageSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/RealManageSquareCardWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,325:1\n85#2:326\n240#3:327\n276#4:328\n*E\n*S KotlinDebug\n*F\n+ 1 RealManageSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/RealManageSquareCardWorkflow\n*L\n134#1:326\n134#1:327\n134#1:328\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008f\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001\u001c\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u0002:\u00016BG\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u0014\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001fH\u0002J\u0016\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00040!2\u0006\u0010\"\u001a\u00020#H\u0002J\u0014\u0010$\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001fH\u0002J\u0014\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001fH\u0002J\u001f\u0010&\u001a\u00020\u00042\u0006\u0010\'\u001a\u00020\u00032\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0016\u00a2\u0006\u0002\u0010*J\u001c\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001f2\u0006\u0010,\u001a\u00020-H\u0002J\u0014\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u001fH\u0002J\u0008\u0010/\u001a\u00020\u0003H\u0002JW\u00100\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t2\u0006\u0010\'\u001a\u00020\u00032\u0006\u00101\u001a\u00020\u00042\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u000303H\u0016\u00a2\u0006\u0002\u00104J\u0010\u00105\u001a\u00020)2\u0006\u00101\u001a\u00020\u0004H\u0016R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001dR\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "stateFactory",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "device",
        "Lcom/squareup/util/Device;",
        "analytics",
        "Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settingsAppletGateway",
        "Lcom/squareup/ui/settings/SettingsAppletGateway;",
        "cardDataRequester",
        "Lcom/squareup/balance/squarecard/SquareCardDataRequester;",
        "instantDepositRunner",
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "(Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/SettingsAppletGateway;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/instantdeposit/InstantDepositRunner;)V",
        "logCardInfoUnavailable",
        "com/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1",
        "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1;",
        "activateSettingsApplet",
        "Lcom/squareup/workflow/WorkflowAction;",
        "fetchCardStatus",
        "Lio/reactivex/Single;",
        "refreshBalance",
        "",
        "finish",
        "finishOrRefetchCardStatus",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "onCanceledCard",
        "canceled",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled;",
        "refetchCardOrAddPhoneNumber",
        "refreshAccountStatusResponse",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/balance/squarecard/ManageSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "ManageSquareCardEvent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;

.field private final cardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

.field private final logCardInfoUnavailable:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

.field private final stateFactory:Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/SettingsAppletGateway;Lcom/squareup/balance/squarecard/SquareCardDataRequester;Lcom/squareup/instantdeposit/InstantDepositRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "stateFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settingsAppletGateway"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardDataRequester"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instantDepositRunner"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->stateFactory:Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->analytics:Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    iput-object p7, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->cardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    iput-object p8, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    .line 251
    new-instance p1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->logCardInfoUnavailable:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1;

    return-void
.end method

.method public static final synthetic access$activateSettingsApplet(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->activateSettingsApplet()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$finish(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->finish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$finishOrRefetchCardStatus(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->finishOrRefetchCardStatus()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->analytics:Lcom/squareup/balance/squarecard/ManageSquareCardAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getStateFactory$p(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;
    .locals 0

    .line 104
    iget-object p0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->stateFactory:Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    return-object p0
.end method

.method public static final synthetic access$onCanceledCard(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 104
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->onCanceledCard(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$refetchCardOrAddPhoneNumber(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->refetchCardOrAddPhoneNumber()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$refreshAccountStatusResponse(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->refreshAccountStatusResponse()V

    return-void
.end method

.method private final activateSettingsApplet()Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 319
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->settingsAppletGateway:Lcom/squareup/ui/settings/SettingsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/settings/SettingsAppletGateway;->activate()V

    .line 320
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->finish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method private final fetchCardStatus(Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 260
    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->cardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/SquareCardDataRequester;->fetchCardStatus()Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    .line 261
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->instantDepositRunner:Lcom/squareup/instantdeposit/InstantDepositRunner;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/instantdeposit/InstantDepositRunner;->checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;

    move-result-object v0

    check-cast v0, Lio/reactivex/SingleSource;

    .line 262
    sget-object v1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;->INSTANCE:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;

    check-cast v1, Lio/reactivex/functions/BiFunction;

    .line 259
    invoke-static {p1, v0, v1}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "zip(\n          cardDataR\u2026us, _ -> status }\n      )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 265
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->cardDataRequester:Lcom/squareup/balance/squarecard/SquareCardDataRequester;

    invoke-interface {p1}, Lcom/squareup/balance/squarecard/SquareCardDataRequester;->fetchCardStatus()Lio/reactivex/Single;

    move-result-object p1

    .line 269
    :goto_0
    new-instance v0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$1;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "combined\n        .map { \u2026ure\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final finish()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 323
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method private final finishOrRefetchCardStatus()Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 310
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->finish()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 314
    :cond_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;-><init>(Z)V

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final onCanceledCard(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 281
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->refreshAccountStatusResponse()V

    .line 283
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccess;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->finishOrRefetchCardStatus()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 284
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccessWithReorder;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput$Canceled$CanceledSuccessWithReorder;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 285
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->stateFactory:Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->startOrderingCard(Z)Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 284
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final refetchCardOrAddPhoneNumber()Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_COLLECT_MOBILE_PHONE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->stateFactory:Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->startAddPhoneNumber()Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    goto :goto_0

    .line 305
    :cond_0
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->finishOrRefetchCardStatus()Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private final refreshAccountStatusResponse()V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->refresh()V

    return-void
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ManageSquareCardState;
    .locals 2

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 125
    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->stateFactory:Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;

    invoke-virtual {p1, p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$Factory;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ManageSquareCardState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/ManageSquareCardState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/ManageSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/ManageSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 134
    check-cast p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;->getRefreshBalance()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->fetchCardStatus(Z)Lio/reactivex/Single;

    move-result-object p1

    .line 326
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$$inlined$asWorker$1;

    invoke-direct {p2, p1, v0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 327
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 328
    const-class p2, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 134
    sget-object p1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$1;->INSTANCE:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 138
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 139
    new-instance p2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 140
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->fetching_card_details_spinner_message:I

    invoke-direct {v0, v1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 141
    new-instance v1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$2;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$2;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 139
    invoke-direct {p2, v0, p3}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 147
    invoke-static {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asDetailScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 138
    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 151
    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;->INSTANCE:Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatusFailure;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 152
    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->logCardInfoUnavailable:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$logCardInfoUnavailable$1;

    check-cast p1, Lcom/squareup/workflow/Worker;

    const/4 p2, 0x2

    invoke-static {p3, p1, v0, p2, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 155
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 156
    new-instance p2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 157
    new-instance v7, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 158
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_fetching_info_error_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 159
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_fetching_info_error_message:I

    invoke-direct {v0, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 160
    sget v3, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    .line 157
    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v7, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 162
    new-instance v0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 156
    invoke-direct {p2, v7, p3}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 169
    invoke-static {p2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asDetailScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 155
    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_1

    .line 173
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    if-eqz p1, :cond_3

    .line 174
    check-cast p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;->getSubWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 175
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$OrderingCard;->getSkipInitialScreens()Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardInput$SkipInitialScreens;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardInput$SkipInitialScreens;

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardInput$DoNotSkipInitialScreens;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardInput$DoNotSkipInitialScreens;

    :goto_0
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardInput;

    move-object v2, p1

    const/4 v3, 0x0

    .line 176
    new-instance p1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$4;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 173
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 184
    :cond_3
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;

    if-eqz p1, :cond_4

    check-cast p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$AddPhoneNumber;->getSubWorkflow()Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberWorkflow;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$5;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$5;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 188
    :cond_4
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    if-eqz p1, :cond_5

    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;->getSubWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v3

    const/4 v4, 0x0

    new-instance p1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;

    invoke-direct {p1, p0, p2}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$6;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;Lcom/squareup/balance/squarecard/ManageSquareCardState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto/16 :goto_1

    .line 201
    :cond_5
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    if-eqz p1, :cond_6

    .line 202
    check-cast p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getSubWorkflow()Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflow;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 203
    new-instance v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;

    .line 204
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CardActivated;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 205
    iget-object p2, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SQUARE_CARD_SHOW_RESET_PIN:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    .line 206
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_CARD_FREEZE_TOGGLE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    .line 207
    iget-object v3, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->features:Lcom/squareup/settings/server/Features;

    .line 208
    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_SHOW_NOTIFICATION_PREFERENCES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    .line 203
    invoke-direct {v2, p1, v0, p2, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;ZZZ)V

    const/4 v3, 0x0

    .line 210
    new-instance p1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$7;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$7;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 201
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_1

    .line 221
    :cond_6
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;

    if-eqz p1, :cond_7

    .line 222
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;->getSubWorkflow()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingActivatedCard;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 223
    new-instance v1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$8;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$8;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;Lcom/squareup/balance/squarecard/ManageSquareCardState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const-string p2, "activated"

    .line 221
    invoke-interface {p3, v0, p1, p2, v1}, Lcom/squareup/workflow/RenderContext;->renderChild(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_1

    .line 234
    :cond_7
    instance-of p1, p2, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;

    if-eqz p1, :cond_9

    .line 235
    move-object p1, p2

    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;->getSubWorkflow()Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsV2Workflow$CancelingOrderedCard;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 236
    new-instance v1, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$9;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$9;-><init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;Lcom/squareup/balance/squarecard/ManageSquareCardState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const-string p2, "ordered"

    .line 234
    invoke-interface {p3, v0, p1, p2, v1}, Lcom/squareup/workflow/RenderContext;->renderChild(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_1
    if-eqz p1, :cond_8

    goto :goto_2

    .line 246
    :cond_8
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    :goto_2
    return-object p1

    .line 234
    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/ManageSquareCardState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ManageSquareCardState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/balance/squarecard/ManageSquareCardState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/ManageSquareCardState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
