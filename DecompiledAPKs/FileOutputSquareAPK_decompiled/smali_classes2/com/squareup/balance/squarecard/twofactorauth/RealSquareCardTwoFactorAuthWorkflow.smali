.class public final Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSquareCardTwoFactorAuthWorkflow.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquareCardTwoFactorAuthWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquareCardTwoFactorAuthWorkflow.kt\ncom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 5 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,190:1\n85#2:191\n85#2:194\n240#3:192\n240#3:195\n276#4:193\n276#4:196\n149#5,5:197\n*E\n*S KotlinDebug\n*F\n+ 1 RealSquareCardTwoFactorAuthWorkflow.kt\ncom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow\n*L\n70#1:191\n83#1:194\n70#1:192\n83#1:195\n70#1:193\n83#1:196\n187#1,5:197\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ:\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u0008j\u0008\u0012\u0004\u0012\u00020\u000f`\u00112\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00132\u0006\u0010\u0014\u001a\u00020\u0003H\u0002J:\u0010\u0015\u001a\u0018\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00100\u0008j\u0008\u0012\u0004\u0012\u00020\u0016`\u00112\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00132\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0014\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001aH\u0002J\u0014\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001aH\u0002J\u001a\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u00032\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u001e\u0010 \u001a\u0018\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00100\u0008j\u0008\u0012\u0004\u0012\u00020\u0016`\u0011H\u0002JR\u0010!\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010\u001d\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00042\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0013H\u0016J\u0010\u0010\"\u001a\u00020\u001f2\u0006\u0010\u0017\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "twoFactorAuthDataStore",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
        "(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)V",
        "confirmationCodeScreen",
        "Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "input",
        "errorScreen",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "state",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;",
        "finishAndGoBack",
        "Lcom/squareup/workflow/WorkflowAction;",
        "finishWithSuccess",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "loadingScreen",
        "render",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final twoFactorAuthDataStore:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "twoFactorAuthDataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->twoFactorAuthDataStore:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;

    return-void
.end method

.method public static final synthetic access$finishAndGoBack(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->finishAndGoBack()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$finishWithSuccess(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->finishWithSuccess()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final confirmationCodeScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "-",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

    .line 174
    new-instance v7, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;

    .line 175
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v2

    .line 176
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_auth_enter_code_message:I

    .line 178
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->getToolbarTitle()I

    move-result v3

    .line 179
    sget v6, Lcom/squareup/common/strings/R$string;->next:I

    const/4 v4, -0x1

    move-object v1, v7

    .line 174
    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;IIII)V

    .line 181
    new-instance p2, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$confirmationCodeScreen$1;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$confirmationCodeScreen$1;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, p2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 173
    invoke-direct {v0, v7, p1}, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;-><init>(Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 198
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 199
    const-class p2, Lcom/squareup/balance/squarecard/common/SquareCardConfirmationCodeScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 200
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 198
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final errorScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;)Lcom/squareup/workflow/legacy/Screen;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "-",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 133
    instance-of v0, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$ExpiredToken;

    if-eqz v0, :cond_0

    .line 134
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_error_expired_token_title:I

    .line 135
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_error_expired_token_message:I

    goto :goto_0

    .line 137
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$InvalidToken;

    if-eqz v0, :cond_1

    .line 138
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_error_invalid_token_title:I

    .line 139
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_error_invalid_token_message:I

    goto :goto_0

    .line 141
    :cond_1
    instance-of v0, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$GenericFailure;

    if-eqz v0, :cond_2

    .line 142
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_error_generic_title:I

    .line 143
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_error_generic_message:I

    .line 147
    :goto_0
    new-instance v2, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 148
    new-instance v10, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;

    .line 149
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v3, v0}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v4, v3

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 150
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/ViewString;

    .line 151
    sget v6, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v10

    .line 148
    invoke-direct/range {v3 .. v9}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v10, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 153
    new-instance v0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 147
    invoke-direct {v2, v10, p1}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 166
    invoke-static {v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1

    .line 143
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final finishAndGoBack()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;"
        }
    .end annotation

    .line 111
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult$Back;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult$Back;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method private final finishWithSuccess()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;"
        }
    .end annotation

    .line 106
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult$TwoFactorCompleted;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult$TwoFactorCompleted;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method private final loadingScreen()Lcom/squareup/workflow/legacy/Screen;
    .locals 3

    .line 115
    new-instance v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;

    .line 116
    new-instance v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_two_factor_auth_sending_code:I

    invoke-direct {v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData$Loading;-><init>(I)V

    check-cast v1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;

    .line 117
    new-instance v2, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$loadingScreen$1;

    invoke-direct {v2, p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$loadingScreen$1;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 115
    invoke-direct {v0, v1, v2}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;-><init>(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$ScreenData;Lkotlin/jvm/functions/Function1;)V

    .line 122
    invoke-static {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 60
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthSerializer;

    invoke-virtual {p1, p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthSerializer;->deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$IssuingSquareCardAuthToken;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$IssuingSquareCardAuthToken;

    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->initialState(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    check-cast p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->render(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "-",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    instance-of v0, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$IssuingSquareCardAuthToken;

    const/4 v1, 0x0

    const-string v2, "props.cardData.card_token"

    if-eqz v0, :cond_0

    .line 70
    iget-object p2, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->twoFactorAuthDataStore:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;->startTwoFactorAuth(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 191
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$$inlined$asWorker$1;

    invoke-direct {p2, p1, v1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 192
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 193
    const-class p2, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 71
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    .line 69
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->loadingScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 77
    :cond_0
    instance-of v0, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$EnterConfirmationCode;

    if-eqz v0, :cond_1

    invoke-direct {p0, p3, p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->confirmationCodeScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 78
    :cond_1
    instance-of v0, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$SubmittingConfirmationCode;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->twoFactorAuthDataStore:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;

    .line 81
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    check-cast p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$SubmittingConfirmationCode;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$SubmittingConfirmationCode;->getCode()Ljava/lang/String;

    move-result-object p2

    .line 80
    invoke-interface {v0, p1, p2}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;->verifyTwoFactorAuth(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 194
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$$inlined$asWorker$2;

    invoke-direct {p2, p1, v1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$$inlined$asWorker$2;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 195
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 196
    const-class p2, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 84
    new-instance p1, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$2;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$2;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    .line 79
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 93
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->loadingScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_2
    instance-of p1, p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;

    if-eqz p1, :cond_3

    check-cast p2, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;

    invoke-direct {p0, p3, p2}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->errorScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 98
    :goto_0
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 95
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthSerializer;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthSerializer;->snapshotState(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
