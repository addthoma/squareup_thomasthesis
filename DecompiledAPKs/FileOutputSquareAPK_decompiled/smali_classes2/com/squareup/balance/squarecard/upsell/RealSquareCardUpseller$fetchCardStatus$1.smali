.class final Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$fetchCardStatus$1;
.super Ljava/lang/Object;
.source "RealSquareCardUpseller.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->fetchCardStatus()Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$fetchCardStatus$1;->this$0:Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/balance/squarecard/CardStatus;)V
    .locals 1

    .line 87
    instance-of v0, p1, Lcom/squareup/balance/squarecard/CardStatus$Failure;

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$fetchCardStatus$1;->this$0:Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;->access$getCardStatus$p(Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/upsell/RealSquareCardUpseller$fetchCardStatus$1;->accept(Lcom/squareup/balance/squarecard/CardStatus;)V

    return-void
.end method
