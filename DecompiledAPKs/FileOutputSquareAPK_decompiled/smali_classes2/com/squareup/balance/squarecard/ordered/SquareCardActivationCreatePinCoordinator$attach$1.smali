.class final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator$attach$1;
.super Ljava/lang/Object;
.source "SquareCardActivationCreatePinCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinScreen;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator$attach$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$ScreenData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePin$Event;",
            ">;)V"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator$attach$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator$attach$1;->$view:Landroid/view/View;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;->access$update(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCreatePinCoordinator$attach$1;->accept(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method
