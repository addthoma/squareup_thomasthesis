.class final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardOrderedReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromSquareCardProgressScreen;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;",
        "it",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromSquareCardProgressScreen;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromSquareCardProgressScreen;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromSquareCardProgressScreen;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 392
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 393
    new-instance v8, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    .line 394
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->getActivationToken()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->getExpiration()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->getCvv()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 393
    invoke-direct/range {v0 .. v7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 392
    invoke-direct {p1, v8}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromSquareCardProgressScreen;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$14$1;->invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromSquareCardProgressScreen;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
