.class public interface abstract Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "SquareCardOrderedScreenWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J@\u0010\u0002\u001a:\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0003j\u0002`\u000bH\u0016JD\u0010\u000c\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u00060\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00050\rj\u0002`\u00102\u0006\u0010\u0011\u001a\u00020\u0004H&JD\u0010\u000c\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u00060\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00050\rj\u0002`\u00102\u0006\u0010\u0012\u001a\u00020\u0013H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflowStarter;",
        "",
        "adapter",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAdaptedWorkflow;",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedScreenWorkflow;",
        "startArg",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract adapter()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end method

.method public abstract start(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            ">;"
        }
    .end annotation
.end method
