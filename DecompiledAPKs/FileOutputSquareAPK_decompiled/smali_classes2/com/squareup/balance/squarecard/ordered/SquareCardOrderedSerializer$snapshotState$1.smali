.class final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardOrderedSerializer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->snapshotState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 44
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 45
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    .line 46
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingCardDetails;

    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 47
    :cond_0
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingDepositsInfo;

    if-eqz v1, :cond_1

    goto/16 :goto_0

    .line 48
    :cond_1
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$StartingCardActivationFailed;

    if-eqz v1, :cond_2

    goto/16 :goto_0

    .line 49
    :cond_2
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$ShowingSquareCardActivationComplete;

    if-eqz v1, :cond_3

    goto/16 :goto_0

    .line 50
    :cond_3
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForActivationCode;

    if-eqz v1, :cond_4

    goto/16 :goto_0

    .line 51
    :cond_4
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    if-eqz v1, :cond_5

    .line 52
    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getActivationToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 53
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getExpiration()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 54
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCvv()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 56
    :cond_5
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForPin;->isBillingEnabled()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 57
    :cond_6
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    if-eqz v1, :cond_7

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCodeFailed;->getReason()Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 58
    :cond_7
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    if-eqz v1, :cond_8

    .line 59
    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->isIncorrectCardInfo()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 60
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->getActivationToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 61
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->getExpiration()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 62
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingSquareCardInfoFailed;->getCvv()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    goto :goto_0

    .line 64
    :cond_8
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    if-eqz v1, :cond_9

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$SettingPinFailed;->isWeakPasscode()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_0

    .line 65
    :cond_9
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    if-eqz v1, :cond_a

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingToConfirmAddressInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/address/AddressSerializationKt;->writeAddress(Lokio/BufferedSink;Lcom/squareup/address/Address;)V

    goto :goto_0

    .line 66
    :cond_a
    instance-of v1, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;

    if-eqz v1, :cond_b

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingCardAddress;->getSubmittedAddress()Lcom/squareup/address/Address;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/address/AddressSerializationKt;->writeAddress(Lokio/BufferedSink;Lcom/squareup/address/Address;)V

    goto :goto_0

    .line 67
    :cond_b
    instance-of v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    if-eqz v0, :cond_c

    .line 68
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getPreviousState()Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer;->snapshotState(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    .line 69
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 70
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    check-cast v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$MissingInformationWarning;->getMessage()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    :cond_c
    :goto_0
    return-void
.end method
