.class final Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;
.super Lkotlin/jvm/internal/Lambda;
.source "CancelSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->render(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "+",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        "output",
        "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithoutAction;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithoutAction;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;

    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->CARD_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;-><init>(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 154
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithSuccess;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;

    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;->BALANCE_DEACTIVATION:Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;

    invoke-direct {p1, v0}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReadyForFeedback;-><init>(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest$FeedbackSource;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 155
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$ExitWithFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithFailure;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithFailure;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 156
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$Aborted;->INSTANCE:Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput$Aborted;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithSuccess;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ExitWithSuccess;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$render$sheet$4;->invoke(Lcom/squareup/balance/squarecard/cancelbizbank/MaybeCancelBizbankOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
