.class final Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "CancelingSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;->render(Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;->this$0:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;->invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {p1}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2;->this$0:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow;

    sget-object v1, Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/canceling/CancelingSquareCardWorkflow$render$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3, v2}, Lcom/squareup/workflow/StatelessWorkflowKt;->action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
