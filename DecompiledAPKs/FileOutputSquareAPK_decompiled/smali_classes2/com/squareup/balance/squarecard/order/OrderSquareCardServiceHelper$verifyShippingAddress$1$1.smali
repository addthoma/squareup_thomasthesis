.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardServiceHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    if-nez v0, :cond_0

    .line 58
    new-instance p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1

    .line 66
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->address_normalization_result:Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;

    if-eqz v0, :cond_4

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse$AddressNormalizationResult;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    .line 70
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->UNKNOWN_STATUS:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    goto :goto_0

    .line 69
    :cond_1
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->FAILED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    goto :goto_0

    .line 68
    :cond_2
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATION_SERVICE_ERROR:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    goto :goto_0

    .line 67
    :cond_3
    sget-object v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->VALIDATED:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    .line 80
    :goto_0
    new-instance v2, Lcom/squareup/protos/client/Status$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/Status$Builder;-><init>()V

    .line 81
    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, v3, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Status$Builder;->localized_title(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v2

    .line 82
    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, v3, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Status$Builder;->localized_description(Ljava/lang/String;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v2

    .line 83
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/Status$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/Status$Builder;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Lcom/squareup/protos/client/Status$Builder;->build()Lcom/squareup/protos/client/Status;

    move-result-object v1

    .line 85
    new-instance v2, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;-><init>()V

    .line 86
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    move-result-object v1

    .line 87
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->verification_status(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    move-result-object v0

    .line 88
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->card_issue_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->token(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    move-result-object v0

    .line 89
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->normalized_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    move-result-object v0

    .line 90
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;->corrected_field:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_field(Ljava/util/List;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;

    move-result-object p1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1

    .line 70
    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$verifyShippingAddress$1$1;->invoke(Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object p1

    return-object p1
.end method
