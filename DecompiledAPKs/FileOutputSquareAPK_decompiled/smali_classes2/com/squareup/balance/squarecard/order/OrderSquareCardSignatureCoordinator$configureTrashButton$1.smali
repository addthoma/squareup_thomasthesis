.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lcom/squareup/util/OnMeasuredCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureTrashButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1",
        "Lcom/squareup/util/OnMeasuredCallback;",
        "onMeasured",
        "",
        "view",
        "Landroid/view/View;",
        "width",
        "",
        "height",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 372
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMeasured(Landroid/view/View;II)V
    .locals 1

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashStampButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashButtonRect$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 379
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashStampButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashButtonLocation$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)[I

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->getLocationOnScreen([I)V

    .line 380
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashButtonRect$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Rect;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashButtonLocation$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)[I

    move-result-object p2

    const/4 p3, 0x0

    aget p2, p2, p3

    iget-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashButtonLocation$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)[I

    move-result-object p3

    const/4 v0, 0x1

    aget p3, p3, v0

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Rect;->offset(II)V

    .line 381
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getTrashButtonRect$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setDeleteStampArea(Landroid/graphics/Rect;)V

    return-void
.end method
