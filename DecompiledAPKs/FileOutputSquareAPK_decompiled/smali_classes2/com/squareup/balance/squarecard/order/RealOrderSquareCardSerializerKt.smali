.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;
.super Ljava/lang/Object;
.source "RealOrderSquareCardSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderSquareCardSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderSquareCardSerializer.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt\n+ 2 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n+ 3 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,306:1\n99#2:307\n165#3:308\n158#3,3:309\n161#3:314\n165#3:315\n158#3,3:316\n161#3:321\n1591#4,2:312\n1591#4,2:319\n*E\n*S KotlinDebug\n*F\n+ 1 RealOrderSquareCardSerializer.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt\n*L\n244#1:307\n259#1:308\n270#1,3:309\n270#1:314\n291#1:315\n300#1,3:316\n300#1:321\n270#1,2:312\n300#1,2:319\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0010\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0007H\u0002\u001a\u000c\u0010\u0008\u001a\u00020\t*\u00020\u0007H\u0002\u001a\u000c\u0010\n\u001a\u00020\u000b*\u00020\u0007H\u0002\u001a\u0012\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00020\r*\u00020\u0007H\u0002\u001a\n\u0010\u000e\u001a\u00020\u000f*\u00020\u0010\u001a\u0014\u0010\u0011\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0006H\u0002\u001a\u0014\u0010\u0015\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0016\u001a\u00020\tH\u0002\u001a\u0014\u0010\u0017\u001a\u00020\u0012*\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u000bH\u0002\u001a\u001a\u0010\u0019\u001a\u00020\u0012*\u00020\u00132\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00020\rH\u0002\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u001b"
    }
    d2 = {
        "minFraction",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "getMinFraction",
        "(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)F",
        "readMatrix",
        "Landroid/graphics/Matrix;",
        "Lokio/BufferedSource;",
        "readStamp",
        "Lcom/squareup/cardcustomizations/stampview/Stamp;",
        "readStampsStatus",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "readStampsToRestore",
        "",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "writeMatrix",
        "",
        "Lokio/BufferedSink;",
        "matrix",
        "writeStamp",
        "stamp",
        "writeStampsStatus",
        "status",
        "writeStampsToRestore",
        "stamps",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$readStampsStatus(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->readStampsStatus(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeStampsStatus(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->writeStampsStatus(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V

    return-void
.end method

.method public static final synthetic access$writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V

    return-void
.end method

.method private static final getMinFraction(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)F
    .locals 2

    .line 279
    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getMinHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getRenderedStamp()Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/squareup/cardcustomizations/stampview/Stamp;->bounds(Landroid/graphics/Matrix;)Landroid/graphics/RectF;

    move-result-object p0

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result p0

    div-float/2addr v0, p0

    return v0
.end method

.method private static final readMatrix(Lokio/BufferedSource;)Landroid/graphics/Matrix;
    .locals 4

    .line 315
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 291
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readFloat(Lokio/BufferedSource;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 315
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    check-cast v1, Ljava/util/List;

    .line 292
    new-instance p0, Landroid/graphics/Matrix;

    invoke-direct {p0}, Landroid/graphics/Matrix;-><init>()V

    .line 293
    check-cast v1, Ljava/util/Collection;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toFloatArray(Ljava/util/Collection;)[F

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    return-object p0
.end method

.method private static final readStamp(Lokio/BufferedSource;)Lcom/squareup/cardcustomizations/stampview/Stamp;
    .locals 2

    .line 282
    new-instance v0, Lcom/squareup/cardcustomizations/stampview/Stamp;

    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, v1, p0}, Lcom/squareup/cardcustomizations/stampview/Stamp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final readStampsStatus(Lokio/BufferedSource;)Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;
    .locals 3

    .line 239
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 240
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Class.forName(className)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 244
    const-class v2, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/bizbank/Stamp;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object p0

    .line 244
    new-instance v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    goto :goto_0

    .line 245
    :cond_0
    const-class p0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$FeatureDisabled;

    invoke-static {p0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p0

    invoke-static {v1, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$FeatureDisabled;->INSTANCE:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$FeatureDisabled;

    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    goto :goto_0

    .line 246
    :cond_1
    const-class p0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;

    invoke-static {p0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p0

    invoke-static {v1, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;->INSTANCE:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$ClientError;

    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    :goto_0
    return-object v0

    .line 247
    :cond_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final readStampsToRestore(Lokio/BufferedSource;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokio/BufferedSource;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 308
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 260
    new-instance v3, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 261
    invoke-static {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->readStamp(Lokio/BufferedSource;)Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object v4

    .line 262
    invoke-static {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->readMatrix(Lokio/BufferedSource;)Landroid/graphics/Matrix;

    move-result-object v5

    .line 263
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readFloat(Lokio/BufferedSource;)F

    move-result v6

    .line 264
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v7

    .line 260
    invoke-direct {v3, v4, v5, v6, v7}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;-><init>(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/graphics/Matrix;FI)V

    .line 308
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public static final toSnapshot(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 3

    const-string v0, "$this$toSnapshot"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 305
    sget-object v0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer;->Companion:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;->snapshotState$default(Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method private static final writeMatrix(Lokio/BufferedSink;Landroid/graphics/Matrix;)V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 299
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 300
    invoke-static {v0}, Lkotlin/collections/ArraysKt;->toList([F)Ljava/util/List;

    move-result-object p1

    .line 317
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 318
    check-cast p1, Ljava/lang/Iterable;

    .line 319
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 318
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 301
    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeFloat(Lokio/BufferedSink;F)Lokio/BufferedSink;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static final writeStamp(Lokio/BufferedSink;Lcom/squareup/cardcustomizations/stampview/Stamp;)V
    .locals 1

    .line 286
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 287
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getSvgString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    return-void
.end method

.method private static final writeStampsStatus(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V
    .locals 2

    .line 252
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "status.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 254
    instance-of v0, p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Success;->getStamps()Ljava/util/List;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method

.method private static final writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokio/BufferedSink;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    .line 310
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 311
    check-cast p1, Ljava/lang/Iterable;

    .line 312
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 311
    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 271
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getRenderedStamp()Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->writeStamp(Lokio/BufferedSink;Lcom/squareup/cardcustomizations/stampview/Stamp;)V

    .line 272
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getTransform()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->writeMatrix(Lokio/BufferedSink;Landroid/graphics/Matrix;)V

    .line 273
    invoke-static {v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->getMinFraction(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)F

    move-result v1

    invoke-static {p0, v1}, Lcom/squareup/workflow/SnapshotKt;->writeFloat(Lokio/BufferedSink;F)Lokio/BufferedSink;

    .line 274
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->getMinHeight()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    goto :goto_0

    :cond_0
    return-void
.end method
