.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Factory;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;,
        Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardSignatureCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardSignatureCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,713:1\n1103#2,7:714\n1103#2,7:723\n1103#2,7:730\n1103#2,7:737\n1642#3,2:721\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSquareCardSignatureCoordinator.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator\n*L\n294#1,7:714\n387#1,7:723\n409#1,7:730\n417#1,7:737\n325#1,2:721\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 \u008a\u00012\u00020\u00012\u00020\u0002:\u0008\u008a\u0001\u008b\u0001\u008c\u0001\u008d\u0001BU\u0008\u0002\u0012\u001c\u0010\u0003\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00080\u0004\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\n\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010L\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001cH\u0016J\u0010\u0010N\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001cH\u0002J(\u0010O\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001c2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J\u0008\u0010Q\u001a\u00020.H\u0002J\u0008\u0010R\u001a\u00020.H\u0002J.\u0010S\u001a\u00020.2\u001c\u0010T\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00080\u00042\u0006\u0010M\u001a\u00020\u001cH\u0002J \u0010U\u001a\u00020.2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J\u0008\u0010V\u001a\u00020.H\u0002J\u0008\u0010W\u001a\u00020XH\u0002J\u0008\u0010Y\u001a\u00020ZH\u0002J\u0008\u0010[\u001a\u00020\\H\u0002J\u000e\u0010]\u001a\u0008\u0012\u0004\u0012\u00020_0^H\u0002J\u0010\u0010`\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001cH\u0016J\u0008\u0010a\u001a\u00020.H\u0002J\u0010\u0010b\u001a\u00020.2\u0006\u0010c\u001a\u00020\u0018H\u0002J\u0008\u0010d\u001a\u00020eH\u0002J \u0010f\u001a\u00020.2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J\u0008\u0010g\u001a\u00020.H\u0002J \u0010h\u001a\u00020.2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J\u0010\u0010i\u001a\u00020.2\u0006\u0010j\u001a\u00020ZH\u0002J\u001a\u0010k\u001a\u00020.2\u0010\u0010l\u001a\u000c\u0012\u0004\u0012\u00020.0Jj\u0002`KH\u0002J\u001a\u0010m\u001a\u00020.2\u0010\u0010l\u001a\u000c\u0012\u0004\u0012\u00020.0Jj\u0002`KH\u0002J\u0008\u0010n\u001a\u00020.H\u0002J\u0008\u0010o\u001a\u00020.H\u0016J\u0008\u0010p\u001a\u00020.H\u0016J(\u0010q\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001c2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J>\u0010r\u001a\u00020.2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00082\u0006\u0010s\u001a\u00020\\2\u0006\u0010t\u001a\u00020Z2\u000c\u0010u\u001a\u0008\u0012\u0004\u0012\u00020_0^H\u0002J\u0008\u0010v\u001a\u00020.H\u0016J\u0008\u0010w\u001a\u00020.H\u0016J\u0016\u0010x\u001a\u00020.2\u000c\u0010u\u001a\u0008\u0012\u0004\u0012\u00020_0^H\u0002J\u001c\u0010y\u001a\u0002012\u0008\u0008\u0001\u0010z\u001a\u00020{2\u0008\u0008\u0001\u0010|\u001a\u00020{H\u0002J\u0008\u0010}\u001a\u000201H\u0002J\u0008\u0010~\u001a\u000201H\u0002J \u0010\u007f\u001a\u00020.2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J\u0012\u0010\u0080\u0001\u001a\u00020.2\u0007\u0010\u0081\u0001\u001a\u00020XH\u0002J+\u0010\u0082\u0001\u001a\u00020.2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00082\u0008\u0010\u0083\u0001\u001a\u00030\u0084\u0001H\u0002J\u0008\u00108\u001a\u000209H\u0002J\t\u0010\u0085\u0001\u001a\u00020.H\u0002J\t\u0010\u0086\u0001\u001a\u00020.H\u0002J)\u0010\u0087\u0001\u001a\u00020.2\u0006\u0010M\u001a\u00020\u001c2\u0016\u0010P\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u0008H\u0002J\r\u0010\u0088\u0001\u001a\u00020.*\u00020\u001cH\u0002J\r\u0010\u0089\u0001\u001a\u00020.*\u00020\u001cH\u0002R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\u00020\u00188BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008$\u0010%R\u000e\u0010&\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010,\u001a\u0008\u0012\u0004\u0012\u00020.0-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u00100\u001a\u0004\u0018\u000101X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\u0003\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005j\u0002`\u00080\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00102\u001a\u00020\u00188BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00083\u0010%R\u000e\u00104\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00105\u001a\u000206X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u00108\u001a\u0004\u0018\u000109X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020;X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020>X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020@X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020>X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020CX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010H\u001a\u0012\u0012\u000e\u0012\u000c\u0012\u0004\u0012\u00020.0Jj\u0002`K0IX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u008e\u0001"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;",
        "Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureScreen;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "mainThreadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "renderSignatureSchedulerProvider",
        "Lcom/squareup/balance/core/RenderSignatureScheduler;",
        "stampsScheduler",
        "gson",
        "Lcom/google/gson/Gson;",
        "businessNameFormatter",
        "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
        "(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "allowStampCustomizationMode",
        "",
        "cardPreview",
        "Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;",
        "clearButton",
        "Landroid/view/View;",
        "drawingModeButton",
        "Landroid/widget/ImageButton;",
        "findFreePositionLocation",
        "",
        "findFreePositionRect",
        "Landroid/graphics/Rect;",
        "hasSigned",
        "getHasSigned",
        "()Z",
        "indicator",
        "Landroid/widget/ImageView;",
        "isFadingControlsOut",
        "Ljava/util/concurrent/atomic/AtomicBoolean;",
        "mode",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;",
        "onNextStepRequest",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "rootView",
        "scaledPreviewSignature",
        "Lcom/squareup/cardcustomizations/signature/Signature;",
        "showingCardPreview",
        "getShowingCardPreview",
        "signatureHint",
        "signatureOutline",
        "Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;",
        "signaturePad",
        "signatureTransformation",
        "Lcom/squareup/cardcustomizations/signature/SignatureTransformation;",
        "signatureView",
        "Lcom/squareup/cardcustomizations/signature/SignatureView;",
        "stampModeButton",
        "stampSize",
        "",
        "stampView",
        "Lcom/squareup/cardcustomizations/stampview/StampView;",
        "strokeWidth",
        "subscriptions",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "trashButtonLocation",
        "trashButtonRect",
        "trashStampButton",
        "undoButton",
        "undoStack",
        "Ljava/util/ArrayDeque;",
        "Lkotlin/Function0;",
        "Lcom/squareup/balance/squarecard/order/Action;",
        "attach",
        "view",
        "bindViews",
        "configureActionBar",
        "screen",
        "configureAllControlsButStamps",
        "configureCardPreview",
        "configureSignatureView",
        "sharedScreens",
        "configureStampButton",
        "configureTrashButton",
        "createCardSignaturePreview",
        "Landroid/graphics/Bitmap;",
        "createSignatureJson",
        "",
        "createSignatureUploadPayload",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
        "createStampsToRestore",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "detach",
        "disableStampsAndSignatureRestoration",
        "enableControls",
        "enable",
        "findFreePositionForStamp",
        "Landroid/graphics/Point;",
        "handleBack",
        "hidePreview",
        "maybeAddStamp",
        "maybeRestoreSignature",
        "signatureJson",
        "modifySignature",
        "action",
        "modifyStamps",
        "notifyState",
        "onClearedSignature",
        "onGlyphAdded",
        "onScreenData",
        "onSignatureEncoded",
        "signatureUploadPayload",
        "signatureAsJson",
        "stampsToRestore",
        "onSigned",
        "onStartedSigning",
        "restoreStamps",
        "scaledSignature",
        "signatureColor",
        "",
        "bgColor",
        "scaledSignatureForPreview",
        "scaledSignatureForUpload",
        "setNameOnCard",
        "showPreview",
        "signatureBitmap",
        "showPreviewOrEmitError",
        "previewData",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;",
        "updateSelectedIndicator",
        "updateStampButton",
        "updateSubscriptions",
        "fadeIn",
        "fadeOut",
        "Companion",
        "Factory",
        "Mode",
        "PreviewData",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Companion;

.field public static final SIGNATURE_VIEW_READY_FOR_UI_TESTING_TAG:Ljava/lang/String; = "SignatureViewReadyForUiTestingTag"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private allowStampCustomizationMode:Z

.field private final businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

.field private cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

.field private clearButton:Landroid/view/View;

.field private drawingModeButton:Landroid/widget/ImageButton;

.field private final findFreePositionLocation:[I

.field private final findFreePositionRect:Landroid/graphics/Rect;

.field private final gson:Lcom/google/gson/Gson;

.field private indicator:Landroid/widget/ImageView;

.field private isFadingControlsOut:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

.field private final onNextStepRequest:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final renderSignatureSchedulerProvider:Lcom/squareup/balance/core/RenderSignatureScheduler;

.field private rootView:Landroid/view/View;

.field private scaledPreviewSignature:Lcom/squareup/cardcustomizations/signature/Signature;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private signatureHint:Landroid/view/View;

.field private signatureOutline:Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

.field private signaturePad:Landroid/view/View;

.field private signatureTransformation:Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

.field private signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

.field private stampModeButton:Landroid/widget/ImageButton;

.field private stampSize:F

.field private stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

.field private final stampsScheduler:Lio/reactivex/Scheduler;

.field private strokeWidth:F

.field private final subscriptions:Lio/reactivex/disposables/CompositeDisposable;

.field private final trashButtonLocation:[I

.field private final trashButtonRect:Landroid/graphics/Rect;

.field private trashStampButton:Landroid/widget/ImageButton;

.field private undoButton:Landroid/view/View;

.field private final undoStack:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque<",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->Companion:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Companion;

    return-void
.end method

.method private constructor <init>(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;>;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            "Lcom/squareup/balance/core/RenderSignatureScheduler;",
            "Lio/reactivex/Scheduler;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;",
            ")V"
        }
    .end annotation

    .line 89
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->renderSignatureSchedulerProvider:Lcom/squareup/balance/core/RenderSignatureScheduler;

    iput-object p5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampsScheduler:Lio/reactivex/Scheduler;

    iput-object p6, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->gson:Lcom/google/gson/Gson;

    iput-object p7, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    .line 95
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->onNextStepRequest:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 96
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    .line 97
    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoStack:Ljava/util/ArrayDeque;

    .line 99
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashButtonRect:Landroid/graphics/Rect;

    const/4 p1, 0x2

    new-array p2, p1, [I

    .line 100
    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashButtonLocation:[I

    .line 102
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->findFreePositionRect:Landroid/graphics/Rect;

    new-array p1, p1, [I

    .line 103
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->findFreePositionLocation:[I

    .line 124
    new-instance p1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->isFadingControlsOut:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 125
    sget-object p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->DRAW:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 80
    invoke-direct/range {p0 .. p7}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;-><init>(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/balance/core/RenderSignatureScheduler;Lio/reactivex/Scheduler;Lcom/google/gson/Gson;Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;)V

    return-void
.end method

.method public static final synthetic access$createCardSignaturePreview(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Bitmap;
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->createCardSignaturePreview()Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createSignatureJson(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/lang/String;
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->createSignatureJson()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createSignatureUploadPayload(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->createSignatureUploadPayload()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createStampsToRestore(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/List;
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->createStampsToRestore()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$enableControls(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Z)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->enableControls(Z)V

    return-void
.end method

.method public static final synthetic access$fadeIn(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->fadeIn(Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$fadeOut(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->fadeOut(Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$findFreePositionForStamp(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Point;
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->findFreePositionForStamp()Landroid/graphics/Point;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getClearButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/view/View;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "clearButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getDrawingModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez p0, :cond_0

    const-string v0, "drawingModeButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getHasSigned$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Z
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getHasSigned()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getIndicator$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageView;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    if-nez p0, :cond_0

    const-string v0, "indicator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMode$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    return-object p0
.end method

.method public static final synthetic access$getOnNextStepRequest$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->onNextStepRequest:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getShowingCardPreview$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Z
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getShowingCardPreview()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/signature/SignatureView;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez p0, :cond_0

    const-string v0, "signatureView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getStampModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez p0, :cond_0

    const-string v0, "stampModeButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getStampSize$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)F
    .locals 0

    .line 80
    iget p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampSize:F

    return p0
.end method

.method public static final synthetic access$getStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Lcom/squareup/cardcustomizations/stampview/StampView;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez p0, :cond_0

    const-string v0, "stampView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getStrokeWidth$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)F
    .locals 0

    .line 80
    iget p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->strokeWidth:F

    return p0
.end method

.method public static final synthetic access$getTrashButtonLocation$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)[I
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashButtonLocation:[I

    return-object p0
.end method

.method public static final synthetic access$getTrashButtonRect$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/graphics/Rect;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashButtonRect:Landroid/graphics/Rect;

    return-object p0
.end method

.method public static final synthetic access$getTrashStampButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/widget/ImageButton;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashStampButton:Landroid/widget/ImageButton;

    if-nez p0, :cond_0

    const-string v0, "trashStampButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getUndoButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Landroid/view/View;
    .locals 1

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "undoButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getUndoStack$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/ArrayDeque;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoStack:Ljava/util/ArrayDeque;

    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->handleBack(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$isFadingControlsOut$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->isFadingControlsOut:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object p0
.end method

.method public static final synthetic access$maybeRestoreSignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Ljava/lang/String;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->maybeRestoreSignature(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$modifySignature(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->modifySignature(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$modifyStamps(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->modifyStamps(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$notifyState(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->notifyState()V

    return-void
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->onScreenData(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$onSignatureEncoded(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->onSignatureEncoded(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$restoreStamps(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Ljava/util/List;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->restoreStamps(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$setClearButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$setDrawingModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/widget/ImageButton;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    return-void
.end method

.method public static final synthetic access$setFadingControlsOut$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->isFadingControlsOut:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static final synthetic access$setIndicator$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/widget/ImageView;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$setMode$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    return-void
.end method

.method public static final synthetic access$setSignatureView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/cardcustomizations/signature/SignatureView;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    return-void
.end method

.method public static final synthetic access$setStampModeButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/widget/ImageButton;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    return-void
.end method

.method public static final synthetic access$setStampSize$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;F)V
    .locals 0

    .line 80
    iput p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampSize:F

    return-void
.end method

.method public static final synthetic access$setStampView$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/cardcustomizations/stampview/StampView;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    return-void
.end method

.method public static final synthetic access$setStrokeWidth$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;F)V
    .locals 0

    .line 80
    iput p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->strokeWidth:F

    return-void
.end method

.method public static final synthetic access$setTrashStampButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/widget/ImageButton;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashStampButton:Landroid/widget/ImageButton;

    return-void
.end method

.method public static final synthetic access$setUndoButton$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    return-void
.end method

.method public static final synthetic access$showPreviewOrEmitError(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->showPreviewOrEmitError(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 672
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 673
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->root_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->rootView:Landroid/view/View;

    .line 674
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->signature_outline:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureOutline:Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

    .line 675
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->signature_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/signature/SignatureView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    .line 676
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->drawing_mode:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.drawing_mode)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    .line 677
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stamps_mode:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    .line 678
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->trash_stamp:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashStampButton:Landroid/widget/ImageButton;

    .line 679
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stamp_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    .line 680
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stamps_selected_indicator:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    .line 681
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->signature_pad:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signaturePad:Landroid/view/View;

    .line 682
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->clear:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    .line 683
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->undo:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    .line 684
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->card_preview:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    .line 685
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->signature_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureHint:Landroid/view/View;

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 214
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 216
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->signature_personalize_card_action_bar:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 215
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/common/strings/R$string;->next:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 219
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getHasSigned()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 220
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureActionBar$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureActionBar$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 221
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureActionBar$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureActionBar$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 222
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final configureAllControlsButStamps()V
    .locals 2

    .line 387
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "clearButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 723
    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 409
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v1, "undoButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 730
    :cond_1
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 417
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_2

    const-string v1, "drawingModeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    .line 737
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$3;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureAllControlsButStamps$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureCardPreview()V
    .locals 0

    .line 539
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->hidePreview()V

    return-void
.end method

.method private final configureSignatureView(Lio/reactivex/Observable;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;>;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 546
    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$bitmapProvider$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$bitmapProvider$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 549
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->notifyState()V

    .line 550
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    const-string v2, "signatureView"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz v0, :cond_1

    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt$sam$com_squareup_cardcustomizations_signature_Signature_BitmapProvider$0;

    invoke-direct {v3, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt$sam$com_squareup_cardcustomizations_signature_Signature_BitmapProvider$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    goto :goto_0

    :cond_1
    move-object v3, v0

    :goto_0
    check-cast v3, Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;

    invoke-virtual {v1, v3}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setBitmapProvider(Lcom/squareup/cardcustomizations/signature/Signature$BitmapProvider;)V

    .line 552
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    const-string v3, "stampView"

    if-nez v1, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->setBitmapProvider(Lkotlin/jvm/functions/Function2;)V

    .line 554
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    if-eqz v1, :cond_4

    new-instance v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt$sam$com_squareup_cardcustomizations_signature_Signature_PainterProvider$0;

    invoke-direct {v4, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt$sam$com_squareup_cardcustomizations_signature_Signature_PainterProvider$0;-><init>(Lkotlin/jvm/functions/Function2;)V

    move-object v1, v4

    :cond_4
    check-cast v1, Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setPainterProvider(Lcom/squareup/cardcustomizations/signature/Signature$PainterProvider;)V

    .line 555
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureOutline:Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

    if-nez v0, :cond_5

    const-string v1, "signatureOutline"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;->layoutChange()Lio/reactivex/Observable;

    move-result-object v0

    .line 556
    check-cast p1, Lio/reactivex/ObservableSource;

    invoke-static {v0, p1}, Lcom/squareup/util/rx2/RxKotlinKt;->zipWith(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 557
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    .line 558
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "signatureOutline.layoutC\u2026_UI_TESTING_TAG\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 577
    invoke-static {p1, p2}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 578
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez p1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    move-object p2, p0

    check-cast p2, Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;

    invoke-virtual {p1, p2}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setListener(Lcom/squareup/cardcustomizations/signature/SignatureView$SignatureStateListener;)V

    .line 580
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez p1, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance p2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$3;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$3;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setStampMovingListener(Lkotlin/jvm/functions/Function1;)V

    .line 592
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez p1, :cond_8

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    new-instance p2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$4;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setStampMovedListener(Lkotlin/jvm/functions/Function0;)V

    .line 607
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez p1, :cond_9

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    new-instance p2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;

    invoke-direct {p2, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureSignatureView$5;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, p2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setStampAddedOrRemovedListener(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final configureStampButton(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 287
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getAllowStampsCustomization()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->allowStampCustomizationMode:Z

    .line 290
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    const-string v1, "stampModeButton"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->allowStampCustomizationMode:Z

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 291
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    const-string v2, "drawingModeButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->allowStampCustomizationMode:Z

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 292
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v2, "indicator"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    iget-boolean v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->allowStampCustomizationMode:Z

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 294
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    .line 714
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureStampButton$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final configureTrashButton()V
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->trashStampButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    const-string v1, "trashStampButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$configureTrashButton$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Lcom/squareup/util/OnMeasuredCallback;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method private final createCardSignaturePreview()Landroid/graphics/Bitmap;
    .locals 2

    .line 649
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledSignatureForPreview()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v1, "scaledSignatureForPreview().bitmap"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final createSignatureJson()Ljava/lang/String;
    .locals 2

    .line 660
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_0

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/signature/Signature;->encode(Lcom/google/gson/Gson;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "signatureView.signature.encode(gson)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final createSignatureUploadPayload()Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;
    .locals 7

    .line 652
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledSignatureForUpload()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Signature;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 653
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const-string v2, "bitmap"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    check-cast v1, Ljava/io/Closeable;

    const/4 v2, 0x0

    check-cast v2, Ljava/lang/Throwable;

    :try_start_0
    move-object v3, v1

    check-cast v3, Ljava/io/ByteArrayOutputStream;

    .line 654
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    move-object v6, v3

    check-cast v6, Ljava/io/OutputStream;

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 655
    sget-object v0, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const-string v4, "it.toByteArray()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v4, v3

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 653
    invoke-static {v1, v2}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 657
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;

    const-string v2, "image/png"

    invoke-direct {v1, v0, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;-><init>(Lokio/ByteString;Ljava/lang/String;)V

    return-object v1

    :catchall_0
    move-exception v0

    .line 653
    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v2

    invoke-static {v1, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private final createStampsToRestore()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 661
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_0

    const-string v1, "stampView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final disableStampsAndSignatureRestoration()V
    .locals 3

    .line 694
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    const-string v1, "stampView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setSaveFromParentEnabled(Z)V

    .line 695
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v2}, Lcom/squareup/cardcustomizations/stampview/StampView;->setSaveEnabled(Z)V

    .line 696
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_2

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v2}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setSaveEnabled(Z)V

    return-void
.end method

.method private final enableControls(Z)V
    .locals 5

    .line 486
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_0

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->DRAW:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0, v3}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setEnabled(Z)V

    .line 487
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_2

    const-string v3, "stampView"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v4, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->STAMP:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    if-ne v3, v4, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView;->setEnabled(Z)V

    .line 488
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    if-nez v0, :cond_4

    const-string v1, "undoButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 489
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    if-nez v0, :cond_5

    const-string v1, "clearButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 490
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_6

    const-string v1, "drawingModeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 491
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_7

    const-string v1, "stampModeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    return-void
.end method

.method private final fadeIn(Landroid/view/View;)V
    .locals 2

    .line 664
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    return-void
.end method

.method private final fadeOut(Landroid/view/View;)V
    .locals 2

    .line 668
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToInvisible(Landroid/view/View;I)V

    return-void
.end method

.method private final findFreePositionForStamp()Landroid/graphics/Point;
    .locals 8

    .line 424
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Do not try to find a free position for stamps on the main thread."

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 426
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_0

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v2

    const-string v0, "signatureView.signature"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 427
    new-instance v3, Landroid/graphics/RectF;

    iget v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampSize:F

    const/4 v1, 0x0

    invoke-direct {v3, v1, v1, v0, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 428
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureOutline:Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

    if-nez v0, :cond_1

    const-string v1, "signatureOutline"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->findFreePositionLocation:[I

    iget-object v4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->findFreePositionRect:Landroid/graphics/Rect;

    invoke-static {v0, v1, v4}, Lcom/squareup/util/Views;->locationOfViewInWindow(Landroid/view/View;[ILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    .line 429
    iget-object v5, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v5, :cond_2

    const-string v0, "stampView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureTransformation()Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-result-object v6

    iget v7, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->strokeWidth:F

    .line 426
    invoke-static/range {v2 .. v7}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->findFreeRegion(Lcom/squareup/cardcustomizations/signature/Signature;Landroid/graphics/RectF;Landroid/graphics/Rect;Lcom/squareup/cardcustomizations/stampview/StampView;Lcom/squareup/cardcustomizations/signature/SignatureTransformation;F)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method private final getHasSigned()Z
    .locals 3

    .line 195
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_0

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->canBeCleared()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_1

    const-string v2, "stampView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->stamps()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :cond_3
    :goto_0
    return v1
.end method

.method private final getShowingCardPreview()Z
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_0

    const-string v1, "cardPreview"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 478
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getShowingCardPreview()Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$GoBack;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 481
    :cond_0
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->hidePreview()V

    :goto_0
    return-void
.end method

.method private final hidePreview()V
    .locals 4

    .line 456
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->rootView:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "rootView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/noho/R$color;->noho_background:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 457
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    const-string v1, "cardPreview"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 458
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signaturePad:Landroid/view/View;

    if-nez v0, :cond_2

    const-string v2, "signaturePad"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 459
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_3

    const-string v2, "signatureView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 460
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureOutline:Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

    if-nez v0, :cond_4

    const-string v2, "signatureOutline"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 461
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_5

    const-string v2, "stampView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 463
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->allowStampCustomizationMode:Z

    if-eqz v0, :cond_9

    .line 464
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_6

    const-string v2, "stampModeButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 465
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_7

    const-string v2, "drawingModeButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 466
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    if-nez v0, :cond_8

    const-string v2, "indicator"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 468
    :cond_9
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    if-nez v0, :cond_a

    const-string v2, "undoButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 469
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    if-nez v0, :cond_b

    const-string v2, "clearButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 474
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v2, "actionBar"

    if-nez v0, :cond_c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 470
    :cond_c
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v3, :cond_d

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 472
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v3, :cond_e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v3}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->signature_personalize_card_action_bar:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 471
    invoke-virtual {v2, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 474
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final maybeAddStamp(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 329
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$1$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$1$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt$sam$i$java_util_concurrent_Callable$0;

    invoke-direct {v3, v2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinatorKt$sam$i$java_util_concurrent_Callable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Ljava/util/concurrent/Callable;

    invoke-static {v3}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v2

    .line 331
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampsScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    .line 332
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    .line 333
    new-instance v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;

    invoke-direct {v3, v0, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$maybeAddStamp$$inlined$let$lambda$1;-><init>(Lcom/squareup/protos/client/bizbank/Stamp;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lio/reactivex/functions/Consumer;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 330
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    :cond_0
    return-void
.end method

.method private final maybeRestoreSignature(Ljava/lang/String;)V
    .locals 2

    .line 232
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_2

    .line 233
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_1

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->restore(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private final modifySignature(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 360
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    const-string v1, "signatureView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 361
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 362
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/SignatureView;->invalidate()V

    :cond_2
    return-void
.end method

.method private final modifyStamps(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 367
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 368
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez p1, :cond_0

    const-string v0, "stampView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/StampView;->invalidate()V

    return-void
.end method

.method private final notifyState()V
    .locals 7

    .line 495
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    iget-boolean v2, v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonEnabled:Z

    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getHasSigned()Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    move-object v0, v3

    :goto_1
    if-eqz v0, :cond_5

    .line 499
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 497
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 498
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getHasSigned()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 499
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 500
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureHint:Landroid/view/View;

    if-nez v0, :cond_4

    const-string v1, "signatureHint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->getHasSigned()Z

    move-result v1

    xor-int/2addr v1, v5

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 502
    :cond_5
    move-object v0, v3

    check-cast v0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureTransformation:Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    .line 503
    check-cast v3, Lcom/squareup/cardcustomizations/signature/Signature;

    iput-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledPreviewSignature:Lcom/squareup/cardcustomizations/signature/Signature;

    .line 505
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    const-string v1, "signatureView"

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->DRAW:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    if-ne v2, v3, :cond_7

    const/4 v2, 0x1

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v0, v2}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setEnabled(Z)V

    .line 506
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    const-string v2, "stampView"

    if-nez v0, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    iget-object v3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v6, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->STAMP:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    if-ne v3, v6, :cond_9

    const/4 v4, 0x1

    :cond_9
    invoke-virtual {v0, v4}, Lcom/squareup/cardcustomizations/stampview/StampView;->setEnabled(Z)V

    .line 507
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->ordinal()I

    move-result v0

    aget v0, v3, v0

    if-eq v0, v5, :cond_c

    const/4 v1, 0x2

    if-eq v0, v1, :cond_a

    goto :goto_3

    .line 509
    :cond_a
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/stampview/StampView;->invalidate()V

    goto :goto_3

    .line 508
    :cond_c
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->invalidate()V

    .line 512
    :goto_3
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateStampButton()V

    .line 513
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateSelectedIndicator()V

    return-void
.end method

.method private final onScreenData(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 201
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureActionBar(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    .line 202
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureStampButton(Lcom/squareup/workflow/legacy/Screen;)V

    .line 204
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->setNameOnCard(Lcom/squareup/workflow/legacy/Screen;)V

    .line 205
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateSubscriptions(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    .line 206
    invoke-direct {p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->maybeAddStamp(Lcom/squareup/workflow/legacy/Screen;)V

    .line 207
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$onScreenData$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$onScreenData$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final onSignatureEncoded(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    .line 281
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 282
    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;

    invoke-direct {v0, p2, p3, p4}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$SignatureComplete;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$SignatureUploadPayload;Ljava/lang/String;Ljava/util/List;)V

    .line 281
    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final restoreStamps(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    .line 325
    check-cast p1, Ljava/lang/Iterable;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v0, :cond_0

    const-string v1, "stampView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 721
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 325
    invoke-virtual {v0, v1}, Lcom/squareup/cardcustomizations/stampview/StampView;->addStamp(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private final scaledSignature(II)Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 9

    .line 635
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v0, :cond_0

    const-string v1, "signatureView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getSignature()Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v2

    const-string v0, "signatureView.signature"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 636
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureTransformation()Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-result-object v3

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move v4, p1

    move v5, p2

    .line 635
    invoke-static/range {v2 .. v8}, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->createScaledSignature$default(Lcom/squareup/cardcustomizations/signature/Signature;Lcom/squareup/cardcustomizations/signature/SignatureTransformation;IILcom/squareup/cardcustomizations/signature/Signature$PainterProvider;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object p1

    return-object p1
.end method

.method private final scaledSignatureForPreview()Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 3

    .line 616
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->renderSignatureSchedulerProvider:Lcom/squareup/balance/core/RenderSignatureScheduler;

    invoke-virtual {v0}, Lcom/squareup/balance/core/RenderSignatureScheduler;->getAllowWorkOnMainThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const-string v1, "Do not create a scaled signature from the main thread"

    invoke-interface {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid(Ljava/lang/String;)V

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledPreviewSignature:Lcom/squareup/cardcustomizations/signature/Signature;

    if-eqz v0, :cond_1

    return-object v0

    .line 621
    :cond_1
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    const-string v1, "signatureView"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 622
    sget v2, Lcom/squareup/balance/core/R$color;->square_card_signature_preview_signature:I

    .line 620
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 625
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2}, Lcom/squareup/cardcustomizations/signature/SignatureView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/balance/squarecard/impl/R$color;->square_card_signature_preview_signature_background:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 626
    invoke-direct {p0, v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledSignature(II)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    .line 627
    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledPreviewSignature:Lcom/squareup/cardcustomizations/signature/Signature;

    return-object v0
.end method

.method private final scaledSignatureForUpload()Lcom/squareup/cardcustomizations/signature/Signature;
    .locals 2

    const/high16 v0, -0x1000000

    const/4 v1, -0x1

    .line 632
    invoke-direct {p0, v0, v1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->scaledSignature(II)Lcom/squareup/cardcustomizations/signature/Signature;

    move-result-object v0

    return-object v0
.end method

.method private final setNameOnCard(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 226
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v0, :cond_0

    const-string v1, "cardPreview"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 227
    :cond_0
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->businessNameFormatter:Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;->getBusinessName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter;->formatBusinessName(Ljava/lang/String;)Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/BusinessNameFormatter$FormatResult;->getText()Ljava/lang/String;

    move-result-object p1

    .line 226
    invoke-virtual {v0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setName(Ljava/lang/String;)V

    return-void
.end method

.method private final showPreview(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 434
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    const-string v1, "cardPreview"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->setSignature(Landroid/graphics/Bitmap;)V

    .line 435
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->rootView:Landroid/view/View;

    if-nez p1, :cond_1

    const-string v0, "rootView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v0, Lcom/squareup/balance/squarecard/impl/R$color;->square_card_signature_preview_background:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 436
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 437
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signaturePad:Landroid/view/View;

    if-nez p1, :cond_3

    const-string v0, "signaturePad"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 438
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez p1, :cond_4

    const-string v0, "signatureView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 439
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez p1, :cond_5

    const-string v0, "stampView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 440
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureOutline:Lcom/squareup/balance/squarecard/ui/SignatureOutlineView;

    if-nez p1, :cond_6

    const-string v0, "signatureOutline"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 441
    iget-boolean p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->allowStampCustomizationMode:Z

    if-eqz p1, :cond_a

    .line 442
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez p1, :cond_7

    const-string v0, "stampModeButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 443
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez p1, :cond_8

    const-string v0, "drawingModeButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 444
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    if-nez p1, :cond_9

    const-string v0, "indicator"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 446
    :cond_a
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoButton:Landroid/view/View;

    if-nez p1, :cond_b

    const-string v0, "undoButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 447
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->clearButton:Landroid/view/View;

    if-nez p1, :cond_c

    const-string v0, "clearButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-static {p1}, Lcom/squareup/util/Views;->setInvisible(Landroid/view/View;)V

    .line 452
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v0, "actionBar"

    if-nez p1, :cond_d

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 448
    :cond_d
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v2, :cond_e

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 450
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->cardPreview:Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;

    if-nez v2, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {v2}, Lcom/squareup/balance/core/views/cardpreview/SquareCardPreview;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->signature_confirm_design_action_bar:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    .line 449
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 452
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final showPreviewOrEmitError(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;",
            ")V"
        }
    .end annotation

    .line 311
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;->getInkLevel()Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v0

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/InkLevel;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 p1, 0x3

    if-ne v0, p1, :cond_0

    .line 314
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;->getSignatureAsBitmap()Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->showPreview(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 313
    :cond_1
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$InkLevelError;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;->getInkLevel()Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$PreviewData;->getSignatureAsJson()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, v1, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event$InkLevelError;-><init>(Lcom/squareup/cardcustomizations/signature/InkLevel;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final signatureTransformation()Lcom/squareup/cardcustomizations/signature/SignatureTransformation;
    .locals 4

    .line 643
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureTransformation:Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    if-eqz v0, :cond_0

    return-object v0

    .line 644
    :cond_0
    sget-object v0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;->Companion:Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez v1, :cond_1

    const-string v2, "signatureView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampView:Lcom/squareup/cardcustomizations/stampview/StampView;

    if-nez v2, :cond_2

    const-string v3, "stampView"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardcustomizations/signature/SignatureTransformation$Companion;->invoke(Lcom/squareup/cardcustomizations/signature/SignatureView;Lcom/squareup/cardcustomizations/stampview/StampView;)Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    move-result-object v0

    .line 645
    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureTransformation:Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    return-object v0
.end method

.method private final updateSelectedIndicator()V
    .locals 3

    .line 527
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 529
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    const-string v1, "stampModeButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ImageButton;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->drawingModeButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_1

    const-string v2, "drawingModeButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Landroid/widget/ImageButton;->getRight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_3
    const/4 v0, 0x0

    .line 532
    :goto_0
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->indicator:Landroid/widget/ImageView;

    if-nez v1, :cond_4

    const-string v2, "indicator"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v1}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-float v0, v0

    .line 533
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 534
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    check-cast v1, Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 535
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method private final updateStampButton()V
    .locals 3

    .line 517
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mode:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;->DRAW:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$Mode;

    if-ne v0, v1, :cond_0

    .line 518
    sget v0, Lcom/squareup/balance/squarecard/impl/R$drawable;->square_card_stamps_icon:I

    goto :goto_0

    .line 520
    :cond_0
    sget v0, Lcom/squareup/vectoricons/R$drawable;->ui_plus_24:I

    .line 523
    :goto_0
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->stampModeButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_1

    const-string v2, "stampModeButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    return-void
.end method

.method private final updateSubscriptions(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$ScreenData;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardSignature$Event;",
            ">;)V"
        }
    .end annotation

    .line 240
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 241
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->onNextStepRequest:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 242
    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$1;

    invoke-direct {v2, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v2, Lio/reactivex/functions/Predicate;

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    .line 243
    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$2;

    invoke-direct {v2, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$2;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    .line 244
    iget-object v2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->renderSignatureSchedulerProvider:Lcom/squareup/balance/core/RenderSignatureScheduler;

    invoke-virtual {v2}, Lcom/squareup/balance/core/RenderSignatureScheduler;->getScheduler()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 245
    new-instance v2, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$3;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 254
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 255
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$4;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "onNextStepRequest\n      \u2026n, previewData)\n        }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    .line 260
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->onNextStepRequest:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 261
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$5;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$5;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 262
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->renderSignatureSchedulerProvider:Lcom/squareup/balance/core/RenderSignatureScheduler;

    invoke-virtual {v1}, Lcom/squareup/balance/core/RenderSignatureScheduler;->getScheduler()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 263
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$6;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 269
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 270
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$7;

    invoke-direct {v1, p0, p2}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$7;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    const-string v0, "onNextStepRequest\n      \u2026tampsToRestore)\n        }"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-static {p1, p2}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->screens:Lio/reactivex/Observable;

    invoke-virtual {v0}, Lio/reactivex/Observable;->publish()Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    .line 149
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->bindViews(Landroid/view/View;)V

    .line 150
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->disableStampsAndSignatureRestoration()V

    .line 151
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureCardPreview()V

    const-string v1, "sharedScreens"

    .line 152
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Lio/reactivex/Observable;

    invoke-direct {p0, v1, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureSignatureView(Lio/reactivex/Observable;Landroid/view/View;)V

    .line 153
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureAllControlsButStamps()V

    .line 154
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->configureTrashButton()V

    .line 155
    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "sharedScreens.subscribe \u2026creenData(view, screen) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-static {v1, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 157
    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "sharedScreens.connect()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {p1}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 165
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureView:Lcom/squareup/cardcustomizations/signature/SignatureView;

    if-nez p1, :cond_0

    const-string v0, "signatureView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/cardcustomizations/signature/SignatureView;->setTag(Ljava/lang/Object;)V

    .line 167
    check-cast v0, Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->signatureTransformation:Lcom/squareup/cardcustomizations/signature/SignatureTransformation;

    return-void
.end method

.method public onClearedSignature()V
    .locals 0

    .line 171
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->notifyState()V

    return-void
.end method

.method public onGlyphAdded()V
    .locals 2

    .line 183
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->notifyState()V

    .line 184
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->undoStack:Ljava/util/ArrayDeque;

    new-instance v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$onGlyphAdded$1;

    invoke-direct {v1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$onGlyphAdded$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    return-void
.end method

.method public onSigned()V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->notifyState()V

    return-void
.end method

.method public onStartedSigning()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->notifyState()V

    return-void
.end method
