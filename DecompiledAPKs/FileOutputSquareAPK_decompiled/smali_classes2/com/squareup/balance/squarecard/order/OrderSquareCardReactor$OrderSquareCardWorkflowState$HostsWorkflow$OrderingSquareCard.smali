.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;
.super Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;
.source "OrderSquareCardReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OrderingSquareCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow<",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\r\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001Bg\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012:\u0010\u000c\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0010\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0011j\u0002`\u00120\u000f0\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00020\rj\u0002`\u0014\u00a2\u0006\u0002\u0010\u0015R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dRH\u0010\u001e\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0010\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0011j\u0002`\u00120\u000f0\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00020\rj\u0002`\u0014X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 \u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;",
        "Lcom/squareup/mailorder/OrderWorkflowResult;",
        "skippedInitialScreens",
        "",
        "squareCardCustomizationSettings",
        "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "signatureAsJson",
        "",
        "stampsToRestore",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "workflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/mailorder/OrderScreenWorkflow;",
        "(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/util/List;Lcom/squareup/workflow/rx1/Workflow;)V",
        "getSignatureAsJson",
        "()Ljava/lang/String;",
        "getSkippedInitialScreens",
        "()Z",
        "getSquareCardCustomizationSettings",
        "()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
        "getStampsToRestore",
        "()Ljava/util/List;",
        "subWorkflow",
        "getSubWorkflow",
        "()Lcom/squareup/workflow/rx1/Workflow;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final signatureAsJson:Ljava/lang/String;

.field private final skippedInitialScreens:Z

.field private final squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

.field private final stampsToRestore:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation
.end field

.field private final subWorkflow:Lcom/squareup/workflow/rx1/Workflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;Ljava/lang/String;Ljava/util/List;Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*+",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "squareCardCustomizationSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "signatureAsJson"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampsToRestore"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 219
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->skippedInitialScreens:Z

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->signatureAsJson:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->stampsToRestore:Ljava/util/List;

    .line 221
    new-instance p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard$subWorkflow$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard$subWorkflow$1;-><init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p5, p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->mapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->subWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    return-void
.end method


# virtual methods
.method public final getSignatureAsJson()Ljava/lang/String;
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->signatureAsJson:Ljava/lang/String;

    return-object v0
.end method

.method public final getSkippedInitialScreens()Z
    .locals 1

    .line 214
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->skippedInitialScreens:Z

    return v0
.end method

.method public final getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->squareCardCustomizationSettings:Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    return-object v0
.end method

.method public final getStampsToRestore()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->stampsToRestore:Ljava/util/List;

    return-object v0
.end method

.method public getSubWorkflow()Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/mailorder/OrderWorkflowResult;",
            ">;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->subWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    return-object v0
.end method
