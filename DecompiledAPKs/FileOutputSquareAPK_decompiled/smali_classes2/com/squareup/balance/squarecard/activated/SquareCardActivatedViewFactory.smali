.class public final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "SquareCardActivatedViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 18

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 13
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 14
    sget-object v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen;->Companion:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_activated_view:I

    .line 15
    new-instance v16, Lcom/squareup/workflow/ScreenHint;

    const-class v8, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1f7

    const/4 v15, 0x0

    move-object/from16 v4, v16

    invoke-direct/range {v4 .. v15}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 16
    sget-object v4, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory$1;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object/from16 v4, v16

    .line 13
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 18
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 19
    sget-object v1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen;->Companion:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    sget v5, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_activated_google_pay_view:I

    .line 20
    new-instance v1, Lcom/squareup/workflow/ScreenHint;

    const-class v10, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x1f7

    const/16 v17, 0x0

    move-object v6, v1

    invoke-direct/range {v6 .. v17}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    sget-object v2, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory$2;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory$2;

    move-object v8, v2

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/16 v9, 0x8

    const/4 v10, 0x0

    .line 18
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 23
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 24
    sget-object v2, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen;->Companion:Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/activated/SuspendedSquareCardDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 25
    sget-object v3, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory$3;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory$3;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 23
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 27
    sget-object v1, Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 28
    sget-object v1, Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/notificationpreferences/common/NotificationPreferencesLoadingLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 29
    sget-object v1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    move-object/from16 v1, p0

    .line 12
    invoke-direct {v1, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
