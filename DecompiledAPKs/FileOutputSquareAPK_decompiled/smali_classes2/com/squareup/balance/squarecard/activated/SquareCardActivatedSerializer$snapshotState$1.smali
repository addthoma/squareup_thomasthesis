.class final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardActivatedSerializer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;->snapshotState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 21
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    .line 22
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;->isToggleInProgress()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    goto :goto_0

    .line 23
    :cond_0
    instance-of v1, v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    if-eqz v1, :cond_1

    .line 24
    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->isSuccess()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 25
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->getTitle()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 26
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;->getMessage()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    :cond_1
    :goto_0
    return-void
.end method
