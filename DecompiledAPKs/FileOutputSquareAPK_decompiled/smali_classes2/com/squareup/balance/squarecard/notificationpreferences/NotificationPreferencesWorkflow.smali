.class public final Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "NotificationPreferencesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002D\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0008\u0012\u00060\u0002j\u0002`\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0001:\u0001\u001eB1\u0008\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000c\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000c\u00a2\u0006\u0002\u0010\u0012J#\u0010\u0013\u001a\u00020\u00042\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u00032\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016\u00a2\u0006\u0002\u0010\u0017J[\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\n\u0010\u0014\u001a\u00060\u0002j\u0002`\u00032\u0006\u0010\u0019\u001a\u00020\u00042\u0016\u0010\u001a\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0008\u0012\u00060\u0002j\u0002`\u00050\u001bH\u0016\u00a2\u0006\u0002\u0010\u001cJ\u0010\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0004H\u0016R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesProps;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesFinished;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "fetchingWorkflow",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
        "displayPreferencesWorkflow",
        "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
        "errorWorkflow",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
        "(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayPreferencesWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final errorWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final fetchingWorkflow:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/fetching/NotificationPreferencesFetchingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/display/NotificationDisplayPreferencesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "fetchingWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayPreferencesWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->fetchingWorkflow:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->displayPreferencesWorkflow:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->errorWorkflow:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$FetchingPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$FetchingPreferences;

    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$FetchingPreferences;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$FetchingPreferences;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->fetchingWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "fetchingWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v2, 0x0

    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 83
    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingError;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingError;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 84
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->errorWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string p2, "errorWorkflow.get()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 85
    new-instance v2, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    .line 86
    new-instance p1, Lcom/squareup/resources/ResourceString;

    .line 87
    sget p2, Lcom/squareup/balance/squarecard/impl/R$string;->notification_preferences_error_fetching_notifications_title:I

    .line 86
    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    const/4 p2, 0x0

    .line 85
    invoke-direct {v2, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;-><init>(Lcom/squareup/resources/TextModel;Z)V

    const/4 v3, 0x0

    .line 91
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$2;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$2;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 83
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 93
    :cond_1
    instance-of p1, p2, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingPreferences;

    if-eqz p1, :cond_2

    .line 94
    iget-object p1, p0, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->displayPreferencesWorkflow:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "displayPreferencesWorkflow.get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 95
    check-cast p2, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingPreferences;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState$DisplayingPreferences;->getPreferences()Lcom/squareup/balance/squarecard/notificationpreferences/PreferencesDataResult$Success;

    move-result-object v3

    const/4 v4, 0x0

    .line 96
    sget-object p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$3;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow$render$3;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 93
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesWorkflow;->snapshotState(Lcom/squareup/balance/squarecard/notificationpreferences/NotificationPreferencesState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
