.class public final Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "ViewBillingViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 17

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 9
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 10
    sget-object v2, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen;->Companion:Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/billingaddress/DisplayAddressScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 11
    sget v3, Lcom/squareup/balance/squarecard/impl/R$layout;->square_card_view_billing_address_layout:I

    .line 12
    new-instance v16, Lcom/squareup/workflow/ScreenHint;

    const-class v8, Lcom/squareup/balance/squarecard/ManageSquareCardSection;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x1f7

    const/4 v15, 0x0

    move-object/from16 v4, v16

    invoke-direct/range {v4 .. v15}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 13
    sget-object v4, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory$1;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory$1;

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object/from16 v4, v16

    .line 9
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, p0

    .line 8
    invoke-direct {v1, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
