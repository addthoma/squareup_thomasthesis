.class public final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;
.super Lcom/squareup/workflow/CompoundWorkflowViewFactory;
.source "RealManageSquareCardWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B7\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardWorkflowViewFactory;",
        "Lcom/squareup/workflow/CompoundWorkflowViewFactory;",
        "orderWorkflowViewFactoryFactory",
        "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
        "manageSquareCardViewFactory",
        "Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;",
        "orderSquareCardViewFactory",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;",
        "squareCardOrderedWorkflowViewFactory",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;",
        "cancelSquareCardViewFactory",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;",
        "addPhoneNumberViewFactory",
        "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;",
        "(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderWorkflowViewFactoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "manageSquareCardViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderSquareCardViewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareCardOrderedWorkflowViewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cancelSquareCardViewFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addPhoneNumberViewFactory"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/workflow/WorkflowViewFactory;

    .line 26
    check-cast p2, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 27
    check-cast p3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p2, 0x1

    aput-object p3, v0, p2

    .line 28
    sget-object p3, Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardViewFactory;

    check-cast p3, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 p3, 0x0

    .line 29
    invoke-static {p1, p3, p2, p3}, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;->create$default(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Ljava/lang/Class;ILjava/lang/Object;)Lcom/squareup/mailorder/OrderWorkflowViewFactory;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p2, 0x3

    aput-object p1, v0, p2

    .line 30
    check-cast p4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x4

    aput-object p4, v0, p1

    .line 31
    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedViewFactory;

    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p2, 0x5

    aput-object p1, v0, p2

    .line 32
    check-cast p5, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x6

    aput-object p5, v0, p1

    .line 33
    check-cast p6, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 p1, 0x7

    aput-object p6, v0, p1

    .line 34
    sget-object p1, Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/billingaddress/ViewBillingViewFactory;

    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/16 p2, 0x8

    aput-object p1, v0, p2

    .line 35
    sget-object p1, Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;->INSTANCE:Lcom/squareup/balance/squarecard/authv2/CardOrderingAuthViewFactory;

    check-cast p1, Lcom/squareup/workflow/WorkflowViewFactory;

    const/16 p2, 0x9

    aput-object p1, v0, p2

    .line 25
    invoke-direct {p0, v0}, Lcom/squareup/workflow/CompoundWorkflowViewFactory;-><init>([Lcom/squareup/workflow/WorkflowViewFactory;)V

    return-void
.end method
