.class public final Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;
.super Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;
.source "ManageSquareCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardOrdered"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001BM\u0012>\u0010\u0003\u001a:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0004j\u0002`\u000b\u0012\u0006\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\rRL\u0010\u0003\u001a:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0002\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0004j\u0002`\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
        "subWorkflow",
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAdaptedWorkflow;",
        "card",
        "(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V",
        "getSubWorkflow",
        "()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final subWorkflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")V"
        }
    .end annotation

    const-string v0, "subWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "card"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 114
    invoke-direct {p0, p2, v0}, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;->subWorkflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    return-void
.end method


# virtual methods
.method public getSubWorkflow()Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowResult;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ManageSquareCardState$HostsWorkflow$WithCard$CardOrdered;->subWorkflow:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter;

    return-object v0
.end method
