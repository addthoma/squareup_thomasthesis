.class public abstract Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule;
.super Ljava/lang/Object;
.source "SquareCardTwoFactorAuthModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0008H!\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule;",
        "",
        "()V",
        "bindsResetPasswordTwoFactorDataStore",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
        "dataStore",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/ResetPasscodeTwoFactorDataStore;",
        "bindsShowPrivateDataTwoFactorDataStore",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule;->Companion:Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindsResetPasswordTwoFactorDataStore(Lcom/squareup/balance/squarecard/twofactorauth/datastore/ResetPasscodeTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;
    .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ResetPasswordTwoFactor;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindsShowPrivateDataTwoFactorDataStore(Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;
    .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ShowPrivateCardDataTwoFactor;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
