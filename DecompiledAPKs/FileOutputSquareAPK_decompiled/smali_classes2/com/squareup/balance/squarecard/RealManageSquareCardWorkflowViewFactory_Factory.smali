.class public final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealManageSquareCardWorkflowViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;",
            ">;)",
            "Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;)Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;
    .locals 8

    .line 63
    new-instance v7, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;-><init>(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;

    invoke-static/range {v1 .. v6}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->newInstance(Lcom/squareup/mailorder/OrderWorkflowViewFactory$Factory;Lcom/squareup/balance/squarecard/ManageSquareCardViewFactory;Lcom/squareup/balance/squarecard/order/OrderSquareCardViewFactory;Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedWorkflowViewFactory;Lcom/squareup/balance/squarecard/cancel/CancelSquareCardViewFactory;Lcom/squareup/balance/squarecard/addnumber/AddPhoneNumberViewFactory;)Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory_Factory;->get()Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflowViewFactory;

    move-result-object v0

    return-object v0
.end method
