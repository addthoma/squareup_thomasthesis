.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;
.super Ljava/lang/Object;
.source "AuthSquareCardReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;",
            ">;)",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lio/reactivex/Scheduler;Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lio/reactivex/Scheduler;Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v3, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lio/reactivex/Scheduler;Lcom/squareup/balance/squarecard/auth/AuthSquareCardAnalytics;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor_Factory;->get()Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    move-result-object v0

    return-object v0
.end method
