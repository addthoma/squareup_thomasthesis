.class final Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;
.super Ljava/lang/Object;
.source "AuthSquareCardCancelDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthSquareCardCancelDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthSquareCardCancelDialogFactory.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1\n*L\n1#1,51:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog$Event;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lkotlin/Unit;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialog$Event;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 28
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lkotlin/Unit;

    .line 29
    new-instance p1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 30
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_cancel_title:I

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 31
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_cancel_message:I

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 34
    sget v1, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 33
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 37
    sget v1, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 36
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_cancel_try_again:I

    new-instance v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1$$special$$inlined$with$lambda$1;

    invoke-direct {v2, p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1$$special$$inlined$with$lambda$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 42
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_cancel_leave:I

    new-instance v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1$$special$$inlined$with$lambda$2;

    invoke-direct {v2, p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1$$special$$inlined$with$lambda$2;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 45
    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1$$special$$inlined$with$lambda$3;

    invoke-direct {v1, p0, v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1$$special$$inlined$with$lambda$3;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
