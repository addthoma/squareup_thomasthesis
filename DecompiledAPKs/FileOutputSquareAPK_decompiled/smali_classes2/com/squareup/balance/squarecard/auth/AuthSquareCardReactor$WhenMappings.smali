.class public final synthetic Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 6

    invoke-static {}, Lcom/squareup/balance/squarecard/order/ApprovalState;->values()[Lcom/squareup/balance/squarecard/order/ApprovalState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->APPROVED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/ApprovalState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/ApprovalState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE_NEEDS_SSN:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/ApprovalState;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->REJECTED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/ApprovalState;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->values()[Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ADDRESS:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->BIRTHDATE:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->SSN:Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
