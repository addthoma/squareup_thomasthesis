.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AuthSquareCardSsnInfoCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthSquareCardSsnInfoCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthSquareCardSsnInfoCoordinator.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,74:1\n1103#2,7:75\n*E\n*S KotlinDebug\n*F\n+ 1 AuthSquareCardSsnInfoCoordinator.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator\n*L\n51#1,7:75\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000cH\u0016J\u0010\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000cH\u0002J\u0016\u0010\u0013\u001a\u00020\u00102\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0015H\u0002J(\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u000c2\u0016\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "Landroid/view/View;",
        "ownerSsn",
        "Lcom/squareup/widgets/SelectableEditText;",
        "attach",
        "",
        "view",
        "bindViews",
        "handleBack",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "update",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private continueButton:Landroid/view/View;

.field private ownerSsn:Lcom/squareup/widgets/SelectableEditText;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getOwnerSsn$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;)Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    if-nez p0, :cond_0

    const-string v0, "ownerSsn"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$setOwnerSsn$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 69
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 70
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_ssn:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    .line 71
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->continue_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->continueButton:Landroid/view/View;

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 65
    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event$DeclineSsnInfo;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event$DeclineSsnInfo;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->continueButton:Landroid/view/View;

    if-nez p1, :cond_0

    const-string v0, "continueButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 56
    :cond_1
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 57
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 58
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$update$3;

    invoke-direct {v2, p0, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$update$3;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_2

    const-string v0, "ownerSsn"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;

    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;->getSsn()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->bindViews(Landroid/view/View;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "ownerSsn"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-static {}, Lcom/squareup/text/TinFormatter;->createSsnFormatter()Lcom/squareup/text/TinFormatter;

    move-result-object v3

    check-cast v3, Lcom/squareup/text/Scrubber;

    iget-object v4, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->ownerSsn:Lcom/squareup/widgets/SelectableEditText;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v4, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v2, v3, v4}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
