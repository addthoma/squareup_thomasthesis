.class public final Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "RealAuthSquareCardScreenWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;,
        Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAuthSquareCardScreenWorkflowStarter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAuthSquareCardScreenWorkflowStarter.kt\ncom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter\n*L\n1#1,260:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 ,2\u00020\u0001:\u0002,-B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004JZ\u0010\u0005\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u0006j\u0002`\u000e2\u001c\u0010\u000f\u001a\u0018\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u0006j\u0002`\u0012H\u0002JD\u0010\u0005\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u0006j\u0002`\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016JD\u0010\u0005\u001a6\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0007\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u0006j\u0002`\u000e2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016JF\u0010\u0017\u001a0\u0012,\u0012*\u0012&\u0012$\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u0008j\u0008\u0012\u0004\u0012\u00020\t`\u00190\u00070\u00182\u0006\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010\u001d\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020\u001e2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010\u001f\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020 2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010!\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020\"2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010#\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020$2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010%\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020&2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010\'\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020(2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010)\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020*2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010+\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b*\u00020\u00102\u0006\u0010\u001b\u001a\u00020\u001cH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflowStarter;",
        "reactor",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;",
        "(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)V",
        "start",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardScreenWorkflow;",
        "workflow",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardWorkflow;",
        "startArg",
        "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toWorkflowState",
        "Lrx/Observable;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "state",
        "input",
        "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
        "toCancelDialogScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;",
        "toIdvErrorScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;",
        "toIdvRejectionScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;",
        "toIdvRetryScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;",
        "toMissingFieldDialogScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;",
        "toPersonalInfoScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;",
        "toSsnInfoScreen",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;",
        "toVerifyingIdvScreen",
        "Companion",
        "ScreenInput",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;

.field private static final SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData;",
            "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final reactor:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;

    .line 76
    sget-object v0, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->INSTANCE:Lcom/squareup/balance/squarecard/common/SquareCardProgress;

    sget-object v1, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->Companion:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$Companion;

    invoke-virtual {v0, v1}, Lcom/squareup/balance/squarecard/common/SquareCardProgress;->createKey(Ljava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    return-void
.end method

.method public static final synthetic access$getSQUARE_CARD_PROGRESS_KEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static final synthetic access$toWorkflowState(Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;
    .locals 0

    .line 66
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toWorkflowState(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "-",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
            "+",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$start$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$start$1;-><init>(Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;Lcom/squareup/workflow/rx1/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->switchMapState(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method private final toCancelDialogScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 242
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getCancel()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardCancelDialogScreenKt;->AuthSquareCardCancelDialogScreen(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toIdvErrorScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 217
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 218
    sget-object v1, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 219
    new-instance v9, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 220
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;->getTitle()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v3, v2

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 221
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;->getMessage()I

    move-result p1

    invoke-direct {v2, p1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/util/ViewString;

    .line 222
    sget v5, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v2, v9

    .line 219
    invoke-direct/range {v2 .. v8}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 224
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getError()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 217
    invoke-direct {v0, v1, v9, p1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method private final toIdvRejectionScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 246
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 247
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 248
    new-instance v8, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 249
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_idv_error_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 250
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->order_card_customization_idv_error_message:I

    invoke-direct {v1, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 251
    sget v4, Lcom/squareup/common/strings/R$string;->okay:I

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, v8

    .line 248
    invoke-direct/range {v1 .. v7}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 253
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getRejected()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 246
    invoke-direct {p1, v0, v8, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final toIdvRetryScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 229
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 230
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 231
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;

    .line 232
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_failure_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/util/ViewString;

    .line 233
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_failure_message:I

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/util/ViewString;

    .line 234
    sget v4, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_failure_try_again:I

    .line 235
    sget v5, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_failure_close:I

    .line 231
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Failed;-><init>(Lcom/squareup/util/ViewString;Lcom/squareup/util/ViewString;II)V

    .line 237
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getRetry()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 229
    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final toMissingFieldDialogScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 201
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;-><init>(Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;)V

    .line 202
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getMissingField()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 200
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialogScreenKt;->AuthSquareCardMissingFieldDialogScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardMissingFieldDialog$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toPersonalInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 183
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;

    .line 184
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerName()Ljava/lang/String;

    move-result-object v1

    .line 185
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerAddress()Lcom/squareup/address/Address;

    move-result-object v2

    .line 186
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 183
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;)V

    .line 188
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getPersonalInfo()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 182
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoScreenKt;->AuthSquareCardPersonalInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toSsnInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 194
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;->getOwnerSsn()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;-><init>(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getSsnInfo()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 193
    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfoScreenKt;->AuthSquareCardSsnInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardSsnInfo$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method

.method private final toVerifyingIdvScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 207
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 208
    sget-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->SQUARE_CARD_PROGRESS_KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    .line 209
    new-instance v1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;

    .line 210
    sget v2, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_idv_verifying:I

    .line 209
    invoke-direct {v1, v2}, Lcom/squareup/balance/squarecard/common/SquareCardProgress$ScreenData$Loading;-><init>(I)V

    .line 212
    invoke-virtual {p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;->getVerifying()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    .line 207
    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final toWorkflowState(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    .line 158
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$CheckingIdvState;

    if-eqz v0, :cond_0

    invoke-static {}, Lrx/Observable;->never()Lrx/Observable;

    move-result-object p1

    const-string p2, "never()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 159
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toPersonalInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto/16 :goto_2

    .line 160
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toSsnInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto/16 :goto_2

    .line 161
    :cond_2
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 162
    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toMissingFieldDialogScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v2

    .line 163
    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$NotifyMissingField;->getMissingField()Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;

    move-result-object v1

    sget-object v3, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/balance/squarecard/auth/MissingAuthSquareCardField;->ordinal()I

    move-result v1

    aget v1, v3, v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_4

    const/4 v3, 0x2

    if-eq v1, v3, :cond_4

    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 166
    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toSsnInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingSsnInfo;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    goto :goto_0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 165
    :cond_4
    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toPersonalInfoScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingPersonalInfo;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 161
    :goto_0
    invoke-virtual {v0, p2, v2}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto :goto_2

    .line 169
    :cond_5
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$StartIdv;

    if-eqz v0, :cond_6

    goto :goto_1

    .line 170
    :cond_6
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$PollIdv;

    if-eqz v0, :cond_7

    :goto_1
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toVerifyingIdvScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto :goto_2

    .line 171
    :cond_7
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toIdvErrorScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvError;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto :goto_2

    .line 172
    :cond_8
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toIdvRetryScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto :goto_2

    .line 173
    :cond_9
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 174
    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toCancelDialogScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvCancelConfirmation;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v1

    .line 175
    new-instance v2, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;->getIdvSettings()Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;-><init>(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)V

    invoke-direct {p0, v2, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toIdvRetryScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRetry;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    .line 173
    invoke-virtual {v0, p2, v1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    goto :goto_2

    .line 177
    :cond_a
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;

    invoke-direct {p0, v1, p2}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->toIdvRejectionScreen(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState$ShowingIdvRejection;Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p2

    .line 178
    :goto_2
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarterKt;->toSnapshot(Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    invoke-static {v0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    const-string p2, "just(ScreenState(this, state.toSnapshot()))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p2, "when (state) {\n      // \u2026s, state.toSnapshot())) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 177
    :cond_b
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public start(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->startWorkflow(Lcom/squareup/balance/squarecard/order/SquareCardIdvSettings;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->reactor:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;

    invoke-virtual {v0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor;->startWorkflow(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter;->start(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    return-object p1
.end method
