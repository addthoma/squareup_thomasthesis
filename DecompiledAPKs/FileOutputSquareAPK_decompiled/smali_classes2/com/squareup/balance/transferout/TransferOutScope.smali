.class public final Lcom/squareup/balance/transferout/TransferOutScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "TransferOutScope.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/transferout/TransferOutScope$ParentComponent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransferOutScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransferOutScope.kt\ncom/squareup/balance/transferout/TransferOutScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,24:1\n35#2:25\n*E\n*S KotlinDebug\n*F\n+ 1 TransferOutScope.kt\ncom/squareup/balance/transferout/TransferOutScope\n*L\n11#1:25\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/balance/transferout/TransferOutScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "ParentComponent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/balance/transferout/TransferOutScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/balance/transferout/TransferOutScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 8
    new-instance v0, Lcom/squareup/balance/transferout/TransferOutScope;

    invoke-direct {v0}, Lcom/squareup/balance/transferout/TransferOutScope;-><init>()V

    sput-object v0, Lcom/squareup/balance/transferout/TransferOutScope;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutScope;

    .line 22
    sget-object v0, Lcom/squareup/balance/transferout/TransferOutScope;->INSTANCE:Lcom/squareup/balance/transferout/TransferOutScope;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(TransferOutScope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/balance/transferout/TransferOutScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    const-class v0, Lcom/squareup/balance/transferout/TransferOutScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 11
    check-cast v0, Lcom/squareup/balance/transferout/TransferOutScope$ParentComponent;

    .line 12
    invoke-interface {v0}, Lcom/squareup/balance/transferout/TransferOutScope$ParentComponent;->transferOutWorkflowRunner()Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;

    move-result-object v0

    .line 14
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 15
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/balance/transferout/TransferOutWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
