.class public interface abstract Lcom/squareup/balance/core/server/bizbank/BizbankService;
.super Ljava/lang/Object;
.source "BizbankService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/core/server/bizbank/BizbankService$GetPrivateCardDataStandardResponse;,
        Lcom/squareup/balance/core/server/bizbank/BizbankService$CreateUserAuthorizationStandardResponse;,
        Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;,
        Lcom/squareup/balance/core/server/bizbank/BizbankService$FinishCardActivationStandardResponse;,
        Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b4\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001:\u0005[\\]^_J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\nH\'J\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\rH\'J\u0018\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0010H\'J\u0012\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0013H\'J\u0018\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0016H\'J\u0012\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0019H\'J\u0018\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u001cH\'J\u0018\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u001fH\'J\u0018\u0010 \u001a\u0008\u0012\u0004\u0012\u00020!0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\"H\'J\u0018\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020%H\'J\u0018\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020(H\'J\u0018\u0010)\u001a\u0008\u0012\u0004\u0012\u00020*0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020+H\'J\u0012\u0010,\u001a\u00020-2\u0008\u0008\u0001\u0010\u0005\u001a\u00020.H\'J\u0018\u0010/\u001a\u0008\u0012\u0004\u0012\u0002000\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u000201H\'J\u0018\u00102\u001a\u0008\u0012\u0004\u0012\u0002030\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u000204H\'J\u0018\u00105\u001a\u0008\u0012\u0004\u0012\u0002060\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u000207H\'J\u0018\u00108\u001a\u0008\u0012\u0004\u0012\u0002090\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020:H\'J\u0018\u0010;\u001a\u0008\u0012\u0004\u0012\u00020<0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020=H\'J\u0018\u0010>\u001a\u0008\u0012\u0004\u0012\u00020*0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020?H\'J\u0012\u0010@\u001a\u00020A2\u0008\u0008\u0001\u0010\u0005\u001a\u00020BH\'J\u0018\u0010C\u001a\u0008\u0012\u0004\u0012\u00020D0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020EH\'J\u0018\u0010F\u001a\u0008\u0012\u0004\u0012\u00020G0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020HH\'J\u0018\u0010I\u001a\u0008\u0012\u0004\u0012\u00020J0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020KH\'J\u0018\u0010L\u001a\u0008\u0012\u0004\u0012\u00020M0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020NH\'J\u0018\u0010O\u001a\u0008\u0012\u0004\u0012\u00020P0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020QH\'J\u0018\u0010R\u001a\u0008\u0012\u0004\u0012\u00020S0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020TH\'J\u0012\u0010U\u001a\u00020V2\u0008\u0008\u0001\u0010\u0005\u001a\u00020WH\'J\u0018\u0010X\u001a\u0008\u0012\u0004\u0012\u00020Y0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020ZH\'\u00a8\u0006`"
    }
    d2 = {
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "",
        "addMobileNumberForSmsNotifications",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsResponse;",
        "request",
        "Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest;",
        "checkIdvStatus",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
        "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;",
        "confirmIssueCard",
        "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;",
        "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;",
        "createCardCustomization",
        "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationResponse;",
        "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;",
        "createUserAuthorization",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService$CreateUserAuthorizationStandardResponse;",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;",
        "deactivateCard",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;",
        "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;",
        "finishCardActivation",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService$FinishCardActivationStandardResponse;",
        "Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;",
        "getAllStamps",
        "Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;",
        "Lcom/squareup/protos/client/bizbank/GetAllStampsRequest;",
        "getCardActivity",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;",
        "getCardBillingAddress",
        "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;",
        "getCustomizationSettings",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest;",
        "getInstantDepositFeeDetails",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;",
        "getNotificationPreferences",
        "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
        "Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;",
        "getPrivateCardData",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService$GetPrivateCardDataStandardResponse;",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;",
        "listCards",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse;",
        "Lcom/squareup/protos/client/bizbank/ListCardsRequest;",
        "provideFeedback",
        "Lcom/squareup/protos/client/bizbank/ProvideFeedbackResponse;",
        "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;",
        "provisionDigitalWalletToken",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;",
        "setBalanceStatus",
        "Lcom/squareup/protos/client/bizbank/SetBalanceStatusResponse;",
        "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;",
        "setCardBillingAddress",
        "Lcom/squareup/protos/client/bizbank/SetCardBillingAddressResponse;",
        "Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest;",
        "setNotificationPreferences",
        "Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;",
        "setPasscode",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;",
        "Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;",
        "setTransactionCategory",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;",
        "startCardActivation",
        "Lcom/squareup/protos/client/bizbank/StartCardActivationResponse;",
        "Lcom/squareup/protos/client/bizbank/StartCardActivationRequest;",
        "startIdv",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;",
        "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;",
        "startIssueCard",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
        "Lcom/squareup/protos/client/bizbank/StartIssueCardRequest;",
        "startTwoFactorAuthentication",
        "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationResponse;",
        "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;",
        "toggleCardFreeze",
        "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;",
        "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;",
        "verifyCardActivationToken",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest;",
        "verifyTwoFactorAuthentication",
        "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;",
        "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;",
        "CreateUserAuthorizationStandardResponse",
        "FinishCardActivationStandardResponse",
        "GetPrivateCardDataStandardResponse",
        "SetPasscodeStandardResponse",
        "VerifyCardActivationTokenStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addMobileNumberForSmsNotifications(Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/AddMobileNumberForSmsNotificationsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/add-mobile-number-for-sms-notifications"
    .end annotation
.end method

.method public abstract checkIdvStatus(Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bizbank/CheckIdentityVerificationStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/check-identity-verification-status"
    .end annotation
.end method

.method public abstract confirmIssueCard(Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/confirm-issue-card"
    .end annotation
.end method

.method public abstract createCardCustomization(Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/CreateCardCustomizationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/create-card-customization"
    .end annotation
.end method

.method public abstract createUserAuthorization(Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$CreateUserAuthorizationStandardResponse;
    .param p1    # Lcom/squareup/protos/bizbank/CreateUserAuthorizationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/create-user-authorization"
    .end annotation
.end method

.method public abstract deactivateCard(Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/DeactivateCardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/DeactivateCardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/deactivate-card"
    .end annotation
.end method

.method public abstract finishCardActivation(Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$FinishCardActivationStandardResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/FinishCardActivationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/finish-card-activation"
    .end annotation
.end method

.method public abstract getAllStamps(Lcom/squareup/protos/client/bizbank/GetAllStampsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/GetAllStampsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/GetAllStampsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bizbank/GetAllStampsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-all-stamps"
    .end annotation
.end method

.method public abstract getCardActivity(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-card-activity"
    .end annotation
.end method

.method public abstract getCardBillingAddress(Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-card-billing-address"
    .end annotation
.end method

.method public abstract getCustomizationSettings(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-customization-settings"
    .end annotation
.end method

.method public abstract getInstantDepositFeeDetails(Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/instant-deposits/get-instant-deposit-fee-details"
    .end annotation
.end method

.method public abstract getNotificationPreferences(Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/service/GetNotificationPreferencesRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-notification-preferences"
    .end annotation
.end method

.method public abstract getPrivateCardData(Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$GetPrivateCardDataStandardResponse;
    .param p1    # Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/get-private-card-data"
    .end annotation
.end method

.method public abstract listCards(Lcom/squareup/protos/client/bizbank/ListCardsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/ListCardsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/list-cards"
    .end annotation
.end method

.method public abstract provideFeedback(Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ProvideFeedbackRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/ProvideFeedbackResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/provide-feedback"
    .end annotation
.end method

.method public abstract provisionDigitalWalletToken(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/provision-digital-wallet-token"
    .end annotation
.end method

.method public abstract setBalanceStatus(Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/SetBalanceStatusRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/SetBalanceStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/set-balance-status"
    .end annotation
.end method

.method public abstract setCardBillingAddress(Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/SetCardBillingAddressRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/SetCardBillingAddressResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/set-card-billing-address"
    .end annotation
.end method

.method public abstract setNotificationPreferences(Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/service/SetNotificationPreferencesRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/bizbank/service/NotificationPreferencesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/set-notification-preferences"
    .end annotation
.end method

.method public abstract setPasscode(Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$SetPasscodeStandardResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/SetPasscodeRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/set-passcode"
    .end annotation
.end method

.method public abstract setTransactionCategory(Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/set-transaction-category"
    .end annotation
.end method

.method public abstract startCardActivation(Lcom/squareup/protos/client/bizbank/StartCardActivationRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/StartCardActivationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/StartCardActivationRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/StartCardActivationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/start-card-activation"
    .end annotation
.end method

.method public abstract startIdv(Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bizbank/StartIdentityVerificationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/start-identity-verification"
    .end annotation
.end method

.method public abstract startIssueCard(Lcom/squareup/protos/client/bizbank/StartIssueCardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/StartIssueCardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/StartIssueCardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/start-issue-card"
    .end annotation
.end method

.method public abstract startTwoFactorAuthentication(Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/StartTwoFactorAuthenticationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/start-two-factor-authentication"
    .end annotation
.end method

.method public abstract toggleCardFreeze(Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bizbank/ToggleCardFreezeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/toggle-card-freeze"
    .end annotation
.end method

.method public abstract verifyCardActivationToken(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/verify-card-activation-token"
    .end annotation
.end method

.method public abstract verifyTwoFactorAuthentication(Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bizbank/verify-two-factor-authentication"
    .end annotation
.end method
