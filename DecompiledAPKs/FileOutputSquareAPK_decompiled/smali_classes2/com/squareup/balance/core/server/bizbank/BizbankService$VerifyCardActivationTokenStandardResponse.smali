.class public final Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;
.super Lcom/squareup/server/StandardResponse;
.source "BizbankService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/core/server/bizbank/BizbankService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VerifyCardActivationTokenStandardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/StandardResponse<",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0002H\u0014\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;",
        "Lcom/squareup/server/StandardResponse;",
        "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
        "factory",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "(Lcom/squareup/server/StandardResponse$Factory;)V",
        "isSuccessful",
        "",
        "response",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/server/StandardResponse$Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/StandardResponse$Factory<",
            "Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    invoke-direct {p0, p1}, Lcom/squareup/server/StandardResponse;-><init>(Lcom/squareup/server/StandardResponse$Factory;)V

    return-void
.end method


# virtual methods
.method protected isSuccessful(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;)Z
    .locals 1

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 232
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    sget-object v0, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic isSuccessful(Ljava/lang/Object;)Z
    .locals 0

    .line 228
    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService$VerifyCardActivationTokenStandardResponse;->isSuccessful(Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse;)Z

    move-result p1

    return p1
.end method
