.class public final Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;
.super Ljava/lang/Object;
.source "BizBankProgressScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\'\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\'\u0010\n\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;",
        "",
        "()V",
        "DETAIL_KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapperKey;",
        "getDETAIL_KEY",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "SHEET_KEY",
        "getSHEET_KEY",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDETAIL_KEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 68
    invoke-static {}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->access$getDETAIL_KEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method

.method public final getSHEET_KEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 70
    invoke-static {}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->access$getSHEET_KEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method
