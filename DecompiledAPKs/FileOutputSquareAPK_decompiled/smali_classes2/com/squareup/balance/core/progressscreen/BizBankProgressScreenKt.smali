.class public final Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;
.super Ljava/lang/Object;
.source "BizBankProgressScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a \u0010\u0000\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0004*\u00020\u0002\u001a6\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001*\u00020\u00022\u001c\u0010\u0006\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007j\u0008\u0012\u0004\u0012\u00020\u0002`\u0008H\u0002\u001a \u0010\t\u001a\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0004*\u00020\u0002\u00a8\u0006\n"
    }
    d2 = {
        "asDetailScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "asScreen",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapperKey;",
        "asSheetScreen",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asDetailScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1

    const-string v0, "$this$asDetailScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->Companion:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;->getDETAIL_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    return-object p0
.end method

.method private static final asScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    invoke-direct {v0, p1, p0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public static final asSheetScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1

    const-string v0, "$this$asSheetScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;->Companion:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Companion;->getSHEET_KEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreenKt;->asScreen(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen;Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    return-object p0
.end method
