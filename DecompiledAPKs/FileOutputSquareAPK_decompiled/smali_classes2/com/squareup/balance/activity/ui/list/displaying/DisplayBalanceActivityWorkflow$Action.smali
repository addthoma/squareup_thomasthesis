.class public abstract Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action;
.super Ljava/lang/Object;
.source "DisplayBalanceActivityWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RefreshData;,
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$OpenTransferReports;,
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;,
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;,
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;,
        Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RetryFetchingMore;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016J\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\u0008H\u0002\u0082\u0001\u0006\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "replaceLoadMoreErrorWithLoading",
        "Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;",
        "ActivityTapped",
        "BalanceActivityLoaded",
        "LoadMoreActivity",
        "OpenTransferReports",
        "RefreshData",
        "RetryFetchingMore",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RefreshData;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$OpenTransferReports;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;",
        "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RetryFetchingMore;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action;-><init>()V

    return-void
.end method

.method private final replaceLoadMoreErrorWithLoading(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;
    .locals 3

    .line 173
    instance-of v0, p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;

    if-eqz v0, :cond_0

    .line 174
    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;->getActivity()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;

    .line 175
    instance-of v1, v1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$ErrorRow;

    if-eqz v1, :cond_0

    .line 177
    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;->getActivity()Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;->getActivity()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p1, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    sget-object v1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$LoadMore;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$LoadMore;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 176
    invoke-virtual {v0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;->copy(Ljava/util/List;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    :cond_0
    return-object p1
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
            ">;)",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v3

    new-instance v4, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$BalanceActivityLoaded;->getActivityLoaded()Ljava/util/List;

    move-result-object v2

    invoke-direct {v4, v2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult$Success;-><init>(Ljava/util/List;)V

    check-cast v4, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    invoke-direct {v0, v3, v4}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 127
    :cond_0
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;

    if-eqz v0, :cond_1

    .line 128
    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;

    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;->getCurrentState()Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v3

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$LoadMoreActivity;->getCurrentState()Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;->getActivities()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    move-result-object v2

    invoke-direct {v0, v3, v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :cond_1
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;

    if-eqz v0, :cond_2

    .line 132
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;->getCurrentState()Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    .line 133
    new-instance p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult$ActivityWasTapped;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$ActivityTapped;->getActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult$ActivityWasTapped;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivity;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;

    goto :goto_0

    .line 135
    :cond_2
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RefreshData;

    if-eqz v0, :cond_3

    .line 136
    sget-object v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingBalanceActivity;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 139
    :cond_3
    instance-of v0, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RetryFetchingMore;

    if-eqz v0, :cond_4

    .line 140
    new-instance v0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;

    .line 141
    move-object v2, p0

    check-cast v2, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RetryFetchingMore;

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RetryFetchingMore;->getCurrentState()Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;->getType()Lcom/squareup/balance/activity/data/BalanceActivityType;

    move-result-object v3

    .line 142
    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$RetryFetchingMore;->getCurrentState()Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$ShowingBalanceActivities;->getActivities()Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action;->replaceLoadMoreErrorWithLoading(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;

    move-result-object v2

    .line 140
    invoke-direct {v0, v3, v2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState$FetchingMoreBalanceActivities;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivityResult;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 146
    :cond_4
    sget-object p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$OpenTransferReports;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action$OpenTransferReports;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object p1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult$NavigateToTransferReports;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult$NavigateToTransferReports;

    move-object v1, p1

    check-cast v1, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;

    :goto_0
    return-object v1

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 120
    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityState;",
            "-",
            "Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
