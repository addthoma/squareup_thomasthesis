.class public abstract Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsSuccessScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Description"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;,
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$PillDecorationDescription;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u0082\u0001\u0002\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
        "",
        "()V",
        "onClick",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;",
        "getOnClick",
        "()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;",
        "priority",
        "",
        "getPriority",
        "()I",
        "text",
        "Lcom/squareup/resources/TextModel;",
        "",
        "getText",
        "()Lcom/squareup/resources/TextModel;",
        "PillDecorationDescription",
        "SimpleLineDescription",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$PillDecorationDescription;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getOnClick()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;
.end method

.method public abstract getPriority()I
.end method

.method public abstract getText()Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end method
