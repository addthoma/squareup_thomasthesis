.class final Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBalanceActivityMapper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->subLabel(Lcom/squareup/protos/bizbank/UnifiedActivity;Z)Lcom/squareup/resources/TextModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/resources/TextModel<",
        "*>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/resources/TextModel;",
        "date",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;Lcom/squareup/protos/bizbank/UnifiedActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;->this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;->$activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/resources/TextModel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "*>;"
        }
    .end annotation

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;->$activity:Lcom/squareup/protos/bizbank/UnifiedActivity;

    iget-object v1, v1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    sget-object v2, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    if-ne v1, v2, :cond_0

    .line 69
    new-instance v1, Lcom/squareup/resources/PhraseModel;

    sget v2, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_sub_label_card_payment:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 70
    invoke-virtual {v1, v0, p1}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object p1

    check-cast p1, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;->this$0:Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;

    invoke-static {v0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;->access$dateToViewString(Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper;Ljava/lang/String;)Lcom/squareup/resources/TextModel;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/list/RealBalanceActivityMapper$subLabel$1;->invoke(Ljava/lang/String;)Lcom/squareup/resources/TextModel;

    move-result-object p1

    return-object p1
.end method
