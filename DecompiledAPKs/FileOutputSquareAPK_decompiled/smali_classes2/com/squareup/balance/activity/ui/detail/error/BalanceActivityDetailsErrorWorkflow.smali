.class public final Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "BalanceActivityDetailsErrorWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsErrorWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsErrorWorkflow.kt\ncom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,38:1\n149#2,5:39\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceActivityDetailsErrorWorkflow.kt\ncom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow\n*L\n34#1,5:39\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u000020\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012 \u0012\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0002`\u00080\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ@\u0010\u000c\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0002`\u00082\u0006\u0010\r\u001a\u00020\u00022\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00030\u000fH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityScreen;",
        "mapper",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
        "(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivity;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;->render(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 30
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;

    .line 31
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow;->mapper:Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;

    invoke-virtual {v1, p1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->actionBarTitle(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/util/ViewString;

    move-result-object p1

    .line 32
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow$render$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 33
    new-instance v2, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow$render$2;

    invoke-direct {v2, p2}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 30
    invoke-direct {v0, p1, v2, v1}, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;-><init>(Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 40
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 41
    const-class p2, Lcom/squareup/balance/activity/ui/detail/error/BalanceActivityDetailsErrorScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 42
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 40
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 35
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
