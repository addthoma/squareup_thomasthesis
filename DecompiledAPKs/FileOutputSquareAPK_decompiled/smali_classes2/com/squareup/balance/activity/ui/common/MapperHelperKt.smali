.class public final Lcom/squareup/balance/activity/ui/common/MapperHelperKt;
.super Ljava/lang/Object;
.source "MapperHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "shouldCrossOut",
        "",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final shouldCrossOut(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;)Z
    .locals 1

    const-string v0, "$this$shouldCrossOut"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->VOIDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DECLINED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
