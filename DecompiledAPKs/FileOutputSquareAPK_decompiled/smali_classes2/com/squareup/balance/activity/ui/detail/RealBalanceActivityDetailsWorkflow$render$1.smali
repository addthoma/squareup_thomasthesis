.class final Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBalanceActivityDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow;->render(Lcom/squareup/balance/activity/ui/details/BalanceActivityDetailsProps;Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
        "",
        "result",
        "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;

    invoke-direct {v0}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    instance-of v0, p1, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult$DataLoaded;

    if-eqz v0, :cond_2

    .line 81
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult$DataLoaded;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult$DataLoaded;->getBalanceActivityDetails()Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    move-result-object v0

    .line 82
    instance-of v1, v0, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;

    .line 83
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult$DataLoaded;->getBalanceActivityDetails()Lcom/squareup/balance/activity/data/BalanceActivityDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivityDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p1

    .line 82
    invoke-direct {v0, p1}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayDetails;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)V

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;

    goto :goto_0

    .line 85
    :cond_0
    sget-object p1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ErrorLoadingDetails;->INSTANCE:Lcom/squareup/balance/activity/data/BalanceActivityDetails$ErrorLoadingDetails;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayFetchingError;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$DisplayFetchingError;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action;

    .line 81
    :goto_0
    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_1

    .line 85
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 88
    :cond_2
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult$Back;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult$Back;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$GoBack;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$Action$GoBack;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_1
    return-object v0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 55
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/RealBalanceActivityDetailsWorkflow$render$1;->invoke(Lcom/squareup/balance/activity/ui/detail/loading/BalanceActivityDetailsLoadingResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
