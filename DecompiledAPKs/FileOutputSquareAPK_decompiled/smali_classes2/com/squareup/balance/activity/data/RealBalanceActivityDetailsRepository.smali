.class public final Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;
.super Ljava/lang/Object;
.source "RealBalanceActivityDetailsRepository.kt"

# interfaces
.implements Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBalanceActivityDetailsRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBalanceActivityDetailsRepository.kt\ncom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,85:1\n250#2,2:86\n*E\n*S KotlinDebug\n*F\n+ 1 RealBalanceActivityDetailsRepository.kt\ncom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository\n*L\n17#1,2:86\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007H\u0002J\u0016\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\tH\u0016J\u0010\u0010\u0011\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0007H\u0002J\u0018\u0010\u0012\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u001e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000c2\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0016\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000c2\u0006\u0010\n\u001a\u00020\u0007H\u0016J\u0016\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u000c2\u0006\u0010\n\u001a\u00020\u0007H\u0016R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetailsRepository;",
        "remoteStore",
        "Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;",
        "(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;)V",
        "cachedDetails",
        "",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
        "addToLocalCache",
        "",
        "activity",
        "balanceActivityDetails",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
        "activityToken",
        "",
        "clear",
        "removeFromLocalCache",
        "updateActivity",
        "isPersonal",
        "",
        "updateCategory",
        "updateCategoryToBusiness",
        "updateCategoryToPersonal",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cachedDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "remoteStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;

    .line 13
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->cachedDetails:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$addToLocalCache(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V
    .locals 0

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->addToLocalCache(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V

    return-void
.end method

.method public static final synthetic access$removeFromLocalCache(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V
    .locals 0

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->removeFromLocalCache(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V

    return-void
.end method

.method public static final synthetic access$updateActivity(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;
    .locals 0

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->updateActivity(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    move-result-object p0

    return-object p0
.end method

.method private final addToLocalCache(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->cachedDetails:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private final removeFromLocalCache(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->cachedDetails:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private final updateActivity(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;
    .locals 2

    .line 74
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivityDetails()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    move-result-object v0

    .line 75
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;

    move-result-object p2

    .line 76
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails;

    move-result-object p2

    const-string/jumbo v0, "updatedDetails"

    .line 78
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p1, v0, p2, v1, v0}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->copy$default(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;ILjava/lang/Object;)Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    move-result-object p1

    return-object p1
.end method

.method private final updateCategory(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation

    .line 49
    invoke-virtual {p1}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getTransactionToken()Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;

    .line 52
    invoke-interface {v1, v0, p2}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;->updateCategory(Ljava/lang/String;Z)Lio/reactivex/Completable;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$1;-><init>(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Completable;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$2;-><init>(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v1}, Lio/reactivex/Completable;->toSingle(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p2

    .line 59
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$3;

    invoke-direct {v0, p1}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$updateCategory$3;-><init>(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "remoteStore\n        .upd\u2026nErrorReturn { activity }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public balanceActivityDetails(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails;",
            ">;"
        }
    .end annotation

    const-string v0, "activityToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->cachedDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 86
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    .line 17
    invoke-virtual {v2}, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;->getUnifiedActivity()Lcom/squareup/protos/bizbank/UnifiedActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_token:Ljava/lang/String;

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 16
    :goto_0
    check-cast v1, Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;

    if-eqz v1, :cond_2

    .line 20
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(detailsFromCache)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 22
    :cond_2
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->remoteStore:Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;

    invoke-interface {v0, p1}, Lcom/squareup/balance/activity/data/RemoteBalanceActivityDetailsDataStore;->fetchDetails(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 23
    new-instance v0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$balanceActivityDetails$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository$balanceActivityDetails$1;-><init>(Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "remoteStore.fetchDetails\u2026            }\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method public clear()V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->cachedDetails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public updateCategoryToBusiness(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->updateCategory(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public updateCategoryToPersonal(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;",
            ">;"
        }
    .end annotation

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 36
    invoke-direct {p0, p1, v0}, Lcom/squareup/balance/activity/data/RealBalanceActivityDetailsRepository;->updateCategory(Lcom/squareup/balance/activity/data/BalanceActivityDetails$ActivityDetailsData;Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
