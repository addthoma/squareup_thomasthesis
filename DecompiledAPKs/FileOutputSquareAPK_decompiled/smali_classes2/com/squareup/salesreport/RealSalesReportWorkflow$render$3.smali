.class final Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSalesReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/RealSalesReportWorkflow;->render(Lcom/squareup/salesreport/SalesReportProps;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/salesreport/SalesReportState;",
        "+",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "it",
        "",
        "invoke",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/SalesReportState;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/SalesReportState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;->$state:Lcom/squareup/salesreport/SalesReportState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 202
    new-instance v0, Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    .line 203
    iget-object v1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;->$state:Lcom/squareup/salesreport/SalesReportState;

    check-cast v1, Lcom/squareup/salesreport/SalesReportState$ExportingReport;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ExportingReport;->getSalesReport()Lcom/squareup/customreport/data/WithSalesReport;

    move-result-object v1

    .line 204
    iget-object v2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v2}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    .line 205
    iget-object v3, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;->$state:Lcom/squareup/salesreport/SalesReportState;

    invoke-virtual {v3}, Lcom/squareup/salesreport/SalesReportState;->getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    move-result-object v3

    .line 202
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/salesreport/SalesReportState$ViewingReport;-><init>(Lcom/squareup/customreport/data/WithSalesReport;Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 201
    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow$render$3;->invoke(Lkotlin/Unit;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
