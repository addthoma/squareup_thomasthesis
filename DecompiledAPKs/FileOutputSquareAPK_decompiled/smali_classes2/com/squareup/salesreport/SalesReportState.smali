.class public abstract Lcom/squareup/salesreport/SalesReportState;
.super Ljava/lang/Object;
.source "SalesReportState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/SalesReportState$ViewingReport;,
        Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;,
        Lcom/squareup/salesreport/SalesReportState$EmptyReport;,
        Lcom/squareup/salesreport/SalesReportState$InitialState;,
        Lcom/squareup/salesreport/SalesReportState$LoadingReport;,
        Lcom/squareup/salesreport/SalesReportState$NetworkFailure;,
        Lcom/squareup/salesreport/SalesReportState$CustomizingReport;,
        Lcom/squareup/salesreport/SalesReportState$ExportingReport;,
        Lcom/squareup/salesreport/SalesReportState$TopSectionState;,
        Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;,
        Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;,
        Lcom/squareup/salesreport/SalesReportState$ViewCount;,
        Lcom/squareup/salesreport/SalesReportState$UiSelectionState;,
        Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportState.kt\ncom/squareup/salesreport/SalesReportState\n*L\n1#1,164:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001cB\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0008\u001d\u001e\u001f !\"#$\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportState;",
        "Landroid/os/Parcelable;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "uiSelectionState",
        "Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
        "(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V",
        "getReportConfig",
        "()Lcom/squareup/customreport/data/ReportConfig;",
        "getUiSelectionState",
        "()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;",
        "verifyState",
        "",
        "earliestSelectableDate",
        "Lorg/threeten/bp/LocalDate;",
        "ChartSalesSelection",
        "CustomizingReport",
        "EmptyReport",
        "ExportingReport",
        "GrossCountSelection",
        "InitialState",
        "ItemOrCategory",
        "LoadingReport",
        "NetworkFailure",
        "TopSectionState",
        "UiSelectionState",
        "ViewCount",
        "ViewingFeesNote",
        "ViewingReport",
        "Lcom/squareup/salesreport/SalesReportState$ViewingReport;",
        "Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;",
        "Lcom/squareup/salesreport/SalesReportState$EmptyReport;",
        "Lcom/squareup/salesreport/SalesReportState$InitialState;",
        "Lcom/squareup/salesreport/SalesReportState$LoadingReport;",
        "Lcom/squareup/salesreport/SalesReportState$NetworkFailure;",
        "Lcom/squareup/salesreport/SalesReportState$CustomizingReport;",
        "Lcom/squareup/salesreport/SalesReportState$ExportingReport;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field private final uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;


# direct methods
.method private constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportState;->uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2}, Lcom/squareup/salesreport/SalesReportState;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/SalesReportState$UiSelectionState;)V

    return-void
.end method


# virtual methods
.method public getReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public getUiSelectionState()Lcom/squareup/salesreport/SalesReportState$UiSelectionState;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportState;->uiSelectionState:Lcom/squareup/salesreport/SalesReportState$UiSelectionState;

    return-object v0
.end method

.method public final verifyState(Lorg/threeten/bp/LocalDate;)V
    .locals 3

    .line 157
    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportState;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    if-eqz p1, :cond_1

    .line 158
    move-object v1, v0

    check-cast v1, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {p1, v1}, Lorg/threeten/bp/LocalDate;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result v1

    if-gtz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    return-void

    .line 159
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startDate("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ") should be on or after"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " earliestStartDate("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 158
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
