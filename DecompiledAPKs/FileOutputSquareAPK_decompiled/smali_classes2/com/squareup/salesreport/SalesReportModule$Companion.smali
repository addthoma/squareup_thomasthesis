.class public final Lcom/squareup/salesreport/SalesReportModule$Companion;
.super Ljava/lang/Object;
.source "SalesReportModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/SalesReportModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportModule.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportModule.kt\ncom/squareup/salesreport/SalesReportModule$Companion\n+ 2 LocalSettingFactory.kt\ncom/squareup/settings/LocalSettingFactoryKt\n*L\n1#1,48:1\n20#2:49\n*E\n*S KotlinDebug\n*F\n+ 1 SalesReportModule.kt\ncom/squareup/salesreport/SalesReportModule$Companion\n*L\n44#1:49\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/salesreport/SalesReportModule$Companion;",
        "",
        "()V",
        "provideIncludeItemsInReportLocalSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "",
        "localSettingFactory",
        "Lcom/squareup/settings/LocalSettingFactory;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/salesreport/SalesReportModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideIncludeItemsInReportLocalSetting(Lcom/squareup/settings/LocalSettingFactory;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Lcom/squareup/salesreport/export/IncludeItemsInReport;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSettingFactory;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "localSettingFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 49
    const-class v1, Ljava/lang/Boolean;

    const-string v2, "include-items-in-sales-report"

    invoke-interface {p1, v1, v2, v0}, Lcom/squareup/settings/LocalSettingFactory;->create(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/settings/LocalSetting;

    move-result-object p1

    return-object p1
.end method
