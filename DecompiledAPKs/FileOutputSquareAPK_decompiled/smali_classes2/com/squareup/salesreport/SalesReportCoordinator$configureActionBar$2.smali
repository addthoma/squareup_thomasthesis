.class final Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesReportCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/SalesReportCoordinator;->configureActionBar(Lcom/squareup/salesreport/SalesReportScreen;ZZLcom/squareup/salesreport/SalesReportState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/salesreport/SalesReportCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/SalesReportCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 177
    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    invoke-static {v0}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getComparisonRangeActions$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/PopupActions;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$2;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    invoke-static {v1}, Lcom/squareup/salesreport/SalesReportCoordinator;->access$getActionBar$p(Lcom/squareup/salesreport/SalesReportCoordinator;)Lcom/squareup/salesreport/widget/NohoActionBarWithTwoActionIcons;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/squareup/salesreport/widget/PopupActions;->toggle(Landroid/view/View;)V

    return-void
.end method
