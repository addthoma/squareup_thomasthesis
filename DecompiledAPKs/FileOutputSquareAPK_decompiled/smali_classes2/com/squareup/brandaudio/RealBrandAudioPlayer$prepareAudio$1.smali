.class final Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;
.super Ljava/lang/Object;
.source "RealBrandAudioPlayer.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/brandaudio/RealBrandAudioPlayer;->prepareAudio(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBrandAudioPlayer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBrandAudioPlayer.kt\ncom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $filePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;


# direct methods
.method constructor <init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    iput-object p2, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .line 66
    :try_start_0
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    invoke-static {v0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$getPlayer$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/AudioPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/brandaudio/AudioPlayer;->reset()V

    .line 67
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    invoke-static {v0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$getContext$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Application;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->$filePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    check-cast v0, Ljava/io/Closeable;

    const/4 v1, 0x0

    .line 68
    check-cast v1, Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    move-object v2, v0

    check-cast v2, Landroid/content/res/AssetFileDescriptor;

    .line 69
    iget-object v3, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    invoke-static {v3}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$getPlayer$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/AudioPlayer;

    move-result-object v4

    const-string v3, "it"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v5

    const-string v3, "it.fileDescriptor"

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v6

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v8

    invoke-interface/range {v4 .. v9}, Lcom/squareup/brandaudio/AudioPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 70
    iget-object v2, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    invoke-static {v2}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$getPlayer$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/AudioPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/brandaudio/AudioPlayer;->prepare()V

    .line 71
    sget-object v2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 68
    :try_start_2
    invoke-static {v0, v1}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->this$0:Lcom/squareup/brandaudio/RealBrandAudioPlayer;

    iget-object v1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->$filePath:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->access$setPreparedAudioPath$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catchall_0
    move-exception v1

    .line 68
    :try_start_3
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v2

    :try_start_4
    invoke-static {v0, v1}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    .line 74
    check-cast v0, Ljava/lang/Throwable;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to prepare brand audio for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;->$filePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void
.end method
