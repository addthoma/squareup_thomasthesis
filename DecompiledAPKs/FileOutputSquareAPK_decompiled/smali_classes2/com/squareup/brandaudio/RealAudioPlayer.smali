.class public final Lcom/squareup/brandaudio/RealAudioPlayer;
.super Landroid/media/MediaPlayer;
.source "RealAudioPlayer.kt"

# interfaces
.implements Lcom/squareup/brandaudio/AudioPlayer;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016J\u0008\u0010\u0008\u001a\u00020\u0005H\u0016J\u0008\u0010\t\u001a\u00020\u0005H\u0016J\u0008\u0010\n\u001a\u00020\u0005H\u0016J\u0010\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u000c\u001a\u00020\rH\u0016J \u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0016J\u0010\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0007H\u0016J\u0016\u0010\u0015\u001a\u00020\u00052\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0005H\u0016\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/brandaudio/RealAudioPlayer;",
        "Landroid/media/MediaPlayer;",
        "Lcom/squareup/brandaudio/AudioPlayer;",
        "()V",
        "clearOnCompletionListener",
        "",
        "isPlaying",
        "",
        "prepare",
        "release",
        "reset",
        "setAudioStreamType",
        "streamType",
        "",
        "setDataSource",
        "fd",
        "Ljava/io/FileDescriptor;",
        "offset",
        "",
        "length",
        "setLooping",
        "setOnCompletionListener",
        "completionListener",
        "Lkotlin/Function0;",
        "start",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    return-void
.end method


# virtual methods
.method public clearOnCompletionListener()V
    .locals 1

    const/4 v0, 0x0

    .line 31
    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    return-void
.end method

.method public isPlaying()Z
    .locals 1

    .line 7
    invoke-super {p0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public prepare()V
    .locals 0

    .line 9
    invoke-super {p0}, Landroid/media/MediaPlayer;->prepare()V

    return-void
.end method

.method public release()V
    .locals 0

    .line 11
    invoke-super {p0}, Landroid/media/MediaPlayer;->release()V

    return-void
.end method

.method public reset()V
    .locals 0

    .line 13
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    return-void
.end method

.method public setAudioStreamType(I)V
    .locals 0

    .line 15
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    return-void
.end method

.method public setDataSource(Ljava/io/FileDescriptor;JJ)V
    .locals 1

    const-string v0, "fd"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-super/range {p0 .. p5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    return-void
.end method

.method public setLooping(Z)V
    .locals 0

    .line 25
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    return-void
.end method

.method public setOnCompletionListener(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "completionListener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/squareup/brandaudio/RealAudioPlayer$setOnCompletionListener$1;

    invoke-direct {v0, p1}, Lcom/squareup/brandaudio/RealAudioPlayer$setOnCompletionListener$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-super {p0, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    return-void
.end method

.method public start()V
    .locals 0

    .line 23
    invoke-super {p0}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method
