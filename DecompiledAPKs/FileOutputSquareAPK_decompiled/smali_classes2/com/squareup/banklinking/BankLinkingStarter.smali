.class public interface abstract Lcom/squareup/banklinking/BankLinkingStarter;
.super Ljava/lang/Object;
.source "BankLinkingStarter.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/BankLinkingStarter$Variant;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0007J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/banklinking/BankLinkingStarter;",
        "Lmortar/Scoped;",
        "maybeStartBankLinking",
        "",
        "variant",
        "Lrx/Observable;",
        "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
        "Variant",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract maybeStartBankLinking()V
.end method

.method public abstract variant()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
            ">;"
        }
    .end annotation
.end method
