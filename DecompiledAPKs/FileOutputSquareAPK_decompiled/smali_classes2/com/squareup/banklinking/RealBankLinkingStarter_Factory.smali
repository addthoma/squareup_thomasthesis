.class public final Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;
.super Ljava/lang/Object;
.source "RealBankLinkingStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/banklinking/RealBankLinkingStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingType;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)",
            "Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;"
        }
    .end annotation

    .line 50
    new-instance v7, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/banklinking/RealBankLinkingStarter;
    .locals 8

    .line 55
    new-instance v7, Lcom/squareup/banklinking/RealBankLinkingStarter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/banklinking/RealBankLinkingStarter;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;Lcom/squareup/util/BrowserLauncher;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/banklinking/RealBankLinkingStarter;
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/onboarding/OnboardingType;

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/onboarding/OnboardingStarter;

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/BrowserLauncher;

    invoke-static/range {v1 .. v6}, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/util/Res;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/banklinking/RealBankLinkingStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/banklinking/RealBankLinkingStarter_Factory;->get()Lcom/squareup/banklinking/RealBankLinkingStarter;

    move-result-object v0

    return-object v0
.end method
