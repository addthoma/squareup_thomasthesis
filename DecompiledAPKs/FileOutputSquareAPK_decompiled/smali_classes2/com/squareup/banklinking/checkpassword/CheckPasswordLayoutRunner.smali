.class public final Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;
.super Ljava/lang/Object;
.source "CheckPasswordLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Factory;,
        Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Binding;,
        Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckPasswordLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckPasswordLayoutRunner.kt\ncom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner\n*L\n1#1,90:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u0000 \u001d2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001c\u001d\u001eB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0008\u0010\u001b\u001a\u00020\u0013H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;",
        "view",
        "Landroid/view/View;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Landroid/view/View;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "Landroid/widget/Button;",
        "password",
        "",
        "getPassword",
        "()Ljava/lang/String;",
        "passwordField",
        "Landroid/widget/EditText;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "showSpinner",
        "",
        "updatePrimaryButton",
        "Binding",
        "Companion",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Companion;

.field private static final PASSWORD_MINIMUM_LENGTH:I = 0x8


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final continueButton:Landroid/widget/Button;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final passwordField:Landroid/widget/EditText;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->Companion:Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 35
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object p2, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 36
    iget-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->password_field:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->passwordField:Landroid/widget/EditText;

    .line 37
    iget-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/banklinking/impl/R$id;->continue_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->continueButton:Landroid/widget/Button;

    .line 42
    iget-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    new-instance p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$1;

    invoke-direct {p2, p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$1;-><init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->updatePrimaryButton()V

    .line 46
    iget-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->passwordField:Landroid/widget/EditText;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setSaveEnabled(Z)V

    .line 47
    iget-object p1, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->passwordField:Landroid/widget/EditText;

    new-instance p2, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$2;

    invoke-direct {p2, p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$2;-><init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;)V

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public static final synthetic access$getGlassSpinner$p(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;)Lcom/squareup/register/widgets/GlassSpinner;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-object p0
.end method

.method public static final synthetic access$getPassword$p(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;)Ljava/lang/String;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->getPassword()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    return-object p0
.end method

.method public static final synthetic access$updatePrimaryButton(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->updatePrimaryButton()V

    return-void
.end method

.method private final getPassword()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->passwordField:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final spinnerData(Z)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 70
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final updatePrimaryButton()V
    .locals 3

    .line 74
    invoke-direct {p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->getPassword()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->getPassword()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x8

    if-lt v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 75
    :goto_0
    iget-object v0, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->continueButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object p2, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v0, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;->getShowSpinner()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->spinnerData(Z)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->setSpinnerState(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V

    .line 60
    iget-object p2, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$showRendering$1;-><init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 64
    iget-object p2, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 61
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 62
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 63
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$showRendering$2;

    invoke-direct {v2, p1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$showRendering$2;-><init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 66
    iget-object p2, p0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->continueButton:Landroid/widget/Button;

    new-instance v0, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$showRendering$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner$showRendering$3;-><init>(Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/banklinking/checkpassword/CheckPasswordLayoutRunner;->showRendering(Lcom/squareup/banklinking/checkpassword/CheckPasswordScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
