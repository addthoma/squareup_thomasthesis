.class final Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;
.super Ljava/lang/Object;
.source "RealBankLinkingStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/RealBankLinkingStarter;->bankLinkingVariant(Lcom/squareup/settings/server/AccountStatusSettings;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lcom/squareup/banklinking/BankLinkingStarter$Variant;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_bankLinkingVariant:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;->$this_bankLinkingVariant:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lcom/squareup/banklinking/BankLinkingStarter$Variant;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;->$this_bankLinkingVariant:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->showBankLinkingAfterActivation()Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->NONE:Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    goto :goto_0

    .line 80
    :cond_0
    iget-object p1, p0, Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;->$this_bankLinkingVariant:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OnboardingSettings;->hasInAppBankLinking()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->NATIVE:Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    goto :goto_0

    .line 81
    :cond_1
    sget-object p1, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->WEB:Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/RealBankLinkingStarter$bankLinkingVariant$1;->apply(Lkotlin/Unit;)Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    move-result-object p1

    return-object p1
.end method
