.class public final Lcom/squareup/banklinking/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_holder_field:I = 0x7f0a012a

.field public static final account_number:I = 0x7f0a012d

.field public static final account_number_field:I = 0x7f0a012e

.field public static final account_type:I = 0x7f0a012f

.field public static final account_type_field:I = 0x7f0a0130

.field public static final address:I = 0x7f0a0178

.field public static final business_checking:I = 0x7f0a026e

.field public static final check_bank_account_info_view:I = 0x7f0a0310

.field public static final check_password_view:I = 0x7f0a0312

.field public static final checking:I = 0x7f0a031a

.field public static final continue_button:I = 0x7f0a03a9

.field public static final direct_debit_guarantee:I = 0x7f0a05bf

.field public static final direct_debit_logo:I = 0x7f0a05c0

.field public static final email:I = 0x7f0a06b2

.field public static final failure_view:I = 0x7f0a0741

.field public static final fetch_bank_account_error:I = 0x7f0a074b

.field public static final fetch_bank_account_spinner:I = 0x7f0a074c

.field public static final fine_print:I = 0x7f0a0756

.field public static final holder_name:I = 0x7f0a07de

.field public static final loading_view:I = 0x7f0a0958

.field public static final name:I = 0x7f0a0a0c

.field public static final organization:I = 0x7f0a0b6f

.field public static final password_field:I = 0x7f0a0bd3

.field public static final primary_institution_number:I = 0x7f0a0c5e

.field public static final primary_institution_number_field:I = 0x7f0a0c5f

.field public static final reference:I = 0x7f0a0d33

.field public static final savings:I = 0x7f0a0e07

.field public static final secondary_institution_number:I = 0x7f0a0e31

.field public static final secondary_institution_number_field:I = 0x7f0a0e32

.field public static final show_result_view:I = 0x7f0a0e81

.field public static final sort_code:I = 0x7f0a0eb7

.field public static final submit_button:I = 0x7f0a0f47

.field public static final success_view:I = 0x7f0a0f4d


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
