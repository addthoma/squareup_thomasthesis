.class final Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$1;
.super Ljava/lang/Object;
.source "BankFieldsCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/widgets/BankFieldsCoordinator;->fieldChanged(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $fieldType:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$1;->$fieldType:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$1;->call(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;)Z
    .locals 1

    .line 80
    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;->getField()Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$fieldChanged$1;->$fieldType:Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
