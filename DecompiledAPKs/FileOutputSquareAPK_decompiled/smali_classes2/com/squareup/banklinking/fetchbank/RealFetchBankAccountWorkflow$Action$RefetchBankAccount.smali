.class public final Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;
.super Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;
.source "RealFetchBankAccountWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RefetchBankAccount"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    new-instance v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;

    invoke-direct {v0}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;->INSTANCE:Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0, v0}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
