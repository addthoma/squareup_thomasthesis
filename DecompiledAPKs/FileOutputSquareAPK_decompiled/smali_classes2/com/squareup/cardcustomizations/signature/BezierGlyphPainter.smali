.class public final Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;
.super Ljava/lang/Object;
.source "BezierGlyphPainter.java"

# interfaces
.implements Lcom/squareup/cardcustomizations/signature/GlyphPainter;


# instance fields
.field private boundingBox:Landroid/graphics/RectF;

.field private final canvas:Landroid/graphics/Canvas;

.field private final paint:Landroid/graphics/Paint;

.field private final points:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation
.end field

.field private final spliner:Lcom/squareup/cardcustomizations/signature/Spliner;


# direct methods
.method public constructor <init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-direct {v0}, Lcom/squareup/cardcustomizations/signature/Spliner;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    .line 27
    iput-object p2, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public addPoint(Lcom/squareup/cardcustomizations/signature/Point$Timestamped;)V
    .locals 7

    .line 39
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_1

    .line 40
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;

    .line 42
    iget v2, v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget v3, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    iget v3, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    return-void

    .line 47
    :cond_0
    iget-wide v2, v0, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->time:J

    iget-wide v4, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->time:J

    cmp-long v6, v2, v4

    if-nez v6, :cond_2

    return-void

    :cond_1
    const/4 v0, 0x0

    .line 52
    :cond_2
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v2, p1}, Lcom/squareup/cardcustomizations/signature/Spliner;->addPoint(Lcom/squareup/cardcustomizations/signature/Point;)V

    if-nez v0, :cond_3

    .line 56
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    iget v2, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget v3, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    iget v4, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget p1, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    invoke-direct {v1, v2, v3, v4, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    return-void

    .line 60
    :cond_3
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Spliner;->getBeziers()Ljava/util/List;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 62
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    iget v2, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget v3, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    iget v4, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->x:F

    iget p1, p1, Lcom/squareup/cardcustomizations/signature/Point$Timestamped;->y:F

    invoke-direct {v1, v2, v3, v4, p1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {v0, v1}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    return-void

    .line 68
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    if-le p1, v1, :cond_5

    .line 69
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x2

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    .line 71
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->bounds()Landroid/graphics/RectF;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    :cond_5
    return-void
.end method

.method public boundingBox()Landroid/graphics/RectF;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public finish()V
    .locals 5

    .line 101
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Spliner;->getBeziers()Ljava/util/List;

    move-result-object v0

    .line 105
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point;

    .line 107
    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    iget v3, v1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    iget v1, v1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    iget-object v4, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    .line 111
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 112
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;

    .line 113
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    .line 114
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;->bounds()Landroid/graphics/RectF;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/cardcustomizations/signature/Rects;->unionWith(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->boundingBox:Landroid/graphics/RectF;

    .line 115
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v1, v0}, Lcom/squareup/cardcustomizations/signature/Spliner;->expandDirtyRect(Lcom/squareup/cardcustomizations/signature/Spliner$Bezier;)V

    :cond_1
    return-void
.end method

.method public getPointCount()I
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public invalidate(Landroid/view/View;)V
    .locals 6

    .line 77
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    neg-int v0, v0

    .line 82
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/signature/Spliner;->getBeziers()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/signature/Point;

    .line 84
    new-instance v2, Landroid/graphics/Rect;

    iget v3, v1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    float-to-int v3, v3

    iget v4, v1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    float-to-int v4, v4

    iget v5, v1, Lcom/squareup/cardcustomizations/signature/Point;->x:F

    float-to-int v5, v5

    iget v1, v1, Lcom/squareup/cardcustomizations/signature/Point;->y:F

    float-to-int v1, v1

    invoke-direct {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 85
    invoke-virtual {v2, v0, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 86
    invoke-virtual {p1, v2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/signature/Spliner;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 91
    iget-object v1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {v1}, Lcom/squareup/cardcustomizations/signature/Spliner;->getDirtyRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 93
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 95
    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->spliner:Lcom/squareup/cardcustomizations/signature/Spliner;

    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/signature/Spliner;->resetDirty()V

    :cond_2
    return-void
.end method

.method public points()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/signature/Point$Timestamped;",
            ">;"
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/cardcustomizations/signature/BezierGlyphPainter;->points:Ljava/util/List;

    return-object v0
.end method
