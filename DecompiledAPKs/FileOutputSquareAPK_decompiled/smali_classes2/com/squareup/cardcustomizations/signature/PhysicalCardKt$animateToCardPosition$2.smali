.class public final Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PhysicalCard.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardcustomizations/signature/PhysicalCardKt;->animateToCardPosition(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2",
        "Landroid/animation/AnimatorListenerAdapter;",
        "onAnimationEnd",
        "",
        "animation",
        "Landroid/animation/Animator;",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# instance fields
.field final synthetic $this_animateToCardPosition:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;->$this_animateToCardPosition:Landroid/view/View;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 105
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;->$this_animateToCardPosition:Landroid/view/View;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 106
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;->$this_animateToCardPosition:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 107
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;->$this_animateToCardPosition:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 108
    iget-object p1, p0, Lcom/squareup/cardcustomizations/signature/PhysicalCardKt$animateToCardPosition$2;->$this_animateToCardPosition:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method
