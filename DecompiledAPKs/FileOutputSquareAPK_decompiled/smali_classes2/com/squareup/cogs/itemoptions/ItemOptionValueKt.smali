.class public final Lcom/squareup/cogs/itemoptions/ItemOptionValueKt;
.super Ljava/lang/Object;
.source "ItemOptionValue.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toItemOptionValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;",
        "cogs_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toItemOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 4

    const-string v0, "$this$toItemOptionValue"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getItemOptionID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "this.itemOptionID"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getUid()Ljava/lang/String;

    move-result-object v2

    const-string v3, "this.uid"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;->getName()Ljava/lang/String;

    move-result-object p0

    const-string v3, "this.name"

    invoke-static {p0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
