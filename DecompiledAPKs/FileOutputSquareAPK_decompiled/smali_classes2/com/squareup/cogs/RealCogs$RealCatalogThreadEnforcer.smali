.class Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;
.super Ljava/lang/Object;
.source "RealCogs.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogThreadsEnforcer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cogs/RealCogs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RealCatalogThreadEnforcer"
.end annotation


# instance fields
.field private final fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;


# direct methods
.method private constructor <init>(Lcom/squareup/FileThreadEnforcer;)V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/FileThreadEnforcer;Lcom/squareup/cogs/RealCogs$1;)V
    .locals 0

    .line 132
    invoke-direct {p0, p1}, Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;-><init>(Lcom/squareup/FileThreadEnforcer;)V

    return-void
.end method


# virtual methods
.method public enforceFileThread(Ljava/lang/String;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/cogs/RealCogs$RealCatalogThreadEnforcer;->fileThreadEnforcer:Lcom/squareup/FileThreadEnforcer;

    invoke-virtual {v0, p1}, Lcom/squareup/FileThreadEnforcer;->enforceOnFileThread(Ljava/lang/String;)V

    return-void
.end method

.method public enforceMainThread()V
    .locals 1

    const-string v0, "Cannot interact with catalog from outside of main thread."

    .line 140
    invoke-static {v0}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread(Ljava/lang/String;)V

    return-void
.end method
