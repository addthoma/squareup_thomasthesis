.class public abstract Lcom/squareup/cogs/CogsServiceReleaseModule;
.super Ljava/lang/Object;
.source "CogsServiceReleaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cogs/CogsServiceCommonModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindCogsService(Lcom/squareup/cogs/CogsService;)Lcom/squareup/cogs/CogsService;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
