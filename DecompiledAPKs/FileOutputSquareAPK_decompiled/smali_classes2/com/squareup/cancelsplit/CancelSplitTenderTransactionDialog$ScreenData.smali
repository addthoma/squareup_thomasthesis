.class public final Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;
.super Ljava/lang/Object;
.source "CancelSplitTenderTransactionDialogScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\t\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\tR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
        "",
        "positiveButton",
        "",
        "negativeButton",
        "title",
        "message",
        "amountPaid",
        "",
        "(IIIILjava/lang/String;)V",
        "getAmountPaid",
        "()Ljava/lang/String;",
        "getMessage",
        "()I",
        "getNegativeButton",
        "getPositiveButton",
        "getTitle",
        "cancelsplit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountPaid:Ljava/lang/String;

.field private final message:I

.field private final negativeButton:I

.field private final positiveButton:I

.field private final title:I


# direct methods
.method public constructor <init>(IIIILjava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->positiveButton:I

    iput p2, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->negativeButton:I

    iput p3, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->title:I

    iput p4, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->message:I

    iput-object p5, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->amountPaid:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getAmountPaid()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->amountPaid:Ljava/lang/String;

    return-object v0
.end method

.method public final getMessage()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->message:I

    return v0
.end method

.method public final getNegativeButton()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->negativeButton:I

    return v0
.end method

.method public final getPositiveButton()I
    .locals 1

    .line 19
    iget v0, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->positiveButton:I

    return v0
.end method

.method public final getTitle()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;->title:I

    return v0
.end method
