.class public final Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;
.super Ljava/lang/Object;
.source "UseFeatureFlagCatalogIntegrationController.kt"

# interfaces
.implements Lcom/squareup/catalogapi/CatalogIntegrationController;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0006H\u0016J\u0008\u0010\u0008\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;",
        "Lcom/squareup/catalogapi/CatalogIntegrationController;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "hasExtraServiceOptions",
        "",
        "shouldShowNewFeature",
        "shouldShowPartiallyRolledOutFeature",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public hasExtraServiceOptions()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public shouldShowNewFeature()Z
    .locals 2

    .line 20
    iget-object v0, p0, Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_SERVICES_CATALOG_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public shouldShowPartiallyRolledOutFeature()Z
    .locals 2

    .line 16
    iget-object v0, p0, Lcom/squareup/catalogapi/UseFeatureFlagCatalogIntegrationController;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ENABLE_SERVICES_CATALOG_INTEGRATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
