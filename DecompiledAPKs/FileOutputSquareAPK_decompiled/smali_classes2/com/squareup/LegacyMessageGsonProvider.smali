.class public Lcom/squareup/LegacyMessageGsonProvider;
.super Ljava/lang/Object;
.source "LegacyMessageGsonProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/LegacyMessageGsonProvider$Holder;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/gson/GsonBuilder;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/LegacyMessageGsonProvider;->gsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static gson()Lcom/google/gson/Gson;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/LegacyMessageGsonProvider$Holder;->gson:Lcom/google/gson/Gson;

    return-object v0
.end method

.method private static gsonBuilder()Lcom/google/gson/GsonBuilder;
    .locals 3

    .line 26
    invoke-static {}, Lcom/squareup/gson/GsonProvider;->gsonBuilder()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    new-instance v1, Lcom/squareup/wire/WireTypeAdapterFactory;

    invoke-direct {v1}, Lcom/squareup/wire/WireTypeAdapterFactory;-><init>()V

    .line 27
    invoke-virtual {v0, v1}, Lcom/google/gson/GsonBuilder;->registerTypeAdapterFactory(Lcom/google/gson/TypeAdapterFactory;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lokio/ByteString;

    new-instance v2, Lcom/squareup/payment/offline/StoreAndForwardPaymentService$ByteStringAdapter;

    invoke-direct {v2}, Lcom/squareup/payment/offline/StoreAndForwardPaymentService$ByteStringAdapter;-><init>()V

    .line 29
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/squareup/protos/client/bills/DiscountLineItem;

    new-instance v2, Lcom/squareup/payment/DiscountLineItemJsonAdapter;

    invoke-direct {v2}, Lcom/squareup/payment/DiscountLineItemJsonAdapter;-><init>()V

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v0

    return-object v0
.end method
