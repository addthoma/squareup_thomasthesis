.class public final enum Lcom/squareup/Currency;
.super Ljava/lang/Enum;
.source "Currency.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/Currency;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/Currency;

.field public static final enum CAD:Lcom/squareup/Currency;

.field public static final enum JPY:Lcom/squareup/Currency;

.field public static final enum USD:Lcom/squareup/Currency;

.field public static final enum XXXX:Lcom/squareup/Currency;


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 11
    new-instance v0, Lcom/squareup/Currency;

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->XXX:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v2, 0x0

    const-string v3, "XXXX"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/Currency;-><init>(Ljava/lang/String;ILcom/squareup/protos/common/CurrencyCode;)V

    sput-object v0, Lcom/squareup/Currency;->XXXX:Lcom/squareup/Currency;

    .line 14
    new-instance v0, Lcom/squareup/Currency;

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v3, 0x1

    const-string v4, "USD"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/Currency;-><init>(Ljava/lang/String;ILcom/squareup/protos/common/CurrencyCode;)V

    sput-object v0, Lcom/squareup/Currency;->USD:Lcom/squareup/Currency;

    .line 17
    new-instance v0, Lcom/squareup/Currency;

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->CAD:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v4, 0x2

    const-string v5, "CAD"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/Currency;-><init>(Ljava/lang/String;ILcom/squareup/protos/common/CurrencyCode;)V

    sput-object v0, Lcom/squareup/Currency;->CAD:Lcom/squareup/Currency;

    .line 19
    new-instance v0, Lcom/squareup/Currency;

    sget-object v1, Lcom/squareup/protos/common/CurrencyCode;->JPY:Lcom/squareup/protos/common/CurrencyCode;

    const/4 v5, 0x3

    const-string v6, "JPY"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/Currency;-><init>(Ljava/lang/String;ILcom/squareup/protos/common/CurrencyCode;)V

    sput-object v0, Lcom/squareup/Currency;->JPY:Lcom/squareup/Currency;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/Currency;

    .line 9
    sget-object v1, Lcom/squareup/Currency;->XXXX:Lcom/squareup/Currency;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Currency;->USD:Lcom/squareup/Currency;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/Currency;->CAD:Lcom/squareup/Currency;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/Currency;->JPY:Lcom/squareup/Currency;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/Currency;->$VALUES:[Lcom/squareup/Currency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput-object p3, p0, Lcom/squareup/Currency;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/Currency;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/Currency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/Currency;

    return-object p0
.end method

.method public static values()[Lcom/squareup/Currency;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/Currency;->$VALUES:[Lcom/squareup/Currency;

    invoke-virtual {v0}, [Lcom/squareup/Currency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/Currency;

    return-object v0
.end method


# virtual methods
.method public toProto()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/Currency;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method
