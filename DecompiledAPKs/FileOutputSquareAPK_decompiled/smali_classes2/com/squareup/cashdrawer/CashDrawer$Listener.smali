.class public interface abstract Lcom/squareup/cashdrawer/CashDrawer$Listener;
.super Ljava/lang/Object;
.source "CashDrawer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashdrawer/CashDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract drawerAvailable()V
.end method

.method public abstract drawerUnavailable(Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
.end method
