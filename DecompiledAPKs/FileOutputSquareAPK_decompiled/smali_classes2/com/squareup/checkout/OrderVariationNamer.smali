.class public interface abstract Lcom/squareup/checkout/OrderVariationNamer;
.super Ljava/lang/Object;
.source "OrderVariationNamer.java"


# virtual methods
.method public abstract fromItemVariation(Lcom/squareup/util/Res;Lcom/squareup/api/items/ItemVariation;)Ljava/lang/String;
.end method

.method public abstract fromItemVariationDetails(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;)Ljava/lang/String;
.end method
