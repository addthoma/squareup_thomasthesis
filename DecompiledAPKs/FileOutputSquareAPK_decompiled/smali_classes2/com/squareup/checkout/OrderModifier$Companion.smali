.class public final Lcom/squareup/checkout/OrderModifier$Companion;
.super Ljava/lang/Object;
.source "OrderModifier.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/OrderModifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0002J0\u0010\u000e\u001a\n \u000b*\u0004\u0018\u00010\u000f0\u000f2\u0006\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u000e\u0008\u0002\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0008H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J*\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001fH\u0007J \u0010!\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u0010\u0010!\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J&\u0010&\u001a\n \u000b*\u0004\u0018\u00010#0#*\u00020#2\u0008\u0010\'\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/checkout/OrderModifier$Companion;",
        "",
        "()V",
        "CONVERSATIONAL_MODE_TO_RES_ID",
        "",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode$Type;",
        "",
        "CONVERSATIONAL_MODIFIER_FORMATS",
        "",
        "amounts",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;",
        "kotlin.jvm.PlatformType",
        "price",
        "Lcom/squareup/protos/common/Money;",
        "featureDetails",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;",
        "modifierQuantity",
        "itemizationQuantity",
        "Ljava/math/BigDecimal;",
        "modes",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
        "fromItemizationHistory",
        "Lcom/squareup/checkout/OrderModifier;",
        "modifierOptionLineItem",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem;",
        "fromOverride",
        "catalogModifier",
        "Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;",
        "optionOverride",
        "Lcom/squareup/api/items/ItemModifierOptionOverride;",
        "isModifierSelectionOverrideApplied",
        "",
        "hideFromCustomer",
        "fromTicketCart",
        "itemModifierOption",
        "Lcom/squareup/api/items/ItemModifierOption;",
        "modifierList",
        "Lcom/squareup/api/items/ItemModifierList;",
        "applyOverride",
        "override",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 314
    invoke-direct {p0}, Lcom/squareup/checkout/OrderModifier$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$amounts(Lcom/squareup/checkout/OrderModifier$Companion;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;
    .locals 0

    .line 314
    invoke-direct {p0, p1}, Lcom/squareup/checkout/OrderModifier$Companion;->amounts(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object p0

    return-object p0
.end method

.method private final amounts(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;
    .locals 1

    .line 382
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;-><init>()V

    .line 383
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    move-result-object v0

    .line 384
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->modifier_option_times_quantity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;

    move-result-object p1

    .line 385
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object p1

    return-object p1
.end method

.method private final applyOverride(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierOptionOverride;Z)Lcom/squareup/api/items/ItemModifierOption;
    .locals 1

    .line 404
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierOption;->newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 406
    iget-object p2, p2, Lcom/squareup/api/items/ItemModifierOptionOverride;->on_by_default:Ljava/lang/Boolean;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    if-eqz p3, :cond_1

    .line 407
    sget-object p2, Lcom/squareup/api/items/ItemModifierOption;->DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

    goto :goto_0

    .line 408
    :cond_1
    iget-object p2, p1, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    .line 405
    :goto_0
    invoke-virtual {v0, p2}, Lcom/squareup/api/items/ItemModifierOption$Builder;->on_by_default(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p1

    .line 410
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->build()Lcom/squareup/api/items/ItemModifierOption;

    move-result-object p1

    return-object p1
.end method

.method private final featureDetails(ILjava/math/BigDecimal;Ljava/util/List;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/math/BigDecimal;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;)",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;"
        }
    .end annotation

    .line 390
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;-><init>()V

    .line 391
    invoke-virtual {v0, p3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode(Ljava/util/List;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object p3

    .line 392
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;-><init>()V

    .line 393
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v0

    .line 395
    invoke-static {p1, p2}, Lcom/squareup/quantity/SharedCalculationsKt;->modifierQuantityTimesItemizationQuantity(ILjava/math/BigDecimal;)Ljava/lang/String;

    move-result-object p1

    .line 394
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity_times_itemization_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object p1

    .line 399
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    move-result-object p1

    .line 392
    invoke-virtual {p3, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object p1

    .line 400
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method static synthetic featureDetails$default(Lcom/squareup/checkout/OrderModifier$Companion;ILjava/math/BigDecimal;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 389
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkout/OrderModifier$Companion;->featureDetails(ILjava/math/BigDecimal;Ljava/util/List;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final fromItemizationHistory(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "modifierOptionLineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    new-instance v0, Lcom/squareup/checkout/OrderModifier;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final fromOverride(Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;Lcom/squareup/api/items/ItemModifierOptionOverride;ZZ)Lcom/squareup/checkout/OrderModifier;
    .locals 10
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "catalogModifier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;-><init>()V

    .line 369
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;

    move-result-object v0

    .line 370
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getOrdinal()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->sort_order(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;

    move-result-object v0

    .line 371
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;

    move-result-object v3

    .line 373
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;-><init>()V

    .line 374
    move-object v4, p0

    check-cast v4, Lcom/squareup/checkout/OrderModifier$Companion;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object v1

    const-string v2, "catalogModifier.`object`()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/api/items/ItemModifierOption;

    invoke-direct {v4, v1, p2, p3}, Lcom/squareup/checkout/OrderModifier$Companion;->applyOverride(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierOptionOverride;Z)Lcom/squareup/api/items/ItemModifierOption;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    move-result-object p2

    .line 376
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object p2

    .line 378
    new-instance p3, Lcom/squareup/checkout/OrderModifier;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 379
    sget-object v6, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    const-string p1, "ONE"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Lcom/squareup/checkout/OrderModifier$Companion;->featureDetails$default(Lcom/squareup/checkout/OrderModifier$Companion;ILjava/math/BigDecimal;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object v6

    const-string p1, "featureDetails(1, ONE)"

    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p3

    move-object v4, p2

    move v5, p4

    .line 378
    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$DisplayDetails;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;ZLcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p3
.end method

.method public final fromTicketCart(Lcom/squareup/api/items/ItemModifierOption;Lcom/squareup/api/items/ItemModifierList;Ljava/math/BigDecimal;)Lcom/squareup/checkout/OrderModifier;
    .locals 9
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "itemModifierOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modifierList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemizationQuantity"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    iget-object v0, p1, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 344
    move-object v0, p0

    check-cast v0, Lcom/squareup/checkout/OrderModifier$Companion;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    invoke-static {v2}, Lcom/squareup/shared/catalog/utils/Dineros;->toMoney(Lcom/squareup/protos/common/dinero/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    const-string v3, "Dineros.toMoney(itemModifierOption.price)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/squareup/checkout/OrderModifier$Companion;->amounts(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 349
    :goto_0
    new-instance v2, Lcom/squareup/checkout/OrderModifier;

    new-instance v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;-><init>()V

    .line 350
    new-instance v4, Lcom/squareup/protos/client/IdPair;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->modifier_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object v3

    .line 351
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Amounts;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object v0

    .line 352
    new-instance v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;-><init>()V

    .line 353
    invoke-virtual {v3, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->backing_modifier_option(Lcom/squareup/api/items/ItemModifierOption;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    move-result-object p1

    .line 354
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->modifier_list(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;

    move-result-object p1

    .line 355
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;

    move-result-object p1

    .line 352
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 356
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->hide_from_customer(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 357
    move-object v3, p0

    check-cast v3, Lcom/squareup/checkout/OrderModifier$Companion;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v5, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/checkout/OrderModifier$Companion;->featureDetails$default(Lcom/squareup/checkout/OrderModifier$Companion;ILjava/math/BigDecimal;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->feature_details(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;

    move-result-object p1

    .line 358
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem;

    move-result-object p1

    const-string p2, "ModifierOptionLineItem.B\u2026tity))\n          .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 349
    invoke-direct {v2, p1, v1}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v2
.end method

.method public final fromTicketCart(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;)Lcom/squareup/checkout/OrderModifier;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "modifierOptionLineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 336
    new-instance v0, Lcom/squareup/checkout/OrderModifier;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkout/OrderModifier;-><init>(Lcom/squareup/protos/client/bills/ModifierOptionLineItem;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
