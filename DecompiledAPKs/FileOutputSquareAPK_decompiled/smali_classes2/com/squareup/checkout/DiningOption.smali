.class public Lcom/squareup/checkout/DiningOption;
.super Lcom/squareup/itemsorter/SortableDiningOption;
.source "DiningOption.java"


# instance fields
.field private final applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

.field private final id:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final ordinal:I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Lcom/squareup/itemsorter/SortableDiningOption;-><init>()V

    const-string v0, "id"

    .line 65
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "name"

    .line 66
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 68
    iput-object p1, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    .line 70
    iput p3, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    .line 71
    iput-object p4, p0, Lcom/squareup/checkout/DiningOption;->applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-void
.end method

.method public static diningOptionsFrom(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiningOption;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 29
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogDiningOption;

    .line 30
    invoke-static {v1}, Lcom/squareup/checkout/DiningOption;->of(Lcom/squareup/shared/catalog/models/CatalogDiningOption;)Lcom/squareup/checkout/DiningOption;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 32
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    return-object v0
.end method

.method public static forTesting(Ljava/lang/String;I)Lcom/squareup/checkout/DiningOption;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/checkout/DiningOption;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, p1, v1}, Lcom/squareup/checkout/DiningOption;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;)V

    return-object v0
.end method

.method public static of(Lcom/squareup/orders/model/Order$Fulfillment;Ljava/lang/String;)Lcom/squareup/checkout/DiningOption;
    .locals 3

    const-string v0, "fulfillment"

    .line 51
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 52
    new-instance v0, Lcom/squareup/checkout/DiningOption;

    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/squareup/checkout/DiningOption;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;)V

    return-object v0
.end method

.method public static of(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/checkout/DiningOption;
    .locals 4

    const-string v0, "diningOptionLineItem"

    .line 43
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->backing_details:Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;->dining_option:Lcom/squareup/api/items/DiningOption;

    .line 46
    new-instance v1, Lcom/squareup/checkout/DiningOption;

    iget-object v2, v0, Lcom/squareup/api/items/DiningOption;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/api/items/DiningOption;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/api/items/DiningOption;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->application_scope:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-direct {v1, v2, v3, v0, p0}, Lcom/squareup/checkout/DiningOption;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;)V

    return-object v1
.end method

.method public static of(Lcom/squareup/shared/catalog/models/CatalogDiningOption;)Lcom/squareup/checkout/DiningOption;
    .locals 4

    const-string v0, "catalogDiningOption"

    .line 37
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 38
    new-instance v0, Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getName()Ljava/lang/String;

    move-result-object v2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/shared/catalog/models/CatalogDiningOption;->getOrdinal()I

    move-result p0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/squareup/checkout/DiningOption;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;)V

    return-object v0
.end method


# virtual methods
.method public buildDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .locals 4

    .line 102
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    const-string v1, "DiningOptionApplicationScope"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 104
    new-instance v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;-><init>()V

    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 106
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    .line 107
    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->dining_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;-><init>()V

    new-instance v2, Lcom/squareup/api/items/DiningOption$Builder;

    invoke-direct {v2}, Lcom/squareup/api/items/DiningOption$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    .line 109
    invoke-virtual {v2, v3}, Lcom/squareup/api/items/DiningOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    .line 110
    invoke-virtual {v2, v3}, Lcom/squareup/api/items/DiningOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object v2

    iget v3, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    .line 111
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/api/items/DiningOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object v2

    .line 112
    invoke-virtual {v2}, Lcom/squareup/api/items/DiningOption$Builder;->build()Lcom/squareup/api/items/DiningOption;

    move-result-object v2

    .line 109
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->dining_option(Lcom/squareup/api/items/DiningOption;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    move-result-object v1

    .line 108
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->backing_details(Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkout/DiningOption;->applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 114
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v0

    return-object v0
.end method

.method public buildDiningOptionLineItemForTest(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    const-string v1, "DiningOptionApplicationScope"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 124
    new-instance v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;-><init>()V

    .line 125
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->dining_option_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;-><init>()V

    new-instance v1, Lcom/squareup/api/items/DiningOption$Builder;

    invoke-direct {v1}, Lcom/squareup/api/items/DiningOption$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    .line 127
    invoke-virtual {v1, v2}, Lcom/squareup/api/items/DiningOption$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    .line 128
    invoke-virtual {v1, v2}, Lcom/squareup/api/items/DiningOption$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    .line 129
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/api/items/DiningOption$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/DiningOption$Builder;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Lcom/squareup/api/items/DiningOption$Builder;->build()Lcom/squareup/api/items/DiningOption;

    move-result-object v1

    .line 127
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->dining_option(Lcom/squareup/api/items/DiningOption;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;

    move-result-object v0

    .line 126
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->backing_details(Lcom/squareup/protos/client/bills/DiningOptionLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 132
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->application_scope(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem$Builder;->build()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object p1

    return-object p1
.end method

.method public copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;
    .locals 4

    .line 95
    new-instance v0, Lcom/squareup/checkout/DiningOption;

    iget-object v1, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    iget v3, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/checkout/DiningOption;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/protos/client/bills/ApplicationScope;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 137
    instance-of v0, p1, Lcom/squareup/checkout/DiningOption;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 140
    :cond_0
    check-cast p1, Lcom/squareup/checkout/DiningOption;

    .line 141
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    iget p1, p1, Lcom/squareup/checkout/DiningOption;->ordinal:I

    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getApplicationScope()Lcom/squareup/protos/client/bills/ApplicationScope;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->applicationScope:Lcom/squareup/protos/client/bills/ApplicationScope;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 145
    iget-object v0, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 146
    iget-object v2, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 147
    iget v1, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 152
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderDiningOption{id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkout/DiningOption;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", name=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/checkout/DiningOption;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/checkout/DiningOption;->ordinal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
