.class public final Lcom/squareup/checkout/Surcharge$CustomSurcharge;
.super Lcom/squareup/checkout/Surcharge;
.source "Surcharge.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Surcharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomSurcharge"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSurcharge.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Surcharge.kt\ncom/squareup/checkout/Surcharge$CustomSurcharge\n*L\n1#1,201:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/checkout/Surcharge$CustomSurcharge;",
        "Lcom/squareup/checkout/Surcharge;",
        "surchargeLineItem",
        "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
        "disabledFees",
        "",
        "",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        "(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "surchargeLineItem"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disabledFees"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/checkout/Surcharge;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 29
    new-instance p2, Ljava/util/LinkedHashMap;

    invoke-direct {p2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p2, Ljava/util/Map;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkout/Surcharge$CustomSurcharge;-><init>(Lcom/squareup/protos/client/bills/SurchargeLineItem;Ljava/util/Map;)V

    return-void
.end method
