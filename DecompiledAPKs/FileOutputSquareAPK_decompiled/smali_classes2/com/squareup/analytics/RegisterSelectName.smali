.class public final enum Lcom/squareup/analytics/RegisterSelectName;
.super Ljava/lang/Enum;
.source "RegisterSelectName.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/RegisterSelectName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_BANK_ACCOUNT_HOLDER:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_BANK_ACCOUNT_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_BANK_ROUTING_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_BUSINESS_NAME:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_CONFIRM_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_COUNTRY_PICKER:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_EIN:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_FIRST_NAME:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_LAST_NAME:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_LAST_SSN:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_PASSWORD:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_PHONE_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_POSTAL:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_STREET:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_VERTICAL_RETAIL:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ONBOARDING_VERTICAL_SPOS:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ORDER_CONTACTLESS_ADDRESS_CHANGED:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum ORDER_CONTACTLESS_PAYMENT_INPUT:Lcom/squareup/analytics/RegisterSelectName;

.field public static final enum PAYMENT_FLOW_METHODS_CASH_FIELD:Lcom/squareup/analytics/RegisterSelectName;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v1, 0x0

    const-string v2, "ONBOARDING_BANK_ACCOUNT_HOLDER"

    const-string v3, "Onboarding: Account holder in bank page"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BANK_ACCOUNT_HOLDER:Lcom/squareup/analytics/RegisterSelectName;

    .line 10
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v2, 0x1

    const-string v3, "ONBOARDING_BANK_ACCOUNT_NUMBER"

    const-string v4, "Onboarding: Account number in bank page"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BANK_ACCOUNT_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    .line 11
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v3, 0x2

    const-string v4, "ONBOARDING_BANK_ROUTING_NUMBER"

    const-string v5, "Onboarding: Routing number in bank page"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BANK_ROUTING_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    .line 12
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v4, 0x3

    const-string v5, "ONBOARDING_BUSINESS_NAME"

    const-string v6, "Onboarding: Business name"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BUSINESS_NAME:Lcom/squareup/analytics/RegisterSelectName;

    .line 13
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v5, 0x4

    const-string v6, "ONBOARDING_CONFIRM_EMAIL"

    const-string v7, "Onboarding: Confirm email address in create account page"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_CONFIRM_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

    .line 14
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v6, 0x5

    const-string v7, "ONBOARDING_COUNTRY_PICKER"

    const-string v8, "Onboarding: Country picker in create account page"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_COUNTRY_PICKER:Lcom/squareup/analytics/RegisterSelectName;

    .line 15
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v7, 0x6

    const-string v8, "ONBOARDING_EIN"

    const-string v9, "Onboarding: Ein in personal info"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_EIN:Lcom/squareup/analytics/RegisterSelectName;

    .line 16
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/4 v8, 0x7

    const-string v9, "ONBOARDING_EMAIL"

    const-string v10, "Onboarding: Email address in create account page"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

    .line 17
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v9, 0x8

    const-string v10, "ONBOARDING_FIRST_NAME"

    const-string v11, "Onboarding: First name in personal info"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_FIRST_NAME:Lcom/squareup/analytics/RegisterSelectName;

    .line 18
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v10, 0x9

    const-string v11, "ONBOARDING_LAST_NAME"

    const-string v12, "Onboarding: Last name in personal info"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_LAST_NAME:Lcom/squareup/analytics/RegisterSelectName;

    .line 19
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v11, 0xa

    const-string v12, "ONBOARDING_LAST_SSN"

    const-string v13, "Onboarding: Last four digits of SSN in personal info"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_LAST_SSN:Lcom/squareup/analytics/RegisterSelectName;

    .line 20
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v12, 0xb

    const-string v13, "ONBOARDING_PASSWORD"

    const-string v14, "Onboarding: Password in create account page"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_PASSWORD:Lcom/squareup/analytics/RegisterSelectName;

    .line 21
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v13, 0xc

    const-string v14, "ONBOARDING_PHONE_NUMBER"

    const-string v15, "Onboarding: Phone number in personal info"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_PHONE_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    .line 22
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v14, 0xd

    const-string v15, "ONBOARDING_POSTAL"

    const-string v13, "Onboarding: Postal code in personal info"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_POSTAL:Lcom/squareup/analytics/RegisterSelectName;

    .line 23
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const/16 v13, 0xe

    const-string v15, "ONBOARDING_STREET"

    const-string v14, "Onboarding: Street in personal info"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_STREET:Lcom/squareup/analytics/RegisterSelectName;

    .line 24
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const-string v14, "ONBOARDING_VERTICAL_RETAIL"

    const/16 v15, 0xf

    const-string v13, "Onboarding: Select Retail vertical"

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_VERTICAL_RETAIL:Lcom/squareup/analytics/RegisterSelectName;

    .line 25
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const-string v13, "ONBOARDING_VERTICAL_SPOS"

    const/16 v14, 0x10

    const-string v15, "Onboarding: Select SPOS vertical"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_VERTICAL_SPOS:Lcom/squareup/analytics/RegisterSelectName;

    .line 26
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const-string v13, "ORDER_CONTACTLESS_ADDRESS_CHANGED"

    const/16 v14, 0x11

    const-string v15, "Order Contactless: Address Changed"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ORDER_CONTACTLESS_ADDRESS_CHANGED:Lcom/squareup/analytics/RegisterSelectName;

    .line 27
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const-string v13, "ORDER_CONTACTLESS_PAYMENT_INPUT"

    const/16 v14, 0x12

    const-string v15, "Order Contactless: Payment Input"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->ORDER_CONTACTLESS_PAYMENT_INPUT:Lcom/squareup/analytics/RegisterSelectName;

    .line 28
    new-instance v0, Lcom/squareup/analytics/RegisterSelectName;

    const-string v13, "PAYMENT_FLOW_METHODS_CASH_FIELD"

    const/16 v14, 0x13

    const-string v15, "Payment Flow Payment Methods Cash Field"

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/analytics/RegisterSelectName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->PAYMENT_FLOW_METHODS_CASH_FIELD:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/squareup/analytics/RegisterSelectName;

    .line 8
    sget-object v13, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BANK_ACCOUNT_HOLDER:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BANK_ACCOUNT_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BANK_ROUTING_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_BUSINESS_NAME:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_CONFIRM_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_COUNTRY_PICKER:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_EIN:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_EMAIL:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_FIRST_NAME:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_LAST_NAME:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_LAST_SSN:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_PASSWORD:Lcom/squareup/analytics/RegisterSelectName;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_PHONE_NUMBER:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_POSTAL:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_STREET:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_VERTICAL_RETAIL:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ONBOARDING_VERTICAL_SPOS:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ORDER_CONTACTLESS_ADDRESS_CHANGED:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->ORDER_CONTACTLESS_PAYMENT_INPUT:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/analytics/RegisterSelectName;->PAYMENT_FLOW_METHODS_CASH_FIELD:Lcom/squareup/analytics/RegisterSelectName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/RegisterSelectName;->$VALUES:[Lcom/squareup/analytics/RegisterSelectName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/squareup/analytics/RegisterSelectName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/RegisterSelectName;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/analytics/RegisterSelectName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/RegisterSelectName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/RegisterSelectName;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/analytics/RegisterSelectName;->$VALUES:[Lcom/squareup/analytics/RegisterSelectName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/RegisterSelectName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/RegisterSelectName;

    return-object v0
.end method
