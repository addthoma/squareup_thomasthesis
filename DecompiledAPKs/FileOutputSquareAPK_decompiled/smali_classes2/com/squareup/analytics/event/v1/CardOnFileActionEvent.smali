.class public Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "CardOnFileActionEvent.java"


# instance fields
.field public final contactToken:Ljava/lang/String;

.field public final entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public final instrumentToken:Ljava/lang/String;

.field public final isSignOnPaperReceipt:Ljava/lang/Boolean;

.field public final isSplitTender:Ljava/lang/Boolean;

.field public final tenderToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 18
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->contactToken:Ljava/lang/String;

    const/4 p1, 0x0

    .line 19
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->instrumentToken:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->tenderToken:Ljava/lang/String;

    .line 21
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 22
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSplitTender:Ljava/lang/Boolean;

    .line 23
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSignOnPaperReceipt:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 29
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->contactToken:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->instrumentToken:Ljava/lang/String;

    const/4 p1, 0x0

    .line 31
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->tenderToken:Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 33
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSplitTender:Ljava/lang/Boolean;

    .line 34
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSignOnPaperReceipt:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 40
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->contactToken:Ljava/lang/String;

    const/4 p1, 0x0

    .line 41
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->instrumentToken:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->tenderToken:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 44
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSplitTender:Ljava/lang/Boolean;

    .line 45
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSignOnPaperReceipt:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 51
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->contactToken:Ljava/lang/String;

    .line 52
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->instrumentToken:Ljava/lang/String;

    const/4 p1, 0x0

    .line 53
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->tenderToken:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 55
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSplitTender:Ljava/lang/Boolean;

    .line 56
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;->isSignOnPaperReceipt:Ljava/lang/Boolean;

    return-void
.end method
