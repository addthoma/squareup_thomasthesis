.class public final Lcom/squareup/analytics/EventStreamAnalytics_Factory;
.super Ljava/lang/Object;
.source "EventStreamAnalytics_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/analytics/EventStreamAnalytics;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p4, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p5, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/analytics/EventStreamAnalytics_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v1/EventStream;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/analytics/EventStreamAnalytics_Factory;"
        }
    .end annotation

    .line 44
    new-instance v6, Lcom/squareup/analytics/EventStreamAnalytics_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/analytics/EventStreamAnalytics_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/analytics/EventStreamAnalytics;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/v1/EventStream;",
            "Lcom/squareup/eventstream/v2/EventstreamV2;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/analytics/EventStreamAnalytics;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/analytics/EventStreamAnalytics;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/analytics/EventStreamAnalytics;-><init>(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/analytics/EventStreamAnalytics;
    .locals 5

    .line 38
    iget-object v0, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v1/EventStream;

    iget-object v1, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/eventstream/v2/EventstreamV2;

    iget-object v2, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/LocalSetting;

    iget-object v4, p0, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->newInstance(Lcom/squareup/eventstream/v1/EventStream;Lcom/squareup/eventstream/v2/EventstreamV2;Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/analytics/EventStreamAnalytics;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/analytics/EventStreamAnalytics_Factory;->get()Lcom/squareup/analytics/EventStreamAnalytics;

    move-result-object v0

    return-object v0
.end method
