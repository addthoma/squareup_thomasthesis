.class public final Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealBuyerLanguageSelectionWorkflow.kt"

# interfaces
.implements Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBuyerLanguageSelectionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBuyerLanguageSelectionWorkflow.kt\ncom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,47:1\n149#2,5:48\n*E\n*S KotlinDebug\n*F\n+ 1 RealBuyerLanguageSelectionWorkflow.kt\ncom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow\n*L\n44#1,5:48\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012&\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ6\u0010\r\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\u0006\u0010\u000e\u001a\u00020\u00032\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0010H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionOutput;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "hideStatusBarWorker",
        "Lcom/squareup/statusbar/workers/HideStatusBarWorker;",
        "englishSelector",
        "Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;",
        "(Lcom/squareup/statusbar/workers/HideStatusBarWorker;Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final englishSelector:Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;

.field private final hideStatusBarWorker:Lcom/squareup/statusbar/workers/HideStatusBarWorker;


# direct methods
.method public constructor <init>(Lcom/squareup/statusbar/workers/HideStatusBarWorker;Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "hideStatusBarWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "englishSelector"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;->hideStatusBarWorker:Lcom/squareup/statusbar/workers/HideStatusBarWorker;

    iput-object p2, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;->englishSelector:Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;->hideStatusBarWorker:Lcom/squareup/statusbar/workers/HideStatusBarWorker;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Worker;

    sget-object v0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow$render$1;->INSTANCE:Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow$render$1;

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 32
    invoke-interface {p2}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 33
    new-instance v7, Lcom/squareup/buyer/language/BuyerLanguageScreen;

    .line 34
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;->englishSelector:Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;

    .line 35
    invoke-virtual {p1}, Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;->getDeviceLocale()Ljava/util/Locale;

    move-result-object p1

    .line 34
    invoke-interface {v0, p1}, Lcom/squareup/buyer/language/BuyerLocaleEnglishSelector;->getCurrentEnglishLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p1

    .line 36
    invoke-static {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenKt;->withDisplayText(Ljava/util/Locale;)Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v1

    .line 37
    sget-object p1, Ljava/util/Locale;->CANADA_FRENCH:Ljava/util/Locale;

    const-string v0, "CANADA_FRENCH"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenKt;->withDisplayText(Ljava/util/Locale;)Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v2

    .line 38
    sget-object p1, Lcom/squareup/util/Locales;->US_SPANISH:Ljava/util/Locale;

    const-string v0, "US_SPANISH"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenKt;->withDisplayText(Ljava/util/Locale;)Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v3

    .line 39
    sget-object p1, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    const-string v0, "JAPANESE"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenKt;->withDisplayText(Ljava/util/Locale;)Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v4

    .line 40
    new-instance p1, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow$render$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow$render$2;-><init>(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 43
    new-instance p1, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow$render$3;

    invoke-direct {p1, p0, p2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow$render$3;-><init>(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;Lcom/squareup/workflow/Sink;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v0, v7

    .line 33
    invoke-direct/range {v0 .. v6}, Lcom/squareup/buyer/language/BuyerLanguageScreen;-><init>(Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v7, Lcom/squareup/workflow/legacy/V2Screen;

    .line 49
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 50
    const-class p2, Lcom/squareup/buyer/language/BuyerLanguageScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v0, ""

    invoke-static {p2, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 51
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 49
    invoke-direct {p1, p2, v7, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflow;->render(Lcom/squareup/buyer/language/BuyerLanguageSelectionProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
