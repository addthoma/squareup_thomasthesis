.class public final Lcom/squareup/buyer/language/RealBuyerLocaleOverride;
.super Ljava/lang/Object;
.source "RealBuyerLocaleOverride.kt"

# interfaces
.implements Lcom/squareup/buyer/language/BuyerLocaleOverride;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBuyerLocaleOverride.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBuyerLocaleOverride.kt\ncom/squareup/buyer/language/RealBuyerLocaleOverride\n*L\n1#1,96:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0008\u0010\u001f\u001a\u00020 H\u0002J\u0008\u0010!\u001a\u00020 H\u0002J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001d0#H\u0016J\u0010\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0016J\u0008\u0010(\u001a\u00020%H\u0016J\u0008\u0010)\u001a\u00020%H\u0016J\u0010\u0010*\u001a\u00020%2\u0006\u0010+\u001a\u00020\u000bH\u0016R\u0014\u0010\r\u001a\u00020\u000b8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u001a\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u00118VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014R\u0014\u0010\u0015\u001a\u00020\u00168VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@RX\u0096\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u000fR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/buyer/language/RealBuyerLocaleOverride;",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "context",
        "Landroid/app/Application;",
        "localeChangedNotifier",
        "Lcom/squareup/locale/LocaleChangedNotifier;",
        "userSettingsProvider",
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "deviceLocale",
        "Ljava/util/Locale;",
        "(Landroid/app/Application;Lcom/squareup/locale/LocaleChangedNotifier;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/settings/server/Features;Ljava/util/Locale;)V",
        "buyerLocale",
        "getBuyerLocale",
        "()Ljava/util/Locale;",
        "buyerMoneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "getBuyerMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "buyerRes",
        "Lcom/squareup/util/Res;",
        "getBuyerRes",
        "()Lcom/squareup/util/Res;",
        "<set-?>",
        "getDeviceLocale",
        "localeOverride",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "getDefaultLocale",
        "isOverrideSet",
        "",
        "isUnitedStatesAccount",
        "localeOverrideFactory",
        "Lio/reactivex/Observable;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "reset",
        "updateLocale",
        "locale",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/app/Application;

.field private deviceLocale:Ljava/util/Locale;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final localeChangedNotifier:Lcom/squareup/locale/LocaleChangedNotifier;

.field private final localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/locale/LocaleChangedNotifier;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/settings/server/Features;Ljava/util/Locale;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeChangedNotifier"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userSettingsProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceLocale"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->context:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeChangedNotifier:Lcom/squareup/locale/LocaleChangedNotifier;

    iput-object p3, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    iput-object p4, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->features:Lcom/squareup/settings/server/Features;

    .line 32
    new-instance p1, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object p2, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->context:Landroid/app/Application;

    check-cast p2, Landroid/content/Context;

    invoke-direct {p0, p5}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->getDefaultLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefa\u2026ultLocale(deviceLocale)))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 34
    iput-object p5, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->deviceLocale:Ljava/util/Locale;

    return-void
.end method

.method public static final synthetic access$getDefaultLocale(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->getDefaultLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDeviceLocale$p(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;)Ljava/util/Locale;
    .locals 0

    .line 24
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->getDeviceLocale()Ljava/util/Locale;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$isOverrideSet(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;)Z
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->isOverrideSet()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setDeviceLocale$p(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;Ljava/util/Locale;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->deviceLocale:Ljava/util/Locale;

    return-void
.end method

.method private final getDefaultLocale(Ljava/util/Locale;)Ljava/util/Locale;
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->T2_BUYER_CHECKOUT_DEFAULTS_TO_US_ENGLISH:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->isUnitedStatesAccount()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v0, "Locale.US"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method

.method private final isOverrideSet()Z
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->getDeviceLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final isUnitedStatesAccount()Z
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v0}, Lcom/squareup/settings/server/UserSettingsProvider;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 93
    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 92
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The current account country code is null!"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public getBuyerLocale()Ljava/util/Locale;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public getBuyerRes()Lcom/squareup/util/Res;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Kt;->requireValue(Lcom/jakewharton/rxrelay2/BehaviorRelay;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceLocale()Ljava/util/Locale;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->deviceLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public localeOverrideFactory()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/locale/LocaleOverrideFactory;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hide()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "localeOverride.hide()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeChangedNotifier:Lcom/squareup/locale/LocaleChangedNotifier;

    invoke-interface {v0}, Lcom/squareup/locale/LocaleChangedNotifier;->getLocale()Lio/reactivex/Observable;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;-><init>(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public reset()V
    .locals 1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->getDeviceLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->getDefaultLocale(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->updateLocale(Ljava/util/Locale;)V

    return-void
.end method

.method public updateLocale(Ljava/util/Locale;)V
    .locals 3

    const-string v0, "locale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->localeOverride:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/locale/LocaleOverrideFactory;

    iget-object v2, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->context:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
