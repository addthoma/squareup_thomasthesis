.class final Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerLocaleOverride.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/util/Locale;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "deviceLocale",
        "Ljava/util/Locale;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyer/language/RealBuyerLocaleOverride;


# direct methods
.method constructor <init>(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;->this$0:Lcom/squareup/buyer/language/RealBuyerLocaleOverride;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Ljava/util/Locale;

    invoke-virtual {p0, p1}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;->invoke(Ljava/util/Locale;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/util/Locale;)V
    .locals 2

    const-string v0, "deviceLocale"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;->this$0:Lcom/squareup/buyer/language/RealBuyerLocaleOverride;

    invoke-static {v0}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->access$isOverrideSet(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;->this$0:Lcom/squareup/buyer/language/RealBuyerLocaleOverride;

    invoke-static {v0, p1}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->access$getDefaultLocale(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->updateLocale(Ljava/util/Locale;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLocaleOverride$onEnterScope$1;->this$0:Lcom/squareup/buyer/language/RealBuyerLocaleOverride;

    invoke-static {v0, p1}, Lcom/squareup/buyer/language/RealBuyerLocaleOverride;->access$setDeviceLocale$p(Lcom/squareup/buyer/language/RealBuyerLocaleOverride;Ljava/util/Locale;)V

    return-void
.end method
