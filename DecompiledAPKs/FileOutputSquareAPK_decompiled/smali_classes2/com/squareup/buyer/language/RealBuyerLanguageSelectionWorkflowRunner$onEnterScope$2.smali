.class final Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerLanguageSelectionWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/buyer/language/Exit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/buyer/language/Exit;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/buyer/language/Exit;

    invoke-virtual {p0, p1}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$2;->invoke(Lcom/squareup/buyer/language/Exit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/buyer/language/Exit;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;->access$getContainer$p(Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    sget-object v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner;->Companion:Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/buyer/language/BuyerLanguageSelectionWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/main/PosContainer;->goBackPastWorkflow(Ljava/lang/String;)V

    return-void
.end method
