.class final enum Lcom/squareup/api/ApiVersion$7;
.super Lcom/squareup/api/ApiVersion;
.source "ApiVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ApiVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/ApiVersion;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/squareup/api/ApiVersion$1;)V

    return-void
.end method


# virtual methods
.method upgrade(Landroid/content/Intent;)V
    .locals 3

    .line 104
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.squareup.register.action.CHARGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.squareup.pos.action.CHARGE"

    .line 105
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    :cond_0
    sget-object v0, Lcom/squareup/api/ApiVersion$7;->V1_3:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion$7;->V2_0:Lcom/squareup/api/ApiVersion;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/api/ApiVersion;->access$100(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;Lcom/squareup/api/ApiVersion;Z)Landroid/content/Intent;

    move-result-object v0

    .line 108
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method
