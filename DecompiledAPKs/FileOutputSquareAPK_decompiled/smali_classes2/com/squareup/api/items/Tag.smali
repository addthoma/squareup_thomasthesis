.class public final Lcom/squareup/api/items/Tag;
.super Lcom/squareup/wire/Message;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Tag$ProtoAdapter_Tag;,
        Lcom/squareup/api/items/Tag$Type;,
        Lcom/squareup/api/items/Tag$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Tag;",
        "Lcom/squareup/api/items/Tag$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Tag;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COLOR:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/api/items/Tag$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final color:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final tag_membership:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final type:Lcom/squareup/api/items/Tag$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Tag$Type#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/api/items/Tag$ProtoAdapter_Tag;

    invoke-direct {v0}, Lcom/squareup/api/items/Tag$ProtoAdapter_Tag;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 37
    sget-object v0, Lcom/squareup/api/items/Tag$Type;->DEFAULT:Lcom/squareup/api/items/Tag$Type;

    sput-object v0, Lcom/squareup/api/items/Tag;->DEFAULT_TYPE:Lcom/squareup/api/items/Tag$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Tag$Type;Ljava/util/List;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Tag$Type;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 80
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/Tag;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Tag$Type;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Tag$Type;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Tag$Type;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 85
    sget-object v0, Lcom/squareup/api/items/Tag;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 86
    iput-object p1, p0, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    const-string p1, "tag_membership"

    .line 89
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    .line 90
    iput-object p5, p0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 108
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Tag;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 109
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Tag;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/api/items/Tag;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Tag;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    iget-object v3, p1, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    .line 114
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 122
    invoke-virtual {p0}, Lcom/squareup/api/items/Tag;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/Tag$Type;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 128
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Tag$Builder;
    .locals 2

    .line 95
    new-instance v0, Lcom/squareup/api/items/Tag$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Tag$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Tag$Builder;->id:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Tag$Builder;->name:Ljava/lang/String;

    .line 98
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    iput-object v1, v0, Lcom/squareup/api/items/Tag$Builder;->type:Lcom/squareup/api/items/Tag$Type;

    .line 99
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/Tag$Builder;->tag_membership:Ljava/util/List;

    .line 100
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Tag$Builder;->color:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/api/items/Tag;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Tag$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/api/items/Tag;->newBuilder()Lcom/squareup/api/items/Tag$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->type:Lcom/squareup/api/items/Tag$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", tag_membership="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->tag_membership:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Tag;->color:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tag{"

    .line 141
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
