.class public final Lcom/squareup/api/items/Fee;
.super Lcom/squareup/wire/Message;
.source "Fee.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Fee$ProtoAdapter_Fee;,
        Lcom/squareup/api/items/Fee$InclusionType;,
        Lcom/squareup/api/items/Fee$AdjustmentType;,
        Lcom/squareup/api/items/Fee$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Fee;",
        "Lcom/squareup/api/items/Fee$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Fee;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/api/items/Fee$AdjustmentType;

.field public static final DEFAULT_APPLIES_TO_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_FEE_TYPE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_FEE_TYPE_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Fee$AdjustmentType#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final amount:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final applies_to_custom_amounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final applies_to_product_set:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final calculation_phase:Lcom/squareup/api/items/CalculationPhase;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.CalculationPhase#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final fee_type_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final fee_type_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Fee$InclusionType#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/api/items/Fee$ProtoAdapter_Fee;

    invoke-direct {v0}, Lcom/squareup/api/items/Fee$ProtoAdapter_Fee;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Fee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->FEE_SUBTOTAL_PHASE:Lcom/squareup/api/items/CalculationPhase;

    sput-object v0, Lcom/squareup/api/items/Fee;->DEFAULT_CALCULATION_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 41
    sget-object v0, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    sput-object v0, Lcom/squareup/api/items/Fee;->DEFAULT_ADJUSTMENT_TYPE:Lcom/squareup/api/items/Fee$AdjustmentType;

    const/4 v0, 0x1

    .line 43
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Fee;->DEFAULT_APPLIES_TO_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    .line 45
    sput-object v0, Lcom/squareup/api/items/Fee;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    .line 47
    sget-object v0, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    sput-object v0, Lcom/squareup/api/items/Fee;->DEFAULT_INCLUSION_TYPE:Lcom/squareup/api/items/Fee$InclusionType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Fee$AdjustmentType;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/api/items/Fee$InclusionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;)V
    .locals 16

    .line 175
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/api/items/Fee;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Fee$AdjustmentType;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/api/items/Fee$InclusionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/api/items/CalculationPhase;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Fee$AdjustmentType;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/api/items/Fee$InclusionType;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 183
    sget-object v1, Lcom/squareup/api/items/Fee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 184
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    move-object v1, p2

    .line 185
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    move-object v1, p3

    .line 186
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    move-object v1, p4

    .line 187
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    move-object v1, p5

    .line 188
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    move-object v1, p6

    .line 189
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    move-object v1, p7

    .line 190
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    move-object v1, p8

    .line 191
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    move-object v1, p9

    .line 192
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    move-object v1, p10

    .line 193
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    move-object v1, p11

    .line 194
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    move-object v1, p12

    .line 195
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    move-object/from16 v1, p13

    .line 196
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 197
    iput-object v1, v0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 224
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Fee;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 225
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Fee;

    .line 226
    invoke-virtual {p0}, Lcom/squareup/api/items/Fee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Fee;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    .line 235
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    .line 236
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 237
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 238
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    .line 239
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    .line 240
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 245
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_e

    .line 247
    invoke-virtual {p0}, Lcom/squareup/api/items/Fee;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/CalculationPhase;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee$AdjustmentType;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/api/items/Fee$InclusionType;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 260
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 261
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 262
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Fee$Builder;
    .locals 2

    .line 202
    new-instance v0, Lcom/squareup/api/items/Fee$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Fee$Builder;-><init>()V

    .line 203
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->id:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->name:Ljava/lang/String;

    .line 205
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->percentage:Ljava/lang/String;

    .line 206
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->amount:Lcom/squareup/protos/common/dinero/Money;

    .line 207
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    .line 208
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->fee_type_id:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->fee_type_name:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 211
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->applies_to_custom_amounts:Ljava/lang/Boolean;

    .line 212
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->enabled:Ljava/lang/Boolean;

    .line 213
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    .line 214
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 215
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->v2_id:Ljava/lang/String;

    .line 216
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/Fee$Builder;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    .line 217
    invoke-virtual {p0}, Lcom/squareup/api/items/Fee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Fee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/items/Fee;->newBuilder()Lcom/squareup/api/items/Fee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 270
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    if-eqz v1, :cond_4

    const-string v1, ", calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->calculation_phase:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", fee_type_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", fee_type_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->fee_type_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    if-eqz v1, :cond_7

    const-string v1, ", adjustment_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->adjustment_type:Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 278
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", applies_to_custom_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_custom_amounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    if-eqz v1, :cond_a

    const-string v1, ", inclusion_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->inclusion_type:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_b

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_b
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    :cond_c
    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_d

    const-string v1, ", applies_to_product_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Fee;->applies_to_product_set:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Fee{"

    .line 284
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
