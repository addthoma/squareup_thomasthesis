.class public final Lcom/squareup/api/items/Page;
.super Lcom/squareup/wire/Message;
.source "Page.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Page$ProtoAdapter_Page;,
        Lcom/squareup/api/items/Page$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/Page;",
        "Lcom/squareup/api/items/Page$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Page;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LAYOUT:Lcom/squareup/api/items/PageLayout;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PAGE_INDEX:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final layout:Lcom/squareup/api/items/PageLayout;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PageLayout#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final page_index:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final tag:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/api/items/Page$ProtoAdapter_Page;

    invoke-direct {v0}, Lcom/squareup/api/items/Page$ProtoAdapter_Page;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/Page;->DEFAULT_PAGE_INDEX:Ljava/lang/Integer;

    .line 32
    sget-object v0, Lcom/squareup/api/items/PageLayout;->UNKNOWN_PAGE_LAYOUT:Lcom/squareup/api/items/PageLayout;

    sput-object v0, Lcom/squareup/api/items/Page;->DEFAULT_LAYOUT:Lcom/squareup/api/items/PageLayout;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Lcom/squareup/api/sync/ObjectId;)V
    .locals 7

    .line 77
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/Page;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/api/items/Page;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 83
    iput-object p1, p0, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    .line 84
    iput-object p2, p0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    .line 85
    iput-object p3, p0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    .line 86
    iput-object p4, p0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    .line 87
    iput-object p5, p0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 105
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/Page;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 106
    :cond_1
    check-cast p1, Lcom/squareup/api/items/Page;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/api/items/Page;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/Page;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    .line 109
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    .line 110
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    iget-object v3, p1, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    .line 111
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    .line 112
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 117
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 119
    invoke-virtual {p0}, Lcom/squareup/api/items/Page;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 121
    iget-object v1, p0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 122
    iget-object v1, p0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/PageLayout;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 125
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/Page$Builder;
    .locals 2

    .line 92
    new-instance v0, Lcom/squareup/api/items/Page$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Page$Builder;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Page$Builder;->id:Ljava/lang/String;

    .line 94
    iget-object v1, p0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/Page$Builder;->page_index:Ljava/lang/Integer;

    .line 95
    iget-object v1, p0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/Page$Builder;->name:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    iput-object v1, v0, Lcom/squareup/api/items/Page$Builder;->layout:Lcom/squareup/api/items/PageLayout;

    .line 97
    iget-object v1, p0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/Page$Builder;->tag:Lcom/squareup/api/sync/ObjectId;

    .line 98
    invoke-virtual {p0}, Lcom/squareup/api/items/Page;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/Page$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/api/items/Page;->newBuilder()Lcom/squareup/api/items/Page$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 133
    iget-object v1, p0, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Page;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", page_index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Page;->page_index:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Page;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    if-eqz v1, :cond_3

    const-string v1, ", layout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Page;->layout:Lcom/squareup/api/items/PageLayout;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 137
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_4

    const-string v1, ", tag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/Page;->tag:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Page{"

    .line 138
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
