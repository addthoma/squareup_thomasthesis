.class public final Lcom/squareup/api/items/TaxRule$Condition;
.super Lcom/squareup/wire/Message;
.source "TaxRule.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TaxRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Condition"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;,
        Lcom/squareup/api/items/TaxRule$Condition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/TaxRule$Condition;",
        "Lcom/squareup/api/items/TaxRule$Condition$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/TaxRule$Condition;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ObjectPredicate#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final max_item_price:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final max_total_amount:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final min_item_price:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 271
    new-instance v0, Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;

    invoke-direct {v0}, Lcom/squareup/api/items/TaxRule$Condition$ProtoAdapter_Condition;-><init>()V

    sput-object v0, Lcom/squareup/api/items/TaxRule$Condition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ObjectPredicate;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;)V
    .locals 6

    .line 337
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/TaxRule$Condition;-><init>(Lcom/squareup/api/items/ObjectPredicate;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/ObjectPredicate;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lcom/squareup/protos/common/dinero/Money;Lokio/ByteString;)V
    .locals 1

    .line 342
    sget-object v0, Lcom/squareup/api/items/TaxRule$Condition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 343
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    .line 344
    iput-object p2, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    .line 345
    iput-object p3, p0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 346
    iput-object p4, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 363
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/TaxRule$Condition;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 364
    :cond_1
    check-cast p1, Lcom/squareup/api/items/TaxRule$Condition;

    .line 365
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Condition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule$Condition;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    .line 366
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    .line 367
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    iget-object p1, p1, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 369
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 374
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 376
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Condition;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 377
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/ObjectPredicate;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 378
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 379
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 380
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 381
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/TaxRule$Condition$Builder;
    .locals 2

    .line 351
    new-instance v0, Lcom/squareup/api/items/TaxRule$Condition$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TaxRule$Condition$Builder;-><init>()V

    .line 352
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    .line 353
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    .line 354
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 355
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Condition$Builder;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    .line 356
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Condition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TaxRule$Condition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 270
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule$Condition;->newBuilder()Lcom/squareup/api/items/TaxRule$Condition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 389
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    if-eqz v1, :cond_0

    const-string v1, ", dining_option_predicate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->dining_option_predicate:Lcom/squareup/api/items/ObjectPredicate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 390
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_1

    const-string v1, ", max_total_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_total_amount:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 391
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_2

    const-string v1, ", min_item_price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->min_item_price:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 392
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_3

    const-string v1, ", max_item_price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule$Condition;->max_item_price:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Condition{"

    .line 393
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
