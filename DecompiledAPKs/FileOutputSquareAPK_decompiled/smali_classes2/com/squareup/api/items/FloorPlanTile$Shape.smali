.class public final enum Lcom/squareup/api/items/FloorPlanTile$Shape;
.super Ljava/lang/Enum;
.source "FloorPlanTile.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/FloorPlanTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Shape"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/FloorPlanTile$Shape$ProtoAdapter_Shape;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/FloorPlanTile$Shape;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/FloorPlanTile$Shape;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/FloorPlanTile$Shape;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum RECTANGULAR:Lcom/squareup/api/items/FloorPlanTile$Shape;

.field public static final enum ROUNDED_RECTANGLE:Lcom/squareup/api/items/FloorPlanTile$Shape;

.field public static final enum UNKNOWN:Lcom/squareup/api/items/FloorPlanTile$Shape;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 341
    new-instance v0, Lcom/squareup/api/items/FloorPlanTile$Shape;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/FloorPlanTile$Shape;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile$Shape;->UNKNOWN:Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 343
    new-instance v0, Lcom/squareup/api/items/FloorPlanTile$Shape;

    const/4 v2, 0x1

    const-string v3, "RECTANGULAR"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/FloorPlanTile$Shape;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile$Shape;->RECTANGULAR:Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 351
    new-instance v0, Lcom/squareup/api/items/FloorPlanTile$Shape;

    const/4 v3, 0x2

    const-string v4, "ROUNDED_RECTANGLE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/FloorPlanTile$Shape;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile$Shape;->ROUNDED_RECTANGLE:Lcom/squareup/api/items/FloorPlanTile$Shape;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 340
    sget-object v4, Lcom/squareup/api/items/FloorPlanTile$Shape;->UNKNOWN:Lcom/squareup/api/items/FloorPlanTile$Shape;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/api/items/FloorPlanTile$Shape;->RECTANGULAR:Lcom/squareup/api/items/FloorPlanTile$Shape;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/FloorPlanTile$Shape;->ROUNDED_RECTANGLE:Lcom/squareup/api/items/FloorPlanTile$Shape;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile$Shape;->$VALUES:[Lcom/squareup/api/items/FloorPlanTile$Shape;

    .line 353
    new-instance v0, Lcom/squareup/api/items/FloorPlanTile$Shape$ProtoAdapter_Shape;

    invoke-direct {v0}, Lcom/squareup/api/items/FloorPlanTile$Shape$ProtoAdapter_Shape;-><init>()V

    sput-object v0, Lcom/squareup/api/items/FloorPlanTile$Shape;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 357
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 358
    iput p3, p0, Lcom/squareup/api/items/FloorPlanTile$Shape;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/FloorPlanTile$Shape;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 368
    :cond_0
    sget-object p0, Lcom/squareup/api/items/FloorPlanTile$Shape;->ROUNDED_RECTANGLE:Lcom/squareup/api/items/FloorPlanTile$Shape;

    return-object p0

    .line 367
    :cond_1
    sget-object p0, Lcom/squareup/api/items/FloorPlanTile$Shape;->RECTANGULAR:Lcom/squareup/api/items/FloorPlanTile$Shape;

    return-object p0

    .line 366
    :cond_2
    sget-object p0, Lcom/squareup/api/items/FloorPlanTile$Shape;->UNKNOWN:Lcom/squareup/api/items/FloorPlanTile$Shape;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/FloorPlanTile$Shape;
    .locals 1

    .line 340
    const-class v0, Lcom/squareup/api/items/FloorPlanTile$Shape;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/FloorPlanTile$Shape;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/FloorPlanTile$Shape;
    .locals 1

    .line 340
    sget-object v0, Lcom/squareup/api/items/FloorPlanTile$Shape;->$VALUES:[Lcom/squareup/api/items/FloorPlanTile$Shape;

    invoke-virtual {v0}, [Lcom/squareup/api/items/FloorPlanTile$Shape;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/FloorPlanTile$Shape;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 375
    iget v0, p0, Lcom/squareup/api/items/FloorPlanTile$Shape;->value:I

    return v0
.end method
