.class public final Lcom/squareup/api/items/DiningOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiningOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/DiningOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/DiningOption;",
        "Lcom/squareup/api/items/DiningOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/DiningOption;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/api/items/DiningOption;

    iget-object v1, p0, Lcom/squareup/api/items/DiningOption$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/DiningOption$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/DiningOption$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/DiningOption$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/DiningOption;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/items/MerchantCatalogObjectReference;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/api/items/DiningOption$Builder;->build()Lcom/squareup/api/items/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/DiningOption$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/api/items/DiningOption$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/api/items/DiningOption$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/DiningOption$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/api/items/DiningOption$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/DiningOption$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/api/items/DiningOption$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method
