.class public final Lcom/squareup/api/items/ItemItemModifierListMembership;
.super Lcom/squareup/wire/Message;
.source "ItemItemModifierListMembership.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;,
        Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ItemItemModifierListMembership;",
        "Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemItemModifierListMembership;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_HIDDEN_FROM_CUSTOMER:Ljava/lang/Boolean;

.field public static final DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

.field public static final DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

.field public static final DEFAULT_VISIBILITY:Lcom/squareup/api/items/Visibility;

.field private static final serialVersionUID:J


# instance fields
.field public final enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final hidden_from_customer:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final item:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final item_modifier_option_overrides:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemItemModifierOptionOverrideMapFieldEntry#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final max_selected_modifiers:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final min_selected_modifiers:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final modifier_list:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final visibility:Lcom/squareup/api/items/Visibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Visibility#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierListMembership$ProtoAdapter_ItemItemModifierListMembership;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/api/items/Visibility;->PUBLIC:Lcom/squareup/api/items/Visibility;

    sput-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_VISIBILITY:Lcom/squareup/api/items/Visibility;

    const/4 v0, -0x1

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MIN_SELECTED_MODIFIERS:Ljava/lang/Integer;

    .line 32
    sput-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_MAX_SELECTED_MODIFIERS:Ljava/lang/Integer;

    const/4 v0, 0x1

    .line 34
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_ENABLED:Ljava/lang/Boolean;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->DEFAULT_HIDDEN_FROM_CUSTOMER:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/Visibility;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/items/Visibility;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 115
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/ItemItemModifierListMembership;-><init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/Visibility;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/Visibility;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/items/Visibility;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemItemModifierOptionOverrideMapFieldEntry;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 123
    sget-object v0, Lcom/squareup/api/items/ItemItemModifierListMembership;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    .line 125
    iput-object p2, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 126
    iput-object p3, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    const-string p1, "item_modifier_option_overrides"

    .line 127
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    .line 128
    iput-object p5, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    .line 129
    iput-object p6, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    .line 130
    iput-object p7, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    .line 131
    iput-object p8, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 152
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 153
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ItemItemModifierListMembership;

    .line 154
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierListMembership;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemItemModifierListMembership;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    .line 158
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    .line 162
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 169
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierListMembership;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/Visibility;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 178
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;
    .locals 2

    .line 136
    new-instance v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    .line 138
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 139
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->visibility:Lcom/squareup/api/items/Visibility;

    .line 140
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->item_modifier_option_overrides:Ljava/util/List;

    .line 141
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->min_selected_modifiers:Ljava/lang/Integer;

    .line 142
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->max_selected_modifiers:Ljava/lang/Integer;

    .line 143
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->enabled:Ljava/lang/Boolean;

    .line 144
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->hidden_from_customer:Ljava/lang/Boolean;

    .line 145
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierListMembership;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemItemModifierListMembership;->newBuilder()Lcom/squareup/api/items/ItemItemModifierListMembership$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_1

    const-string v1, ", modifier_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    if-eqz v1, :cond_2

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->visibility:Lcom/squareup/api/items/Visibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", item_modifier_option_overrides="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->item_modifier_option_overrides:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", min_selected_modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->min_selected_modifiers:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", max_selected_modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->max_selected_modifiers:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", hidden_from_customer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemItemModifierListMembership;->hidden_from_customer:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemItemModifierListMembership{"

    .line 194
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
