.class public final Lcom/squareup/api/items/PricingRule;
.super Lcom/squareup/wire/Message;
.source "PricingRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/PricingRule$ProtoAdapter_PricingRule;,
        Lcom/squareup/api/items/PricingRule$DiscountTargetScope;,
        Lcom/squareup/api/items/PricingRule$ApplicationMode;,
        Lcom/squareup/api/items/PricingRule$ExcludeStrategy;,
        Lcom/squareup/api/items/PricingRule$Stackable;,
        Lcom/squareup/api/items/PricingRule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/PricingRule;",
        "Lcom/squareup/api/items/PricingRule$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/PricingRule;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APPLICATION_MODE:Lcom/squareup/api/items/PricingRule$ApplicationMode;

.field public static final DEFAULT_DISCOUNT_TARGET_SCOPE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

.field public static final DEFAULT_EXCLUDE_STRATEGY:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MAX_APPLICATIONS_PER_ATTACHMENT:Ljava/lang/Integer;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_STACKABLE:Lcom/squareup/api/items/PricingRule$Stackable;

.field public static final DEFAULT_VALID_FROM:Ljava/lang/String; = ""

.field public static final DEFAULT_VALID_UNTIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingRule$ApplicationMode#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final apply_products:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final discount_id:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingRule$DiscountTargetScope#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final exclude_products:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingRule$ExcludeStrategy#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final match_products:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final max_applications_per_attachment:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x10
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final stackable:Lcom/squareup/api/items/PricingRule$Stackable;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.PricingRule$Stackable#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final valid_from:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final valid_until:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final validity:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/api/items/PricingRule$ProtoAdapter_PricingRule;

    invoke-direct {v0}, Lcom/squareup/api/items/PricingRule$ProtoAdapter_PricingRule;-><init>()V

    sput-object v0, Lcom/squareup/api/items/PricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/api/items/PricingRule$Stackable;->EXCLUSIVE:Lcom/squareup/api/items/PricingRule$Stackable;

    sput-object v0, Lcom/squareup/api/items/PricingRule;->DEFAULT_STACKABLE:Lcom/squareup/api/items/PricingRule$Stackable;

    .line 39
    sget-object v0, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->LEAST_EXPENSIVE:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    sput-object v0, Lcom/squareup/api/items/PricingRule;->DEFAULT_EXCLUDE_STRATEGY:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    .line 41
    sget-object v0, Lcom/squareup/api/items/PricingRule$ApplicationMode;->AUTOMATIC:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    sput-object v0, Lcom/squareup/api/items/PricingRule;->DEFAULT_APPLICATION_MODE:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    .line 43
    sget-object v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    sput-object v0, Lcom/squareup/api/items/PricingRule;->DEFAULT_DISCOUNT_TARGET_SCOPE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    const/4 v0, -0x1

    .line 45
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/PricingRule;->DEFAULT_MAX_APPLICATIONS_PER_ATTACHMENT:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$Stackable;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$ExcludeStrategy;Lcom/squareup/api/items/PricingRule$ApplicationMode;Lcom/squareup/api/items/PricingRule$DiscountTargetScope;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/items/PricingRule$Stackable;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/items/PricingRule$ExcludeStrategy;",
            "Lcom/squareup/api/items/PricingRule$ApplicationMode;",
            "Lcom/squareup/api/items/PricingRule$DiscountTargetScope;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/api/sync/ObjectId;",
            ")V"
        }
    .end annotation

    .line 203
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/api/items/PricingRule;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$Stackable;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$ExcludeStrategy;Lcom/squareup/api/items/PricingRule$ApplicationMode;Lcom/squareup/api/items/PricingRule$DiscountTargetScope;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$Stackable;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/PricingRule$ExcludeStrategy;Lcom/squareup/api/items/PricingRule$ApplicationMode;Lcom/squareup/api/items/PricingRule$DiscountTargetScope;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/items/PricingRule$Stackable;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lcom/squareup/api/items/PricingRule$ExcludeStrategy;",
            "Lcom/squareup/api/items/PricingRule$ApplicationMode;",
            "Lcom/squareup/api/items/PricingRule$DiscountTargetScope;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/api/sync/ObjectId;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 211
    sget-object v1, Lcom/squareup/api/items/PricingRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 212
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    move-object v1, p2

    .line 213
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    const-string/jumbo v1, "validity"

    move-object v2, p3

    .line 214
    invoke-static {v1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    move-object v1, p4

    .line 215
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    move-object v1, p5

    .line 216
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    move-object v1, p6

    .line 217
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    move-object v1, p7

    .line 218
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    move-object v1, p8

    .line 219
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    move-object v1, p9

    .line 220
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    move-object v1, p10

    .line 221
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    move-object v1, p11

    .line 222
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    move-object v1, p12

    .line 223
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    move-object/from16 v1, p13

    .line 224
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    move-object/from16 v1, p14

    .line 225
    iput-object v1, v0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 252
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/PricingRule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 253
    :cond_1
    check-cast p1, Lcom/squareup/api/items/PricingRule;

    .line 254
    invoke-virtual {p0}, Lcom/squareup/api/items/PricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/PricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    .line 255
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    .line 256
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    .line 257
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    .line 258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    .line 260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    .line 263
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    .line 264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    .line 265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    .line 268
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 273
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 275
    invoke-virtual {p0}, Lcom/squareup/api/items/PricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 276
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 277
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 278
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 279
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 280
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingRule$Stackable;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingRule$ExcludeStrategy;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingRule$ApplicationMode;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 288
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 289
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 290
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/PricingRule$Builder;
    .locals 2

    .line 230
    new-instance v0, Lcom/squareup/api/items/PricingRule$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/PricingRule$Builder;-><init>()V

    .line 231
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->id:Ljava/lang/String;

    .line 232
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->name:Ljava/lang/String;

    .line 233
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->validity:Ljava/util/List;

    .line 234
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->match_products:Lcom/squareup/api/sync/ObjectId;

    .line 235
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->apply_products:Lcom/squareup/api/sync/ObjectId;

    .line 236
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    .line 237
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->valid_from:Ljava/lang/String;

    .line 238
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->valid_until:Ljava/lang/String;

    .line 239
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    .line 240
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    .line 241
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    .line 242
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    .line 243
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->max_applications_per_attachment:Ljava/lang/Integer;

    .line 244
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/PricingRule$Builder;->discount_id:Lcom/squareup/api/sync/ObjectId;

    .line 245
    invoke-virtual {p0}, Lcom/squareup/api/items/PricingRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/PricingRule$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/api/items/PricingRule;->newBuilder()Lcom/squareup/api/items/PricingRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 298
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", validity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->validity:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 301
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_3

    const-string v1, ", match_products="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->match_products:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 302
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_4

    const-string v1, ", apply_products="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->apply_products:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 303
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    if-eqz v1, :cond_5

    const-string v1, ", stackable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->stackable:Lcom/squareup/api/items/PricingRule$Stackable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 304
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", valid_from="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_from:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", valid_until="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->valid_until:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_7
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_8

    const-string v1, ", exclude_products="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_products:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 307
    :cond_8
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    if-eqz v1, :cond_9

    const-string v1, ", exclude_strategy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->exclude_strategy:Lcom/squareup/api/items/PricingRule$ExcludeStrategy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 308
    :cond_9
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    if-eqz v1, :cond_a

    const-string v1, ", application_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->application_mode:Lcom/squareup/api/items/PricingRule$ApplicationMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 309
    :cond_a
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    if-eqz v1, :cond_b

    const-string v1, ", discount_target_scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_target_scope:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 310
    :cond_b
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    const-string v1, ", max_applications_per_attachment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->max_applications_per_attachment:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 311
    :cond_c
    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_d

    const-string v1, ", discount_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/PricingRule;->discount_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PricingRule{"

    .line 312
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
