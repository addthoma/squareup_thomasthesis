.class public final Lcom/squareup/api/items/InventoryInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InventoryInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/InventoryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/InventoryInfo;",
        "Lcom/squareup/api/items/InventoryInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public item:Lcom/squareup/api/sync/ObjectId;

.field public quantity:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/InventoryInfo;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/api/items/InventoryInfo;

    iget-object v1, p0, Lcom/squareup/api/items/InventoryInfo$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/InventoryInfo$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/InventoryInfo$Builder;->quantity:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/InventoryInfo;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/squareup/api/items/InventoryInfo$Builder;->build()Lcom/squareup/api/items/InventoryInfo;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/InventoryInfo$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/api/items/InventoryInfo$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/InventoryInfo$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/api/items/InventoryInfo$Builder;->item:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public quantity(Ljava/lang/Long;)Lcom/squareup/api/items/InventoryInfo$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/api/items/InventoryInfo$Builder;->quantity:Ljava/lang/Long;

    return-object p0
.end method
