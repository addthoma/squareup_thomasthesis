.class public final enum Lcom/squareup/api/items/Placeholder$PlaceholderType;
.super Ljava/lang/Enum;
.source "Placeholder.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Placeholder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlaceholderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Placeholder$PlaceholderType$ProtoAdapter_PlaceholderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Placeholder$PlaceholderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Placeholder$PlaceholderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADD_CUSTOMER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ADJUST_TIP:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ALL_ITEMS_AND_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ALL_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ASSIGN_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum AUTO_GRATUITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum CALCULATOR:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum CLOCK_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum COMP_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum COMP_VOID:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum COUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum COUPONS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum CUSTOM_AMOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum CUSTOM_DISCOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum CUSTOM_GIFT_CARD:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum CUSTOM_QUANTITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum DINING_OPTION_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum EDIT_TICKET_ALLERGY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum EDIT_TICKET_NAME_AND_NOTES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum EDIT_TICKET_SEATS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ISSUE_REFUND:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum LINE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum MOVE_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum OPEN_ALL_DRAWERS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum ORDER_HISTORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum PAY_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_2:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_3:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_4:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_5:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_6:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_7:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_8:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum QUANTITY_9:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum REWARDS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum SHAPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum SPLIT_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum TEXT_LABEL:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum UNKNOWN:Lcom/squareup/api/items/Placeholder$PlaceholderType;

.field public static final enum VOID_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 175
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->UNKNOWN:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 177
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v2, 0x1

    const-string v3, "DISCOUNTS_CATEGORY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 179
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v3, 0x2

    const-string v4, "REWARDS_FINDER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->REWARDS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 181
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v4, 0x3

    const-string v5, "ALL_ITEMS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 183
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v5, 0x4

    const-string v6, "COUPONS_FINDER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COUPONS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 190
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v6, 0x5

    const-string v7, "GIFT_CARDS_CATEGORY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 196
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v7, 0x6

    const-string v8, "CUSTOM_AMOUNT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_AMOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 201
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/4 v8, 0x7

    const-string v9, "CALCULATOR"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CALCULATOR:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 207
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v9, 0x8

    const-string v10, "QUANTITY_2"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_2:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 209
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v10, 0x9

    const-string v11, "QUANTITY_3"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_3:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 211
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v11, 0xa

    const-string v12, "QUANTITY_4"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_4:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 213
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v12, 0xb

    const-string v13, "QUANTITY_5"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_5:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 215
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v13, 0xc

    const-string v14, "QUANTITY_6"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_6:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 217
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v14, 0xd

    const-string v15, "QUANTITY_7"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_7:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 219
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v15, 0xe

    const-string v14, "QUANTITY_8"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_8:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 221
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v14, "QUANTITY_9"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_9:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 227
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "CUSTOM_QUANTITY"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_QUANTITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 233
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "CLOCK_IN_OUT"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CLOCK_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 239
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ISSUE_REFUND"

    const/16 v14, 0x12

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ISSUE_REFUND:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 245
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ADJUST_TIP"

    const/16 v14, 0x13

    const/16 v15, 0x13

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADJUST_TIP:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 251
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "COMP_VOID"

    const/16 v14, 0x14

    const/16 v15, 0x14

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COMP_VOID:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 257
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ADD_CUSTOMER"

    const/16 v14, 0x15

    const/16 v15, 0x15

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADD_CUSTOMER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 263
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "EDIT_TICKET_NAME_AND_NOTES"

    const/16 v14, 0x16

    const/16 v15, 0x16

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_NAME_AND_NOTES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 269
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "VOID_TICKET"

    const/16 v14, 0x17

    const/16 v15, 0x17

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->VOID_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 275
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "COMP_TICKET"

    const/16 v14, 0x18

    const/16 v15, 0x18

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COMP_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 281
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "SPLIT_TICKET"

    const/16 v14, 0x19

    const/16 v15, 0x19

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->SPLIT_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 287
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "MOVE_TICKET"

    const/16 v14, 0x1a

    const/16 v15, 0x1a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->MOVE_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 293
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ASSIGN_TICKET"

    const/16 v14, 0x1b

    const/16 v15, 0x1b

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ASSIGN_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 299
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "EDIT_TICKET_SEATS"

    const/16 v14, 0x1c

    const/16 v15, 0x1c

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_SEATS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 305
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "EDIT_TICKET_ALLERGY"

    const/16 v14, 0x1d

    const/16 v15, 0x1d

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_ALLERGY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 311
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "CUSTOM_DISCOUNT"

    const/16 v14, 0x1e

    const/16 v15, 0x1e

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_DISCOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 317
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "DINING_OPTION_CATEGORY"

    const/16 v14, 0x1f

    const/16 v15, 0x1f

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DINING_OPTION_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 323
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ORDER_HISTORY"

    const/16 v14, 0x20

    const/16 v15, 0x20

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ORDER_HISTORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 329
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "CUSTOM_GIFT_CARD"

    const/16 v14, 0x21

    const/16 v15, 0x21

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_GIFT_CARD:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 335
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "OPEN_ALL_DRAWERS"

    const/16 v14, 0x22

    const/16 v15, 0x22

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->OPEN_ALL_DRAWERS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 341
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "PAY_IN_OUT"

    const/16 v14, 0x23

    const/16 v15, 0x23

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->PAY_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 346
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "LINE"

    const/16 v14, 0x24

    const/16 v15, 0x24

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->LINE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 351
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "SHAPE"

    const/16 v14, 0x25

    const/16 v15, 0x25

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->SHAPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 356
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "TEXT_LABEL"

    const/16 v14, 0x26

    const/16 v15, 0x26

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->TEXT_LABEL:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 362
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "AUTO_GRATUITY"

    const/16 v14, 0x27

    const/16 v15, 0x27

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->AUTO_GRATUITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 367
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ALL_SERVICES"

    const/16 v14, 0x28

    const/16 v15, 0x28

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 372
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "ALL_ITEMS_AND_SERVICES"

    const/16 v14, 0x29

    const/16 v15, 0x29

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS_AND_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 379
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const-string v13, "COUNT"

    const/16 v14, 0x2a

    const/16 v15, 0x2a

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/api/items/Placeholder$PlaceholderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 174
    sget-object v13, Lcom/squareup/api/items/Placeholder$PlaceholderType;->UNKNOWN:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->REWARDS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COUPONS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_AMOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CALCULATOR:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_2:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_3:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_4:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_5:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_6:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_7:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_8:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_9:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_QUANTITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CLOCK_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ISSUE_REFUND:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADJUST_TIP:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COMP_VOID:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADD_CUSTOMER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_NAME_AND_NOTES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->VOID_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COMP_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->SPLIT_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->MOVE_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ASSIGN_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_SEATS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_ALLERGY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_DISCOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DINING_OPTION_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ORDER_HISTORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_GIFT_CARD:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->OPEN_ALL_DRAWERS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->PAY_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->LINE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->SHAPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->TEXT_LABEL:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->AUTO_GRATUITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS_AND_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->$VALUES:[Lcom/squareup/api/items/Placeholder$PlaceholderType;

    .line 381
    new-instance v0, Lcom/squareup/api/items/Placeholder$PlaceholderType$ProtoAdapter_PlaceholderType;

    invoke-direct {v0}, Lcom/squareup/api/items/Placeholder$PlaceholderType$ProtoAdapter_PlaceholderType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 385
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 386
    iput p3, p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Placeholder$PlaceholderType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 436
    :pswitch_0
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 435
    :pswitch_1
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS_AND_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 434
    :pswitch_2
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_SERVICES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 433
    :pswitch_3
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->AUTO_GRATUITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 432
    :pswitch_4
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->TEXT_LABEL:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 431
    :pswitch_5
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->SHAPE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 430
    :pswitch_6
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->LINE:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 429
    :pswitch_7
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->PAY_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 428
    :pswitch_8
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->OPEN_ALL_DRAWERS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 427
    :pswitch_9
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_GIFT_CARD:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 426
    :pswitch_a
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ORDER_HISTORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 425
    :pswitch_b
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DINING_OPTION_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 424
    :pswitch_c
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_DISCOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 423
    :pswitch_d
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_ALLERGY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 422
    :pswitch_e
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_SEATS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 421
    :pswitch_f
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ASSIGN_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 420
    :pswitch_10
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->MOVE_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 419
    :pswitch_11
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->SPLIT_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 418
    :pswitch_12
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COMP_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 417
    :pswitch_13
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->VOID_TICKET:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 416
    :pswitch_14
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->EDIT_TICKET_NAME_AND_NOTES:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 415
    :pswitch_15
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADD_CUSTOMER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 414
    :pswitch_16
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COMP_VOID:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 413
    :pswitch_17
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ADJUST_TIP:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 412
    :pswitch_18
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ISSUE_REFUND:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 411
    :pswitch_19
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CLOCK_IN_OUT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 410
    :pswitch_1a
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_QUANTITY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 409
    :pswitch_1b
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_9:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 408
    :pswitch_1c
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_8:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 407
    :pswitch_1d
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_7:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 406
    :pswitch_1e
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_6:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 405
    :pswitch_1f
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_5:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 404
    :pswitch_20
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_4:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 403
    :pswitch_21
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_3:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 402
    :pswitch_22
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->QUANTITY_2:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 401
    :pswitch_23
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CALCULATOR:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 400
    :pswitch_24
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->CUSTOM_AMOUNT:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 399
    :pswitch_25
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 398
    :pswitch_26
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->COUPONS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 397
    :pswitch_27
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 396
    :pswitch_28
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->REWARDS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 395
    :pswitch_29
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    .line 394
    :pswitch_2a
    sget-object p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->UNKNOWN:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2a
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Placeholder$PlaceholderType;
    .locals 1

    .line 174
    const-class v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Placeholder$PlaceholderType;
    .locals 1

    .line 174
    sget-object v0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->$VALUES:[Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Placeholder$PlaceholderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Placeholder$PlaceholderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 443
    iget v0, p0, Lcom/squareup/api/items/Placeholder$PlaceholderType;->value:I

    return v0
.end method
