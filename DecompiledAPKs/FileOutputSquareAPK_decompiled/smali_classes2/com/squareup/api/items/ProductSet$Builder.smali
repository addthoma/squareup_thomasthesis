.class public final Lcom/squareup/api/items/ProductSet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProductSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ProductSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/ProductSet;",
        "Lcom/squareup/api/items/ProductSet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public all_products:Ljava/lang/Boolean;

.field public custom_amounts:Ljava/lang/Boolean;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public products_all:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public products_any:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public quantity_exact:Ljava/lang/Integer;

.field public quantity_max:Ljava/lang/Integer;

.field public quantity_min:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 243
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 244
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ProductSet$Builder;->products_any:Ljava/util/List;

    .line 245
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/ProductSet$Builder;->products_all:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public all_products(Ljava/lang/Boolean;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 320
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->all_products:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/ProductSet;
    .locals 12

    .line 334
    new-instance v11, Lcom/squareup/api/items/ProductSet;

    iget-object v1, p0, Lcom/squareup/api/items/ProductSet$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/ProductSet$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/ProductSet$Builder;->products_any:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/api/items/ProductSet$Builder;->products_all:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_exact:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_min:Ljava/lang/Integer;

    iget-object v7, p0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_max:Ljava/lang/Integer;

    iget-object v8, p0, Lcom/squareup/api/items/ProductSet$Builder;->all_products:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/api/items/ProductSet$Builder;->custom_amounts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/api/items/ProductSet;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 224
    invoke-virtual {p0}, Lcom/squareup/api/items/ProductSet$Builder;->build()Lcom/squareup/api/items/ProductSet;

    move-result-object v0

    return-object v0
.end method

.method public custom_amounts(Ljava/lang/Boolean;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 328
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->custom_amounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 249
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 257
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public products_all(Ljava/util/List;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/ProductSet$Builder;"
        }
    .end annotation

    .line 282
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 283
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->products_all:Ljava/util/List;

    return-object p0
.end method

.method public products_any(Ljava/util/List;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/ProductSet$Builder;"
        }
    .end annotation

    .line 270
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 271
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->products_any:Ljava/util/List;

    return-object p0
.end method

.method public quantity_exact(Ljava/lang/Integer;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_exact:Ljava/lang/Integer;

    return-object p0
.end method

.method public quantity_max(Ljava/lang/Integer;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 311
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_max:Ljava/lang/Integer;

    return-object p0
.end method

.method public quantity_min(Ljava/lang/Integer;)Lcom/squareup/api/items/ProductSet$Builder;
    .locals 0

    .line 303
    iput-object p1, p0, Lcom/squareup/api/items/ProductSet$Builder;->quantity_min:Ljava/lang/Integer;

    return-object p0
.end method
