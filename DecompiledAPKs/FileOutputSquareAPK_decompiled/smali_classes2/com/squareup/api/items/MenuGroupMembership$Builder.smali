.class public final Lcom/squareup/api/items/MenuGroupMembership$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MenuGroupMembership.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/MenuGroupMembership;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/MenuGroupMembership;",
        "Lcom/squareup/api/items/MenuGroupMembership$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public member:Lcom/squareup/api/sync/ObjectId;

.field public ordinal:Ljava/lang/Integer;

.field public parent_menu_group:Lcom/squareup/api/sync/ObjectId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/MenuGroupMembership;
    .locals 7

    .line 174
    new-instance v6, Lcom/squareup/api/items/MenuGroupMembership;

    iget-object v1, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->parent_menu_group:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->member:Lcom/squareup/api/sync/ObjectId;

    iget-object v4, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->ordinal:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/MenuGroupMembership;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/sync/ObjectId;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/api/items/MenuGroupMembership$Builder;->build()Lcom/squareup/api/items/MenuGroupMembership;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/MenuGroupMembership$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public member(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/MenuGroupMembership$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->member:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/MenuGroupMembership$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public parent_menu_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/MenuGroupMembership$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/api/items/MenuGroupMembership$Builder;->parent_menu_group:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method
