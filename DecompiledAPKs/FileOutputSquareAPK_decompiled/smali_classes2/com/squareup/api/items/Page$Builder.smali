.class public final Lcom/squareup/api/items/Page$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Page.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Page;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Page;",
        "Lcom/squareup/api/items/Page$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public layout:Lcom/squareup/api/items/PageLayout;

.field public name:Ljava/lang/String;

.field public page_index:Ljava/lang/Integer;

.field public tag:Lcom/squareup/api/sync/ObjectId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 152
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/Page;
    .locals 8

    .line 194
    new-instance v7, Lcom/squareup/api/items/Page;

    iget-object v1, p0, Lcom/squareup/api/items/Page$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/Page$Builder;->page_index:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/api/items/Page$Builder;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/items/Page$Builder;->layout:Lcom/squareup/api/items/PageLayout;

    iget-object v5, p0, Lcom/squareup/api/items/Page$Builder;->tag:Lcom/squareup/api/sync/ObjectId;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/Page;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lcom/squareup/api/items/PageLayout;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 141
    invoke-virtual {p0}, Lcom/squareup/api/items/Page$Builder;->build()Lcom/squareup/api/items/Page;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/api/items/Page$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public layout(Lcom/squareup/api/items/PageLayout;)Lcom/squareup/api/items/Page$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/api/items/Page$Builder;->layout:Lcom/squareup/api/items/PageLayout;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Page$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/api/items/Page$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public page_index(Ljava/lang/Integer;)Lcom/squareup/api/items/Page$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/api/items/Page$Builder;->page_index:Ljava/lang/Integer;

    return-object p0
.end method

.method public tag(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Page$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/api/items/Page$Builder;->tag:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method
