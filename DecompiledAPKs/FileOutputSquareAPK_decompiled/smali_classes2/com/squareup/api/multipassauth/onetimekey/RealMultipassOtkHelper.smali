.class public final Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;
.super Ljava/lang/Object;
.source "RealMultipassOtkHelper.kt"

# interfaces
.implements Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealMultipassOtkHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealMultipassOtkHelper.kt\ncom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper\n*L\n1#1,104:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\nH\u0016J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\nH\u0002J\u0008\u0010\u0015\u001a\u00020\u0016H\u0002J\u0008\u0010\u0017\u001a\u00020\u0018H\u0002J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\nH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
        "multipassService",
        "Lcom/squareup/api/multipassauth/MultipassService;",
        "sessionIdPIIProvider",
        "Lcom/squareup/account/SessionIdPIIProvider;",
        "server",
        "Lcom/squareup/http/Server;",
        "(Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/http/Server;)V",
        "HTTPS_SCHEME",
        "",
        "OTK_PATH",
        "OTK_RETURN_TO_QUERY_PARAM",
        "authAndRedirectToUrl",
        "Lio/reactivex/Single;",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
        "targetUrl",
        "buildOtkUri",
        "Landroid/net/Uri;",
        "targetUri",
        "otk",
        "clientSessionToken",
        "Lcom/squareup/protos/multipass/service/ClientSessionToken;",
        "deviceDetails",
        "Lcom/squareup/protos/multipass/service/DeviceDetails;",
        "encodeSessionToken",
        "Lokio/ByteString;",
        "sessionToken",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final HTTPS_SCHEME:Ljava/lang/String;

.field private final OTK_PATH:Ljava/lang/String;

.field private final OTK_RETURN_TO_QUERY_PARAM:Ljava/lang/String;

.field private final multipassService:Lcom/squareup/api/multipassauth/MultipassService;

.field private final server:Lcom/squareup/http/Server;

.field private final sessionIdPIIProvider:Lcom/squareup/account/SessionIdPIIProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/account/SessionIdPIIProvider;Lcom/squareup/http/Server;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "multipassService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionIdPIIProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "server"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->multipassService:Lcom/squareup/api/multipassauth/MultipassService;

    iput-object p2, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->sessionIdPIIProvider:Lcom/squareup/account/SessionIdPIIProvider;

    iput-object p3, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->server:Lcom/squareup/http/Server;

    const-string p1, "https"

    .line 26
    iput-object p1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->HTTPS_SCHEME:Ljava/lang/String;

    const-string p1, "session/otk"

    .line 27
    iput-object p1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->OTK_PATH:Ljava/lang/String;

    const-string p1, "return_to"

    .line 28
    iput-object p1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->OTK_RETURN_TO_QUERY_PARAM:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$buildOtkUri(Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->buildOtkUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    return-object p0
.end method

.method private final buildOtkUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .line 72
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->HTTPS_SCHEME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->OTK_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p2

    .line 76
    iget-object v0, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->OTK_RETURN_TO_QUERY_PARAM:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, v0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    const-string p2, "Uri.Builder()\n        .s\u2026tPath())\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final clientSessionToken()Lcom/squareup/protos/multipass/service/ClientSessionToken;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->sessionIdPIIProvider:Lcom/squareup/account/SessionIdPIIProvider;

    invoke-interface {v0}, Lcom/squareup/account/SessionIdPIIProvider;->getSessionIdPII()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sessionIdPIIProvider.getSessionIdPII()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->encodeSessionToken(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;-><init>()V

    .line 83
    invoke-virtual {v1, v0}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->value(Lokio/ByteString;)Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/ClientSessionToken$Builder;->build()Lcom/squareup/protos/multipass/service/ClientSessionToken;

    move-result-object v0

    const-string v1, "ClientSessionToken.Build\u2026onToken)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final deviceDetails()Lcom/squareup/protos/multipass/service/DeviceDetails;
    .locals 2

    .line 88
    new-instance v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;-><init>()V

    .line 89
    sget-object v1, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;->WEB:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->type(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object v0

    const-string v1, "DeviceDetails.Builder()\n\u2026ype(WEB)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final encodeSessionToken(Ljava/lang/String;)Lokio/ByteString;
    .locals 15

    .line 97
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x4

    rsub-int/lit8 v0, v0, 0x4

    .line 100
    sget-object v1, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "="

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3, v0}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x5f

    const/16 v5, 0x2f

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    .line 99
    invoke-static/range {v3 .. v8}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    const/16 v10, 0x2d

    const/16 v11, 0x2b

    const/4 v12, 0x0

    const/4 v13, 0x4

    const/4 v14, 0x0

    .line 100
    invoke-static/range {v9 .. v14}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;CCZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-virtual {v1, v0}, Lokio/ByteString$Companion;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public authAndRedirectToUrl(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
            ">;"
        }
    .end annotation

    const-string v0, "targetUrl"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 34
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    const-string v1, "targetUri?.getHost() ?: \"\""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->server:Lcom/squareup/http/Server;

    invoke-virtual {v1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    .line 38
    iget-object v1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->server:Lcom/squareup/http/Server;

    invoke-virtual {v1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/http/Endpoints;->endpointsEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;-><init>()V

    .line 43
    invoke-direct {p0}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->clientSessionToken()Lcom/squareup/protos/multipass/service/ClientSessionToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id(Lcom/squareup/protos/multipass/service/ClientSessionToken;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    move-result-object v0

    .line 44
    invoke-direct {p0}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->deviceDetails()Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details(Lcom/squareup/protos/multipass/service/DeviceDetails;)Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->build()Lcom/squareup/protos/multipass/service/ClientCredentials;

    move-result-object v0

    const-string v1, "ClientCredentials.Builde\u2026tails())\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v1, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;-><init>()V

    .line 48
    invoke-virtual {v1, v0}, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;->client_credentials(Lcom/squareup/protos/multipass/service/ClientCredentials;)Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;->build()Lcom/squareup/protos/multipass/service/CreateOtkRequest;

    move-result-object v0

    const-string v1, "CreateOtkRequest.Builder\u2026entials)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v1, p0, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;->multipassService:Lcom/squareup/api/multipassauth/MultipassService;

    invoke-interface {v1, v0}, Lcom/squareup/api/multipassauth/MultipassService;->createOtk(Lcom/squareup/protos/multipass/service/CreateOtkRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper$authAndRedirectToUrl$1;-><init>(Lcom/squareup/api/multipassauth/onetimekey/RealMultipassOtkHelper;Landroid/net/Uri;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "multipassService.createO\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 39
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Host of targetUrl passed in does not match current server environment"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
