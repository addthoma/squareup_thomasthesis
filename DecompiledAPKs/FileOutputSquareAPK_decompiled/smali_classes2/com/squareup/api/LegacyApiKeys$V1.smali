.class public Lcom/squareup/api/LegacyApiKeys$V1;
.super Ljava/lang/Object;
.source "LegacyApiKeys.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/LegacyApiKeys;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "V1"
.end annotation


# static fields
.field public static final ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED:Ljava/lang/String; = "com.squareup.register.ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

.field public static final ERROR_DISABLED:Ljava/lang/String; = "com.squareup.register.ERROR_DISABLED"

.field public static final ERROR_GIFT_CARDS_NOT_SUPPORTED:Ljava/lang/String; = "com.squareup.register.ERROR_GIFT_CARDS_NOT_SUPPORTED"

.field public static final ERROR_ILLEGAL_LOCATION_ID:Ljava/lang/String; = "com.squareup.register.ERROR_ILLEGAL_LOCATION_ID"

.field public static final ERROR_INSUFFICIENT_CARD_BALANCE:Ljava/lang/String; = "com.squareup.register.ERROR_INSUFFICIENT_CARD_BALANCE"

.field public static final ERROR_INVALID_CUSTOMER_ID:Ljava/lang/String; = "com.squareup.register.INVALID_CUSTOMER_ID"

.field public static final ERROR_INVALID_REQUEST:Ljava/lang/String; = "com.squareup.register.ERROR_INVALID_REQUEST"

.field public static final ERROR_NO_EMPLOYEE_LOGGED_IN:Ljava/lang/String; = "com.squareup.register.ERROR_NO_EMPLOYEE_LOGGED_IN"

.field public static final ERROR_NO_NETWORK:Ljava/lang/String; = "com.squareup.register.ERROR_NO_NETWORK"

.field public static final ERROR_NO_RESULT:Ljava/lang/String; = "com.squareup.register.ERROR_NO_RESULT"

.field public static final ERROR_TRANSACTION_ALREADY_IN_PROGRESS:Ljava/lang/String; = "com.squareup.register.ERROR_TRANSACTION_ALREADY_IN_PROGRESS"

.field public static final ERROR_TRANSACTION_CANCELED:Ljava/lang/String; = "com.squareup.register.ERROR_TRANSACTION_CANCELED"

.field public static final ERROR_UNAUTHORIZED_CLIENT_ID:Ljava/lang/String; = "com.squareup.register.ERROR_UNAUTHORIZED_CLIENT_ID"

.field public static final ERROR_UNEXPECTED:Ljava/lang/String; = "com.squareup.register.ERROR_UNEXPECTED"

.field public static final ERROR_UNSUPPORTED_API_VERSION:Ljava/lang/String; = "com.squareup.register.UNSUPPORTED_API_VERSION"

.field public static final ERROR_USER_NOT_ACTIVATED:Ljava/lang/String; = "com.squareup.register.ERROR_USER_NOT_ACTIVATED"

.field public static final ERROR_USER_NOT_LOGGED_IN:Ljava/lang/String; = "com.squareup.register.ERROR_USER_NOT_LOGGED_IN"

.field public static final EXTRA_API_VERSION:Ljava/lang/String; = "com.squareup.register.API_VERSION"

.field public static final EXTRA_AUTO_RETURN_TIMEOUT_MS:Ljava/lang/String; = "com.squareup.register.AUTO_RETURN_TIMEOUT_MS"

.field public static final EXTRA_CURRENCY_CODE:Ljava/lang/String; = "com.squareup.register.CURRENCY_CODE"

.field public static final EXTRA_CUSTOMER_ID:Ljava/lang/String; = "com.squareup.register.CUSTOMER_ID"

.field public static final EXTRA_LOCATION_ID:Ljava/lang/String; = "com.squareup.register.LOCATION_ID"

.field public static final EXTRA_NOTE:Ljava/lang/String; = "com.squareup.register.NOTE"

.field public static final EXTRA_REGISTER_CLIENT_ID:Ljava/lang/String; = "com.squareup.register.CLIENT_ID"

.field public static final EXTRA_REQUEST_METADATA:Ljava/lang/String; = "com.squareup.register.REQUEST_METADATA"

.field public static final EXTRA_SDK_VERSION:Ljava/lang/String; = "com.squareup.register.SDK_VERSION"

.field public static final EXTRA_TENDER_CARD:Ljava/lang/String; = "com.squareup.register.TENDER_CARD"

.field public static final EXTRA_TENDER_CARD_ON_FILE:Ljava/lang/String; = "com.squareup.register.TENDER_CARD_ON_FILE"

.field public static final EXTRA_TENDER_CASH:Ljava/lang/String; = "com.squareup.register.TENDER_CASH"

.field public static final EXTRA_TENDER_OTHER:Ljava/lang/String; = "com.squareup.register.TENDER_OTHER"

.field public static final EXTRA_TENDER_TYPES:Ljava/lang/String; = "com.squareup.register.TENDER_TYPES"

.field public static final EXTRA_TOTAL_AMOUNT:Ljava/lang/String; = "com.squareup.register.TOTAL_AMOUNT"

.field public static final EXTRA_WEB_CALLBACK_URI:Ljava/lang/String; = "com.squareup.register.WEB_CALLBACK_URI"

.field public static final INTENT_ACTION_CHARGE:Ljava/lang/String; = "com.squareup.register.action.CHARGE"

.field private static final NAMESPACE:Ljava/lang/String; = "com.squareup.register."

.field public static final NOTE_MAX_LENGTH:I = 0x1f4

.field public static final RESULT_CLIENT_TRANSACTION_ID:Ljava/lang/String; = "com.squareup.register.CLIENT_TRANSACTION_ID"

.field public static final RESULT_ERROR_CODE:Ljava/lang/String; = "com.squareup.register.ERROR_CODE"

.field public static final RESULT_ERROR_DESCRIPTION:Ljava/lang/String; = "com.squareup.register.ERROR_DESCRIPTION"

.field public static final RESULT_REQUEST_METADATA:Ljava/lang/String; = "com.squareup.register.REQUEST_METADATA"

.field public static final RESULT_SERVER_TRANSACTION_ID:Ljava/lang/String; = "com.squareup.register.SERVER_TRANSACTION_ID"


# instance fields
.field final synthetic this$0:Lcom/squareup/api/LegacyApiKeys;


# direct methods
.method public constructor <init>(Lcom/squareup/api/LegacyApiKeys;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/api/LegacyApiKeys$V1;->this$0:Lcom/squareup/api/LegacyApiKeys;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
