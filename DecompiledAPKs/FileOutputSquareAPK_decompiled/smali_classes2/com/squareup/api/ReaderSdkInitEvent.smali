.class public Lcom/squareup/api/ReaderSdkInitEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "ReaderSdkInitEvent.java"


# instance fields
.field public final apk_byte_size:J

.field public final apk_package_name:Ljava/lang/String;

.field public final apk_version_code:I

.field public final apk_version_name:Ljava/lang/String;

.field public final bin_id:Ljava/lang/String;

.field public final debuggable:Z

.field public final init_duration_ms:J

.field public final installer_package:Ljava/lang/String;

.field public final is_flutter:Z

.field public final is_react_native:Z

.field public final min_sdk_version:J

.field public final target_sdk_version:J

.field public final uses_androidx:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;J)V
    .locals 10

    const-string v0, "unknown"

    .line 34
    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->READER_SDK_INIT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/api/ReaderSdkInitEvent;->apk_package_name:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 40
    :try_start_0
    iget-object v4, p0, Lcom/squareup/api/ReaderSdkInitEvent;->apk_package_name:Ljava/lang/String;

    invoke-virtual {v1, v4, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 41
    iget-object v5, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :try_start_1
    iget v4, v4, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-object v5, v0

    :catch_1
    const/4 v4, -0x1

    .line 45
    :goto_0
    iput-object v5, p0, Lcom/squareup/api/ReaderSdkInitEvent;->apk_version_name:Ljava/lang/String;

    .line 46
    iput v4, p0, Lcom/squareup/api/ReaderSdkInitEvent;->apk_version_code:I

    const-wide/16 v4, -0x1

    .line 53
    :try_start_2
    iget-object v6, p0, Lcom/squareup/api/ReaderSdkInitEvent;->apk_package_name:Ljava/lang/String;

    invoke-virtual {v1, v6, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 54
    iget v7, v6, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3

    .line 55
    :try_start_3
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x18

    if-lt v8, v9, :cond_0

    .line 56
    iget v2, v6, Landroid/content/pm/ApplicationInfo;->minSdkVersion:I

    .line 58
    :cond_0
    iget v8, v6, Landroid/content/pm/ApplicationInfo;->flags:I

    const/4 v9, 0x2

    and-int/2addr v8, v9

    if-ne v8, v9, :cond_1

    const/4 v3, 0x1

    .line 59
    :cond_1
    new-instance v8, Ljava/io/File;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    invoke-direct {v8, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v4
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    nop

    goto :goto_1

    :catch_3
    const/4 v7, -0x1

    .line 63
    :goto_1
    iput-wide v4, p0, Lcom/squareup/api/ReaderSdkInitEvent;->apk_byte_size:J

    .line 64
    sget-object v4, Lcom/squareup/sdk/reader/Client;->BIN_ID:Ljava/lang/String;

    iput-object v4, p0, Lcom/squareup/api/ReaderSdkInitEvent;->bin_id:Ljava/lang/String;

    int-to-long v4, v7

    .line 65
    iput-wide v4, p0, Lcom/squareup/api/ReaderSdkInitEvent;->target_sdk_version:J

    int-to-long v4, v2

    .line 66
    iput-wide v4, p0, Lcom/squareup/api/ReaderSdkInitEvent;->min_sdk_version:J

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 71
    :goto_2
    iput-object v0, p0, Lcom/squareup/api/ReaderSdkInitEvent;->installer_package:Ljava/lang/String;

    .line 72
    iput-boolean v3, p0, Lcom/squareup/api/ReaderSdkInitEvent;->debuggable:Z

    .line 73
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcom/squareup/api/ReaderSdkInitEvent;->init_duration_ms:J

    const-string p2, "libreactnativejni.so"

    .line 74
    invoke-direct {p0, p1, p2}, Lcom/squareup/api/ReaderSdkInitEvent;->containsNativeLibrary(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/api/ReaderSdkInitEvent;->is_react_native:Z

    const-string p1, "io.flutter.plugin.common.PluginRegistry"

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/api/ReaderSdkInitEvent;->hasClass(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/api/ReaderSdkInitEvent;->is_flutter:Z

    const-string p1, "androidx.activity.ComponentActivity"

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/api/ReaderSdkInitEvent;->hasClass(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/api/ReaderSdkInitEvent;->uses_androidx:Z

    return-void
.end method

.method private containsNativeLibrary(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    .line 81
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    iget-object p1, p1, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object p1

    .line 83
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, p1, v2

    .line 84
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    :cond_1
    return v0
.end method

.method private hasClass(Ljava/lang/String;)Z
    .locals 0

    .line 95
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method
