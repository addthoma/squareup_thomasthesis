.class public Lcom/squareup/api/StoreCardCancelledEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "StoreCardCancelledEvent.java"


# instance fields
.field private final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    .line 15
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_STORE_CARD_FAILURE:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 16
    iput-object p2, p0, Lcom/squareup/api/StoreCardCancelledEvent;->sequenceUuid:Ljava/lang/String;

    return-void
.end method
