.class public Lcom/squareup/api/BuyerFlowStartedEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "BuyerFlowStartedEvent.java"


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;J)V
    .locals 6

    .line 10
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_BUYER_STARTED:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    return-void
.end method
