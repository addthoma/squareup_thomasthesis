.class final Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Request"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/rpc/Request;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 434
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/rpc/Request;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/rpc/Request;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 473
    new-instance v0, Lcom/squareup/api/rpc/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Request$Builder;-><init>()V

    .line 474
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 475
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    const/4 v4, 0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x2

    if-eq v3, v4, :cond_4

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    const/16 v4, 0x578

    if-eq v3, v4, :cond_2

    const/16 v4, 0x5dc

    if-eq v3, v4, :cond_1

    const/16 v4, 0x641

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    .line 497
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 492
    :pswitch_0
    sget-object v3, Lcom/squareup/api/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/PutRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->put_request(Lcom/squareup/api/sync/PutRequest;)Lcom/squareup/api/rpc/Request$Builder;

    goto :goto_0

    .line 491
    :pswitch_1
    sget-object v3, Lcom/squareup/api/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/GetRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->get_request(Lcom/squareup/api/sync/GetRequest;)Lcom/squareup/api/rpc/Request$Builder;

    goto :goto_0

    .line 490
    :pswitch_2
    sget-object v3, Lcom/squareup/api/sync/CreateSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/CreateSessionRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->create_session_request(Lcom/squareup/api/sync/CreateSessionRequest;)Lcom/squareup/api/rpc/Request$Builder;

    goto :goto_0

    .line 489
    :pswitch_3
    sget-object v3, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state(Lcom/squareup/api/sync/WritableSessionState;)Lcom/squareup/api/rpc/Request$Builder;

    goto :goto_0

    .line 483
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/api/sync/ApiVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/sync/ApiVersion;

    invoke-virtual {v0, v4}, Lcom/squareup/api/rpc/Request$Builder;->api_version(Lcom/squareup/api/sync/ApiVersion;)Lcom/squareup/api/rpc/Request$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 485
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/rpc/Request$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 480
    :pswitch_5
    sget-object v3, Lcom/squareup/api/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/sync/RequestScope;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->scope(Lcom/squareup/api/sync/RequestScope;)Lcom/squareup/api/rpc/Request$Builder;

    goto :goto_0

    .line 495
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->request_enqueued_at_millis(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;

    goto/16 :goto_0

    .line 494
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->skip_writable_session_state_validation(Ljava/lang/Boolean;)Lcom/squareup/api/rpc/Request$Builder;

    goto/16 :goto_0

    .line 493
    :cond_2
    sget-object v3, Lcom/squareup/protos/inventory/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/inventory/InventoryAdjustment;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request(Lcom/squareup/protos/inventory/InventoryAdjustment;)Lcom/squareup/api/rpc/Request$Builder;

    goto/16 :goto_0

    .line 479
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->method_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    goto/16 :goto_0

    .line 478
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->service_name(Ljava/lang/String;)Lcom/squareup/api/rpc/Request$Builder;

    goto/16 :goto_0

    .line 477
    :cond_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/api/rpc/Request$Builder;->id(Ljava/lang/Long;)Lcom/squareup/api/rpc/Request$Builder;

    goto/16 :goto_0

    .line 501
    :cond_6
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/rpc/Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 502
    invoke-virtual {v0}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x41a
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 432
    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/rpc/Request;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 456
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 457
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 458
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 459
    sget-object v0, Lcom/squareup/api/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    const/16 v2, 0x3e8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 460
    sget-object v0, Lcom/squareup/api/sync/ApiVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 461
    sget-object v0, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    const/16 v2, 0x3ea

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 462
    sget-object v0, Lcom/squareup/api/sync/CreateSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    const/16 v2, 0x41a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 463
    sget-object v0, Lcom/squareup/api/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    const/16 v2, 0x41b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/squareup/api/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    const/16 v2, 0x41c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    const/16 v2, 0x5dc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 466
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    const/16 v2, 0x641

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 467
    sget-object v0, Lcom/squareup/protos/inventory/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    const/16 v2, 0x578

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 468
    invoke-virtual {p2}, Lcom/squareup/api/rpc/Request;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 432
    check-cast p2, Lcom/squareup/api/rpc/Request;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/rpc/Request;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/rpc/Request;)I
    .locals 4

    .line 439
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request;->id:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->service_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 440
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->method_name:Ljava/lang/String;

    const/4 v3, 0x3

    .line 441
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->scope:Lcom/squareup/api/sync/RequestScope;

    const/16 v3, 0x3e8

    .line 442
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/ApiVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->api_version:Lcom/squareup/api/sync/ApiVersion;

    const/16 v3, 0x3e9

    .line 443
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    const/16 v3, 0x3ea

    .line 444
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/CreateSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    const/16 v3, 0x41a

    .line 445
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->get_request:Lcom/squareup/api/sync/GetRequest;

    const/16 v3, 0x41b

    .line 446
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->put_request:Lcom/squareup/api/sync/PutRequest;

    const/16 v3, 0x41c

    .line 447
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->skip_writable_session_state_validation:Ljava/lang/Boolean;

    const/16 v3, 0x5dc

    .line 448
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->request_enqueued_at_millis:Ljava/lang/Long;

    const/16 v3, 0x641

    .line 449
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/inventory/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/rpc/Request;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    const/16 v3, 0x578

    .line 450
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 432
    check-cast p1, Lcom/squareup/api/rpc/Request;

    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;->encodedSize(Lcom/squareup/api/rpc/Request;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/rpc/Request;)Lcom/squareup/api/rpc/Request;
    .locals 2

    .line 507
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request;->newBuilder()Lcom/squareup/api/rpc/Request$Builder;

    move-result-object p1

    .line 508
    iget-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->scope:Lcom/squareup/api/sync/RequestScope;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/sync/RequestScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request$Builder;->scope:Lcom/squareup/api/sync/RequestScope;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/RequestScope;

    iput-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->scope:Lcom/squareup/api/sync/RequestScope;

    .line 509
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/api/sync/WritableSessionState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/WritableSessionState;

    iput-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->writable_session_state:Lcom/squareup/api/sync/WritableSessionState;

    .line 510
    :cond_1
    iget-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/api/sync/CreateSessionRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request$Builder;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/CreateSessionRequest;

    iput-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->create_session_request:Lcom/squareup/api/sync/CreateSessionRequest;

    .line 511
    :cond_2
    iget-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->get_request:Lcom/squareup/api/sync/GetRequest;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/api/sync/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request$Builder;->get_request:Lcom/squareup/api/sync/GetRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/GetRequest;

    iput-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->get_request:Lcom/squareup/api/sync/GetRequest;

    .line 512
    :cond_3
    iget-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->put_request:Lcom/squareup/api/sync/PutRequest;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/api/sync/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request$Builder;->put_request:Lcom/squareup/api/sync/PutRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/sync/PutRequest;

    iput-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->put_request:Lcom/squareup/api/sync/PutRequest;

    .line 513
    :cond_4
    iget-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/inventory/InventoryAdjustment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/inventory/InventoryAdjustment;

    iput-object v0, p1, Lcom/squareup/api/rpc/Request$Builder;->inventory_adjust_request:Lcom/squareup/protos/inventory/InventoryAdjustment;

    .line 514
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 515
    invoke-virtual {p1}, Lcom/squareup/api/rpc/Request$Builder;->build()Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 432
    check-cast p1, Lcom/squareup/api/rpc/Request;

    invoke-virtual {p0, p1}, Lcom/squareup/api/rpc/Request$ProtoAdapter_Request;->redact(Lcom/squareup/api/rpc/Request;)Lcom/squareup/api/rpc/Request;

    move-result-object p1

    return-object p1
.end method
