.class public final enum Lcom/squareup/api/rpc/Error$ServerErrorCode;
.super Ljava/lang/Enum;
.source "Error.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerErrorCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/rpc/Error$ServerErrorCode$ProtoAdapter_ServerErrorCode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/rpc/Error$ServerErrorCode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/rpc/Error$ServerErrorCode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/rpc/Error$ServerErrorCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

.field public static final enum UNKNOWN_METHOD_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

.field public static final enum UNKNOWN_SERVICE_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 238
    new-instance v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const/4 v1, 0x0

    const-string v2, "INTERNAL_SERVER_ERROR"

    const/16 v3, 0x1f4

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/api/rpc/Error$ServerErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 240
    new-instance v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const/4 v2, 0x1

    const-string v3, "UNKNOWN_METHOD_ERROR"

    const/16 v4, 0x2711

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/api/rpc/Error$ServerErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_METHOD_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 242
    new-instance v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const/4 v3, 0x2

    const-string v4, "UNKNOWN_SERVICE_ERROR"

    const/16 v5, 0x2712

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/api/rpc/Error$ServerErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_SERVICE_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 237
    sget-object v4, Lcom/squareup/api/rpc/Error$ServerErrorCode;->INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_METHOD_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_SERVICE_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->$VALUES:[Lcom/squareup/api/rpc/Error$ServerErrorCode;

    .line 244
    new-instance v0, Lcom/squareup/api/rpc/Error$ServerErrorCode$ProtoAdapter_ServerErrorCode;

    invoke-direct {v0}, Lcom/squareup/api/rpc/Error$ServerErrorCode$ProtoAdapter_ServerErrorCode;-><init>()V

    sput-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 248
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 249
    iput p3, p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/rpc/Error$ServerErrorCode;
    .locals 1

    const/16 v0, 0x1f4

    if-eq p0, v0, :cond_2

    const/16 v0, 0x2711

    if-eq p0, v0, :cond_1

    const/16 v0, 0x2712

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 259
    :cond_0
    sget-object p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_SERVICE_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    return-object p0

    .line 258
    :cond_1
    sget-object p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->UNKNOWN_METHOD_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    return-object p0

    .line 257
    :cond_2
    sget-object p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->INTERNAL_SERVER_ERROR:Lcom/squareup/api/rpc/Error$ServerErrorCode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/rpc/Error$ServerErrorCode;
    .locals 1

    .line 237
    const-class v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/rpc/Error$ServerErrorCode;
    .locals 1

    .line 237
    sget-object v0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->$VALUES:[Lcom/squareup/api/rpc/Error$ServerErrorCode;

    invoke-virtual {v0}, [Lcom/squareup/api/rpc/Error$ServerErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/rpc/Error$ServerErrorCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 266
    iget v0, p0, Lcom/squareup/api/rpc/Error$ServerErrorCode;->value:I

    return v0
.end method
