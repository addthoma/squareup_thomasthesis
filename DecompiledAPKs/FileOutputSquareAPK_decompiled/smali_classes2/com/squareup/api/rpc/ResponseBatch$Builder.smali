.class public final Lcom/squareup/api/rpc/ResponseBatch$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ResponseBatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/rpc/ResponseBatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/rpc/ResponseBatch;",
        "Lcom/squareup/api/rpc/ResponseBatch$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error:Lcom/squareup/api/rpc/Error;

.field public response:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/rpc/Response;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 99
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/rpc/ResponseBatch$Builder;->response:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/rpc/ResponseBatch;
    .locals 4

    .line 121
    new-instance v0, Lcom/squareup/api/rpc/ResponseBatch;

    iget-object v1, p0, Lcom/squareup/api/rpc/ResponseBatch$Builder;->response:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/api/rpc/ResponseBatch$Builder;->error:Lcom/squareup/api/rpc/Error;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/rpc/ResponseBatch;-><init>(Ljava/util/List;Lcom/squareup/api/rpc/Error;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/api/rpc/ResponseBatch$Builder;->build()Lcom/squareup/api/rpc/ResponseBatch;

    move-result-object v0

    return-object v0
.end method

.method public error(Lcom/squareup/api/rpc/Error;)Lcom/squareup/api/rpc/ResponseBatch$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/api/rpc/ResponseBatch$Builder;->error:Lcom/squareup/api/rpc/Error;

    return-object p0
.end method

.method public response(Ljava/util/List;)Lcom/squareup/api/rpc/ResponseBatch$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/rpc/Response;",
            ">;)",
            "Lcom/squareup/api/rpc/ResponseBatch$Builder;"
        }
    .end annotation

    .line 106
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 107
    iput-object p1, p0, Lcom/squareup/api/rpc/ResponseBatch$Builder;->response:Ljava/util/List;

    return-object p0
.end method
