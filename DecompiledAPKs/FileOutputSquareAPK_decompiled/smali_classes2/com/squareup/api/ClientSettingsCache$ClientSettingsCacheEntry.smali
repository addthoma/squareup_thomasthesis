.class public Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;
.super Ljava/lang/Object;
.source "ClientSettingsCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ClientSettingsCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClientSettingsCacheEntry"
.end annotation


# instance fields
.field public final clientSettings:Lcom/squareup/server/api/ClientSettings;

.field public final updatedAt:J


# direct methods
.method public constructor <init>(Lcom/squareup/server/api/ClientSettings;J)V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;->clientSettings:Lcom/squareup/server/api/ClientSettings;

    .line 66
    iput-wide p2, p0, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;->updatedAt:J

    return-void
.end method
