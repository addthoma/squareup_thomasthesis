.class public final Lcom/squareup/api/RequestParams;
.super Ljava/lang/Object;
.source "RequestParams.java"


# instance fields
.field public final action:Ljava/lang/String;

.field public final apiVersion:Lcom/squareup/api/ApiVersion;

.field public final clientInfo:Lcom/squareup/api/ClientInfo;

.field public final locationId:Ljava/lang/String;

.field public final saveCardOnFileParams:Lcom/squareup/api/SaveCardOnFileParams;

.field public final sdkVersion:Ljava/lang/String;

.field public final state:Ljava/lang/String;

.field public final transactionParams:Lcom/squareup/api/TransactionParams;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 4

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 36
    invoke-static {v0}, Lcom/squareup/api/ApiVersion;->getVersion(Landroid/content/Intent;)Lcom/squareup/api/ApiVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    .line 37
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    invoke-static {v0, v1}, Lcom/squareup/api/ApiVersion;->upgradeIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;

    move-result-object v0

    .line 38
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 39
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/api/RequestParams;->action:Ljava/lang/String;

    .line 40
    new-instance v1, Lcom/squareup/api/ClientInfo;

    invoke-direct {v1, p1}, Lcom/squareup/api/ClientInfo;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    const-string p1, "com.squareup.pos.SDK_VERSION"

    .line 41
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/RequestParams;->sdkVersion:Ljava/lang/String;

    .line 42
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result p1

    if-eqz p1, :cond_0

    const-string p1, "location_id"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, "com.squareup.pos.LOCATION_ID"

    .line 43
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lcom/squareup/api/RequestParams;->locationId:Ljava/lang/String;

    .line 44
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result p1

    if-eqz p1, :cond_1

    const-string p1, "state"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string p1, "com.squareup.pos.STATE"

    .line 45
    invoke-virtual {v0, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_1
    iput-object p1, p0, Lcom/squareup/api/RequestParams;->state:Ljava/lang/String;

    .line 48
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v1, "com.squareup.pos.action.CHARGE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 49
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 50
    invoke-static {v0}, Lcom/squareup/api/TransactionParams;->fromWebRequest(Landroid/content/Intent;)Lcom/squareup/api/TransactionParams;

    move-result-object p1

    goto :goto_2

    .line 51
    :cond_2
    invoke-static {v0}, Lcom/squareup/api/TransactionParams;->fromNativeRequest(Landroid/content/Intent;)Lcom/squareup/api/TransactionParams;

    move-result-object p1

    :goto_2
    move-object v3, v1

    move-object v1, p1

    move-object p1, v3

    goto :goto_3

    .line 52
    :cond_3
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v2, "com.squareup.pos.action.STORE_CARD"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 53
    invoke-static {v0}, Lcom/squareup/api/SaveCardOnFileParams;->fromNativeRequest(Landroid/content/Intent;)Lcom/squareup/api/SaveCardOnFileParams;

    move-result-object p1

    goto :goto_3

    :cond_4
    move-object p1, v1

    .line 55
    :goto_3
    iput-object v1, p0, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    .line 56
    iput-object p1, p0, Lcom/squareup/api/RequestParams;->saveCardOnFileParams:Lcom/squareup/api/SaveCardOnFileParams;

    return-void
.end method


# virtual methods
.method copyToIntent(Landroid/content/Intent;)V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->action:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v0, v0, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    const-string v1, "com.squareup.pos.CLIENT_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    iget-object v0, v0, Lcom/squareup/api/ApiVersion;->versionString:Ljava/lang/String;

    const-string v1, "com.squareup.pos.API_VERSION"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->locationId:Ljava/lang/String;

    const-string v1, "com.squareup.pos.LOCATION_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->state:Ljava/lang/String;

    const-string v1, "com.squareup.pos.STATE"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v0, v0, Lcom/squareup/api/ClientInfo;->browserApplicationId:Ljava/lang/String;

    const-string v1, "com.android.browser.application_id"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v0, v0, Lcom/squareup/api/ClientInfo;->webCallbackUri:Ljava/lang/String;

    const-string v1, "callback_url"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isChargeRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    invoke-virtual {v0, p1}, Lcom/squareup/api/TransactionParams;->copyToIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 73
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/api/RequestParams;->isStoreCardRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->saveCardOnFileParams:Lcom/squareup/api/SaveCardOnFileParams;

    invoke-virtual {v0, p1}, Lcom/squareup/api/SaveCardOnFileParams;->copyToIntent(Landroid/content/Intent;)V

    :cond_2
    :goto_0
    return-void
.end method

.method isChargeRequest()Z
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isConnectReaderRequest()Z
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->action:Ljava/lang/String;

    const-string v1, "com.squareup.pos.action.CONNECT_READER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method isStoreCardRequest()Z
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->action:Ljava/lang/String;

    const-string v1, "com.squareup.pos.action.STORE_CARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method isWebRequest()Z
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-boolean v0, v0, Lcom/squareup/api/ClientInfo;->isWebRequest:Z

    return v0
.end method
