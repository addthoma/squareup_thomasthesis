.class public final enum Lcom/squareup/api/sync/ApiVersion;
.super Ljava/lang/Enum;
.source "ApiVersion.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/ApiVersion$ProtoAdapter_ApiVersion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/sync/ApiVersion;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/sync/ApiVersion;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/ApiVersion;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum VERSION_0:Lcom/squareup/api/sync/ApiVersion;

.field public static final enum VERSION_1:Lcom/squareup/api/sync/ApiVersion;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 16
    new-instance v0, Lcom/squareup/api/sync/ApiVersion;

    const/4 v1, 0x0

    const-string v2, "VERSION_0"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/sync/ApiVersion;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/ApiVersion;->VERSION_0:Lcom/squareup/api/sync/ApiVersion;

    .line 18
    new-instance v0, Lcom/squareup/api/sync/ApiVersion;

    const/4 v2, 0x1

    const-string v3, "VERSION_1"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/sync/ApiVersion;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/sync/ApiVersion;->VERSION_1:Lcom/squareup/api/sync/ApiVersion;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/sync/ApiVersion;

    .line 10
    sget-object v3, Lcom/squareup/api/sync/ApiVersion;->VERSION_0:Lcom/squareup/api/sync/ApiVersion;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/sync/ApiVersion;->VERSION_1:Lcom/squareup/api/sync/ApiVersion;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/sync/ApiVersion;->$VALUES:[Lcom/squareup/api/sync/ApiVersion;

    .line 20
    new-instance v0, Lcom/squareup/api/sync/ApiVersion$ProtoAdapter_ApiVersion;

    invoke-direct {v0}, Lcom/squareup/api/sync/ApiVersion$ProtoAdapter_ApiVersion;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/ApiVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/squareup/api/sync/ApiVersion;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/sync/ApiVersion;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 34
    :cond_0
    sget-object p0, Lcom/squareup/api/sync/ApiVersion;->VERSION_1:Lcom/squareup/api/sync/ApiVersion;

    return-object p0

    .line 33
    :cond_1
    sget-object p0, Lcom/squareup/api/sync/ApiVersion;->VERSION_0:Lcom/squareup/api/sync/ApiVersion;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/sync/ApiVersion;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/api/sync/ApiVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/sync/ApiVersion;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/sync/ApiVersion;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/api/sync/ApiVersion;->$VALUES:[Lcom/squareup/api/sync/ApiVersion;

    invoke-virtual {v0}, [Lcom/squareup/api/sync/ApiVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/sync/ApiVersion;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 41
    iget v0, p0, Lcom/squareup/api/sync/ApiVersion;->value:I

    return v0
.end method
