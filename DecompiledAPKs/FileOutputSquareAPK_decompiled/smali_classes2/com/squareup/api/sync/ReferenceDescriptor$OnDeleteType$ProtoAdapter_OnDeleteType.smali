.class final Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType$ProtoAdapter_OnDeleteType;
.super Lcom/squareup/wire/EnumAdapter;
.source "ReferenceDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnDeleteType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 185
    const-class v0, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;
    .locals 0

    .line 190
    invoke-static {p1}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;->fromValue(I)Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 183
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType$ProtoAdapter_OnDeleteType;->fromValue(I)Lcom/squareup/api/sync/ReferenceDescriptor$OnDeleteType;

    move-result-object p1

    return-object p1
.end method
