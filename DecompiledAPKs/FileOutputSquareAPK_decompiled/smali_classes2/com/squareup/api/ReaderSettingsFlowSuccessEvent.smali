.class public Lcom/squareup/api/ReaderSettingsFlowSuccessEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "ReaderSettingsFlowSuccessEvent.java"


# instance fields
.field public final reason:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;JZ)V
    .locals 6

    .line 13
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_READER_SETTINGS_SUCCESS:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    if-eqz p5, :cond_0

    const-string p1, "stale"

    .line 15
    iput-object p1, p0, Lcom/squareup/api/ReaderSettingsFlowSuccessEvent;->reason:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p1, "canceled"

    .line 17
    iput-object p1, p0, Lcom/squareup/api/ReaderSettingsFlowSuccessEvent;->reason:Ljava/lang/String;

    :goto_0
    return-void
.end method
