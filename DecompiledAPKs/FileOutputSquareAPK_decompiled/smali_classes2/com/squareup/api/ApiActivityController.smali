.class public Lcom/squareup/api/ApiActivityController;
.super Ljava/lang/Object;
.source "ApiActivityController.java"


# static fields
.field private static final DIALOG_FAILURE_LOGGED:Ljava/lang/String; = "dialogFailureLogged"

.field public static final EXTRA_CONTACT:Ljava/lang/String; = "CONTACT"

.field public static final EXTRA_REQUEST_START_TIME:Ljava/lang/String; = "REQUEST_START_TIME"

.field public static final EXTRA_SEQUENCE_UUID:Ljava/lang/String; = "SEQUENCE_UUID"

.field private static final INVALID_INTENT_ACTION:Ljava/lang/String; = "Invalid intent action received. Intent filters should prevent any unsupported actions from reaching Point of Sale API."

.field private static final REGISTER_LAUNCHED_KEY:Ljava/lang/String; = "registerLaunched"

.field private static final REQUEST_START_TIME:Ljava/lang/String; = "requestStartTime"

.field private static final SEQUENCE_UUID_KEY:Ljava/lang/String; = "SEQUENCE_UUID"


# instance fields
.field private activity:Lcom/squareup/ui/ApiActivity;

.field private final activityListener:Lcom/squareup/ActivityListener;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appDelegate:Lcom/squareup/AppDelegate;

.field private final bus:Lcom/squareup/badbus/BadBus;

.field private clientId:Ljava/lang/String;

.field private clock:Lcom/squareup/util/Clock;

.field private dialogFailureLogged:Z

.field private final isReaderSdk:Z

.field private final liveSubscriptions:Lio/reactivex/disposables/CompositeDisposable;

.field private progressBar:Landroid/widget/ProgressBar;

.field private registerLaunched:Z

.field private requestParams:Lcom/squareup/api/RequestParams;

.field private requestStartTime:J

.field private sequenceUuid:Ljava/lang/String;

.field private subscribed:Z

.field private final uuidGenerator:Lcom/squareup/log/UUIDGenerator;

.field private final validationHolder:Lcom/squareup/api/ApiValidator;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/AppDelegate;Lcom/squareup/api/ApiValidator;Lcom/squareup/ActivityListener;Lcom/squareup/util/Clock;ZLcom/squareup/badbus/BadBus;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/api/ApiActivityController;->liveSubscriptions:Lio/reactivex/disposables/CompositeDisposable;

    .line 110
    iput-object p1, p0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    .line 111
    iput-object p2, p0, Lcom/squareup/api/ApiActivityController;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    .line 112
    iput-object p3, p0, Lcom/squareup/api/ApiActivityController;->appDelegate:Lcom/squareup/AppDelegate;

    .line 113
    iput-object p4, p0, Lcom/squareup/api/ApiActivityController;->validationHolder:Lcom/squareup/api/ApiValidator;

    .line 114
    iput-object p5, p0, Lcom/squareup/api/ApiActivityController;->activityListener:Lcom/squareup/ActivityListener;

    .line 115
    iput-object p6, p0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    .line 116
    iput-boolean p7, p0, Lcom/squareup/api/ApiActivityController;->isReaderSdk:Z

    .line 117
    iput-object p8, p0, Lcom/squareup/api/ApiActivityController;->bus:Lcom/squareup/badbus/BadBus;

    return-void
.end method

.method private static createReceiptScreenStrategy(Lcom/squareup/api/RequestParams;Z)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v1, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 367
    sget-object p0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->READ_FROM_PAYMENT:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    goto :goto_0

    .line 368
    :cond_0
    iget-object p0, p0, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    iget-boolean p0, p0, Lcom/squareup/api/TransactionParams;->skipReceipt:Z

    if-eqz p0, :cond_1

    .line 369
    sget-object p0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SKIP_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    goto :goto_0

    .line 371
    :cond_1
    sget-object p0, Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;->SHOW_RECEIPT_SCREEN:Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    :goto_0
    return-object p0
.end method

.method private static createTipSettings(Lcom/squareup/api/TransactionParams;)Lcom/squareup/settings/server/TipSettings;
    .locals 14

    .line 377
    iget-object v0, p0, Lcom/squareup/api/TransactionParams;->customTipPercentages:[I

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/util/Percentage;

    .line 383
    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    sget-object v2, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    if-eqz v0, :cond_0

    .line 385
    :goto_0
    array-length v1, v0

    if-ge v3, v1, :cond_0

    .line 386
    aget v1, v0, v3

    invoke-static {v1}, Lcom/squareup/util/Percentage;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v1

    invoke-interface {v11, v3, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 390
    :cond_0
    new-instance v0, Lcom/squareup/settings/server/TipSettings;

    iget-boolean v6, p0, Lcom/squareup/api/TransactionParams;->tippingEnabled:Z

    const/4 v7, 0x1

    iget-boolean v8, p0, Lcom/squareup/api/TransactionParams;->showCustomTip:Z

    iget-boolean v9, p0, Lcom/squareup/api/TransactionParams;->showSeparateTipScreen:Z

    sget-object v10, Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;->POST_TAX_TIP_CALCULATION:Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v13}, Lcom/squareup/settings/server/TipSettings;-><init>(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;Lcom/squareup/server/account/protos/Tipping;Z)V

    return-object v0
.end method

.method private finishWithError(Lcom/squareup/api/ApiErrorResult;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    .line 186
    invoke-virtual {v0}, Lcom/squareup/ui/ApiActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p1, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-direct {p0, p1, v0}, Lcom/squareup/api/ApiActivityController;->finishWithError(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V

    return-void
.end method

.method private finishWithError(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V
    .locals 7

    .line 191
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiActivityController;->logRequestFailure(Lcom/squareup/api/ApiErrorResult;)V

    .line 193
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v0}, Lcom/squareup/api/RequestParams;->isWebRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    iget-object v0, v0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    .line 195
    iget-object v3, v0, Lcom/squareup/api/ClientInfo;->browserApplicationId:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/api/ClientInfo;->webCallbackUri:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    iget-object v5, v0, Lcom/squareup/api/RequestParams;->state:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    move-object v1, p1

    move-object v2, p2

    .line 196
    invoke-static/range {v1 .. v6}, Lcom/squareup/api/ApiTransactionController;->createWebErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object p1

    .line 198
    iget-object p2, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    iget-object p2, p2, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    invoke-static {p1, p2}, Lcom/squareup/api/ApiVersion;->downgradeWebResponseIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;

    move-result-object p1

    .line 199
    iget-object p2, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/ApiActivity;->startActivity(Landroid/content/Intent;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p1}, Lcom/squareup/ui/ApiActivity;->finish()V

    goto :goto_0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    iget-object v0, v0, Lcom/squareup/api/RequestParams;->state:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/squareup/api/ApiActivityController;->isReaderSdk:Z

    .line 203
    invoke-static {p1, p2, v0, v1}, Lcom/squareup/api/ApiTransactionController;->createNativeErrorResultIntent(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object p1

    .line 205
    iget-object p2, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    iget-object p2, p2, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    invoke-static {p1, p2}, Lcom/squareup/api/ApiVersion;->downgradeResponseIntent(Landroid/content/Intent;Lcom/squareup/api/ApiVersion;)Landroid/content/Intent;

    move-result-object p1

    .line 206
    iget-object p2, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    const/4 v0, 0x0

    invoke-virtual {p2, v0, p1}, Lcom/squareup/ui/ApiActivity;->setResult(ILandroid/content/Intent;)V

    .line 207
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p1}, Lcom/squareup/ui/ApiActivity;->finish()V

    :goto_0
    return-void
.end method

.method private finishWithError(Lcom/squareup/api/ApiValidationException;)V
    .locals 2

    .line 178
    iget-object v0, p1, Lcom/squareup/api/ApiValidationException;->errorResult:Lcom/squareup/api/ApiErrorResult;

    .line 179
    iget-object v1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {v1}, Lcom/squareup/ui/ApiActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 180
    invoke-virtual {p1, v1}, Lcom/squareup/api/ApiValidationException;->getErrorDescription(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    .line 181
    invoke-direct {p0, v0, p1}, Lcom/squareup/api/ApiActivityController;->finishWithError(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V

    return-void
.end method

.method private isOrphanedRequest(Lcom/squareup/api/ApiErrorResult;)Z
    .locals 1

    .line 246
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_START_METHOD:Lcom/squareup/api/ApiErrorResult;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_WEB_CALLBACK_URI:Lcom/squareup/api/ApiErrorResult;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->UNSUPPORTED_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_API_VERSION:Lcom/squareup/api/ApiErrorResult;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method static synthetic lambda$null$0(Lcom/squareup/jailkeeper/JailKeeper$State;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 222
    sget-object v0, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private launchRegister(Lkotlin/Pair;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/api/RequestParams;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 325
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/RequestParams;

    .line 326
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    .line 327
    invoke-virtual {v0}, Lcom/squareup/api/RequestParams;->isChargeRequest()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    iget-object v1, v0, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    .line 329
    iget-object v2, p0, Lcom/squareup/api/ApiActivityController;->appDelegate:Lcom/squareup/AppDelegate;

    const-class v3, Lcom/squareup/api/ApiValidationLoggedInComponent;

    .line 330
    invoke-interface {v2, v3}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/ApiValidationLoggedInComponent;

    .line 331
    invoke-interface {v2}, Lcom/squareup/api/ApiValidationLoggedInComponent;->transaction()Lcom/squareup/payment/Transaction;

    move-result-object v2

    .line 332
    new-instance v3, Lcom/squareup/payment/TransactionConfig$Builder;

    invoke-direct {v3}, Lcom/squareup/payment/TransactionConfig$Builder;-><init>()V

    iget-object v4, v1, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    .line 333
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/squareup/payment/TransactionConfig$Builder;->amount(J)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v3

    iget-object v4, v1, Lcom/squareup/api/TransactionParams;->note:Ljava/lang/String;

    .line 334
    invoke-virtual {v3, v4}, Lcom/squareup/payment/TransactionConfig$Builder;->note(Ljava/lang/String;)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/squareup/api/ApiActivityController;->isReaderSdk:Z

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    .line 335
    invoke-static {v1}, Lcom/squareup/api/ApiActivityController;->createTipSettings(Lcom/squareup/api/TransactionParams;)Lcom/squareup/settings/server/TipSettings;

    move-result-object v4

    goto :goto_0

    :cond_0
    move-object v4, v5

    :goto_0
    invoke-virtual {v3, v4}, Lcom/squareup/payment/TransactionConfig$Builder;->tipSettings(Lcom/squareup/settings/server/TipSettings;)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/squareup/api/ApiActivityController;->isReaderSdk:Z

    .line 336
    invoke-static {v0, v4}, Lcom/squareup/api/ApiActivityController;->createReceiptScreenStrategy(Lcom/squareup/api/RequestParams;Z)Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/payment/TransactionConfig$Builder;->receiptScreenStrategy(Lcom/squareup/payment/Transaction$ReceiptScreenStrategy;)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v3

    iget-boolean v4, v1, Lcom/squareup/api/TransactionParams;->skipSignature:Z

    .line 337
    invoke-virtual {v3, v4}, Lcom/squareup/payment/TransactionConfig$Builder;->skipSignature(Z)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v4, v4, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    .line 338
    invoke-virtual {v3, v4}, Lcom/squareup/payment/TransactionConfig$Builder;->apiClientId(Ljava/lang/String;)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v3

    iget-boolean v1, v1, Lcom/squareup/api/TransactionParams;->delayCapture:Z

    .line 339
    invoke-virtual {v3, v1}, Lcom/squareup/payment/TransactionConfig$Builder;->delayCapture(Z)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v1

    iget-boolean v3, p0, Lcom/squareup/api/ApiActivityController;->isReaderSdk:Z

    .line 340
    invoke-virtual {v1, v3}, Lcom/squareup/payment/TransactionConfig$Builder;->clearTaxes(Z)Lcom/squareup/payment/TransactionConfig$Builder;

    move-result-object v1

    .line 341
    invoke-virtual {v1}, Lcom/squareup/payment/TransactionConfig$Builder;->build()Lcom/squareup/payment/TransactionConfig;

    move-result-object v1

    .line 342
    invoke-virtual {v2, v1}, Lcom/squareup/payment/Transaction;->setConfig(Lcom/squareup/payment/TransactionConfig;)V

    if-eqz p1, :cond_1

    .line 344
    invoke-virtual {v2, p1, v5, v5}, Lcom/squareup/payment/Transaction;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 348
    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    const-class v3, Lcom/squareup/ui/main/ApiMainActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 349
    invoke-virtual {v0, v1}, Lcom/squareup/api/RequestParams;->copyToIntent(Landroid/content/Intent;)V

    .line 350
    invoke-virtual {v0}, Lcom/squareup/api/RequestParams;->isStoreCardRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CONTACT"

    .line 351
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 353
    :cond_2
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    const-string v0, "SEQUENCE_UUID"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    iget-wide v2, p0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    const-string p1, "REQUEST_START_TIME"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/high16 p1, 0x2000000

    .line 355
    invoke-virtual {v1, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 356
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/ApiActivity;->startActivity(Landroid/content/Intent;)V

    const/4 p1, 0x1

    .line 357
    iput-boolean p1, p0, Lcom/squareup/api/ApiActivityController;->registerLaunched:Z

    .line 358
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p1}, Lcom/squareup/ui/ApiActivity;->useDefaultExitAnimation()V

    .line 359
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {p1}, Lcom/squareup/ui/ApiActivity;->finish()V

    .line 360
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    sget v0, Lcom/squareup/common/bootstrap/R$anim;->api_activity_enter:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/ApiActivity;->overridePendingTransition(II)V

    return-void
.end method

.method private logRequestFailure(Lcom/squareup/api/ApiErrorResult;)V
    .locals 9

    .line 271
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v0}, Lcom/squareup/api/RequestParams;->isConnectReaderRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v8, Lcom/squareup/api/ReaderSettingsFlowFailureEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    iget-object v3, p0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/ApiActivityController;->clientId:Ljava/lang/String;

    iget-wide v6, p0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    move-object v1, v8

    move-object v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/api/ReaderSettingsFlowFailureEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V

    invoke-interface {v0, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v0}, Lcom/squareup/api/RequestParams;->isChargeRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v8, Lcom/squareup/api/TransactionFailureEvent;

    iget-object v2, p0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    iget-object v3, p0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/ApiActivityController;->clientId:Ljava/lang/String;

    iget-wide v6, p0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    move-object v1, v8

    move-object v5, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/api/TransactionFailureEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/ApiErrorResult;J)V

    invoke-interface {v0, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 279
    :cond_1
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {p1}, Lcom/squareup/api/RequestParams;->isStoreCardRequest()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 280
    iget-object p1, p0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v6, Lcom/squareup/api/StoreCardCancelledEvent;

    iget-object v1, p0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    iget-object v2, p0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/ApiActivityController;->clientId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/StoreCardCancelledEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;J)V

    invoke-interface {p1, v6}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void

    .line 283
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid intent action received. Intent filters should prevent any unsupported actions from reaching Point of Sale API."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private showErrorDialog(Lcom/squareup/api/ApiValidationException;)V
    .locals 3

    .line 256
    iget-object p1, p1, Lcom/squareup/api/ApiValidationException;->errorResult:Lcom/squareup/api/ApiErrorResult;

    .line 257
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    .line 258
    invoke-virtual {v0}, Lcom/squareup/ui/ApiActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p1, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Square Register"

    .line 262
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-boolean v1, p0, Lcom/squareup/api/ApiActivityController;->dialogFailureLogged:Z

    if-nez v1, :cond_0

    .line 264
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiActivityController;->logRequestFailure(Lcom/squareup/api/ApiErrorResult;)V

    const/4 v1, 0x1

    .line 265
    iput-boolean v1, p0, Lcom/squareup/api/ApiActivityController;->dialogFailureLogged:Z

    .line 267
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/api/-$$Lambda$v_1FYEjJAxr2dqUbzrSugVCS2c4;

    invoke-direct {v2, v1}, Lcom/squareup/api/-$$Lambda$v_1FYEjJAxr2dqUbzrSugVCS2c4;-><init>(Lcom/squareup/ui/ApiActivity;)V

    invoke-virtual {v1, p1, v0, v2}, Lcom/squareup/ui/ApiActivity;->showInvalidStartDialog(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;Ljava/lang/Runnable;)V

    return-void
.end method

.method private subscribeToValidation()V
    .locals 4

    const/4 v0, 0x1

    .line 212
    iput-boolean v0, p0, Lcom/squareup/api/ApiActivityController;->subscribed:Z

    .line 213
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 214
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->liveSubscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/api/ApiActivityController;->validationHolder:Lcom/squareup/api/ApiValidator;

    new-instance v2, Lcom/squareup/api/-$$Lambda$ApiActivityController$BYiYieZzJKVGqJTano5RSw5cEq0;

    invoke-direct {v2, p0}, Lcom/squareup/api/-$$Lambda$ApiActivityController$BYiYieZzJKVGqJTano5RSw5cEq0;-><init>(Lcom/squareup/api/ApiActivityController;)V

    new-instance v3, Lcom/squareup/api/-$$Lambda$ApiActivityController$j31PROLFbe774b28nZ6Yg3W3UHw;

    invoke-direct {v3, p0}, Lcom/squareup/api/-$$Lambda$ApiActivityController$j31PROLFbe774b28nZ6Yg3W3UHw;-><init>(Lcom/squareup/api/ApiActivityController;)V

    invoke-virtual {v1, v2, v3}, Lcom/squareup/api/ApiValidator;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private unsubscribe()V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->liveSubscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    const/4 v0, 0x0

    .line 321
    iput-boolean v0, p0, Lcom/squareup/api/ApiActivityController;->subscribed:Z

    return-void
.end method


# virtual methods
.method public doBackPressed()V
    .locals 1

    .line 312
    iget-boolean v0, p0, Lcom/squareup/api/ApiActivityController;->registerLaunched:Z

    if-eqz v0, :cond_0

    return-void

    .line 315
    :cond_0
    invoke-direct {p0}, Lcom/squareup/api/ApiActivityController;->unsubscribe()V

    .line 316
    sget-object v0, Lcom/squareup/api/ApiErrorResult;->VALIDATION_CANCELED:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiActivityController;->finishWithError(Lcom/squareup/api/ApiErrorResult;)V

    return-void
.end method

.method public synthetic lambda$null$1$ApiActivityController(Lkotlin/Pair;Lcom/squareup/jailkeeper/JailKeeper$State;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 225
    iget-object p2, p0, Lcom/squareup/api/ApiActivityController;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 226
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiActivityController;->launchRegister(Lkotlin/Pair;)V

    return-void
.end method

.method public synthetic lambda$subscribeToValidation$2$ApiActivityController(Lkotlin/Pair;)V
    .locals 3

    .line 217
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->appDelegate:Lcom/squareup/AppDelegate;

    const-class v1, Lcom/squareup/api/ApiValidationLoggedInComponent;

    .line 218
    invoke-interface {v0, v1}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiValidationLoggedInComponent;

    .line 219
    invoke-interface {v0}, Lcom/squareup/api/ApiValidationLoggedInComponent;->jailKeeper()Lcom/squareup/jailkeeper/JailKeeper;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/squareup/api/ApiActivityController;->bus:Lcom/squareup/badbus/BadBus;

    const-class v2, Lcom/squareup/jailkeeper/JailKeeper$State;

    invoke-virtual {v1, v2}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    .line 221
    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/-$$Lambda$ApiActivityController$4w3ZsQhGWazTPR6kyzn4Y1E_vNQ;->INSTANCE:Lcom/squareup/api/-$$Lambda$ApiActivityController$4w3ZsQhGWazTPR6kyzn4Y1E_vNQ;

    .line 222
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/squareup/api/ApiActivityController;->liveSubscriptions:Lio/reactivex/disposables/CompositeDisposable;

    new-instance v2, Lcom/squareup/api/-$$Lambda$ApiActivityController$-PsFhXGa380AN5OC-PmG7vtJiW8;

    invoke-direct {v2, p0, p1}, Lcom/squareup/api/-$$Lambda$ApiActivityController$-PsFhXGa380AN5OC-PmG7vtJiW8;-><init>(Lcom/squareup/api/ApiActivityController;Lkotlin/Pair;)V

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public synthetic lambda$subscribeToValidation$3$ApiActivityController(Ljava/lang/Throwable;)V
    .locals 3

    .line 230
    instance-of v0, p1, Lcom/squareup/api/ApiValidationException;

    if-eqz v0, :cond_1

    .line 233
    check-cast p1, Lcom/squareup/api/ApiValidationException;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 235
    iget-object v2, p1, Lcom/squareup/api/ApiValidationException;->errorResult:Lcom/squareup/api/ApiErrorResult;

    invoke-virtual {v2}, Lcom/squareup/api/ApiErrorResult;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Api Validation Error: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    iget-object v0, p1, Lcom/squareup/api/ApiValidationException;->errorResult:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p0, v0}, Lcom/squareup/api/ApiActivityController;->isOrphanedRequest(Lcom/squareup/api/ApiErrorResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiActivityController;->showErrorDialog(Lcom/squareup/api/ApiValidationException;)V

    goto :goto_0

    .line 240
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/api/ApiActivityController;->finishWithError(Lcom/squareup/api/ApiValidationException;)V

    :goto_0
    return-void

    .line 231
    :cond_1
    invoke-static {p1}, Lio/reactivex/exceptions/Exceptions;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1
.end method

.method public onCreate(Lcom/squareup/ui/ApiActivity;Landroid/widget/ProgressBar;JLandroid/os/Bundle;)V
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    .line 122
    iput-object v1, v0, Lcom/squareup/api/ApiActivityController;->activity:Lcom/squareup/ui/ApiActivity;

    move-object/from16 v3, p2

    .line 123
    iput-object v3, v0, Lcom/squareup/api/ApiActivityController;->progressBar:Landroid/widget/ProgressBar;

    .line 125
    new-instance v3, Lcom/squareup/api/RequestParams;

    invoke-direct {v3, v1}, Lcom/squareup/api/RequestParams;-><init>(Landroid/app/Activity;)V

    iput-object v3, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    .line 126
    iget-object v3, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    iget-object v3, v3, Lcom/squareup/api/RequestParams;->clientInfo:Lcom/squareup/api/ClientInfo;

    iget-object v3, v3, Lcom/squareup/api/ClientInfo;->clientId:Ljava/lang/String;

    iput-object v3, v0, Lcom/squareup/api/ApiActivityController;->clientId:Ljava/lang/String;

    if-nez v2, :cond_5

    const/4 v2, 0x0

    .line 129
    iput-boolean v2, v0, Lcom/squareup/api/ApiActivityController;->registerLaunched:Z

    .line 130
    iput-boolean v2, v0, Lcom/squareup/api/ApiActivityController;->dialogFailureLogged:Z

    .line 131
    iget-object v3, v0, Lcom/squareup/api/ApiActivityController;->uuidGenerator:Lcom/squareup/log/UUIDGenerator;

    invoke-interface {v3}, Lcom/squareup/log/UUIDGenerator;->randomUUID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    .line 132
    iget-object v3, v0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v3}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v3

    iput-wide v3, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    .line 134
    iget-object v3, v0, Lcom/squareup/api/ApiActivityController;->activityListener:Lcom/squareup/ActivityListener;

    invoke-virtual {v3}, Lcom/squareup/ActivityListener;->getActivityCreatedCount()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v9, 0x0

    :goto_0
    if-eqz v9, :cond_1

    .line 138
    iget-wide v2, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    iget-object v4, v0, Lcom/squareup/api/ApiActivityController;->appDelegate:Lcom/squareup/AppDelegate;

    invoke-interface {v4}, Lcom/squareup/AppDelegate;->getProcessStartupUptime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    goto :goto_1

    .line 140
    :cond_1
    iget-wide v2, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    sub-long v2, v2, p3

    :goto_1
    move-wide v10, v2

    .line 143
    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v2}, Lcom/squareup/api/RequestParams;->isConnectReaderRequest()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 144
    iget-object v1, v0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/api/ReaderSettingsEvent;

    iget-object v6, v0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    iget-object v7, v0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/api/ApiActivityController;->clientId:Ljava/lang/String;

    iget-wide v12, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    move-object v5, v2

    invoke-direct/range {v5 .. v13}, Lcom/squareup/api/ReaderSettingsEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;ZJJ)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_2

    .line 147
    :cond_2
    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v2}, Lcom/squareup/api/RequestParams;->isChargeRequest()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 148
    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/api/NewTransactionEvent;

    iget-object v6, v0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    .line 149
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/ApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    iget-wide v12, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    iget-object v14, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    move-object v5, v3

    invoke-direct/range {v5 .. v14}, Lcom/squareup/api/NewTransactionEvent;-><init>(Lcom/squareup/util/Clock;Landroid/content/Intent;Ljava/lang/String;ZJJLcom/squareup/api/RequestParams;)V

    .line 148
    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_2

    .line 151
    :cond_3
    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v2}, Lcom/squareup/api/RequestParams;->isStoreCardRequest()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 152
    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/api/NewStoreCardEvent;

    iget-object v6, v0, Lcom/squareup/api/ApiActivityController;->clock:Lcom/squareup/util/Clock;

    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/ApiActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    iget-wide v12, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    iget-object v14, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    move-object v5, v3

    invoke-direct/range {v5 .. v14}, Lcom/squareup/api/NewStoreCardEvent;-><init>(Lcom/squareup/util/Clock;Landroid/content/Intent;Ljava/lang/String;ZJJLcom/squareup/api/RequestParams;)V

    .line 152
    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 158
    :goto_2
    iget-object v1, v0, Lcom/squareup/api/ApiActivityController;->validationHolder:Lcom/squareup/api/ApiValidator;

    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v1, v2}, Lcom/squareup/api/ApiValidator;->createValidation(Lcom/squareup/api/RequestParams;)V

    .line 159
    invoke-direct {p0}, Lcom/squareup/api/ApiActivityController;->subscribeToValidation()V

    goto :goto_3

    .line 156
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Invalid intent action received. Intent filters should prevent any unsupported actions from reaching Point of Sale API."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    const-string v1, "registerLaunched"

    .line 161
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/api/ApiActivityController;->registerLaunched:Z

    const-string v1, "dialogFailureLogged"

    .line 162
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/squareup/api/ApiActivityController;->dialogFailureLogged:Z

    const-string v1, "SEQUENCE_UUID"

    .line 163
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    const-wide/16 v3, 0x0

    const-string v1, "requestStartTime"

    .line 164
    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    .line 166
    iget-boolean v1, v0, Lcom/squareup/api/ApiActivityController;->registerLaunched:Z

    if-nez v1, :cond_7

    .line 167
    iget-object v1, v0, Lcom/squareup/api/ApiActivityController;->validationHolder:Lcom/squareup/api/ApiValidator;

    invoke-virtual {v1}, Lcom/squareup/api/ApiValidator;->hasValidation()Z

    move-result v1

    if-nez v1, :cond_6

    .line 169
    iget-object v1, v0, Lcom/squareup/api/ApiActivityController;->validationHolder:Lcom/squareup/api/ApiValidator;

    iget-object v2, v0, Lcom/squareup/api/ApiActivityController;->requestParams:Lcom/squareup/api/RequestParams;

    invoke-virtual {v1, v2}, Lcom/squareup/api/ApiValidator;->createValidation(Lcom/squareup/api/RequestParams;)V

    .line 172
    :cond_6
    invoke-direct {p0}, Lcom/squareup/api/ApiActivityController;->subscribeToValidation()V

    :cond_7
    :goto_3
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .line 300
    invoke-direct {p0}, Lcom/squareup/api/ApiActivityController;->unsubscribe()V

    return-void
.end method

.method public onPause()V
    .locals 0

    .line 296
    invoke-direct {p0}, Lcom/squareup/api/ApiActivityController;->unsubscribe()V

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 306
    iget-boolean v0, p0, Lcom/squareup/api/ApiActivityController;->subscribed:Z

    if-nez v0, :cond_0

    .line 307
    invoke-direct {p0}, Lcom/squareup/api/ApiActivityController;->subscribeToValidation()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 288
    iget-boolean v0, p0, Lcom/squareup/api/ApiActivityController;->registerLaunched:Z

    const-string v1, "registerLaunched"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 289
    iget-boolean v0, p0, Lcom/squareup/api/ApiActivityController;->dialogFailureLogged:Z

    const-string v1, "dialogFailureLogged"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 290
    iget-object v0, p0, Lcom/squareup/api/ApiActivityController;->sequenceUuid:Ljava/lang/String;

    const-string v1, "SEQUENCE_UUID"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-wide v0, p0, Lcom/squareup/api/ApiActivityController;->requestStartTime:J

    const-string v2, "requestStartTime"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void
.end method
