.class public Lcom/squareup/activity/ui/IssueReceiptCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "IssueReceiptCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/ui/IssueReceiptCoordinator$Factory;,
        Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;
    }
.end annotation


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private bottomPanel:Landroid/view/View;

.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private contentView:Landroid/view/View;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private digitalReceiptHintView:Lcom/squareup/widgets/MessageView;

.field private emailDisclaimer:Lcom/squareup/widgets/MessageView;

.field private emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

.field private final emailScrubber:Lcom/squareup/text/EmailScrubber;

.field private final eventHandler:Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

.field private frame:Landroid/view/ViewGroup;

.field private icon:Lcom/squareup/glyph/SquareGlyphView;

.field private final phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

.field private printFormalReceipt:Landroid/widget/TextView;

.field private printReceipt:Landroid/widget/TextView;

.field private final screenData:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/activity/ui/IssueReceiptScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private smsDisclaimer:Lcom/squareup/widgets/MessageView;

.field private smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

.field private title:Lcom/squareup/widgets/ScalingTextView;

.field private topPanel:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lrx/Observable;Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;Lcom/squareup/text/InsertingScrubber;Ljavax/inject/Provider;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lrx/Observable<",
            "Lcom/squareup/activity/ui/IssueReceiptScreenData;",
            ">;",
            "Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;",
            "Lcom/squareup/text/InsertingScrubber;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    .line 141
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->device:Lcom/squareup/util/Device;

    .line 143
    iput-object p3, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->eventHandler:Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

    .line 144
    iput-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->screenData:Lrx/Observable;

    .line 145
    iput-object p4, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    .line 146
    new-instance p1, Lcom/squareup/text/EmailScrubber;

    invoke-direct {p1}, Lcom/squareup/text/EmailScrubber;-><init>()V

    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailScrubber:Lcom/squareup/text/EmailScrubber;

    .line 147
    iput-object p5, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->countryCodeProvider:Ljavax/inject/Provider;

    .line 148
    iput-object p6, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->eventHandler:Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->forceFrameLayoutHeight()V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)Landroid/view/View;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->bottomPanel:Landroid/view/View;

    return-object p0
.end method

.method private animateOut()V
    .locals 7

    .line 355
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 357
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->bottomPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v2, v0, v1

    if-nez v2, :cond_0

    return-void

    :cond_0
    float-to-int v2, v0

    .line 365
    invoke-direct {p0, v2}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->convertTopPanelToFixedHeight(I)V

    const/4 v2, 0x2

    new-array v3, v2, [F

    .line 367
    fill-array-data v3, :array_0

    const-string v4, "alpha"

    invoke-static {v4, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    new-array v4, v2, [F

    const/4 v5, 0x0

    aput v1, v4, v5

    const/4 v1, 0x1

    aput v0, v4, v1

    const-string v0, "translationY"

    .line 368
    invoke-static {v0, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 370
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 371
    iget-object v6, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->bottomPanel:Landroid/view/View;

    new-array v2, v2, [Landroid/animation/PropertyValuesHolder;

    aput-object v3, v2, v5

    aput-object v0, v2, v1

    invoke-static {v6, v2}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    iget-object v2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->topPanel:Landroid/view/View;

    new-array v1, v1, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v1, v5

    invoke-static {v2, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 374
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->contentView:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 375
    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$11;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$11;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 382
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    const-wide/16 v1, 0x12c

    .line 385
    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 386
    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 388
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 408
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 409
    sget v0, Lcom/squareup/activity/R$id;->bottom_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->bottomPanel:Landroid/view/View;

    .line 410
    sget v0, Lcom/squareup/activity/R$id;->email_receipt_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    .line 411
    sget v0, Lcom/squareup/activity/R$id;->email_disclaimer:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailDisclaimer:Lcom/squareup/widgets/MessageView;

    .line 412
    sget v0, Lcom/squareup/activity/R$id;->frame:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->frame:Landroid/view/ViewGroup;

    .line 413
    sget v0, Lcom/squareup/activity/R$id;->glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    .line 414
    sget v0, Lcom/squareup/activity/R$id;->print_receipt_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printReceipt:Landroid/widget/TextView;

    .line 415
    sget v0, Lcom/squareup/activity/R$id;->print_formal_receipt_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printFormalReceipt:Landroid/widget/TextView;

    .line 416
    sget v0, Lcom/squareup/activity/R$id;->content_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->contentView:Landroid/view/View;

    .line 417
    sget v0, Lcom/squareup/activity/R$id;->issue_receipt_subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 418
    sget v0, Lcom/squareup/activity/R$id;->sms_receipt_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/GlyphButtonEditText;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    .line 419
    sget v0, Lcom/squareup/activity/R$id;->sms_disclaimer:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsDisclaimer:Lcom/squareup/widgets/MessageView;

    .line 420
    sget v0, Lcom/squareup/activity/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/ScalingTextView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    .line 421
    sget v0, Lcom/squareup/activity/R$id;->top_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->topPanel:Landroid/view/View;

    .line 422
    sget v0, Lcom/squareup/activity/R$id;->receipt_digital_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->digitalReceiptHintView:Lcom/squareup/widgets/MessageView;

    .line 423
    sget v0, Lcom/squareup/activity/R$id;->switch_language_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private convertTopPanelToFixedHeight(I)V
    .locals 3

    .line 392
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->topPanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 393
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->topPanel:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    mul-int/lit8 v2, p1, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    neg-int p1, p1

    .line 394
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 395
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    const/4 p1, 0x0

    .line 396
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 397
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->topPanel:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private enableClickAnywhereToContinue()V
    .locals 3

    .line 340
    new-instance v0, Lcom/squareup/activity/ui/IssueReceiptCoordinator$10;

    invoke-direct {v0, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$10;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    .line 346
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->frame:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 347
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setClickable(Z)V

    .line 348
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/ScalingTextView;->setClickable(Z)V

    .line 349
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->frame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/ScalingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private forceFrameLayoutHeight()V
    .locals 2

    .line 402
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->frame:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 403
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->contentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 404
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->frame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private receiveScreenData(Lcom/squareup/activity/ui/IssueReceiptScreenData;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 3

    .line 255
    instance-of v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    if-eqz v0, :cond_0

    .line 256
    check-cast p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->updateView(Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;Lcom/squareup/locale/LocaleOverrideFactory;)V

    goto :goto_0

    .line 257
    :cond_0
    instance-of v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;

    if-eqz v0, :cond_1

    .line 258
    check-cast p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->showReceiptFinishedState(Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;Lcom/squareup/util/Res;)V

    :goto_0
    return-void

    .line 260
    :cond_1
    new-instance p2, Ljava/lang/IllegalStateException;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 261
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Wondrous new screen data class: %s"

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private setActionBarConfig(ILcom/squareup/util/Res;)V
    .locals 2

    .line 331
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 332
    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 p2, 0x1

    .line 333
    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->eventHandler:Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/activity/ui/-$$Lambda$PXKwmodrsNbL-xJAf19D3hhsac0;

    invoke-direct {v0, p2}, Lcom/squareup/activity/ui/-$$Lambda$PXKwmodrsNbL-xJAf19D3hhsac0;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;)V

    .line 334
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 336
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p2}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private showReceiptFinishedState(Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;Lcom/squareup/util/Res;)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->iconGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 267
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    iget-boolean v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->iconGlyphVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 269
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    iget v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->titleText:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/ScalingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->titleVisible:Z

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 272
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    invoke-virtual {p2}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->getEditText()Landroid/widget/EditText;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->emailReceiptEnabled:Z

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->setEnabled(Z)V

    .line 273
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    invoke-virtual {p2}, Lcom/squareup/ui/library/GlyphButtonEditText;->getEditText()Landroid/widget/EditText;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/OnScreenRectangleEditText;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->smsReceiptEnabled:Z

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setEnabled(Z)V

    .line 274
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printReceipt:Landroid/widget/TextView;

    iget-boolean p1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;->printReceiptEnabled:Z

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 276
    invoke-direct {p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->enableClickAnywhereToContinue()V

    return-void
.end method

.method private updateText(Lcom/squareup/locale/LocaleFormatter;Lcom/squareup/util/Res;)V
    .locals 2

    .line 309
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/activity/R$drawable;->buyer_language_icon:I

    .line 311
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x0

    .line 310
    invoke-virtual {p1, v0, v1, v1, v1}, Lcom/squareup/marketfont/MarketTextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 313
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/CountryCode;

    .line 314
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsDisclaimer:Lcom/squareup/widgets/MessageView;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->smsReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailDisclaimer:Lcom/squareup/widgets/MessageView;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->emailReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    invoke-static {p1}, Lcom/squareup/address/CountryResources;->generalReceiptDisclaimerId(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 317
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->digitalReceiptHintView:Lcom/squareup/widgets/MessageView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_0

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->digitalReceiptHintView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->digitalReceiptHintView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 324
    :goto_0
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printReceipt:Landroid/widget/TextView;

    sget v0, Lcom/squareup/activity/R$string;->receipt_paper:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printFormalReceipt:Landroid/widget/TextView;

    sget v0, Lcom/squareup/activity/R$string;->receipt_paper_formal:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    sget v0, Lcom/squareup/activity/R$string;->receipt_sms:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/GlyphButtonEditText;->setHint(Ljava/lang/String;)V

    .line 327
    iget-object p1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    sget v0, Lcom/squareup/activity/R$string;->receipt_email:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method private updateView(Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 2

    .line 281
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->bottomPanel:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->inputsVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 282
    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->inputsVisible:Z

    if-nez v0, :cond_0

    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    iget-boolean v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->iconGlyphVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 288
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    iget-boolean v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->titleVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 289
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    iget-boolean v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->titleVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 290
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    iget-boolean v1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->switchLanguageVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 292
    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->updateText(Lcom/squareup/locale/LocaleFormatter;Lcom/squareup/util/Res;)V

    .line 293
    iget v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->upButtonTextId:I

    invoke-virtual {p2}, Lcom/squareup/locale/LocaleOverrideFactory;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    invoke-direct {p0, v0, p2}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->setActionBarConfig(ILcom/squareup/util/Res;)V

    .line 295
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->emailValid:Z

    invoke-virtual {p2, v0}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setButtonEnabled(Z)V

    .line 296
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->emailReceiptVisible:Z

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 298
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->smsValid:Z

    invoke-virtual {p2, v0}, Lcom/squareup/ui/library/GlyphButtonEditText;->setButtonEnabled(Z)V

    .line 299
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->smsReceiptVisible:Z

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 301
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printReceipt:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printReceiptVisible:Z

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 302
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printReceipt:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printReceiptVisible:Z

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 304
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printFormalReceipt:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printFormalReceiptVisible:Z

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 305
    iget-object p2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printFormalReceipt:Landroid/widget/TextView;

    iget-boolean p1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;->printFormalReceiptVisible:Z

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 152
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 153
    invoke-direct {p0, p1}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->bindViews(Landroid/view/View;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->title:Lcom/squareup/widgets/ScalingTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/ScalingTextView;->setFreezesText(Z)V

    .line 157
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setFreezesText(Z)V

    .line 159
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->frame:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    .line 161
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->icon:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->switchLanguageButton:Lcom/squareup/marketfont/MarketTextView;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$1;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$1;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$2;

    iget-object v2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v1, p0, v2, v0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$2;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$3;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$3;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->smsReceipt:Lcom/squareup/ui/library/GlyphButtonEditText;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$4;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$4;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$5;

    iget-object v2, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailScrubber:Lcom/squareup/text/EmailScrubber;

    invoke-direct {v1, p0, v2, v0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$5;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 203
    invoke-interface {v3}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    .line 202
    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setAutoCompleteAdapter(Landroid/widget/ArrayAdapter;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$6;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$6;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->emailReceipt:Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$7;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$7;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/GlyphButtonAutoCompleteEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printReceipt:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$8;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$8;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->printFormalReceipt:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/activity/ui/IssueReceiptCoordinator$9;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator$9;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->eventHandler:Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/activity/ui/-$$Lambda$PXKwmodrsNbL-xJAf19D3hhsac0;

    invoke-direct {v1, v0}, Lcom/squareup/activity/ui/-$$Lambda$PXKwmodrsNbL-xJAf19D3hhsac0;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator$EventHandler;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->screenData:Lrx/Observable;

    new-instance v1, Lcom/squareup/activity/ui/-$$Lambda$IssueReceiptCoordinator$rH9gBzPztrFcJyW_ebNorxXwCM4;

    invoke-direct {v1, p0}, Lcom/squareup/activity/ui/-$$Lambda$IssueReceiptCoordinator$rH9gBzPztrFcJyW_ebNorxXwCM4;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    .line 236
    invoke-virtual {v0, v1}, Lrx/Observable;->scan(Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 244
    iget-object v1, p0, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    .line 245
    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->localeOverrideFactory()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    .line 246
    new-instance v2, Lcom/squareup/activity/ui/-$$Lambda$IssueReceiptCoordinator$LixkmVjhs7FJ8xUV0IWwhDh7V_o;

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/activity/ui/-$$Lambda$IssueReceiptCoordinator$LixkmVjhs7FJ8xUV0IWwhDh7V_o;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;Lrx/Observable;Lrx/Observable;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$IssueReceiptCoordinator(Lcom/squareup/activity/ui/IssueReceiptScreenData;Lcom/squareup/activity/ui/IssueReceiptScreenData;)Lcom/squareup/activity/ui/IssueReceiptScreenData;
    .locals 0

    .line 237
    instance-of p1, p1, Lcom/squareup/activity/ui/IssueReceiptScreenData$IssueReceiptViewData;

    if-eqz p1, :cond_0

    instance-of p1, p2, Lcom/squareup/activity/ui/IssueReceiptScreenData$AnimateOutViewData;

    if-eqz p1, :cond_0

    .line 239
    invoke-direct {p0}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->animateOut()V

    :cond_0
    return-object p2
.end method

.method public synthetic lambda$attach$2$IssueReceiptCoordinator(Lrx/Observable;Lrx/Observable;)Lrx/Subscription;
    .locals 1

    .line 247
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/activity/ui/-$$Lambda$IssueReceiptCoordinator$6kUe711CivbYk5YfbarUraX0CZQ;

    invoke-direct {p2, p0}, Lcom/squareup/activity/ui/-$$Lambda$IssueReceiptCoordinator$6kUe711CivbYk5YfbarUraX0CZQ;-><init>(Lcom/squareup/activity/ui/IssueReceiptCoordinator;)V

    .line 248
    invoke-static {p2}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$IssueReceiptCoordinator(Lcom/squareup/activity/ui/IssueReceiptScreenData;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 249
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/ui/IssueReceiptCoordinator;->receiveScreenData(Lcom/squareup/activity/ui/IssueReceiptScreenData;Lcom/squareup/locale/LocaleOverrideFactory;)V

    return-void
.end method
