.class public abstract Lcom/squareup/activity/AbstractTransactionsHistoryLoader;
.super Ljava/lang/Object;
.source "AbstractTransactionsHistoryLoader.java"

# interfaces
.implements Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;
.implements Lmortar/Scoped;


# instance fields
.field private final billListService:Lcom/squareup/server/bills/BillListServiceHelper;

.field protected final bills:Lcom/squareup/activity/BillsList;

.field private billsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Bill;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private lastError:Lcom/squareup/activity/LoaderError;

.field protected loadToken:Ljava/util/UUID;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private nextPaginationToken:Ljava/lang/String;

.field private final onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field protected final res:Lcom/squareup/util/Res;

.field protected final rpcScheduler:Lio/reactivex/Scheduler;

.field private scope:Lmortar/MortarScope;

.field private state:Lcom/squareup/activity/LoaderState;

.field private final tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

.field protected final userToken:Ljava/lang/String;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billsMap:Ljava/util/HashMap;

    .line 90
    iput-object p1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    .line 91
    iput-object p2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billListService:Lcom/squareup/server/bills/BillListServiceHelper;

    .line 92
    iput-object p3, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    .line 93
    iput-object p4, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->rpcScheduler:Lio/reactivex/Scheduler;

    .line 94
    iput-object p5, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->mainScheduler:Lio/reactivex/Scheduler;

    .line 95
    iput-object p6, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->userToken:Ljava/lang/String;

    .line 96
    iput-object p7, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    .line 97
    iput-object p8, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 98
    iput-object p9, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->features:Lcom/squareup/settings/server/Features;

    .line 100
    sget-object p1, Lcom/squareup/activity/LoaderState;->NEW:Lcom/squareup/activity/LoaderState;

    iput-object p1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->state:Lcom/squareup/activity/LoaderState;

    return-void
.end method

.method private createCartForBillFamilyIfNull(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;
    .locals 3

    .line 499
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 500
    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Bill;

    .line 501
    invoke-direct {p0, v2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->createCartForBillIfNull(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/Bill;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 504
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    invoke-direct {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->createCartForBillIfNull(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/Bill;

    move-result-object v1

    .line 505
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object p1

    .line 506
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->bill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object p1

    .line 507
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object p1

    .line 508
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    move-result-object p1

    return-object p1
.end method

.method private createCartForBillIfNull(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/Bill;
    .locals 5

    .line 473
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-nez v0, :cond_3

    .line 476
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill;->tender:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Tender;

    .line 477
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    .line 478
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v4, :cond_1

    .line 479
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v4}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 481
    :cond_1
    iget-object v4, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v4, :cond_0

    .line 482
    iget-object v3, v3, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v3}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    goto :goto_0

    .line 486
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill;->newBuilder()Lcom/squareup/protos/client/bills/Bill$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/Cart$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$Builder;-><init>()V

    new-instance v3, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 489
    invoke-virtual {v3, v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v1

    .line 490
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v1

    .line 491
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object v1

    .line 488
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 492
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    .line 487
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Bill$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/Bill$Builder;

    move-result-object p1

    .line 493
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Builder;->build()Lcom/squareup/protos/client/bills/Bill;

    move-result-object p1

    :cond_3
    return-object p1
.end method

.method private getBillFamilies()Lio/reactivex/Single;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;>;"
        }
    .end annotation

    .line 297
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billListService:Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->userToken:Ljava/lang/String;

    .line 298
    invoke-virtual {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getQuery()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getSearchLimit()I

    move-result v3

    invoke-virtual {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getNextPaginationToken()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/server/bills/BillListServiceHelper;->getBillFamilies(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;ILjava/lang/String;Z)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$mnaX8GSsqrRR0_DEPNp9ppz1NeA;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$mnaX8GSsqrRR0_DEPNp9ppz1NeA;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    .line 299
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private getBillFromToken(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;>;"
        }
    .end annotation

    .line 378
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;-><init>()V

    .line 379
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;

    move-result-object p1

    .line 380
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;

    move-result-object v2

    .line 381
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billListService:Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->userToken:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/server/bills/BillListServiceHelper;->getBillFamilies(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;ILjava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method private getRelatedBills(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lio/reactivex/Observable;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;>;"
        }
    .end annotation

    .line 345
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    .line 346
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 347
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 348
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill_tokens:Ljava/util/List;

    .line 350
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 354
    iget-object v5, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 355
    invoke-direct {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getBillFromToken(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 358
    :cond_0
    iget-object v5, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billsMap:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 361
    :cond_1
    new-instance v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    new-instance v3, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    const/4 v5, 0x0

    new-instance v6, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    invoke-direct {v6}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;-><init>()V

    .line 364
    invoke-virtual {v6, v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->bill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object v0

    .line 365
    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object v0

    .line 366
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->related_bill_tokens(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;

    move-result-object p1

    .line 367
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    move-result-object p1

    aput-object p1, v4, v5

    .line 363
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->bill_family(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;

    move-result-object p1

    .line 368
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    .line 361
    invoke-static {v1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lio/reactivex/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getRelatedBills(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$3(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 313
    check-cast p0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 315
    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    return-object p0
.end method

.method static synthetic lambda$sortBillFamiliesInDescendingOrderByCompletedTime$7(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)I
    .locals 0

    .line 373
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill;->dates:Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    .line 374
    invoke-virtual {p1, p0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p0

    return p0
.end method

.method private load(Z)V
    .locals 3

    .line 275
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->state:Lcom/squareup/activity/LoaderState;

    sget-object v1, Lcom/squareup/activity/LoaderState;->LOADING:Lcom/squareup/activity/LoaderState;

    if-ne v0, v1, :cond_0

    return-void

    .line 279
    :cond_0
    sget-object v0, Lcom/squareup/activity/LoaderState;->LOADING:Lcom/squareup/activity/LoaderState;

    invoke-virtual {p0, v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->setState(Lcom/squareup/activity/LoaderState;)V

    .line 281
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->scope:Lmortar/MortarScope;

    invoke-direct {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getBillFamilies()Lio/reactivex/Single;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->rpcScheduler:Lio/reactivex/Scheduler;

    .line 282
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->mainScheduler:Lio/reactivex/Scheduler;

    .line 283
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$ABNAaU_JiZVLU9xqWeLjhQGOyq8;

    invoke-direct {v2, p0, p1}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$ABNAaU_JiZVLU9xqWeLjhQGOyq8;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Z)V

    .line 284
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 281
    invoke-static {v0, p1}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private onGetBillFamiliesSuccess(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;Z)V
    .locals 1

    .line 385
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->pagination_token:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->nextPaginationToken:Ljava/lang/String;

    .line 386
    invoke-virtual {p0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->toItems(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;)Ljava/util/List;

    move-result-object p1

    .line 387
    invoke-virtual {p0, p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onSuccess(Ljava/util/List;Z)V

    return-void
.end method

.method private onGetBillsFailure(ZZ)V
    .locals 2

    .line 398
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 399
    invoke-virtual {p0, v0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onBillsLoaded(Ljava/util/List;Z)V

    if-eqz p2, :cond_0

    .line 401
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->activity_applet_network_error_title:I

    .line 402
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/services/hairball/R$string;->square_server_error_title:I

    .line 403
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz p2, :cond_1

    .line 404
    iget-object p2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/billhistoryui/R$string;->activity_applet_network_error_message:I

    .line 405
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    :cond_1
    iget-object p2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/services/hairball/R$string;->square_server_error_message:I

    .line 406
    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 407
    :goto_1
    new-instance v1, Lcom/squareup/activity/LoaderError;

    invoke-direct {v1, v0, p2, p1}, Lcom/squareup/activity/LoaderError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->lastError:Lcom/squareup/activity/LoaderError;

    .line 408
    sget-object p1, Lcom/squareup/activity/LoaderState;->FAILED:Lcom/squareup/activity/LoaderState;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->setState(Lcom/squareup/activity/LoaderState;)V

    return-void
.end method

.method private onGetBillsSuccess(Lcom/squareup/protos/client/bills/GetBillsResponse;Z)V
    .locals 1

    .line 391
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillsResponse;->pagination_token:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->nextPaginationToken:Ljava/lang/String;

    .line 392
    invoke-virtual {p0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->toItems(Lcom/squareup/protos/client/bills/GetBillsResponse;)Ljava/util/List;

    move-result-object p1

    .line 393
    invoke-virtual {p0, p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onSuccess(Ljava/util/List;Z)V

    return-void
.end method

.method private sortBillFamiliesInDescendingOrderByCompletedTime(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
            ">;)V"
        }
    .end annotation

    .line 372
    sget-object v0, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;->INSTANCE:Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uyHjy8IzGNTNxSC4-Gv9OuAUaYo;

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method


# virtual methods
.method addBill(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 512
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/BillsList;->addIfNotPresent(Lcom/squareup/billhistory/model/BillHistory;)Z

    return-void
.end method

.method addOrReplace(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/BillsList;->addOrSafelyReplace(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method applyCachedTipsToTenders(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 452
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 454
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 460
    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    iget-object v3, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/papersignature/TenderStatusCache;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->isUnsettledCardTender()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 461
    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    iget-object v3, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/papersignature/TenderStatusCache;->getTipAmount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    const/4 v3, 0x0

    .line 462
    invoke-virtual {v1, v2, v3}, Lcom/squareup/billhistory/model/TenderHistory;->withSettledTip(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    .line 464
    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method fireChanged()V
    .locals 2

    .line 258
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/BillsList;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method protected getBillListQuery()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBills()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0}, Lcom/squareup/activity/BillsList;->getBills()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getLastError()Lcom/squareup/activity/LoaderError;
    .locals 1

    .line 270
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->lastError:Lcom/squareup/activity/LoaderError;

    return-object v0
.end method

.method getNextPaginationToken()Ljava/lang/String;
    .locals 1

    .line 262
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->nextPaginationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSearchLimit()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getState()Lcom/squareup/activity/LoaderState;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->state:Lcom/squareup/activity/LoaderState;

    return-object v0
.end method

.method protected hasMore()Z
    .locals 1

    .line 179
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->nextPaginationToken:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public synthetic lambda$getBillFamilies$6$AbstractTransactionsHistoryLoader(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/SingleSource;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 300
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-nez v0, :cond_0

    .line 301
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 304
    :cond_0
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 305
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    .line 307
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    .line 308
    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billsMap:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->bill:Lcom/squareup/protos/client/bills/Bill;

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 310
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    invoke-static {v0}, Lio/reactivex/Observable;->fromIterable(Ljava/lang/Iterable;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$2EsKy_Y9p7xfSFGNZAQr6iK_vsQ;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    .line 311
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$SUrZy4tVUVsbjJBR3oBDCefDBQo;->INSTANCE:Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$SUrZy4tVUVsbjJBR3oBDCefDBQo;

    .line 312
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$WtPxR3Dc41vTvtfwkhwxRY8nlVY;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$WtPxR3Dc41vTvtfwkhwxRY8nlVY;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    .line 317
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 327
    invoke-virtual {v0}, Lio/reactivex/Observable;->toList()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$Woe_vtLGp_2150vOpCI2hCHZx7Q;

    invoke-direct {v1, p0, p1}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$Woe_vtLGp_2150vOpCI2hCHZx7Q;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;)V

    .line 328
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$load$2$AbstractTransactionsHistoryLoader(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 285
    new-instance v0, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uHuYAf7pZSGRFt6uEroqQKItCNM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$uHuYAf7pZSGRFt6uEroqQKItCNM;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Z)V

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$4YD5iKiGmFj_HV3OHA8JxK9o9mw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/activity/-$$Lambda$AbstractTransactionsHistoryLoader$4YD5iKiGmFj_HV3OHA8JxK9o9mw;-><init>(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Z)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$AbstractTransactionsHistoryLoader(ZLcom/squareup/protos/client/bills/GetBillFamiliesResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 285
    invoke-direct {p0, p2, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onGetBillFamiliesSuccess(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;Z)V

    return-void
.end method

.method public synthetic lambda$null$1$AbstractTransactionsHistoryLoader(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 287
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    .line 288
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onGetBillsFailure(ZZ)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 290
    invoke-direct {p0, p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onGetBillsFailure(ZZ)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$4$AbstractTransactionsHistoryLoader(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 320
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 321
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    .line 322
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;->related_bill:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Bill;

    .line 323
    iget-object v1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->billsMap:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$5$AbstractTransactionsHistoryLoader(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;Ljava/util/List;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 329
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 330
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    .line 332
    iget-object v1, v1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 335
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->sortBillFamiliesInDescendingOrderByCompletedTime(Ljava/util/List;)V

    .line 336
    new-instance p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->newBuilder()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;

    move-result-object p1

    .line 337
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->bill_family(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;

    move-result-object p1

    .line 338
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;-><init>(Ljava/lang/Object;)V

    return-object p2
.end method

.method public load()Ljava/util/UUID;
    .locals 1

    const/4 v0, 0x0

    .line 127
    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->nextPaginationToken:Ljava/lang/String;

    .line 128
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->loadToken:Ljava/util/UUID;

    const/4 v0, 0x0

    .line 129
    invoke-direct {p0, v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->load(Z)V

    .line 130
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->loadToken:Ljava/util/UUID;

    return-object v0
.end method

.method public loadMore()V
    .locals 1

    const/4 v0, 0x1

    .line 137
    invoke-direct {p0, v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->load(Z)V

    return-void
.end method

.method protected onBillsLoaded(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;Z)V"
        }
    .end annotation

    if-nez p2, :cond_0

    .line 220
    iget-object p2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {p2}, Lcom/squareup/activity/BillsList;->clearNonPendingBills()V

    .line 222
    :cond_0
    iget-object p2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {p2, p1}, Lcom/squareup/activity/BillsList;->addOrSafelyReplace(Ljava/util/Collection;)V

    return-void
.end method

.method onChanged()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->scope:Lmortar/MortarScope;

    .line 105
    iget-object p1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {p1, p0}, Lcom/squareup/papersignature/TenderStatusCache;->addOnTenderTipsChangedListener(Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->tenderStatusCache:Lcom/squareup/papersignature/TenderStatusCache;

    invoke-virtual {v0, p0}, Lcom/squareup/papersignature/TenderStatusCache;->removeOnTenderTipsChangedListener(Lcom/squareup/papersignature/TenderStatusCache$OnTenderTipsChangedListener;)V

    const/4 v0, 0x0

    .line 110
    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->scope:Lmortar/MortarScope;

    return-void
.end method

.method protected onSuccess(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;Z)V"
        }
    .end annotation

    .line 201
    invoke-virtual {p0, p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onBillsLoaded(Ljava/util/List;Z)V

    const/4 p1, 0x0

    .line 202
    iput-object p1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->lastError:Lcom/squareup/activity/LoaderError;

    .line 203
    sget-object p1, Lcom/squareup/activity/LoaderState;->LOADED:Lcom/squareup/activity/LoaderState;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->setState(Lcom/squareup/activity/LoaderState;)V

    return-void
.end method

.method public onTenderTipsChanged(Ljava/util/Collection;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)V"
        }
    .end annotation

    .line 154
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    .line 155
    iget-object v1, v0, Lcom/squareup/billhistory/model/TenderHistory;->billId:Lcom/squareup/protos/client/IdPair;

    invoke-static {v1}, Lcom/squareup/billhistory/model/BillHistoryId;->forBill(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v1

    .line 156
    invoke-virtual {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 159
    iget-object v2, v0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/TenderHistory;->buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    .line 162
    invoke-virtual {v2, v3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tenderState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v2

    .line 163
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v2

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 164
    invoke-virtual {v2, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v0

    .line 167
    invoke-virtual {v1, v0}, Lcom/squareup/billhistory/model/BillHistory;->replaceTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v1, v0}, Lcom/squareup/activity/BillsList;->safelyReplaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)Z

    goto :goto_0

    .line 171
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->fireChanged()V

    return-void
.end method

.method replaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/BillsList;->safelyReplaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)Z

    return-void
.end method

.method protected reset()V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0}, Lcom/squareup/activity/BillsList;->clearNonPendingBills()V

    const/4 v0, 0x0

    .line 238
    iput-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->nextPaginationToken:Ljava/lang/String;

    .line 239
    sget-object v0, Lcom/squareup/activity/LoaderState;->NEW:Lcom/squareup/activity/LoaderState;

    invoke-virtual {p0, v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->setState(Lcom/squareup/activity/LoaderState;)V

    return-void
.end method

.method protected setState(Lcom/squareup/activity/LoaderState;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->state:Lcom/squareup/activity/LoaderState;

    if-eq v0, p1, :cond_0

    .line 227
    iput-object p1, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->state:Lcom/squareup/activity/LoaderState;

    .line 228
    invoke-virtual {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->fireChanged()V

    :cond_0
    return-void
.end method

.method toItems(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 412
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 414
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;->bill_family:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    .line 415
    invoke-direct {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->createCartForBillFamilyIfNull(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;

    move-result-object v1

    .line 417
    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v3

    iget-object v4, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->features:Lcom/squareup/settings/server/Features;

    sget-object v5, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 418
    invoke-interface {v4, v5}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v4

    .line 417
    invoke-static {v1, v2, v3, v4}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    .line 419
    new-instance v2, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 420
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    .line 421
    invoke-virtual {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->applyCachedTipsToTenders(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 422
    invoke-virtual {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setTenders(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 423
    invoke-static {v1}, Lcom/squareup/billhistory/model/BillHistory;->sumTips(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 425
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method toItems(Lcom/squareup/protos/client/bills/GetBillsResponse;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetBillsResponse;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 433
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 435
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillsResponse;->bill:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Bill;

    .line 436
    invoke-direct {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->createCartForBillIfNull(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/Bill;

    move-result-object v1

    .line 438
    iget-object v2, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v3}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidCompAllowed()Z

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    .line 439
    new-instance v2, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 440
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    .line 441
    invoke-virtual {p0, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->applyCachedTipsToTenders(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 442
    invoke-virtual {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setTenders(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 443
    invoke-static {v1}, Lcom/squareup/billhistory/model/BillHistory;->sumTips(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    .line 445
    invoke-virtual {v2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method
