.class public Lcom/squareup/activity/AllTransactionsHistoryLoader;
.super Lcom/squareup/activity/AbstractTransactionsHistoryLoader;
.source "AllTransactionsHistoryLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;
    }
.end annotation


# static fields
.field static final ITEMS_PER_REQUEST:I = 0x14


# instance fields
.field private final bus:Lcom/squareup/badbus/BadBus;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private loadSequencer:Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lcom/squareup/payment/pending/PendingTransactionsStore;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/settings/server/Features;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 11
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p7    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v10, p0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p12

    move-object/from16 v9, p11

    .line 56
    invoke-direct/range {v0 .. v9}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;-><init>(Lcom/squareup/util/Res;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/activity/BillsList;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Ljava/lang/String;Lcom/squareup/papersignature/TenderStatusCache;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/settings/server/Features;)V

    move-object v0, p1

    .line 58
    iput-object v0, v10, Lcom/squareup/activity/AllTransactionsHistoryLoader;->bus:Lcom/squareup/badbus/BadBus;

    move-object/from16 v0, p5

    .line 59
    iput-object v0, v10, Lcom/squareup/activity/AllTransactionsHistoryLoader;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    move-object/from16 v0, p8

    .line 60
    iput-object v0, v10, Lcom/squareup/activity/AllTransactionsHistoryLoader;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 61
    invoke-interface/range {p13 .. p13}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, v10, Lcom/squareup/activity/AllTransactionsHistoryLoader;->internetState:Lio/reactivex/Observable;

    return-void
.end method

.method private doBillIdChanged(Lcom/squareup/billhistory/Bills$BillIdChanged;)V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    iget-object v1, p1, Lcom/squareup/billhistory/Bills$BillIdChanged;->oldBillId:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object p1, p1, Lcom/squareup/billhistory/Bills$BillIdChanged;->newBillId:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/activity/BillsList;->updateBillIdIfPresent(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v0, Lcom/squareup/activity/-$$Lambda$AkvIVN6MG_qHwXz4MExsG6NOIvk;

    invoke-direct {v0, p0}, Lcom/squareup/activity/-$$Lambda$AkvIVN6MG_qHwXz4MExsG6NOIvk;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;)V

    invoke-interface {p1, v0}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/connectivity/InternetState;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static synthetic lambda$z7mI4W_zYPxZ14C9J06Jjyk2bjg(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/billhistory/Bills$BillIdChanged;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->doBillIdChanged(Lcom/squareup/billhistory/Bills$BillIdChanged;)V

    return-void
.end method


# virtual methods
.method protected getSearchLimit()I
    .locals 1

    const/16 v0, 0x14

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$AllTransactionsHistoryLoader(Ljava/util/List;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/BillsList;->safelyReplacePendingBills(Ljava/util/Collection;)V

    .line 79
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->fireChanged()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$2$AllTransactionsHistoryLoader(Lcom/squareup/connectivity/InternetState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    invoke-virtual {p0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object p1

    sget-object v0, Lcom/squareup/activity/LoaderState;->FAILED:Lcom/squareup/activity/LoaderState;

    if-ne p1, v0, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->getLastError()Lcom/squareup/activity/LoaderError;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 98
    iget-boolean p1, p1, Lcom/squareup/activity/LoaderError;->loadingMore:Z

    if-eqz p1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadMore()V

    goto :goto_0

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->load()Ljava/util/UUID;

    :cond_1
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onEnterScope(Lmortar/MortarScope;)V

    .line 69
    new-instance v0, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;

    iget-object v1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    invoke-direct {v0, p0, v1}, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/payment/pending/PendingTransactionsStore;)V

    iput-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadSequencer:Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;

    .line 70
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadSequencer:Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;

    invoke-virtual {v0}, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->prepare()V

    .line 72
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 74
    invoke-interface {v0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->allPendingTransactionsAsBillHistory()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 75
    invoke-interface {v1}, Lcom/squareup/payment/pending/PendingTransactionsStore;->processingOfflineTransactions()Lio/reactivex/Observable;

    move-result-object v1

    .line 76
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    .line 73
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$z6bm_E8zwOQtOjHWD_41etM6kRI;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$z6bm_E8zwOQtOjHWD_41etM6kRI;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;)V

    .line 77
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 72
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->pendingTransactionsStore:Lcom/squareup/payment/pending/PendingTransactionsStore;

    .line 85
    invoke-interface {v0}, Lcom/squareup/payment/pending/PendingTransactionsStore;->onProcessedForwardedTransactionsAsBillHistory()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->bills:Lcom/squareup/activity/BillsList;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/activity/-$$Lambda$-hdfrRS2GOvIm2cldXAEdaQoVvs;

    invoke-direct {v2, v1}, Lcom/squareup/activity/-$$Lambda$-hdfrRS2GOvIm2cldXAEdaQoVvs;-><init>(Lcom/squareup/activity/BillsList;)V

    .line 86
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 84
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->bus:Lcom/squareup/badbus/BadBus;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->registerOnBadBus(Lmortar/MortarScope;Lcom/squareup/badbus/BadBus;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 92
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$TCXMght2Hw3sFaD8bMo7rQZqQNw;->INSTANCE:Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$TCXMght2Hw3sFaD8bMo7rQZqQNw;

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$R_6EIUzDdqg2AdUcXznHPKeVih0;

    invoke-direct {v1, p0}, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$R_6EIUzDdqg2AdUcXznHPKeVih0;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;)V

    .line 94
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 90
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 108
    invoke-super {p0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onExitScope()V

    .line 109
    iget-object v0, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadSequencer:Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {v0}, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->shutdown()V

    :cond_0
    return-void
.end method

.method protected onSuccess(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;Z)V"
        }
    .end annotation

    .line 119
    invoke-super {p0, p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->onSuccess(Ljava/util/List;Z)V

    .line 123
    iget-object p1, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadSequencer:Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;

    if-eqz p1, :cond_0

    .line 124
    iget-object p2, p0, Lcom/squareup/activity/AllTransactionsHistoryLoader;->loadToken:Ljava/util/UUID;

    invoke-virtual {p1, p2}, Lcom/squareup/activity/AllTransactionsHistoryLoader$LoadSequencer;->maybeLoadMore(Ljava/util/UUID;)V

    :cond_0
    return-void
.end method

.method registerOnBadBus(Lmortar/MortarScope;Lcom/squareup/badbus/BadBus;)V
    .locals 1

    .line 115
    const-class v0, Lcom/squareup/billhistory/Bills$BillIdChanged;

    invoke-virtual {p2, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$z7mI4W_zYPxZ14C9J06Jjyk2bjg;

    invoke-direct {v0, p0}, Lcom/squareup/activity/-$$Lambda$AllTransactionsHistoryLoader$z7mI4W_zYPxZ14C9J06Jjyk2bjg;-><init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;)V

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
