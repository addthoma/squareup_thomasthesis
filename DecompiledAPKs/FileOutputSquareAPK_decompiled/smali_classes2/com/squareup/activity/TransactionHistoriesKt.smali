.class public final Lcom/squareup/activity/TransactionHistoriesKt;
.super Ljava/lang/Object;
.source "TransactionHistories.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0004\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "getTransactionIds",
        "Lcom/squareup/transactionhistory/TransactionIds;",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "toTransactionIds",
        "Lcom/squareup/billhistory/model/BillHistoryId;",
        "Lcom/squareup/protos/client/IdPair;",
        "bill-history-ui_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getTransactionIds(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/TransactionIds;
    .locals 1

    const-string v0, "$this$getTransactionIds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v0, "id"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/activity/TransactionHistoriesKt;->toTransactionIds(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p0

    return-object p0
.end method

.method public static final toTransactionIds(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/transactionhistory/TransactionIds;
    .locals 1

    const-string v0, "$this$toTransactionIds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object p0

    const-string v0, "id"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/activity/TransactionHistoriesKt;->toTransactionIds(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p0

    return-object p0
.end method

.method public static final toTransactionIds(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/transactionhistory/TransactionIds;
    .locals 2

    const-string v0, "$this$toTransactionIds"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/transactionhistory/TransactionIds;->Companion:Lcom/squareup/transactionhistory/TransactionIds$Companion;

    iget-object v1, p0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/squareup/transactionhistory/TransactionIds$Companion;->newTransactionIds(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/transactionhistory/TransactionIds;

    move-result-object p0

    return-object p0
.end method
