.class public Lcom/squareup/activity/refund/CreatorDetailsHelper;
.super Ljava/lang/Object;
.source "CreatorDetailsHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static forEmployeeToken(Ljava/lang/String;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;)Lcom/squareup/protos/client/CreatorDetails;
    .locals 2

    .line 35
    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->isEldmEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    if-nez p0, :cond_0

    return-object v1

    .line 42
    :cond_0
    invoke-interface {p1, p0}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 43
    invoke-interface {p1, p0}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeInfo(Lcom/squareup/permissions/Employee;)Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p0

    goto :goto_0

    .line 44
    :cond_1
    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object p0

    .line 49
    :goto_0
    iget-object p1, p0, Lcom/squareup/permissions/EmployeeInfo;->employeeToken:Ljava/lang/String;

    if-nez p1, :cond_2

    return-object v1

    .line 53
    :cond_2
    new-instance p1, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    .line 54
    invoke-virtual {p0, p2}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object p0

    .line 55
    invoke-virtual {p0}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object p0

    return-object p0

    :cond_3
    return-object v1
.end method
