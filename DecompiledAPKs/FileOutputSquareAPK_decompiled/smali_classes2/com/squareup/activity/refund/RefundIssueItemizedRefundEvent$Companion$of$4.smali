.class final Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;
.super Lkotlin/jvm/internal/Lambda;
.source "ItemizedRefundAnalytics.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion;->of(Lcom/squareup/activity/refund/RefundData;)Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemizedRefundAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemizedRefundAnalytics.kt\ncom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4\n*L\n1#1,176:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;

    invoke-direct {v0}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;-><init>()V

    sput-object v0, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;->INSTANCE:Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/activity/refund/TenderDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundIssueItemizedRefundEvent$Companion$of$4;->invoke(Lcom/squareup/activity/refund/TenderDetails;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/activity/refund/TenderDetails;)Ljava/lang/String;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory$Type;->name()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toLowerCase()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
