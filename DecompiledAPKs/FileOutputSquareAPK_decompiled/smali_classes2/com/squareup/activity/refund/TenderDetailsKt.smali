.class public final Lcom/squareup/activity/refund/TenderDetailsKt;
.super Ljava/lang/Object;
.source "TenderDetails.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001a\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "setMaxForRefundTender",
        "",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "maxAmountAvailable",
        "Lcom/squareup/protos/common/Money;",
        "tenderDetail",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "activity_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setMaxForRefundTender(Lcom/squareup/money/MaxMoneyScrubber;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/TenderDetails;)V
    .locals 1

    const-string v0, "$this$setMaxForRefundTender"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxAmountAvailable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderDetail"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p2}, Lcom/squareup/activity/refund/TenderDetails;->getEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    return-void
.end method
