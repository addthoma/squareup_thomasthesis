.class public final Lcom/squareup/activity/refund/RefundData$Companion;
.super Ljava/lang/Object;
.source "RefundData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/RefundData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundData.kt\ncom/squareup/activity/refund/RefundData$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,870:1\n1360#2:871\n1429#2,3:872\n1587#2,3:875\n1529#2,3:878\n704#2:881\n777#2,2:882\n1642#2:884\n250#2,2:885\n1643#2:887\n1642#2:888\n250#2,2:889\n1643#2:891\n1642#2,2:892\n*E\n*S KotlinDebug\n*F\n+ 1 RefundData.kt\ncom/squareup/activity/refund/RefundData$Companion\n*L\n257#1:871\n257#1,3:872\n258#1,3:875\n262#1,3:878\n273#1:881\n273#1,2:882\n291#1:884\n291#1,2:885\n291#1:887\n325#1:888\n325#1,2:889\n325#1:891\n342#1,2:892\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J`\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0014\u0010\u0014\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015H\u0007Jn\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00182\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0012\u001a\u00020\u00132\u0014\u0010\u0014\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015H\u0007J\"\u0010\u001b\u001a\u00020\u0016*\u00020\u001c2\u0014\u0010\u0014\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0016\u0018\u00010\u0015H\u0002J\u000e\u0010\u001d\u001a\u0004\u0018\u00010\u0016*\u00020\u001cH\u0002\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundData$Companion;",
        "",
        "()V",
        "fromResidualBill",
        "Lcom/squareup/activity/refund/RefundData;",
        "authorizedEmployeeToken",
        "",
        "clientToken",
        "sourceBill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "residualBillResponse",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
        "roundingType",
        "Lcom/squareup/calc/constants/RoundingType;",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "shouldShowRefundGiftCard",
        "",
        "itemizationMaxReturnableQuantity",
        "",
        "Ljava/math/BigDecimal;",
        "fromResidualBillAndCatalog",
        "Lrx/Single;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "maxReturnableQuantity",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
        "unitQuantityOrNull",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 229
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundData$Companion;-><init>()V

    return-void
.end method

.method public static final synthetic access$unitQuantityOrNull(Lcom/squareup/activity/refund/RefundData$Companion;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;)Ljava/math/BigDecimal;
    .locals 0

    .line 229
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundData$Companion;->unitQuantityOrNull(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;)Ljava/math/BigDecimal;

    move-result-object p0

    return-object p0
.end method

.method private final maxReturnableQuantity(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/util/Map;)Ljava/math/BigDecimal;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Ljava/math/BigDecimal;"
        }
    .end annotation

    if-nez p2, :cond_0

    .line 403
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    const-string p2, "source_itemization_quantity"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p2, Ljava/math/BigDecimal;

    invoke-direct {p2, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 404
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 405
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    const-string v0, "source_itemization_token_pair.client_id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Ljava/math/BigDecimal;

    goto :goto_0

    .line 406
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v0, "source_itemization_token_pair.server_id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p1}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Ljava/math/BigDecimal;

    :goto_0
    return-object p2

    .line 408
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Expected max returnable to be present in itemizationMaxReturnableQuantity."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final unitQuantityOrNull(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;)Ljava/math/BigDecimal;
    .locals 1

    .line 415
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-static {v0}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v0, "itemization.quantity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final fromResidualBill(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLjava/util/Map;)Lcom/squareup/activity/refund/RefundData;
    .locals 37
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lcom/squareup/activity/refund/RefundData;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v10, p6

    move-object/from16 v11, p9

    const-string v2, "clientToken"

    move-object/from16 v12, p2

    invoke-static {v12, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "sourceBill"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "residualBillResponse"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "roundingType"

    move-object/from16 v3, p5

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "res"

    invoke-static {v10, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "perUnitFormatter"

    move-object/from16 v13, p7

    invoke-static {v13, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v15, v2

    check-cast v15, Ljava/util/List;

    .line 249
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v16, v2

    check-cast v16, Ljava/util/List;

    .line 250
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v17, v2

    check-cast v17, Ljava/util/List;

    .line 253
    invoke-static/range {p5 .. p5}, Lcom/squareup/activity/refund/RefundDataKt;->roundingTypeToMathRoundingMode(Lcom/squareup/calc/constants/RoundingType;)Ljava/math/RoundingMode;

    move-result-object v30

    .line 254
    new-instance v14, Lcom/squareup/protos/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/squareup/billhistory/model/BillHistory;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v14, v2, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 255
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedRefundBills()Ljava/util/List;

    move-result-object v2

    const-string v3, "sourceBill.relatedRefundBills"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/activity/refund/RefundDataKt;->previouslyRefundedTenders(Ljava/util/List;)Ljava/util/Map;

    move-result-object v18

    .line 256
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    const-string v9, "residualBillResponse.residual_tender"

    invoke-static {v2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 871
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 872
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 873
    check-cast v4, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    .line 257
    iget-object v4, v4, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 874
    :cond_0
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 876
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v8, v14

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    .line 258
    invoke-static {v8, v3}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v8

    goto :goto_1

    .line 262
    :cond_1
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->can_only_issue_amount_based_refund:Ljava/lang/Boolean;

    const-string v3, "residualBillResponse.can\u2026issue_amount_based_refund"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/16 v19, 0x0

    const/4 v7, 0x1

    if-nez v2, :cond_14

    .line 261
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_14

    .line 262
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    const-string v3, "residualBillResponse.residual_itemization"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 878
    instance-of v4, v2, Ljava/util/Collection;

    if-eqz v4, :cond_3

    move-object v4, v2

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/4 v2, 0x1

    goto :goto_2

    .line 879
    :cond_3
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    .line 263
    iget-object v4, v4, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v5, "it.itemization.quantity"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v5, Ljava/math/BigDecimal;

    invoke-direct {v5, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 264
    invoke-static {v5}, Lcom/squareup/util/BigDecimals;->isZero(Ljava/math/BigDecimal;)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_5

    goto/16 :goto_a

    .line 271
    :cond_5
    sget-object v20, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    .line 272
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 881
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 882
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    if-eqz v11, :cond_9

    .line 276
    iget-object v6, v5, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    if-nez v6, :cond_7

    goto :goto_4

    .line 284
    :cond_7
    iget-object v6, v5, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 283
    invoke-interface {v11, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 287
    iget-object v5, v5, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 286
    invoke-interface {v11, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    goto :goto_4

    :cond_8
    const/4 v5, 0x0

    goto :goto_5

    :cond_9
    :goto_4
    const/4 v5, 0x1

    :goto_5
    if-eqz v5, :cond_6

    .line 289
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 883
    :cond_a
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 884
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_6
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/16 v22, 0x0

    if-eqz v2, :cond_f

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    .line 293
    sget-object v2, Lcom/squareup/activity/refund/RefundData;->Companion:Lcom/squareup/activity/refund/RefundData$Companion;

    const-string v3, "residualItemization"

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v5, v11}, Lcom/squareup/activity/refund/RefundData$Companion;->maxReturnableQuantity(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/util/Map;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 295
    sget-object v2, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    .line 297
    sget-object v3, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v6, v0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v3, v6}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v23

    move-object v3, v5

    move-object v6, v4

    move-object/from16 v24, v9

    move-object v9, v5

    move-object/from16 v5, v30

    move-object v11, v6

    const/16 v12, 0x20

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v35, v8

    move-object/from16 v8, v23

    .line 295
    invoke-virtual/range {v2 .. v8}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundableItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 299
    move-object v3, v15

    check-cast v3, Ljava/util/Collection;

    move-object v4, v2

    check-cast v4, Ljava/lang/Iterable;

    invoke-static {v3, v4}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 306
    invoke-static/range {p3 .. p3}, Lcom/squareup/activity/refund/BillHistoryUtils;->getAllRelatedItemizations(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 885
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/bills/Itemization;

    .line 307
    iget-object v5, v5, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v9, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    move-object/from16 v22, v4

    .line 886
    :cond_c
    move-object/from16 v4, v22

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization;

    if-eqz v4, :cond_e

    .line 315
    sget-object v3, Lcom/squareup/activity/refund/RefundData;->Companion:Lcom/squareup/activity/refund/RefundData$Companion;

    invoke-direct {v3, v9}, Lcom/squareup/activity/refund/RefundData$Companion;->unitQuantityOrNull(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;)Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_d

    goto :goto_7

    .line 316
    :cond_d
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    const-string v2, "BigDecimal.valueOf(this.toLong())"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    :goto_7
    invoke-virtual {v11, v3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    const-string v2, "this.subtract(other)"

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    move-object/from16 v11, v16

    check-cast v11, Ljava/util/Collection;

    sget-object v2, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    .line 321
    sget-object v3, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v6, v0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v3, v6}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v12

    move-object v3, v9

    move-object/from16 v6, v30

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v13, v24

    move-object v9, v12

    .line 319
    invoke-virtual/range {v2 .. v9}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundedItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v11, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    move-object/from16 v12, p2

    move-object/from16 v11, p9

    move-object v9, v13

    move-object/from16 v8, v35

    const/4 v7, 0x1

    move-object/from16 v13, p7

    goto/16 :goto_6

    .line 308
    :cond_e
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ResidualItemization token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v9, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "not found in source bill\'s cart"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 308
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_f
    move-object/from16 v35, v8

    move-object v13, v9

    const/16 v12, 0x20

    .line 324
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    const-string v3, "residualBillResponse.residual_tip"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 888
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    .line 327
    iget-object v4, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    invoke-static {v4, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Iterable;

    .line 889
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    .line 328
    iget-object v6, v6, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_tender_server_token:Ljava/lang/String;

    iget-object v7, v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v7, v7, Lcom/squareup/protos/client/bills/TipLineItem;->tender_server_token:Ljava/lang/String;

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    goto :goto_9

    :cond_11
    move-object/from16 v5, v22

    .line 890
    :goto_9
    check-cast v5, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    if-eqz v5, :cond_12

    .line 333
    move-object v4, v15

    check-cast v4, Ljava/util/Collection;

    sget-object v6, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    const-string v7, "residualTip"

    .line 334
    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    invoke-virtual {v6, v3, v5, v10}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundableTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/lang/Iterable;

    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 336
    move-object/from16 v4, v16

    check-cast v4, Ljava/util/Collection;

    sget-object v6, Lcom/squareup/activity/refund/ItemizationDetails;->Companion:Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    invoke-virtual {v6, v3, v5, v10}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->ofRefundedTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v4, v3}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    goto :goto_8

    .line 329
    :cond_12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 330
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ResidualTip tender server token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem;->tender_server_token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "not found in residual tender"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 329
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_13
    move-object/from16 v6, v20

    const/4 v7, 0x0

    goto :goto_b

    :cond_14
    :goto_a
    move-object/from16 v35, v8

    move-object v13, v9

    .line 268
    sget-object v2, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    move-object v6, v2

    const/4 v7, 0x1

    .line 342
    :goto_b
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tender:Ljava/util/List;

    invoke-static {v2, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 892
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_15

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;

    .line 343
    move-object/from16 v4, v17

    check-cast v4, Ljava/util/Collection;

    sget-object v5, Lcom/squareup/activity/refund/TenderDetails;->Companion:Lcom/squareup/activity/refund/TenderDetails$Companion;

    const-string v8, "it"

    invoke-static {v3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-virtual {v5, v3, v0, v10, v8}, Lcom/squareup/activity/refund/TenderDetails$Companion;->of(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Z)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :cond_15
    const/4 v8, 0x1

    .line 346
    new-instance v34, Lcom/squareup/activity/refund/RefundData;

    .line 349
    invoke-virtual/range {p3 .. p3}, Lcom/squareup/billhistory/model/BillHistory;->getSourceBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v9

    const-string v0, "sourceBill.getSourceBillId()"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundableAmount"

    move-object/from16 v5, v35

    .line 355
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 360
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v28

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    .line 361
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v29

    .line 362
    sget-object v13, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->UNKNOWN_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v35, 0x0

    .line 365
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_16

    const/16 v36, 0x1

    goto :goto_d

    :cond_16
    const/16 v36, 0x0

    :goto_d
    const/16 v31, 0x0

    const v32, 0x479f0c00    # 81432.0f

    const/16 v33, 0x0

    const-string v0, ""

    move-object/from16 v19, v14

    move-object v14, v0

    move-object/from16 v0, v34

    move-object/from16 v1, p4

    move-object v2, v15

    move-object/from16 v3, v16

    move-object/from16 v4, v17

    move-object v8, v9

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v15, v19

    move-object/from16 v16, v18

    move-object/from16 v17, v20

    move-object/from16 v18, v21

    move/from16 v19, v22

    move-object/from16 v20, v23

    move-object/from16 v21, v24

    move/from16 v22, p8

    move/from16 v23, v36

    move/from16 v24, v25

    move/from16 v25, v26

    move/from16 v26, v27

    move-object/from16 v27, v35

    .line 346
    invoke-direct/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;-><init>(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v34
.end method

.method public final fromResidualBillAndCatalog(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/cogs/Cogs;ZLjava/util/Map;)Lrx/Single;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/cogs/Cogs;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p8

    const-string v1, "clientToken"

    move-object v4, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "sourceBill"

    move-object v5, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "residualBillResponse"

    move-object/from16 v6, p4

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "roundingType"

    move-object/from16 v7, p5

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    move-object/from16 v8, p6

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "perUnitFormatter"

    move-object/from16 v9, p7

    invoke-static {v9, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "cogs"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    move-object v2, p0

    check-cast v2, Lcom/squareup/activity/refund/RefundData$Companion;

    move-object v3, p1

    move/from16 v10, p9

    move-object/from16 v11, p10

    invoke-virtual/range {v2 .. v11}, Lcom/squareup/activity/refund/RefundData$Companion;->fromResidualBill(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLjava/util/Map;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v1

    .line 395
    invoke-static {v1, v0}, Lcom/squareup/activity/refund/RefundData;->access$populateCatalogInfo(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/cogs/Cogs;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method
