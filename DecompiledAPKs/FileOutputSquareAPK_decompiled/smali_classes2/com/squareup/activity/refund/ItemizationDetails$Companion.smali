.class public final Lcom/squareup/activity/refund/ItemizationDetails$Companion;
.super Ljava/lang/Object;
.source "ItemizationDetails.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/ItemizationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nItemizationDetails.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ItemizationDetails.kt\ncom/squareup/activity/refund/ItemizationDetails$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,311:1\n1360#2:312\n1429#2,3:313\n*E\n*S KotlinDebug\n*F\n+ 1 ItemizationDetails.kt\ncom/squareup/activity/refund/ItemizationDetails$Companion\n*L\n271#1:312\n271#1,3:313\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JD\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0004H\u0002J\u0018\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\tH\u0007JD\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0004H\u0007J&\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\rH\u0007JL\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0004H\u0007J&\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u000c\u001a\u00020\rH\u0007\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/activity/refund/ItemizationDetails$Companion;",
        "",
        "()V",
        "detailsWithMoneyPerItem",
        "",
        "Lcom/squareup/activity/refund/ItemizationDetails;",
        "residualItemization",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
        "moneyPerItem",
        "Lcom/squareup/protos/common/Money;",
        "quantity",
        "Ljava/math/BigDecimal;",
        "res",
        "Lcom/squareup/util/Res;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "seats",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
        "of",
        "name",
        "",
        "amount",
        "ofRefundableItemization",
        "maxRefundableQuantity",
        "roundingMode",
        "Ljava/math/RoundingMode;",
        "ofRefundableTip",
        "residualTip",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
        "residualTender",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
        "ofRefundedItemization",
        "sourceItemization",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "maxRefundedQuantity",
        "ofRefundedTip",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;-><init>()V

    return-void
.end method

.method private final detailsWithMoneyPerItem(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/math/BigDecimal;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    .line 243
    sget-object v3, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v3

    if-gtz v3, :cond_0

    .line 244
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 245
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 246
    sget v3, Lcom/squareup/activity/R$string;->refund_custom_amount:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 247
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->item:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails$Item;->name:Ljava/lang/String;

    .line 254
    :goto_0
    iget-object v4, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    const-string v5, "residualItemization.itemization"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    .line 251
    invoke-static {v2, v5, v4, v1, v6}, Lcom/squareup/activity/ui/BillHistorySubtitleFormatterKt;->createBillHistoryItemizationSubtitle(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    .line 259
    iget-object v4, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz v4, :cond_2

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    if-eqz v4, :cond_2

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->item_variation_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;

    goto :goto_1

    :cond_2
    move-object v4, v5

    :goto_1
    if-eqz v4, :cond_3

    .line 260
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;

    if-eqz v4, :cond_3

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$BackingDetails;->item_variation:Lcom/squareup/api/items/ItemVariation;

    if-eqz v4, :cond_3

    iget-object v4, v4, Lcom/squareup/api/items/ItemVariation;->id:Ljava/lang/String;

    move-object/from16 v18, v4

    goto :goto_2

    :cond_3
    move-object/from16 v18, v5

    .line 266
    :goto_2
    iget-object v4, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-static {v4}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual/range {p3 .. p3}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v4

    .line 269
    :goto_3
    iget-object v5, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-static {v5}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto :goto_4

    :cond_5
    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    :goto_4
    const/4 v5, 0x0

    .line 271
    invoke-static {v5, v4}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 312
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    move-object v15, v5

    check-cast v15, Ljava/util/Collection;

    .line 313
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_5
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    move-object/from16 v4, v19

    check-cast v4, Lkotlin/collections/IntIterator;

    invoke-virtual {v4}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v13

    .line 272
    new-instance v14, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 273
    iget-object v9, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    const-string v4, "name"

    .line 275
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v4, "itemizationQuantity"

    .line 279
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v16, 0x0

    .line 280
    iget-object v8, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_bill_server_token:Ljava/lang/String;

    const/16 v17, 0x260

    const/16 v20, 0x0

    move-object v4, v14

    move-object v5, v3

    move-object/from16 v6, p2

    move-object v7, v2

    move-object/from16 v12, v18

    move-object/from16 v21, v14

    move/from16 v14, v16

    move-object/from16 v22, v15

    move-object v15, v1

    move/from16 v16, v17

    move-object/from16 v17, v20

    .line 272
    invoke-direct/range {v4 .. v17}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object/from16 v4, v21

    move-object/from16 v5, v22

    .line 281
    invoke-interface {v5, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object v15, v5

    goto :goto_5

    :cond_6
    move-object v5, v15

    .line 315
    move-object v15, v5

    check-cast v15, Ljava/util/List;

    return-object v15
.end method


# virtual methods
.method public final of(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/ItemizationDetails;
    .locals 15
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "name"

    move-object/from16 v2, p1

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    move-object/from16 v3, p2

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    new-instance v0, Lcom/squareup/activity/refund/ItemizationDetails;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x7fc

    const/4 v14, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v14}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final ofRefundableItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            "Ljava/math/BigDecimal;",
            "Ljava/math/RoundingMode;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "residualItemization"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maxRefundableQuantity"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "roundingMode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "seats"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v0, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v1, "residualItemization.itemization.quantity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p2, v1}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v5

    .line 148
    sget-object p2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v5, p2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result p2

    if-gtz p2, :cond_0

    .line 149
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 154
    :cond_0
    iget-object p2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-static {p2}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 155
    iget-object p2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    goto :goto_0

    .line 157
    :cond_1
    iget-object p2, p1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 156
    invoke-static {p2, v1, p3}, Lcom/squareup/money/MoneyMath;->divide(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    :goto_0
    move-object v4, p2

    .line 161
    move-object v2, p0

    check-cast v2, Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    const-string p2, "moneyPerItem"

    .line 163
    invoke-static {v4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "quantity"

    .line 164
    invoke-static {v5, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p1

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    .line 161
    invoke-direct/range {v2 .. v8}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->detailsWithMoneyPerItem(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final ofRefundableTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "residualTip"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "residualTender"

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    move-object/from16 v3, p3

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    iget-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-static {v1}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 217
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 220
    :cond_0
    new-instance v15, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 221
    iget-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v7, v1, Lcom/squareup/protos/client/bills/TipLineItem;->tender_server_token:Ljava/lang/String;

    .line 222
    invoke-static/range {p2 .. p3}, Lcom/squareup/activity/refund/ItemizationDetailsKt;->tipName(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    .line 223
    iget-object v1, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v3, v1, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    const-string v1, "residualTip.residual_tip.amounts.applied_money"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    .line 224
    iget-object v5, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x7d4

    const/4 v14, 0x0

    move-object v1, v15

    .line 220
    invoke-direct/range {v1 .. v14}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 219
    invoke-static {v15}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final ofRefundedItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/client/bills/Itemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            "Lcom/squareup/protos/client/bills/Itemization;",
            "Ljava/math/BigDecimal;",
            "Ljava/math/RoundingMode;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object v1, p1

    move-object v0, p2

    move-object v2, p3

    move-object v3, p4

    const-string v4, "residualItemization"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "sourceItemization"

    invoke-static {p2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "maxRefundedQuantity"

    invoke-static {p3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "roundingMode"

    invoke-static {p4, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "res"

    move-object v5, p5

    invoke-static {p5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "perUnitFormatter"

    move-object/from16 v6, p6

    invoke-static {v6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "seats"

    move-object/from16 v7, p7

    invoke-static {v7, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    iget-object v4, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_quantity:Ljava/lang/String;

    const-string v8, "residualItemization.source_itemization_quantity"

    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Ljava/math/BigDecimal;

    invoke-direct {v8, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 103
    iget-object v4, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v9, "residualItemization.itemization.quantity"

    invoke-static {v4, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v9, Ljava/math/BigDecimal;

    invoke-direct {v9, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v8, v9}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    const-string v8, "this.subtract(other)"

    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p3, v4}, Ljava/math/BigDecimal;->min(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v4

    .line 105
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v4, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-gtz v2, :cond_0

    .line 106
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 111
    :cond_0
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    invoke-static {v2}, Lcom/squareup/quantity/ItemQuantities;->isUnitPriced(Lcom/squareup/protos/client/bills/Itemization;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_1
    iget-object v2, v0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 116
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const-string v8, "sourceItemization.quantity"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v8, Ljava/math/BigDecimal;

    invoke-direct {v8, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 114
    invoke-static {v2, v8, p4}, Lcom/squareup/money/MoneyMath;->divide(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 113
    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    move-object v2, v0

    .line 120
    move-object v0, p0

    check-cast v0, Lcom/squareup/activity/refund/ItemizationDetails$Companion;

    const-string v3, "moneyPerItem"

    .line 122
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "quantity"

    .line 123
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    move-object v3, v4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    .line 120
    invoke-direct/range {v0 .. v6}, Lcom/squareup/activity/refund/ItemizationDetails$Companion;->detailsWithMoneyPerItem(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final ofRefundedTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "residualTip"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "residualTender"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "res"

    move-object/from16 v3, p3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    .line 185
    iget-object v2, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    .line 188
    iget-object v2, v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 189
    iget-object v4, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    .line 187
    invoke-static {v2, v4}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 191
    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 192
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 195
    :cond_0
    new-instance v15, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 196
    iget-object v4, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v7, v4, Lcom/squareup/protos/client/bills/TipLineItem;->tender_server_token:Ljava/lang/String;

    .line 197
    invoke-static/range {p2 .. p3}, Lcom/squareup/activity/refund/ItemizationDetailsKt;->tipName(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 198
    invoke-static {v2}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    const-string v1, "negate(refundedTipMoney)"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    .line 199
    iget-object v0, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->source_bill_server_token:Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x7d4

    const/4 v14, 0x0

    move-object v1, v15

    move-object v2, v3

    move-object v3, v5

    move-object v5, v0

    .line 195
    invoke-direct/range {v1 .. v14}, Lcom/squareup/activity/refund/ItemizationDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/math/BigDecimal;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 194
    invoke-static {v15}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 185
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 184
    :cond_2
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
