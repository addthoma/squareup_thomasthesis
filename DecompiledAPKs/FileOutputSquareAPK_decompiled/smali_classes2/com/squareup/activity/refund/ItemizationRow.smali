.class public final Lcom/squareup/activity/refund/ItemizationRow;
.super Landroid/widget/LinearLayout;
.source "ItemizationRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/ItemizationRow$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000c\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001aB\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010\u0018\u001a\u00020\u0019R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/activity/refund/ItemizationRow;",
        "Landroid/widget/LinearLayout;",
        "parent",
        "formatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V",
        "amountField",
        "Landroid/widget/TextView;",
        "checkbox",
        "Landroid/widget/CompoundButton;",
        "value",
        "",
        "checked",
        "getChecked",
        "()Z",
        "setChecked",
        "(Z)V",
        "getFormatter",
        "()Lcom/squareup/text/Formatter;",
        "nameField",
        "noteField",
        "getParent",
        "()Landroid/widget/LinearLayout;",
        "toggle",
        "",
        "Factory",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private amountField:Landroid/widget/TextView;

.field private checkbox:Landroid/widget/CompoundButton;

.field private checked:Z

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private nameField:Landroid/widget/TextView;

.field private noteField:Landroid/widget/TextView;

.field private final parent:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/widget/LinearLayout;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/LinearLayout;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->parent:Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/squareup/activity/refund/ItemizationRow;->formatter:Lcom/squareup/text/Formatter;

    .line 49
    invoke-virtual {p0}, Lcom/squareup/activity/refund/ItemizationRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$dimen;->marin_title_subtitle_row_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/ItemizationRow;->setMinimumHeight(I)V

    .line 50
    iget-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->parent:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p2, Lcom/squareup/activity/R$layout;->itemization_row:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 52
    sget p1, Lcom/squareup/activity/R$id;->name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->nameField:Landroid/widget/TextView;

    .line 53
    sget p1, Lcom/squareup/activity/R$id;->note:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->noteField:Landroid/widget/TextView;

    .line 54
    sget p1, Lcom/squareup/activity/R$id;->amount:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->amountField:Landroid/widget/TextView;

    .line 55
    sget p1, Lcom/squareup/activity/R$id;->checkbox:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CompoundButton;

    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->checkbox:Landroid/widget/CompoundButton;

    .line 57
    iget-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->checkbox:Landroid/widget/CompoundButton;

    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_button_check:I

    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setBackgroundResource(I)V

    return-void
.end method

.method public static final synthetic access$getAmountField$p(Lcom/squareup/activity/refund/ItemizationRow;)Landroid/widget/TextView;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/activity/refund/ItemizationRow;->amountField:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic access$getNameField$p(Lcom/squareup/activity/refund/ItemizationRow;)Landroid/widget/TextView;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/activity/refund/ItemizationRow;->nameField:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic access$getNoteField$p(Lcom/squareup/activity/refund/ItemizationRow;)Landroid/widget/TextView;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/activity/refund/ItemizationRow;->noteField:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic access$setAmountField$p(Lcom/squareup/activity/refund/ItemizationRow;Landroid/widget/TextView;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->amountField:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$setNameField$p(Lcom/squareup/activity/refund/ItemizationRow;Landroid/widget/TextView;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->nameField:Landroid/widget/TextView;

    return-void
.end method

.method public static final synthetic access$setNoteField$p(Lcom/squareup/activity/refund/ItemizationRow;Landroid/widget/TextView;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->noteField:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final getChecked()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/activity/refund/ItemizationRow;->checked:Z

    return v0
.end method

.method public final getFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationRow;->formatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public final getParent()Landroid/widget/LinearLayout;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationRow;->parent:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final setChecked(Z)V
    .locals 1

    .line 43
    iput-boolean p1, p0, Lcom/squareup/activity/refund/ItemizationRow;->checked:Z

    .line 44
    iget-object v0, p0, Lcom/squareup/activity/refund/ItemizationRow;->checkbox:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method public final toggle()V
    .locals 1

    .line 61
    iget-boolean v0, p0, Lcom/squareup/activity/refund/ItemizationRow;->checked:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/activity/refund/ItemizationRow;->setChecked(Z)V

    return-void
.end method
