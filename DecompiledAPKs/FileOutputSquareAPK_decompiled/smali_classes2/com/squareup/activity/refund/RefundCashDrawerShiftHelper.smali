.class public final Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;
.super Ljava/lang/Object;
.source "RefundCashDrawerShiftHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;",
        "",
        "()V",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;->Companion:Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final addRefundTendersToManagedCashDrawer(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;->Companion:Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;->addRefundTendersToManagedCashDrawer(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;)V

    return-void
.end method

.method public static final shouldOpenDrawerForRefund(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper;->Companion:Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/activity/refund/RefundCashDrawerShiftHelper$Companion;->shouldOpenDrawerForRefund(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    return p0
.end method
