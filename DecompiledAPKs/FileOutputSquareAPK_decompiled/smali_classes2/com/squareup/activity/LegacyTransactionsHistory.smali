.class public Lcom/squareup/activity/LegacyTransactionsHistory;
.super Ljava/lang/Object;
.source "LegacyTransactionsHistory.java"


# instance fields
.field private currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

.field private final mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

.field private final onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;


# direct methods
.method public constructor <init>(Lcom/squareup/activity/AllTransactionsHistoryLoader;Lcom/squareup/activity/SearchResultsLoader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 46
    iput-object p1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    .line 47
    iput-object p2, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    .line 49
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->reset()V

    return-void
.end method

.method private addRefundBill(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 238
    invoke-virtual {p1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getBills()Ljava/util/List;

    move-result-object v0

    .line 240
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/BillHistory;

    .line 241
    iget-object v2, p2, Lcom/squareup/billhistory/model/BillHistory;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/billhistory/model/BillHistory;->containsBillWithId(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    new-instance v2, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 244
    invoke-virtual {p2}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setRelatedBills(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v1

    .line 245
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    .line 242
    invoke-virtual {p1, v1}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->replaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)V

    goto :goto_0

    .line 250
    :cond_1
    invoke-virtual {p1, p2}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->addOrReplace(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method private fireChanged()V
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private setCurrentLoader(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    if-eq p1, v0, :cond_0

    .line 256
    iput-object p1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    .line 257
    invoke-direct {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->fireChanged()V

    :cond_0
    return-void
.end method


# virtual methods
.method public addRefundBill(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-direct {p0, v0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->addRefundBill(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 217
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-direct {p0, v0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->addRefundBill(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;Lcom/squareup/billhistory/model/BillHistory;)V

    .line 220
    :cond_0
    invoke-direct {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->fireChanged()V

    return-void
.end method

.method public clearQuery()V
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-direct {p0, v0}, Lcom/squareup/activity/LegacyTransactionsHistory;->setCurrentLoader(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/SearchResultsLoader;->reset()V

    return-void
.end method

.method public getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/SearchResultsLoader;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getBills()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getBills()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getLastError()Lcom/squareup/activity/LoaderError;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getLastError()Lcom/squareup/activity/LoaderError;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    iget-object v1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 173
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/activity/SearchResultsLoader;->getQueryForDisplay()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getState()Lcom/squareup/activity/LoaderState;
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->getState()Lcom/squareup/activity/LoaderState;

    move-result-object v0

    return-object v0
.end method

.method public getTender(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 3

    .line 127
    invoke-virtual {p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getTenderId()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 138
    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    return-object p1

    .line 130
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No bill with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " has been loaded."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public hasBillWithId(Lcom/squareup/billhistory/model/BillHistoryId;)Z
    .locals 0

    .line 110
    invoke-virtual {p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->getBill(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hasMore()Z
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->hasMore()Z

    move-result v0

    return v0
.end method

.method public hasQuery()Z
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    iget-object v1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTextQuery()Z
    .locals 1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/SearchResultsLoader;->hasTextQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$onChanged$0$LegacyTransactionsHistory(Lkotlin/Unit;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 56
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public synthetic lambda$onChanged$1$LegacyTransactionsHistory(Lkotlin/Unit;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 58
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->hasQuery()Z

    move-result p1

    return p1
.end method

.method public load()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->load()Ljava/util/UUID;

    return-void
.end method

.method public loadMore()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/AbstractTransactionsHistoryLoader;->loadMore()V

    return-void
.end method

.method public onChanged()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->onChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    .line 55
    invoke-virtual {v1}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->onChanged()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/activity/-$$Lambda$LegacyTransactionsHistory$9O4GI4a35QiYtWedU7CIBV2Eqr4;

    invoke-direct {v2, p0}, Lcom/squareup/activity/-$$Lambda$LegacyTransactionsHistory$9O4GI4a35QiYtWedU7CIBV2Eqr4;-><init>(Lcom/squareup/activity/LegacyTransactionsHistory;)V

    .line 56
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    .line 57
    invoke-virtual {v2}, Lcom/squareup/activity/SearchResultsLoader;->onChanged()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/activity/-$$Lambda$LegacyTransactionsHistory$-uJ65XXyGFhY2r_o86rG2VIcBm8;

    invoke-direct {v3, p0}, Lcom/squareup/activity/-$$Lambda$LegacyTransactionsHistory$-uJ65XXyGFhY2r_o86rG2VIcBm8;-><init>(Lcom/squareup/activity/LegacyTransactionsHistory;)V

    .line 58
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    .line 53
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public replaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 229
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->replaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/SearchResultsLoader;->replaceIfPresent(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 231
    invoke-direct {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->fireChanged()V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->mainLoader:Lcom/squareup/activity/AllTransactionsHistoryLoader;

    iput-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->currentLoader:Lcom/squareup/activity/AbstractTransactionsHistoryLoader;

    .line 82
    invoke-virtual {v0}, Lcom/squareup/activity/AllTransactionsHistoryLoader;->reset()V

    .line 83
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0}, Lcom/squareup/activity/SearchResultsLoader;->reset()V

    return-void
.end method

.method public setQuery(Lcom/squareup/Card;)V
    .locals 1

    if-nez p1, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->clearQuery()V

    goto :goto_0

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/SearchResultsLoader;->setQuery(Lcom/squareup/Card;)V

    .line 203
    iget-object p1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-direct {p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->setCurrentLoader(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    :goto_0
    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 1

    .line 178
    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->clearQuery()V

    goto :goto_0

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/SearchResultsLoader;->setQuery(Ljava/lang/String;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-direct {p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->setCurrentLoader(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    :goto_0
    return-void
.end method

.method public setQueryWithInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V
    .locals 1

    if-nez p1, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/squareup/activity/LegacyTransactionsHistory;->clearQuery()V

    goto :goto_0

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/activity/SearchResultsLoader;->setQueryInstrumentSearch(Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V

    .line 193
    iget-object p1, p0, Lcom/squareup/activity/LegacyTransactionsHistory;->searchResultsLoader:Lcom/squareup/activity/SearchResultsLoader;

    invoke-direct {p0, p1}, Lcom/squareup/activity/LegacyTransactionsHistory;->setCurrentLoader(Lcom/squareup/activity/AbstractTransactionsHistoryLoader;)V

    :goto_0
    return-void
.end method
