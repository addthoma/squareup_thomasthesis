.class public interface abstract Lcom/squareup/barcodescanners/BarcodeScanner$Listener;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/BarcodeScanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract barcodeScannerConnected()V
.end method

.method public abstract barcodeScannerDisconnected()V
.end method

.method public abstract characterScanned(C)V
.end method
