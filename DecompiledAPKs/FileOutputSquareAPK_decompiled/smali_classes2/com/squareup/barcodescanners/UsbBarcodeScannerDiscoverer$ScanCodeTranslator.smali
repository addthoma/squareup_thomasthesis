.class Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;
.super Ljava/lang/Object;
.source "UsbBarcodeScannerDiscoverer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ScanCodeTranslator"
.end annotation


# static fields
.field private static final letters:[C

.field private static final numberRowNormal:[C

.field private static final numberRowShifted:[C

.field private static final symbolsNormal:[C

.field private static final symbolsShifted:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "abcdefghijklmnopqrstuvwxyz"

    .line 398
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->letters:[C

    const-string v0, "1234567890"

    .line 401
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->numberRowNormal:[C

    const-string v0, "!@#$%^&*()"

    .line 404
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->numberRowShifted:[C

    const-string v0, " -=[]\\ ;\'`,./"

    .line 408
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->symbolsNormal:[C

    const-string v0, " _+{}| :\"~<>?"

    .line 412
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->symbolsShifted:[C

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static translateScanCodeToChar(BZ)C
    .locals 2

    const/16 v0, 0x1e

    if-lt p0, v0, :cond_1

    const/16 v1, 0x27

    if-gt p0, v1, :cond_1

    sub-int/2addr p0, v0

    if-eqz p1, :cond_0

    .line 424
    sget-object p1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->numberRowShifted:[C

    aget-char p0, p1, p0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->numberRowNormal:[C

    aget-char p0, p1, p0

    :goto_0
    return p0

    :cond_1
    const/4 v0, 0x4

    if-lt p0, v0, :cond_3

    const/16 v1, 0x1d

    if-gt p0, v1, :cond_3

    .line 426
    sget-object v1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->letters:[C

    sub-int/2addr p0, v0

    aget-char p0, v1, p0

    if-eqz p1, :cond_2

    .line 427
    invoke-static {p0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result p0

    :cond_2
    return p0

    :cond_3
    const/16 v0, 0x2c

    if-lt p0, v0, :cond_5

    const/16 v1, 0x38

    if-gt p0, v1, :cond_5

    sub-int/2addr p0, v0

    if-eqz p1, :cond_4

    .line 430
    sget-object p1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->symbolsShifted:[C

    aget-char p0, p1, p0

    goto :goto_1

    :cond_4
    sget-object p1, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$ScanCodeTranslator;->symbolsNormal:[C

    aget-char p0, p1, p0

    :goto_1
    return p0

    :cond_5
    const/4 p0, 0x0

    return p0
.end method
