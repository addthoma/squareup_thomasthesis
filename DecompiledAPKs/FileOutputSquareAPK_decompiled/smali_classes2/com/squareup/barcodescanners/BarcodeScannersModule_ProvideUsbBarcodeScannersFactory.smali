.class public final Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;
.super Ljava/lang/Object;
.source "BarcodeScannersModule_ProvideUsbBarcodeScannersFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;",
        ">;"
    }
.end annotation


# instance fields
.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field

.field private final usbManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->usbDiscovererProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->usbManagerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUsbBarcodeScanners(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/util/Res;)Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
    .locals 0

    .line 53
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/barcodescanners/BarcodeScannersModule;->provideUsbBarcodeScanners(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/util/Res;)Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/usb/UsbDiscoverer;

    iget-object v2, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->usbManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/hardware/usb/UsbManager;

    iget-object v3, p0, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->provideUsbBarcodeScanners(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/usb/UsbDiscoverer;Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/util/Res;)Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/barcodescanners/BarcodeScannersModule_ProvideUsbBarcodeScannersFactory;->get()Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;

    move-result-object v0

    return-object v0
.end method
