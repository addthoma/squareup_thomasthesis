.class Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;
.super Ljava/lang/Object;
.source "UsbBarcodeScannerDiscoverer.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScanner;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UsbBarcodeScanner"
.end annotation


# static fields
.field private static final AFTER_CONNECTION_IGNORE_REPORT_TIME_MS:I = 0x1f4

.field private static final HID_REPORT_KEYCODE_BYTE_INDEX:I = 0x2

.field private static final HID_REPORT_MODIFIER_BYTE_INDEX:I = 0x0

.field private static final HONEYWELL_MANUFACTURER_NAME:Ljava/lang/String; = "Honeywell"

.field private static final REPORT_TIMEOUT_MS:I = 0x3e8

.field private static final SYMBOL_MANUFACTURER_NAME:Ljava/lang/String; = "Symbol"


# instance fields
.field private final backgroundThreadExecutor:Ljava/util/concurrent/Executor;

.field private final deviceName:Ljava/lang/String;

.field private listener:Lcom/squareup/barcodescanners/BarcodeScanner$Listener;

.field private final usbDevice:Landroid/hardware/usb/UsbDevice;

.field private usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

.field private final usbManager:Lcom/squareup/hardware/usb/UsbManager;


# direct methods
.method constructor <init>(Lcom/squareup/hardware/usb/UsbManager;Landroid/hardware/usb/UsbDevice;Ljava/util/concurrent/Executor;Lcom/squareup/util/Res;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p2, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    .line 45
    iput-object p3, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    .line 46
    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    .line 47
    invoke-static {p2, p4}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->lookupDeviceName(Landroid/hardware/usb/UsbDevice;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->deviceName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDeviceConnection;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Landroid/hardware/usb/UsbDevice;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;)Lcom/squareup/barcodescanners/BarcodeScanner$Listener;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->listener:Lcom/squareup/barcodescanners/BarcodeScanner$Listener;

    return-object p0
.end method

.method static lookupDeviceName(Landroid/hardware/usb/UsbDevice;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 7

    .line 217
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    .line 218
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v1

    const-string v2, "Orbit MS7120"

    const/4 v3, 0x0

    const/16 v4, 0xc2e

    const-string v5, "Honeywell"

    const-string v6, "Symbol"

    if-ne v0, v4, :cond_6

    const/16 v0, 0xb01

    if-ne v1, v0, :cond_0

    const-string v2, "1300G"

    goto :goto_1

    :cond_0
    const/16 v0, 0x200

    if-ne v1, v0, :cond_1

    goto :goto_1

    :cond_1
    const/16 v0, 0x204

    if-ne v1, v0, :cond_2

    goto :goto_1

    :cond_2
    const/16 v0, 0x901

    if-ne v1, v0, :cond_3

    const-string v2, "Xenon 1900"

    goto :goto_1

    :cond_3
    const/16 v0, 0xa01

    if-ne v1, v0, :cond_4

    const-string v2, "Voyager 1200G"

    goto :goto_1

    :cond_4
    const/16 v0, 0xb41

    if-ne v1, v0, :cond_5

    const-string v2, "Voyager 1250G"

    goto :goto_1

    :cond_5
    move-object v2, v3

    goto :goto_1

    :cond_6
    const/16 v2, 0x5e0

    if-ne v0, v2, :cond_8

    const/16 v0, 0x1200

    if-ne v1, v0, :cond_7

    const-string v2, "LS2208"

    goto :goto_0

    :cond_7
    move-object v2, v3

    :goto_0
    move-object v5, v6

    goto :goto_1

    :cond_8
    move-object v2, v3

    move-object v5, v2

    :goto_1
    if-nez v5, :cond_9

    .line 243
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown vendor ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 244
    sget p0, Lcom/squareup/hardware/R$string;->scanner_unknown_vendor:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_9
    const-string/jumbo v0, "vendor"

    if-nez v2, :cond_a

    .line 248
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown product ID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 249
    sget p0, Lcom/squareup/hardware/R$string;->scanner_unknown_product:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 250
    invoke-virtual {p0, v0, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 251
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 252
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 255
    :cond_a
    sget p0, Lcom/squareup/hardware/R$string;->scanner_vendor_and_product:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 256
    invoke-virtual {p0, v0, v5}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string p1, "product"

    .line 257
    invoke-virtual {p0, p1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 258
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 259
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getConnectionType()Ljava/lang/String;
    .locals 1

    const-string v0, "USB"

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getManufacturerName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method setListener(Lcom/squareup/barcodescanners/BarcodeScanner$Listener;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->listener:Lcom/squareup/barcodescanners/BarcodeScanner$Listener;

    return-void
.end method

.method public start()V
    .locals 5

    .line 82
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    .line 90
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "No interfaces for %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbManager:Lcom/squareup/hardware/usb/UsbManager;

    iget-object v3, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-interface {v0, v3}, Lcom/squareup/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 95
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez v0, :cond_1

    new-array v0, v1, [Ljava/lang/Object;

    .line 98
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "Could not open connection to %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    .line 105
    iget-object v4, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 106
    invoke-virtual {v4, v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-nez v0, :cond_2

    new-array v0, v1, [Ljava/lang/Object;

    .line 110
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "Could not claim interface for %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->listener:Lcom/squareup/barcodescanners/BarcodeScanner$Listener;

    invoke-interface {v0}, Lcom/squareup/barcodescanners/BarcodeScanner$Listener;->barcodeScannerConnected()V

    .line 116
    new-instance v0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;

    invoke-direct {v0, p0, v3}, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner$1;-><init>(Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;Landroid/hardware/usb/UsbEndpoint;)V

    .line 202
    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->backgroundThreadExecutor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public stop()V
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0}, Landroid/hardware/usb/UsbDeviceConnection;->close()V

    const/4 v0, 0x0

    .line 208
    iput-object v0, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDeviceConnection:Landroid/hardware/usb/UsbDeviceConnection;

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UsbBarcodeScanner{usbDevice="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->usbDevice:Landroid/hardware/usb/UsbDevice;

    .line 76
    invoke-virtual {v1}, Landroid/hardware/usb/UsbDevice;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", deviceName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/barcodescanners/UsbBarcodeScannerDiscoverer$UsbBarcodeScanner;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
