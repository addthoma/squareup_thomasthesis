.class public final Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;
.super Ljava/lang/Object;
.source "CapitalLoadingLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/capital/flexloan/CapitalLoadingScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\rB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/capital/flexloan/CapitalLoadingScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;->view:Landroid/view/View;

    .line 20
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/capital/flexloan/CapitalLoadingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object p2, p0, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 26
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 30
    iget-object p2, p0, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner$showRendering$1;-><init>(Lcom/squareup/capital/flexloan/CapitalLoadingScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/CapitalLoadingLayoutRunner;->showRendering(Lcom/squareup/capital/flexloan/CapitalLoadingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
