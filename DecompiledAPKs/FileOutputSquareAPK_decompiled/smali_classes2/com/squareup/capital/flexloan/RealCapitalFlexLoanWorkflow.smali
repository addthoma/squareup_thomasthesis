.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCapitalFlexLoanWorkflow.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;,
        Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalFlexLoanWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalFlexLoanWorkflow.kt\ncom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 4 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,227:1\n41#2:228\n56#2,2:229\n276#3:231\n149#4,5:232\n149#4,5:237\n149#4,5:242\n149#4,5:247\n149#4,5:252\n*E\n*S KotlinDebug\n*F\n+ 1 RealCapitalFlexLoanWorkflow.kt\ncom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow\n*L\n76#1:228\n76#1,2:229\n76#1:231\n81#1,5:232\n86#1,5:237\n102#1,5:242\n113#1,5:247\n135#1,5:252\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 02\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002:\u0002/0B?\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u001f\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016\u00a2\u0006\u0002\u0010\u001dJ\u0016\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\"H\u0002J\u0016\u0010#\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010$\u001a\u00020\"H\u0002J\u0016\u0010%\u001a\u0008\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\"H\u0002JS\u0010&\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\'\u001a\u00020\u00042\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030)H\u0016\u00a2\u0006\u0002\u0010*J\u0010\u0010+\u001a\u00020\u001c2\u0006\u0010\'\u001a\u00020\u0004H\u0016J\u000c\u0010,\u001a\u00020-*\u00020.H\u0002R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "capitalPlanWorkflow",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;",
        "capitalOfferWorkflow",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;",
        "capitalApplicationPendingWorkflow",
        "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;",
        "capitalNoOfferWorkflow",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;",
        "capitalErrorWorkflow",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
        "capitalFlexLoanDataRepository",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
        "analytics",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
        "logViewApplicationPending",
        "Lcom/squareup/workflow/Worker;",
        "",
        "offerId",
        "",
        "logViewCapitalStateError",
        "errorCode",
        "logViewOffer",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "toState",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;",
        "Action",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ANALYTICS_KEY:Ljava/lang/String; = "CapitalFlexLoanWorkflowAnalyticsWorker"

.field public static final Companion:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Companion;


# instance fields
.field private final analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

.field private final capitalApplicationPendingWorkflow:Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;

.field private final capitalErrorWorkflow:Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

.field private final capitalFlexLoanDataRepository:Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

.field private final capitalNoOfferWorkflow:Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;

.field private final capitalOfferWorkflow:Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;

.field private final capitalPlanWorkflow:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->Companion:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "capitalPlanWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalOfferWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalApplicationPendingWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalNoOfferWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalErrorWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capitalFlexLoanDataRepository"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalPlanWorkflow:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalOfferWorkflow:Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalApplicationPendingWorkflow:Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;

    iput-object p4, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalNoOfferWorkflow:Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;

    iput-object p5, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalErrorWorkflow:Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

    iput-object p6, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalFlexLoanDataRepository:Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

    iput-object p7, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;)Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->analytics:Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    return-object p0
.end method

.method public static final synthetic access$toState(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->toState(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    move-result-object p0

    return-object p0
.end method

.method private final logViewApplicationPending(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 170
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$logViewApplicationPending$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$logViewApplicationPending$1;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final logViewCapitalStateError(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 173
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$logViewCapitalStateError$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$logViewCapitalStateError$1;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final logViewOffer(Ljava/lang/String;)Lcom/squareup/workflow/Worker;
    .locals 3

    .line 167
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$logViewOffer$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$logViewOffer$1;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    return-object p1
.end method

.method private final toState(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;
    .locals 4

    .line 144
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$NoOffer;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowNoOffer;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowNoOffer;

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    goto/16 :goto_0

    .line 145
    :cond_0
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    if-eqz v0, :cond_1

    .line 146
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowFlexOffer;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    goto/16 :goto_0

    .line 147
    :cond_1
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    if-eqz v0, :cond_2

    .line 148
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowApplicationPending;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    goto :goto_0

    .line 149
    :cond_2
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;

    if-eqz v0, :cond_3

    .line 150
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;

    .line 151
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v1, v1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    const-string v2, "customer.preview_active_plan.plan_id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    sget-object v2, Lcom/squareup/capital/flexloan/CapitalFlexPlanType;->CURRENT_PLAN:Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    .line 153
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$CurrentPlan;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    const-string v3, "customer.preview_active_\u2026fc3986_plan_servicing_url"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;-><init>(Ljava/lang/String;Lcom/squareup/capital/flexloan/CapitalFlexPlanType;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    goto :goto_0

    .line 155
    :cond_3
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;

    if-eqz v0, :cond_4

    .line 156
    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;

    .line 157
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v1, v1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->plan_id:Ljava/lang/String;

    const-string v2, "customer.preview_latest_completed_plan.plan_id"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    sget-object v2, Lcom/squareup/capital/flexloan/CapitalFlexPlanType;->PREVIOUS_PLAN:Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    .line 159
    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$PreviousPlan;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewPlan;->rfc3986_plan_servicing_url:Ljava/lang/String;

    const-string v3, "customer.preview_latest_\u2026fc3986_plan_servicing_url"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowPlan;-><init>(Ljava/lang/String;Lcom/squareup/capital/flexloan/CapitalFlexPlanType;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    goto :goto_0

    .line 161
    :cond_4
    instance-of v0, p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$ShowError;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    goto :goto_0

    .line 162
    :cond_5
    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$DoNothing;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action$DoNothing;

    check-cast p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$Action;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    sget-object p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FetchingFlexStatus;->INSTANCE:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FetchingFlexStatus;

    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->render(Lkotlin/Unit;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of p1, p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FetchingFlexStatus;

    const-string v0, ""

    if-eqz p1, :cond_1

    .line 76
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalFlexLoanDataRepository:Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

    invoke-interface {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;->fetchFlexLoanStatus()Lio/reactivex/Observable;

    move-result-object p1

    .line 228
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 230
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 231
    const-class p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 77
    new-instance p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$1;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 75
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 81
    new-instance p1, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;

    sget-object p2, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$2;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$2;

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, p2}, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 233
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 234
    const-class p3, Lcom/squareup/capital/flexloan/CapitalLoadingScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 235
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 233
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 82
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 230
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 84
    :cond_1
    instance-of p1, p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$NoOffer;

    if-eqz p1, :cond_2

    .line 85
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalNoOfferWorkflow:Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$3;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$3;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 238
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 239
    const-class p3, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 240
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 238
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 87
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 89
    :cond_2
    instance-of p1, p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$OfferAvailable;

    if-eqz p1, :cond_3

    .line 91
    check-cast p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$OfferAvailable;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$OfferAvailable;->getOffer()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->offer_set_id:Ljava/lang/String;

    const-string v1, "state.offer.customer.pre\u2026w_main_offer.offer_set_id"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->logViewOffer(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const-string v1, "CapitalFlexLoanWorkflowAnalyticsWorker.logViewOffer"

    .line 90
    invoke-static {p3, p1, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalOfferWorkflow:Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$OfferAvailable;->getOffer()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Offer;

    move-result-object v3

    const/4 v4, 0x0

    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$4;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$4;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 243
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 244
    const-class p3, Lcom/squareup/capital/flexloan/offer/CapitalOfferScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 245
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 243
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 103
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 105
    :cond_3
    instance-of p1, p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$ApplicationPending;

    if-eqz p1, :cond_4

    .line 108
    check-cast p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$ApplicationPending;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$ApplicationPending;->getFinancingRequest()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$FinancingRequest;->getCustomer()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/Customer;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iget-object p1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;->financing_request_id:Ljava/lang/String;

    const-string p2, "state.financingRequest.c\u2026view.financing_request_id"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->logViewApplicationPending(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const-string p2, "CapitalFlexLoanWorkflowAnalyticsWorker.logApplicationPending"

    .line 106
    invoke-static {p3, p1, p2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 112
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalApplicationPendingWorkflow:Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$5;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$5;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 248
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 249
    const-class p3, Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 250
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 248
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 114
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 116
    :cond_4
    instance-of p1, p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;

    if-eqz p1, :cond_5

    .line 117
    new-instance v3, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;

    .line 118
    check-cast p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;->getId()Ljava/lang/String;

    move-result-object p1

    .line 119
    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;->getPlanType()Lcom/squareup/capital/flexloan/CapitalFlexPlanType;

    move-result-object v0

    .line 120
    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$FlexPlanExists;->getPlanUrl()Ljava/lang/String;

    move-result-object p2

    .line 117
    invoke-direct {v3, p1, v0, p2}, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflowProps;-><init>(Ljava/lang/String;Lcom/squareup/capital/flexloan/CapitalFlexPlanType;Ljava/lang/String;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalPlanWorkflow:Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v4, 0x0

    .line 126
    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$6;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$6;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 123
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 128
    :cond_5
    instance-of p1, p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$CapitalError;

    if-eqz p1, :cond_6

    .line 130
    check-cast p2, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$CapitalError;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$CapitalError;->getFailure()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;->getFailureData()Lcom/squareup/capital/flexloan/FailureData;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/FailureData;->getErrorCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->logViewCapitalStateError(Ljava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object p1

    const-string v1, "CapitalFlexLoanWorkflowAnalyticsWorker.logCapitalError"

    .line 129
    invoke-static {p3, p1, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->capitalErrorWorkflow:Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

    move-object v2, p1

    check-cast v2, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p2}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState$CapitalError;->getFailure()Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/capital/flexloan/CapitalFlexLoanStatus$Failure;->getFailureData()Lcom/squareup/capital/flexloan/FailureData;

    move-result-object v3

    const/4 v4, 0x0

    sget-object p1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$7;->INSTANCE:Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow$render$7;

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 253
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 254
    const-class p3, Lcom/squareup/capital/flexloan/error/CapitalErrorScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 255
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 253
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 136
    sget-object p1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p2, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;->snapshotState(Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
