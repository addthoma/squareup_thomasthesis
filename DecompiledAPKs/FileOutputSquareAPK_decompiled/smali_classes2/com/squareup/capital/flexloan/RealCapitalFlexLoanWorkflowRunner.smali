.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "RealCapitalFlexLoanWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lkotlin/Unit;",
        ">;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00030\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0014J\u0008\u0010\u0010\u001a\u00020\u0003H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "",
        "viewFactory",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;",
        "workflow",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;Lcom/squareup/ui/main/PosContainer;)V",
        "getWorkflow",
        "()Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;Lcom/squareup/ui/main/PosContainer;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;->Companion:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 17
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 18
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 15
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->workflow:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->workflow:Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->getWorkflow()Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 26
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public startWorkflow()V
    .locals 0

    .line 31
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
