.class public abstract Lcom/squareup/capital/flexloan/CapitalFlexLoanModule;
.super Ljava/lang/Object;
.source "CapitalFlexLoanModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H!J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0015H!J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H!J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0005\u001a\u00020\u001cH!J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0005\u001a\u00020\u001fH!J\u0010\u0010 \u001a\u00020!2\u0006\u0010\u0005\u001a\u00020\"H!J\u0010\u0010#\u001a\u00020$2\u0006\u0010\u0005\u001a\u00020%H!\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanModule;",
        "",
        "()V",
        "bindCapitalErrorWorkflow",
        "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
        "workflow",
        "Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;",
        "bindCapitalFlexLoanDataStore",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;",
        "capitalFlexLoanDataStore",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;",
        "bindCapitalFlexLoanOfferFormatter",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;",
        "capitalFlexLoanOfferFormatter",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;",
        "bindCapitalFlexLoanViewFactory",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;",
        "viewFactory",
        "Lcom/squareup/capital/flexloan/RealCapitalWorkflowViewFactory;",
        "bindCapitalFlexLoanWorkflow",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;",
        "bindCapitalFlexLoanWorkflowRunner",
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;",
        "workflowRunner",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;",
        "bindCapitalNoOfferWorkflow",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;",
        "Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;",
        "bindCapitalOfferWorkflow",
        "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;",
        "Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;",
        "bindCapitalPendingWorkflow",
        "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;",
        "Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;",
        "bindCapitalPlanWorkflow",
        "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;",
        "Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindCapitalErrorWorkflow(Lcom/squareup/capital/flexloan/error/RealCapitalErrorWorkflow;)Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanDataStore(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanRequestDataStore;)Lcom/squareup/capital/flexloan/CapitalFlexLoanRequestDataStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanOfferFormatter(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanOfferFormatter;)Lcom/squareup/capital/flexloan/CapitalFlexLoanOfferFormatter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanViewFactory(Lcom/squareup/capital/flexloan/RealCapitalWorkflowViewFactory;)Lcom/squareup/capital/flexloan/CapitalFlexLoanViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanWorkflow(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalFlexLoanWorkflowRunner(Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflowRunner;)Lcom/squareup/capital/flexloan/CapitalFlexLoanWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalNoOfferWorkflow(Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;)Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalOfferWorkflow(Lcom/squareup/capital/flexloan/offer/RealCapitalOfferWorkflow;)Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalPendingWorkflow(Lcom/squareup/capital/flexloan/applicationpending/RealCapitalApplicationPendingWorkflow;)Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCapitalPlanWorkflow(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
