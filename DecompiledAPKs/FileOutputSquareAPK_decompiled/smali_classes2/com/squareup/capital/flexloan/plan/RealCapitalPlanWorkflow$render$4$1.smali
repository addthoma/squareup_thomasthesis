.class final Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;
.super Ljava/lang/Object;
.source "RealCapitalPlanWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->invoke()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "otkUri",
        "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $planId:Ljava/lang/String;

.field final synthetic $targetUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;


# direct methods
.method constructor <init>(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;

    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->$targetUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->$planId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;)V
    .locals 2

    .line 102
    instance-of v0, p1, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$OtkUri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;

    iget-object v0, v0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    invoke-static {v0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->access$getBrowserLauncher$p(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    check-cast p1, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$OtkUri;

    invoke-virtual {p1}, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$OtkUri;->getUri()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 103
    :cond_0
    instance-of p1, p1, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri$None;

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;

    iget-object p1, p1, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4;->this$0:Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->$targetUrl:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->$planId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;->access$handleOtkFetchFailure(Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;

    invoke-virtual {p0, p1}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow$render$4$1;->accept(Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper$MultipassOtkUri;)V

    return-void
.end method
