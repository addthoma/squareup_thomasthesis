.class public final Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealCapitalPlanWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p6, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;",
            ">;)",
            "Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;"
        }
    .end annotation

    .line 51
    new-instance v7, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;)Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;
    .locals 8

    .line 57
    new-instance v7, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;-><init>(Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;
    .locals 7

    .line 43
    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;

    invoke-static/range {v1 .. v6}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->newInstance(Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/plan/CapitalPlanScreenMapper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;Lcom/squareup/api/multipassauth/onetimekey/MultipassOtkHelper;)Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow_Factory;->get()Lcom/squareup/capital/flexloan/plan/RealCapitalPlanWorkflow;

    move-result-object v0

    return-object v0
.end method
