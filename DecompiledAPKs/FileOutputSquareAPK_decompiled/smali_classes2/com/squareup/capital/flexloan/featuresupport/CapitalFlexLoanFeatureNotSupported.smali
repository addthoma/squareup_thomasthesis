.class public final Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupported;
.super Ljava/lang/Object;
.source "CapitalFlexLoanFeatureNotSupported.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupported;",
        "Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupport;",
        "()V",
        "supported",
        "",
        "getSupported",
        "()Z",
        "impl-unsupported_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final supported:Z


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSupported()Z
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureNotSupported;->supported:Z

    return v0
.end method
