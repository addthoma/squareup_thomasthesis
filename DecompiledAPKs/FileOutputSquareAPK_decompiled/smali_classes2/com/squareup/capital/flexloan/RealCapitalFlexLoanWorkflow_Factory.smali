.class public final Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealCapitalFlexLoanWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p7, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
            ">;)",
            "Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;"
        }
    .end annotation

    .line 57
    new-instance v8, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;
    .locals 9

    .line 64
    new-instance v8, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;-><init>(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;
    .locals 8

    .line 48
    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;

    invoke-static/range {v1 .. v7}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->newInstance(Lcom/squareup/capital/flexloan/plan/CapitalPlanWorkflow;Lcom/squareup/capital/flexloan/offer/CapitalOfferWorkflow;Lcom/squareup/capital/flexloan/applicationpending/CapitalApplicationPendingWorkflow;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;Lcom/squareup/capital/flexloan/error/CapitalErrorWorkflow;Lcom/squareup/capital/flexloan/CapitalFlexLoanDataRepository;Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;)Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow_Factory;->get()Lcom/squareup/capital/flexloan/RealCapitalFlexLoanWorkflow;

    move-result-object v0

    return-object v0
.end method
