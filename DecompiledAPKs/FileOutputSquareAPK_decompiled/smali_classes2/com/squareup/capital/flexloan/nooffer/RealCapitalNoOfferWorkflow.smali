.class public final Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealCapitalNoOfferWorkflow.kt"

# interfaces
.implements Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;",
        ">;",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCapitalNoOfferWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCapitalNoOfferWorkflow.kt\ncom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,23:1\n179#2,3:24\n199#2,4:27\n*E\n*S KotlinDebug\n*F\n+ 1 RealCapitalNoOfferWorkflow.kt\ncom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow\n*L\n14#1,3:24\n14#1,4:27\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0005J)\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00032\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u000cH\u0016\u00a2\u0006\u0002\u0010\rR\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00030\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;",
        "()V",
        "finish",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final finish:Lcom/squareup/workflow/WorkflowAction;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    .line 27
    new-instance v0, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow$$special$$inlined$action$1;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow$$special$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    .line 26
    iput-object v0, p0, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getFinish$p(Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;->finish:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method


# virtual methods
.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance p1, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;

    new-instance v0, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow$render$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow$render$1;-><init>(Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v0}, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;-><init>(Lkotlin/jvm/functions/Function0;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/nooffer/RealCapitalNoOfferWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;

    move-result-object p1

    return-object p1
.end method
