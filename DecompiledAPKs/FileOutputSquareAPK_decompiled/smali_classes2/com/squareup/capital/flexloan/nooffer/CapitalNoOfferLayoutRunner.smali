.class public final Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;
.super Ljava/lang/Object;
.source "CapitalNoOfferLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCapitalNoOfferLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CapitalNoOfferLayoutRunner.kt\ncom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner\n*L\n1#1,69:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002J\u0008\u0010\u0010\u001a\u00020\u000eH\u0002J\u0018\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "messageViewIcon",
        "Landroid/widget/ImageView;",
        "kotlin.jvm.PlatformType",
        "showBackButton",
        "",
        "configureActionBar",
        "",
        "rendering",
        "configureMessageView",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final messageViewIcon:Landroid/widget/ImageView;

.field private final showBackButton:Z

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->view:Landroid/view/View;

    .line 27
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 28
    iget-object p1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/capital/flexloan/impl/R$id;->capital_no_offer_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->messageViewIcon:Landroid/widget/ImageView;

    .line 29
    iget-object p1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->showBackButton:Z

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;)V
    .locals 4

    .line 50
    iget-object v0, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 41
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 43
    iget-boolean v2, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->showBackButton:Z

    if-eqz v2, :cond_0

    .line 44
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 49
    :goto_0
    new-instance p1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/capital/flexloan/impl/R$string;->square_capital:I

    invoke-direct {p1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final configureMessageView()V
    .locals 4

    .line 54
    sget v0, Lcom/squareup/capital/flexloan/impl/R$color;->capital_no_offer_icon_color:I

    .line 55
    iget-object v1, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->messageViewIcon:Landroid/widget/ImageView;

    const-string v2, "messageViewIcon"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    .line 56
    iget-object v3, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 57
    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    .line 55
    invoke-direct {v2, v0, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    check-cast v2, Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->configureActionBar(Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->configureMessageView()V

    .line 37
    iget-object p2, p0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner$showRendering$1;-><init>(Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferLayoutRunner;->showRendering(Lcom/squareup/capital/flexloan/nooffer/CapitalNoOfferScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
