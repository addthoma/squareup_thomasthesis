.class public abstract Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceReleaseModule;
.super Ljava/lang/Object;
.source "CapitalFlexLoanServiceReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004H!\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanServiceReleaseModule;",
        "",
        "()V",
        "bindsCapitalService",
        "Lcom/squareup/capital/flexloan/CapitalService;",
        "realService",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindsCapitalService(Lcom/squareup/capital/flexloan/CapitalService;)Lcom/squareup/capital/flexloan/CapitalService;
    .param p1    # Lcom/squareup/capital/flexloan/CapitalService;
        .annotation runtime Lcom/squareup/api/RealService;
        .end annotation
    .end param
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
