.class public Lcom/squareup/address/StatePickerScreen$DialogBuilder;
.super Ljava/lang/Object;
.source "StatePickerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/StatePickerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogBuilder"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final countryCode:Lcom/squareup/CountryCode;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field runner:Lcom/squareup/address/AddressLayoutRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/CountryCode;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->context:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->countryCode:Lcom/squareup/CountryCode;

    .line 55
    const-class p2, Lcom/squareup/address/StatePickerScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/address/StatePickerScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/address/StatePickerScreen$Component;->inject(Lcom/squareup/address/StatePickerScreen$DialogBuilder;)V

    return-void
.end method


# virtual methods
.method public build()Landroid/app/Dialog;
    .locals 4

    .line 59
    iget-object v0, p0, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->countryCode:Lcom/squareup/CountryCode;

    iget-object v1, p0, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->res:Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/address/StatePickerScreen$CountryStates;->forCountryCode(Lcom/squareup/CountryCode;Lcom/squareup/util/Res;)Lcom/squareup/address/StatePickerScreen$CountryStates;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 61
    invoke-static {v0}, Lcom/squareup/address/StatePickerScreen$CountryStates;->access$100(Lcom/squareup/address/StatePickerScreen$CountryStates;)[Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/squareup/address/-$$Lambda$StatePickerScreen$DialogBuilder$rRxzavTdm6E7zc-UHNPyb7d8Pfw;

    invoke-direct {v3, p0, v0}, Lcom/squareup/address/-$$Lambda$StatePickerScreen$DialogBuilder$rRxzavTdm6E7zc-UHNPyb7d8Pfw;-><init>(Lcom/squareup/address/StatePickerScreen$DialogBuilder;Lcom/squareup/address/StatePickerScreen$CountryStates;)V

    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$build$0$StatePickerScreen$DialogBuilder(Lcom/squareup/address/StatePickerScreen$CountryStates;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 62
    iget-object p2, p0, Lcom/squareup/address/StatePickerScreen$DialogBuilder;->runner:Lcom/squareup/address/AddressLayoutRunner;

    invoke-static {p1}, Lcom/squareup/address/StatePickerScreen$CountryStates;->access$200(Lcom/squareup/address/StatePickerScreen$CountryStates;)[Ljava/lang/String;

    move-result-object p1

    aget-object p1, p1, p3

    invoke-virtual {p2, p1}, Lcom/squareup/address/AddressLayoutRunner;->onPickState(Ljava/lang/String;)V

    return-void
.end method
