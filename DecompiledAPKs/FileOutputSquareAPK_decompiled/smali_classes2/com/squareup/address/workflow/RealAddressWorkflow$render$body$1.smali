.class final Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAddressWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/address/workflow/RealAddressWorkflow;->render(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/address/workflow/AddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/address/workflow/AddressState;",
        "+",
        "Lcom/squareup/address/Address;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/address/workflow/AddressState;",
        "Lcom/squareup/address/Address;",
        "newValue",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1;->this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation

    const-string v0, "newValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1;->this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;

    new-instance v1, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1$1;

    invoke-direct {v1, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/address/workflow/RealAddressWorkflow;->access$updateAddress(Lcom/squareup/address/workflow/RealAddressWorkflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1;->invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
