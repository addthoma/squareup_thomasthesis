.class public final Lcom/squareup/address/workflow/RealAddressWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAddressWorkflow.kt"

# interfaces
.implements Lcom/squareup/address/workflow/AddressWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;,
        Lcom/squareup/address/workflow/RealAddressWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/address/workflow/AddressProps;",
        "Lcom/squareup/address/workflow/AddressState;",
        "Lcom/squareup/address/Address;",
        "Lcom/squareup/address/workflow/AddressScreen;",
        ">;",
        "Lcom/squareup/address/workflow/AddressWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddressWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddressWorkflow.kt\ncom/squareup/address/workflow/RealAddressWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,245:1\n32#2,12:246\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddressWorkflow.kt\ncom/squareup/address/workflow/RealAddressWorkflow\n*L\n39#1,12:246\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 /2\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00012\u00020\u0006:\u0002/0B\u000f\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0012\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u001a\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00022\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J(\u0010\u0015\u001a\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\u0017\u0018\u00010\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J \u0010\u001b\u001a\u00020\u00032\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u0003H\u0016J\u001c\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000b2\u0006\u0010 \u001a\u00020!H\u0002J0\u0010\"\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u00032\u0016\u0010#\u001a\u0012\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040$j\u0002`%H\u0016J\u001a\u0010&\u001a\u00020\u001a2\u0006\u0010\'\u001a\u00020\u001a2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u0010\u0010(\u001a\u00020\u00142\u0006\u0010\u001e\u001a\u00020\u0003H\u0016J-\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000b2\u0017\u0010*\u001a\u0013\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030+\u00a2\u0006\u0002\u0008,H\u0002J\"\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000b2\u000c\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0017H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/address/workflow/RealAddressWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/address/workflow/AddressProps;",
        "Lcom/squareup/address/workflow/AddressState;",
        "Lcom/squareup/address/Address;",
        "Lcom/squareup/address/workflow/AddressScreen;",
        "Lcom/squareup/address/workflow/AddressWorkflow;",
        "addressService",
        "Lcom/squareup/server/address/AddressService;",
        "(Lcom/squareup/server/address/AddressService;)V",
        "showAddress",
        "Lcom/squareup/workflow/WorkflowAction;",
        "showChooseStateDialog",
        "getScrubber",
        "Lcom/squareup/text/PostalScrubber;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "loadPostalLocations",
        "Lcom/squareup/workflow/Worker;",
        "",
        "Lcom/squareup/server/address/PostalLocation;",
        "postalCode",
        "",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "onStatePicked",
        "stateInfo",
        "Lcom/squareup/address/workflow/StateInfo;",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/address/workflow/AddressContext;",
        "scrubPostal",
        "newValue",
        "snapshotState",
        "updateAddress",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "updateLocations",
        "locations",
        "Companion",
        "PostalLocationWorker",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final APARTMENT_KEY:Ljava/lang/String; = "apartment"

.field public static final CITY_KEY:Ljava/lang/String; = "city"

.field public static final Companion:Lcom/squareup/address/workflow/RealAddressWorkflow$Companion;

.field public static final POSTAL_KEY:Ljava/lang/String; = "postal"

.field public static final STATE_KEY:Ljava/lang/String; = "state"

.field public static final STREET_KEY:Ljava/lang/String; = "street"


# instance fields
.field private final addressService:Lcom/squareup/server/address/AddressService;

.field private final showAddress:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation
.end field

.field private final showChooseStateDialog:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/address/workflow/RealAddressWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/address/workflow/RealAddressWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/address/workflow/RealAddressWorkflow;->Companion:Lcom/squareup/address/workflow/RealAddressWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/address/AddressService;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "addressService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow;->addressService:Lcom/squareup/server/address/AddressService;

    .line 197
    sget-object p1, Lcom/squareup/address/workflow/RealAddressWorkflow$showChooseStateDialog$1;->INSTANCE:Lcom/squareup/address/workflow/RealAddressWorkflow$showChooseStateDialog$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v1, p1, v0, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow;->showChooseStateDialog:Lcom/squareup/workflow/WorkflowAction;

    .line 203
    sget-object p1, Lcom/squareup/address/workflow/RealAddressWorkflow$showAddress$1;->INSTANCE:Lcom/squareup/address/workflow/RealAddressWorkflow$showAddress$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v1, p1, v0, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow;->showAddress:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getAddressService$p(Lcom/squareup/address/workflow/RealAddressWorkflow;)Lcom/squareup/server/address/AddressService;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow;->addressService:Lcom/squareup/server/address/AddressService;

    return-object p0
.end method

.method public static final synthetic access$getShowAddress$p(Lcom/squareup/address/workflow/RealAddressWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow;->showAddress:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getShowChooseStateDialog$p(Lcom/squareup/address/workflow/RealAddressWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow;->showChooseStateDialog:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$onStatePicked(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/address/workflow/StateInfo;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow;->onStatePicked(Lcom/squareup/address/workflow/StateInfo;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateAddress(Lcom/squareup/address/workflow/RealAddressWorkflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow;->updateAddress(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateLocations(Lcom/squareup/address/workflow/RealAddressWorkflow;Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow;->updateLocations(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final getScrubber(Lcom/squareup/CountryCode;)Lcom/squareup/text/PostalScrubber;
    .locals 1

    if-nez p1, :cond_0

    goto :goto_0

    .line 172
    :cond_0
    sget-object v0, Lcom/squareup/address/workflow/RealAddressWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 175
    :goto_0
    sget-object p1, Lcom/squareup/address/workflow/NoopPostalScrubber;->INSTANCE:Lcom/squareup/address/workflow/NoopPostalScrubber;

    check-cast p1, Lcom/squareup/text/PostalScrubber;

    goto :goto_1

    .line 174
    :cond_1
    invoke-static {}, Lcom/squareup/text/PostalScrubber;->forCanada()Lcom/squareup/text/PostalScrubber;

    move-result-object p1

    const-string v0, "PostalScrubber.forCanada()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 173
    :cond_2
    invoke-static {}, Lcom/squareup/text/PostalScrubber;->forUs()Lcom/squareup/text/PostalScrubber;

    move-result-object p1

    const-string v0, "PostalScrubber.forUs()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p1
.end method

.method private final loadPostalLocations(Ljava/lang/String;Lcom/squareup/CountryCode;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/CountryCode;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;>;"
        }
    .end annotation

    .line 152
    invoke-direct {p0, p1, p2}, Lcom/squareup/address/workflow/RealAddressWorkflow;->scrubPostal(Ljava/lang/String;Lcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v0

    .line 153
    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    if-ne p2, v1, :cond_0

    invoke-static {v0}, Lcom/squareup/text/PostalCodes;->isUsZipCode(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 154
    new-instance p2, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;

    invoke-direct {p2, p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/workflow/Worker;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return-object p2
.end method

.method private final onStatePicked(Lcom/squareup/address/workflow/StateInfo;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/address/workflow/StateInfo;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation

    .line 209
    new-instance v0, Lcom/squareup/address/workflow/RealAddressWorkflow$onStatePicked$1;

    invoke-direct {v0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$onStatePicked$1;-><init>(Lcom/squareup/address/workflow/StateInfo;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow;->updateAddress(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final scrubPostal(Ljava/lang/String;Lcom/squareup/CountryCode;)Ljava/lang/String;
    .locals 1

    .line 164
    invoke-direct {p0, p2}, Lcom/squareup/address/workflow/RealAddressWorkflow;->getScrubber(Lcom/squareup/CountryCode;)Lcom/squareup/text/PostalScrubber;

    move-result-object p2

    .line 165
    invoke-virtual {p2, p1}, Lcom/squareup/text/PostalScrubber;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p2, p1}, Lcom/squareup/text/PostalScrubber;->shorten(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "scrubber.shorten(newValue)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 168
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final updateAddress(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/workflow/AddressState;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation

    .line 143
    new-instance v0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateAddress$1;

    invoke-direct {v0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$updateAddress$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final updateLocations(Ljava/util/List;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/address/Address;",
            ">;"
        }
    .end annotation

    .line 178
    new-instance v0, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;

    invoke-direct {v0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow$updateLocations$1;-><init>(Ljava/util/List;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow;->updateAddress(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/address/workflow/AddressState;
    .locals 3

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 246
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v2

    :goto_1
    if-eqz p2, :cond_3

    .line 251
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 253
    array-length v2, p2

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 254
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 255
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p2, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 257
    :cond_3
    check-cast v2, Lcom/squareup/address/workflow/AddressState;

    if-eqz v2, :cond_4

    goto :goto_2

    .line 39
    :cond_4
    new-instance v2, Lcom/squareup/address/workflow/AddressState;

    invoke-direct {v2, p1}, Lcom/squareup/address/workflow/AddressState;-><init>(Lcom/squareup/address/workflow/AddressProps;)V

    :goto_2
    return-object v2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/AddressProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/address/workflow/RealAddressWorkflow;->initialState(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/address/workflow/AddressState;
    .locals 11

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressProps;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 49
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_3

    .line 50
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getStreet()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getApartment()Ljava/lang/String;

    move-result-object v3

    .line 52
    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getCity()Ljava/lang/String;

    move-result-object v4

    .line 53
    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getState()Ljava/lang/String;

    move-result-object v5

    .line 54
    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getPostalCode()Ljava/lang/String;

    move-result-object v6

    .line 55
    invoke-virtual {p2}, Lcom/squareup/address/workflow/AddressProps;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v7

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p3}, Lcom/squareup/address/workflow/AddressState;->getLocations()Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    move-object v8, p1

    if-eqz v0, :cond_2

    .line 57
    invoke-virtual {p3}, Lcom/squareup/address/workflow/AddressState;->getSelectedLocation()I

    move-result p1

    move v9, p1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    const/4 v9, 0x0

    :goto_1
    if-eqz v0, :cond_3

    .line 58
    invoke-virtual {p3}, Lcom/squareup/address/workflow/AddressState;->getViewState()Lcom/squareup/address/workflow/AddressState$ViewState;

    move-result-object p1

    goto :goto_2

    :cond_3
    sget-object p1, Lcom/squareup/address/workflow/AddressState$ViewState;->ShowingAddress:Lcom/squareup/address/workflow/AddressState$ViewState;

    :goto_2
    move-object v10, p1

    move-object v1, p3

    .line 49
    invoke-virtual/range {v1 .. v10}, Lcom/squareup/address/workflow/AddressState;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/CountryCode;Ljava/util/List;ILcom/squareup/address/workflow/AddressState$ViewState;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p3

    :goto_3
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/AddressProps;

    check-cast p2, Lcom/squareup/address/workflow/AddressProps;

    check-cast p3, Lcom/squareup/address/workflow/AddressState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/address/workflow/RealAddressWorkflow;->onPropsChanged(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/address/workflow/AddressState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/address/workflow/AddressScreen;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/address/workflow/AddressProps;",
            "Lcom/squareup/address/workflow/AddressState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/address/workflow/AddressState;",
            "-",
            "Lcom/squareup/address/Address;",
            ">;)",
            "Lcom/squareup/address/workflow/AddressScreen;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    const-string v2, "props"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    move-object/from16 v4, p2

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v5, "context"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getPostalCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/address/workflow/AddressProps;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Lcom/squareup/address/workflow/RealAddressWorkflow;->loadPostalLocations(Ljava/lang/String;Lcom/squareup/CountryCode;)Lcom/squareup/workflow/Worker;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 70
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getPostalCode()Ljava/lang/String;

    move-result-object v6

    .line 71
    new-instance v7, Lcom/squareup/address/workflow/RealAddressWorkflow$render$1$1;

    move-object v8, v0

    check-cast v8, Lcom/squareup/address/workflow/RealAddressWorkflow;

    invoke-direct {v7, v8}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$1$1;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 68
    invoke-interface {v1, v5, v6, v7}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 75
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getCity()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/squareup/address/workflow/RealAddressWorkflow$render$cityRendering$1;

    invoke-direct {v6, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$cityRendering$1;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const-string v7, "city"

    invoke-static {v1, v5, v7, v6}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v9

    .line 78
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getState()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/squareup/address/workflow/RealAddressWorkflow$render$stateRendering$1;

    invoke-direct {v6, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$stateRendering$1;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v5, v2, v6}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v10

    .line 83
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getStreet()Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1;

    invoke-direct {v5, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$1;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const-string v6, "street"

    invoke-static {v1, v2, v6, v5}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    .line 86
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getApartment()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$2;

    invoke-direct {v6, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$2;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const-string v7, "apartment"

    invoke-static {v1, v5, v7, v6}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v5

    .line 89
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getLocations()Ljava/util/List;

    move-result-object v6

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    const/4 v7, 0x1

    xor-int/2addr v6, v7

    if-eqz v6, :cond_1

    .line 90
    new-instance v6, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    .line 93
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getLocations()Ljava/util/List;

    move-result-object v11

    .line 94
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getSelectedLocation()I

    move-result v12

    .line 95
    new-instance v8, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3;

    invoke-direct {v8, v0, v1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$3;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v13, v8

    check-cast v13, Lkotlin/jvm/functions/Function1;

    move-object v8, v6

    .line 90
    invoke-direct/range {v8 .. v13}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)V

    check-cast v6, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    goto :goto_0

    .line 116
    :cond_1
    new-instance v6, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$ManualInput;

    invoke-direct {v6, v9, v10}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$ManualInput;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    check-cast v6, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    :goto_0
    move-object v15, v6

    .line 121
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getPostalCode()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$4;

    invoke-direct {v8, v0}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$4;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const-string v9, "postal"

    invoke-static {v1, v6, v9, v8}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v14

    .line 124
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/address/workflow/AddressProps;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v16

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/address/workflow/AddressProps;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v6

    invoke-direct {v0, v6}, Lcom/squareup/address/workflow/RealAddressWorkflow;->getScrubber(Lcom/squareup/CountryCode;)Lcom/squareup/text/PostalScrubber;

    move-result-object v17

    .line 126
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/address/workflow/AddressProps;->getEnabled()Z

    move-result v18

    .line 127
    new-instance v3, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$5;

    invoke-direct {v3, v0, v1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$body$5;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object/from16 v19, v3

    check-cast v19, Lkotlin/jvm/functions/Function0;

    .line 82
    new-instance v3, Lcom/squareup/address/workflow/AddressBodyScreen;

    move-object v11, v3

    move-object v12, v2

    move-object v13, v5

    invoke-direct/range {v11 .. v19}, Lcom/squareup/address/workflow/AddressBodyScreen;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;Lcom/squareup/CountryCode;Lcom/squareup/text/PostalScrubber;ZLkotlin/jvm/functions/Function0;)V

    .line 130
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getViewState()Lcom/squareup/address/workflow/AddressState$ViewState;

    move-result-object v2

    sget-object v5, Lcom/squareup/address/workflow/RealAddressWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v2}, Lcom/squareup/address/workflow/AddressState$ViewState;->ordinal()I

    move-result v2

    aget v2, v5, v2

    if-eq v2, v7, :cond_3

    const/4 v1, 0x2

    if-ne v2, v1, :cond_2

    .line 139
    new-instance v2, Lcom/squareup/address/workflow/AddressScreen;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v1, v4}, Lcom/squareup/address/workflow/AddressScreen;-><init>(Lcom/squareup/address/workflow/AddressBodyScreen;Lcom/squareup/address/workflow/ChooseStateDialogScreen;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_1

    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1

    .line 131
    :cond_3
    new-instance v2, Lcom/squareup/address/workflow/AddressScreen;

    .line 133
    new-instance v5, Lcom/squareup/address/workflow/ChooseStateDialogScreen;

    .line 134
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/address/workflow/AddressState;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v4

    .line 135
    new-instance v6, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;

    invoke-direct {v6, v0, v1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$2;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 136
    new-instance v7, Lcom/squareup/address/workflow/RealAddressWorkflow$render$3;

    invoke-direct {v7, v0, v1}, Lcom/squareup/address/workflow/RealAddressWorkflow$render$3;-><init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Lcom/squareup/workflow/RenderContext;)V

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 133
    invoke-direct {v5, v4, v6, v7}, Lcom/squareup/address/workflow/ChooseStateDialogScreen;-><init>(Lcom/squareup/CountryCode;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 131
    invoke-direct {v2, v3, v5}, Lcom/squareup/address/workflow/AddressScreen;-><init>(Lcom/squareup/address/workflow/AddressBodyScreen;Lcom/squareup/address/workflow/ChooseStateDialogScreen;)V

    :goto_1
    return-object v2
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/AddressProps;

    check-cast p2, Lcom/squareup/address/workflow/AddressState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/address/workflow/RealAddressWorkflow;->render(Lcom/squareup/address/workflow/AddressProps;Lcom/squareup/address/workflow/AddressState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/address/workflow/AddressScreen;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/address/workflow/AddressState;

    invoke-virtual {p0, p1}, Lcom/squareup/address/workflow/RealAddressWorkflow;->snapshotState(Lcom/squareup/address/workflow/AddressState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
