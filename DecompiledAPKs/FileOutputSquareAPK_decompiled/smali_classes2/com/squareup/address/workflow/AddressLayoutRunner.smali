.class public final Lcom/squareup/address/workflow/AddressLayoutRunner;
.super Ljava/lang/Object;
.source "AddressLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;,
        Lcom/squareup/address/workflow/AddressLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/address/workflow/AddressBodyScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAddressLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AddressLayoutRunner.kt\ncom/squareup/address/workflow/AddressLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,190:1\n1103#2,7:191\n1103#2,7:198\n*E\n*S KotlinDebug\n*F\n+ 1 AddressLayoutRunner.kt\ncom/squareup/address/workflow/AddressLayoutRunner\n*L\n137#1,7:191\n145#1,7:198\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000  2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002 !B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0002H\u0002J\u0018\u0010\u001d\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001fH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\r\u001a\u00020\u000c2\u0006\u0010\u000b\u001a\u00020\u000c@BX\u0082\u000e\u00a2\u0006\u0008\n\u0000\"\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R,\u0010\u0015\u001a\u0004\u0018\u00010\u0014*\u00020\u00112\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u00148B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/address/workflow/AddressLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/address/workflow/AddressBodyScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "apartmentInput",
        "Landroid/widget/EditText;",
        "cityInput",
        "cityPicker",
        "Landroid/widget/Spinner;",
        "value",
        "Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;",
        "layoutState",
        "setLayoutState",
        "(Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;)V",
        "postalInput",
        "Lcom/squareup/widgets/SelectableEditText;",
        "stateInput",
        "streetInput",
        "Lcom/squareup/text/Scrubber;",
        "scrubber",
        "getScrubber",
        "(Lcom/squareup/widgets/SelectableEditText;)Lcom/squareup/text/Scrubber;",
        "setScrubber",
        "(Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/text/Scrubber;)V",
        "renderRegionalSpecificUi",
        "",
        "rendering",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "LayoutState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/address/workflow/AddressLayoutRunner$Companion;


# instance fields
.field private final apartmentInput:Landroid/widget/EditText;

.field private final cityInput:Landroid/widget/EditText;

.field private final cityPicker:Landroid/widget/Spinner;

.field private layoutState:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

.field private final postalInput:Lcom/squareup/widgets/SelectableEditText;

.field private final stateInput:Lcom/squareup/widgets/SelectableEditText;

.field private final streetInput:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/address/workflow/AddressLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/address/workflow/AddressLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/address/workflow/AddressLayoutRunner;->Companion:Lcom/squareup/address/workflow/AddressLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->address_street:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.address_street)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->streetInput:Landroid/widget/EditText;

    .line 47
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->address_apartment:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.address_apartment)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->apartmentInput:Landroid/widget/EditText;

    .line 48
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->address_city:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.address_city)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    .line 49
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->address_state:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.address_state)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    .line 50
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->address_postal:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.address_postal)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    .line 51
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->address_city_picker:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.address_city_picker)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    .line 52
    sget-object v0, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->MANUAL:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    iput-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->layoutState:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    .line 69
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter$LengthFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x91000

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 74
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x80002

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 76
    check-cast p1, Landroid/view/ViewGroup;

    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method private final getScrubber(Lcom/squareup/widgets/SelectableEditText;)Lcom/squareup/text/Scrubber;
    .locals 1

    .line 177
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Scrubber isn\'t meant to be retrieved from EditText"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final renderRegionalSpecificUi(Lcom/squareup/address/workflow/AddressBodyScreen;)V
    .locals 5

    .line 133
    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v4, Lcom/squareup/address/workflow/AddressLayoutRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v0

    aget v0, v4, v0

    if-eq v0, v1, :cond_2

    const/4 v4, 0x2

    if-eq v0, v4, :cond_1

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    .line 153
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    new-array v1, v3, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x82000

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 157
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x81000

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    goto :goto_1

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    .line 144
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    check-cast v2, Landroid/text/method/KeyListener;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    check-cast v0, Landroid/view/View;

    .line 198
    new-instance v1, Lcom/squareup/address/workflow/AddressLayoutRunner$renderRegionalSpecificUi$$inlined$onClickDebounced$2;

    invoke-direct {v1, p1}, Lcom/squareup/address/workflow/AddressLayoutRunner$renderRegionalSpecificUi$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/address/workflow/AddressBodyScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x81070

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    goto :goto_1

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, v3}, Lcom/squareup/widgets/SelectableEditText;->setFocusable(Z)V

    .line 136
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    check-cast v2, Landroid/text/method/KeyListener;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    check-cast v0, Landroid/view/View;

    .line 191
    new-instance v1, Lcom/squareup/address/workflow/AddressLayoutRunner$renderRegionalSpecificUi$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/address/workflow/AddressLayoutRunner$renderRegionalSpecificUi$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/address/workflow/AddressBodyScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    const v1, 0x80002

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 140
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "0123456789-"

    invoke-static {v1}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    check-cast v1, Landroid/text/method/KeyListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 161
    :goto_1
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getPostalScrubber()Lcom/squareup/text/PostalScrubber;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-direct {p0, v0, v1}, Lcom/squareup/address/workflow/AddressLayoutRunner;->setScrubber(Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/text/Scrubber;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/address/CountryResources;->stateHint(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    .line 164
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getCountry()Lcom/squareup/CountryCode;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/address/CountryResources;->postalHint(Lcom/squareup/CountryCode;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setHint(I)V

    .line 165
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->apartmentInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getCountry()Lcom/squareup/CountryCode;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->apartmentHint(Lcom/squareup/CountryCode;)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method private final setLayoutState(Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;)V
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    check-cast v0, Landroid/view/View;

    sget-object v1, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->MANUAL:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 55
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    check-cast v0, Landroid/view/View;

    sget-object v1, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->PICKER:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    if-ne p1, v1, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 56
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->layoutState:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    sget-object v1, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->PICKER:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->MANUAL:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    if-ne p1, v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->scrollToVisible(Landroid/view/View;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->apartmentInput:Landroid/widget/EditText;

    sget-object v1, Lcom/squareup/address/workflow/AddressLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    if-eq v1, v3, :cond_4

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 63
    sget v1, Lcom/squareup/address/workflow/impl/R$id;->address_city:I

    goto :goto_1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 62
    :cond_4
    sget v1, Lcom/squareup/address/workflow/impl/R$id;->address_city_picker:I

    .line 61
    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 65
    iput-object p1, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->layoutState:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    return-void
.end method

.method private final setScrubber(Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/text/Scrubber;)V
    .locals 2

    .line 170
    sget v0, Lcom/squareup/address/workflow/impl/R$id;->scrubber:I

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/squareup/text/ScrubbingTextWatcher;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lcom/squareup/text/ScrubbingTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz p2, :cond_1

    .line 172
    new-instance v0, Lcom/squareup/text/ScrubbingTextWatcher;

    move-object v1, p1

    check-cast v1, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v0, p2, v1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    .line 173
    move-object p2, v0

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    sget p2, Lcom/squareup/address/workflow/impl/R$id;->scrubber:I

    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/SelectableEditText;->setTag(ILjava/lang/Object;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/address/workflow/AddressBodyScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 5

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->streetInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 84
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->apartmentInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 85
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 86
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 87
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 88
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getEnabled()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    .line 90
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->streetInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getStreet()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 91
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->apartmentInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getApartment()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 92
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityInput:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getLocation()Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 93
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->stateInput:Lcom/squareup/widgets/SelectableEditText;

    check-cast p2, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getLocation()Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 94
    iget-object p2, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->postalInput:Lcom/squareup/widgets/SelectableEditText;

    check-cast p2, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getPostal()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 96
    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen;->getLocation()Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;

    move-result-object p2

    .line 97
    instance-of v0, p2, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    const/4 v1, 0x0

    check-cast v1, Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    new-instance v1, Lcom/squareup/address/workflow/PostalLocationAdapter;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "cityPicker.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, p2

    check-cast v3, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    invoke-virtual {v3}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getOptions()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lcom/squareup/address/workflow/PostalLocationAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    check-cast v1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    invoke-virtual {v3}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getSelected()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/Spinner;->setSelection(IZ)V

    .line 101
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressLayoutRunner;->cityPicker:Landroid/widget/Spinner;

    new-instance v1, Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;

    invoke-direct {v1, p2}, Lcom/squareup/address/workflow/AddressLayoutRunner$showRendering$1;-><init>(Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;)V

    check-cast v1, Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 122
    sget-object p2, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->PICKER:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    invoke-direct {p0, p2}, Lcom/squareup/address/workflow/AddressLayoutRunner;->setLayoutState(Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;)V

    goto :goto_0

    .line 124
    :cond_0
    instance-of p2, p2, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$ManualInput;

    if-eqz p2, :cond_1

    .line 125
    sget-object p2, Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;->MANUAL:Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;

    invoke-direct {p0, p2}, Lcom/squareup/address/workflow/AddressLayoutRunner;->setLayoutState(Lcom/squareup/address/workflow/AddressLayoutRunner$LayoutState;)V

    .line 129
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/address/workflow/AddressLayoutRunner;->renderRegionalSpecificUi(Lcom/squareup/address/workflow/AddressBodyScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/address/workflow/AddressBodyScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/address/workflow/AddressLayoutRunner;->showRendering(Lcom/squareup/address/workflow/AddressBodyScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
