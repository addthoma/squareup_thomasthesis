.class public interface abstract Lcom/squareup/cashmanagement/CashDrawerShiftStore;
.super Ljava/lang/Object;
.source "CashDrawerShiftStore.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract dropAllCashDrawerShifts()V
.end method

.method public abstract dropCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;Ljava/util/Date;)V
.end method

.method public abstract getCashDrawerShift(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end method

.method public abstract getCashDrawerShifts(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/util/ReadOnlyCursorList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;",
            ")",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftRow;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getOpenCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end method

.method public abstract repopulateCashDrawerShifts(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract saveCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
.end method
