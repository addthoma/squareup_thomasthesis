.class public Lcom/squareup/cashmanagement/DefaultStartingCashSetting;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "DefaultStartingCashSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultStartingCash:Lcom/squareup/settings/LongLocalSetting;


# direct methods
.method protected constructor <init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/LongLocalSetting;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/settings/LongLocalSetting;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-wide/16 v0, 0x0

    .line 22
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    .line 23
    iput-object p2, p0, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->accountStatusProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->defaultStartingCash:Lcom/squareup/settings/LongLocalSetting;

    return-void
.end method


# virtual methods
.method protected getValueFromDeviceProfile()Ljava/lang/Long;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->accountStatusProvider:Ljavax/inject/Provider;

    .line 33
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 35
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 37
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    return-object v0
.end method

.method protected bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->getValueFromDeviceProfile()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/Long;
    .locals 3

    .line 28
    iget-object v0, p0, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->defaultStartingCash:Lcom/squareup/settings/LongLocalSetting;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/settings/LongLocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method protected bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->getValueFromLocalSettings()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected setValueLocallyInternal(Ljava/lang/Long;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->defaultStartingCash:Lcom/squareup/settings/LongLocalSetting;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/LongLocalSetting;->set(Ljava/lang/Long;)V

    return-void
.end method

.method protected bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;->setValueLocallyInternal(Ljava/lang/Long;)V

    return-void
.end method
