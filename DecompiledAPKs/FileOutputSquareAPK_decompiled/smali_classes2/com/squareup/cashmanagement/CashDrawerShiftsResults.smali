.class public Lcom/squareup/cashmanagement/CashDrawerShiftsResults;
.super Ljava/lang/Object;
.source "CashDrawerShiftsResults.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static failure(Ljava/lang/Throwable;)Lcom/squareup/cashmanagement/CashDrawerShiftsResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Throwable;",
            ")",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsResult<",
            "TT;>;"
        }
    .end annotation

    .line 10
    new-instance v0, Lcom/squareup/cashmanagement/-$$Lambda$CashDrawerShiftsResults$9sJlC38MCHqTlZK-kPSTYFz1wwg;

    invoke-direct {v0, p0}, Lcom/squareup/cashmanagement/-$$Lambda$CashDrawerShiftsResults$9sJlC38MCHqTlZK-kPSTYFz1wwg;-><init>(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method static synthetic lambda$failure$1(Ljava/lang/Throwable;)Ljava/lang/Object;
    .locals 1

    .line 11
    instance-of v0, p0, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/lang/RuntimeException;

    throw p0

    .line 12
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static synthetic lambda$of$0(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public static of(Ljava/lang/Object;)Lcom/squareup/cashmanagement/CashDrawerShiftsResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsResult<",
            "TT;>;"
        }
    .end annotation

    .line 6
    new-instance v0, Lcom/squareup/cashmanagement/-$$Lambda$CashDrawerShiftsResults$CEPSHiGnCFte5ymee3Gq2bdKkqM;

    invoke-direct {v0, p0}, Lcom/squareup/cashmanagement/-$$Lambda$CashDrawerShiftsResults$CEPSHiGnCFte5ymee3Gq2bdKkqM;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method
