.class public Lcom/squareup/cashmanagement/CashDrawerShiftCursor;
.super Lcom/squareup/util/AbstractReadOnlyCursorList;
.source "CashDrawerShiftCursor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/util/AbstractReadOnlyCursorList<",
        "Lcom/squareup/cashmanagement/CashDrawerShiftRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateIndex:I

.field private final descriptionIndex:I

.field private final shiftIdIndex:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/util/AbstractReadOnlyCursorList;-><init>(Landroid/database/Cursor;)V

    const-string p1, "shift_id"

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->shiftIdIndex:I

    const-string p1, "started_date"

    .line 20
    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->dateIndex:I

    const-string p1, "description"

    .line 21
    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->descriptionIndex:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)I
    .locals 0

    .line 11
    iget p0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->shiftIdIndex:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)I
    .locals 0

    .line 11
    iget p0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->dateIndex:I

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)I
    .locals 0

    .line 11
    iget p0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->descriptionIndex:I

    return p0
.end method


# virtual methods
.method protected getCurrent()Lcom/squareup/cashmanagement/CashDrawerShiftRow;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;

    invoke-direct {v0, p0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor$1;-><init>(Lcom/squareup/cashmanagement/CashDrawerShiftCursor;)V

    return-object v0
.end method

.method protected bridge synthetic getCurrent()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashDrawerShiftCursor;->getCurrent()Lcom/squareup/cashmanagement/CashDrawerShiftRow;

    move-result-object v0

    return-object v0
.end method
