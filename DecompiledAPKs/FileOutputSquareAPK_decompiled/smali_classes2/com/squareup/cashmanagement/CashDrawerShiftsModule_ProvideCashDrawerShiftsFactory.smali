.class public final Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;
.super Ljava/lang/Object;
.source "CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final bundleKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;>;"
        }
    .end annotation
.end field

.field private final cashManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final fileExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final hasCashDrawerDataProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final printingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final queuerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/RealTaskQueuer;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;"
        }
    .end annotation
.end field

.field private final storeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/RealTaskQueuer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            ">;)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->queuerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->fileExecutorProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p4, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->storeProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p5, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->hasCashDrawerDataProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p6, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->serviceProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p7, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->userTokenProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p8, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p9, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->bundleKeyProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p10, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p11, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p12, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p13, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->printingDispatcherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/RealTaskQueuer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            ">;)",
            "Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;"
        }
    .end annotation

    .line 94
    new-instance v14, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static provideCashDrawerShifts(Lcom/squareup/cashmanagement/RealTaskQueuer;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cashmanagement/CashDrawerShiftStore;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/server/cashmanagement/CashManagementService;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;)Lcom/squareup/cashmanagement/CashDrawerShiftManager;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cashmanagement/RealTaskQueuer;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftStore;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
            ">;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;",
            ")",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;"
        }
    .end annotation

    .line 103
    invoke-static/range {p0 .. p12}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule;->provideCashDrawerShifts(Lcom/squareup/cashmanagement/RealTaskQueuer;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cashmanagement/CashDrawerShiftStore;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/server/cashmanagement/CashManagementService;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;)Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cashmanagement/CashDrawerShiftManager;
    .locals 14

    .line 80
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->queuerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cashmanagement/RealTaskQueuer;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->fileExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->storeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cashmanagement/CashDrawerShiftStore;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->hasCashDrawerDataProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/server/cashmanagement/CashManagementService;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->bundleKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->cashManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/cashmanagement/CashManagementSettings;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->printingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;

    invoke-static/range {v1 .. v13}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->provideCashDrawerShifts(Lcom/squareup/cashmanagement/RealTaskQueuer;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cashmanagement/CashDrawerShiftStore;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/server/cashmanagement/CashManagementService;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/BundleKey;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/cashmanagement/CashDrawerShiftReportPrintingDispatcher;)Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashDrawerShiftsModule_ProvideCashDrawerShiftsFactory;->get()Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    move-result-object v0

    return-object v0
.end method
