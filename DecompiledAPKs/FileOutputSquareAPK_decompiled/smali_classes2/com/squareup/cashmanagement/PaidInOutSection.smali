.class public Lcom/squareup/cashmanagement/PaidInOutSection;
.super Ljava/lang/Object;
.source "PaidInOutSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;
    }
.end annotation


# instance fields
.field public final headerText:Ljava/lang/String;

.field public final paidInOutEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;",
            ">;"
        }
    .end annotation
.end field

.field public final totalPaidInOut:Lcom/squareup/print/payload/LabelAmountPair;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ")V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/cashmanagement/PaidInOutSection;->headerText:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lcom/squareup/cashmanagement/PaidInOutSection;->paidInOutEvents:Ljava/util/List;

    .line 15
    iput-object p3, p0, Lcom/squareup/cashmanagement/PaidInOutSection;->totalPaidInOut:Lcom/squareup/print/payload/LabelAmountPair;

    return-void
.end method
