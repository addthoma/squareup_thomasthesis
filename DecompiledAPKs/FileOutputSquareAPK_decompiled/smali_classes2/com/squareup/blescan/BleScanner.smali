.class public final Lcom/squareup/blescan/BleScanner;
.super Ljava/lang/Object;
.source "BleScanner.kt"

# interfaces
.implements Lcom/squareup/blescan/WirelessSearcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blescan/BleScanner$Scan;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u001bB5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000e\u0010\u0008\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0008\u0010\u0010\u001a\u00020\u0011H\u0002J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u001c\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\u00160\u00152\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0008\u0010\u001a\u001a\u00020\u0013H\u0016R\u0014\u0010\u000e\u001a\u0008\u0018\u00010\u000fR\u00020\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/blescan/BleScanner;",
        "Lcom/squareup/blescan/WirelessSearcher;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "bluetoothUtils",
        "Lcom/squareup/cardreader/BluetoothUtils;",
        "bleScanFilter",
        "Lcom/squareup/blescan/BleScanFilter;",
        "bluetoothLeScanner",
        "Ljavax/inject/Provider;",
        "Landroid/bluetooth/le/BluetoothLeScanner;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/blescan/BleScanFilter;Ljavax/inject/Provider;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V",
        "activeScan",
        "Lcom/squareup/blescan/BleScanner$Scan;",
        "isSearching",
        "",
        "onTimeout",
        "",
        "startSearch",
        "Lcom/squareup/blescan/BleOperationResult;",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/blescan/DiscoveredDevices;",
        "timeoutMillis",
        "",
        "stopSearch",
        "Scan",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activeScan:Lcom/squareup/blescan/BleScanner$Scan;

.field private final bleScanFilter:Lcom/squareup/blescan/BleScanFilter;

.field private final bluetoothLeScanner:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/blescan/BleScanFilter;Ljavax/inject/Provider;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/blescan/BleScanFilter;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    const-string v0, "mainThread"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothUtils"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bleScanFilter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothLeScanner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "threadEnforcer"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blescan/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p2, p0, Lcom/squareup/blescan/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    iput-object p3, p0, Lcom/squareup/blescan/BleScanner;->bleScanFilter:Lcom/squareup/blescan/BleScanFilter;

    iput-object p4, p0, Lcom/squareup/blescan/BleScanner;->bluetoothLeScanner:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/blescan/BleScanner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method public static final synthetic access$getBleScanFilter$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/blescan/BleScanFilter;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/blescan/BleScanner;->bleScanFilter:Lcom/squareup/blescan/BleScanFilter;

    return-object p0
.end method

.method public static final synthetic access$getBluetoothLeScanner$p(Lcom/squareup/blescan/BleScanner;)Ljavax/inject/Provider;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/blescan/BleScanner;->bluetoothLeScanner:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$getMainThread$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/blescan/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method

.method public static final synthetic access$getThreadEnforcer$p(Lcom/squareup/blescan/BleScanner;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/blescan/BleScanner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-object p0
.end method

.method public static final synthetic access$isSearching(Lcom/squareup/blescan/BleScanner;)Z
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/blescan/BleScanner;->isSearching()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$onTimeout(Lcom/squareup/blescan/BleScanner;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/blescan/BleScanner;->onTimeout()V

    return-void
.end method

.method private final isSearching()Z
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->activeScan:Lcom/squareup/blescan/BleScanner$Scan;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final onTimeout()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Scan timed out"

    .line 148
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    invoke-virtual {p0}, Lcom/squareup/blescan/BleScanner;->stopSearch()V

    return-void
.end method


# virtual methods
.method public startSearch(I)Lcom/squareup/blescan/BleOperationResult;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/blescan/BleOperationResult<",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/blescan/DiscoveredDevices;",
            ">;>;"
        }
    .end annotation

    .line 125
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 126
    invoke-direct {p0}, Lcom/squareup/blescan/BleScanner;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    sget-object p1, Lcom/squareup/blescan/BleOperationResult$Failed$AlreadyRunning;->INSTANCE:Lcom/squareup/blescan/BleOperationResult$Failed$AlreadyRunning;

    check-cast p1, Lcom/squareup/blescan/BleOperationResult;

    return-object p1

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    sget-object p1, Lcom/squareup/blescan/BleOperationResult$Failed$BleNotSupported;->INSTANCE:Lcom/squareup/blescan/BleOperationResult$Failed$BleNotSupported;

    check-cast p1, Lcom/squareup/blescan/BleOperationResult;

    return-object p1

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 133
    sget-object p1, Lcom/squareup/blescan/BleOperationResult$Failed$BleDisabled;->INSTANCE:Lcom/squareup/blescan/BleOperationResult$Failed$BleDisabled;

    check-cast p1, Lcom/squareup/blescan/BleOperationResult;

    return-object p1

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->hasPermission()Z

    move-result v0

    if-nez v0, :cond_3

    .line 136
    sget-object p1, Lcom/squareup/blescan/BleOperationResult$Failed$BlePermissionNotGranted;->INSTANCE:Lcom/squareup/blescan/BleOperationResult$Failed$BlePermissionNotGranted;

    check-cast p1, Lcom/squareup/blescan/BleOperationResult;

    return-object p1

    :cond_3
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting to scan for ble devices."

    .line 138
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/blescan/BleScanner$startSearch$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/blescan/BleScanner;

    invoke-direct {v1, v2}, Lcom/squareup/blescan/BleScanner$startSearch$1;-><init>(Lcom/squareup/blescan/BleScanner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/blescan/BleScannerKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/blescan/BleScannerKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    int-to-long v3, p1

    invoke-interface {v0, v2, v3, v4}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 142
    new-instance p1, Lcom/squareup/blescan/BleScanner$Scan;

    invoke-direct {p1, p0}, Lcom/squareup/blescan/BleScanner$Scan;-><init>(Lcom/squareup/blescan/BleScanner;)V

    .line 143
    iput-object p1, p0, Lcom/squareup/blescan/BleScanner;->activeScan:Lcom/squareup/blescan/BleScanner$Scan;

    .line 144
    invoke-virtual {p1}, Lcom/squareup/blescan/BleScanner$Scan;->start()Lcom/squareup/blescan/BleOperationResult;

    move-result-object p1

    return-object p1
.end method

.method public stopSearch()V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 156
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/blescan/BleScanner$stopSearch$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/blescan/BleScanner;

    invoke-direct {v1, v2}, Lcom/squareup/blescan/BleScanner$stopSearch$1;-><init>(Lcom/squareup/blescan/BleScanner;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/blescan/BleScannerKt$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/blescan/BleScannerKt$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 158
    iget-object v0, p0, Lcom/squareup/blescan/BleScanner;->activeScan:Lcom/squareup/blescan/BleScanner$Scan;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "stopSearch: already stopped!"

    .line 160
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 164
    check-cast v1, Lcom/squareup/blescan/BleScanner$Scan;

    iput-object v1, p0, Lcom/squareup/blescan/BleScanner;->activeScan:Lcom/squareup/blescan/BleScanner$Scan;

    .line 166
    invoke-virtual {v0}, Lcom/squareup/blescan/BleScanner$Scan;->stop()V

    return-void
.end method
