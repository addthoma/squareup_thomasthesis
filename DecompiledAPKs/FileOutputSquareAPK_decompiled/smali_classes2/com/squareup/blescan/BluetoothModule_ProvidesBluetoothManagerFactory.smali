.class public final Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;
.super Ljava/lang/Object;
.source "BluetoothModule_ProvidesBluetoothManagerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/bluetooth/BluetoothManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providesBluetoothManager(Landroid/app/Application;)Landroid/bluetooth/BluetoothManager;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/blescan/BluetoothModule;->providesBluetoothManager(Landroid/app/Application;)Landroid/bluetooth/BluetoothManager;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/bluetooth/BluetoothManager;

    return-object p0
.end method


# virtual methods
.method public get()Landroid/bluetooth/BluetoothManager;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;->providesBluetoothManager(Landroid/app/Application;)Landroid/bluetooth/BluetoothManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/blescan/BluetoothModule_ProvidesBluetoothManagerFactory;->get()Landroid/bluetooth/BluetoothManager;

    move-result-object v0

    return-object v0
.end method
