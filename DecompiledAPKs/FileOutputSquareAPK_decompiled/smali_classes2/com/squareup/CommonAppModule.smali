.class public abstract Lcom/squareup/CommonAppModule;
.super Ljava/lang/Object;
.source "CommonAppModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/account/AccountModule;,
        Lcom/squareup/accountstatus/AccountStatusModule;,
        Lcom/squareup/ActivityComponentModule;,
        Lcom/squareup/android/activity/AndroidConfigurationChangeMonitorModule;,
        Lcom/squareup/util/AndroidModule;,
        Lcom/squareup/android/util/AndroidUtilModule;,
        Lcom/squareup/anrchaperone/AnrChaperoneModule;,
        Lcom/squareup/AppBootstrapModule;,
        Lcom/squareup/badbus/BadBusModule;,
        Lcom/squareup/cardreader/dagger/CardReaderStoreModule;,
        Lcom/squareup/android/util/ClockModule;,
        Lcom/squareup/CommonUserAgentModule;,
        Lcom/squareup/thread/CoroutineDispatcherModule;,
        Lcom/squareup/utilities/ui/DeviceModule;,
        Lcom/squareup/settings/DeviceSettingsModule;,
        Lcom/squareup/settings/DeviceSharedPreferencesModule;,
        Lcom/squareup/server/analytics/EventStreamModule;,
        Lcom/squareup/experiments/ExperimentsModule;,
        Lcom/squareup/thread/FileThreadModule;,
        Lcom/squareup/ForAppScopedModule;,
        Lcom/squareup/gson/GsonModule;,
        Lcom/squareup/http/HttpRetrofit1Module;,
        Lcom/squareup/wavpool/swipe/HeadsetModule;,
        Lcom/squareup/hudtoaster/HudToasterModule;,
        Lcom/squareup/settings/InstallationIdModule;,
        Lcom/squareup/http/interceptor/InterceptorModule;,
        Lcom/squareup/core/location/CommonLocationModule;,
        Lcom/squareup/location/analytics/LocationAnalyticsModule;,
        Lcom/squareup/log/LogModule;,
        Lcom/squareup/thread/MainThreadModule;,
        Lcom/squareup/money/MoneyModule;,
        Lcom/squareup/settings/PersistentFactoryModule;,
        Lcom/squareup/text/phone/number/PhoneNumberHelperModule;,
        Lcom/squareup/text/scrubber/PhoneNumberScrubberModule;,
        Lcom/squareup/firebase/versions/PlayServicesVersionsModule;,
        Lcom/squareup/android/util/PosBuildModule;,
        Lcom/squareup/ui/component/PosComponentFactoryModule;,
        Lcom/squareup/qrcodegenerator/QrCodeGeneratorModule;,
        Lcom/squareup/queue/QueueRootModule;,
        Lcom/squareup/android/util/ResModule;,
        Lcom/squareup/server/RestAdapterCommonModule;,
        Lcom/squareup/server/RetrofitModule;,
        Lcom/squareup/thread/Rx1SchedulerModule;,
        Lcom/squareup/thread/Rx2SchedulerModule;,
        Lcom/squareup/settings/server/ServerSettingsModule;,
        Lcom/squareup/api/ServicesCommonModule;,
        Lcom/squareup/receiving/SessionExpiredHandlerModule;,
        Lcom/squareup/text/TextModule;,
        Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOut;,
        Lcom/squareup/jvm/util/UniqueModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModule$Real;
    }
.end annotation


# static fields
.field private static final ONBOARD_REDIRECT_PATH:Ljava/lang/String; = "onboard_redirect_avt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideAudioThreadEnforcer$0(Landroid/os/HandlerThread;)Ljava/lang/Thread;
    .locals 0

    .line 236
    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$provideNativeLibraryLogger$1(Lcom/squareup/log/OhSnapLogger;Ljava/lang/String;)V
    .locals 1

    .line 263
    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->NATIVE_LIBRARY:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {p0, v0, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$provideUrlRedirectSetting$2(Lcom/squareup/settings/server/Features;)Z
    .locals 1

    .line 307
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_API_URL_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    return p0
.end method

.method static provideAudioHandlerThread()Landroid/os/HandlerThread;
    .locals 2
    .annotation runtime Lcom/squareup/util/AudioThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 229
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Audio Thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 230
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    return-object v0
.end method

.method static provideAudioThreadEnforcer(Landroid/os/HandlerThread;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 2
    .param p0    # Landroid/os/HandlerThread;
        .annotation runtime Lcom/squareup/util/AudioThread;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/util/AudioThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 236
    sget-object v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;->Companion:Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;

    new-instance v1, Lcom/squareup/-$$Lambda$CommonAppModule$XAurpjQOxnnaWth6MR9X15baRb4;

    invoke-direct {v1, p0}, Lcom/squareup/-$$Lambda$CommonAppModule$XAurpjQOxnnaWth6MR9X15baRb4;-><init>(Landroid/os/HandlerThread;)V

    invoke-virtual {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;->invoke(Lkotlin/jvm/functions/Function0;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object p0

    return-object p0
.end method

.method static provideCrashnadoReporter(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/crashnado/CrashnadoReporter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 268
    new-instance v0, Lcom/squareup/CommonAppModule$1;

    invoke-direct {v0, p2, p0, p1}, Lcom/squareup/CommonAppModule$1;-><init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;)V

    return-object v0
.end method

.method static provideEnsureThumbor(Lcom/squareup/pollexor/Thumbor;)Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 301
    new-instance v0, Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;

    invoke-direct {v0, p0}, Lcom/squareup/picasso/pollexor/PollexorRequestTransformer;-><init>(Lcom/squareup/pollexor/Thumbor;)V

    .line 302
    new-instance v1, Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    invoke-direct {v1, v0, p0}, Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;-><init>(Lcom/squareup/picasso/Picasso$RequestTransformer;Lcom/squareup/pollexor/Thumbor;)V

    return-object v1
.end method

.method static provideLatestApiSequenceUuid(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 312
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "latest-api-sequence-uuid"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideNativeLibraryLogger(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 263
    new-instance v0, Lcom/squareup/-$$Lambda$CommonAppModule$mjkmUmjx2aaHmPwOF35cWlfOVCs;

    invoke-direct {v0, p0}, Lcom/squareup/-$$Lambda$CommonAppModule$mjkmUmjx2aaHmPwOF35cWlfOVCs;-><init>(Lcom/squareup/log/OhSnapLogger;)V

    return-object v0
.end method

.method static provideOnboardRedirectPath(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 216
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "onboard_redirect_avt"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-object v0
.end method

.method static providePicassoMemoryCache(Landroid/app/Application;)Lcom/squareup/picasso/Cache;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 254
    new-instance v0, Lcom/squareup/picasso/LruCache;

    invoke-direct {v0, p0}, Lcom/squareup/picasso/LruCache;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static provideRegisterGson()Lcom/google/gson/Gson;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 245
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    return-object v0
.end method

.method static provideSerialAudioThreadExecutor(Landroid/os/HandlerThread;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 1
    .param p0    # Landroid/os/HandlerThread;
        .annotation runtime Lcom/squareup/util/AudioThread;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/util/AudioThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 241
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 p0, 0x1

    invoke-static {v0, p0}, Lcom/squareup/thread/executor/Executors;->stoppableHandlerExecutor(Landroid/os/Handler;Z)Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    move-result-object p0

    return-object p0
.end method

.method static provideSerialFileThreadExecutor(Landroid/os/HandlerThread;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 1
    .param p0    # Landroid/os/HandlerThread;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 224
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 p0, 0x1

    invoke-static {v0, p0}, Lcom/squareup/thread/executor/Executors;->stoppableHandlerExecutor(Landroid/os/Handler;Z)Lcom/squareup/thread/executor/StoppableHandlerExecutor;

    move-result-object p0

    return-object p0
.end method

.method static provideSqlBrite()Lcom/squareup/sqlbrite3/SqlBrite;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 249
    new-instance v0, Lcom/squareup/sqlbrite3/SqlBrite$Builder;

    invoke-direct {v0}, Lcom/squareup/sqlbrite3/SqlBrite$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/sqlbrite3/SqlBrite$Builder;->build()Lcom/squareup/sqlbrite3/SqlBrite;

    move-result-object v0

    return-object v0
.end method

.method static provideUrlRedirectSetting(Lcom/squareup/settings/server/Features;)Lcom/squareup/http/UrlRedirectSetting;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 307
    new-instance v0, Lcom/squareup/-$$Lambda$CommonAppModule$qgkSLww92y_H1bFUZEJxufF-yU8;

    invoke-direct {v0, p0}, Lcom/squareup/-$$Lambda$CommonAppModule$qgkSLww92y_H1bFUZEJxufF-yU8;-><init>(Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method

.method static provideViewHierarchy(Lcom/squareup/radiography/FocusedActivityScanner;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 206
    invoke-static {}, Lcom/squareup/radiography/Xrays;->create()Lcom/squareup/radiography/Xrays;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/radiography/Xrays;->scanAllWindows()Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/squareup/radiography/FocusedActivityScanner;->scanFocusedActivity()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method static provideX2SystemProperties()Lcom/squareup/util/SystemProperties;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 294
    new-instance v0, Lcom/squareup/util/SystemProperties;

    invoke-direct {v0}, Lcom/squareup/util/SystemProperties;-><init>()V

    return-object v0
.end method

.method static providesUUIDGenerator()Lcom/squareup/log/UUIDGenerator;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 258
    new-instance v0, Lcom/squareup/log/UUIDGenerator$Impl;

    invoke-direct {v0}, Lcom/squareup/log/UUIDGenerator$Impl;-><init>()V

    return-object v0
.end method


# virtual methods
.method abstract bindBugsnagAdditionalMetadataLogger(Lcom/squareup/log/BugsnagAdditionalMetadataLogger;)Lcom/squareup/log/CrashAdditionalLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract foregroundActivityProvider(Lcom/squareup/ActivityListener;)Lcom/squareup/util/ForegroundActivityProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBackgroundJobManager(Lcom/squareup/backgroundjob/RealBackgroundJobManager;)Lcom/squareup/backgroundjob/BackgroundJobManager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideEventStreamCommonProperties(Lcom/squareup/analytics/Analytics;)Lcom/squareup/eventstream/CommonProperties;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHttpProfiler(Lcom/squareup/analytics/LoggingHttpProfiler;)Lcom/squareup/http/HttpProfiler;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/LoggingHttpProfiler;",
            ")",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;"
        }
    .end annotation
.end method

.method abstract providePostInstallEncryptedEmail(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;)Lcom/squareup/ui/login/PostInstallEncryptedEmail;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideQueueServiceStarter(Lcom/squareup/queue/QueueService$Starter;)Lcom/squareup/queue/QueueServiceStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideServerExperiments(Lcom/squareup/experiments/ServerExperiments;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
