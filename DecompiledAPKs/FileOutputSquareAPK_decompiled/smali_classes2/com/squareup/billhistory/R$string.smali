.class public final Lcom/squareup/billhistory/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final card:I = 0x7f1202d0

.field public static final cash:I = 0x7f12035f

.field public static final no_sale:I = 0x7f12108e

.field public static final no_sale_on_date_time_format:I = 0x7f12108f

.field public static final no_sale_uppercase:I = 0x7f121090

.field public static final payment_caption:I = 0x7f1213b2

.field public static final payment_caption_on_date_time_format:I = 0x7f1213b3

.field public static final payment_type_other:I = 0x7f121406

.field public static final payment_type_other_uppercase:I = 0x7f121408

.field public static final payment_type_zero_amount:I = 0x7f12140a

.field public static final payment_type_zero_amount_uppercase:I = 0x7f12140b

.field public static final receipt_detail_card:I = 0x7f1215a6

.field public static final receipt_detail_paid_cardcase:I = 0x7f1215af

.field public static final receipt_detail_paid_cardcase_uppercase:I = 0x7f1215b0

.field public static final refund_caption:I = 0x7f12162c

.field public static final refund_caption_on_date_time_format:I = 0x7f12162d

.field public static final tender_adjustment:I = 0x7f121950

.field public static final tender_adjustment_uppercase:I = 0x7f121951

.field public static final tender_unknown:I = 0x7f121953

.field public static final tender_unknown_uppercase:I = 0x7f121954

.field public static final voided_sale:I = 0x7f121bbd

.field public static final voided_sale_on_date_time_format:I = 0x7f121bbe


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
