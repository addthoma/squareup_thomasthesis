.class public Lcom/squareup/billhistory/model/TabTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "TabTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/TabTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/TabTenderHistory;",
        "Lcom/squareup/billhistory/model/TabTenderHistory$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private customerName:Ljava/lang/String;

.field private smallCustomerImageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->TAB:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 7
    iget-object p0, p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->customerName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 7
    iget-object p0, p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->smallCustomerImageUrl:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/billhistory/model/TabTenderHistory;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/billhistory/model/TabTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/TabTenderHistory;-><init>(Lcom/squareup/billhistory/model/TabTenderHistory$Builder;Lcom/squareup/billhistory/model/TabTenderHistory$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/TabTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public customerName(Ljava/lang/String;)Lcom/squareup/billhistory/model/TabTenderHistory$Builder;
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->customerName:Ljava/lang/String;

    return-object p0
.end method

.method public from(Lcom/squareup/billhistory/model/TabTenderHistory;)Lcom/squareup/billhistory/model/TabTenderHistory$Builder;
    .locals 1

    .line 17
    iget-object v0, p1, Lcom/squareup/billhistory/model/TabTenderHistory;->customerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->customerName:Ljava/lang/String;

    .line 18
    iget-object v0, p1, Lcom/squareup/billhistory/model/TabTenderHistory;->smallCustomerImageUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->smallCustomerImageUrl:Ljava/lang/String;

    .line 19
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;

    return-object p1
.end method

.method public bridge synthetic from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/billhistory/model/TabTenderHistory;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TabTenderHistory;)Lcom/squareup/billhistory/model/TabTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public smallCustomerImageUrl(Ljava/lang/String;)Lcom/squareup/billhistory/model/TabTenderHistory$Builder;
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/billhistory/model/TabTenderHistory$Builder;->smallCustomerImageUrl:Ljava/lang/String;

    return-object p0
.end method
