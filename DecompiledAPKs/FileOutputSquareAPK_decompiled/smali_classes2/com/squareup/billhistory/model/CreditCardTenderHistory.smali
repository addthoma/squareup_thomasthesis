.class public Lcom/squareup/billhistory/model/CreditCardTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "CreditCardTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    }
.end annotation


# instance fields
.field public final applicationId:Ljava/lang/String;

.field public final applicationPreferredName:Ljava/lang/String;

.field public final authorizationCode:Ljava/lang/String;

.field public final brand:Lcom/squareup/Card$Brand;

.field public final buyerName:Ljava/lang/String;

.field public final buyerSelectedAccountName:Ljava/lang/String;

.field public final cardSuffix:Ljava/lang/String;

.field public final cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

.field public final entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public final felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

.field public final felicaMaskedCardNumber:Ljava/lang/String;

.field public final felicaTerminalId:Ljava/lang/String;

.field public final tip:Lcom/squareup/protos/common/Money;

.field public final tipPercentage:Lcom/squareup/util/Percentage;


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)V
    .locals 1

    .line 155
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    .line 156
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$100(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/Card$Brand;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    .line 157
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$200(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 158
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$300(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaMaskedCardNumber:Ljava/lang/String;

    .line 159
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$400(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaTerminalId:Ljava/lang/String;

    .line 160
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$500(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardSuffix:Ljava/lang/String;

    .line 161
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$600(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerName:Ljava/lang/String;

    .line 162
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$700(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->tip:Lcom/squareup/protos/common/Money;

    .line 163
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$800(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/util/Percentage;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->tipPercentage:Lcom/squareup/util/Percentage;

    .line 164
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$900(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->authorizationCode:Ljava/lang/String;

    .line 165
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$1000(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->applicationPreferredName:Ljava/lang/String;

    .line 166
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$1100(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buyerSelectedAccountName:Ljava/lang/String;

    .line 167
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$1200(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->applicationId:Ljava/lang/String;

    .line 168
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$1300(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardholderVerificationMethod:Lcom/squareup/protos/client/bills/CardTender$Emv$CardholderVerificationMethod;

    .line 169
    invoke-static {p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->access$1400(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;Lcom/squareup/billhistory/model/CreditCardTenderHistory$1;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;-><init>(Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;)V

    return-void
.end method

.method private getDescription(Lcom/squareup/util/Res;I)Ljava/lang/CharSequence;
    .locals 1

    .line 240
    sget v0, Lcom/squareup/billhistory/R$string;->receipt_detail_card:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 241
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string p2, "card_brand"

    invoke-virtual {v0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardSuffix:Ljava/lang/String;

    if-nez p2, :cond_0

    const-string p2, ""

    :cond_0
    const-string v0, "card_suffix"

    .line 242
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 243
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;
    .locals 1

    .line 173
    new-instance v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/CreditCardTenderHistory;)Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/CreditCardTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    iget-object v1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 3

    .line 204
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    sget-object v1, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    if-ne v0, v1, :cond_3

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    sget-object v1, Lcom/squareup/billhistory/model/CreditCardTenderHistory$1;->$SwitchMap$com$squareup$protos$client$bills$CardTender$Card$FelicaBrand:[I

    iget-object v2, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 215
    :cond_0
    sget v1, Lcom/squareup/utilities/R$string;->receipt_tender_felica_suica:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 212
    :cond_1
    sget v1, Lcom/squareup/utilities/R$string;->receipt_tender_felica_id:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 209
    :cond_2
    sget v1, Lcom/squareup/utilities/R$string;->receipt_tender_felica_qp:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string/jumbo p1, "\u00a0"

    .line 219
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaMaskedCardNumber:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 222
    :cond_3
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    .line 223
    iget v0, v0, Lcom/squareup/text/CardBrandResources;->shortBrandNameId:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->getDescription(Lcom/squareup/util/Res;I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public getImageResId()I
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    sget-object v1, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    if-ne v0, v1, :cond_3

    .line 190
    sget-object v0, Lcom/squareup/billhistory/model/CreditCardTenderHistory$1;->$SwitchMap$com$squareup$protos$client$bills$CardTender$Card$FelicaBrand:[I

    iget-object v1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 196
    :cond_0
    sget v0, Lcom/squareup/vectoricons/R$drawable;->payment_card_ic_24:I

    return v0

    .line 194
    :cond_1
    sget v0, Lcom/squareup/vectoricons/R$drawable;->payment_card_id_24:I

    return v0

    .line 192
    :cond_2
    sget v0, Lcom/squareup/vectoricons/R$drawable;->payment_card_quicpay_24:I

    return v0

    .line 199
    :cond_3
    :goto_0
    invoke-super {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getImageResId()I

    move-result v0

    return v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 3

    .line 233
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    .line 234
    iget v0, v0, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    .line 235
    iget-object v1, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardSuffix:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/util/ReceiptTenderNameUtils;->getReceiptCardTenderName(Lcom/squareup/util/Res;ILjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getTipPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->tipPercentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public getUppercaseDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    .line 228
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    .line 229
    iget v0, v0, Lcom/squareup/text/CardBrandResources;->shortUppercaseBrandNameId:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->getDescription(Lcom/squareup/util/Res;I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method public tip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->tip:Lcom/squareup/protos/common/Money;

    return-object v0
.end method
