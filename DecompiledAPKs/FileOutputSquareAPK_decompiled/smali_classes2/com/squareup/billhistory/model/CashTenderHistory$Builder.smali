.class public Lcom/squareup/billhistory/model/CashTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "CashTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/CashTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/CashTenderHistory;",
        "Lcom/squareup/billhistory/model/CashTenderHistory$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private changeAmount:Lcom/squareup/protos/common/Money;

.field private tenderedAmount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->changeAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/billhistory/model/CashTenderHistory;
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/billhistory/model/CashTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/CashTenderHistory;-><init>(Lcom/squareup/billhistory/model/CashTenderHistory$Builder;Lcom/squareup/billhistory/model/CashTenderHistory$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/CashTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public changeAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->changeAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public from(Lcom/squareup/billhistory/model/CashTenderHistory;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;
    .locals 1

    .line 20
    iget-object v0, p1, Lcom/squareup/billhistory/model/CashTenderHistory;->tenderedAmount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->tenderedAmount:Lcom/squareup/protos/common/Money;

    .line 21
    iget-object v0, p1, Lcom/squareup/billhistory/model/CashTenderHistory;->changeAmount:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->changeAmount:Lcom/squareup/protos/common/Money;

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    return-object p1
.end method

.method public bridge synthetic from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/billhistory/model/CashTenderHistory;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/CashTenderHistory;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public tenderedAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/CashTenderHistory$Builder;
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/billhistory/model/CashTenderHistory$Builder;->tenderedAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
