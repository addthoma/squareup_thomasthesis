.class public Lcom/squareup/billhistory/model/OtherTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "OtherTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    }
.end annotation


# instance fields
.field public final name:Ljava/lang/String;

.field public final note:Ljava/lang/String;

.field public final tenderType:I

.field public final tip:Lcom/squareup/protos/common/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)V
    .locals 1

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    .line 65
    invoke-static {p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->access$100(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->name:Ljava/lang/String;

    .line 66
    invoke-static {p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->access$200(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->note:Ljava/lang/String;

    .line 67
    invoke-static {p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->access$300(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->tip:Lcom/squareup/protos/common/Money;

    .line 68
    invoke-static {p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->access$400(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)I

    move-result p1

    iput p1, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->tenderType:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;Lcom/squareup/billhistory/model/OtherTenderHistory$1;)V
    .locals 0

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory;-><init>(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    .locals 1

    .line 72
    new-instance v0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/OtherTenderHistory;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/OtherTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 2

    .line 89
    iget v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->tenderType:I

    sget-object v1, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->THIRD_PARTY_CARD:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 90
    invoke-super {p0}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 91
    invoke-static {v0}, Lcom/squareup/util/ProtoGlyphs;->unbrandedCard(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getDescription(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    sget v0, Lcom/squareup/billhistory/R$string;->payment_type_other:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getNote()Ljava/lang/String;
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->note:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiptTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/squareup/util/ReceiptTenderNameUtils;->getReceiptOtherTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public tip()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory;->tip:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method
