.class public Lcom/squareup/billhistory/model/BillHistory;
.super Ljava/lang/Object;
.source "BillHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;,
        Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;,
        Lcom/squareup/billhistory/model/BillHistory$Builder;
    }
.end annotation


# instance fields
.field public final cart:Lcom/squareup/protos/client/bills/Cart;

.field public final creatorName:Ljava/lang/String;

.field public final electronicSignature:Ljava/lang/String;

.field public final errorMessage:Ljava/lang/String;

.field public final errorTitle:Ljava/lang/String;

.field private final firstBillId:Lcom/squareup/protos/client/IdPair;

.field private final firstCart:Lcom/squareup/protos/client/bills/Cart;

.field private final firstOrder:Lcom/squareup/payment/Order;

.field public final id:Lcom/squareup/billhistory/model/BillHistoryId;

.field public final isRefund:Z

.field public final isVoided:Z

.field public final loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

.field public final note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

.field public final order:Lcom/squareup/payment/Order;

.field public final paymentState:Lcom/squareup/protos/client/bills/Tender$State;

.field public final pending:Z

.field public final receiptNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final refundAmount:Lcom/squareup/protos/common/Money;

.field public final refundTotal:Lcom/squareup/protos/common/Money;

.field private final relatedBills:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation
.end field

.field public final sourceBillId:Lcom/squareup/protos/client/IdPair;

.field public final storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

.field public final tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation
.end field

.field public final time:Ljava/util/Date;

.field public final tip:Lcom/squareup/protos/common/Money;

.field public final total:Lcom/squareup/protos/common/Money;


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistoryId;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Date;",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Tender$State;",
            "Lcom/squareup/transactionhistory/pending/StoreAndForwardState;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Z",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p4

    move-object/from16 v2, p19

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v3, p1

    .line 256
    iput-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    move-object v4, p2

    .line 257
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    move-object v4, p3

    .line 258
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    .line 259
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    move-object v4, p5

    .line 260
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    move-object v4, p6

    .line 261
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    move-object v4, p7

    .line 262
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    move-object v4, p8

    .line 263
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move v4, p9

    .line 264
    iput-boolean v4, v0, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    .line 266
    invoke-static {}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedByDescendingComparator()Ljava/util/Comparator;

    move-result-object v4

    move-object/from16 v5, p10

    .line 265
    invoke-static {v5, v4}, Lcom/squareup/util/SquareCollections;->sortedCopy(Ljava/util/List;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    .line 267
    iget-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-virtual {p4}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/squareup/billhistory/model/BillHistory;->computeRefundTotal(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->refundTotal:Lcom/squareup/protos/common/Money;

    move-object/from16 v4, p11

    .line 268
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    move-object/from16 v4, p12

    .line 269
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->electronicSignature:Ljava/lang/String;

    move-object/from16 v4, p13

    .line 270
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->creatorName:Ljava/lang/String;

    move-object/from16 v4, p14

    .line 271
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    move-object/from16 v4, p15

    .line 272
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-object/from16 v4, p16

    .line 273
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->refundAmount:Lcom/squareup/protos/common/Money;

    move-object/from16 v4, p17

    .line 274
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->errorTitle:Ljava/lang/String;

    move-object/from16 v4, p18

    .line 275
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->errorMessage:Ljava/lang/String;

    .line 276
    iput-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 277
    invoke-direct {p0, p4, v2}, Lcom/squareup/billhistory/model/BillHistory;->doesCartOnlyHaveVoidedItems(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)Z

    move-result v4

    iput-boolean v4, v0, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    move/from16 v4, p20

    .line 278
    iput-boolean v4, v0, Lcom/squareup/billhistory/model/BillHistory;->isRefund:Z

    move-object/from16 v4, p21

    .line 279
    iput-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    if-eqz p22, :cond_0

    move-object/from16 v3, p22

    goto :goto_0

    .line 281
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    :goto_0
    iput-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->firstBillId:Lcom/squareup/protos/client/IdPair;

    if-eqz p23, :cond_1

    move-object/from16 v1, p23

    .line 282
    :cond_1
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->firstOrder:Lcom/squareup/payment/Order;

    if-eqz p24, :cond_2

    move-object/from16 v2, p24

    .line 283
    :cond_2
    iput-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/billhistory/model/BillHistory$1;)V
    .locals 0

    .line 152
    invoke-direct/range {p0 .. p24}, Lcom/squareup/billhistory/model/BillHistory;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;
    .locals 0

    .line 152
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    .line 152
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->firstBillId:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/payment/Order;
    .locals 0

    .line 152
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->firstOrder:Lcom/squareup/payment/Order;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/protos/client/bills/Cart;
    .locals 0

    .line 152
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public static computeRefundTotal(Ljava/util/List;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 818
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 819
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 822
    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->isFailedOrRejected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 823
    invoke-virtual {v0}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 824
    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_1
    return-object p1
.end method

.method private doesCartOnlyHaveVoidedItems(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    .line 872
    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    if-nez v1, :cond_0

    goto :goto_0

    .line 875
    :cond_0
    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    .line 876
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    return v0

    .line 879
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/Cart;->getItems()Ljava/util/List;

    move-result-object p1

    .line 880
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    if-ne p1, p2, :cond_2

    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0
.end method

.method private getFirstBill()Lcom/squareup/server/payment/RelatedBillHistory;
    .locals 4

    .line 714
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 715
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/billhistory/model/BillHistory;->firstBillId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/IdPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method private getFirstCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    .line 744
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method private isRefundable(Lcom/squareup/billhistory/model/TenderHistory;)Z
    .locals 3

    .line 833
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    .line 838
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/BillHistory;->refundsForTender(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;

    move-result-object v0

    .line 839
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 840
    invoke-direct {p0, p1, v0}, Lcom/squareup/billhistory/model/BillHistory;->refundableAmount(Lcom/squareup/billhistory/model/TenderHistory;Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 839
    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_2

    :cond_1
    const/4 v2, 0x1

    :cond_2
    return v2
.end method

.method private refundableAmount(Lcom/squareup/billhistory/model/TenderHistory;Ljava/util/List;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 845
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-ne v0, v1, :cond_0

    const-wide/16 v0, 0x0

    .line 846
    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1

    .line 849
    :cond_0
    iget-object v0, p1, Lcom/squareup/billhistory/model/TenderHistory;->amount:Lcom/squareup/protos/common/Money;

    .line 850
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 851
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 852
    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtractWithZeroMinimum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private refundsForTender(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 858
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 859
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 861
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->isFailedOrRejected()Z

    move-result v3

    if-nez v3, :cond_0

    .line 862
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 863
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 864
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTendersRefundMoney()Ljava/util/Map;

    move-result-object v3

    iget-object v4, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 865
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static sumTips(Ljava/util/List;)Lcom/squareup/protos/common/Money;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 336
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 337
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static timestampsMatch(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)Z
    .locals 4

    .line 800
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getCreatedAt()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 805
    :cond_0
    iget-object p0, p0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide p0

    sub-long/2addr v1, p0

    invoke-static {v1, v2}, Ljava/lang/Math;->abs(J)J

    move-result-wide p0

    const-wide/16 v1, 0x3e8

    cmp-long v3, p0, v1

    if-gez v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public areAllTendersPastRefundDate(J)Z
    .locals 2

    .line 495
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 496
    invoke-virtual {v1, p1, p2}, Lcom/squareup/billhistory/model/TenderHistory;->isPastRefundDate(J)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public containsBillWithId(Ljava/lang/String;)Z
    .locals 4

    .line 636
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 639
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 640
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 641
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->eq(Ljava/lang/Comparable;Ljava/lang/Comparable;)Z

    move-result v2

    if-eqz v2, :cond_1

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public getAllRelatedBills()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 652
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    return-object v0
.end method

.method public getAllRelatedSaleBills()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 672
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 673
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 674
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->isRefund()Z

    move-result v3

    if-nez v3, :cond_0

    .line 675
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getAllRelatedTenders()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 699
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 700
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedSaleBills()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 701
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_1

    goto :goto_0

    .line 705
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender;

    .line 707
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v5

    .line 706
    invoke-static {v4, v5}, Lcom/squareup/billhistory/model/TenderHistory;->fromHistoricalTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method public getFirstBillTenders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 729
    invoke-direct {p0}, Lcom/squareup/billhistory/model/BillHistory;->getFirstBill()Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v0

    if-nez v0, :cond_0

    .line 730
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getTenders()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 732
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 733
    invoke-direct {p0}, Lcom/squareup/billhistory/model/BillHistory;->getFirstBill()Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getTenders()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 735
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/Tender;

    .line 736
    invoke-direct {p0}, Lcom/squareup/billhistory/model/BillHistory;->getFirstBill()Lcom/squareup/server/payment/RelatedBillHistory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/billhistory/model/TenderHistory;->fromHistoricalTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getFirstOrder()Lcom/squareup/payment/Order;
    .locals 1

    .line 751
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->firstOrder:Lcom/squareup/payment/Order;

    return-object v0
.end method

.method public getFirstOrderSubtotal()Lcom/squareup/protos/common/Money;
    .locals 7

    .line 760
    invoke-direct {p0}, Lcom/squareup/billhistory/model/BillHistory;->getFirstCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 761
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 763
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v3, :cond_0

    .line 764
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    .line 767
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->surcharge_money:Lcom/squareup/protos/common/Money;

    if-eqz v3, :cond_1

    .line 768
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->surcharge_money:Lcom/squareup/protos/common/Money;

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/squareup/checkout/SubtotalType;

    const/4 v4, 0x0

    .line 771
    sget-object v5, Lcom/squareup/checkout/SubtotalType;->TAX:Lcom/squareup/checkout/SubtotalType;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/squareup/checkout/SubtotalType;->SWEDISH_ROUNDING:Lcom/squareup/checkout/SubtotalType;

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/squareup/util/SquareCollections;->asSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    .line 774
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getFirstOrder()Lcom/squareup/payment/Order;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/payment/Order;->getOrderAdjustments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/payment/OrderAdjustment;

    .line 777
    iget-object v6, v5, Lcom/squareup/payment/OrderAdjustment;->subtotalType:Lcom/squareup/checkout/SubtotalType;

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 778
    iget-object v5, v5, Lcom/squareup/payment/OrderAdjustment;->appliedMoney:Lcom/squareup/protos/common/Money;

    iget-object v5, v5, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    sub-long/2addr v1, v5

    goto :goto_0

    .line 782
    :cond_3
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 3

    .line 608
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->hasCardTender()Z

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->hasCashTender()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v2}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/util/ProtoGlyphs;->splitTender(ZZLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0

    .line 610
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 612
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->hasMatchingRefundToGiftCard()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 613
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->GIFT_CARD_SMALL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0

    .line 614
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->hasCardTender()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 615
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0

    .line 617
    :cond_3
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->getUnbrandedTenderGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    return-object v0
.end method

.method public getImageResId()I
    .locals 2

    .line 622
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/TenderHistory;->getImageResId()I

    move-result v0

    return v0
.end method

.method public getLoyaltyDetails()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;
    .locals 1

    .line 602
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    return-object v0
.end method

.method public getRefundTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 507
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->refundTotal:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getRelatedBillsExceptFirst()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 659
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 660
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 661
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/billhistory/model/BillHistory;->firstBillId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/IdPair;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 662
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getRelatedRefundBills()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation

    .line 685
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 686
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 687
    invoke-virtual {v2}, Lcom/squareup/server/payment/RelatedBillHistory;->isRefund()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 688
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getSourceBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 298
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;
    .locals 1

    .line 306
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    return-object v0
.end method

.method public getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;
    .locals 3

    const-string v0, "tenderId"

    .line 388
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 390
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 391
    iget-object v2, v1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 396
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tender "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not found in bill "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTenders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation

    .line 403
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    return-object v0
.end method

.method public getTicketName()Ljava/lang/String;
    .locals 1

    .line 583
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->name:Ljava/lang/String;

    return-object v0

    .line 587
    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    if-eqz v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->kitchen_printing:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;->name:Ljava/lang/String;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTicketNote()Ljava/lang/String;
    .locals 1

    .line 595
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->note:Ljava/lang/String;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 310
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public hasCardTender()Z
    .locals 3

    .line 351
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 352
    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v2, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasCashTender()Z
    .locals 3

    .line 368
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 369
    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v2, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasError()Z
    .locals 2

    .line 576
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->errorTitle:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->errorMessage:Ljava/lang/String;

    .line 577
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasMatchingRefundToGiftCard()Z
    .locals 3

    .line 532
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 533
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getDestination()Lcom/squareup/protos/client/bills/Refund$Destination;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 537
    invoke-static {p0, v1}, Lcom/squareup/billhistory/model/BillHistory;->timestampsMatch(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasMultipleBills()Z
    .locals 1

    .line 629
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedBillsExceptFirst()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasNonLostTender()Z
    .locals 3

    .line 375
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 376
    iget-object v1, v1, Lcom/squareup/billhistory/model/TenderHistory;->tenderState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v2, Lcom/squareup/protos/client/bills/Tender$State;->LOST:Lcom/squareup/protos/client/bills/Tender$State;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasNonZeroTip()Z
    .locals 5

    .line 330
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasReceipt()Z
    .locals 3

    .line 564
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 568
    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    :goto_0
    return v1
.end method

.method public hasSuccessfulOrPendingRefund()Z
    .locals 3

    .line 486
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 487
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->isRefund()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->isFailedOrRejected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public idsEqual(Lcom/squareup/billhistory/model/BillHistory;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object p1, p1, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistoryId;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public isAwaitingMerchantTip()Z
    .locals 4

    .line 314
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$State;->AWAITING_MERCHANT_TIP:Lcom/squareup/protos/client/bills/Tender$State;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->hasCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    return v3

    .line 317
    :cond_1
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 321
    invoke-virtual {v1}, Lcom/squareup/billhistory/model/TenderHistory;->shouldPrintTipEntryInput()Z

    move-result v1

    if-eqz v1, :cond_2

    return v2

    :cond_3
    return v3
.end method

.method public isExchange()Z
    .locals 4

    .line 554
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedBills()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/payment/RelatedBillHistory;

    .line 555
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/IdPair;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    .line 560
    invoke-virtual {v1}, Lcom/squareup/server/payment/RelatedBillHistory;->isExchange()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    return v0
.end method

.method public isFullyRefunded()Z
    .locals 2

    .line 477
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/billhistory/model/TenderHistory;

    .line 478
    invoke-direct {p0, v1}, Lcom/squareup/billhistory/model/BillHistory;->isRefundable(Lcom/squareup/billhistory/model/TenderHistory;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public isNoSale()Z
    .locals 3

    .line 343
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/billhistory/model/NoSaleTenderHistory;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isSplitTender()Z
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isStoreAndForward()Z
    .locals 1

    .line 302
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public mostRecentTenderTimestamp()Ljava/util/Date;
    .locals 4

    .line 408
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const-string v1, "tenders"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 409
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    .line 410
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 411
    iget-object v3, v2, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    invoke-virtual {v3, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 412
    iget-object v0, v2, Lcom/squareup/billhistory/model/TenderHistory;->timestamp:Ljava/util/Date;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public numCardTenders()I
    .locals 4

    .line 359
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 360
    iget-object v2, v2, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    sget-object v3, Lcom/squareup/billhistory/model/TenderHistory$Type;->CARD:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v2, v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public refund(Lcom/squareup/server/payment/RelatedBillHistory;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 28

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "refund"

    .line 463
    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 464
    new-instance v2, Ljava/util/ArrayList;

    move-object v13, v2

    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->relatedBills:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 465
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    new-instance v1, Lcom/squareup/billhistory/model/BillHistory;

    move-object v3, v1

    iget-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v5, v0, Lcom/squareup/billhistory/model/BillHistory;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    iget-object v7, v0, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    iget-object v8, v0, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    iget-object v9, v0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    iget-object v10, v0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    iget-object v11, v0, Lcom/squareup/billhistory/model/BillHistory;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    iget-boolean v12, v0, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    iget-object v14, v0, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/billhistory/model/BillHistory;->electronicSignature:Ljava/lang/String;

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    move-object/from16 v17, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-object/from16 v18, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->refundAmount:Lcom/squareup/protos/common/Money;

    move-object/from16 v19, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->errorTitle:Ljava/lang/String;

    move-object/from16 v20, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->errorMessage:Ljava/lang/String;

    move-object/from16 v21, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v22, v2

    iget-boolean v2, v0, Lcom/squareup/billhistory/model/BillHistory;->isRefund:Z

    move/from16 v23, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-object/from16 v24, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->firstBillId:Lcom/squareup/protos/client/IdPair;

    move-object/from16 v25, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->firstOrder:Lcom/squareup/payment/Order;

    move-object/from16 v26, v2

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v27, v2

    const/16 v16, 0x0

    invoke-direct/range {v3 .. v27}, Lcom/squareup/billhistory/model/BillHistory;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V

    return-object v1
.end method

.method public refundableAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 515
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->refundTotal:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtractWithZeroMinimum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public refundableAmount(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/protos/common/Money;
    .locals 1

    if-nez p1, :cond_0

    .line 520
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/BillHistory;->refundableAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1

    .line 522
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/BillHistory;->refundsForTender(Lcom/squareup/billhistory/model/TenderHistory;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/billhistory/model/BillHistory;->refundableAmount(Lcom/squareup/billhistory/model/TenderHistory;Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public replaceTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 6

    const-string v0, "newTender"

    .line 426
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 428
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 432
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/billhistory/model/TenderHistory;

    .line 433
    iget-object v4, v3, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    iget-object v5, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 434
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x1

    goto :goto_0

    .line 437
    :cond_0
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    if-nez v2, :cond_3

    .line 443
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Existing tender ids: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 444
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/billhistory/model/TenderHistory;

    .line 445
    iget-object v2, v2, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 447
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Tender "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not found in bill "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ". "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 451
    :cond_3
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/TenderHistory;->tip()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 453
    new-instance v1, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v1, p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    .line 454
    invoke-virtual {v1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setTenders(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v1

    .line 455
    invoke-static {v0}, Lcom/squareup/billhistory/model/BillHistory;->sumTips(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object v1

    .line 456
    invoke-virtual {v1, p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->total(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 457
    invoke-static {v0}, Lcom/squareup/billhistory/Bills;->calculatePaymentState(Ljava/util/List;)Lcom/squareup/protos/client/bills/Tender$State;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    .line 458
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 786
    iget-object v1, p0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "BillHistory(id=%s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
