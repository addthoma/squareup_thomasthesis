.class public final synthetic Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/text/Formatter;


# instance fields
.field private final synthetic f$0:Lcom/squareup/util/Res;

.field private final synthetic f$1:Lcom/squareup/quantity/PerUnitFormatter;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;->f$0:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;->f$1:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method


# virtual methods
.method public final format(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;->f$0:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/billhistory/model/-$$Lambda$BillHistory$ItemizationsNote$83dIiFaeqtbuTGAsNYhjRYKy7l0;->f$1:Lcom/squareup/quantity/PerUnitFormatter;

    check-cast p1, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;

    invoke-static {v0, v1, p1}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->lambda$localizedNote$1(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNoteComponent;)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method
