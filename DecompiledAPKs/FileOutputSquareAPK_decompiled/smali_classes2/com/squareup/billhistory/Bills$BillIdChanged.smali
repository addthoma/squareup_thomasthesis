.class public Lcom/squareup/billhistory/Bills$BillIdChanged;
.super Ljava/lang/Object;
.source "Bills.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/Bills;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BillIdChanged"
.end annotation


# instance fields
.field public final newBillId:Lcom/squareup/billhistory/model/BillHistoryId;

.field public final oldBillId:Lcom/squareup/billhistory/model/BillHistoryId;


# direct methods
.method public constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/billhistory/model/BillHistoryId;)V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/billhistory/Bills$BillIdChanged;->oldBillId:Lcom/squareup/billhistory/model/BillHistoryId;

    .line 87
    iput-object p2, p0, Lcom/squareup/billhistory/Bills$BillIdChanged;->newBillId:Lcom/squareup/billhistory/model/BillHistoryId;

    return-void
.end method
