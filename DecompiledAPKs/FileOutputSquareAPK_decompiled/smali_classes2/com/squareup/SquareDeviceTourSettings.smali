.class public abstract Lcom/squareup/SquareDeviceTourSettings;
.super Ljava/lang/Object;
.source "SquareDeviceTourSettings.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/SquareDeviceTourSettings$NoTourSettings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008&\u0018\u00002\u00020\u0001:\u0001\u000bB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&J\u0008\u0010\u0005\u001a\u00020\u0004H&J\u0008\u0010\u0006\u001a\u00020\u0004H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\u0008H&J\u0006\u0010\n\u001a\u00020\u0008\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/SquareDeviceTourSettings;",
        "",
        "()V",
        "deviceTourViewed",
        "",
        "featureTourViewed",
        "notifyTourShowing",
        "shouldShowDeviceTour",
        "",
        "shouldShowFeatureTour",
        "shouldShowTour",
        "NoTourSettings",
        "square-device-tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract deviceTourViewed()V
.end method

.method public abstract featureTourViewed()V
.end method

.method public abstract notifyTourShowing()V
.end method

.method public abstract shouldShowDeviceTour()Z
.end method

.method public abstract shouldShowFeatureTour()Z
.end method

.method public final shouldShowTour()Z
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/SquareDeviceTourSettings;->shouldShowDeviceTour()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/SquareDeviceTourSettings;->shouldShowFeatureTour()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
