.class public final Lcom/squareup/cardreader/TamperFeatureV2;
.super Ljava/lang/Object;
.source "TamperFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/TamperFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000b\u001a\u00020\u000cH\u0002J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0008\u0010\u0011\u001a\u00020\u000eH\u0002J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u000cH\u0002J\u0008\u0010\u0019\u001a\u00020\u000cH\u0002J\u0008\u0010\u001a\u001a\u00020\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/cardreader/TamperFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/TamperFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;",
        "clearFlaggedStatus",
        "Lcom/squareup/cardreader/lcr/CrTamperResult;",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$TamperFeatureMessage;",
        "initialize",
        "onTamperData",
        "tamperData",
        "",
        "onTamperStatus",
        "tamperStatus",
        "",
        "requestTamperData",
        "requestTamperStatus",
        "resetIfInitilized",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field private featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/TamperFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/TamperFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method public static final synthetic access$getFeaturePointer$p(Lcom/squareup/cardreader/TamperFeatureV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;
    .locals 1

    .line 16
    iget-object p0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-nez p0, :cond_0

    const-string v0, "featurePointer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setFeaturePointer$p(Lcom/squareup/cardreader/TamperFeatureV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)V
    .locals 0

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    return-void
.end method

.method private final clearFlaggedStatus()Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_reset_tag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object v0

    const-string v1, "TamperFeatureNative.cr_t\u2026reset_tag(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final initialize()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->tamper_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    move-result-object v0

    const-string v1, "TamperFeatureNative.tamp\u2026ardreaderPointer(), this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    return-void
.end method

.method private final requestTamperData()Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_get_tamper_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object v0

    const-string v1, "TamperFeatureNative.cr_t\u2026mper_data(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final requestTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperResult;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_get_tamper_status(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    move-result-object v0

    const-string v1, "TamperFeatureNative.cr_t\u2026er_status(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$TamperFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$TamperFeatureMessage$InitializeTamperFeature;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/TamperFeatureV2;->initialize()V

    goto :goto_0

    .line 25
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$TamperFeatureMessage$RequestTamperStatus;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/cardreader/TamperFeatureV2;->requestTamperStatus()Lcom/squareup/cardreader/lcr/CrTamperResult;

    goto :goto_0

    .line 26
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$TamperFeatureMessage$RequestTamperData;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/cardreader/TamperFeatureV2;->requestTamperData()Lcom/squareup/cardreader/lcr/CrTamperResult;

    goto :goto_0

    .line 27
    :cond_2
    instance-of p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$TamperFeatureMessage$ClearFlaggedStatus;

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/squareup/cardreader/TamperFeatureV2;->clearFlaggedStatus()Lcom/squareup/cardreader/lcr/CrTamperResult;

    :cond_3
    :goto_0
    return-void
.end method

.method public onTamperData([B)V
    .locals 2

    const-string v0, "tamperData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput$OnTamperData;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput$OnTamperData;-><init>([B)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onTamperStatus(I)V
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput$OnTamperStatus;

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrTamperStatus;

    move-result-object p1

    const-string v2, "CrTamperStatus.swigToEnum(tamperStatus)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$TamperFeatureOutput$OnTamperStatus;-><init>(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 37
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/TamperFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-eqz v0, :cond_2

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    .line 39
    iget-object v0, p0, Lcom/squareup/cardreader/TamperFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/TamperFeatureNative;->cr_tamper_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;

    :cond_2
    return-void
.end method
