.class public Lcom/squareup/cardreader/squid/x2/X2CardReader;
.super Ljava/lang/Object;
.source "X2CardReader.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReader;


# instance fields
.field private final localCardReader:Lcom/squareup/cardreader/LocalCardReader;

.field private final x2SystemFeature:Lcom/squareup/cardreader/X2SystemFeatureLegacy;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/LocalCardReader;Lcom/squareup/cardreader/X2SystemFeatureLegacy;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    .line 24
    iput-object p2, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->x2SystemFeature:Lcom/squareup/cardreader/X2SystemFeatureLegacy;

    return-void
.end method


# virtual methods
.method public abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    return-void
.end method

.method public ackTmnWriteNotify()V
    .locals 2

    .line 120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public cancelPayment()V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->cancelPayment()V

    return-void
.end method

.method public cancelTmnRequest()V
    .locals 2

    .line 124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public enableSwipePassthrough(Z)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->enableSwipePassthrough(Z)V

    return-void
.end method

.method public forget()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->forget()V

    return-void
.end method

.method public getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    return-object v0
.end method

.method public getId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    return-object v0
.end method

.method public identify()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->identify()V

    return-void
.end method

.method public initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/cardreader/LocalCardReader;->initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method

.method public onCoreDumpDataSent()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->onCoreDumpDataSent()V

    return-void
.end method

.method public onPinBypass()V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->onPinBypass()V

    return-void
.end method

.method public onPinDigitEntered(I)V
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->onPinDigitEntered(I)V

    return-void
.end method

.method public onPinPadReset()V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->onPinPadReset()V

    return-void
.end method

.method public onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->onSecureTouchApplicationEvent(Lcom/squareup/securetouch/SecureTouchApplicationEvent;)V

    return-void
.end method

.method public onTamperDataSent()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->onTamperDataSent()V

    return-void
.end method

.method public powerOff()V
    .locals 2

    .line 177
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "powerOff not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public processARPC([B)V
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->processARPC([B)V

    return-void
.end method

.method public processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->processFirmwareUpdateResponse(Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;)V

    return-void
.end method

.method public processSecureSessionMessageFromServer([B)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->processSecureSessionMessageFromServer([B)V

    return-void
.end method

.method public reinitializeSecureSession()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->reinitializeSecureSession()V

    return-void
.end method

.method public requestPowerStatus()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->requestPowerStatus()V

    return-void
.end method

.method public reset()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->reset()V

    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 1

    .line 148
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Not implemented"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->selectApplication(Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public sendPowerupHint(I)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/LocalCardReader;->sendPowerupHint(I)V

    return-void
.end method

.method public sendTmnDataToReader([B)V
    .locals 1

    .line 116
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not implemented"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setDocked(Z)V
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->x2SystemFeature:Lcom/squareup/cardreader/X2SystemFeatureLegacy;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/X2SystemFeatureLegacy;->setDocked(Z)V

    return-void
.end method

.method public startPayment(JJ)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/LocalCardReader;->startPayment(JJ)V

    return-void
.end method

.method public startRefund(JJ)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/LocalCardReader;->startRefund(JJ)V

    return-void
.end method

.method public startTmnCheckBalance(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 0

    .line 106
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Not implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startTmnMiryo([B)V
    .locals 1

    .line 128
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not implemented"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startTmnPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 0

    .line 95
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Not implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startTmnRefund(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 0

    .line 100
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Not implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startTmnTestPayment(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;Ljava/lang/String;J)V
    .locals 0

    .line 112
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Not implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public submitPinBlock()V
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2CardReader;->localCardReader:Lcom/squareup/cardreader/LocalCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/LocalCardReader;->submitPinBlock()V

    return-void
.end method
