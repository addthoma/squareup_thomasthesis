.class public interface abstract Lcom/squareup/cardreader/CardReaderStatusListener;
.super Ljava/lang/Object;
.source "CardReaderStatusListener.java"


# virtual methods
.method public abstract initializingSecureSession(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onAudioReaderFailedToConnect(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onBatteryUpdate(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onCardReaderBackendInitialized(Lcom/squareup/cardreader/CardReader;)V
.end method

.method public abstract onCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onCoreDump(Lcom/squareup/cardreader/CardReader;[B[B)V
.end method

.method public abstract onDeviceUnsupported(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onFullCommsEstablished(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onInitFirmwareUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onInitRegisterUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onInitializationComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onReaderError(Lcom/squareup/cardreader/CardReader;)V
.end method

.method public abstract onReaderReady()V
.end method

.method public abstract onSecureSessionAborted(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
.end method

.method public abstract onSecureSessionDenied(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
.end method

.method public abstract onSecureSessionError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
.end method

.method public abstract onSecureSessionInvalid(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onSecureSessionValid(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract onTamperData(Lcom/squareup/cardreader/CardReader;[B)V
.end method

.method public abstract onTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;)V
.end method

.method public abstract sendSecureSessionMessageToServer(Lcom/squareup/cardreader/CardReader;[B)V
.end method
