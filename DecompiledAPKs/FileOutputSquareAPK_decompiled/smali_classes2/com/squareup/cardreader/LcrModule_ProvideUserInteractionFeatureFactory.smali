.class public final Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideUserInteractionFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final userInteractionFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->sessionProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->userInteractionFeatureNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUserInteractionFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;)Lcom/squareup/cardreader/UserInteractionFeatureLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;"
        }
    .end annotation

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/cardreader/LcrModule;->provideUserInteractionFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;)Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/UserInteractionFeatureLegacy;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->sessionProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->userInteractionFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->provideUserInteractionFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;)Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->get()Lcom/squareup/cardreader/UserInteractionFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
