.class synthetic Lcom/squareup/cardreader/BatteryLevel$1;
.super Ljava/lang/Object;
.source "BatteryLevel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/BatteryLevel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 82
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->values()[Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/cardreader/BatteryLevel$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I

    :try_start_0
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_CHARGED:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_DISCHARGING:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel$1;->$SwitchMap$com$squareup$cardreader$lcr$CrsBatteryMode:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->CRS_BATTERY_MODE_LOW_CRITICAL:Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
