.class public Lcom/squareup/cardreader/dipper/DipperEventHandler;
.super Ljava/lang/Object;
.source "DipperEventHandler.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderStatusListener;
.implements Lcom/squareup/cardreader/MagSwipeListener;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;
    }
.end annotation


# static fields
.field private static final CONNECTION_TIMEOUT_INTERVAL:I = 0x2710


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

.field private final localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final nfcReaderHasConnected:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final r12HasConnected:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final r6HasConnected:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

.field private final secureSessionService:Lcom/squareup/cardreader/SecureSessionService;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private timeoutRunnable:Ljava/lang/Runnable;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final userCountryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/LocalSetting;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/cardreader/SecureSessionService;Lcom/squareup/payment/Transaction;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/cardreader/DippedCardTracker;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/log/ReaderEventLogger;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/cardreader/SecureSessionService;",
            "Lcom/squareup/payment/Transaction;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 137
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p2

    .line 138
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    move-object v1, p3

    .line 139
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p4

    .line 140
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p5

    .line 141
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    move-object v1, p6

    .line 142
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object v1, p7

    .line 143
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p8

    .line 144
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    move-object v1, p9

    .line 145
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    move-object v1, p10

    .line 146
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    move-object v1, p11

    .line 147
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->nfcReaderHasConnected:Lcom/squareup/settings/LocalSetting;

    move-object v1, p12

    .line 148
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->r6HasConnected:Lcom/squareup/settings/LocalSetting;

    move-object v1, p13

    .line 149
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->r12HasConnected:Lcom/squareup/settings/LocalSetting;

    move-object/from16 v1, p14

    .line 150
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    move-object/from16 v1, p15

    .line 151
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object/from16 v1, p16

    .line 152
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    move-object/from16 v1, p17

    .line 153
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->secureSessionService:Lcom/squareup/cardreader/SecureSessionService;

    move-object/from16 v1, p18

    .line 154
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p19

    .line 155
    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->userCountryCodeProvider:Ljavax/inject/Provider;

    .line 156
    sget-object v1, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    .line 157
    sget-object v1, Lcom/squareup/cardreader/UnsetCardReaderListeners;->UNSET_LISTENER:Lcom/squareup/cardreader/UnsetCardReaderListeners;

    iput-object v1, v0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    return-void
.end method

.method private abortSecureSessionFromHttpError(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/ReceivedResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReader;",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "+[B>;)V"
        }
    .end annotation

    .line 267
    instance-of v0, p2, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_0

    .line 268
    sget-object p2, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->NETWORK_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->abortSecureSession(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    goto :goto_0

    .line 269
    :cond_0
    instance-of v0, p2, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_1

    .line 270
    sget-object p2, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->CLIENT_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->abortSecureSession(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    goto :goto_0

    .line 271
    :cond_1
    instance-of p2, p2, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz p2, :cond_2

    .line 272
    sget-object p2, Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;->SERVER_ERROR:Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->abortSecureSession(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/dipper/DipperEventHandler;)Lcom/squareup/log/ReaderEventLogger;
    .locals 0

    .line 98
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/cardreader/dipper/DipperEventHandler;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->timeoutRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method private cancelLoggingTimeout()V
    .locals 2

    .line 490
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->timeoutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 491
    iput-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->timeoutRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method private handlePossibleSecureSessionOffline(Lcom/squareup/cardreader/CardReaderInfo;)Z
    .locals 2

    .line 454
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v0}, Lcom/squareup/connectivity/ConnectivityMonitor;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_OFFLINE_SWIPE_ONLY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method abortSecureSession(Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 3

    .line 278
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    .line 279
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/CardReader;->abortSecureSession(Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    return-void
.end method

.method public initializingSecureSession(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->initializingSecureSession(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public synthetic lambda$null$0$DipperEventHandler(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "Establishing cr_securesession"

    .line 259
    invoke-static {p2, v0}, Lcom/squareup/receiving/SuccessOrFailureLogger;->logFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/String;)V

    .line 260
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->abortSecureSessionFromHttpError(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/ReceivedResponse;)V

    return-void
.end method

.method public synthetic lambda$sendSecureSessionMessageToServer$1$DipperEventHandler(Lcom/squareup/cardreader/CardReader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 256
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/cardreader/dipper/-$$Lambda$cnNyrqY_nsZUpMF307mkmS0LmSU;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$cnNyrqY_nsZUpMF307mkmS0LmSU;-><init>(Lcom/squareup/cardreader/CardReader;)V

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$DipperEventHandler$tlEmpRRQsGTDxyqJSF-oxfWhGwY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$DipperEventHandler$tlEmpRRQsGTDxyqJSF-oxfWhGwY;-><init>(Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReader;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public onAudioReaderFailedToConnect(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onAudioReaderFailedToConnect(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_LOW_BATTERY:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->READER_POWER_OFF:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 232
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onBatteryUpdate(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onBatteryUpdate(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 380
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/DippedCardTracker;->onCardInserted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 382
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->setActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_INSERTED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 384
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 383
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 386
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasSecureSession()Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/DippedCardTracker;->onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onCardReaderBackendInitialized(Lcom/squareup/cardreader/CardReader;)V
    .locals 4

    .line 163
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R6:Lcom/squareup/settings/server/Features$Feature;

    .line 164
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    .line 165
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    .line 170
    new-instance v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    invoke-direct {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;-><init>()V

    .line 171
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_209030_QUICKCHIP:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 172
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_PINBLOCK_V2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 173
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_ACCOUNT_TYPE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->account_type_selection:Ljava/lang/Boolean;

    .line 174
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SPOC_PRNG_SEED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 175
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_SONIC_BRANDING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->sonic_branding:Ljava/lang/Boolean;

    .line 176
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_COMMON_DEBIT_SUPPORT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->common_debit_support:Ljava/lang/Boolean;

    .line 177
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->READER_FW_FELICA_NOTIFICATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->felica_notification:Ljava/lang/Boolean;

    .line 178
    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getMcc()I

    move-result v0

    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v2}, Lcom/squareup/protos/common/CurrencyCode;->getValue()I

    move-result v2

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object v1

    invoke-interface {p1, v0, v2, v1}, Lcom/squareup/cardreader/CardReader;->initializeFeatures(IILcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCardReaderBackendInitialized(Lcom/squareup/cardreader/CardReader;)V

    .line 181
    new-instance v0, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/dipper/DipperEventHandler$TimeoutRunnable;-><init>(Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/dipper/DipperEventHandler$1;)V

    iput-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->timeoutRunnable:Ljava/lang/Runnable;

    .line 182
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->timeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x2710

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 398
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/DippedCardTracker;->onCardRemoved(Lcom/squareup/cardreader/CardReaderId;)V

    .line 400
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->isActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_CARD_REMOVED:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 402
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 401
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 406
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 407
    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    .line 408
    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasStartedPaymentOnReader()Z

    move-result v0

    if-nez v0, :cond_1

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onCoreDump(Lcom/squareup/cardreader/CardReader;[B[B)V
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCoreDump(Lcom/squareup/cardreader/CardReader;[B[B)V

    return-void
.end method

.method public onDeviceUnsupported(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onDeviceUnsupported(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 471
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {p1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result p1

    if-nez p1, :cond_0

    .line 472
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setCardReaderStatusListener(Lcom/squareup/cardreader/CardReaderStatusListener;)V

    .line 473
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/CardReaderListeners;->setMagSwipeListener(Lcom/squareup/cardreader/MagSwipeListener;)V

    .line 474
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setFirmwareUpdateListener(Lcom/squareup/cardreader/FirmwareUpdater$Listener;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 479
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetCardReaderStatusListener()V

    .line 481
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetMagSwipeListener()V

    .line 482
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->localCardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetFirmwareUpdateListener()V

    .line 486
    :cond_0
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cancelLoggingTimeout()V

    return-void
.end method

.method public onFullCommsEstablished(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 3

    .line 203
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_FULL_COMMS:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 204
    sget-object v0, Lcom/squareup/cardreader/dipper/DipperEventHandler$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 217
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Full comms established on unknown reader type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->nfcReaderHasConnected:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->nfcReaderHasConnected:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->r12HasConnected:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->r6HasConnected:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 221
    :goto_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onFullCommsEstablished(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onInitFirmwareUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onInitFirmwareUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onInitRegisterUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_REGISTER_UPGRADE_REQUIRED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onInitRegisterUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onInitializationComplete(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_READY_FOR_PAYMENTS:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 370
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    .line 371
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->getPairingEventListener()Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;->succeededPairing(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 375
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onInitializationComplete(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 376
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cancelLoggingTimeout()V

    return-void
.end method

.method public onMagSwipeFailed(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 4

    .line 435
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 436
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 435
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 437
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/MagSwipeListener;->onMagSwipeFailed(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    return-void
.end method

.method public onMagSwipePassthrough(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 4

    .line 442
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 443
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 442
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 444
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/MagSwipeListener;->onMagSwipePassthrough(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onMagSwipeSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 4

    .line 428
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 429
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 428
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 430
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/MagSwipeListener;->onMagSwipeSuccess(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onReaderError(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onReaderError(Lcom/squareup/cardreader/CardReader;)V

    return-void
.end method

.method public onReaderReady()V
    .locals 1

    .line 417
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onReaderReady()V

    return-void
.end method

.method public onSecureSessionAborted(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V
    .locals 2

    .line 285
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->handlePossibleSecureSessionOffline(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 289
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    .line 295
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionAborted(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReader$AbortSecureSessionReason;)V

    return-void
.end method

.method public onSecureSessionDenied(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    .line 324
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_DENIED_BY_SERVER:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 326
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_SERVER_DENIED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionDenied(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/log/ReaderEventLogger;->logSecureSessionResult(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    .line 338
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_FAILED:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    return-void
.end method

.method public onSecureSessionInvalid(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 310
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/DipperEventHandler;->handlePossibleSecureSessionOffline(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_INVALID:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 313
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_SS_RENEWING_SESSION:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionInvalid(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onSecureSessionValid(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->INIT_SS_VALID:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 347
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 349
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionValid(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onTamperData(Lcom/squareup/cardreader/CardReader;[B)V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onTamperData(Lcom/squareup/cardreader/CardReader;[B)V

    return-void
.end method

.method public onTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 3

    .line 353
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->SPE_FWUP_WITHOUT_MATCHING_TMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 354
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->supportsTmsCountryCodeCheck()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getTmsCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->userCountryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/CountryCode;

    .line 359
    iget-object v2, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {v2, p1, v0, v1}, Lcom/squareup/log/ReaderEventLogger;->logTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/CountryCode;Lcom/squareup/CountryCode;)V

    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 362
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderInfo;->setTmsCountryCodeValidState(Z)V

    .line 365
    :cond_1
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onUseChipCard(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 4

    .line 421
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->PAYMENT_USE_CHIP_CARD:Lcom/squareup/analytics/ReaderEventName;

    sget-object v2, Lcom/squareup/cardreader/PaymentTimings;->EMPTY:Lcom/squareup/cardreader/PaymentTimings;

    iget-object v3, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 422
    invoke-virtual {v3}, Lcom/squareup/payment/Transaction;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    .line 421
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/squareup/log/ReaderEventLogger;->logPaymentEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/PaymentTimings;Lcom/squareup/protos/client/IdPair;)V

    .line 423
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/MagSwipeListener;->onUseChipCard(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public sendSecureSessionMessageToServer(Lcom/squareup/cardreader/CardReader;[B)V
    .locals 3

    .line 252
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/ReaderEventName;->INIT_SEND_SS_CONTENTS:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->secureSessionService:Lcom/squareup/cardreader/SecureSessionService;

    .line 255
    invoke-interface {v0, p2}, Lcom/squareup/cardreader/SecureSessionService;->sendSecureSessionContents([B)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/cardreader/dipper/-$$Lambda$DipperEventHandler$QXM9D2C4ALwq5hOB08KqX933Keo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/dipper/-$$Lambda$DipperEventHandler$QXM9D2C4ALwq5hOB08KqX933Keo;-><init>(Lcom/squareup/cardreader/dipper/DipperEventHandler;Lcom/squareup/cardreader/CardReader;)V

    .line 256
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 262
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->sendSecureSessionMessageToServer(Lcom/squareup/cardreader/CardReader;[B)V

    return-void
.end method

.method setCardReaderStatusListener(Lcom/squareup/cardreader/CardReaderStatusListener;)V
    .locals 0

    .line 463
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->cardReaderStatusListener:Lcom/squareup/cardreader/CardReaderStatusListener;

    return-void
.end method

.method setMagSwipeListener(Lcom/squareup/cardreader/MagSwipeListener;)V
    .locals 0

    .line 467
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/DipperEventHandler;->magSwipeListener:Lcom/squareup/cardreader/MagSwipeListener;

    return-void
.end method
