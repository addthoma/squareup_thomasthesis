.class public final Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "BatteryLevelWarningScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private percent:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/cardreader/dipper/-$$Lambda$BatteryLevelWarningScreen$aVQt6CMRt0tAcAVNCNdp5MeO8bY;->INSTANCE:Lcom/squareup/cardreader/dipper/-$$Lambda$BatteryLevelWarningScreen$aVQt6CMRt0tAcAVNCNdp5MeO8bY;

    .line 56
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 26
    iput p1, p0, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;->percent:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;)I
    .locals 0

    .line 22
    iget p0, p0, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;->percent:I

    return p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;
    .locals 1

    .line 56
    new-instance v0, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 51
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InMainActivityScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 52
    iget p2, p0, Lcom/squareup/cardreader/dipper/BatteryLevelWarningScreen;->percent:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
