.class public Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;
.super Ljava/lang/Object;
.source "RealEmvDipScreenHandler.java"

# interfaces
.implements Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$NoOpEmvCardInsertRemoveProcessor;
    }
.end annotation


# instance fields
.field private final defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

.field private final emvCardInsertRemoveProcessors:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    .line 47
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->emvCardInsertRemoveProcessors:Ljava/util/Stack;

    .line 51
    new-instance p1, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$NoOpEmvCardInsertRemoveProcessor;

    invoke-direct {p1}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$NoOpEmvCardInsertRemoveProcessor;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->pushEmvProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method private getCurrentProcessor()Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
    .locals 1

    .line 121
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 122
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->emvCardInsertRemoveProcessors:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    return-object v0
.end method

.method private pushEmvProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
    .locals 2

    .line 98
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 100
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->emvCardInsertRemoveProcessors:Ljava/util/Stack;

    const-string v1, "emvCardInsertRemoveProcessor"

    .line 101
    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    .line 100
    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->emvCardInsertRemoveProcessors:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "EmvDipScreenHandler::addEmvCardInsertRemoveProcessor adding duplicate processor"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->pushEmvProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 55
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->getCurrentProcessor()Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;->processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 59
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->getCurrentProcessor()Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;->processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public registerDefaultEmvCardInsertRemoveProcessor(Lmortar/MortarScope;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method public registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
    .locals 1

    .line 67
    new-instance v0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;-><init>(Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z
    .locals 2

    .line 111
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 112
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->emvCardInsertRemoveProcessors:Ljava/util/Stack;

    const-string v1, "emvCardInsertRemoveProcessor"

    .line 113
    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    .line 112
    invoke-virtual {v0, p1}, Ljava/util/Stack;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public shouldDisableEmvDips()Z
    .locals 1

    .line 117
    invoke-direct {p0}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->getCurrentProcessor()Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$NoOpEmvCardInsertRemoveProcessor;

    return v0
.end method
