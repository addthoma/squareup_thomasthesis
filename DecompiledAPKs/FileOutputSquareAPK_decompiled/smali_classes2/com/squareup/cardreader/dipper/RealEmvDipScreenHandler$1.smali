.class Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;
.super Ljava/lang/Object;
.source "RealEmvDipScreenHandler.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;

.field final synthetic val$emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;->this$0:Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;

    iput-object p2, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;->val$emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 69
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;->this$0:Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;

    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;->val$emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->access$000(Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;->this$0:Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler$1;->val$emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/RealEmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    return-void
.end method
