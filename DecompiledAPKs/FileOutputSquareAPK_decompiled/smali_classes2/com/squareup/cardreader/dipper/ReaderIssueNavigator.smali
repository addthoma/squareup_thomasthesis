.class public Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;
.super Ljava/lang/Object;
.source "ReaderIssueNavigator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
        "",
        "flow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "displayer",
        "Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;",
        "posContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Ldagger/Lazy;Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;Lcom/squareup/ui/main/PosContainer;)V",
        "navigateToReaderIssueScreen",
        "",
        "issueScreen",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "showNfcWarningScreen",
        "warningScreen",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;",
        "showReaderConcreteWarningScreen",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;",
        "showReaderErrorScreen",
        "errorScreen",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;",
        "showReaderWarningScreen",
        "showWarningScreen",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final displayer:Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;Lcom/squareup/ui/main/PosContainer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;",
            "Lcom/squareup/ui/main/PosContainer;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "posContainer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->flow:Ldagger/Lazy;

    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->displayer:Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;

    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->posContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method private final showNfcWarningScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->displayer:Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;->getScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;->showNfcWarningScreen(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private final showReaderConcreteWarningScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;)V
    .locals 3

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->posContainer:Lcom/squareup/ui/main/PosContainer;

    new-instance v1, Lcom/squareup/ui/main/HistoryFactory$Simple;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;->getScreen()Lcom/squareup/container/LayoutScreen;

    move-result-object p1

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    const-string v2, "History.single(warningScreen.screen)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    check-cast v1, Lcom/squareup/ui/main/HistoryFactory;

    sget-object p1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-interface {v0, v1, p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory(Lcom/squareup/ui/main/HistoryFactory;Lflow/Direction;)V

    return-void
.end method

.method private final showReaderErrorScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;)V
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/ui/main/errors/ReaderErrorScreen;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;->getParams()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/errors/ReaderErrorScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    .line 49
    invoke-static {v0}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object v0

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-virtual {p1, v0, v1}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method private final showReaderWarningScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;)V
    .locals 2

    .line 40
    new-instance v0, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;->getParams()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/errors/RootReaderWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    .line 42
    invoke-static {v0}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object v0

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-virtual {p1, v0, v1}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method


# virtual methods
.method public navigateToReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 1

    const-string v0, "issueScreen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    instance-of v0, p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->showReaderWarningScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;)V

    goto :goto_0

    .line 31
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->showReaderErrorScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowErrorScreen;)V

    goto :goto_0

    .line 32
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->showReaderConcreteWarningScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowConcreteWarningScreen;)V

    goto :goto_0

    .line 33
    :cond_2
    instance-of v0, p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;->showNfcWarningScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;)V

    :cond_3
    :goto_0
    return-void
.end method
