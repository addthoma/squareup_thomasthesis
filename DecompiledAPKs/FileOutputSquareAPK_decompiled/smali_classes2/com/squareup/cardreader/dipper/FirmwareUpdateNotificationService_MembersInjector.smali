.class public final Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;
.super Ljava/lang/Object;
.source "FirmwareUpdateNotificationService_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;",
        ">;"
    }
.end annotation


# instance fields
.field private final firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->notificationWrapperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;",
            ">;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectFirmwareUpdateDispatcher(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    return-void
.end method

.method public static injectNotificationWrapper(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/notification/NotificationWrapper;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    return-void
.end method

.method public static injectPercentageFormatter(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 63
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/util/Res;)V
    .locals 0

    .line 56
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;)V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectRes(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/util/Res;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectPercentageFormatter(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/text/Formatter;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->firmwareUpdateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectFirmwareUpdateDispatcher(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notification/NotificationWrapper;

    invoke-static {p1, v0}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectNotificationWrapper(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;Lcom/squareup/notification/NotificationWrapper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService_MembersInjector;->injectMembers(Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationService;)V

    return-void
.end method
