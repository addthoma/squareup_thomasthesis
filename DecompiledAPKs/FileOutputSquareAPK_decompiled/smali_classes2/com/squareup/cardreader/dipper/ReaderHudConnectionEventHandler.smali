.class public Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;
.super Ljava/lang/Object;
.source "ReaderHudConnectionEventHandler.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lcom/squareup/cardreader/LibraryLoadErrorListener;


# instance fields
.field private final authenticator:Lcom/squareup/account/LegacyAuthenticator;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

.field private final readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

.field private final readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;


# direct methods
.method public constructor <init>(Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;Lcom/squareup/cardreader/dipper/ReaderHudManager;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p3, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    .line 35
    iput-object p2, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->features:Lcom/squareup/settings/server/Features;

    .line 36
    iput-object p4, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    .line 37
    iput-object p5, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-void
.end method


# virtual methods
.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 53
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 55
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->USE_MAGSTRIPE_ONLY_READERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-nez p1, :cond_1

    .line 57
    iget-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastLegacyReaderConnected()Z

    :cond_1
    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->firmwareUpdateDispatcher:Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/dipper/FirmwareUpdateDispatcher;->hasRecentRebootAssetCompleted(Lcom/squareup/cardreader/CardReaderInfo;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 70
    :cond_1
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->readerHudManager:Lcom/squareup/cardreader/dipper/ReaderHudManager;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderHudManager;->toastReaderDisconnected(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Z

    :cond_3
    return-void
.end method

.method public onLibrariesFailedToLoad()V
    .locals 3

    .line 44
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->LIBRARY_LOAD_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 46
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->warningType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderHudConnectionEventHandler;->readerIssueScreenRequestSink:Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    new-instance v2, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;

    invoke-direct {v2, v0}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowWarningScreen;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)V

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method
