.class public Lcom/squareup/cardreader/ble/RealBleAutoConnector;
.super Ljava/lang/Object;
.source "RealBleAutoConnector.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleAutoConnector;
.implements Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;
.implements Lcom/squareup/cardreader/BlePairingListener;
.implements Lcom/squareup/cardreader/ble/BluetoothStatusListener;
.implements Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;


# static fields
.field private static final squareReaderNamePrefix:Ljava/lang/String; = "Square Reader"


# instance fields
.field private final bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

.field private final bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

.field private bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

.field private final bluetoothAdapter:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

.field private final storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

.field private final wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleEventLogFilter;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p3, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    .line 59
    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    .line 60
    iput-object p4, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 61
    iput-object p5, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    .line 62
    iput-object p2, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothAdapter:Ljavax/inject/Provider;

    .line 63
    iput-object p6, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 64
    iput-object p7, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 65
    iput-object p9, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->features:Lcom/squareup/settings/server/Features;

    .line 66
    iput-object p10, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    .line 67
    invoke-virtual {p8}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->getPairingEventListener()Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    .line 68
    iput-object p11, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    return-void
.end method

.method private connectToWirelessReaders()V
    .locals 4

    .line 179
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->getSavedCardReaders()Ljava/util/Map;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 179
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 182
    iget-object v2, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothAdapter:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothAdapter;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    .line 184
    invoke-static {v2}, Lcom/squareup/cardreader/WirelessConnection$Factory;->forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    .line 186
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/SavedCardReader;

    iget-boolean v1, v1, Lcom/squareup/cardreader/SavedCardReader;->isBluetoothClassic:Z

    if-nez v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v3, Lcom/squareup/analytics/ReaderEventName;->BLE_REGISTERING_FOR_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v1, v3, v2}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    .line 189
    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/CardReaderFactory;->forBleAutoConnect(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_0

    .line 194
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->attemptConnectionToBondedDevices()V

    return-void
.end method

.method private performInitializationTasks()V
    .locals 0

    .line 145
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->createConnections()V

    return-void
.end method


# virtual methods
.method attemptConnectionToBondedDevices()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothAdapter:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 139
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    .line 140
    invoke-virtual {p0, v1}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->onBonded(Landroid/bluetooth/BluetoothDevice;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method createConnections()V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0}, Lcom/squareup/cardreader/loader/LibraryLoader;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->connectToWirelessReaders()V

    :cond_1
    :goto_0
    return-void
.end method

.method public destroy()V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->releaseLogger(Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->removeBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->removeBluetoothStatusListener(Lcom/squareup/cardreader/ble/BluetoothStatusListener;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->removeBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    return-void
.end method

.method public initialize()V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->addBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothStatusReceiver:Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;->addBluetoothStatusListener(Lcom/squareup/cardreader/ble/BluetoothStatusListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/CardReaderListeners;->addBlePairingListener(Lcom/squareup/cardreader/BlePairingListener;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleEventLogFilter:Lcom/squareup/cardreader/ble/BleEventLogFilter;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->getLogger()Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    .line 77
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0}, Lcom/squareup/cardreader/loader/LibraryLoader;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->performInitializationTasks()V

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->addLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    :goto_0
    return-void
.end method

.method public onBluetoothDisabled()V
    .locals 0

    return-void
.end method

.method public onBluetoothEnabled()V
    .locals 0

    .line 102
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->createConnections()V

    return-void
.end method

.method public onBonded(Landroid/bluetooth/BluetoothDevice;)V
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->hasCardReaderWithAddress(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->storedCardReaders:Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    .line 155
    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;->getSavedCardReaders()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 159
    :cond_1
    invoke-static {p1}, Lcom/squareup/cardreader/WirelessConnection$Factory;->forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    .line 161
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result p1

    const/4 v2, 0x2

    if-eq p1, v2, :cond_2

    goto :goto_0

    .line 164
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_R12:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-nez p1, :cond_3

    return-void

    :cond_3
    if-eqz v1, :cond_5

    const-string p1, "Square Reader"

    .line 170
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    goto :goto_0

    .line 173
    :cond_4
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->forBleAutoConnect(Lcom/squareup/cardreader/WirelessConnection;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public onLibrariesFailedToLoad(Ljava/lang/String;)V
    .locals 0

    .line 98
    iget-object p1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    return-void
.end method

.method public onLibrariesLoaded()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/loader/LibraryLoader;->removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->performInitializationTasks()V

    return-void
.end method

.method public onPairingFailed(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V
    .locals 0

    return-void
.end method

.method public onPairingSuccess(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_NO_CONFIRMATION_NEEDED:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    return-void
.end method

.method public onReaderForceUnPair(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->bleLogger:Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLE_PAIRING_FORGETTING_DURING_AUTO_CONNECT:Lcom/squareup/analytics/ReaderEventName;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;->logEvent(Lcom/squareup/analytics/ReaderEventName;Lcom/squareup/cardreader/WirelessConnection;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector;->wirelessPairingEventListener:Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;->failedPairing(Ljava/lang/String;)V

    return-void
.end method
