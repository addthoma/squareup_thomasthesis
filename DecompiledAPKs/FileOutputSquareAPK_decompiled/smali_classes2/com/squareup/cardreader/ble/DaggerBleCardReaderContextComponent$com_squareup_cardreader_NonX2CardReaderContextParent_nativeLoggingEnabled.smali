.class Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;
.super Ljava/lang/Object;
.source "DaggerBleCardReaderContextComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 0

    .line 522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 523
    iput-object p1, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 528
    iget-object v0, p0, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;->nonX2CardReaderContextParent:Lcom/squareup/cardreader/NonX2CardReaderContextParent;

    invoke-interface {v0}, Lcom/squareup/cardreader/NonX2CardReaderContextParent;->nativeLoggingEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 518
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/DaggerBleCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
