.class public final Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt;
.super Ljava/lang/Object;
.source "ConnectionNegotiator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0019\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0080@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0007"
    }
    d2 = {
        "COMPATIBLE_SERVICE_VERSION",
        "",
        "readBondStatus",
        "Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;",
        "connection",
        "Lcom/squareup/blecoroutines/Connection;",
        "(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final COMPATIBLE_SERVICE_VERSION:I = 0x1


# direct methods
.method public static final readBondStatus(Lcom/squareup/blecoroutines/Connection;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blecoroutines/Connection;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p1, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;

    iget v1, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p1, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->label:I

    sub-int/2addr p1, v2

    iput p1, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;-><init>(Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p1, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 164
    iget v2, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p0, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->L$0:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/blecoroutines/Connection;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    .line 167
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 164
    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 165
    sget-object p1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_BOND_STATUS:Ljava/util/UUID;

    const-string v2, "UUID_CHARACTERISTIC_BOND_STATUS"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p0, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->L$0:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/cardreader/ble/ConnectionNegotiatorKt$readBondStatus$1;->label:I

    invoke-static {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2Kt;->readLcrCharacteristic(Lcom/squareup/blecoroutines/Connection;Ljava/util/UUID;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_3

    return-object v1

    .line 164
    :cond_3
    :goto_1
    check-cast p1, Landroid/bluetooth/BluetoothGattCharacteristic;

    .line 166
    invoke-static {p1}, Lcom/squareup/blecoroutines/BleErrorKt;->asUint8(Landroid/bluetooth/BluetoothGattCharacteristic;)Ljava/lang/Integer;

    move-result-object p0

    const-string p1, "connection.readLcrCharac\u2026_STATUS)\n      .asUint8()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    .line 168
    :try_start_0
    invoke-static {}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->values()[Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    move-result-object p1

    aget-object p0, p1, p0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 170
    :catch_0
    new-instance p1, Lcom/squareup/cardreader/ble/ConnectionError;

    sget-object v0, Lcom/squareup/cardreader/ble/Error;->INVALID_BOND_STATUS:Lcom/squareup/cardreader/ble/Error;

    invoke-static {p0}, Lkotlin/coroutines/jvm/internal/Boxing;->boxInt(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-direct {p1, v0, p0}, Lcom/squareup/cardreader/ble/ConnectionError;-><init>(Lcom/squareup/cardreader/ble/Error;Ljava/lang/Integer;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
