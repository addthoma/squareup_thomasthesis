.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideCardReaderNameFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/cardreader/ble/BleDeviceModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/BleDeviceModule;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;)V

    return-object v0
.end method

.method public static provideCardReaderName(Lcom/squareup/cardreader/ble/BleDeviceModule;)Ljava/lang/String;
    .locals 0

    .line 34
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideCardReaderName()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideCardReaderNameFactory;->provideCardReaderName(Lcom/squareup/cardreader/ble/BleDeviceModule;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
