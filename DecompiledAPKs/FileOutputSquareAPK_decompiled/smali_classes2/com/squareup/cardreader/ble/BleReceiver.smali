.class public Lcom/squareup/cardreader/ble/BleReceiver;
.super Landroid/bluetooth/BluetoothGattCallback;
.source "BleReceiver.java"


# instance fields
.field private final address:Ljava/lang/String;

.field private bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

.field private bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

.field private bleSender:Lcom/squareup/cardreader/ble/BleSender;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

.field private destroyed:Z

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 80
    invoke-direct {p0}, Landroid/bluetooth/BluetoothGattCallback;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->address:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    .line 83
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    .line 84
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    .line 85
    iput-object p5, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    const/4 p1, 0x0

    .line 86
    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    .line 87
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleReceiver;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method private logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V
    .locals 2

    .line 410
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "device is null!"

    .line 412
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->address:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    return-void

    .line 419
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->WRONG_DEVICE_IN_GATT_CALLBACK:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    .line 421
    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->callbackName(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p2

    .line 422
    invoke-virtual {p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p2

    .line 420
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-void
.end method

.method private onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 1

    .line 399
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    if-eqz v0, :cond_0

    .line 400
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    const/4 v0, 0x1

    .line 91
    iput-boolean v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v0, 0x0

    .line 92
    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    .line 93
    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    .line 94
    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    return-void
.end method

.method isDestroyed()Z
    .locals 1

    .line 394
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    return v0
.end method

.method public synthetic lambda$onReadRemoteRssi$0$BleReceiver(Lcom/squareup/dipper/events/DipperEvent$RssiReceived;)V
    .locals 1

    .line 390
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    return-void
.end method

.method public onCharacteristicChanged(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;)V
    .locals 2

    const-string v0, "onCharacteristicChanged"

    .line 302
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 303
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "Received onCharacteristicChanged on destroyed BleReceiver!"

    .line 304
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 308
    :cond_0
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_DATA:Ljava/util/UUID;

    invoke-virtual {p1, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 309
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->onDataReceived([B)V

    goto :goto_0

    .line 310
    :cond_1
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

    invoke-virtual {p1, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/16 p1, 0x11

    .line 311
    invoke-virtual {p2, p1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    .line 312
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p2}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p2

    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    .line 314
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->mtu(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    .line 313
    invoke-interface {p2, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 315
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->onMtuReceived(Ljava/lang/Integer;)V

    :goto_0
    return-void

    .line 317
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WTF: unexpected notification on characteristic: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onCharacteristicRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 5

    const-string v0, "onCharacteristicRead"

    .line 238
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 239
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "Received onCharacteristicRead on destroyed BleReceiver!"

    .line 240
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 245
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_READ:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 246
    invoke-static {v0, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 247
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicRead;

    invoke-direct {p1, p2, p3}, Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicRead;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 248
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    return-void

    .line 252
    :cond_1
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_ACK_VECTOR:Ljava/util/UUID;

    invoke-virtual {p1, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/16 v1, 0x12

    const/4 v2, 0x1

    if-eqz p1, :cond_2

    .line 253
    invoke-virtual {p2, v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v0

    const-string p3, "Read ackVector: %d"

    .line 254
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->onAckVectorReceived(Ljava/lang/Integer;)V

    .line 257
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto/16 :goto_0

    .line 258
    :cond_2
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v3, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERVICE_VERSION:Ljava/util/UUID;

    invoke-virtual {p1, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/16 v3, 0x11

    if-eqz p1, :cond_3

    .line 259
    invoke-virtual {p2, v3, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v0

    const-string p3, "Read service version: %d"

    .line 260
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 261
    new-instance p2, Lcom/squareup/cardreader/ble/BleAction$ServiceCharacteristicVersion;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p2, p1}, Lcom/squareup/cardreader/ble/BleAction$ServiceCharacteristicVersion;-><init>(I)V

    invoke-direct {p0, p2}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 262
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto/16 :goto_0

    .line 263
    :cond_3
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v4, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_BOND_STATUS:Ljava/util/UUID;

    invoke-virtual {p1, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 264
    invoke-virtual {p2, v3, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v0

    const-string p3, "Read bondState: %d"

    .line 265
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    new-instance p2, Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;

    invoke-static {}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->values()[Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    move-result-object p3

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    aget-object p1, p3, p1

    invoke-direct {p2, p1}, Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;-><init>(Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;)V

    invoke-direct {p0, p2}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 268
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto/16 :goto_0

    .line 269
    :cond_4
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v4, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_COMMS_VERSION:Ljava/util/UUID;

    invoke-virtual {p1, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 270
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getValue()[B

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    .line 271
    invoke-static {p1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object p3

    aput-object p3, p2, v0

    const-string p3, "Read comms_version: %s"

    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    new-instance p2, Lcom/squareup/cardreader/ble/BleAction$ReceivedReaderCommsVersion;

    invoke-direct {p2, p1}, Lcom/squareup/cardreader/ble/BleAction$ReceivedReaderCommsVersion;-><init>([B)V

    invoke-direct {p0, p2}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 274
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto/16 :goto_0

    .line 275
    :cond_5
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v4, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

    invoke-virtual {p1, v4}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 276
    invoke-virtual {p2, v3, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v0

    const-string p3, "Read mtuSize: %d"

    .line 277
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 279
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->onMtuReceived(Ljava/lang/Integer;)V

    .line 280
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_0

    .line 281
    :cond_6
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v3, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_CONN_INTERVAL:Ljava/util/UUID;

    invoke-virtual {p1, v3}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 282
    invoke-virtual {p2, v1, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getIntValue(II)Ljava/lang/Integer;

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v0

    const-string p3, "Read connectionInterval: %d"

    .line 283
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    new-instance p2, Lcom/squareup/cardreader/ble/BleAction$ReceivedConnectionInterval;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p2, p1}, Lcom/squareup/cardreader/ble/BleAction$ReceivedConnectionInterval;-><init>(I)V

    invoke-direct {p0, p2}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 286
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_0

    .line 287
    :cond_7
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p1

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    invoke-virtual {p1, v1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 288
    invoke-virtual {p2, v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getStringValue(I)Ljava/lang/String;

    move-result-object p1

    new-array p2, v2, [Ljava/lang/Object;

    aput-object p1, p2, v0

    const-string p3, "Read serial number: %s"

    .line 289
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    new-instance p2, Lcom/squareup/cardreader/ble/BleAction$ReceivedSerialNumber;

    invoke-direct {p2, p1}, Lcom/squareup/cardreader/ble/BleAction$ReceivedSerialNumber;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 292
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_0

    .line 294
    :cond_8
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_UNKNOWN:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 295
    invoke-static {v0, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p3

    invoke-interface {p1, p3}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 296
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$ReceivedUnknownCharacteristicRead;

    invoke-direct {p1, p2}, Lcom/squareup/cardreader/ble/BleAction$ReceivedUnknownCharacteristicRead;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    :goto_0
    return-void
.end method

.method public onCharacteristicWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattCharacteristic;I)V
    .locals 1

    const-string v0, "onCharacteristicWrite"

    .line 344
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 345
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "Received onCharacteristicWrite on destroyed BleReceiver!"

    .line 346
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 351
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 352
    invoke-static {v0, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 353
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicWrite;

    invoke-direct {p1, p2, p3}, Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicWrite;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;I)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 355
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object p2

    aput-object p2, p1, v0

    const-string p2, "Callback: successfully wrote GATT Characteristic %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    const-string v0, "onConnectionStateChange"

    .line 138
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 139
    new-instance p1, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CONNECTION_STATE_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-direct {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    .line 140
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->newState(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p1

    .line 141
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p1

    .line 143
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array p2, v1, [Ljava/lang/Object;

    const-string p3, "Received onConnectionStateChange on destroyed BleReceiver!"

    .line 144
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p2, 0x1

    .line 145
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->bleReceiverAlreadyDestroyed(Z)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    .line 146
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p2}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-void

    :cond_0
    if-eqz p3, :cond_3

    const/4 v0, 0x2

    if-eq p3, v0, :cond_2

    if-eqz p2, :cond_1

    .line 165
    new-instance p3, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;

    invoke-direct {p3, p2}, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;-><init>(I)V

    invoke-direct {p0, p3}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 166
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_0

    :cond_1
    new-array p2, v1, [Ljava/lang/Object;

    const-string p3, "Callback: ignoring connection state of (dis)/connecting"

    .line 168
    invoke-static {p3, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-array p3, v1, [Ljava/lang/Object;

    const-string v0, "Callback: Connection state connected"

    .line 152
    invoke-static {v0, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    new-instance p3, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;

    invoke-direct {p3, p2}, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;-><init>(I)V

    invoke-direct {p0, p3}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 154
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_0

    :cond_3
    new-array p3, v1, [Ljava/lang/Object;

    const-string v0, "Callback: Connection state disconnected"

    .line 157
    invoke-static {v0, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    new-instance p3, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    invoke-direct {p3, p2}, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;-><init>(I)V

    invoke-direct {p0, p3}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 159
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p2}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    .line 172
    :goto_0
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p2}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-void
.end method

.method public onDescriptorRead(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 0

    .line 373
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "WTF: onDescriptorRead?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onDescriptorWrite(Landroid/bluetooth/BluetoothGatt;Landroid/bluetooth/BluetoothGattDescriptor;I)V
    .locals 1

    const-string v0, "onDescriptorWrite"

    .line 324
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 325
    iget-boolean p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    const-string p2, "Received onDescriptorWrite on destroyed BleReceiver!"

    .line 326
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    if-eqz p3, :cond_1

    .line 331
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->DESCRIPTOR_WRITE:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 332
    invoke-static {v0, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattDescriptor;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 333
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;

    invoke-direct {p1, p2, p3}, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;-><init>(Landroid/bluetooth/BluetoothGattDescriptor;I)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    goto :goto_0

    :cond_1
    new-array p1, v0, [Ljava/lang/Object;

    const-string p3, "Callback: wrote GATT Descriptor success"

    .line 335
    invoke-static {p3, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$NotificationEnabled;

    invoke-virtual {p2}, Landroid/bluetooth/BluetoothGattDescriptor;->getCharacteristic()Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/cardreader/ble/BleAction$NotificationEnabled;-><init>(Landroid/bluetooth/BluetoothGattCharacteristic;)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 338
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    return-void
.end method

.method public onMtuChanged(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 2

    const-string v0, "onMtuChanged"

    .line 361
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 362
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    sget-object v1, Lcom/squareup/cardreader/ble/GattConnectionEventName;->MTU_CHANGED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;-><init>(Lcom/squareup/cardreader/ble/GattConnectionEventName;)V

    .line 364
    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->mtu(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p2

    invoke-virtual {p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->status(I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p2

    .line 363
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    return-void
.end method

.method public onReadRemoteRssi(Landroid/bluetooth/BluetoothGatt;II)V
    .locals 1

    if-eqz p3, :cond_0

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    .line 385
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->address:Ljava/lang/String;

    aput-object v0, p1, p2

    const/4 p2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p1, p2

    const-string p2, "Failed to read RSSI for reader %s. Status: %d"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 388
    :cond_0
    new-instance p3, Lcom/squareup/dipper/events/BleDevice;

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothGatt;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p1

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->address:Ljava/lang/String;

    invoke-direct {p3, p1, v0}, Lcom/squareup/dipper/events/BleDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    new-instance p1, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;

    invoke-direct {p1, p3, p2}, Lcom/squareup/dipper/events/DipperEvent$RssiReceived;-><init>(Lcom/squareup/dipper/events/BleDevice;I)V

    .line 390
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleReceiver;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance p3, Lcom/squareup/cardreader/ble/-$$Lambda$BleReceiver$g4vBoZaz5A1Yilk7cpQwJmSndZA;

    invoke-direct {p3, p0, p1}, Lcom/squareup/cardreader/ble/-$$Lambda$BleReceiver$g4vBoZaz5A1Yilk7cpQwJmSndZA;-><init>(Lcom/squareup/cardreader/ble/BleReceiver;Lcom/squareup/dipper/events/DipperEvent$RssiReceived;)V

    invoke-interface {p2, p3}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onReliableWriteCompleted(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 0

    .line 368
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "WTF: onReliableWriteCompleted?"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothGatt;I)V
    .locals 7

    const-string v0, "onServicesDiscovered"

    .line 176
    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleReceiver;->logIfWrongReader(Landroid/bluetooth/BluetoothGatt;Ljava/lang/String;)V

    .line 177
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleReceiver;->destroyed:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array p1, v1, [Ljava/lang/Object;

    const-string p2, "Received onServicesDiscovered on destroyed BleReceiver!"

    .line 178
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    if-nez p2, :cond_6

    .line 183
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object v0

    if-nez v0, :cond_1

    .line 185
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_CACHED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 186
    invoke-static {v0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 187
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    invoke-direct {p1, v1, p2}, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;-><init>(ZI)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 188
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto/16 :goto_2

    .line 191
    :cond_1
    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->LCR_CHARACTERISTICS:[Ljava/util/UUID;

    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    aget-object v5, v2, v4

    .line 192
    invoke-virtual {v0, v5}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v6

    if-nez v6, :cond_2

    .line 193
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object p2, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_MISSING_CHARACTERISTIC:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    .line 195
    invoke-static {p2, v0, v5}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Ljava/util/UUID;Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p2

    .line 194
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 196
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$OldServicesCached;

    invoke-direct {p1}, Lcom/squareup/cardreader/ble/BleAction$OldServicesCached;-><init>()V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 197
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    return-void

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 202
    :cond_3
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_DEVICE_INFORMATION_SERVICE:Ljava/util/UUID;

    .line 203
    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGatt;->getService(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattService;

    move-result-object p1

    if-nez p1, :cond_4

    .line 208
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_DISCOVERY_FAILED_CACHED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 209
    invoke-static {v0}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 210
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    invoke-direct {p1, v1, p2}, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;-><init>(ZI)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 211
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_1

    .line 212
    :cond_4
    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object p1

    if-nez p1, :cond_5

    .line 214
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object p2, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICE_MISSING_CHARACTERISTIC:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    sget-object v0, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_DEVICE_INFORMATION_SERVICE:Ljava/util/UUID;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    .line 216
    invoke-static {p2, v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Ljava/util/UUID;Ljava/util/UUID;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object p2

    .line 215
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 218
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$OldServicesCached;

    invoke-direct {p1}, Lcom/squareup/cardreader/ble/BleAction$OldServicesCached;-><init>()V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 219
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    return-void

    :cond_5
    :goto_1
    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Callback: discovered services successful"

    .line 223
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    const/4 v0, 0x1

    invoke-direct {p1, v0, p2}, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;-><init>(ZI)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 225
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    goto :goto_2

    .line 228
    :cond_6
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    sget-object v0, Lcom/squareup/cardreader/ble/GattConnectionEventName;->SERVICES_DISCOVERED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 229
    invoke-static {v0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;I)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 230
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    invoke-direct {p1, v1, p2}, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;-><init>(ZI)V

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleReceiver;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    .line 231
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleReceiver;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->onActionCompleted()V

    :goto_2
    return-void
.end method
