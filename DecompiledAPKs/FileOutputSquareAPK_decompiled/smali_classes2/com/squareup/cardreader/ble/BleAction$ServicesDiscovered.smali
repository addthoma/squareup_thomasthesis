.class public Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;
.super Ljava/lang/Object;
.source "BleAction.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServicesDiscovered"
.end annotation


# instance fields
.field public final status:I

.field public final success:Z


# direct methods
.method public constructor <init>(ZI)V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-boolean p1, p0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->success:Z

    .line 79
    iput p2, p0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->status:I

    return-void
.end method


# virtual methods
.method public describe()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 83
    iget-boolean v1, p0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->success:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->status:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "Services Discovered, success: %b, status: %d"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
