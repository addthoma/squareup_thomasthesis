.class public Lcom/squareup/cardreader/ble/BleEventLogFilter;
.super Ljava/lang/Object;
.source "BleEventLogFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final loggerStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 51
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 52
    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->loggerStack:Ljava/util/Stack;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/cardreader/ble/BleEventLogFilter;Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V
    .locals 0

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->logEvent(Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V

    return-void
.end method

.method private logEvent(Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->loggerStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 96
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->build()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;

    move-result-object p1

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p1, p2, v0

    const-string v0, "To EventStream: %s"

    .line 98
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {p2, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleEventLogFilter;->logOhSnap(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;)V

    return-void
.end method

.method private logOhSnap(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;)V
    .locals 5

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    .line 81
    iget-object v2, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->name:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->error:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->macAddress:Ljava/lang/String;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;->hardwareSerialNumberLast4:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object p1, v1, v2

    array-length p1, v1

    :goto_0
    if-ge v3, p1, :cond_1

    aget-object v2, v1, v3

    if-eqz v2, :cond_0

    .line 86
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 90
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->BLE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const-string v2, "; "

    invoke-static {v0, v2}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getLogger()Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;
    .locals 2

    .line 60
    new-instance v0, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;-><init>(Lcom/squareup/cardreader/ble/BleEventLogFilter;)V

    .line 61
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->loggerStack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public releaseLogger(Lcom/squareup/cardreader/ble/BleEventLogFilter$ContextualBleLogger;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleEventLogFilter;->loggerStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    const-string v0, "Cannot release a logger when you are not in the current BLE logging context"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    return-void
.end method
