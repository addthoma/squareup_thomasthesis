.class public Lcom/squareup/cardreader/ble/GlobalBleModule;
.super Ljava/lang/Object;
.source "GlobalBleModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/blescan/BluetoothModule;
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getRealBluetoothDevicesCountInitializer(Landroid/app/Application;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/logging/BluetoothDevicesCountInitializer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ")",
            "Lcom/squareup/logging/BluetoothDevicesCountInitializer;"
        }
    .end annotation

    .line 147
    new-instance v6, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    const-string v0, "bluetooth"

    .line 148
    invoke-virtual {p0, v0}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/bluetooth/BluetoothManager;

    move-object v0, v6

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)V

    return-object v6
.end method

.method public static getRealBluetoothUtils(Landroid/app/Application;)Lcom/squareup/cardreader/BluetoothUtils;
    .locals 2

    .line 127
    new-instance v0, Lcom/squareup/blescan/RealBluetoothUtils;

    const-string v1, "bluetooth"

    .line 128
    invoke-virtual {p0, v1}, Landroid/app/Application;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    invoke-direct {v0, p0, v1}, Lcom/squareup/blescan/RealBluetoothUtils;-><init>(Landroid/app/Application;Landroid/bluetooth/BluetoothManager;)V

    return-object v0
.end method

.method public static getRealSystemBleScanner(Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/SystemBleScanner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;"
        }
    .end annotation

    .line 135
    invoke-interface {p0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBle()Z

    move-result p0

    if-nez p0, :cond_0

    .line 136
    sget-object p0, Lcom/squareup/cardreader/ble/SystemBleScanner;->NO_OP:Lcom/squareup/cardreader/ble/SystemBleScanner;

    return-object p0

    .line 137
    :cond_0
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt p0, v0, :cond_1

    .line 138
    new-instance p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;

    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;)V

    return-object p0

    .line 140
    :cond_1
    new-instance p0, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;

    invoke-direct {p0, p1, p3, p4, p5}, Lcom/squareup/cardreader/ble/SystemBleScannerKitKat;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)V

    return-object p0
.end method

.method static provideBleDispatcher(Landroid/os/Handler;)Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 117
    invoke-static {p0}, Lkotlinx/coroutines/android/HandlerDispatcherKt;->from(Landroid/os/Handler;)Lkotlinx/coroutines/android/HandlerDispatcher;

    move-result-object p0

    return-object p0
.end method

.method static provideBleThread(Landroid/os/Handler;)Ljava/lang/Thread;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 112
    invoke-virtual {p0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-virtual {p0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object p0

    return-object p0
.end method

.method static provideBleThreadEnforcer(Ljavax/inject/Provider;)Lcom/squareup/thread/enforcer/ThreadEnforcer;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Thread;",
            ">;)",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;"
        }
    .end annotation

    .line 122
    sget-object v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;->Companion:Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$hVHdaclZFgHwxRHeBBglW2va5SI;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/ble/-$$Lambda$hVHdaclZFgHwxRHeBBglW2va5SI;-><init>(Ljavax/inject/Provider;)V

    invoke-virtual {v0, v1}, Lcom/squareup/thread/enforcer/ThreadEnforcer$Companion;->invoke(Lkotlin/jvm/functions/Function0;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method provideBleBondingBroadcastReceiver()Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 79
    new-instance v0, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;-><init>()V

    return-object v0
.end method

.method provideBleExecutor()Landroid/os/Handler;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 104
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Sq-BLE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 106
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 107
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-object v1
.end method

.method provideBleScanFilter()Lcom/squareup/cardreader/ble/BleScanFilter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 83
    new-instance v0, Lcom/squareup/cardreader/ble/BleScanFilter;

    invoke-direct {v0}, Lcom/squareup/cardreader/ble/BleScanFilter;-><init>()V

    return-object v0
.end method

.method provideBleScanner(Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/ble/SystemBleScanner;Lcom/squareup/cardreader/ble/BleScanFilter;)Lcom/squareup/cardreader/ble/BleScanner;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/cardreader/ble/BleScanner;

    invoke-direct {v0, p2, p1, p3, p4}, Lcom/squareup/cardreader/ble/BleScanner;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/ble/SystemBleScanner;Lcom/squareup/cardreader/ble/BleScanFilter;)V

    return-object v0
.end method

.method provideBluetoothDiscoverer(Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;"
        }
    .end annotation

    .line 94
    new-instance v0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/thread/executor/MainThread;)V

    return-object v0
.end method

.method provideBluetoothDiscoveryBroadcastReceiver(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 100
    new-instance v0, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/bluetooth/BluetoothDiscoveryBroadcastReceiver;-><init>(Lcom/squareup/cardreader/bluetooth/BluetoothDiscoverer;)V

    return-object v0
.end method

.method provideBluetoothStatusReceiver(Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/BluetoothUtils;)Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 74
    new-instance v0, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;-><init>(Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/BluetoothUtils;)V

    return-object v0
.end method
