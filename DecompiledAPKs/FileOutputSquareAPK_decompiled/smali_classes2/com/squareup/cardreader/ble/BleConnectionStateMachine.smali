.class public Lcom/squareup/cardreader/ble/BleConnectionStateMachine;
.super Ljava/lang/Object;
.source "BleConnectionStateMachine.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;,
        Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;,
        Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;,
        Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;
    }
.end annotation


# static fields
.field static final BLE_MESSAGE_MAGIC_NUMBER:I = 0x2a

.field static final CONNECTION_SETUP_BONDING_DELAY_MILLIS:I = 0x3e8

.field static final CONNECTION_SETUP_MTU_DELAY_MILLIS:I = 0x1f4

.field static final MAX_RECONNECT_ATTEMPTS:I = 0x3

.field static final PAIRING_TIMEOUT_DEFAULT_MILLIS:I = 0x1f40

.field static final PAIRING_TIMEOUT_WAITING_FOR_CONNECT_MILLIS:I = 0x7530

.field static final PAIRING_TIMEOUT_WAITING_FOR_DISCONNECT_MILLIS:I = 0x2710


# instance fields
.field private final application:Landroid/app/Application;

.field private final bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

.field private final bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

.field private bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

.field private bleHandler:Landroid/os/Handler;

.field private final bleReceiverFactory:Lcom/squareup/cardreader/ble/BleReceiverFactory;

.field private final bleSender:Lcom/squareup/cardreader/ble/BleSender;

.field private final bleThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final bondStatusRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

.field private final commsRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private previousAction:Lcom/squareup/cardreader/ble/BleAction;

.field private reconnectAttempts:I

.field private state:Lcom/squareup/dipper/events/BleConnectionState;

.field private final timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/cardreader/ble/BleBackendLegacy;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Landroid/os/Handler;Lcom/squareup/cardreader/ble/BleReceiverFactory;Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothDevice;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->application:Landroid/app/Application;

    .line 137
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    .line 138
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    .line 139
    iput-object p4, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    .line 141
    new-instance p1, Landroid/os/Handler;

    invoke-virtual {p5}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object p2

    invoke-direct {p1, p2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    .line 142
    iput-object p6, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleReceiverFactory:Lcom/squareup/cardreader/ble/BleReceiverFactory;

    .line 143
    iput-object p7, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    .line 144
    iput-object p8, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    .line 145
    iput-object p9, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 146
    iput-object p10, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 147
    iput-object p11, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    .line 148
    iput-object p12, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 149
    iput-object p13, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 151
    new-instance p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bondStatusRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;

    .line 152
    new-instance p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    invoke-direct {p1, p0, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    .line 153
    new-instance p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;

    invoke-direct {p1, p0, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->commsRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;

    .line 154
    sget-object p1, Lcom/squareup/dipper/events/BleConnectionState;->CREATED:Lcom/squareup/dipper/events/BleConnectionState;

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Landroid/app/Application;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->application:Landroid/app/Application;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleConnectType;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleReceiverFactory;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleReceiverFactory:Lcom/squareup/cardreader/ble/BleReceiverFactory;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 0

    .line 96
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleActionInternal(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)Lcom/squareup/cardreader/ble/BleSender;
    .locals 0

    .line 96
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    return-object p0
.end method

.method private bleConnection()Lcom/squareup/cardreader/WirelessConnection;
    .locals 1

    .line 698
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-static {v0}, Lcom/squareup/cardreader/WirelessConnection$Factory;->forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    return-object v0
.end method

.method private cancelRunners()V
    .locals 2

    .line 494
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 495
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->commsRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 496
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bondStatusRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method private cardReaderAlreadyInitialized()Z
    .locals 2

    .line 691
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object v1, Lcom/squareup/dipper/events/BleConnectionState;->READY:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object v1, Lcom/squareup/dipper/events/BleConnectionState;->POSSIBLY_DISCONNECTING:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_RECONNECT_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private connectSilently()V
    .locals 2

    .line 488
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cancelRunners()V

    .line 489
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->CREATED:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 490
    new-instance v0, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectType;->RECONNECT_SILENTLY:Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;-><init>(Lcom/squareup/cardreader/ble/BleConnectType;)V

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleActionInternal(Lcom/squareup/cardreader/ble/BleAction;)V

    return-void
.end method

.method private createBond()V
    .locals 5

    const-string v0, "Unable to remove bond programmatically. Ask the user to."

    .line 441
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-virtual {v1, p0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->addBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V

    .line 442
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    const-string v2, "#createBond didn\'t work, trying to remove the bond."

    new-array v3, v1, [Ljava/lang/Object;

    .line 448
    invoke-static {v2, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "removeBond"

    new-array v4, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 450
    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    .line 451
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Successfully removed bond, reconnecting silently."

    new-array v3, v1, [Ljava/lang/Object;

    .line 452
    invoke-static {v2, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 453
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectSilently()V

    goto :goto_0

    .line 455
    :cond_1
    new-instance v2, Lcom/squareup/cardreader/ble/BleAction$UnableToCreateBond;

    invoke-direct {v2}, Lcom/squareup/cardreader/ble/BleAction$UnableToCreateBond;-><init>()V

    invoke-virtual {p0, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    new-array v2, v1, [Ljava/lang/Object;

    .line 456
    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto :goto_2

    .line 459
    :catch_0
    :try_start_1
    new-instance v2, Lcom/squareup/cardreader/ble/BleAction$UnableToCreateBond;

    invoke-direct {v2}, Lcom/squareup/cardreader/ble/BleAction$UnableToCreateBond;-><init>()V

    invoke-virtual {p0, v2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    new-array v1, v1, [Ljava/lang/Object;

    .line 460
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 462
    :goto_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->removeBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V

    :goto_1
    return-void

    :goto_2
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-virtual {v1, p0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->removeBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V

    .line 463
    throw v0
.end method

.method private destroy()V
    .locals 2

    .line 577
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleConnectionStateHelper;->isDestroying(Lcom/squareup/dipper/events/BleConnectionState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Must be in DESTROYING state to destroy: state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyForUnexpectedState(Ljava/lang/String;)V

    return-void

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    .line 583
    iput v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    .line 586
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleSender;->closeConnection()V

    .line 587
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleReceiverFactory:Lcom/squareup/cardreader/ble/BleReceiverFactory;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleReceiverFactory;->destroyViolently()V

    .line 588
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$wWvYKpYFtLfOQE2oU9sT_x29ozI;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$wWvYKpYFtLfOQE2oU9sT_x29ozI;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private destroyForUnexpectedState(Ljava/lang/String;)V
    .locals 1

    .line 704
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_UNEXPECTED_STATE:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V

    .line 705
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 706
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectWhenAvailable()V

    return-void
.end method

.method private destroyFromSellerAction()V
    .locals 2

    .line 567
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object v1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FROM_SELLER_ACTION:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq v0, v1, :cond_0

    const-string v0, "Must be in DESTROYING_FROM_SELLER_ACTION state"

    .line 568
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyForUnexpectedState(Ljava/lang/String;)V

    return-void

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleSender;->closeConnection()V

    return-void
.end method

.method private firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V
    .locals 8

    .line 651
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    if-ne p2, v0, :cond_0

    const/4 v0, 0x0

    .line 652
    iput v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    const/4 v0, 0x3

    const/4 v6, 0x3

    goto :goto_0

    .line 657
    :cond_0
    iget v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    move v6, v0

    .line 659
    :goto_0
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v4

    .line 661
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v7, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$i2ajRuGe7F8-LCZ8PCQBi32peog;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;I)V

    invoke-interface {v0, v7}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private firePairingSuccess()V
    .locals 3

    const/4 v0, 0x0

    .line 631
    iput v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    .line 632
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    .line 634
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/BleSender;->beginMonitoringRemoteRssi()V

    .line 636
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$QuwKTO01KHy23nGbhyCdxTOIfLA;

    invoke-direct {v2, p0, v0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$QuwKTO01KHy23nGbhyCdxTOIfLA;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/WirelessConnection;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private fireReaderForceUnpaired()V
    .locals 3

    const/4 v0, 0x0

    .line 680
    iput v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    .line 681
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    .line 682
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$QoLTL6P5fyYiHLRHx0OrywdYgUI;

    invoke-direct {v2, p0, v0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$QoLTL6P5fyYiHLRHx0OrywdYgUI;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/WirelessConnection;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getBleDevice()Lcom/squareup/dipper/events/BleDevice;
    .locals 3

    .line 675
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    .line 676
    new-instance v1, Lcom/squareup/dipper/events/BleDevice;

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/squareup/dipper/events/BleDevice;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V
    .locals 3

    .line 595
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cancelRunners()V

    .line 596
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$PairingTimeout;

    if-eqz v0, :cond_0

    .line 597
    sget-object p1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_PAIRING_TIMEOUT:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 598
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 599
    sget-object p1, Lcom/squareup/dipper/events/BleErrorType;->TIMEOUT:Lcom/squareup/dipper/events/BleErrorType;

    sget-object p2, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    goto/16 :goto_1

    .line 600
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;

    if-nez v0, :cond_5

    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicRead;

    if-nez v0, :cond_5

    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$FailedCharacteristicWrite;

    if-nez v0, :cond_5

    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$FailedDescriptorWrite;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 614
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    if-eqz v0, :cond_3

    .line 615
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object p2, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq p1, p2, :cond_2

    .line 616
    sget-object p1, Lcom/squareup/dipper/events/BleErrorType;->UNKNOWN_ERROR_TYPE:Lcom/squareup/dipper/events/BleErrorType;

    sget-object p2, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->WILL_TRY_TO_RECONNECT:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    .line 618
    :cond_2
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->connectSilently()V

    goto :goto_1

    .line 619
    :cond_3
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$DestroyReader;

    if-eqz v0, :cond_4

    .line 620
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 621
    sget-object p1, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FROM_SELLER_ACTION:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 622
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyFromSellerAction()V

    goto :goto_1

    .line 625
    :cond_4
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 626
    invoke-interface {p1}, Lcom/squareup/cardreader/ble/BleAction;->describe()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    const/4 p1, 0x2

    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget p2, p2, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v1, p1

    const-string p1, "Failing on action \"%s\" in state %s on id: %d"

    .line 625
    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 624
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyForUnexpectedState(Ljava/lang/String;)V

    goto :goto_1

    .line 604
    :cond_5
    :goto_0
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderAlreadyInitialized()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 609
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleSender;->closeConnection()V

    .line 610
    sget-object p1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_RECONNECT_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    goto :goto_1

    .line 612
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectSilently()V

    :goto_1
    return-void
.end method

.method private newInitializeHelper()Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;
    .locals 1

    .line 540
    new-instance v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$1;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V

    return-object v0
.end method

.method private onBleActionInternal(Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 6

    .line 178
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 180
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    .line 181
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    .line 182
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v3, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$NyudYtUd5Oa_34DHh-zz4pFGnv0;

    invoke-direct {v3, p0, v0, v1, p1}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$NyudYtUd5Oa_34DHh-zz4pFGnv0;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V

    invoke-interface {v2, v3}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 185
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$2;->$SwitchMap$com$squareup$dipper$events$BleConnectionState:[I

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {v1}, Lcom/squareup/dipper/events/BleConnectionState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    .line 434
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyForUnexpectedState(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 412
    :pswitch_0
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$PairingTimeout;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 413
    :cond_1
    :goto_0
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 414
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 415
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectWhenAvailable()V

    goto/16 :goto_2

    .line 403
    :pswitch_1
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    if-eqz v0, :cond_2

    .line 404
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_BAD_READER_STATE:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 405
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 406
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectWhenAvailable()V

    goto/16 :goto_2

    .line 408
    :cond_2
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 396
    :pswitch_2
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$PairingTimeout;

    if-eqz v0, :cond_3

    .line 397
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->connectSilently()V

    goto/16 :goto_2

    .line 399
    :cond_3
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 382
    :pswitch_3
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    if-eqz v0, :cond_5

    .line 383
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FROM_DISCONNECT_WHILE_CONNECTED:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 384
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 385
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;

    .line 386
    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleAction$DisconnectedAction;->isReaderForceUnPair()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 387
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->fireReaderForceUnpaired()V

    goto/16 :goto_2

    .line 389
    :cond_4
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectWhenAvailable()V

    goto/16 :goto_2

    .line 391
    :cond_5
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$PairingTimeout;

    if-nez v0, :cond_24

    .line 392
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 372
    :pswitch_4
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$NotificationEnabled;

    if-eqz v0, :cond_6

    .line 373
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 374
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->READY:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 375
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->initializeCardReader()V

    .line 376
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingSuccess()V

    goto/16 :goto_2

    .line 378
    :cond_6
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 364
    :pswitch_5
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$NotificationEnabled;

    if-eqz v0, :cond_7

    .line 365
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 366
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_DATA:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->enableNotifications(Ljava/util/UUID;Ljava/util/UUID;)V

    goto/16 :goto_2

    .line 368
    :cond_7
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 355
    :pswitch_6
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ReceivedReaderCommsVersion;

    if-eqz v0, :cond_8

    .line 356
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 357
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ble/BleAction$ReceivedReaderCommsVersion;

    iget-object v1, v1, Lcom/squareup/cardreader/ble/BleAction$ReceivedReaderCommsVersion;->value:[B

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->setCommsVersion([B)V

    .line 358
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_READ_MTU:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->enableNotifications(Ljava/util/UUID;Ljava/util/UUID;)V

    goto/16 :goto_2

    .line 360
    :cond_8
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 347
    :pswitch_7
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ReceivedConnectionInterval;

    if-eqz v0, :cond_9

    .line 348
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_COMMS_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 349
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBackend:Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ble/BleAction$ReceivedConnectionInterval;

    iget v1, v1, Lcom/squareup/cardreader/ble/BleAction$ReceivedConnectionInterval;->value:I

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->onConnectionIntervalReceived(I)V

    goto/16 :goto_2

    .line 351
    :cond_9
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 338
    :pswitch_8
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleBondingBroadcastReceiver:Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;->removeBondSuccessListener(Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver$BleBondSuccessListener;)V

    .line 339
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$BondedWithReader;

    if-eqz v0, :cond_a

    .line 340
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_INTERVAL:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 341
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setUpMtuAndConnectionInterval()V

    goto/16 :goto_2

    .line 343
    :cond_a
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 301
    :pswitch_9
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;

    if-eqz v0, :cond_10

    .line 302
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;->bondStatus:Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;

    .line 303
    sget-object v4, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$2;->$SwitchMap$com$squareup$cardreader$ble$R12Gatt$BondStatus:[I

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/R12Gatt$BondStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    if-eq v4, v2, :cond_f

    const/4 v2, 0x2

    if-eq v4, v2, :cond_d

    if-eq v4, v1, :cond_c

    const/4 v1, 0x4

    if-eq v4, v1, :cond_b

    .line 326
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown bond state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyForUnexpectedState(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 323
    :cond_b
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectSilently()V

    goto/16 :goto_2

    .line 319
    :cond_c
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->createBond()V

    goto/16 :goto_2

    .line 308
    :cond_d
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_e

    new-array v0, v3, [Ljava/lang/Object;

    const-string v1, "R12 thinks we\'re bonded, and so do we, continuing the process!"

    .line 309
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_INTERVAL:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 311
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setUpMtuAndConnectionInterval()V

    goto/16 :goto_2

    :cond_e
    new-array v0, v3, [Ljava/lang/Object;

    const-string v1, "R12 thinks we\'re bonded, but we\'re not, starting the process again"

    .line 313
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->createBond()V

    goto/16 :goto_2

    .line 305
    :cond_f
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->createBond()V

    goto/16 :goto_2

    .line 329
    :cond_10
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$UnableToCreateBond;

    if-eqz v0, :cond_11

    .line 330
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 331
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 332
    sget-object v0, Lcom/squareup/dipper/events/BleErrorType;->UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleErrorType;

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    goto/16 :goto_2

    .line 334
    :cond_11
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 283
    :pswitch_a
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ReceivedSerialNumber;

    if-eqz v0, :cond_13

    .line 284
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$ReceivedSerialNumber;

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleAction$ReceivedSerialNumber;->serialNumber:Ljava/lang/String;

    if-eqz v0, :cond_12

    .line 286
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v1

    .line 287
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logSerialNumberReceived(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)V

    goto :goto_1

    :cond_12
    new-array v0, v3, [Ljava/lang/Object;

    const-string v1, "Null serial number from BLE device information service"

    .line 289
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    :goto_1
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 295
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bondStatusRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2

    .line 297
    :cond_13
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 255
    :pswitch_b
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ServiceCharacteristicVersion;

    if-eqz v0, :cond_15

    .line 256
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$ServiceCharacteristicVersion;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleAction$ServiceCharacteristicVersion;->isCompatible()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 257
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_DEVICE_INFORMATION_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERIAL_NUMBER:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V

    goto/16 :goto_2

    .line 261
    :cond_14
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_INCOMPATIBLE_SERVICE_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 262
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 263
    sget-object v0, Lcom/squareup/dipper/events/BleErrorType;->SERVICE_VERSION_INCOMPATIBLE:Lcom/squareup/dipper/events/BleErrorType;

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    goto/16 :goto_2

    .line 265
    :cond_15
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ReceivedBondState;

    if-eqz v0, :cond_16

    new-array v0, v3, [Ljava/lang/Object;

    const-string v1, "Received BondState when we didn\'t expect it, is this a Samsung device?"

    .line 268
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 269
    :cond_16
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    if-eqz v0, :cond_18

    .line 270
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->previousAction:Lcom/squareup/cardreader/ble/BleAction;

    instance-of v0, v0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    if-eqz v0, :cond_17

    .line 272
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    iget-boolean v1, v0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->success:Z

    if-eqz v1, :cond_24

    iget-boolean v0, v0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->success:Z

    goto/16 :goto_2

    .line 276
    :cond_17
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 279
    :cond_18
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 232
    :pswitch_c
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    if-eqz v0, :cond_1a

    .line 233
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;

    iget-boolean v0, v0, Lcom/squareup/cardreader/ble/BleAction$ServicesDiscovered;->success:Z

    if-nez v0, :cond_19

    .line 234
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectSilently()V

    goto/16 :goto_2

    .line 236
    :cond_19
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_SERVICE_VERSION:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->readCharacteristic(Ljava/util/UUID;Ljava/util/UUID;)V

    goto/16 :goto_2

    .line 239
    :cond_1a
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$OldServicesCached;

    if-eqz v0, :cond_1b

    .line 240
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_OLD_SERVICES_CACHED:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 241
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 242
    sget-object v0, Lcom/squareup/dipper/events/BleErrorType;->OLD_SERVICE_CACHED:Lcom/squareup/dipper/events/BleErrorType;

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    goto/16 :goto_2

    .line 243
    :cond_1b
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;

    if-eqz v0, :cond_1d

    .line 244
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->previousAction:Lcom/squareup/cardreader/ble/BleAction;

    instance-of v0, v0, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;

    if-eqz v0, :cond_1c

    goto/16 :goto_2

    .line 248
    :cond_1c
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 251
    :cond_1d
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto/16 :goto_2

    .line 219
    :pswitch_d
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;

    if-eqz v0, :cond_1f

    .line 220
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;

    iget v0, v0, Lcom/squareup/cardreader/ble/BleAction$ConnectedAction;->status:I

    if-eqz v0, :cond_1e

    .line 221
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectSilently()V

    goto/16 :goto_2

    .line 224
    :cond_1e
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleSender;->discoverServices()V

    goto :goto_2

    .line 228
    :cond_1f
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    goto :goto_2

    .line 187
    :pswitch_e
    instance-of v0, p1, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;

    if-eqz v0, :cond_23

    .line 188
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 190
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v3, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$wFn1QlgtsFfpnpFQLecIdW9uWwI;

    invoke-direct {v3, p0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$wFn1QlgtsFfpnpFQLecIdW9uWwI;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V

    invoke-interface {v0, v3}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 193
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    sget-object v3, Lcom/squareup/cardreader/ble/BleConnectType;->RECONNECT_SILENTLY:Lcom/squareup/cardreader/ble/BleConnectType;

    if-ne v0, v3, :cond_20

    .line 194
    iget v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    .line 197
    :cond_20
    iget v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    if-ge v0, v1, :cond_22

    .line 198
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 200
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 201
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/BluetoothUtils;->isConnectedBle(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 202
    new-instance v0, Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;

    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->getBleDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;-><init>(Lcom/squareup/dipper/events/BleDevice;)V

    .line 203
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$v7uD6P8G-rzMrc6b0FD3VJuXki0;

    invoke-direct {v2, p0, v0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$v7uD6P8G-rzMrc6b0FD3VJuXki0;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 208
    :cond_21
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->newInitializeHelper()Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleSender;->initialize(Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V

    goto :goto_2

    .line 210
    :cond_22
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->DESTROYING_FOR_TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 211
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroy()V

    .line 212
    sget-object v0, Lcom/squareup/dipper/events/BleErrorType;->TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleErrorType;

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    goto :goto_2

    .line 215
    :cond_23
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->handleUnexpectedAction(Lcom/squareup/cardreader/ble/BleAction;Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 437
    :cond_24
    :goto_2
    :pswitch_f
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->previousAction:Lcom/squareup/cardreader/ble/BleAction;

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method private reconnectWhenAvailable()V
    .locals 2

    .line 557
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$S9qDcciDQuNdXoLdlf_dEzJp3h4;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$S9qDcciDQuNdXoLdlf_dEzJp3h4;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private setUpMtuAndConnectionInterval()V
    .locals 4

    .line 524
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    sget-object v1, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_INTERVAL:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq v0, v1, :cond_0

    const-string v0, "Must be in WAITING_FOR_CONNECTION_INTERVAL"

    .line 525
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->destroyForUnexpectedState(Ljava/lang/String;)V

    return-void

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    sget-object v3, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->EXCHANGE_MTU:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 530
    invoke-virtual {v3}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->value()[B

    move-result-object v3

    .line 529
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/cardreader/ble/BleSender;->writeData(Ljava/util/UUID;Ljava/util/UUID;[B)V

    .line 531
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    sget-object v3, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->UPDATE_CONN_PARAMS:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 532
    invoke-virtual {v3}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->value()[B

    move-result-object v3

    .line 531
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/cardreader/ble/BleSender;->writeData(Ljava/util/UUID;Ljava/util/UUID;[B)V

    .line 533
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    sget-object v1, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_LCR_SERVICE:Ljava/util/UUID;

    sget-object v2, Lcom/squareup/cardreader/ble/R12Gatt;->UUID_CHARACTERISTIC_CONNECTION_CONTROL:Ljava/util/UUID;

    sget-object v3, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->RESET_TRANSPORT:Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;

    .line 534
    invoke-virtual {v3}, Lcom/squareup/cardreader/ble/R12Gatt$ConnectionControlCommands;->value()[B

    move-result-object v3

    .line 533
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/cardreader/ble/BleSender;->writeData(Ljava/util/UUID;Ljava/util/UUID;[B)V

    .line 536
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->commsRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method getBondStatusRunner()Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;
    .locals 1

    .line 743
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bondStatusRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$BondStatusRunner;

    return-object v0
.end method

.method getCommsRunner()Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;
    .locals 1

    .line 747
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->commsRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$CommsRunner;

    return-object v0
.end method

.method getState()Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    .line 735
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    return-object v0
.end method

.method getTimeoutRunner()Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;
    .locals 1

    .line 739
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 161
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 166
    :cond_0
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/cardreader/ble/BleAction;

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleActionInternal(Lcom/squareup/cardreader/ble/BleAction;)V

    const/4 p1, 0x1

    return p1
.end method

.method public synthetic lambda$destroy$5$BleConnectionStateMachine()V
    .locals 2

    .line 588
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public synthetic lambda$firePairingFailure$7$BleConnectionStateMachine(Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;I)V
    .locals 8

    .line 662
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    if-ne p1, v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getBlePairingListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/BlePairingListener;

    .line 664
    invoke-interface {v1, p2, p3}, Lcom/squareup/cardreader/BlePairingListener;->onPairingFailed(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V

    goto :goto_0

    .line 667
    :cond_0
    new-instance p2, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    .line 668
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->getBleDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v3

    const/4 v0, 0x1

    add-int/lit8 v5, p4, 0x1

    sget-object p4, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->TERMINAL:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    if-ne p1, p4, :cond_1

    const/4 v6, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    const/4 v6, 0x0

    :goto_1
    const/4 v7, 0x0

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v2 .. v7}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;-><init>(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)V

    .line 670
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    return-void
.end method

.method public synthetic lambda$firePairingSuccess$6$BleConnectionStateMachine(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 637
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getBlePairingListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/BlePairingListener;

    .line 638
    invoke-interface {v1, p1}, Lcom/squareup/cardreader/BlePairingListener;->onPairingSuccess(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_0

    .line 640
    :cond_0
    new-instance p1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;

    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->getBleDevice()Lcom/squareup/dipper/events/BleDevice;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;-><init>(Lcom/squareup/dipper/events/BleDevice;)V

    .line 641
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    return-void
.end method

.method public synthetic lambda$fireReaderForceUnpaired$8$BleConnectionStateMachine(Lcom/squareup/cardreader/WirelessConnection;)V
    .locals 2

    .line 683
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleReaderForceUnpaired(Lcom/squareup/cardreader/WirelessConnection;)V

    .line 684
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getBlePairingListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/BlePairingListener;

    .line 685
    invoke-interface {v1, p1}, Lcom/squareup/cardreader/BlePairingListener;->onReaderForceUnPair(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$onBleActionInternal$0$BleConnectionStateMachine(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 183
    invoke-interface {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleConnectionAction(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleAction;)V

    return-void
.end method

.method public synthetic lambda$onBleActionInternal$1$BleConnectionStateMachine()V
    .locals 3

    .line 190
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    .line 191
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-interface {v0, v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleConnectionEnqueued(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)V

    return-void
.end method

.method public synthetic lambda$onBleActionInternal$2$BleConnectionStateMachine(Lcom/squareup/dipper/events/DipperEvent$BleAlreadyConnected;)V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    return-void
.end method

.method public synthetic lambda$reconnectWhenAvailable$4$BleConnectionStateMachine()V
    .locals 2

    .line 558
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->forBleAutoConnect(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "BLE is not enabled. Not setting up autoConnect."

    .line 561
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$setState$3$BleConnectionStateMachine(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V
    .locals 7

    .line 511
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    .line 512
    invoke-interface/range {v1 .. v6}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleConnectionStateChange(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V

    return-void
.end method

.method public onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 173
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 174
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onBonded(Landroid/bluetooth/BluetoothDevice;)V
    .locals 1

    .line 468
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bluetoothDevice:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 469
    new-instance p1, Lcom/squareup/cardreader/ble/BleAction$BondedWithReader;

    invoke-direct {p1}, Lcom/squareup/cardreader/ble/BleAction$BondedWithReader;-><init>()V

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    :cond_0
    return-void
.end method

.method reconnectSilently()V
    .locals 2

    .line 475
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->cancelRunners()V

    .line 476
    sget-object v0, Lcom/squareup/dipper/events/BleErrorType;->UNKNOWN_ERROR_TYPE:Lcom/squareup/dipper/events/BleErrorType;

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;->WILL_TRY_TO_RECONNECT:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->firePairingFailure(Lcom/squareup/dipper/events/BleErrorType;Lcom/squareup/cardreader/ble/BleConnectionStateMachine$FailureMode;)V

    .line 477
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;)V

    .line 478
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleSender:Lcom/squareup/cardreader/ble/BleSender;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleSender;->closeConnection()V

    return-void
.end method

.method resetReconnectionAttempts()V
    .locals 1

    const/4 v0, 0x0

    .line 755
    iput v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->reconnectAttempts:I

    return-void
.end method

.method setBleConnectType(Lcom/squareup/cardreader/ble/BleConnectType;)V
    .locals 0

    .line 730
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    return-void
.end method

.method setBleHandler(Landroid/os/Handler;)V
    .locals 0

    .line 759
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    return-void
.end method

.method setState(Lcom/squareup/dipper/events/BleConnectionState;)V
    .locals 1

    const/4 v0, 0x0

    .line 500
    invoke-virtual {p0, p1, v0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->setState(Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V

    return-void
.end method

.method setState(Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V
    .locals 8

    .line 509
    invoke-direct {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnection()Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    .line 510
    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    .line 511
    iget-object v6, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v7, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$zlVIRhL0X8jvpirWBUpie3jRNfU;

    move-object v0, v7

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/ble/-$$Lambda$BleConnectionStateMachine$zlVIRhL0X8jvpirWBUpie3jRNfU;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V

    invoke-interface {v6, v7}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    .line 515
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    .line 517
    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-static {p1, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateHelper;->shouldRestartTimer(Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleConnectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 518
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    invoke-virtual {p1, p2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 519
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->bleHandler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->timeoutRunner:Lcom/squareup/cardreader/ble/BleConnectionStateMachine$TimeoutRunner;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleConnectionStateHelper;->getPairingTimeout(Lcom/squareup/dipper/events/BleConnectionState;)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method setStateForTests(Lcom/squareup/dipper/events/BleConnectionState;)V
    .locals 0

    .line 751
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->state:Lcom/squareup/dipper/events/BleConnectionState;

    return-void
.end method
