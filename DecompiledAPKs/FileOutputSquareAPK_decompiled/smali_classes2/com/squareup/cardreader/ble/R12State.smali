.class public final enum Lcom/squareup/cardreader/ble/R12State;
.super Ljava/lang/Enum;
.source "ConnectionState.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ble/R12State;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0011\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/R12State;",
        "",
        "(Ljava/lang/String;I)V",
        "CREATED",
        "WAITING_FOR_CONNECTION_TO_READER",
        "WAITING_FOR_SERVICE_DISCOVERY",
        "WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION",
        "WAITING_FOR_SERIAL_NUMBER",
        "WAITING_FOR_BOND_STATUS_FROM_READER",
        "WAITING_FOR_REMOVE_BOND",
        "WAITING_FOR_BOND",
        "WAITING_FOR_CONNECTION_CONTROL",
        "WAITING_FOR_COMMS_VERSION",
        "WAITING_FOR_MTU_NOTIFICATIONS",
        "WAITING_FOR_DATA_NOTIFICATIONS",
        "READY",
        "WAITING_FOR_DISCONNECTION",
        "DISCONNECTED",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ble/R12State;

.field public static final enum CREATED:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum DISCONNECTED:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum READY:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_BOND:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_COMMS_VERSION:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_DISCONNECTION:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_REMOVE_BOND:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/R12State;

.field public static final enum WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/cardreader/ble/R12State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/cardreader/ble/R12State;

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x0

    const-string v3, "CREATED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->CREATED:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x1

    const-string v3, "WAITING_FOR_CONNECTION_TO_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x2

    const-string v3, "WAITING_FOR_SERVICE_DISCOVERY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERVICE_DISCOVERY:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x3

    const-string v3, "WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERVICE_CHARACTERISTIC_VERSION:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x4

    const-string v3, "WAITING_FOR_SERIAL_NUMBER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_SERIAL_NUMBER:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x5

    const-string v3, "WAITING_FOR_BOND_STATUS_FROM_READER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_BOND_STATUS_FROM_READER:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x6

    const-string v3, "WAITING_FOR_REMOVE_BOND"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_REMOVE_BOND:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/4 v2, 0x7

    const-string v3, "WAITING_FOR_BOND"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_BOND:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0x8

    const-string v3, "WAITING_FOR_CONNECTION_CONTROL"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_CONNECTION_CONTROL:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0x9

    const-string v3, "WAITING_FOR_COMMS_VERSION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_COMMS_VERSION:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0xa

    const-string v3, "WAITING_FOR_MTU_NOTIFICATIONS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_MTU_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0xb

    const-string v3, "WAITING_FOR_DATA_NOTIFICATIONS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_DATA_NOTIFICATIONS:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0xc

    const-string v3, "READY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->READY:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0xd

    const-string v3, "WAITING_FOR_DISCONNECTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->WAITING_FOR_DISCONNECTION:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ble/R12State;

    const/16 v2, 0xe

    const-string v3, "DISCONNECTED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ble/R12State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ble/R12State;->DISCONNECTED:Lcom/squareup/cardreader/ble/R12State;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ble/R12State;->$VALUES:[Lcom/squareup/cardreader/ble/R12State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ble/R12State;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/ble/R12State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/R12State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ble/R12State;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/ble/R12State;->$VALUES:[Lcom/squareup/cardreader/ble/R12State;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ble/R12State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ble/R12State;

    return-object v0
.end method
