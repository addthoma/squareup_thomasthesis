.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_ProvideBleThreadFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Thread;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;->bleExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/os/Handler;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBleThread(Landroid/os/Handler;)Ljava/lang/Thread;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule;->provideBleThread(Landroid/os/Handler;)Ljava/lang/Thread;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Thread;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;->get()Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/Thread;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;->bleExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/GlobalBleModule_ProvideBleThreadFactory;->provideBleThread(Landroid/os/Handler;)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method
