.class public final Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;
.super Ljava/lang/Object;
.source "BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleCardReaderGraphInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;->bleCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderGraphInitializer(Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent$Module;->provideCardReaderGraphInitializer(Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;->bleCardReaderGraphInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;->provideCardReaderGraphInitializer(Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleCardReaderContextComponent_Module_ProvideCardReaderGraphInitializerFactory;->get()Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;

    move-result-object v0

    return-object v0
.end method
