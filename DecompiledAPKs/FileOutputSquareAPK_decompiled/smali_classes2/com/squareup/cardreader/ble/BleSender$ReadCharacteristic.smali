.class Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;
.super Ljava/lang/Object;
.source "BleSender.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleSender$GattAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReadCharacteristic"
.end annotation


# instance fields
.field final characteristicUuid:Ljava/util/UUID;

.field final serviceUuid:Ljava/util/UUID;

.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleSender;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->serviceUuid:Ljava/util/UUID;

    .line 239
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->characteristicUuid:Ljava/util/UUID;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;Lcom/squareup/cardreader/ble/BleSender$1;)V
    .locals 0

    .line 233
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;-><init>(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V

    return-void
.end method


# virtual methods
.method public perform()V
    .locals 5

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 243
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->characteristicUuid:Ljava/util/UUID;

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Starting characteristic read on %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->serviceUuid:Ljava/util/UUID;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->characteristicUuid:Ljava/util/UUID;

    .line 245
    invoke-static {v1, v2, v4}, Lcom/squareup/cardreader/ble/BleSender;->access$800(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)Landroid/bluetooth/BluetoothGattCharacteristic;

    move-result-object v1

    if-nez v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->serviceUuid:Ljava/util/UUID;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->characteristicUuid:Ljava/util/UUID;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleSender;->access$900(Lcom/squareup/cardreader/ble/BleSender;Ljava/util/UUID;Ljava/util/UUID;)V

    return-void

    .line 252
    :cond_0
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$600(Lcom/squareup/cardreader/ble/BleSender;)Landroid/bluetooth/BluetoothGatt;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/bluetooth/BluetoothGatt;->readCharacteristic(Landroid/bluetooth/BluetoothGattCharacteristic;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-array v0, v0, [Ljava/lang/Object;

    .line 253
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "reading characteristic: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 255
    :cond_1
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$700(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v2

    sget-object v4, Lcom/squareup/cardreader/ble/GattConnectionEventName;->CHARACTERISTIC_READ_INIT_FAILED:Lcom/squareup/cardreader/ble/GattConnectionEventName;

    .line 256
    invoke-static {v4, v1}, Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;->of(Lcom/squareup/cardreader/ble/GattConnectionEventName;Landroid/bluetooth/BluetoothGattCharacteristic;)Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/squareup/cardreader/ReaderEventLogger;->logGattConnectionEvent(Lcom/squareup/cardreader/ReaderEventLogger$GattConnectionEvent;)V

    .line 257
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleSender$ReadCharacteristic;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleSender;->access$1000(Lcom/squareup/cardreader/ble/BleSender;)Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    move-result-object v2

    new-instance v4, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;

    new-array v0, v0, [Ljava/lang/Object;

    .line 259
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "Failed to initiate read on characteristic %s"

    .line 258
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/squareup/cardreader/ble/BleAction$ReconnectDueToGattError;-><init>(Ljava/lang/String;)V

    .line 257
    invoke-interface {v2, v4}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    :goto_0
    return-void
.end method
