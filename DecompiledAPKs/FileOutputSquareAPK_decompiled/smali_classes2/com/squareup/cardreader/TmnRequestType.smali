.class public final enum Lcom/squareup/cardreader/TmnRequestType;
.super Ljava/lang/Enum;
.source "TmnRequestType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/TmnRequestType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnRequestType;",
        "",
        "(Ljava/lang/String;I)V",
        "TMN_REQUEST_TRANSACTION",
        "TMN_REQUEST_CHECK_BALANCE",
        "TMN_REQUEST_CANCELLATION",
        "TMN_REQUEST_CHECK_HISTORY",
        "TMN_REQUEST_ONLINE_TEST",
        "TMN_REQUEST_CHECK_RESULT",
        "TMN_REQUEST_VOID_UNKNOWN",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/TmnRequestType;

.field public static final enum TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/TmnRequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/cardreader/TmnRequestType;

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x0

    const-string v3, "TMN_REQUEST_TRANSACTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_TRANSACTION:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x1

    const-string v3, "TMN_REQUEST_CHECK_BALANCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CHECK_BALANCE:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x2

    const-string v3, "TMN_REQUEST_CANCELLATION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CANCELLATION:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x3

    const-string v3, "TMN_REQUEST_CHECK_HISTORY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CHECK_HISTORY:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x4

    const-string v3, "TMN_REQUEST_ONLINE_TEST"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_ONLINE_TEST:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x5

    const-string v3, "TMN_REQUEST_CHECK_RESULT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_CHECK_RESULT:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnRequestType;

    const/4 v2, 0x6

    const-string v3, "TMN_REQUEST_VOID_UNKNOWN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnRequestType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnRequestType;->TMN_REQUEST_VOID_UNKNOWN:Lcom/squareup/cardreader/TmnRequestType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/TmnRequestType;->$VALUES:[Lcom/squareup/cardreader/TmnRequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/TmnRequestType;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/TmnRequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TmnRequestType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/TmnRequestType;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/TmnRequestType;->$VALUES:[Lcom/squareup/cardreader/TmnRequestType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/TmnRequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/TmnRequestType;

    return-object v0
.end method
