.class public final Lcom/squareup/cardreader/SecureTouchFeatureV2;
.super Ljava/lang/Object;
.source "SecureTouchFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/SecureTouchFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0010H\u0016J\u000e\u0010\u0012\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0014J\u0008\u0010\u0015\u001a\u00020\u0010H\u0016J\u0008\u0010\u0016\u001a\u00020\u0010H\u0002J \u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0019H\u0016J\u0010\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u001d\u001a\u00020\u0019H\u0016J\u001b\u0010\u001e\u001a\u00020\u00102\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 H\u0002\u00a2\u0006\u0002\u0010\"J\u0010\u0010#\u001a\u00020\u00102\u0006\u0010$\u001a\u00020\u0019H\u0016J\u0008\u0010%\u001a\u00020\u0010H\u0016J\u0018\u0010&\u001a\u00020\u00102\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u0019H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/cardreader/SecureTouchFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/SecureTouchFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureTouchFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V",
        "disableSquidTouchDriver",
        "",
        "enableSquidTouchDriver",
        "handleMessage",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureTouchFeatureMessage;",
        "hideSecureTouchPinPad",
        "initialize",
        "onSecureTouchAccessibilityPinPadCenter",
        "x",
        "",
        "y",
        "pinPadType",
        "onSecureTouchKeepaliveFailed",
        "keepAliveErrorCode",
        "onSecureTouchKeypadVisible",
        "buttons",
        "",
        "Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;",
        "([Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V",
        "onSecureTouchPinPadEvent",
        "buttonType",
        "resetIfInitilized",
        "showSecureTouchPinPad",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "pinTryValue",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureTouchFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$SecureTouchFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method private final initialize()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->secure_touch_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    move-result-object v0

    const-string v1, "SecureTouchFeatureNative\u2026ardreaderPointer(), this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    return-void
.end method

.method private final onSecureTouchKeypadVisible([Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
    .locals 5

    .line 96
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    const-string v2, "featurePointer"

    if-ge v1, v0, :cond_1

    aget-object v3, p1, v1

    .line 98
    iget-object v4, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    if-nez v4, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 97
    :cond_0
    invoke-static {v4, v3}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_regular_set_button_location(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    if-nez p1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;->CRS_STM_PINPAD_CONFIG_REGULAR:Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;

    .line 102
    invoke-static {p1, v0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_sent_pinpad_configs(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;Lcom/squareup/cardreader/lcr/CrsStmPinPadConfigType;)V

    return-void
.end method


# virtual methods
.method public disableSquidTouchDriver()V
    .locals 0

    return-void
.end method

.method public enableSquidTouchDriver()V
    .locals 0

    return-void
.end method

.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureTouchFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureTouchFeatureMessage$Initialize;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$SecureTouchFeatureMessage$Initialize;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureTouchFeatureV2;->initialize()V

    :cond_0
    return-void
.end method

.method public hideSecureTouchPinPad()V
    .locals 0

    return-void
.end method

.method public onSecureTouchAccessibilityPinPadCenter(III)V
    .locals 0

    return-void
.end method

.method public onSecureTouchKeepaliveFailed(I)V
    .locals 0

    return-void
.end method

.method public onSecureTouchPinPadEvent(I)V
    .locals 0

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 85
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/SecureTouchFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    if-eqz v0, :cond_2

    .line 86
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 87
    iget-object v0, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNative;->cr_secure_touch_mode_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    :cond_2
    return-void
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_secure_touch_mode_feature_t;

    return-void
.end method

.method public showSecureTouchPinPad(Lcom/squareup/cardreader/CardInfo;I)V
    .locals 0

    const-string p2, "cardInfo"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
