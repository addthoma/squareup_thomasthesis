.class public abstract Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CardReaderFeatureOutput"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnInitialized;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnFailed;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderError;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderReady;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReportError;,
        Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnRpcCallbackRecvd;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0003\u0004\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0007\n\u000b\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        "()V",
        "OnCommsVersionAcquired",
        "OnFailed",
        "OnInitialized",
        "OnReaderError",
        "OnReaderReady",
        "OnReportError",
        "OnRpcCallbackRecvd",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnInitialized;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnFailed;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderError;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderReady;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReportError;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnRpcCallbackRecvd;",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 206
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 206
    invoke-direct {p0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;-><init>()V

    return-void
.end method
