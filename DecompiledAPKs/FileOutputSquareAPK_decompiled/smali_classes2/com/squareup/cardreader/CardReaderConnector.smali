.class public Lcom/squareup/cardreader/CardReaderConnector;
.super Ljava/lang/Object;
.source "CardReaderConnector.java"


# instance fields
.field private final cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderContext;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    .line 17
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 18
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderConnector;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method


# virtual methods
.method public getId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    iget-object v0, v0, Lcom/squareup/cardreader/CardReaderContext;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method

.method public synthetic lambda$onCardReaderConnected$0$CardReaderConnector()V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->hasCardReaderContext(Lcom/squareup/cardreader/CardReaderContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderConnector;->cardReaderContext:Lcom/squareup/cardreader/CardReaderContext;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->addCardReader(Lcom/squareup/cardreader/CardReaderContext;)V

    :cond_0
    return-void
.end method

.method public onCardReaderConnected()V
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderConnector;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$CardReaderConnector$5heIEj9O7ioIaI8s3xAk-gtvdiE;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/-$$Lambda$CardReaderConnector$5heIEj9O7ioIaI8s3xAk-gtvdiE;-><init>(Lcom/squareup/cardreader/CardReaderConnector;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
