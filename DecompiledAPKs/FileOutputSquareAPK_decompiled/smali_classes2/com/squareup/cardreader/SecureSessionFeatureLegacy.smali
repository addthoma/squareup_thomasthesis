.class Lcom/squareup/cardreader/SecureSessionFeatureLegacy;
.super Ljava/lang/Object;
.source "SecureSessionFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/SecureSessionFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;,
        Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;
    }
.end annotation


# static fields
.field private static final SQUID_SECURE_SESSION:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;


# instance fields
.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

.field private final isReaderSdkApp:Z

.field private listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

.field private final minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

.field private secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

.field private final secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 260
    new-instance v7, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;

    const-wide/16 v1, 0x0

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;-><init>(JJJ)V

    sput-object v7, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->SQUID_SECURE_SESSION:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;

    return-void
.end method

.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ms/MinesweeperTicket;Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;Lcom/squareup/cardreader/SecureSessionRevocationState;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/ms/MinesweeperTicket;",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            "Z)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 43
    iput-object p3, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

    .line 44
    iput-object p4, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    .line 45
    iput-object p5, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    .line 46
    iput-boolean p6, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->isReaderSdkApp:Z

    return-void
.end method

.method private initSecureSessionOrRetry(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V
    .locals 3

    if-eqz p1, :cond_4

    .line 60
    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    .line 62
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "Faking secure session success for Squid"

    .line 65
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    sget-object v0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->SQUID_SECURE_SESSION:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionEvent(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V

    return-void

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    invoke-virtual {p1}, Lcom/squareup/cardreader/SecureSessionRevocationState;->isRevoked()Z

    move-result p1

    if-eqz p1, :cond_1

    new-array p1, v0, [Ljava/lang/Object;

    const-string v0, "Avoiding unsafe secure session initialization after a revocation"

    .line 72
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    .line 74
    invoke-virtual {v0}, Lcom/squareup/cardreader/SecureSessionRevocationState;->getReasonTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    .line 75
    invoke-virtual {v1}, Lcom/squareup/cardreader/SecureSessionRevocationState;->getReasonDescription()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->globalRevocationState:Lcom/squareup/cardreader/SecureSessionRevocationState;

    .line 76
    invoke-virtual {v2}, Lcom/squareup/cardreader/SecureSessionRevocationState;->getUxHint()Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object v2

    .line 73
    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    return-void

    .line 80
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-eqz p1, :cond_2

    .line 83
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->cr_securesession_feature_establish_session(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    goto :goto_0

    .line 85
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderProvider:Ljavax/inject/Provider;

    .line 86
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->minesweeperTicket:Lcom/squareup/ms/MinesweeperTicket;

    invoke-interface {p1, v0, p0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->securesession_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    .line 88
    iget-boolean p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->isReaderSdkApp:Z

    if-eqz p1, :cond_3

    .line 89
    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->readThingFromDisk()V

    :cond_3
    :goto_0
    return-void

    .line 59
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "listener must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private readThingFromDisk()V
    .locals 5

    .line 100
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-eqz v0, :cond_1

    .line 102
    :try_start_0
    sget-object v0, Lcom/squareup/sdk/reader/Client;->WBID:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 103
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 104
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 105
    new-array v0, v0, [B

    .line 107
    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v2

    .line 108
    :goto_0
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 109
    iget-object v3, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-interface {v3, v2, v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->set_kb(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not read wbid"

    .line 112
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    return-void

    .line 115
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We should only ever attempt to set the Thing if secure session has already been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    return-object v0
.end method


# virtual methods
.method public initializeSecureSession(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->initSecureSessionOrRetry(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V

    return-void

    .line 52
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "SecureSession already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public notifySecureSessionServerError()V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-nez v0, :cond_0

    return-void

    .line 165
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->cr_securesession_feature_notify_server_error(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-void
.end method

.method public onPinBypass()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->cr_securesession_feature_pin_bypass(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-void

    .line 194
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Securesession PIN bypass not supported on X2 or T2"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onPinDigitEntered(I)V
    .locals 2

    .line 177
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->pin_add_digit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;I)Z

    return-void

    .line 178
    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Securesession PIN digit entry not supported on X2 or T2"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPinPadReset()V
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->pin_reset(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V

    return-void

    .line 170
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Securesession PIN pad reset not supported on X2 or T2"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onPinRequested(ZLcom/squareup/cardreader/CardInfo;Z)V
    .locals 2

    .line 228
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    new-instance v1, Lcom/squareup/cardreader/PinRequestData;

    invoke-direct {v1, p2, p1, p3}, Lcom/squareup/cardreader/PinRequestData;-><init>(Lcom/squareup/cardreader/CardInfo;ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onPinRequested(Lcom/squareup/cardreader/PinRequestData;)V

    return-void
.end method

.method public onSecureSessionInvalid()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 220
    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_INVALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "onSecureSessionInvalid sending type: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_INVALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionEvent(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V

    return-void
.end method

.method public onSecureSessionSendToServer(I[B)V
    .locals 2

    .line 201
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "onSecureSessionSendToServer sending type: %s"

    .line 203
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionSendToServer(Lcom/squareup/cardreader/lcr/CrSecureSessionMessageType;[B)V

    return-void
.end method

.method public onSecureSessionValid(JJJ)V
    .locals 8

    .line 210
    new-instance v7, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;

    move-object v0, v7

    move-wide v1, p1

    move-wide v3, p3

    move-wide v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;-><init>(JJJ)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 213
    sget-object p2, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    const/4 p3, 0x0

    aput-object p2, p1, p3

    const-string p2, "onSecureSessionValid sending type: %s"

    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 215
    iget-object p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    sget-object p2, Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;->CR_SECURESESSION_FEATURE_EVENT_TYPE_SESSION_VALID:Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;

    invoke-interface {p1, v7, p2}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionEvent(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;Lcom/squareup/cardreader/lcr/CrSecureSessionEventType;)V

    return-void
.end method

.method public processServerMessage([B)V
    .locals 4

    .line 137
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    .line 143
    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->securesession_recv_server_message(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getResult()Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getLocalizedTitle()Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getLocalizedDescription()Ljava/lang/String;

    move-result-object v2

    .line 147
    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;->getUxHint()Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;

    move-result-object p1

    .line 149
    sget-object v3, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SERVER_DENY_ERROR:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    if-ne v0, v3, :cond_0

    .line 151
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    goto :goto_0

    .line 152
    :cond_0
    sget-object p1, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->CR_SECURESESSION_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    if-eq v0, p1, :cond_2

    .line 154
    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigValue()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrSecureSessionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "SecureSession error: %s"

    .line 155
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;->onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    goto :goto_0

    .line 139
    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Online securesession not supported on X2 or T2"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    return-void
.end method

.method public reinitialize(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V
    .locals 0

    .line 133
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->initSecureSessionOrRetry(Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;)V

    return-void
.end method

.method public resetSecureSession()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->cr_securesession_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    .line 125
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->cr_securesession_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    :cond_0
    const/4 v0, 0x0

    .line 127
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->listener:Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionListener;

    .line 128
    iput-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    :cond_1
    return-void
.end method

.method public submitPinBlock()V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isSquareDeviceCardReader()Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSessionFeatureNative:Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;

    invoke-direct {p0}, Lcom/squareup/cardreader/SecureSessionFeatureLegacy;->secureSession()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;->pin_submit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;

    return-void

    .line 186
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Securesession PIN block submission not supported on X2 or T2"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
