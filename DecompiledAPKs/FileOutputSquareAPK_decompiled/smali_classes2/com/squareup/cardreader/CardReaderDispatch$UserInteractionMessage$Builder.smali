.class public Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private lines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 440
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->lines:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addLine(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;
    .locals 1

    .line 452
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->lines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;
    .locals 4

    .line 443
    new-instance v0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->lines:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/cardreader/CardReaderDispatch$1;)V

    return-object v0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;
    .locals 0

    .line 447
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
