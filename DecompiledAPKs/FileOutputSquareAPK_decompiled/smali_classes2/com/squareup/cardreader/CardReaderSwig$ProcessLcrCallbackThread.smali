.class final enum Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;
.super Ljava/lang/Enum;
.source "CardReaderSwig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderSwig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ProcessLcrCallbackThread"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

.field public static final enum MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

.field public static final enum MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

.field public static final enum SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 887
    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    const/4 v1, 0x0

    const-string v2, "MainThreadIfNoPendingCancel"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    const/4 v2, 0x1

    const-string v3, "MainThread"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    new-instance v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    const/4 v3, 0x2

    const-string v4, "SameThread"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    .line 886
    sget-object v4, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThreadIfNoPendingCancel:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->MainThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->SameThread:Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->$VALUES:[Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 886
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;
    .locals 1

    .line 886
    const-class v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;
    .locals 1

    .line 886
    sget-object v0, Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->$VALUES:[Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/CardReaderSwig$ProcessLcrCallbackThread;

    return-object v0
.end method
