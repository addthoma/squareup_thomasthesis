.class Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;
.super Ljava/lang/Object;
.source "SecureSessionFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureSessionFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SecureSessionData"
.end annotation


# instance fields
.field public final readerTransactionCount:J

.field public final readerUtcEpochTime:J

.field public final sessionId:J


# direct methods
.method constructor <init>(JJJ)V
    .locals 0

    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    iput-wide p1, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;->sessionId:J

    .line 255
    iput-wide p3, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;->readerTransactionCount:J

    .line 256
    iput-wide p5, p0, Lcom/squareup/cardreader/SecureSessionFeatureLegacy$SecureSessionData;->readerUtcEpochTime:J

    return-void
.end method
