.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1596
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;
    .locals 3

    .line 1606
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1593
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication;

    move-result-object v0

    return-object v0
.end method

.method public emv_application(Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;
    .locals 0

    .line 1600
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$SelectApplication$Builder;->emv_application:Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    return-object p0
.end method
