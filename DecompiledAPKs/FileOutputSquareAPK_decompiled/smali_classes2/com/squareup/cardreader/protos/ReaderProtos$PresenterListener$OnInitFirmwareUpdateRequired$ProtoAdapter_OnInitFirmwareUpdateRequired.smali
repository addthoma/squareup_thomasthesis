.class final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$ProtoAdapter_OnInitFirmwareUpdateRequired;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnInitFirmwareUpdateRequired"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4016
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4032
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;-><init>()V

    .line 4033
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4034
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 4037
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4041
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4042
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4014
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$ProtoAdapter_OnInitFirmwareUpdateRequired;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4027
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4014
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$ProtoAdapter_OnInitFirmwareUpdateRequired;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)I
    .locals 0

    .line 4021
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4014
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$ProtoAdapter_OnInitFirmwareUpdateRequired;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;
    .locals 0

    .line 4047
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;

    move-result-object p1

    .line 4048
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4049
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4014
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired$ProtoAdapter_OnInitFirmwareUpdateRequired;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    move-result-object p1

    return-object p1
.end method
