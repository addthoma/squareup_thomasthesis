.class public final Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$Asset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public block_index_table:Lokio/ByteString;

.field public encrypted_data:Lokio/ByteString;

.field public header:Lokio/ByteString;

.field public is_blocking:Ljava/lang/Boolean;

.field public requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2292
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public block_index_table(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    .locals 0

    .line 2314
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->block_index_table:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$Asset;
    .locals 8

    .line 2325
    new-instance v7, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->encrypted_data:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->is_blocking:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->header:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->block_index_table:Lokio/ByteString;

    iget-object v5, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset;-><init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lokio/ByteString;Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2281
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$Asset;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_data(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    .locals 0

    .line 2296
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->encrypted_data:Lokio/ByteString;

    return-object p0
.end method

.method public header(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    .locals 0

    .line 2306
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->header:Lokio/ByteString;

    return-object p0
.end method

.method public is_blocking(Ljava/lang/Boolean;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    .locals 0

    .line 2301
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->is_blocking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public requires_reboot(Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;)Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;
    .locals 0

    .line 2319
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Asset$Builder;->requires_reboot:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0
.end method
