.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Reboot"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$Reboot$ProtoAdapter_Reboot;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUDIO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public static final enum BLE:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public static final enum NO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public static final enum USB:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

.field public static final enum YES:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1948
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v1, 0x0

    const-string v2, "NO"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->NO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 1950
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v2, 0x1

    const-string v3, "YES"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->YES:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 1952
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v3, 0x2

    const-string v4, "AUDIO"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->AUDIO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 1954
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v4, 0x3

    const-string v5, "USB"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->USB:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 1956
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v5, 0x4

    const-string v6, "BLE"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->BLE:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 1947
    sget-object v6, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->NO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->YES:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->AUDIO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->USB:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->BLE:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    .line 1958
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot$ProtoAdapter_Reboot;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot$ProtoAdapter_Reboot;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1962
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1963
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1975
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->BLE:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0

    .line 1974
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->USB:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0

    .line 1973
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->AUDIO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0

    .line 1972
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->YES:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0

    .line 1971
    :cond_4
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->NO:Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
    .locals 1

    .line 1947
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;
    .locals 1

    .line 1947
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1982
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$Reboot;->value:I

    return v0
.end method
