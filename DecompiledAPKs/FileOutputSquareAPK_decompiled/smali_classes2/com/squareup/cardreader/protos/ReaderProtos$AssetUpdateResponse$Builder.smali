.class public final Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

.field public assets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2464
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2465
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public app_update(Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;
    .locals 0

    .line 2475
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    return-object p0
.end method

.method public assets(Ljava/util/List;)Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;)",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;"
        }
    .end annotation

    .line 2469
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2470
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;
    .locals 4

    .line 2481
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;-><init>(Ljava/util/List;Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2459
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    move-result-object v0

    return-object v0
.end method
