.class final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$ProtoAdapter_OnBatteryDead;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnBatteryDead"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4282
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4297
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;-><init>()V

    .line 4298
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4299
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 4302
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4306
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4307
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4280
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$ProtoAdapter_OnBatteryDead;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4292
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4280
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$ProtoAdapter_OnBatteryDead;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)I
    .locals 0

    .line 4287
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4280
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$ProtoAdapter_OnBatteryDead;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;
    .locals 0

    .line 4312
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;

    move-result-object p1

    .line 4313
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4314
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4280
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead$ProtoAdapter_OnBatteryDead;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    move-result-object p1

    return-object p1
.end method
