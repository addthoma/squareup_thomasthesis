.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnCoreDump"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$ProtoAdapter_OnCoreDump;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DATA_DATA:Lokio/ByteString;

.field public static final DEFAULT_KEY_DATA:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final data_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x2
    .end annotation
.end field

.field public final key_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4436
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$ProtoAdapter_OnCoreDump;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$ProtoAdapter_OnCoreDump;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 4440
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->DEFAULT_KEY_DATA:Lokio/ByteString;

    .line 4442
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->DEFAULT_DATA_DATA:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 4457
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;-><init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V
    .locals 1

    .line 4461
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4462
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    .line 4463
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4478
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4479
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    .line 4480
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    .line 4481
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    .line 4482
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4487
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 4489
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4490
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4491
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 4492
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;
    .locals 2

    .line 4468
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;-><init>()V

    .line 4469
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->key_data:Lokio/ByteString;

    .line 4470
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->data_data:Lokio/ByteString;

    .line 4471
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4435
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4499
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4500
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", key_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->key_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4501
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    if-eqz v1, :cond_1

    const-string v1, ", data_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->data_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OnCoreDump{"

    .line 4502
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
