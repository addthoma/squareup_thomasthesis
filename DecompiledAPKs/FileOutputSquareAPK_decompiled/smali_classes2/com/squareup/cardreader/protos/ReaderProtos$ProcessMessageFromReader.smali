.class public final Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProcessMessageFromReader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;,
        Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$InitializingSecureSession#ADAPTER"
        tag = 0x2d
    .end annotation
.end field

.field public final on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnBatteryDead#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnCardInserted#ADAPTER"
        tag = 0x16
    .end annotation
.end field

.field public final on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$OnCardReaderInfoChanged#ADAPTER"
        tag = 0x25
    .end annotation
.end field

.field public final on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnCardRemoved#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnCardReaderBackendInitialized#ADAPTER"
        tag = 0x38
    .end annotation
.end field

.field public final on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnCoreDump#ADAPTER"
        tag = 0x4c
    .end annotation
.end field

.field public final on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnDeviceUnsupported#ADAPTER"
        tag = 0x32
    .end annotation
.end field

.field public final on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess#ADAPTER"
        tag = 0x58
    .end annotation
.end field

.field public final on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete#ADAPTER"
        tag = 0x59
    .end annotation
.end field

.field public final on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted#ADAPTER"
        tag = 0x5a
    .end annotation
.end field

.field public final on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnFullCommsEstablished#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnInitializationComplete#ADAPTER"
        tag = 0x56
    .end annotation
.end field

.field public final on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad#ADAPTER"
        tag = 0x41
    .end annotation
.end field

.field public final on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnReaderFailedToConnect#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionAborted#ADAPTER"
        tag = 0x5d
    .end annotation
.end field

.field public final on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionDenied#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionError#ADAPTER"
        tag = 0x36
    .end annotation
.end field

.field public final on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionInvalid#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionValid#ADAPTER"
        tag = 0x57
    .end annotation
.end field

.field public final on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnTamperData#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer#ADAPTER"
        tag = 0x2c
    .end annotation
.end field

.field public final send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$SendSecureSessionMessageToServer#ADAPTER"
        tag = 0xd
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7201
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;Lokio/ByteString;)V
    .locals 1

    .line 7368
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7369
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    .line 7370
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    .line 7371
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    .line 7372
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    .line 7373
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    .line 7374
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    .line 7375
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    .line 7376
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    .line 7377
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    .line 7378
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    .line 7379
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    .line 7380
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    .line 7381
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    .line 7382
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    .line 7383
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    .line 7384
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    .line 7385
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    .line 7386
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    .line 7387
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    .line 7388
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    .line 7389
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    .line 7390
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    .line 7391
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    .line 7392
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    .line 7393
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    .line 7394
    iget-object p2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    .line 7395
    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7435
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7436
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    .line 7437
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    .line 7438
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    .line 7439
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    .line 7440
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    .line 7441
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    .line 7442
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    .line 7443
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    .line 7444
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    .line 7445
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    .line 7446
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    .line 7447
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    .line 7448
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    .line 7449
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    .line 7450
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    .line 7451
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    .line 7452
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    .line 7453
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    .line 7454
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    .line 7455
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    .line 7456
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    .line 7457
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    .line 7458
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    .line 7459
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    .line 7460
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    .line 7461
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    .line 7462
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    .line 7463
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    .line 7464
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 7469
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1b

    .line 7471
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7472
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7473
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7474
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7475
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7476
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7477
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7478
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7479
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7480
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7481
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7482
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7483
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7484
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7485
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7486
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7487
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7488
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7489
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7490
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7491
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7492
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7493
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7494
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7495
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7496
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7497
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7498
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->hashCode()I

    move-result v2

    :cond_1a
    add-int/2addr v0, v2

    .line 7499
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1b
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 2

    .line 7400
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;-><init>()V

    .line 7401
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    .line 7402
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    .line 7403
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    .line 7404
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    .line 7405
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    .line 7406
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    .line 7407
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    .line 7408
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    .line 7409
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    .line 7410
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    .line 7411
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    .line 7412
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    .line 7413
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    .line 7414
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    .line 7415
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    .line 7416
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    .line 7417
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    .line 7418
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    .line 7419
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    .line 7420
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    .line 7421
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    .line 7422
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    .line 7423
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    .line 7424
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    .line 7425
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    .line 7426
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    .line 7427
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    .line 7428
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7200
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7506
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7507
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    if-eqz v1, :cond_0

    const-string v1, ", send_firmware_manifest_to_server="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7508
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    if-eqz v1, :cond_1

    const-string v1, ", on_firmware_update_started="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7509
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    if-eqz v1, :cond_2

    const-string v1, ", on_firmware_update_progress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7510
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    if-eqz v1, :cond_3

    const-string v1, ", on_firmware_update_asset_success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7511
    :cond_3
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    if-eqz v1, :cond_4

    const-string v1, ", on_firmware_update_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7512
    :cond_4
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    if-eqz v1, :cond_5

    const-string v1, ", on_firmware_update_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7513
    :cond_5
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    if-eqz v1, :cond_6

    const-string v1, ", on_cardreader_backend_initialized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7514
    :cond_6
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    if-eqz v1, :cond_7

    const-string v1, ", on_device_unsupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7515
    :cond_7
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    if-eqz v1, :cond_8

    const-string v1, ", on_reader_failed_to_connect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7516
    :cond_8
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    if-eqz v1, :cond_9

    const-string v1, ", on_init_firmware_update_required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7517
    :cond_9
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    if-eqz v1, :cond_a

    const-string v1, ", on_init_register_update_required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7518
    :cond_a
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    if-eqz v1, :cond_b

    const-string v1, ", on_full_comms_established="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7519
    :cond_b
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    if-eqz v1, :cond_c

    const-string v1, ", on_battery_dead="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7520
    :cond_c
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    if-eqz v1, :cond_d

    const-string v1, ", on_tamper_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7521
    :cond_d
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    if-eqz v1, :cond_e

    const-string v1, ", on_core_dump="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7522
    :cond_e
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    if-eqz v1, :cond_f

    const-string v1, ", initializing_secure_session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7523
    :cond_f
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    if-eqz v1, :cond_10

    const-string v1, ", send_secure_session_message_to_server="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7524
    :cond_10
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    if-eqz v1, :cond_11

    const-string v1, ", on_secure_session_aborted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7525
    :cond_11
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    if-eqz v1, :cond_12

    const-string v1, ", on_secure_session_invalid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7526
    :cond_12
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    if-eqz v1, :cond_13

    const-string v1, ", on_secure_session_denied="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7527
    :cond_13
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    if-eqz v1, :cond_14

    const-string v1, ", on_secure_session_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7528
    :cond_14
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    if-eqz v1, :cond_15

    const-string v1, ", on_secure_session_valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7529
    :cond_15
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    if-eqz v1, :cond_16

    const-string v1, ", on_initialization_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7530
    :cond_16
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    if-eqz v1, :cond_17

    const-string v1, ", on_card_inserted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7531
    :cond_17
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    if-eqz v1, :cond_18

    const-string v1, ", on_card_removed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7532
    :cond_18
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    if-eqz v1, :cond_19

    const-string v1, ", on_card_reader_info_changed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7533
    :cond_19
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    if-eqz v1, :cond_1a

    const-string v1, ", on_libraries_failed_to_load="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProcessMessageFromReader{"

    .line 7534
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
