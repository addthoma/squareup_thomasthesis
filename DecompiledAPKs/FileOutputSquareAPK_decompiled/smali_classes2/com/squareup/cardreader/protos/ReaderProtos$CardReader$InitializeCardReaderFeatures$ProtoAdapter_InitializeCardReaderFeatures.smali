.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InitializeCardReaderFeatures"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 243
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 265
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;-><init>()V

    .line 266
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 267
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 273
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 271
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    goto :goto_0

    .line 270
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->currency_code(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    goto :goto_0

    .line 269
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->mcc(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    goto :goto_0

    .line 277
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 278
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 257
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 258
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 259
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 260
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 241
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)I
    .locals 4

    .line 248
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->mcc:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->currency_code:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 249
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    const/4 v3, 0x3

    .line 250
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 251
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
    .locals 2

    .line 283
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;

    move-result-object p1

    .line 284
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    .line 285
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 286
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 241
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$ProtoAdapter_InitializeCardReaderFeatures;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    move-result-object p1

    return-object p1
.end method
