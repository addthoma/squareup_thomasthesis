.class public final Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReaderFeatureFlags"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;,
        Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCOUNT_TYPE_SELECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_COMMON_DEBIT_SUPPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_FELICA_NOTIFICATION:Ljava/lang/Boolean;

.field public static final DEFAULT_PINBLOCK_FORMAT_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_QUICKCHIP_FW209030:Ljava/lang/Boolean;

.field public static final DEFAULT_SONIC_BRANDING:Ljava/lang/Boolean;

.field public static final DEFAULT_SPOC_PRNG_SEED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final account_type_selection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final common_debit_support:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final felica_notification:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final pinblock_format_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final quickchip_fw209030:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final sonic_branding:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final spoc_prng_seed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8403
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$ProtoAdapter_ReaderFeatureFlags;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 8407
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_QUICKCHIP_FW209030:Ljava/lang/Boolean;

    .line 8409
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_PINBLOCK_FORMAT_V2:Ljava/lang/Boolean;

    .line 8411
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_ACCOUNT_TYPE_SELECTION:Ljava/lang/Boolean;

    .line 8413
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_SPOC_PRNG_SEED:Ljava/lang/Boolean;

    .line 8415
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_COMMON_DEBIT_SUPPORT:Ljava/lang/Boolean;

    .line 8417
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_SONIC_BRANDING:Ljava/lang/Boolean;

    .line 8419
    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->DEFAULT_FELICA_NOTIFICATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 9

    .line 8466
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 8472
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8473
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 8474
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 8475
    iput-object p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    .line 8476
    iput-object p4, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 8477
    iput-object p5, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    .line 8478
    iput-object p6, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    .line 8479
    iput-object p7, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8499
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8500
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    .line 8501
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 8502
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 8503
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    .line 8504
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 8505
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    .line 8506
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    .line 8507
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    .line 8508
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8513
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 8515
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8516
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8517
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8518
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8519
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8520
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8521
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8522
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 8523
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;
    .locals 2

    .line 8484
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;-><init>()V

    .line 8485
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 8486
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 8487
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->account_type_selection:Ljava/lang/Boolean;

    .line 8488
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 8489
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->common_debit_support:Ljava/lang/Boolean;

    .line 8490
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->sonic_branding:Ljava/lang/Boolean;

    .line 8491
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->felica_notification:Ljava/lang/Boolean;

    .line 8492
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 8402
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8530
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8531
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", quickchip_fw209030="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->quickchip_fw209030:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8532
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", pinblock_format_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->pinblock_format_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8533
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", account_type_selection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->account_type_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8534
    :cond_2
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", spoc_prng_seed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->spoc_prng_seed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8535
    :cond_3
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", common_debit_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->common_debit_support:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8536
    :cond_4
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", sonic_branding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->sonic_branding:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8537
    :cond_5
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", felica_notification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;->felica_notification:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReaderFeatureFlags{"

    .line 8538
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
