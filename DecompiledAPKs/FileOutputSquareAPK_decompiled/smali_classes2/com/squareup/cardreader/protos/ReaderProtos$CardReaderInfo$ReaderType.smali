.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReaderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType$ProtoAdapter_ReaderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum A10:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ENCRYPTED_KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum O1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum R12:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum R4:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum R6:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum S1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum UNENCRYPTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

.field public static final enum X2:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 6723
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6725
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v2, 0x1

    const-string v3, "KEYED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6727
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v3, 0x2

    const-string v4, "UNENCRYPTED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNENCRYPTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6729
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v4, 0x3

    const-string v5, "O1"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->O1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6731
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v5, 0x4

    const-string v6, "S1"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->S1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6733
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v6, 0x5

    const-string v7, "R4"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R4:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6735
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v7, 0x6

    const-string v8, "R6"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R6:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6737
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/4 v8, 0x7

    const-string v9, "ENCRYPTED_KEYED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6739
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/16 v9, 0x8

    const-string v10, "A10"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->A10:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6741
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/16 v10, 0x9

    const-string v11, "R12"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R12:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6743
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/16 v11, 0xa

    const-string v12, "X2"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->X2:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6722
    sget-object v12, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNENCRYPTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->O1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->S1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R4:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R6:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->A10:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R12:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->X2:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    .line 6745
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType$ProtoAdapter_ReaderType;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType$ProtoAdapter_ReaderType;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 6749
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 6750
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 6768
    :pswitch_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->X2:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6767
    :pswitch_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R12:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6766
    :pswitch_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->A10:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6765
    :pswitch_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6764
    :pswitch_4
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R6:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6763
    :pswitch_5
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->R4:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6762
    :pswitch_6
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->S1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6761
    :pswitch_7
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->O1:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6760
    :pswitch_8
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNENCRYPTED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6759
    :pswitch_9
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->KEYED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    .line 6758
    :pswitch_a
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->UNKNOWN:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;
    .locals 1

    .line 6722
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;
    .locals 1

    .line 6722
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 6775
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$ReaderType;->value:I

    return v0
.end method
