.class public final Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adf_name:Lokio/ByteString;

.field public label:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1883
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adf_name(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;
    .locals 0

    .line 1887
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->adf_name:Lokio/ByteString;

    return-object p0
.end method

.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;
    .locals 4

    .line 1898
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->adf_name:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->label:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;-><init>(Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1878
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;
    .locals 0

    .line 1892
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$EmvApplication$Builder;->label:Ljava/lang/String;

    return-object p0
.end method
