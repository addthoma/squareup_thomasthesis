.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1713
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;
    .locals 3

    .line 1723
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC$Builder;->data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1710
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC;

    move-result-object v0

    return-object v0
.end method

.method public data(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC$Builder;
    .locals 0

    .line 1717
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessARPC$Builder;->data:Lokio/ByteString;

    return-object p0
.end method
