.class final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$ProtoAdapter_OnFirmwareUpdateError;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnFirmwareUpdateError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 3565
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3582
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;-><init>()V

    .line 3583
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 3584
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 3595
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 3588
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    invoke-virtual {v0, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;->result(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 3590
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 3599
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 3600
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3563
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$ProtoAdapter_OnFirmwareUpdateError;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3576
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 3577
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 3563
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$ProtoAdapter_OnFirmwareUpdateError;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)I
    .locals 3

    .line 3570
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->result:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$FirmwareUpdateResult;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 3571
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 3563
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$ProtoAdapter_OnFirmwareUpdateError;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;
    .locals 0

    .line 3605
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;

    move-result-object p1

    .line 3606
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 3607
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 3563
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError$ProtoAdapter_OnFirmwareUpdateError;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    move-result-object p1

    return-object p1
.end method
