.class public final Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AssetUpdateResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$ProtoAdapter_AssetUpdateResponse;,
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;,
        Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_UPDATE:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

.field private static final serialVersionUID:J


# instance fields
.field public final app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$AssetUpdateResponse$AppUpdate#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final assets:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$Asset#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2391
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$ProtoAdapter_AssetUpdateResponse;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$ProtoAdapter_AssetUpdateResponse;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2395
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;->SUGGEST:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->DEFAULT_APP_UPDATE:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;",
            ")V"
        }
    .end annotation

    .line 2411
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;-><init>(Ljava/util/List;Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$Asset;",
            ">;",
            "Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 2415
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "assets"

    .line 2416
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    .line 2417
    iput-object p2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2432
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2433
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    .line 2434
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    .line 2435
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    .line 2436
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 2441
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 2443
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2444
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2445
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2446
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;
    .locals 2

    .line 2422
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;-><init>()V

    .line 2423
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->assets:Ljava/util/List;

    .line 2424
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    .line 2425
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2390
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2453
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2454
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", assets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->assets:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2455
    :cond_0
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    if-eqz v1, :cond_1

    const-string v1, ", app_update="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;->app_update:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse$AppUpdate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AssetUpdateResponse{"

    .line 2456
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
