.class public final Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;
.super Lcom/squareup/wire/Message;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnSecureSessionDenied"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ProtoAdapter_OnSecureSessionDenied;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;,
        Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SERVER_DENY_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

.field private static final serialVersionUID:J


# instance fields
.field public final server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.cardreader.protos.ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5035
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ProtoAdapter_OnSecureSessionDenied;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ProtoAdapter_OnSecureSessionDenied;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5039
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->GENERIC_ERROR:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->DEFAULT_SERVER_DENY_TYPE:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;)V
    .locals 1

    .line 5048
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;Lokio/ByteString;)V
    .locals 1

    .line 5052
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5053
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5067
    :cond_0
    instance-of v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5068
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    .line 5069
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    iget-object p1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5070
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 5075
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 5077
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5078
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 5079
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;
    .locals 2

    .line 5058
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;-><init>()V

    .line 5059
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    iput-object v1, v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    .line 5060
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5034
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5086
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5087
    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    if-eqz v1, :cond_0

    const-string v1, ", server_deny_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->server_deny_type:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied$ServerDenyType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "OnSecureSessionDenied{"

    .line 5088
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
