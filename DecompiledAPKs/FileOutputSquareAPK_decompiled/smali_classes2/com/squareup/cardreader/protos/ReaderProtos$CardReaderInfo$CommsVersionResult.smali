.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommsVersionResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult$ProtoAdapter_CommsVersionResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARDREADER_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

.field public static final enum FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

.field public static final enum OK:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 6581
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    const/4 v1, 0x0

    const-string v2, "OK"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->OK:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6583
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    const/4 v2, 0x1

    const-string v3, "CARDREADER_UPDATE_REQUIRED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->CARDREADER_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6585
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    const/4 v3, 0x2

    const-string v4, "FIRMWARE_UPDATE_REQUIRED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6580
    sget-object v4, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->OK:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->CARDREADER_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    .line 6587
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult$ProtoAdapter_CommsVersionResult;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult$ProtoAdapter_CommsVersionResult;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 6591
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 6592
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 6602
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->FIRMWARE_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    return-object p0

    .line 6601
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->CARDREADER_UPDATE_REQUIRED:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    return-object p0

    .line 6600
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->OK:Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;
    .locals 1

    .line 6580
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;
    .locals 1

    .line 6580
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 6609
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReaderInfo$CommsVersionResult;->value:I

    return v0
.end method
