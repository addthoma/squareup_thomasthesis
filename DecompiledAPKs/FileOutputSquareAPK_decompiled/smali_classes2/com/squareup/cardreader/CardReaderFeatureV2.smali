.class public final Lcom/squareup/cardreader/CardReaderFeatureV2;
.super Ljava/lang/Object;
.source "CardReaderFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CardreaderPointerProvider;
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/CardReaderFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B#\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u000e\u001a\u00020\rH\u0016J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0008\u0010\u0013\u001a\u00020\u0010H\u0002J(\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u0008\u0010\u001a\u001a\u00020\u0010H\u0016J\u0010\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u0016H\u0016J\u0008\u0010\u001d\u001a\u00020\u0010H\u0016J\u0018\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u00162\u0006\u0010\u0011\u001a\u00020 H\u0016J\u0008\u0010!\u001a\u00020\u0010H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/cardreader/CardReaderFeatureV2;",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/CardReaderFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;",
        "timerProvider",
        "Lcom/squareup/cardreader/TimerPointerProvider;",
        "backendProvider",
        "Lcom/squareup/cardreader/BackendPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/TimerPointerProvider;Lcom/squareup/cardreader/BackendPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;",
        "cardreaderPointer",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CardReaderFeatureMessage;",
        "initialize",
        "onCommsVersionAcquired",
        "resultValue",
        "",
        "transportVersion",
        "appVersion",
        "endpointVersion",
        "onReaderError",
        "onReaderReady",
        "readerType",
        "onRpcCallbackRecvd",
        "reportError",
        "endPoint",
        "",
        "resetIfInitilized",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final backendProvider:Lcom/squareup/cardreader/BackendPointerProvider;

.field private featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final timerProvider:Lcom/squareup/cardreader/TimerPointerProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/TimerPointerProvider;Lcom/squareup/cardreader/BackendPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/TimerPointerProvider;",
            "Lcom/squareup/cardreader/BackendPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timerProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backendProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->timerProvider:Lcom/squareup/cardreader/TimerPointerProvider;

    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->backendProvider:Lcom/squareup/cardreader/BackendPointerProvider;

    return-void
.end method

.method public static final synthetic access$getFeaturePointer$p(Lcom/squareup/cardreader/CardReaderFeatureV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    if-nez p0, :cond_0

    const-string v0, "featurePointer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setFeaturePointer$p(Lcom/squareup/cardreader/CardReaderFeatureV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    return-void
.end method

.method private final initialize()V
    .locals 4

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "initialize"

    .line 36
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->backendProvider:Lcom/squareup/cardreader/BackendPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/BackendPointerProvider;->backendPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->timerProvider:Lcom/squareup/cardreader/TimerPointerProvider;

    invoke-interface {v1}, Lcom/squareup/cardreader/TimerPointerProvider;->timerPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    move-result-object v1

    .line 38
    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    const-string v1, "CardreaderNative.cardrea\u2026erPointer(), this\n      )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderResult;->CR_CARDREADER_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    if-ne v0, v1, :cond_1

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnInitialized;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnInitialized;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    goto :goto_0

    .line 45
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnFailed;

    const-string v3, "result"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnFailed;-><init>(Lcom/squareup/cardreader/lcr/CrCardreaderResult;)V

    check-cast v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CardReaderFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    instance-of p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CardReaderFeatureMessage$Initialize;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderFeatureV2;->initialize()V

    :cond_0
    return-void
.end method

.method public onCommsVersionAcquired(IIII)V
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 79
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;

    .line 80
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object p1

    const-string v2, "CrCommsVersionResult.swigToEnum(resultValue)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;-><init>(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;III)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 78
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onReaderError()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderError;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderError;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onReaderReady(I)V
    .locals 3

    .line 63
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderReady;

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderType;

    move-result-object p1

    const-string v2, "CrCardreaderType.swigToEnum(readerType)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReaderReady;-><init>(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onRpcCallbackRecvd()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnRpcCallbackRecvd;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnRpcCallbackRecvd;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public reportError(ILjava/lang/String;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 94
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReportError;

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnReportError;-><init>(ILjava/lang/String;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 93
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 51
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/CardReaderFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_notify_reader_unplugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CardreaderNative;->cr_cardreader_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    :cond_3
    return-void
.end method
