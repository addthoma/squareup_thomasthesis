.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceivedManifest"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00052\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;",
        "manifest",
        "",
        "requiredUpdate",
        "",
        "result",
        "",
        "([BZI)V",
        "getManifest",
        "()[B",
        "getRequiredUpdate",
        "()Z",
        "getResult",
        "()I",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final manifest:[B

.field private final requiredUpdate:Z

.field private final result:I


# direct methods
.method public constructor <init>([BZI)V
    .locals 1

    const-string v0, "manifest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 316
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    iput-boolean p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    iput p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;[BZIILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->copy([BZI)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    return v0
.end method

.method public final copy([BZI)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;
    .locals 1

    const-string v0, "manifest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;-><init>([BZI)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    iget-boolean v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    iget p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getManifest()[B
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    return-object v0
.end method

.method public final getRequiredUpdate()Z
    .locals 1

    .line 314
    iget-boolean v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    return v0
.end method

.method public final getResult()I
    .locals 1

    .line 315
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceivedManifest(manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->manifest:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", requiredUpdate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->requiredUpdate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", result="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;->result:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
