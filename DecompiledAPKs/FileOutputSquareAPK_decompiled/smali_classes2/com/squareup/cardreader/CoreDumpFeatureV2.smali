.class public final Lcom/squareup/cardreader/CoreDumpFeatureV2;
.super Ljava/lang/Object;
.source "CoreDumpFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/CoreDumpFeature;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCoreDumpFeatureV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CoreDumpFeatureV2.kt\ncom/squareup/cardreader/CoreDumpFeatureV2\n*L\n1#1,94:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u0012\n\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0008\u0010\u0011\u001a\u00020\u0012H\u0002J\u000e\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015J\u0008\u0010\u0016\u001a\u00020\u0012H\u0002J\u0008\u0010\u0017\u001a\u00020\u0012H\u0016J\u0010\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J \u0010\u001b\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u001dH\u0016J\u0018\u0010 \u001a\u00020\u00122\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\"H\u0016J\u0010\u0010$\u001a\u00020\u00122\u0006\u0010%\u001a\u00020\u001aH\u0016J\u0008\u0010&\u001a\u00020\u0012H\u0016J\u0008\u0010\'\u001a\u00020\u0010H\u0002J\u0008\u0010(\u001a\u00020\u0012H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/cardreader/CoreDumpFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/CoreDumpFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V",
        "checkForCoreDump",
        "Lcom/squareup/cardreader/lcr/CrCoredumpResult;",
        "eraseCoreDump",
        "",
        "handleMessage",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage;",
        "initialize",
        "onCoreDumpErased",
        "onCoreDumpInfo",
        "isCoredump",
        "",
        "onCoreDumpProgress",
        "bytesReceived",
        "",
        "bytesSoFar",
        "bytesTotal",
        "onCoreDumpReceived",
        "keyBytes",
        "",
        "dataBytes",
        "onCoreDumpTriggered",
        "wasTriggered",
        "resetIfInitilized",
        "retrieveCoreDump",
        "triggerCoreDump",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CoreDumpFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method private final checkForCoreDump()Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_get_info(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object v0

    const-string v1, "CoredumpFeatureNative.cr\u2026_get_info(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final eraseCoreDump()V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_erase(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    return-void
.end method

.method private final initialize()V
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->coredump_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    move-result-object v0

    const-string v1, "CoredumpFeatureNative.co\u2026ardreaderPointer(), this)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    return-void
.end method

.method private final retrieveCoreDump()Lcom/squareup/cardreader/lcr/CrCoredumpResult;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_get_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    move-result-object v0

    const-string v1, "CoredumpFeatureNative.cr\u2026_get_data(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final triggerCoreDump()V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->coredump_trigger_dump(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V

    return-void
.end method


# virtual methods
.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$Initialize;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$Initialize;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->initialize()V

    goto :goto_0

    .line 23
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$TriggerCoreDump;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$TriggerCoreDump;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->triggerCoreDump()V

    goto :goto_0

    .line 24
    :cond_1
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$CheckForCoreDump;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$CheckForCoreDump;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->checkForCoreDump()Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    goto :goto_0

    .line 25
    :cond_2
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$RetrieveCoreDump;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$RetrieveCoreDump;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->retrieveCoreDump()Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    goto :goto_0

    .line 26
    :cond_3
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$EraseCoreDump;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$CoreDumpFeatureMessage$EraseCoreDump;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/squareup/cardreader/CoreDumpFeatureV2;->eraseCoreDump()V

    :cond_4
    :goto_0
    return-void
.end method

.method public onCoreDumpErased()V
    .locals 3

    .line 91
    new-instance v0, Lkotlin/NotImplementedError;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onCoreDumpInfo(Z)V
    .locals 2

    .line 69
    new-instance p1, Lkotlin/NotImplementedError;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p1, v0, v1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onCoreDumpProgress(III)V
    .locals 0

    .line 86
    new-instance p1, Lkotlin/NotImplementedError;

    const/4 p2, 0x0

    const/4 p3, 0x1

    invoke-direct {p1, p2, p3, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onCoreDumpReceived([B[B)V
    .locals 1

    const-string v0, "keyBytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "dataBytes"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    new-instance p1, Lkotlin/NotImplementedError;

    const/4 p2, 0x0

    const/4 v0, 0x1

    invoke-direct {p1, p2, v0, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public onCoreDumpTriggered(Z)V
    .locals 2

    .line 64
    new-instance p1, Lkotlin/NotImplementedError;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p1, v0, v1, v0}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 36
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/CoreDumpFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-eqz v0, :cond_2

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    .line 38
    iget-object v0, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CoredumpFeatureNative;->cr_coredump_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)Lcom/squareup/cardreader/lcr/CrCoredumpResult;

    :cond_2
    return-void
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iput-object p1, p0, Lcom/squareup/cardreader/CoreDumpFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_coredump_t;

    return-void
.end method
