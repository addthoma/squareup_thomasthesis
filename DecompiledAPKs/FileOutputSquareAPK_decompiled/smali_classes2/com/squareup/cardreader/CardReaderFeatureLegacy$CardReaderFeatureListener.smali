.class public interface abstract Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;
.super Ljava/lang/Object;
.source "CardReaderFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CardReaderFeatureListener"
.end annotation


# virtual methods
.method public abstract onAudioConnectionTimeout()V
.end method

.method public abstract onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
.end method

.method public abstract onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
.end method

.method public abstract onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
.end method
