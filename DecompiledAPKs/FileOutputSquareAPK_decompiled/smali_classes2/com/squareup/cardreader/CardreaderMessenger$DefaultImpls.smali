.class public final Lcom/squareup/cardreader/CardreaderMessenger$DefaultImpls;
.super Ljava/lang/Object;
.source "CardreaderMessengerInterface.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardreaderMessenger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static responses(Lcom/squareup/cardreader/CardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardreaderMessenger;",
            "Lcom/squareup/cardreader/CardreaderConnectionId;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "cardreaderId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-interface {p0}, Lcom/squareup/cardreader/CardreaderMessenger;->getResponses()Lio/reactivex/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/cardreader/CardreaderMessenger$responses$1;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/CardreaderMessenger$responses$1;-><init>(Lcom/squareup/cardreader/CardreaderConnectionId;)V

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p0

    .line 16
    sget-object p1, Lcom/squareup/cardreader/CardreaderMessenger$responses$2;->INSTANCE:Lcom/squareup/cardreader/CardreaderMessenger$responses$2;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "responses.filter { it.re\u2026      .map { it.message }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
