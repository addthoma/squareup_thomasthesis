.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnCommsVersionAcquired"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;",
        "resultValue",
        "Lcom/squareup/cardreader/lcr/CrCommsVersionResult;",
        "transportVersion",
        "",
        "appVersion",
        "endpointVersion",
        "(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;III)V",
        "getAppVersion",
        "()I",
        "getEndpointVersion",
        "getResultValue",
        "()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;",
        "getTransportVersion",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final appVersion:I

.field private final endpointVersion:I

.field private final resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

.field private final transportVersion:I


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;III)V
    .locals 1

    const-string v0, "resultValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 220
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    iput p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    iput p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    iput p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;IIIILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->copy(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;III)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    return v0
.end method

.method public final copy(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;III)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;
    .locals 1

    const-string v0, "resultValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;-><init>(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    iget p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAppVersion()I
    .locals 1

    .line 218
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    return v0
.end method

.method public final getEndpointVersion()I
    .locals 1

    .line 219
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    return v0
.end method

.method public final getResultValue()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    return-object v0
.end method

.method public final getTransportVersion()I
    .locals 1

    .line 217
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnCommsVersionAcquired(resultValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->resultValue:Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", transportVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->transportVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", appVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->appVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", endpointVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$CardReaderFeatureOutput$OnCommsVersionAcquired;->endpointVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
