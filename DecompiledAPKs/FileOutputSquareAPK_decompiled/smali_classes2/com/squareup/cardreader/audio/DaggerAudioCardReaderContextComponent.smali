.class public final Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;
.super Ljava/lang/Object;
.source "DaggerAudioCardReaderContextComponent.java"

# interfaces
.implements Lcom/squareup/cardreader/audio/AudioCardReaderContextComponent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_libraryLoader;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_swipeBus;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_telephoneManager;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_audioManager;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badBus;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderPauseAndResumer;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bluetoothUtils;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideMinesweeper;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_magSwipeFailureFilter;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideSquidInterfaceScheduler;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideNativeBinaries;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_touchReporting;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_isReaderSdk;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_minesweeperTicket;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_application;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_realCardReaderListeners;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideTmnTimings;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_swipeEventLogger;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_accessibilityManager;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_running;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideHeadsetConnectionState;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_crashnado;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_lcrExecutor;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderFactory;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderListeners;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainThread;,
        Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;
    }
.end annotation


# instance fields
.field private accessibilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/view/accessibility/AccessibilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private audioManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/media/AudioManager;",
            ">;"
        }
    .end annotation
.end field

.field private badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private badEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private crashnadoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/Crashnado;",
            ">;"
        }
    .end annotation
.end field

.field private firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

.field private isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private libraryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private magSwipeFailureFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            ">;"
        }
    .end annotation
.end field

.field private mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private minesweeperTicketProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/MinesweeperTicket;",
            ">;"
        }
    .end annotation
.end field

.field private nativeLoggingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioBackendInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioBackendNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioBackendProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioBackendSessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioGraphInitiailizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioGraphInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioPlayerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioStartAndStopperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioStartAndStopper;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioTrackFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/AudioTrackFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private provideAudioVolumeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private provideCanPlayAudioProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderAddressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderConstantsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderContextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderContext;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInfoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderLogBridge;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private provideCardReaderSwigProvider:Ljavax/inject/Provider;

.field private provideCardreaderNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideClassifyingDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/ClassifyingDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideConnectionTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;",
            ">;"
        }
    .end annotation
.end field

.field private provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

.field private provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventDataForwarderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/EventDataForwarder;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventLogFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/EventLogFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/EventlogFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideFastSqLinkSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideFirmwareUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private provideGen2SignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private provideInputSampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private provideLocalCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LocalCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private provideLogNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/LogNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideMicRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/MicRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private provideMinesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private provideNativeBinariesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/NativeBinaries;",
            ">;"
        }
    .end annotation
.end field

.field private provideNfcListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideO1SignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/o1/O1SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideOutputSampleRateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentCompletionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentFeatureV2;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentFeatureProvider:Ljavax/inject/Provider;

.field private providePaymentListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;",
            ">;"
        }
    .end annotation
.end field

.field private providePaymentProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PaymentProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private providePowerFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private providePowerFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideR4FastSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideR4SlowSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideReaderTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
            ">;"
        }
    .end annotation
.end field

.field private provideResProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private provideSampleFeederProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/SampleFeeder;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

.field private provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationFeature;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureSessionRevocationState;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SecureTouchFeatureInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSlowSqLinkSignalDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideSquarewaveDecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SquarewaveDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/SystemFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideSystemFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTamperFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideTamperFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TamperFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimerApiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/TimerApiLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideTimerNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TimerNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private provideTmnTimingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tmn/TmnTimings;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserInteractionFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/UserInteractionFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field

.field private provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private r6CardReaderAwakenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/R6CardReaderAwakener;",
            ">;"
        }
    .end annotation
.end field

.field private realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private runningProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;"
        }
    .end annotation
.end field

.field private swipeEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/SwipeEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private telephoneManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/telephony/TelephonyManager;",
            ">;"
        }
    .end annotation
.end field

.field private touchReportingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/securetouch/TouchReporting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 0

    .line 396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 398
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->initialize(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    .line 399
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->initialize2(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$1;)V
    .locals 0

    .line 181
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;-><init>(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    return-void
.end method

.method public static builder()Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;
    .locals 2

    .line 403
    new-instance v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$Builder;-><init>(Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$1;)V

    return-object v0
.end method

.method private initialize(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 26

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 409
    invoke-static/range {p1 .. p1}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderIdFactory;->create(Lcom/squareup/cardreader/CardReaderModule;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderIdFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    .line 410
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideConnectionTypeFactory;->create()Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideConnectionTypeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideConnectionTypeProvider:Ljavax/inject/Provider;

    .line 411
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderAddressFactory;->create()Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderAddressFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderAddressProvider:Ljavax/inject/Provider;

    .line 412
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;->create()Lcom/squareup/wavpool/swipe/AndroidAudioModule_ProvideCardReaderNameFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderNameProvider:Ljavax/inject/Provider;

    .line 413
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideConnectionTypeProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderAddressProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderNameProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInfoFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    .line 414
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainThread;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_mainThread;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    .line 415
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderListeners;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderListeners;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 416
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderFactory;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderFactory;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 417
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_lcrExecutor;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_lcrExecutor;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 418
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_nativeLoggingEnabled;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->nativeLoggingEnabledProvider:Ljavax/inject/Provider;

    .line 419
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_crashnado;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_crashnado;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    .line 420
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;->create()Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideInputSampleRateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideInputSampleRateFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideInputSampleRateProvider:Ljavax/inject/Provider;

    .line 421
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;->create()Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideOutputSampleRateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideOutputSampleRateFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideOutputSampleRateProvider:Ljavax/inject/Provider;

    .line 422
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideHeadsetConnectionState;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideHeadsetConnectionState;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;

    .line 423
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_running;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_running;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->runningProvider:Ljavax/inject/Provider;

    .line 424
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_accessibilityManager;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_accessibilityManager;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->accessibilityManagerProvider:Ljavax/inject/Provider;

    .line 425
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->runningProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->accessibilityManagerProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideCanPlayAudioFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCanPlayAudioProvider:Ljavax/inject/Provider;

    .line 426
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideInputSampleRateProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioTrackFinisherFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioTrackFinisherProvider:Ljavax/inject/Provider;

    .line 427
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    .line 428
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendSessionFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendSessionProvider:Ljavax/inject/Provider;

    .line 429
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideAudioBackendNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideAudioBackendNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendNativeProvider:Ljavax/inject/Provider;

    .line 430
    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideOutputSampleRateProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCanPlayAudioProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioTrackFinisherProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendSessionProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendNativeProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v10}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioPlayerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioPlayerProvider:Ljavax/inject/Provider;

    .line 431
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 432
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    .line 433
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_swipeEventLogger;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_swipeEventLogger;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->swipeEventLoggerProvider:Ljavax/inject/Provider;

    .line 434
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->swipeEventLoggerProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideEventDataForwarderFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventDataForwarderProvider:Ljavax/inject/Provider;

    .line 435
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendInterfaceProvider:Ljavax/inject/Provider;

    .line 436
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCardreaderNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideCardreaderNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    .line 437
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideCardReaderInterfaceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideCardReaderInterfaceFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInterfaceProvider:Ljavax/inject/Provider;

    .line 438
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideInputSampleRateProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideOutputSampleRateProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioPlayerProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventDataForwarderProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendInterfaceProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInterfaceProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v10}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideAudioBackendFactory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 439
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideTimerNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideTimerNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTimerNativeProvider:Ljavax/inject/Provider;

    .line 440
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTimerNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTimerApiProvider:Ljavax/inject/Provider;

    .line 441
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideLogNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideLogNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLogNativeProvider:Ljavax/inject/Provider;

    .line 442
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLogNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_Real_ProvideCardReaderLoggerInterfaceFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideCardReaderLoggerInterfaceFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;

    .line 443
    invoke-static {}, Lcom/squareup/cardreader/NativeCardReaderConstants_Factory;->create()Lcom/squareup/cardreader/NativeCardReaderConstants_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    .line 444
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->nativeLoggingEnabledProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTimerApiProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderLoggerInterfaceProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v10}, Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderFeatureLegacy_Factory;

    move-result-object v3

    invoke-static {v3}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v3

    invoke-static {v2, v3}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 445
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideCoredumpFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;

    .line 446
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCoredumpFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideCoreDumpFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCoreDumpFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

    .line 447
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideEventlogFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideEventlogFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;

    .line 448
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventlogFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideEventLogFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideEventLogFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventLogFeatureProvider:Ljavax/inject/Provider;

    .line 449
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideFirmwareUpdateFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideFirmwareUpdateFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    .line 450
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdateFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4}, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

    .line 451
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentFeatureNativeProvider:Ljavax/inject/Provider;

    .line 452
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideTmnTimings;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideTmnTimings;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    .line 453
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_realCardReaderListeners;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_realCardReaderListeners;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 454
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;

    .line 455
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->paymentFeatureDelegateSenderProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureDelegateFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;

    .line 456
    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentFeatureNativeProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTmnTimingsProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentFeatureDelegateProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v9}, Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePaymentFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentFeatureProvider:Ljavax/inject/Provider;

    .line 457
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePowerFeatureNativeProvider:Ljavax/inject/Provider;

    .line 458
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePowerFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePowerFeatureProvider:Ljavax/inject/Provider;

    .line 459
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_application;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_application;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    .line 460
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_minesweeperTicket;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_minesweeperTicket;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->minesweeperTicketProvider:Ljavax/inject/Provider;

    .line 461
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    .line 462
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationStateFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    .line 463
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_isReaderSdk;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_isReaderSdk;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 464
    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->minesweeperTicketProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionFeatureNativeProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v9}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

    .line 465
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchFeatureNativeInterfaceFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchFeatureNativeInterfaceFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    .line 466
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_touchReporting;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_touchReporting;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->touchReportingProvider:Ljavax/inject/Provider;

    .line 467
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideNativeBinaries;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideNativeBinaries;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideNativeBinariesProvider:Ljavax/inject/Provider;

    .line 468
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideSquidInterfaceScheduler;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideSquidInterfaceScheduler;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    .line 469
    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureTouchFeatureNativeInterfaceProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->touchReportingProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideNativeBinariesProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSquidInterfaceSchedulerProvider:Ljavax/inject/Provider;

    invoke-static/range {v3 .. v8}, Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureTouchCardReaderFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    .line 470
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    .line 471
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSystemFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSystemFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    .line 472
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTamperFeatureNativeProvider:Ljavax/inject/Provider;

    .line 473
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTamperFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideTamperFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTamperFeatureProvider:Ljavax/inject/Provider;

    .line 474
    invoke-static {}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;->create()Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionnFeatureNativeFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;

    .line 475
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderPointerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideUserInteractionnFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3}, Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideUserInteractionFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideUserInteractionFeatureProvider:Ljavax/inject/Provider;

    .line 476
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    .line 477
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_AllExceptDipper_ProvideCardReaderListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderListenerProvider:Ljavax/inject/Provider;

    .line 478
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    .line 479
    new-instance v2, Ldagger/internal/DelegateFactory;

    invoke-direct {v2}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    .line 480
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdaterFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    .line 481
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdateListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideFirmwareUpdateListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;

    .line 482
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_magSwipeFailureFilter;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_magSwipeFailureFilter;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    .line 483
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->magSwipeFailureFilterProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentProcessorFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    .line 484
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentListenerProvider:Ljavax/inject/Provider;

    .line 485
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideNfcListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideNfcListenerProvider:Ljavax/inject/Provider;

    .line 486
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentCompletionListenerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvidePaymentCompletionListenerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentCompletionListenerProvider:Ljavax/inject/Provider;

    .line 487
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideMinesweeper;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_provideMinesweeper;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideMinesweeperProvider:Ljavax/inject/Provider;

    .line 488
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Lcom/squareup/cardreader/LcrModule_ProvideResFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideResFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideResProvider:Ljavax/inject/Provider;

    .line 489
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideMinesweeperProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideResProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionRevocationStateProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4}, Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideSecureSessionRevocationFeatureFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    .line 490
    new-instance v2, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bluetoothUtils;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_bluetoothUtils;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 491
    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->lcrExecutorProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCoreDumpFeatureProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventLogFeatureProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->firmwareUpdateFeatureLegacyProvider:Ljavax/inject/Provider;

    iget-object v9, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentFeatureProvider:Ljavax/inject/Provider;

    iget-object v10, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePowerFeatureProvider:Ljavax/inject/Provider;

    iget-object v11, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionFeatureProvider:Ljavax/inject/Provider;

    iget-object v12, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureTouchCardReaderFeatureProvider:Ljavax/inject/Provider;

    iget-object v13, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSystemFeatureProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideTamperFeatureProvider:Ljavax/inject/Provider;

    iget-object v15, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideUserInteractionFeatureProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdateListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideNfcListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentCompletionListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v20, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSecureSessionRevocationFeatureProvider:Ljavax/inject/Provider;

    move-object/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v23, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    move-object/from16 v25, v1

    invoke-static/range {v3 .. v25}, Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_Prod_ProvideCardReaderSwigFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v2, v1}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 492
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v6, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v7, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    iget-object v8, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    invoke-static/range {v2 .. v8}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderInitializerFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 493
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInitializerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFirmwareUpdaterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->providePaymentProcessorProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LocalCardReaderModule_ProvideLocalCardReaderFactory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    .line 494
    new-instance v1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderPauseAndResumer;

    move-object/from16 v2, p2

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_cardReaderPauseAndResumer;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    .line 495
    new-instance v1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badBus;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badBus;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->badBusProvider:Ljavax/inject/Provider;

    .line 496
    new-instance v1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_audioManager;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_audioManager;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->audioManagerProvider:Ljavax/inject/Provider;

    .line 497
    new-instance v1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_telephoneManager;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_telephoneManager;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->telephoneManagerProvider:Ljavax/inject/Provider;

    .line 498
    new-instance v1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_badEventSink;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->badEventSinkProvider:Ljavax/inject/Provider;

    .line 499
    new-instance v1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_swipeBus;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_swipeBus;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->swipeBusProvider:Ljavax/inject/Provider;

    .line 500
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSampleFeederProvider:Ljavax/inject/Provider;

    .line 501
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;->create()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideO1SignalDecoderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideO1SignalDecoderProvider:Ljavax/inject/Provider;

    .line 502
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideFastSqLinkSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideFastSqLinkSignalDecoderFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFastSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    .line 503
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideFastSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4FastSignalDecoderFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideR4FastSignalDecoderProvider:Ljavax/inject/Provider;

    .line 504
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSlowSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    .line 505
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSlowSqLinkSignalDecoderProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4SlowSignalDecoderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideR4SlowSignalDecoderFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideR4SlowSignalDecoderProvider:Ljavax/inject/Provider;

    .line 506
    invoke-static {}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;->create()Lcom/squareup/wavpool/swipe/DecoderModule_ProvideGen2SignalDecoderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideGen2SignalDecoderProvider:Ljavax/inject/Provider;

    .line 507
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideO1SignalDecoderProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideR4FastSignalDecoderProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideR4SlowSignalDecoderProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideGen2SignalDecoderProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideClassifyingDecoderFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideClassifyingDecoderProvider:Ljavax/inject/Provider;

    .line 508
    iget-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideReaderTypeProviderFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_ProvideReaderTypeProviderFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideReaderTypeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private initialize2(Lcom/squareup/cardreader/CardReaderModule;Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V
    .locals 13

    .line 514
    iget-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->applicationProvider:Ljavax/inject/Provider;

    invoke-static {p1}, Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidHeadsetConnectionListener_Factory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    .line 515
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->badEventSinkProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->swipeBusProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSampleFeederProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideClassifyingDecoderProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/wavpool/swipe/AsyncDecoderModule_ProvideDecoderExecutorFactory;->create()Lcom/squareup/wavpool/swipe/AsyncDecoderModule_ProvideDecoderExecutorFactory;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideEventDataForwarderProvider:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->swipeEventLoggerProvider:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object v11, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideReaderTypeProvider:Ljavax/inject/Provider;

    iget-object v12, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v12}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideSquarewaveDecoderFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSquarewaveDecoderProvider:Ljavax/inject/Provider;

    .line 516
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->crashnadoProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideInputSampleRateProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;->create()Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->mainThreadProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->telephoneManagerProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->badEventSinkProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideSquarewaveDecoderProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->androidHeadsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v7}, Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioRecordingModule_ProvideMicRecorderFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideMicRecorderProvider:Ljavax/inject/Provider;

    .line 517
    new-instance p1, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_libraryLoader;

    invoke-direct {p1, p2}, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent$com_squareup_cardreader_NonX2CardReaderContextParent_libraryLoader;-><init>(Lcom/squareup/cardreader/NonX2CardReaderContextParent;)V

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->libraryLoaderProvider:Ljavax/inject/Provider;

    .line 518
    iget-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/android/util/ClockModule_ProvideClockFactory;->create()Lcom/squareup/android/util/ClockModule_ProvideClockFactory;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderConstantsProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0}, Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/R6CardReaderAwakener_Factory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->r6CardReaderAwakenerProvider:Ljavax/inject/Provider;

    .line 519
    invoke-static {}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;->create()Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAndroidDeviceParamsFactory;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidDeviceParamsModule_ProvideAudioVolumeFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioVolumeProvider:Ljavax/inject/Provider;

    .line 520
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->badBusProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->runningProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->audioManagerProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideHeadsetConnectionStateProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideMicRecorderProvider:Ljavax/inject/Provider;

    iget-object v6, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->libraryLoaderProvider:Ljavax/inject/Provider;

    iget-object v7, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioBackendProvider:Ljavax/inject/Provider;

    iget-object v8, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->r6CardReaderAwakenerProvider:Ljavax/inject/Provider;

    iget-object v9, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioVolumeProvider:Ljavax/inject/Provider;

    iget-object v10, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-static/range {v0 .. v10}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioStartAndStopperFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioStartAndStopperProvider:Ljavax/inject/Provider;

    .line 521
    iget-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioStartAndStopperProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2}, Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AndroidAudioPlaybackModule_ProvideAudioGraphInitiailizerFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioGraphInitiailizerProvider:Ljavax/inject/Provider;

    .line 522
    iget-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderIdProvider:Ljavax/inject/Provider;

    iget-object p2, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideLocalCardReaderProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderInfoProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderSwigProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideAudioGraphInitiailizerProvider:Ljavax/inject/Provider;

    invoke-static {p1, p2, v0, v1, v2}, Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderModule_ProvideCardReaderContextFactory;

    move-result-object p1

    invoke-static {p1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public cardReaderContext()Lcom/squareup/cardreader/CardReaderContext;
    .locals 1

    .line 527
    iget-object v0, p0, Lcom/squareup/cardreader/audio/DaggerAudioCardReaderContextComponent;->provideCardReaderContextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderContext;

    return-object v0
.end method
