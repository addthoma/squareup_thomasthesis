.class public Lcom/squareup/cardreader/UserInteractionMessageFormatter;
.super Ljava/lang/Object;
.source "UserInteractionMessageFormatter.java"


# static fields
.field private static final EMPTY_STRING:Ljava/lang/String; = ""


# instance fields
.field private final height:I

.field private final width:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput p1, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->width:I

    .line 18
    iput p2, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->height:I

    return-void
.end method

.method private static centerContent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V
    .locals 1

    .line 56
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p2, :cond_0

    const/4 v0, 0x0

    .line 58
    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    sub-int/2addr p2, v0

    .line 61
    div-int/lit8 v0, p2, 0x2

    sub-int/2addr p2, v0

    .line 63
    invoke-static {p0, v0}, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->pad(Ljava/lang/StringBuilder;I)V

    .line 64
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-static {p0, p2}, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->pad(Ljava/lang/StringBuilder;I)V

    :goto_0
    return-void
.end method

.method private static pad(Ljava/lang/StringBuilder;I)V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    const/16 v1, 0x20

    .line 77
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public format(Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;)Ljava/lang/String;
    .locals 5

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->width:I

    iget v2, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->height:I

    mul-int v1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 32
    iget v1, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->height:I

    .line 33
    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 34
    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->title:Ljava/lang/String;

    iget v3, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->width:I

    invoke-static {v0, v2, v3}, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->centerContent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    add-int/lit8 v1, v1, -0x1

    .line 37
    :cond_1
    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v1, :cond_2

    .line 40
    iget v3, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->width:I

    invoke-static {v0, v3}, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->pad(Ljava/lang/StringBuilder;I)V

    :cond_2
    const/4 v3, 0x0

    .line 42
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    :goto_0
    if-ge v3, v1, :cond_3

    .line 43
    iget-object v2, p1, Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;->lines:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget v4, p0, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->width:I

    invoke-static {v0, v2, v4}, Lcom/squareup/cardreader/UserInteractionMessageFormatter;->centerContent(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 45
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
