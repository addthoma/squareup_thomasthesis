.class public final Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
.super Ljava/lang/Object;
.source "ReaderWarningParameters.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field public final defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

.field public final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final importantMessageId:I

.field public final localizedMessage:Ljava/lang/String;

.field public final localizedTitle:Ljava/lang/String;

.field public final messageId:I

.field public final secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

.field public final titleId:I

.field public final url:Ljava/lang/String;

.field public final userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

.field public final vectorId:Ljava/lang/Integer;

.field public final warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 93
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$1;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$1;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$000(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->vectorId:Ljava/lang/Integer;

    .line 37
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$100(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 38
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$200(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    .line 39
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$300(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    .line 40
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$400(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$500(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    .line 42
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$600(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    .line 43
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$700(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    .line 44
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$800(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    .line 45
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$900(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    .line 46
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$1000(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->url:Ljava/lang/String;

    .line 47
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$1100(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 48
    invoke-static {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->access$1200(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$1;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;-><init>(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto/16 :goto_1

    .line 54
    :cond_1
    check-cast p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    .line 55
    iget v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    iget v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    iget v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    iget v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->vectorId:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->vectorId:Ljava/lang/Integer;

    .line 58
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    .line 60
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    .line 61
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    .line 62
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    .line 63
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    .line 64
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->url:Ljava/lang/String;

    .line 65
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    iget-object v3, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    iget-object p1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    .line 67
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    .line 71
    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->vectorId:Ljava/lang/Integer;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    .line 72
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->url:Ljava/lang/String;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    .line 71
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 81
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-virtual {p2}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->url:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 85
    iget p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->userInteractionMessage:Lcom/squareup/cardreader/CardReaderDispatch$UserInteractionMessage;

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 89
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-eqz p2, :cond_0

    iget p2, p2, Lcom/squareup/cardreader/CardReaderId;->id:I

    goto :goto_0

    :cond_0
    const/4 p2, -0x1

    :goto_0
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget-object p2, p0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ordinal()I

    move-result v0

    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
