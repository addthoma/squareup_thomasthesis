.class public final enum Lcom/squareup/cardreader/ui/api/ReaderWarningType;
.super Ljava/lang/Enum;
.source "ReaderWarningType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ui/api/ReaderWarningType;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum DIP_REQUIRED_DURING_FALLBACK_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum DIP_REQUIRED_ROOT_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum FALLBACK_SCHEME:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum FALLBACK_TECHNICAL:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum FIRMWARE_UPDATE_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum LIBRARY_LOAD_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum NFC_ENABLED_WARNING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum PAYMENT_DECLINED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum POST_REBOOTING_FWUP_DISCONNECT:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum R12_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum R12_UPDATE_BLOCKING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum R6_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum SECURE_SESSION_FAILED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum TALKBACK_ENABLED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum TAMPER_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

.field public static final enum UPDATE_REGISTER:Lcom/squareup/cardreader/ui/api/ReaderWarningType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 9
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v1, 0x0

    const-string v2, "GENERIC_ERROR"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 10
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v2, 0x1

    const-string v3, "DEVICE_UNSUPPORTED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 11
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v3, 0x2

    const-string v4, "DIP_REQUIRED_ROOT_SCREEN"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DIP_REQUIRED_ROOT_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 12
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v4, 0x3

    const-string v5, "DIP_REQUIRED_DURING_FALLBACK_SCREEN"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DIP_REQUIRED_DURING_FALLBACK_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v5, 0x4

    const-string v6, "FALLBACK_SCHEME"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FALLBACK_SCHEME:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v6, 0x5

    const-string v7, "FALLBACK_TECHNICAL"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FALLBACK_TECHNICAL:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v7, 0x6

    const-string v8, "FIRMWARE_UPDATE_ERROR"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FIRMWARE_UPDATE_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/4 v8, 0x7

    const-string v9, "LIBRARY_LOAD_ERROR"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->LIBRARY_LOAD_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v9, 0x8

    const-string v10, "NFC_ENABLED_WARNING"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->NFC_ENABLED_WARNING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v10, 0x9

    const-string v11, "PAYMENT_CANCELED"

    invoke-direct {v0, v11, v10}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v11, 0xa

    const-string v12, "PAYMENT_DECLINED"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_DECLINED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v12, 0xb

    const-string v13, "POST_REBOOTING_FWUP_DISCONNECT"

    invoke-direct {v0, v13, v12}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->POST_REBOOTING_FWUP_DISCONNECT:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v13, 0xc

    const-string v14, "R6_LOW_BATTERY"

    invoke-direct {v0, v14, v13}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R6_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v14, 0xd

    const-string v15, "R12_LOW_BATTERY"

    invoke-direct {v0, v15, v14}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R12_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v15, 0xe

    const-string v14, "R12_UPDATE_BLOCKING"

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R12_UPDATE_BLOCKING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const-string v14, "SECURE_SESSION_FAILED"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->SECURE_SESSION_FAILED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const-string v14, "TALKBACK_ENABLED"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->TALKBACK_ENABLED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const-string v14, "TAMPER_ERROR"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->TAMPER_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 27
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const-string v14, "UPDATE_REGISTER"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/cardreader/ui/api/ReaderWarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->UPDATE_REGISTER:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 7
    sget-object v14, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->GENERIC_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DEVICE_UNSUPPORTED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DIP_REQUIRED_ROOT_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->DIP_REQUIRED_DURING_FALLBACK_SCREEN:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FALLBACK_SCHEME:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FALLBACK_TECHNICAL:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->FIRMWARE_UPDATE_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->LIBRARY_LOAD_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->NFC_ENABLED_WARNING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_CANCELED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->PAYMENT_DECLINED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->POST_REBOOTING_FWUP_DISCONNECT:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R6_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R12_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R12_UPDATE_BLOCKING:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->SECURE_SESSION_FAILED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->TALKBACK_ENABLED:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->TAMPER_ERROR:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->UPDATE_REGISTER:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->$VALUES:[Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningType;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ui/api/ReaderWarningType;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->$VALUES:[Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ui/api/ReaderWarningType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    return-object v0
.end method
