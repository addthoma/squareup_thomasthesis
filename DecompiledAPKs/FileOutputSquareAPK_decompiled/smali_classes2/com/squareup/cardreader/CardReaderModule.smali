.class public Lcom/squareup/cardreader/CardReaderModule;
.super Ljava/lang/Object;
.source "CardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderModule$AllExceptDipper;,
        Lcom/squareup/cardreader/CardReaderModule$Prod;
    }
.end annotation


# instance fields
.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderId;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderModule;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-void
.end method

.method static provideCardReaderContext(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;)Lcom/squareup/cardreader/CardReaderContext;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 129
    new-instance v6, Lcom/squareup/cardreader/CardReaderContext;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/CardReaderContext;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReader;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderDispatch;Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;)V

    return-object v6
.end method

.method static provideCardReaderInfo(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 68
    new-instance v0, Lcom/squareup/cardreader/CardReaderInfo;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderInfo;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static provideCardReaderInitializer(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/FirmwareUpdater;Ljavax/inject/Provider;)Lcom/squareup/cardreader/CardReaderInitializer;
    .locals 9
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;)",
            "Lcom/squareup/cardreader/CardReaderInitializer;"
        }
    .end annotation

    .line 76
    new-instance v8, Lcom/squareup/cardreader/CardReaderInitializer;

    move-object v0, v8

    move-object v1, p1

    move-object v2, p0

    move-object v3, p3

    move-object v4, p6

    move-object v5, p4

    move-object v6, p2

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/CardReaderInitializer;-><init>(Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/FirmwareUpdater;)V

    return-object v8
.end method

.method static provideFirmwareUpdateListener(Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 98
    invoke-virtual {p0}, Lcom/squareup/cardreader/FirmwareUpdater;->getInternalListener()Lcom/squareup/cardreader/FirmwareUpdater$InternalListener;

    move-result-object p0

    return-object p0
.end method

.method static provideFirmwareUpdater(Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/FirmwareUpdater;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReader;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/FirmwareUpdater;"
        }
    .end annotation

    .line 92
    new-instance v6, Lcom/squareup/cardreader/FirmwareUpdater;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/FirmwareUpdater;-><init>(Lcom/squareup/cardreader/CardReaderListeners;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/thread/executor/MainThread;)V

    return-object v6
.end method

.method static provideNfcListener(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderDispatch$NfcPaymentListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 116
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentProcessor;->getInternalListener()Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

    move-result-object p0

    return-object p0
.end method

.method static providePaymentCompletionListener(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderDispatch$CompletedPaymentListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 122
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentProcessor;->getInternalListener()Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

    move-result-object p0

    return-object p0
.end method

.method static providePaymentListener(Lcom/squareup/cardreader/PaymentProcessor;)Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 111
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentProcessor;->getInternalListener()Lcom/squareup/cardreader/PaymentProcessor$InternalListenerReader;

    move-result-object p0

    return-object p0
.end method

.method static providePaymentProcessor(Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/MagSwipeFailureFilter;Lcom/squareup/cardreader/FirmwareUpdater;)Lcom/squareup/cardreader/PaymentProcessor;
    .locals 7
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderDispatch;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/MagSwipeFailureFilter;",
            "Lcom/squareup/cardreader/FirmwareUpdater;",
            ")",
            "Lcom/squareup/cardreader/PaymentProcessor;"
        }
    .end annotation

    .line 105
    new-instance v6, Lcom/squareup/cardreader/PaymentProcessor;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/cardreader/PaymentProcessor;-><init>(Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/MagSwipeFailureFilter;Lcom/squareup/cardreader/FirmwareUpdater;)V

    return-object v6
.end method


# virtual methods
.method provideCardReaderId()Lcom/squareup/cardreader/CardReaderId;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderModule;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-object v0
.end method
