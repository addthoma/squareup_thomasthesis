.class public final Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;
.super Ljava/lang/Object;
.source "GlobalHeadsetModule_ProvideHeadsetFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/Headset;",
        ">;"
    }
.end annotation


# instance fields
.field private final headsetConnectionListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/GlobalHeadsetModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->headsetConnectionListenerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->headsetListenerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;-><init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideHeadset(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/Headset$Listener;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/Headset;"
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/GlobalHeadsetModule;->provideHeadset(Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/Headset;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/Headset;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->headsetConnectionListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;

    iget-object v2, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->headsetListenerProvider:Ljavax/inject/Provider;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->provideHeadset(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/wavpool/swipe/HeadsetConnectionListener;Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/Headset;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetFactory;->get()Lcom/squareup/wavpool/swipe/Headset;

    move-result-object v0

    return-object v0
.end method
