.class public final Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/FirmwareUpdateFeature;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\u0018\u00002\u00020\u00012\u00020\u0002B\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0008\u0010\u0013\u001a\u00020\u0010H\u0002J\u0010\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u001b\u0010\u0017\u001a\u00020\u00102\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019H\u0016\u00a2\u0006\u0002\u0010\u001bJ\u0008\u0010\u001c\u001a\u00020\u001dH\u0002J \u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0016J\u0008\u0010%\u001a\u00020\u001dH\u0002J\u0008\u0010&\u001a\u00020\u0010H\u0016J \u0010\'\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020 2\u0006\u0010)\u001a\u00020 2\u0006\u0010*\u001a\u00020 H\u0002J\u0010\u0010+\u001a\u00020\u00102\u0006\u0010#\u001a\u00020$H\u0016J\u0010\u0010,\u001a\u00020\u00102\u0006\u0010-\u001a\u00020$H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/FirmwareUpdateFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;",
        "getFeaturePointer",
        "()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;",
        "setFeaturePointer",
        "(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)V",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage;",
        "initialize",
        "onFirmwareUpdateTmsCountryChanges",
        "countryCode",
        "",
        "onVersionInfo",
        "firmwareVersions",
        "",
        "Lcom/squareup/cardreader/FirmwareAssetVersionInfo;",
        "([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V",
        "pauseUpdates",
        "Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;",
        "receivedManifest",
        "manifest",
        "",
        "requiredUpdate",
        "",
        "result",
        "",
        "requestManifest",
        "resetIfInitilized",
        "update",
        "header",
        "encryptedData",
        "blockIndexTableBytes",
        "updateComplete",
        "updateProgress",
        "percentComplete",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field public featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method private final initialize()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 34
    invoke-static {v0, p0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->firmware_update_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    move-result-object v0

    const-string v1, "FirmwareUpdateFeatureNat\u2026aderPointer(), this\n    )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    return-void
.end method

.method private final pauseUpdates()Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_stop_sending_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object v0

    const-string v1, "FirmwareUpdateFeatureNat\u2026ding_data(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final requestManifest()Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_get_manifest(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object v0

    const-string v1, "crFirmwareUpdateFeatureGetManifest"

    .line 49
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final update([B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 57
    :cond_0
    invoke-static {v0, p1, p2, p3}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->firmware_send_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;[B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    const-string p2, "FirmwareUpdateFeatureNat\u2026lockIndexTableBytes\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final getFeaturePointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Initialize;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Initialize;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->initialize()V

    goto :goto_0

    .line 27
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$RequestManifest;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$RequestManifest;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->requestManifest()Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    goto :goto_0

    .line 28
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Update;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Update;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Update;->getHeader()[B

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Update;->getEncryptedData()[B

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$Update;->getBlockIndexTableBytes()[B

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->update([B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    goto :goto_0

    .line 29
    :cond_2
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$PauseUpdates;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$FirmwareUpdateFeatureMessage$PauseUpdates;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->pauseUpdates()Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    :cond_3
    :goto_0
    return-void
.end method

.method public onFirmwareUpdateTmsCountryChanges(Ljava/lang/String;)V
    .locals 2

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnFirmwareUpdateTmsCountryChanges;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnFirmwareUpdateTmsCountryChanges;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
    .locals 2

    const-string v0, "firmwareVersions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnVersionInfo;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$OnVersionInfo;-><init>([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public receivedManifest([BZI)V
    .locals 2

    const-string v0, "manifest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$ReceivedManifest;-><init>([BZI)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 40
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-eqz v0, :cond_2

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    .line 42
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;->cr_firmware_update_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    :cond_2
    return-void
.end method

.method public final setFeaturePointer(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    return-void
.end method

.method public updateComplete(I)V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateComplete;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateComplete;-><init>(I)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public updateProgress(I)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateProgress;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$FirmwareUpdateFeatureOutput$UpdateProgress;-><init>(I)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method
