.class public Lcom/squareup/cardreader/UserInteractionFeatureLegacy;
.super Ljava/lang/Object;
.source "UserInteractionFeatureLegacy.java"


# instance fields
.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

.field private final userInteractionFeatureNative:Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 15
    iput-object p2, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeatureNative:Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public identify()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeatureNative:Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;->cr_user_interaction_identify_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    return-void
.end method

.method public initialize()V
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    if-nez v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeatureNative:Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 23
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v1

    .line 22
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;->user_interaction_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    return-void

    .line 20
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UserInteractionFeature is already initialized!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public resetUserInteractionFeature()V
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    if-eqz v0, :cond_0

    .line 28
    iget-object v1, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeatureNative:Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;->cr_user_interaction_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeatureNative:Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;->cr_user_interaction_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;

    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lcom/squareup/cardreader/UserInteractionFeatureLegacy;->userInteractionFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;

    :cond_0
    return-void
.end method
