.class public Lcom/squareup/cardreader/lcr/PaymentFeatureNative;
.super Ljava/lang/Object;
.source "PaymentFeatureNative.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/PaymentFeatureNativeConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_payment_cancel_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->cr_payment_cancel_payment(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_payment_enable_swipe_passthrough(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Z)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->cr_payment_enable_swipe_passthrough(JZ)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_payment_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->cr_payment_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_payment_request_card_presence(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->cr_payment_request_card_presence(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_payment_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->cr_payment_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;II)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;
    .locals 2

    .line 33
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_initialize(JLjava/lang/Object;II)J

    move-result-wide p0

    const-wide/16 p2, 0x0

    cmp-long v0, p0, p2

    if-nez v0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 34
    :cond_0
    new-instance p2, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    const/4 p3, 0x0

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;-><init>(JZ)V

    move-object p0, p2

    :goto_0
    return-object p0
.end method

.method public static payment_process_server_response(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 46
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_process_server_response(J[B)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_select_account_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 54
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->swigValue()I

    move-result p0

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_select_account_type(JI)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_select_application(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 42
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_select_application(J[B)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_send_powerup_hint(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;I)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 50
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_send_powerup_hint(JI)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_start_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;JIIIIIIII)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 12

    .line 38
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    move-wide v2, p1

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v11}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_start_payment(JJIIIIIIII)I

    move-result v0

    invoke-static {v0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    return-object v0
.end method

.method public static payment_tmn_cancel_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 70
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_tmn_cancel_request(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 62
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_tmn_send_bytes_to_reader(J[B)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_tmn_start_miryo(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 66
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_tmn_start_miryo(J[B)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_tmn_start_transaction(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 7

    .line 58
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnRequestType;->swigValue()I

    move-result v2

    invoke-virtual {p3}, Lcom/squareup/cardreader/lcr/CrsTmnBrandId;->swigValue()I

    move-result v4

    move-object v3, p2

    move-wide v5, p4

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_tmn_start_transaction(JILjava/lang/String;IJ)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method

.method public static payment_tmn_write_notify_ack(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 74
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeJNI;->payment_tmn_write_notify_ack(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p0

    return-object p0
.end method
