.class public Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;
.super Ljava/lang/Object;
.source "CrsStmPinPadButtonInfo.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 79
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->new_CrsStmPinPadButtonInfo()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;-><init>(JZ)V

    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    return-void
.end method

.method protected static getCPtr(Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)J
    .locals 2

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 21
    :cond_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 5

    monitor-enter p0

    .line 29
    :try_start_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->delete_CrsStmPinPadButtonInfo(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->delete()V

    return-void
.end method

.method public getButton_id()I
    .locals 2

    .line 43
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_button_id_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I

    move-result v0

    return v0
.end method

.method public getX_size()I
    .locals 2

    .line 67
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_x_size_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I

    move-result v0

    return v0
.end method

.method public getX_start()I
    .locals 2

    .line 51
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_x_start_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I

    move-result v0

    return v0
.end method

.method public getY_size()I
    .locals 2

    .line 75
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_y_size_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I

    move-result v0

    return v0
.end method

.method public getY_start()I
    .locals 2

    .line 59
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_y_start_get(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)I

    move-result v0

    return v0
.end method

.method public setButton_id(I)V
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_button_id_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V

    return-void
.end method

.method public setX_size(I)V
    .locals 2

    .line 63
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_x_size_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V

    return-void
.end method

.method public setX_start(I)V
    .locals 2

    .line 47
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_x_start_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V

    return-void
.end method

.method public setY_size(I)V
    .locals 2

    .line 71
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_y_size_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V

    return-void
.end method

.method public setY_start(I)V
    .locals 2

    .line 55
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmPinPadButtonInfo_y_start_set(JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;I)V

    return-void
.end method
