.class public Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeJNI;
.super Ljava/lang/Object;
.source "SecureSessionFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native CrSecureSessionResultError_error_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)I
.end method

.method public static final native CrSecureSessionResultError_error_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;I)V
.end method

.method public static final native CrSecureSessionResultError_localizedDescription_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)Ljava/lang/String;
.end method

.method public static final native CrSecureSessionResultError_localizedDescription_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;Ljava/lang/String;)V
.end method

.method public static final native CrSecureSessionResultError_localizedTitle_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)Ljava/lang/String;
.end method

.method public static final native CrSecureSessionResultError_localizedTitle_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;Ljava/lang/String;)V
.end method

.method public static final native CrSecureSessionResultError_result_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)I
.end method

.method public static final native CrSecureSessionResultError_result_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;I)V
.end method

.method public static final native CrSecureSessionResultError_uxHint_get(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;)I
.end method

.method public static final native CrSecureSessionResultError_uxHint_set(JLcom/squareup/cardreader/lcr/CrSecureSessionResultError;I)V
.end method

.method public static final native cr_securesession_feature_establish_session(J)I
.end method

.method public static final native cr_securesession_feature_free(J)I
.end method

.method public static final native cr_securesession_feature_notify_server_error(J)I
.end method

.method public static final native cr_securesession_feature_pin_bypass(J)I
.end method

.method public static final native cr_securesession_feature_term(J)I
.end method

.method public static final native new_CrSecureSessionResultError()J
.end method

.method public static final native pin_add_digit(JI)Z
.end method

.method public static final native pin_reset(J)V
.end method

.method public static final native pin_submit(J)I
.end method

.method public static final native securesession_initialize(JLjava/lang/Object;Ljava/lang/Object;)J
.end method

.method public static final native securesession_recv_server_message(J[B)J
.end method

.method public static final native set_kb(J[B)Z
.end method
