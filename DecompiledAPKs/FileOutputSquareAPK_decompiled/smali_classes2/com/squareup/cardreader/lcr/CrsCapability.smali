.class public final enum Lcom/squareup/cardreader/lcr/CrsCapability;
.super Ljava/lang/Enum;
.source "CrsCapability.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrsCapability$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrsCapability;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_CL_CARD_PRESENCE:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_EVENTLOG_NUM_PARAMS_ENCODING:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_FEATURE_FLAGS:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_K400_POWERUP_HINT:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_MAX_ENUM:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_PAYMENT_PROFILING:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_SECURESESSION_V4:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_SIGNED_MANIFESTS:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_SQUID_COMMS_ENCRYPTION:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_SQUID_COMMS_MASTER_KEY_ESTABLISHMENT:Lcom/squareup/cardreader/lcr/CrsCapability;

.field public static final enum CRS_CAPABILITY_SQUID_COMMS_SESSION_KEY_ESTABLISHMENT:Lcom/squareup/cardreader/lcr/CrsCapability;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "CRS_CAPABILITY_PAYMENT_PROFILING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_PAYMENT_PROFILING:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v3, 0x2

    const-string v4, "CRS_CAPABILITY_K400_POWERUP_HINT"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_K400_POWERUP_HINT:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v4, 0x3

    const-string v5, "CRS_CAPABILITY_SIGNED_MANIFESTS"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SIGNED_MANIFESTS:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v5, 0x4

    const-string v6, "CRS_CAPABILITY_SECURESESSION_V4"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SECURESESSION_V4:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v6, 0x5

    const-string v7, "CRS_CAPABILITY_FEATURE_FLAGS"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_FEATURE_FLAGS:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v7, 0x6

    const-string v8, "CRS_CAPABILITY_CL_CARD_PRESENCE"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_CL_CARD_PRESENCE:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/4 v8, 0x7

    const-string v9, "CRS_CAPABILITY_EVENTLOG_NUM_PARAMS_ENCODING"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_EVENTLOG_NUM_PARAMS_ENCODING:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/16 v9, 0x8

    const-string v10, "CRS_CAPABILITY_SQUID_COMMS_MASTER_KEY_ESTABLISHMENT"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SQUID_COMMS_MASTER_KEY_ESTABLISHMENT:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/16 v10, 0x9

    const-string v11, "CRS_CAPABILITY_SQUID_COMMS_SESSION_KEY_ESTABLISHMENT"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SQUID_COMMS_SESSION_KEY_ESTABLISHMENT:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const/16 v11, 0xa

    const-string v12, "CRS_CAPABILITY_SQUID_COMMS_ENCRYPTION"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SQUID_COMMS_ENCRYPTION:Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    const-string v12, "CRS_CAPABILITY_MAX_ENUM"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/lcr/CrsCapability;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_MAX_ENUM:Lcom/squareup/cardreader/lcr/CrsCapability;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 11
    sget-object v12, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_PAYMENT_PROFILING:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_K400_POWERUP_HINT:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SIGNED_MANIFESTS:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SECURESESSION_V4:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_FEATURE_FLAGS:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_CL_CARD_PRESENCE:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_EVENTLOG_NUM_PARAMS_ENCODING:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SQUID_COMMS_MASTER_KEY_ESTABLISHMENT:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SQUID_COMMS_SESSION_KEY_ESTABLISHMENT:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_SQUID_COMMS_ENCRYPTION:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsCapability;->CRS_CAPABILITY_MAX_ENUM:Lcom/squareup/cardreader/lcr/CrsCapability;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsCapability;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsCapability$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 46
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrsCapability$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrsCapability;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ")V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    .line 52
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsCapability$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsCapability;
    .locals 6

    .line 29
    const-class v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrsCapability;

    .line 30
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 31
    aget-object p0, v1, p0

    return-object p0

    .line 32
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 33
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 35
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrsCapability;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrsCapability;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrsCapability;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrsCapability;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsCapability;->$VALUES:[Lcom/squareup/cardreader/lcr/CrsCapability;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrsCapability;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrsCapability;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrsCapability;->swigValue:I

    return v0
.end method
