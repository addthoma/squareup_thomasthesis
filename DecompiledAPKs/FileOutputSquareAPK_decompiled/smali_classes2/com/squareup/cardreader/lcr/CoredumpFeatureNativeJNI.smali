.class public Lcom/squareup/cardreader/lcr/CoredumpFeatureNativeJNI;
.super Ljava/lang/Object;
.source "CoredumpFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native CR_COREDUMP_RESULT_SUCCESS_get()I
.end method

.method public static final native coredump_initialize(JLjava/lang/Object;)J
.end method

.method public static final native coredump_trigger_dump(J)V
.end method

.method public static final native cr_coredump_erase(J)I
.end method

.method public static final native cr_coredump_free(J)I
.end method

.method public static final native cr_coredump_get_data(J)I
.end method

.method public static final native cr_coredump_get_info(J)I
.end method

.method public static final native cr_coredump_term(J)I
.end method
