.class public Lcom/squareup/cardreader/lcr/SystemFeatureNativeJNI;
.super Ljava/lang/Object;
.source "SystemFeatureNativeJNI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native cr_system_free(J)I
.end method

.method public static final native cr_system_mark_feature_flags_ready_to_send(J)I
.end method

.method public static final native cr_system_read_system_info(J)I
.end method

.method public static final native cr_system_send_external_charging_state(JZ)I
.end method

.method public static final native cr_system_set_hardware_platform_feature(JI)I
.end method

.method public static final native cr_system_term(J)I
.end method

.method public static final native system_initialize(JLjava/lang/Object;)J
.end method

.method public static final native system_set_reader_feature_flag(JLjava/lang/String;S)I
.end method
