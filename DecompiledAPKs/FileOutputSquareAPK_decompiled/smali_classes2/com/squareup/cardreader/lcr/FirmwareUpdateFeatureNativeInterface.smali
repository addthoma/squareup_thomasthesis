.class public interface abstract Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_firmware_update_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
.end method

.method public abstract cr_firmware_update_feature_get_manifest(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
.end method

.method public abstract cr_firmware_update_feature_stop_sending_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
.end method

.method public abstract cr_firmware_update_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
.end method

.method public abstract firmware_send_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;[B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
.end method

.method public abstract firmware_update_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;
.end method
