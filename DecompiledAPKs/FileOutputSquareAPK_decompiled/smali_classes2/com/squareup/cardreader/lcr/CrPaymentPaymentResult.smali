.class public final enum Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;
.super Ljava/lang/Enum;
.source "CrPaymentPaymentResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_DECLINE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_REVERSE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE_WITH_SIGNATURE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_SCHEME_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_TECHNICAL_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_TERMINATED:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

.field public static final enum CR_PAYMENT_PAYMENT_RESULT_TIMED_OUT:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v1, 0x0

    const-string v2, "CR_PAYMENT_PAYMENT_RESULT_TERMINATED"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_TERMINATED:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v2, 0x1

    const-string v3, "CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v3, 0x2

    const-string v4, "CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE_WITH_SIGNATURE"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE_WITH_SIGNATURE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v4, 0x3

    const-string v5, "CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_DECLINE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_DECLINE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v5, 0x4

    const-string v6, "CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_REVERSE"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_REVERSE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v6, 0x5

    const-string v7, "CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v7, 0x6

    const-string v8, "CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_SCHEME_FALLBACK"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_SCHEME_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/4 v8, 0x7

    const-string v9, "CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_TECHNICAL_FALLBACK"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_TECHNICAL_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/16 v9, 0x8

    const-string v10, "CR_PAYMENT_PAYMENT_RESULT_TIMED_OUT"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_TIMED_OUT:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 11
    sget-object v10, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_TERMINATED:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_ICC_APPROVE_WITH_SIGNATURE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_DECLINE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_FAILURE_ICC_REVERSE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_SCHEME_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_SUCCESS_MAGSWIPE_TECHNICAL_FALLBACK:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->CR_PAYMENT_PAYMENT_RESULT_TIMED_OUT:Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 44
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrPaymentPaymentResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;",
            ")V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    .line 50
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;
    .locals 6

    .line 27
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    .line 28
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 29
    aget-object p0, v1, p0

    return-object p0

    .line 30
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 31
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 33
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 23
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigValue:I

    return v0
.end method
