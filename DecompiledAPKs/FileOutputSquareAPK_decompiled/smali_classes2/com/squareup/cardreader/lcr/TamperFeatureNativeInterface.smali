.class public interface abstract Lcom/squareup/cardreader/lcr/TamperFeatureNativeInterface;
.super Ljava/lang/Object;
.source "TamperFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_tamper_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
.end method

.method public abstract cr_tamper_get_tamper_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
.end method

.method public abstract cr_tamper_get_tamper_status(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
.end method

.method public abstract cr_tamper_init(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_event_api_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
.end method

.method public abstract cr_tamper_reset_tag(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
.end method

.method public abstract cr_tamper_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;)Lcom/squareup/cardreader/lcr/CrTamperResult;
.end method

.method public abstract tamper_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_tamper_t;
.end method
