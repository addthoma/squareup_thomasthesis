.class public Lcom/squareup/cardreader/lcr/AudioBackendNativePassthrough;
.super Ljava/lang/Object;
.source "AudioBackendNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;
    .locals 1

    .line 9
    invoke-static {}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    move-result-object v0

    return-object v0
.end method

.method public cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 0

    .line 32
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
    .locals 0

    .line 15
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    move-result-object p1

    return-object p1
.end method

.method public cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 0

    .line 26
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 0

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    return-void
.end method

.method public decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;
    .locals 0

    .line 51
    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V
    .locals 0

    .line 45
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V

    return-void
.end method

.method public initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 0

    .line 39
    invoke-static {p1, p2, p3, p4, p5}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object p1

    return-object p1
.end method

.method public set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V
    .locals 0

    .line 57
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V

    return-void
.end method
