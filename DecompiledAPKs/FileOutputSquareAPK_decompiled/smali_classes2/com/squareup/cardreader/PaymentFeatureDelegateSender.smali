.class public final Lcom/squareup/cardreader/PaymentFeatureDelegateSender;
.super Ljava/lang/Object;
.source "PaymentFeatureDelegateSender.kt"

# interfaces
.implements Lcom/squareup/cardreader/SendsToPos;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/cardreader/CardReaderScope;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/cardreader/SendsToPos<",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "realCardReaderListeners",
        "Lcom/squareup/cardreader/RealCardReaderListeners;",
        "cardReaderId",
        "Lcom/squareup/cardreader/CardReaderId;",
        "(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderId;)V",
        "sendResponseToPos",
        "",
        "message",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReaderId:Lcom/squareup/cardreader/CardReaderId;

.field private final realCardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderId;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "realCardReaderListeners"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;->realCardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    iput-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    return-void
.end method


# virtual methods
.method public sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;)V
    .locals 3

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;->realCardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    .line 17
    new-instance v1, Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;

    iget-object v2, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-direct {v1, v2, p1}, Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;)V

    .line 16
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishPaymentFeatureMessage(Lcom/squareup/cardreader/PaymentFeatureMessageWrapper;)V

    return-void
.end method

.method public bridge synthetic sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;)V

    return-void
.end method
