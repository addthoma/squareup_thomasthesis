.class public final Lcom/squareup/cardreader/PaymentCounter_Factory;
.super Ljava/lang/Object;
.source "PaymentCounter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/PaymentCounter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentCounter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/PaymentCounter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;)",
            "Lcom/squareup/cardreader/PaymentCounter_Factory;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/cardreader/PaymentCounter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/PaymentCounter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/PaymentCounter;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/cardreader/PaymentCounter;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/PaymentCounter;-><init>(Lcom/squareup/cardreader/CardReaderHub;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/PaymentCounter;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentCounter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub;

    invoke-static {v0}, Lcom/squareup/cardreader/PaymentCounter_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderHub;)Lcom/squareup/cardreader/PaymentCounter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentCounter_Factory;->get()Lcom/squareup/cardreader/PaymentCounter;

    move-result-object v0

    return-object v0
.end method
