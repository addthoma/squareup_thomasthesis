.class public final Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;
.super Ljava/lang/Object;
.source "RelinkerLibraryLoaderModule_ProvideCrashnadoFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crashnado/Crashnado;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final crashReporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/CrashnadoReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/CrashnadoReporter;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->crashReporterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crashnado/CrashnadoReporter;",
            ">;)",
            "Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCrashnado(Landroid/app/Application;Lcom/squareup/crashnado/CrashnadoReporter;)Lcom/squareup/crashnado/Crashnado;
    .locals 0

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule;->provideCrashnado(Landroid/app/Application;Lcom/squareup/crashnado/CrashnadoReporter;)Lcom/squareup/crashnado/Crashnado;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/crashnado/Crashnado;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/crashnado/Crashnado;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->crashReporterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crashnado/CrashnadoReporter;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->provideCrashnado(Landroid/app/Application;Lcom/squareup/crashnado/CrashnadoReporter;)Lcom/squareup/crashnado/Crashnado;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoaderModule_ProvideCrashnadoFactory;->get()Lcom/squareup/crashnado/Crashnado;

    move-result-object v0

    return-object v0
.end method
