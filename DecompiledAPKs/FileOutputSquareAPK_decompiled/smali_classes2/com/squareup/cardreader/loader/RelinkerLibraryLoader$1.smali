.class Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;
.super Ljava/lang/Object;
.source "RelinkerLibraryLoader.java"

# interfaces
.implements Lcom/getkeepsafe/relinker/ReLinker$LoadListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->load()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;->this$0:Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V
    .locals 0

    .line 36
    invoke-static {p0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->access$300(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V

    return-void
.end method


# virtual methods
.method public failure(Ljava/lang/Throwable;)V
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;->this$0:Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;

    invoke-static {v0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->access$000(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 42
    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "Error loading library: libregister.so\n%s"

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;->logNativeLibraryLoadEvent(Ljava/lang/String;)V

    const-string v0, "Error loading library: libregister.so"

    .line 43
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;->this$0:Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;

    invoke-static {p1}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->access$100(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V

    return-void
.end method

.method public synthetic lambda$success$1$RelinkerLibraryLoader$1()V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;->this$0:Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;

    invoke-static {v0}, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;->access$200(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)Lcom/squareup/crashnado/Crashnado;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;->this$0:Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;

    new-instance v2, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$1$2Rjk2GwpfBfYMXuH_bN9L1ZEP8E;

    invoke-direct {v2, v1}, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$1$2Rjk2GwpfBfYMXuH_bN9L1ZEP8E;-><init>(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader;)V

    invoke-interface {v0, v2}, Lcom/squareup/crashnado/Crashnado;->install(Lcom/squareup/crashnado/Crashnado$CrashnadoListener;)V

    return-void
.end method

.method public success()V
    .locals 3

    .line 36
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$1$aq0Qjg7jpRs-3cVPsT5Ux9weDrE;

    invoke-direct {v1, p0}, Lcom/squareup/cardreader/loader/-$$Lambda$RelinkerLibraryLoader$1$aq0Qjg7jpRs-3cVPsT5Ux9weDrE;-><init>(Lcom/squareup/cardreader/loader/RelinkerLibraryLoader$1;)V

    const-string v2, "Sq-crashnado-install-relinker"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
