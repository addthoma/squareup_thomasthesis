.class public interface abstract Lcom/squareup/cardreader/loader/LibraryLoader;
.super Ljava/lang/Object;
.source "LibraryLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/loader/LibraryLoader$NativeLibraryLogger;,
        Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;
    }
.end annotation


# virtual methods
.method public abstract addLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V
.end method

.method public abstract hasLoadError()Z
.end method

.method public abstract isLoaded()Z
.end method

.method public abstract load()V
.end method

.method public abstract removeLibraryLoadedListener(Lcom/squareup/cardreader/loader/LibraryLoader$LibraryLoaderListener;)V
.end method
