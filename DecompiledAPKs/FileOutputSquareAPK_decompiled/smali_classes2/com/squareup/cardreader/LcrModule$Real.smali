.class public abstract Lcom/squareup/cardreader/LcrModule$Real;
.super Ljava/lang/Object;
.source "LcrModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/LcrModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Real"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCardReaderLoggerInterface(Lcom/squareup/cardreader/lcr/LogNativeInterface;)Lcom/squareup/cardreader/CardReaderLogBridge;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 90
    new-instance v0, Lcom/squareup/cardreader/CardReaderLogBridge$CardReaderNativeLogBridge;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/CardReaderLogBridge$CardReaderNativeLogBridge;-><init>(Lcom/squareup/cardreader/lcr/LogNativeInterface;)V

    return-object v0
.end method

.method static provideTimerApi(Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/lcr/TimerNativeInterface;)Lcom/squareup/cardreader/TimerApiLegacy;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 85
    new-instance v0, Lcom/squareup/cardreader/TimerApiLegacy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/TimerApiLegacy;-><init>(Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/lcr/TimerNativeInterface;)V

    return-object v0
.end method


# virtual methods
.method abstract provideCardReaderConstants(Lcom/squareup/cardreader/NativeCardReaderConstants;)Lcom/squareup/cardreader/CardReaderConstants;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
