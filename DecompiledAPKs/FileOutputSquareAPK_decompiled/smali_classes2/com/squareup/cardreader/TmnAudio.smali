.class public final enum Lcom/squareup/cardreader/TmnAudio;
.super Ljava/lang/Enum;
.source "TmnAudio.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/TmnAudio;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/cardreader/TmnAudio;",
        "",
        "(Ljava/lang/String;I)V",
        "TMN_AUDIO_QP_SUCCESS",
        "TMN_AUDIO_ID_SUCCESS",
        "TMN_AUDIO_SUICA_SUCCESS",
        "TMN_AUDIO_SUICA_SUCCESS_2",
        "TMN_AUDIO_FAILURE",
        "TMN_AUDIO_RE_TOUCH_LOOP",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/TmnAudio;

.field public static final enum TMN_AUDIO_FAILURE:Lcom/squareup/cardreader/TmnAudio;

.field public static final enum TMN_AUDIO_ID_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

.field public static final enum TMN_AUDIO_QP_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

.field public static final enum TMN_AUDIO_RE_TOUCH_LOOP:Lcom/squareup/cardreader/TmnAudio;

.field public static final enum TMN_AUDIO_SUICA_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

.field public static final enum TMN_AUDIO_SUICA_SUCCESS_2:Lcom/squareup/cardreader/TmnAudio;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/cardreader/TmnAudio;

    new-instance v1, Lcom/squareup/cardreader/TmnAudio;

    const/4 v2, 0x0

    const-string v3, "TMN_AUDIO_QP_SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnAudio;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_QP_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnAudio;

    const/4 v2, 0x1

    const-string v3, "TMN_AUDIO_ID_SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnAudio;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_ID_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnAudio;

    const/4 v2, 0x2

    const-string v3, "TMN_AUDIO_SUICA_SUCCESS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnAudio;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_SUICA_SUCCESS:Lcom/squareup/cardreader/TmnAudio;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnAudio;

    const/4 v2, 0x3

    const-string v3, "TMN_AUDIO_SUICA_SUCCESS_2"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnAudio;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_SUICA_SUCCESS_2:Lcom/squareup/cardreader/TmnAudio;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnAudio;

    const/4 v2, 0x4

    const-string v3, "TMN_AUDIO_FAILURE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnAudio;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_FAILURE:Lcom/squareup/cardreader/TmnAudio;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/TmnAudio;

    const/4 v2, 0x5

    const-string v3, "TMN_AUDIO_RE_TOUCH_LOOP"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/TmnAudio;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/TmnAudio;->TMN_AUDIO_RE_TOUCH_LOOP:Lcom/squareup/cardreader/TmnAudio;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/TmnAudio;->$VALUES:[Lcom/squareup/cardreader/TmnAudio;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/TmnAudio;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/TmnAudio;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TmnAudio;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/TmnAudio;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/TmnAudio;->$VALUES:[Lcom/squareup/cardreader/TmnAudio;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/TmnAudio;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/TmnAudio;

    return-object v0
.end method
