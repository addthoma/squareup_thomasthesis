.class final enum Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;
.super Ljava/lang/Enum;
.source "SecureTouchFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureTouchFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "SpeTouchDriverRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

.field public static final enum DISABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

.field public static final enum ENABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

.field public static final enum NONE:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 114
    new-instance v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->NONE:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    new-instance v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    const/4 v2, 0x1

    const-string v3, "ENABLE_TOUCH_DRIVER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->ENABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    new-instance v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    const/4 v3, 0x2

    const-string v4, "DISABLE_TOUCH_DRIVER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->DISABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    sget-object v4, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->NONE:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->ENABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->DISABLE_TOUCH_DRIVER:Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->$VALUES:[Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 114
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;
    .locals 1

    .line 114
    const-class v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;
    .locals 1

    .line 114
    sget-object v0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->$VALUES:[Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SpeTouchDriverRequest;

    return-object v0
.end method
