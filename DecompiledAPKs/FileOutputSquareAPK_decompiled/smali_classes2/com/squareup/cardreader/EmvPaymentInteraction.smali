.class public final Lcom/squareup/cardreader/EmvPaymentInteraction;
.super Lcom/squareup/cardreader/PaymentInteraction;
.source "PaymentInteraction.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0014\u0010\u0006\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/cardreader/EmvPaymentInteraction;",
        "Lcom/squareup/cardreader/PaymentInteraction;",
        "transactionType",
        "Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;",
        "currentTimeMillis",
        "",
        "amountAuthorized",
        "(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)V",
        "getAmountAuthorized",
        "()J",
        "getCurrentTimeMillis",
        "getTransactionType",
        "()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;


# instance fields
.field private final amountAuthorized:J

.field private final currentTimeMillis:J

.field private final transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/EmvPaymentInteraction;->Companion:Lcom/squareup/cardreader/EmvPaymentInteraction$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)V
    .locals 1

    const-string v0, "transactionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, p4, p5, v0}, Lcom/squareup/cardreader/PaymentInteraction;-><init>(JLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    iput-wide p2, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    iput-wide p4, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->amountAuthorized:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/EmvPaymentInteraction;Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJILjava/lang/Object;)Lcom/squareup/cardreader/EmvPaymentInteraction;
    .locals 2

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-wide p2, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    :cond_1
    move-wide v0, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide p4

    :cond_2
    move-wide p6, p4

    move-object p2, p0

    move-object p3, p1

    move-wide p4, v0

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/cardreader/EmvPaymentInteraction;->copy(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)Lcom/squareup/cardreader/EmvPaymentInteraction;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object v0
.end method

.method public final component2()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    return-wide v0
.end method

.method public final component3()J
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v0

    return-wide v0
.end method

.method public final copy(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)Lcom/squareup/cardreader/EmvPaymentInteraction;
    .locals 7

    const-string v0, "transactionType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/EmvPaymentInteraction;

    move-object v1, v0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/EmvPaymentInteraction;-><init>(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/EmvPaymentInteraction;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/EmvPaymentInteraction;

    iget-object v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    iget-object v1, p1, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    iget-wide v2, p1, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAmountAuthorized()J
    .locals 2

    .line 18
    iget-wide v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->amountAuthorized:J

    return-wide v0
.end method

.method public final getCurrentTimeMillis()J
    .locals 2

    .line 17
    iget-wide v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    return-wide v0
.end method

.method public final getTransactionType()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v1

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EmvPaymentInteraction(transactionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currentTimeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/EmvPaymentInteraction;->currentTimeMillis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", amountAuthorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
