.class public final Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;
.super Ljava/lang/Object;
.source "LcrModule_Real_ProvideTimerApiFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/TimerApiLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final lcrExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field private final timerNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TimerNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TimerNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->timerNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/TimerNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTimerApi(Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/lcr/TimerNativeInterface;)Lcom/squareup/cardreader/TimerApiLegacy;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/cardreader/LcrModule$Real;->provideTimerApi(Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/lcr/TimerNativeInterface;)Lcom/squareup/cardreader/TimerApiLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/TimerApiLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/TimerApiLegacy;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->lcrExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->timerNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/lcr/TimerNativeInterface;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->provideTimerApi(Ljava/util/concurrent/ExecutorService;Lcom/squareup/cardreader/lcr/TimerNativeInterface;)Lcom/squareup/cardreader/TimerApiLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_Real_ProvideTimerApiFactory;->get()Lcom/squareup/cardreader/TimerApiLegacy;

    move-result-object v0

    return-object v0
.end method
