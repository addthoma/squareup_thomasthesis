.class public final Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;
.super Ljava/lang/Object;
.source "GlobalHeadsetModule_ProvideHeadsetListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/Headset$Listener;",
        ">;"
    }
.end annotation


# instance fields
.field private final headsetStateDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/GlobalHeadsetModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    .line 25
    iput-object p2, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/GlobalHeadsetModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
            ">;)",
            "Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;-><init>(Lcom/squareup/cardreader/GlobalHeadsetModule;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideHeadsetListener(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/HeadsetStateDispatcher;)Lcom/squareup/wavpool/swipe/Headset$Listener;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/GlobalHeadsetModule;->provideHeadsetListener(Lcom/squareup/cardreader/HeadsetStateDispatcher;)Lcom/squareup/wavpool/swipe/Headset$Listener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/Headset$Listener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/Headset$Listener;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->module:Lcom/squareup/cardreader/GlobalHeadsetModule;

    iget-object v1, p0, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->headsetStateDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/HeadsetStateDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->provideHeadsetListener(Lcom/squareup/cardreader/GlobalHeadsetModule;Lcom/squareup/cardreader/HeadsetStateDispatcher;)Lcom/squareup/wavpool/swipe/Headset$Listener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalHeadsetModule_ProvideHeadsetListenerFactory;->get()Lcom/squareup/wavpool/swipe/Headset$Listener;

    move-result-object v0

    return-object v0
.end method
