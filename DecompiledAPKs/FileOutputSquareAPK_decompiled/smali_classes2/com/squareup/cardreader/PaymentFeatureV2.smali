.class public final Lcom/squareup/cardreader/PaymentFeatureV2;
.super Ljava/lang/Object;
.source "PaymentFeatureV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/PaymentFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/PaymentFeatureV2$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentFeatureV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentFeatureV2.kt\ncom/squareup/cardreader/PaymentFeatureV2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,712:1\n1360#2:713\n1429#2,3:714\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentFeatureV2.kt\ncom/squareup/cardreader/PaymentFeatureV2\n*L\n339#1:713\n339#1,3:714\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 T2\u00020\u00012\u00020\u0002:\u0001TB\u001b\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0002J \u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u000c2\u0006\u0010\u001c\u001a\u00020\u0012H\u0016J\u0010\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u0012H\u0016J\u0018\u0010\u001f\u001a\u00020\u000c2\u0006\u0010 \u001a\u00020\u00122\u0006\u0010!\u001a\u00020\u0012H\u0016J \u0010\"\u001a\u00020\u000c2\u0006\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020$H\u0016J\u0010\u0010\'\u001a\u00020\u000c2\u0006\u0010(\u001a\u00020)H\u0016J \u0010*\u001a\u00020\u000c2\u0006\u0010+\u001a\u00020\u00182\u0006\u0010,\u001a\u00020$2\u0006\u0010(\u001a\u00020)H\u0016J \u0010-\u001a\u00020\u000c2\u0006\u0010.\u001a\u00020\u00122\u0006\u0010/\u001a\u00020\u001a2\u0006\u00100\u001a\u00020\u001aH\u0016J \u00101\u001a\u00020\u000c2\u0006\u0010+\u001a\u00020\u00182\u0006\u0010,\u001a\u00020$2\u0006\u0010(\u001a\u00020)H\u0016J\u001b\u00102\u001a\u00020\u000c2\u000c\u00103\u001a\u0008\u0012\u0004\u0012\u00020504H\u0016\u00a2\u0006\u0002\u00106JM\u00107\u001a\u00020\u000c2\u0006\u0010+\u001a\u00020\u00182\u0006\u00108\u001a\u00020\u00122\u0006\u00109\u001a\u00020$2\u0008\u0010(\u001a\u0004\u0018\u00010)2\u0006\u0010!\u001a\u00020\u00122\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00020;042\u0006\u0010 \u001a\u00020\u0012H\u0016\u00a2\u0006\u0002\u0010<J\u0018\u0010=\u001a\u00020\u000c2\u0006\u0010+\u001a\u00020\u00182\u0006\u0010(\u001a\u00020)H\u0016J\u0010\u0010>\u001a\u00020\u000c2\u0006\u0010+\u001a\u00020\u0018H\u0016J\u0018\u0010?\u001a\u00020\u000c2\u0006\u0010@\u001a\u00020\u001a2\u0006\u0010+\u001a\u00020\u0018H\u0016J#\u0010A\u001a\u00020\u000c2\u0006\u0010B\u001a\u00020\u00122\u000c\u0010:\u001a\u0008\u0012\u0004\u0012\u00020;04H\u0016\u00a2\u0006\u0002\u0010CJ \u0010D\u001a\u00020\u000c2\u0006\u0010E\u001a\u00020\u00122\u0006\u0010/\u001a\u00020\u00122\u0006\u0010F\u001a\u00020\u0018H\u0016J\u0008\u0010G\u001a\u00020\u000cH\u0016J\u0018\u0010H\u001a\n I*\u0004\u0018\u00010\u00100\u00102\u0006\u0010J\u001a\u00020\u0018H\u0002J\u000c\u0010K\u001a\u00020L*\u00020MH\u0002J\u000c\u0010N\u001a\u00020O*\u00020PH\u0002J\u000c\u0010Q\u001a\u00020R*\u00020SH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006U"
    }
    d2 = {
        "Lcom/squareup/cardreader/PaymentFeatureV2;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/PaymentFeature;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "cardreaderProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V",
        "featurePointer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;",
        "initializePayment",
        "Lcom/squareup/cardreader/lcr/CrPaymentResult;",
        "mcc",
        "",
        "currencyCode",
        "onAccountSelectionRequired",
        "accounts",
        "",
        "languageBytes",
        "",
        "applicationId",
        "",
        "onAudioRequest",
        "audioMessageCode",
        "onAudioVisualRequest",
        "audioVisualId",
        "onCardActionRequired",
        "cardActionCode",
        "stdMsgCode",
        "onCardPresenceChanged",
        "present",
        "",
        "willContinuePayment",
        "presenceWasPreviouslyUnknown",
        "onCardholderNameReceived",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "onContactlessEmvAuthRequest",
        "data",
        "cardPresenceRequired",
        "onDisplayRequest",
        "displayMessageCode",
        "amount",
        "balance",
        "onEmvAuthRequest",
        "onListApplications",
        "applications",
        "",
        "Lcom/squareup/cardreader/EmvApplication;",
        "([Lcom/squareup/cardreader/EmvApplication;)V",
        "onPaymentComplete",
        "paymentResultCode",
        "approvedOffline",
        "paymentTimingsArray",
        "Lcom/squareup/cardreader/PaymentTiming;",
        "([BIZLcom/squareup/cardreader/CardInfo;I[Lcom/squareup/cardreader/PaymentTiming;I)V",
        "onSwipePassthrough",
        "onTmnAuthRequest",
        "onTmnDataToTmn",
        "transactionId",
        "onTmnTransactionComplete",
        "tmnTransactionResultCode",
        "(I[Lcom/squareup/cardreader/PaymentTiming;)V",
        "onTmnWriteNotify",
        "balanceBefore",
        "miryoData",
        "resetIfInitilized",
        "sendTmnBytesToReader",
        "kotlin.jvm.PlatformType",
        "tmnBytes",
        "toCardAction",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;",
        "Lcom/squareup/cardreader/lcr/CrPaymentCardAction;",
        "toPaymentResult",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;",
        "Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;",
        "toStandardMessage",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;",
        "Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;",
        "Companion",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/cardreader/PaymentFeatureV2$Companion;


# instance fields
.field private final cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

.field private featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/cardreader/PaymentFeatureV2$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/cardreader/PaymentFeatureV2$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/cardreader/PaymentFeatureV2;->Companion:Lcom/squareup/cardreader/PaymentFeatureV2$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
            ">;",
            "Lcom/squareup/cardreader/CardreaderPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    return-void
.end method

.method public static final synthetic access$getFeaturePointer$p(Lcom/squareup/cardreader/PaymentFeatureV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;
    .locals 1

    .line 243
    iget-object p0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez p0, :cond_0

    const-string v0, "featurePointer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setFeaturePointer$p(Lcom/squareup/cardreader/PaymentFeatureV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)V
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    return-void
.end method

.method private final initializePayment(II)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->cardreaderProvider:Lcom/squareup/cardreader/CardreaderPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 286
    invoke-static {v0, p0, p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;II)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    move-result-object p1

    const-string p2, "PaymentFeatureNative.pay\u2026, mcc, currencyCode\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    .line 289
    sget-object p1, Lcom/squareup/cardreader/lcr/CrPaymentResult;->CR_PAYMENT_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-object p1
.end method

.method private final sendTmnBytesToReader([B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_0

    const-string v1, "featurePointer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method private final toCardAction(Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;
    .locals 1

    .line 480
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 498
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->RequestTap:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 497
    :pswitch_1
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessLimitExceededInsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 495
    :pswitch_2
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessCardLimitExceededTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 493
    :pswitch_3
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessUnlockPhoneToPay:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 492
    :pswitch_4
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessPresentOnlyOne:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 491
    :pswitch_5
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessSeePhone:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 490
    :pswitch_6
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessErrorTryAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 489
    :pswitch_7
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ContactlessErrorTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 487
    :pswitch_8
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->InsertFromContactless:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 486
    :pswitch_9
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->SwipeAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 485
    :pswitch_a
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->RequestSwipeScheme:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 484
    :pswitch_b
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->RequestSwipeTechnical:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 483
    :pswitch_c
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->ReinsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 482
    :pswitch_d
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->InsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    goto :goto_0

    .line 481
    :pswitch_e
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;->None:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final toPaymentResult(Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;
    .locals 1

    .line 503
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 513
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->TimedOut:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 512
    :pswitch_1
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessMagstripeTechnicalFallback:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 510
    :pswitch_2
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessMagstripeSchemeFallback:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 509
    :pswitch_3
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessMagstripe:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 508
    :pswitch_4
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->FailureIccReverse:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 507
    :pswitch_5
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->FailureIccDecline:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 506
    :pswitch_6
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessIccApproveWithSignature:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 505
    :pswitch_7
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->SuccessIccApprove:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    goto :goto_0

    .line 504
    :pswitch_8
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;->Terminated:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private final toStandardMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;
    .locals 1

    .line 518
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 553
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->TooManyTap:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 552
    :pswitch_1
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->UnlockPhoneToPay:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 551
    :pswitch_2
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PresentCardAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 550
    :pswitch_3
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->SeePhoneForInstructions:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 549
    :pswitch_4
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->NoMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 548
    :pswitch_5
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PlsInsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 547
    :pswitch_6
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->InsertSwipeOrTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 546
    :pswitch_7
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->AuthorisingPlsWait:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 545
    :pswitch_8
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->ApprovedPlsSign:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto/16 :goto_0

    .line 544
    :pswitch_9
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PlsPresentOneCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 543
    :pswitch_a
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PlsInsertOrSwipeCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 542
    :pswitch_b
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CardReadOkPlsRemoveCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 541
    :pswitch_c
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Processing:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 540
    :pswitch_d
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PresentCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 539
    :pswitch_e
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Welcome:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 538
    :pswitch_f
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->TryAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 537
    :pswitch_10
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->UseMagStrip:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 536
    :pswitch_11
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->UseChipReader:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 535
    :pswitch_12
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->RemoveCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 534
    :pswitch_13
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->ProcessingError:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 533
    :pswitch_14
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PleaseWait:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 532
    :pswitch_15
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PinOk:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 531
    :pswitch_16
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->NotAccepted:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 530
    :pswitch_17
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->InsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 529
    :pswitch_18
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->IncorrectPin:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 528
    :pswitch_19
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->EnterPin:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 527
    :pswitch_1a
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->EnterAmount:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 526
    :pswitch_1b
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Declined:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 525
    :pswitch_1c
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CardError:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 524
    :pswitch_1d
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CancelOrEnter:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 523
    :pswitch_1e
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CallYourBank:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 522
    :pswitch_1f
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Approved:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 521
    :pswitch_20
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->AmountOk:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 520
    :pswitch_21
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Amount:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    goto :goto_0

    .line 519
    :pswitch_22
    sget-object p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->None:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    :goto_0
    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;)V
    .locals 13

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$InitializePayment;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$InitializePayment;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$InitializePayment;->getMcc()I

    move-result v1

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$InitializePayment;->getCurrencyCode()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/squareup/cardreader/PaymentFeatureV2;->initializePayment(II)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    goto/16 :goto_0

    .line 252
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;

    const-string v1, "featurePointer"

    if-eqz v0, :cond_2

    .line 253
    iget-object v2, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v3

    .line 254
    sget-object v1, Lcom/squareup/cardreader/PaymentAccountType;->PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/PaymentAccountType;

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toSwigEnum(Lcom/squareup/cardreader/PaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->swigValue()I

    move-result v5

    .line 255
    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getTransactionType()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue()I

    move-result v6

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getYear()I

    move-result v7

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getMonth()I

    move-result v8

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getDay()I

    move-result v9

    .line 256
    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getHour()I

    move-result v10

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getMinute()I

    move-result v11

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->getSecond()I

    move-result v12

    .line 252
    invoke-static/range {v2 .. v12}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_start_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;JIIIIIIII)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026e, message.second\n      )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 258
    :cond_2
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartTmnPaymentInteraction;

    if-eqz v0, :cond_4

    .line 259
    iget-object v2, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    move-object v0, p1

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartTmnPaymentInteraction;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartTmnPaymentInteraction;->getRequestType()Lcom/squareup/cardreader/TmnRequestType;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toSwigEnum(Lcom/squareup/cardreader/TmnRequestType;)Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartTmnPaymentInteraction;->getTransactionId()Ljava/lang/String;

    move-result-object v4

    .line 260
    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartTmnPaymentInteraction;->getBrandId()Lcom/squareup/cardreader/TmnBrandId;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toSwigEnum(Lcom/squareup/cardreader/TmnBrandId;)Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v5

    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartTmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v6

    .line 258
    invoke-static/range {v2 .. v7}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_start_transaction(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026.amountAuthorized\n      )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 262
    :cond_4
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$CancelPayment;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_cancel_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.cr_\u2026l_payment(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 263
    :cond_6
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SelectApplication;

    if-eqz v0, :cond_8

    .line 264
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SelectApplication;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SelectApplication;->getApplication()Lcom/squareup/cardreader/EmvApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/EmvApplication;->getAdfName()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_select_application(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026sage.application.adfName)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 265
    :cond_8
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$ProcessARPC;

    if-eqz v0, :cond_a

    .line 266
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$ProcessARPC;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$ProcessARPC;->getData()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_process_server_response(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026urePointer, message.data)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 267
    :cond_a
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$CheckCardPresence;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_request_card_presence(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.cr_\u2026_presence(featurePointer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 268
    :cond_c
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$EnableSwipePassthrough;

    if-eqz v0, :cond_e

    .line 269
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$EnableSwipePassthrough;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$EnableSwipePassthrough;->getEnable()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_enable_swipe_passthrough(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Z)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.cr_\u2026ePointer, message.enable)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_e
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SendReaderPowerupHint;

    if-eqz v0, :cond_10

    .line 271
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SendReaderPowerupHint;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SendReaderPowerupHint;->getTimeoutSeconds()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_send_powerup_hint(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026, message.timeoutSeconds)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 272
    :cond_10
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SelectAccountType;

    if-eqz v0, :cond_12

    .line 274
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_11

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SelectAccountType;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$SelectAccountType;->getAccountType()Lcom/squareup/cardreader/PaymentAccountType;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->toSwigEnum(Lcom/squareup/cardreader/PaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    move-result-object v1

    .line 273
    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_select_account_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026pe.toSwigEnum()\n        )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :cond_12
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$TmnSendBytesToReader;

    if-eqz v0, :cond_14

    .line 277
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    move-object v1, p1

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$TmnSendBytesToReader;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$TmnSendBytesToReader;->getTmnBytes()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    const-string v1, "PaymentFeatureNative.pay\u2026ointer, message.tmnBytes)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    :goto_0
    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$PaymentFeatureResult;

    invoke-direct {v2, v0, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$PaymentFeatureResult;-><init>(Lcom/squareup/cardreader/lcr/CrPaymentResult;Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;)V

    check-cast v2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void

    .line 277
    :cond_14
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public onAccountSelectionRequired([I[BLjava/lang/String;)V
    .locals 2

    const-string v0, "accounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "languageBytes"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applicationId"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureV2;->Companion:Lcom/squareup/cardreader/PaymentFeatureV2$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureV2$Companion;->accountTypesFromInt([I)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 713
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 714
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 715
    check-cast v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    .line 339
    invoke-static {v1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toPosEnum(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/PaymentAccountType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 716
    :cond_0
    check-cast v0, Ljava/util/List;

    .line 340
    sget-object p1, Lcom/squareup/cardreader/PaymentFeatureV2;->Companion:Lcom/squareup/cardreader/PaymentFeatureV2$Companion;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/PaymentFeatureV2$Companion;->languagePrefsFromBytes([B)Ljava/util/List;

    move-result-object p1

    .line 341
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 342
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnAccountSelectionRequired;

    invoke-direct {v1, v0, p1, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnAccountSelectionRequired;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 341
    invoke-interface {p2, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onAudioRequest(I)V
    .locals 2

    .line 466
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    move-result-object p1

    const-string v0, "CrsTmnAudio.swigToEnum(audioMessageCode)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 467
    invoke-static {p1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)Lcom/squareup/cardreader/TmnAudio;

    move-result-object p1

    .line 468
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnAudioRequest;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnAudioRequest;-><init>(Lcom/squareup/cardreader/TmnAudio;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onAudioVisualRequest(I)V
    .locals 3

    .line 592
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    move-result-object p1

    .line 593
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnAudioVisualRequest;

    const-string v2, "audioVisualIdentifier"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnAudioVisualRequest;-><init>(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onCardActionRequired(II)V
    .locals 2

    .line 321
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    move-result-object p1

    const-string v0, "CrPaymentCardAction.swigToEnum(cardActionCode)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentFeatureV2;->toCardAction(Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;

    move-result-object p1

    .line 323
    invoke-static {p2}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object p2

    const-string v0, "CrPaymentStandardMessage.swigToEnum(stdMsgCode)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    invoke-direct {p0, p2}, Lcom/squareup/cardreader/PaymentFeatureV2;->toStandardMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    move-result-object p2

    .line 325
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnCardActionRequired;

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnCardActionRequired;-><init>(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$CardAction;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onCardPresenceChanged(ZZZ)V
    .locals 1

    .line 311
    iget-object p3, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 312
    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnCardPresenceChanged;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnCardPresenceChanged;-><init>(ZZ)V

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 311
    invoke-interface {p3, v0}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 2

    const-string v0, "cardInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 424
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnCardholderNameReceived;

    invoke-static {p1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnCardholderNameReceived;-><init>(Lcom/squareup/cardreader/CardDescription;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onContactlessEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 399
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnContactlessEmvAuthRequest;

    .line 402
    invoke-static {p3}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;

    move-result-object p3

    .line 399
    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnContactlessEmvAuthRequest;-><init>([BZLcom/squareup/cardreader/CardDescription;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 398
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onDisplayRequest(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balance"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 457
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object p1

    const-string v0, "CrsTmnMessage.swigToEnum(displayMessageCode)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    invoke-static {p1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toPosEnum(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Lcom/squareup/cardreader/TmnMessage;

    move-result-object p1

    .line 459
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 460
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnDisplayRequest;

    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnDisplayRequest;-><init>(Lcom/squareup/cardreader/TmnMessage;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 459
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 383
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 384
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnEmvAuthRequest;

    .line 387
    invoke-static {p3}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;

    move-result-object p3

    .line 384
    invoke-direct {v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnEmvAuthRequest;-><init>([BZLcom/squareup/cardreader/CardDescription;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 383
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 2

    const-string v0, "applications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;-><init>([Lcom/squareup/cardreader/EmvApplication;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onPaymentComplete([BIZLcom/squareup/cardreader/CardInfo;I[Lcom/squareup/cardreader/PaymentTiming;I)V
    .locals 11

    move-object v0, p0

    const-string v1, "data"

    move-object v3, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "paymentTimingsArray"

    move-object/from16 v8, p6

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 359
    invoke-static {p2}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    move-result-object v1

    const-string v2, "CrPaymentPaymentResult.s\u2026ToEnum(paymentResultCode)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 360
    invoke-direct {p0, v1}, Lcom/squareup/cardreader/PaymentFeatureV2;->toPaymentResult(Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;

    move-result-object v4

    .line 361
    invoke-static/range {p5 .. p5}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object v1

    const-string v2, "CrPaymentStandardMessage.swigToEnum(stdMsgCode)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    invoke-direct {p0, v1}, Lcom/squareup/cardreader/PaymentFeatureV2;->toStandardMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    move-result-object v7

    .line 363
    invoke-static/range {p7 .. p7}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    move-result-object v9

    .line 364
    iget-object v1, v0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 365
    new-instance v10, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;

    if-eqz p4, :cond_0

    .line 369
    invoke-static {p4}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    move-object v6, v2

    const-string v2, "cardAction"

    .line 372
    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v10

    move-object v3, p1

    move v5, p3

    move-object/from16 v8, p6

    .line 365
    invoke-direct/range {v2 .. v9}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete;-><init>([BLcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnPaymentComplete$PaymentResult;ZLcom/squareup/cardreader/CardDescription;Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;[Lcom/squareup/cardreader/PaymentTiming;Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)V

    check-cast v10, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 364
    invoke-interface {v1, v10}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onSwipePassthrough([BLcom/squareup/cardreader/CardInfo;)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 417
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 418
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnSwipePassthrough;

    invoke-static {p2}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toCardDescription(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/CardDescription;

    move-result-object p2

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnSwipePassthrough;-><init>([BLcom/squareup/cardreader/CardDescription;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 417
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onTmnAuthRequest([B)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnAuthRequest;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnAuthRequest;-><init>([B)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onTmnDataToTmn(Ljava/lang/String;[B)V
    .locals 2

    const-string v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 432
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnDataToTmn;

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnDataToTmn;-><init>(Ljava/lang/String;[B)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onTmnTransactionComplete(I[Lcom/squareup/cardreader/PaymentTiming;)V
    .locals 2

    const-string v0, "paymentTimingsArray"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object p1

    const-string v0, "com.squareup.cardreader.\u2026tmnTransactionResultCode)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 443
    invoke-static {p1}, Lcom/squareup/cardreader/PaymentFeatureV2Kt;->access$toPosEnum(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Lcom/squareup/cardreader/TmnTransactionResult;

    move-result-object p1

    .line 444
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 445
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnTransactionComplete;-><init>(Lcom/squareup/cardreader/TmnTransactionResult;[Lcom/squareup/cardreader/PaymentTiming;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 444
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onTmnWriteNotify(II[B)V
    .locals 0

    const-string p1, "miryoData"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 476
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object p2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnWriteNotify;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnTmnWriteNotify;

    check-cast p2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 293
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/PaymentFeatureV2;

    iget-object v0, v0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-eqz v0, :cond_2

    .line 294
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    const-string v1, "featurePointer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    .line 295
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureV2;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    :cond_2
    return-void
.end method
