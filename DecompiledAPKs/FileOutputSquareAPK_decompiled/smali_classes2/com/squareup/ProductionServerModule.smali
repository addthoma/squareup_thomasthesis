.class public abstract Lcom/squareup/ProductionServerModule;
.super Ljava/lang/Object;
.source "ProductionServerModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ServerCommonModule;
    }
.end annotation


# static fields
.field private static final PRODUCTION_API_URL_DURATION_MILLIS:J = 0x1b7740L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideApiUrlSelector(Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/ApiUrlSelector;
    .locals 4
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ApiUrlSelector;

    const-string v1, "https://api-global.squareup.com/"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-wide/32 v2, 0x1b7740

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/squareup/ApiUrlSelector;-><init>(Ljava/util/List;JLcom/squareup/log/OhSnapLogger;)V

    return-object v0
.end method

.method static provideConnectApiServer()Lcom/squareup/http/Server;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/http/Server;

    const-string v1, "https://connect.squareup.com"

    invoke-direct {v0, v1}, Lcom/squareup/http/Server;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static provideServer(Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;Lcom/squareup/http/UrlRedirectSetting;)Lcom/squareup/http/Server;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/RedirectingServer;

    const-string v1, "https://api-global.squareup.com/"

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/RedirectingServer;-><init>(Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettingsApiUrl;Lcom/squareup/http/UrlRedirectSetting;)V

    return-object v0
.end method

.method static provideWeeblyApiServer()Lcom/squareup/http/Server;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/http/Server;

    const-string v1, "https://www.weebly.com"

    invoke-direct {v0, v1}, Lcom/squareup/http/Server;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
