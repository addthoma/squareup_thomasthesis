.class public final Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;
.super Ljava/lang/Object;
.source "RealClientActionTranslationDispatcher.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealClientActionTranslationDispatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealClientActionTranslationDispatcher.kt\ncom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,51:1\n1360#2:52\n1429#2,3:53\n732#2,9:56\n*E\n*S KotlinDebug\n*F\n+ 1 RealClientActionTranslationDispatcher.kt\ncom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher\n*L\n46#1:52\n46#1,3:53\n47#1,9:56\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B*\u0008\u0007\u0012\u0011\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0012\u0010\u0011\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010\u0002\u001a\r\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\u0008\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslationDispatcher;",
        "handlers",
        "",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "deepLinks",
        "Lcom/squareup/ui/main/DeepLinks;",
        "clientActionDispatcherAnalytics",
        "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
        "(Ljava/util/Set;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;)V",
        "canHandleInternally",
        "",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "firstHandleableUrl",
        "",
        "handle",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clientActionDispatcherAnalytics:Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;

.field private final deepLinks:Lcom/squareup/ui/main/DeepLinks;

.field private final handlers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
            ">;",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "handlers"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deepLinks"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientActionDispatcherAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->handlers:Ljava/util/Set;

    iput-object p2, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    iput-object p3, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->clientActionDispatcherAnalytics:Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;

    return-void
.end method

.method private final firstHandleableUrl(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->handlers:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 53
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 54
    check-cast v2, Lcom/squareup/clientactiontranslation/ClientActionTranslator;

    .line 46
    invoke-interface {v2, p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslator;->translate(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 56
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 63
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    if-eqz v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    :cond_2
    check-cast p1, Ljava/util/List;

    .line 48
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;->getUrl()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method


# virtual methods
.method public canHandleInternally(Lcom/squareup/protos/client/ClientAction;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_2

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->firstHandleableUrl(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-nez p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    return v0
.end method

.method public handle(Lcom/squareup/protos/client/ClientAction;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 28
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->firstHandleableUrl(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 35
    iget-object v0, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    iget-object v2, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->clientActionDispatcherAnalytics:Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;

    invoke-interface {v2, p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;->logHandled(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    goto :goto_0

    .line 40
    :cond_1
    iget-object v2, p0, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->clientActionDispatcherAnalytics:Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;

    invoke-interface {v2, p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;->logTranslatorFoundButLinkNotHandled(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    :goto_0
    return v0

    .line 28
    :cond_2
    move-object v1, p0

    check-cast v1, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;

    .line 30
    iget-object v1, v1, Lcom/squareup/clientactiontranslation/RealClientActionTranslationDispatcher;->clientActionDispatcherAnalytics:Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;

    invoke-interface {v1, p1}, Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;->logNoTranslatorFound(Lcom/squareup/protos/client/ClientAction;)V

    return v0
.end method
