.class public interface abstract Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;
.super Ljava/lang/Object;
.source "ClientActionDispatcherAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/clientactiontranslation/ClientActionDispatcherAnalytics;",
        "",
        "logHandled",
        "",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "deepLinkUrl",
        "",
        "logNoTranslatorFound",
        "logTranslatorFoundButLinkNotHandled",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logHandled(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V
.end method

.method public abstract logNoTranslatorFound(Lcom/squareup/protos/client/ClientAction;)V
.end method

.method public abstract logTranslatorFoundButLinkNotHandled(Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V
.end method
