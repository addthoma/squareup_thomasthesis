.class final Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CheckoutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CheckoutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CheckoutResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/checkoutfe/CheckoutResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 171
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/checkoutfe/CheckoutResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CheckoutResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;-><init>()V

    .line 195
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 196
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 203
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 201
    :cond_0
    sget-object v3, Lcom/squareup/checkoutfe/CheckoutClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details(Lcom/squareup/checkoutfe/CheckoutClientDetails;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    goto :goto_0

    .line 200
    :cond_1
    sget-object v3, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order(Lcom/squareup/orders/model/Order;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    goto :goto_0

    .line 199
    :cond_2
    sget-object v3, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response(Lcom/squareup/protos/connect/v2/CreatePaymentResponse;)Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    goto :goto_0

    .line 198
    :cond_3
    iget-object v3, v0, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 208
    invoke-virtual {v0}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->build()Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CheckoutResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    sget-object v0, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 186
    sget-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 187
    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 189
    invoke-virtual {p2}, Lcom/squareup/checkoutfe/CheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    check-cast p2, Lcom/squareup/checkoutfe/CheckoutResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CheckoutResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/checkoutfe/CheckoutResponse;)I
    .locals 4

    .line 176
    sget-object v0, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->errors:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    const/4 v3, 0x2

    .line 177
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->order:Lcom/squareup/orders/model/Order;

    const/4 v3, 0x3

    .line 178
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/checkoutfe/CheckoutClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/checkoutfe/CheckoutResponse;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    const/4 v3, 0x4

    .line 179
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;->encodedSize(Lcom/squareup/checkoutfe/CheckoutResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/checkoutfe/CheckoutResponse;)Lcom/squareup/checkoutfe/CheckoutResponse;
    .locals 2

    .line 213
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutResponse;->newBuilder()Lcom/squareup/checkoutfe/CheckoutResponse$Builder;

    move-result-object p1

    .line 214
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/checkoutfe/LocalizedError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 215
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    iput-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->create_payment_response:Lcom/squareup/protos/connect/v2/CreatePaymentResponse;

    .line 216
    :cond_0
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order;

    iput-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 217
    :cond_1
    iget-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/checkoutfe/CheckoutClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutfe/CheckoutClientDetails;

    iput-object v0, p1, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->client_details:Lcom/squareup/checkoutfe/CheckoutClientDetails;

    .line 218
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 219
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutResponse$Builder;->build()Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CheckoutResponse$ProtoAdapter_CheckoutResponse;->redact(Lcom/squareup/checkoutfe/CheckoutResponse;)Lcom/squareup/checkoutfe/CheckoutResponse;

    move-result-object p1

    return-object p1
.end method
