.class public final Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CancelCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CancelCheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CancelCheckoutRequest;",
        "Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public idempotency_key:Ljava/lang/String;

.field public order_id:Ljava/lang/String;

.field public order_version:Ljava/lang/Integer;

.field public payment_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->payment_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CancelCheckoutRequest;
    .locals 7

    .line 168
    new-instance v6, Lcom/squareup/checkoutfe/CancelCheckoutRequest;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->order_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->order_version:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->payment_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutfe/CancelCheckoutRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->build()Lcom/squareup/checkoutfe/CancelCheckoutRequest;

    move-result-object v0

    return-object v0
.end method

.method public idempotency_key(Ljava/lang/String;)Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    return-object p0
.end method

.method public order_id(Ljava/lang/String;)Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->order_id:Ljava/lang/String;

    return-object p0
.end method

.method public order_version(Ljava/lang/Integer;)Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->order_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public payment_ids(Ljava/util/List;)Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;"
        }
    .end annotation

    .line 161
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 162
    iput-object p1, p0, Lcom/squareup/checkoutfe/CancelCheckoutRequest$Builder;->payment_ids:Ljava/util/List;

    return-object p0
.end method
