.class synthetic Lcom/squareup/applet/BadgePresenter$1;
.super Ljava/lang/Object;
.source "BadgePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/BadgePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 77
    invoke-static {}, Lcom/squareup/applet/Applet$Badge$Priority;->values()[Lcom/squareup/applet/Applet$Badge$Priority;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/applet/BadgePresenter$1;->$SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I

    :try_start_0
    sget-object v0, Lcom/squareup/applet/BadgePresenter$1;->$SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I

    sget-object v1, Lcom/squareup/applet/Applet$Badge$Priority;->NORMAL:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-virtual {v1}, Lcom/squareup/applet/Applet$Badge$Priority;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/applet/BadgePresenter$1;->$SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I

    sget-object v1, Lcom/squareup/applet/Applet$Badge$Priority;->HIGH:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-virtual {v1}, Lcom/squareup/applet/Applet$Badge$Priority;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/applet/BadgePresenter$1;->$SwitchMap$com$squareup$applet$Applet$Badge$Priority:[I

    sget-object v1, Lcom/squareup/applet/Applet$Badge$Priority;->FATAL:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-virtual {v1}, Lcom/squareup/applet/Applet$Badge$Priority;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
