.class public final Lcom/squareup/applet/Applet$Badge$Visible;
.super Lcom/squareup/applet/Applet$Badge;
.source "Applet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/Applet$Badge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Visible"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nApplet.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Applet.kt\ncom/squareup/applet/Applet$Badge$Visible\n*L\n1#1,153:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/applet/Applet$Badge$Visible;",
        "Lcom/squareup/applet/Applet$Badge;",
        "count",
        "",
        "text",
        "",
        "priority",
        "Lcom/squareup/applet/Applet$Badge$Priority;",
        "(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V",
        "getCount",
        "()I",
        "getPriority",
        "()Lcom/squareup/applet/Applet$Badge$Priority;",
        "getText",
        "()Ljava/lang/CharSequence;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final count:I

.field private final priority:Lcom/squareup/applet/Applet$Badge$Priority;

.field private final text:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V
    .locals 1

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priority"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, v0}, Lcom/squareup/applet/Applet$Badge;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    iput-object p2, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    .line 67
    iget p1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Negative counts are not allowed."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static synthetic copy$default(Lcom/squareup/applet/Applet$Badge$Visible;ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;ILjava/lang/Object;)Lcom/squareup/applet/Applet$Badge$Visible;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/applet/Applet$Badge$Visible;->copy(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge$Visible;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    return v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Lcom/squareup/applet/Applet$Badge$Priority;
    .locals 1

    iget-object v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    return-object v0
.end method

.method public final copy(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge$Visible;
    .locals 1

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priority"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/applet/Applet$Badge$Visible;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/applet/Applet$Badge$Visible;-><init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/applet/Applet$Badge$Visible;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/applet/Applet$Badge$Visible;

    iget v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    iget v1, p1, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    iget-object p1, p1, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCount()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    return v0
.end method

.method public final getPriority()Lcom/squareup/applet/Applet$Badge$Priority;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    return-object v0
.end method

.method public final getText()Ljava/lang/CharSequence;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Visible(count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", priority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/applet/Applet$Badge$Visible;->priority:Lcom/squareup/applet/Applet$Badge$Priority;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
