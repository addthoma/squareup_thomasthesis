.class public Lcom/squareup/applet/AppletsDrawerLayout;
.super Lcom/squareup/ui/WorkaroundDrawerLayout;
.source "AppletsDrawerLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/applet/AppletsDrawerLayout$ParentComponent;
    }
.end annotation


# instance fields
.field appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private bottomItem:Landroid/view/ViewGroup;

.field private combinedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/AppletsDrawerItem;",
            ">;"
        }
    .end annotation
.end field

.field private mainItemsList:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/WorkaroundDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lcom/squareup/applet/AppletsDrawerLayout;->combinedList:Ljava/util/List;

    .line 39
    const-class p2, Lcom/squareup/applet/AppletsDrawerLayout$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletsDrawerLayout$ParentComponent;

    invoke-interface {p1, p0}, Lcom/squareup/applet/AppletsDrawerLayout$ParentComponent;->inject(Lcom/squareup/applet/AppletsDrawerLayout;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$color;->marin_screen_scrim:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletsDrawerLayout;->setScrimColor(I)V

    const/high16 p1, 0x40000

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletsDrawerLayout;->setDescendantFocusability(I)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/applet/AppletsDrawerLayout;->lockDrawerInitially()V

    return-void
.end method

.method private lockDrawerInitially()V
    .locals 2

    const/4 v0, 0x1

    const v1, 0x800003

    .line 127
    invoke-virtual {p0, v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->setDrawerLockMode(II)V

    return-void
.end method


# virtual methods
.method addItem(Z)Lcom/squareup/applet/AppletsDrawerItem;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/applet/AppletsDrawerItem;

    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/applet/AppletsDrawerItem;-><init>(Landroid/content/Context;)V

    if-eqz p1, :cond_1

    .line 84
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout;->bottomItem:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p1

    if-gtz p1, :cond_0

    .line 88
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout;->bottomItem:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 85
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There can be only one applet fixed to the bottom of the drawer."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 90
    :cond_1
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout;->mainItemsList:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 93
    :goto_0
    iget-object p1, p0, Lcom/squareup/applet/AppletsDrawerLayout;->combinedList:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected announceAppletNameForAccessibility(Ljava/lang/String;)V
    .locals 2

    .line 146
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsDrawerLayout;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 147
    new-instance v1, Lcom/squareup/applet/AppletsDrawerLayout$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/applet/AppletsDrawerLayout$2;-><init>(Lcom/squareup/applet/AppletsDrawerLayout;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    const/16 p1, 0x20

    .line 158
    invoke-virtual {v0, p1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    return-void
.end method

.method public clearDrawer()V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->mainItemsList:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 77
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->bottomItem:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method closeDrawer()V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletsDrawerPresenter;->closeDrawer()V

    return-void
.end method

.method itemAt(I)Lcom/squareup/applet/AppletsDrawerItem;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->combinedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletsDrawerItem;

    return-object p1
.end method

.method protected lockDrawer()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x3

    .line 113
    invoke-virtual {p0, v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->setDrawerLockMode(II)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 66
    invoke-super {p0}, Lcom/squareup/ui/WorkaroundDrawerLayout;->onAttachedToWindow()V

    .line 67
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->appletsDrawerPresenter:Lcom/squareup/applet/AppletsDrawerPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/AppletsDrawerPresenter;->dropView(Ljava/lang/Object;)V

    .line 72
    invoke-super {p0}, Lcom/squareup/ui/WorkaroundDrawerLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 50
    invoke-super {p0}, Lcom/squareup/ui/WorkaroundDrawerLayout;->onFinishInflate()V

    .line 51
    sget v0, Lcom/squareup/pos/container/R$id;->applets_main_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->mainItemsList:Landroid/view/ViewGroup;

    .line 52
    sget v0, Lcom/squareup/pos/container/R$id;->applets_bottom_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/applet/AppletsDrawerLayout;->bottomItem:Landroid/view/ViewGroup;

    .line 54
    new-instance v0, Lcom/squareup/applet/AppletsDrawerLayout$1;

    invoke-direct {v0, p0}, Lcom/squareup/applet/AppletsDrawerLayout$1;-><init>(Lcom/squareup/applet/AppletsDrawerLayout;)V

    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletsDrawerLayout;->addDrawerListener(Landroidx/drawerlayout/widget/DrawerLayout$DrawerListener;)V

    return-void
.end method

.method setSelectedItem(I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 102
    :goto_0
    iget-object v2, p0, Lcom/squareup/applet/AppletsDrawerLayout;->combinedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 103
    iget-object v2, p0, Lcom/squareup/applet/AppletsDrawerLayout;->combinedList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/applet/AppletsDrawerItem;

    if-ne v1, p1, :cond_0

    const/4 v3, 0x1

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2, v3}, Lcom/squareup/applet/AppletsDrawerItem;->setSelected(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected unlockDrawer()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 132
    invoke-virtual {p0, v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->setDrawerLockMode(II)V

    const v1, 0x800003

    .line 133
    invoke-virtual {p0, v0, v1}, Lcom/squareup/applet/AppletsDrawerLayout;->setDrawerLockMode(II)V

    return-void
.end method
