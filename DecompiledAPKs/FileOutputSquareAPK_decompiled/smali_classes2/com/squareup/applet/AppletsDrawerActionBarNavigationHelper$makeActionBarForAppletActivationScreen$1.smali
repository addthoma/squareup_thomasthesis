.class final Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper$makeActionBarForAppletActivationScreen$1;
.super Ljava/lang/Object;
.source "AppletsDrawerActionBarNavigationHelper.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;


# direct methods
.method constructor <init>(Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper$makeActionBarForAppletActivationScreen$1;->this$0:Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper$makeActionBarForAppletActivationScreen$1;->this$0:Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;

    invoke-static {v0}, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;->access$getDrawerRunner$p(Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;)Lcom/squareup/applet/AppletsDrawerRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/AppletsDrawerRunner;->toggleDrawer()V

    return-void
.end method
