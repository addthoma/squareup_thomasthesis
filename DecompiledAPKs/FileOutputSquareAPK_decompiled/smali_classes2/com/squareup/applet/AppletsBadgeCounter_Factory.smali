.class public final Lcom/squareup/applet/AppletsBadgeCounter_Factory;
.super Ljava/lang/Object;
.source "AppletsBadgeCounter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/applet/AppletsBadgeCounter;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->appletsProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/applet/AppletsBadgeCounter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/applet/AppletsBadgeCounter_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/applet/AppletsBadgeCounter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/applet/Applets;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)Lcom/squareup/applet/AppletsBadgeCounter;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/applet/AppletsBadgeCounter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/applet/AppletsBadgeCounter;-><init>(Lcom/squareup/applet/Applets;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/applet/AppletsBadgeCounter;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->appletsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/Applets;

    iget-object v1, p0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/Scheduler;

    invoke-static {v0, v1, v2}, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->newInstance(Lcom/squareup/applet/Applets;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;)Lcom/squareup/applet/AppletsBadgeCounter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/applet/AppletsBadgeCounter_Factory;->get()Lcom/squareup/applet/AppletsBadgeCounter;

    move-result-object v0

    return-object v0
.end method
