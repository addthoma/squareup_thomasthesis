.class public final Lcom/squareup/applet/Applet$Badge$Companion;
.super Ljava/lang/Object;
.source "Applet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/Applet$Badge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/applet/Applet$Badge$Companion;",
        "",
        "()V",
        "toBadge",
        "Lcom/squareup/applet/Applet$Badge;",
        "count",
        "",
        "priority",
        "Lcom/squareup/applet/Applet$Badge$Priority;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/applet/Applet$Badge$Companion;-><init>()V

    return-void
.end method

.method public static synthetic toBadge$default(Lcom/squareup/applet/Applet$Badge$Companion;ILcom/squareup/applet/Applet$Badge$Priority;ILjava/lang/Object;)Lcom/squareup/applet/Applet$Badge;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 82
    sget-object p2, Lcom/squareup/applet/Applet$Badge$Priority;->NORMAL:Lcom/squareup/applet/Applet$Badge$Priority;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge(ILcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final toBadge(I)Lcom/squareup/applet/Applet$Badge;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge$default(Lcom/squareup/applet/Applet$Badge$Companion;ILcom/squareup/applet/Applet$Badge$Priority;ILjava/lang/Object;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    return-object p1
.end method

.method public final toBadge(ILcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "priority"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p1, :cond_0

    .line 85
    sget-object p1, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    goto :goto_0

    .line 87
    :cond_0
    new-instance v0, Lcom/squareup/applet/Applet$Badge$Visible;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Integer.toString(count)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/applet/Applet$Badge$Visible;-><init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    :goto_0
    return-object p1
.end method
