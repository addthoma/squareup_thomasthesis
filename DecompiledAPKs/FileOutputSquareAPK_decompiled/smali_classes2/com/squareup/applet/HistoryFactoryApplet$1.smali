.class Lcom/squareup/applet/HistoryFactoryApplet$1;
.super Ljava/lang/Object;
.source "HistoryFactoryApplet.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/applet/HistoryFactoryApplet;->pushActivationScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/applet/HistoryFactoryApplet;


# direct methods
.method constructor <init>(Lcom/squareup/applet/HistoryFactoryApplet;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/applet/HistoryFactoryApplet$1;->this$0:Lcom/squareup/applet/HistoryFactoryApplet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 0

    .line 88
    iget-object p1, p0, Lcom/squareup/applet/HistoryFactoryApplet$1;->this$0:Lcom/squareup/applet/HistoryFactoryApplet;

    invoke-virtual {p1, p2}, Lcom/squareup/applet/HistoryFactoryApplet;->getHomeScreens(Lflow/History;)Ljava/util/List;

    move-result-object p1

    if-nez p2, :cond_0

    .line 91
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lflow/History$Builder;->addAll(Ljava/util/Collection;)Lflow/History$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1

    .line 95
    :cond_0
    invoke-static {p2, p1}, Lcom/squareup/container/Histories;->pushStack(Lflow/History;Ljava/util/List;)Lkotlin/Pair;

    move-result-object p1

    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/History;

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/applet/HistoryFactoryApplet$1;->this$0:Lcom/squareup/applet/HistoryFactoryApplet;

    invoke-virtual {v0}, Lcom/squareup/applet/HistoryFactoryApplet;->getPermissions()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
