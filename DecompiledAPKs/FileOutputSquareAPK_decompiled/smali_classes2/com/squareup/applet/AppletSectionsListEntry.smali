.class public Lcom/squareup/applet/AppletSectionsListEntry;
.super Ljava/lang/Object;
.source "AppletSectionsListEntry.java"


# instance fields
.field protected final res:Lcom/squareup/util/Res;

.field public final section:Lcom/squareup/applet/AppletSection;

.field private final titleResId:I

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V
    .locals 1

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/applet/AppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILjava/lang/String;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/applet/AppletSection;ILjava/lang/String;Lcom/squareup/util/Res;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    .line 27
    iput-object p3, p0, Lcom/squareup/applet/AppletSectionsListEntry;->value:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/squareup/applet/AppletSectionsListEntry;->res:Lcom/squareup/util/Res;

    .line 29
    iput p2, p0, Lcom/squareup/applet/AppletSectionsListEntry;->titleResId:I

    return-void
.end method


# virtual methods
.method public getGroupingTitle()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSection()Lcom/squareup/applet/AppletSection;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListEntry;->section:Lcom/squareup/applet/AppletSection;

    return-object v0
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListEntry;->res:Lcom/squareup/util/Res;

    iget v1, p0, Lcom/squareup/applet/AppletSectionsListEntry;->titleResId:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListEntry;->value:Ljava/lang/String;

    return-object v0
.end method

.method public getValueTextObservable()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 59
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListEntry;->getValueText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 60
    :cond_0
    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
