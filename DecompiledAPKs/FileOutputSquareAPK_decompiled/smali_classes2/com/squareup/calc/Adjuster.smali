.class public abstract Lcom/squareup/calc/Adjuster;
.super Ljava/lang/Object;
.source "Adjuster.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getAggregateBaseAmount()J
    .locals 5

    .line 258
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 260
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/calc/order/Item;

    .line 261
    invoke-interface {v3}, Lcom/squareup/calc/order/Item;->baseAmount()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    return-wide v1
.end method


# virtual methods
.method public abstract calculate()V
.end method

.method public getAdjustedItemFor(Lcom/squareup/calc/order/Item;)Lcom/squareup/calc/AdjustedItem;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 98
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getAdjustedItems()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/calc/AdjustedItem;

    return-object p1
.end method

.method public abstract getAdjustedItems()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/calc/order/Item;",
            "Lcom/squareup/calc/AdjustedItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAppliedDiscounts()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAppliedTaxes()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/calc/order/Adjustment;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCollectedAmountPerDiscount()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCollectedAmountPerSurcharge()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getCollectedAmountPerTax()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/math/BigDecimal;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getItems()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Item;",
            ">;"
        }
    .end annotation
.end method

.method public getSubtotal()J
    .locals 4

    .line 234
    invoke-direct {p0}, Lcom/squareup/calc/Adjuster;->getAggregateBaseAmount()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllDiscounts()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getSurchargeAdjustedItemFor(Lcom/squareup/calc/order/Surcharge;)Lcom/squareup/calc/AdjustedItem;
    .locals 1

    .line 207
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 208
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getAdjustedItems()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/squareup/calc/Adjuster;->getSurchargeItemFor(Lcom/squareup/calc/order/Surcharge;)Lcom/squareup/calc/order/Item;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/calc/AdjustedItem;

    return-object p1
.end method

.method public abstract getSurchargeItemFor(Lcom/squareup/calc/order/Surcharge;)Lcom/squareup/calc/order/Item;
.end method

.method public abstract getSurcharges()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/calc/order/Surcharge;",
            ">;"
        }
    .end annotation
.end method

.method public getTotal()J
    .locals 4

    .line 246
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getSubtotal()J

    move-result-wide v0

    .line 247
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllSurcharges()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 248
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAdditiveTaxes()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getTotalCollectedForAdditiveTaxes()J
    .locals 5

    .line 151
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 153
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 154
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerTax()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 155
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/calc/order/Adjustment;

    invoke-interface {v3}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v3

    sget-object v4, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v3, v4, :cond_0

    .line 156
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 160
    :cond_1
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForAllDiscounts()J
    .locals 3

    .line 121
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 123
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 124
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerDiscount()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 125
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 127
    :cond_0
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForAllSurcharges()J
    .locals 3

    .line 217
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 219
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 220
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerSurcharge()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 221
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 223
    :cond_0
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForAllTaxes()J
    .locals 3

    .line 189
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 191
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerTax()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 193
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 196
    :cond_0
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForDiscount(Lcom/squareup/calc/order/Adjustment;)J
    .locals 2

    .line 107
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerDiscount()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    if-eqz p1, :cond_0

    .line 111
    invoke-virtual {p1}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public getTotalCollectedForInclusiveTaxes()J
    .locals 5

    .line 170
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 172
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 173
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerTax()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/calc/order/Adjustment;

    invoke-interface {v3}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v3

    sget-object v4, Lcom/squareup/calc/constants/InclusionType;->INCLUSIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v3, v4, :cond_0

    .line 175
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    .line 179
    :cond_1
    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getTotalCollectedForTax(Lcom/squareup/calc/order/Adjustment;)J
    .locals 2

    .line 136
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 138
    invoke-virtual {p0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerTax()Ljava/util/Map;

    move-result-object v0

    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    if-eqz p1, :cond_0

    .line 140
    invoke-virtual {p1}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method
