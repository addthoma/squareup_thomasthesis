.class public Lcom/squareup/backgroundjob/log/BackgroundJobLog;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "BackgroundJobLog.java"


# instance fields
.field public final error_message:Ljava/lang/String;

.field public final job_tag:Ljava/lang/String;

.field public final log_priority:I

.field public final stack_trace:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->BACKGROUND_JOB_LOG:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 26
    iput p1, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLog;->log_priority:I

    .line 27
    iput-object p2, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLog;->job_tag:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLog;->error_message:Ljava/lang/String;

    .line 29
    invoke-static {p4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLog;->stack_trace:Ljava/lang/String;

    return-void
.end method
