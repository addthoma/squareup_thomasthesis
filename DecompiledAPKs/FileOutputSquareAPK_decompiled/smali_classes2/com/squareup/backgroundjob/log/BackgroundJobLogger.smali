.class public Lcom/squareup/backgroundjob/log/BackgroundJobLogger;
.super Ljava/lang/Object;
.source "BackgroundJobLogger.java"

# interfaces
.implements Lcom/evernote/android/job/util/JobLogger;
.implements Lmortar/Scoped;


# static fields
.field private static final VERBOSE_LOGGING:Z


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private formattedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4

    .line 71
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->BACKGROUND_JOB:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 p1, 0x2

    aput-object p2, v1, p1

    .line 72
    invoke-static {p3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v1, p2

    const-string p1, "%s -> tag=%s. %s \n%s"

    .line 71
    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public log(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_1

    const/4 v0, 0x6

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/backgroundjob/log/BackgroundJobLog;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/backgroundjob/log/BackgroundJobLog;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    :cond_1
    const-string v0, "Job requires"

    .line 60
    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 61
    iget-object v0, p0, Lcom/squareup/backgroundjob/log/BackgroundJobLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/backgroundjob/log/BackgroundJobLog;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/backgroundjob/log/BackgroundJobLog;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 41
    invoke-static {p0}, Lcom/evernote/android/job/JobConfig;->addLogger(Lcom/evernote/android/job/util/JobLogger;)Z

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 45
    invoke-static {p0}, Lcom/evernote/android/job/JobConfig;->removeLogger(Lcom/evernote/android/job/util/JobLogger;)V

    return-void
.end method
