.class public abstract Lcom/squareup/backgroundjob/BackgroundJob;
.super Lcom/evernote/android/job/Job;
.source "BackgroundJob.java"


# instance fields
.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;


# direct methods
.method public constructor <init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/evernote/android/job/Job;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/backgroundjob/BackgroundJob;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    return-void
.end method

.method private logExecuting(Lcom/squareup/backgroundjob/JobParams;)V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 69
    invoke-interface {p1}, Lcom/squareup/backgroundjob/JobParams;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-interface {p1}, Lcom/squareup/backgroundjob/JobParams;->getTag()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-interface {p1}, Lcom/squareup/backgroundjob/JobParams;->getScheduledAt()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 70
    invoke-static {}, Lcom/evernote/android/job/JobConfig;->getClock()Lcom/evernote/android/job/util/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/evernote/android/job/util/Clock;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 v1, 0x3

    aput-object p1, v0, v1

    const-string p1, "Executing background job: id=%d, tag=%s, scheduled_at=%d, executed_at=%d"

    .line 68
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected onFinishRunJob(Lcom/squareup/backgroundjob/JobParams;)V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/backgroundjob/BackgroundJob;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-interface {v0, p1}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;->hideNotificationFor(Lcom/squareup/backgroundjob/JobParams;)V

    return-void
.end method

.method protected onRunJob(Lcom/evernote/android/job/Job$Params;)Lcom/evernote/android/job/Job$Result;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/backgroundjob/RealParams;

    invoke-direct {v0, p1}, Lcom/squareup/backgroundjob/RealParams;-><init>(Lcom/evernote/android/job/Job$Params;)V

    .line 39
    invoke-virtual {p0, v0}, Lcom/squareup/backgroundjob/BackgroundJob;->onStartRunJob(Lcom/squareup/backgroundjob/JobParams;)V

    .line 40
    invoke-virtual {p0, v0}, Lcom/squareup/backgroundjob/BackgroundJob;->runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;

    move-result-object p1

    .line 41
    invoke-virtual {p0, v0}, Lcom/squareup/backgroundjob/BackgroundJob;->onFinishRunJob(Lcom/squareup/backgroundjob/JobParams;)V

    return-object p1
.end method

.method protected onStartRunJob(Lcom/squareup/backgroundjob/JobParams;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/backgroundjob/BackgroundJob;->logExecuting(Lcom/squareup/backgroundjob/JobParams;)V

    return-void
.end method

.method public abstract runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
.end method
