.class public Lcom/squareup/backgroundjob/mock/MockParams;
.super Ljava/lang/Object;
.source "MockParams.java"

# interfaces
.implements Lcom/squareup/backgroundjob/JobParams;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    }
.end annotation


# instance fields
.field private final endMs:J

.field private final extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

.field private final id:I

.field private final scheduledAt:J

.field private final startMs:J

.field private final tag:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILjava/lang/String;JJJLcom/evernote/android/job/util/support/PersistableBundleCompat;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lcom/squareup/backgroundjob/mock/MockParams;->id:I

    .line 28
    iput-object p2, p0, Lcom/squareup/backgroundjob/mock/MockParams;->tag:Ljava/lang/String;

    .line 29
    iput-wide p3, p0, Lcom/squareup/backgroundjob/mock/MockParams;->startMs:J

    .line 30
    iput-wide p5, p0, Lcom/squareup/backgroundjob/mock/MockParams;->endMs:J

    .line 31
    iput-wide p7, p0, Lcom/squareup/backgroundjob/mock/MockParams;->scheduledAt:J

    .line 32
    iput-object p9, p0, Lcom/squareup/backgroundjob/mock/MockParams;->extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/String;JJJLcom/evernote/android/job/util/support/PersistableBundleCompat;Lcom/squareup/backgroundjob/mock/MockParams$1;)V
    .locals 0

    .line 16
    invoke-direct/range {p0 .. p9}, Lcom/squareup/backgroundjob/mock/MockParams;-><init>(ILjava/lang/String;JJJLcom/evernote/android/job/util/support/PersistableBundleCompat;)V

    return-void
.end method


# virtual methods
.method public getEndMs()J
    .locals 2

    .line 48
    iget-wide v0, p0, Lcom/squareup/backgroundjob/mock/MockParams;->endMs:J

    return-wide v0
.end method

.method public getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockParams;->extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/backgroundjob/mock/MockParams;->id:I

    return v0
.end method

.method public getScheduledAt()J
    .locals 2

    .line 52
    iget-wide v0, p0, Lcom/squareup/backgroundjob/mock/MockParams;->scheduledAt:J

    return-wide v0
.end method

.method public getStartMs()J
    .locals 2

    .line 44
    iget-wide v0, p0, Lcom/squareup/backgroundjob/mock/MockParams;->startMs:J

    return-wide v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/backgroundjob/mock/MockParams;->tag:Ljava/lang/String;

    return-object v0
.end method
