.class public Lcom/squareup/backgroundjob/mock/MockParams$Builder;
.super Ljava/lang/Object;
.source "MockParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/backgroundjob/mock/MockParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private endMs:J

.field private extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

.field private id:I

.field private scheduledAt:J

.field private startMs:J

.field private tag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 61
    iput v0, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->id:I

    const-string v0, ""

    .line 62
    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->tag:Ljava/lang/String;

    const-wide/16 v0, -0x1

    .line 63
    iput-wide v0, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->startMs:J

    .line 64
    iput-wide v0, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->endMs:J

    .line 65
    iput-wide v0, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->scheduledAt:J

    .line 66
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    iput-object v0, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/backgroundjob/mock/MockParams;
    .locals 12

    .line 99
    new-instance v11, Lcom/squareup/backgroundjob/mock/MockParams;

    iget v1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->id:I

    iget-object v2, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->tag:Ljava/lang/String;

    iget-wide v3, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->startMs:J

    iget-wide v5, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->endMs:J

    iget-wide v7, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->scheduledAt:J

    iget-object v9, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/backgroundjob/mock/MockParams;-><init>(ILjava/lang/String;JJJLcom/evernote/android/job/util/support/PersistableBundleCompat;Lcom/squareup/backgroundjob/mock/MockParams$1;)V

    return-object v11
.end method

.method public endMs(J)Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    .locals 0

    .line 84
    iput-wide p1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->endMs:J

    return-object p0
.end method

.method public extras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->extras:Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    return-object p0
.end method

.method public id(I)Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    .locals 0

    .line 69
    iput p1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->id:I

    return-object p0
.end method

.method public scheduledAt(J)Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    .locals 0

    .line 89
    iput-wide p1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->scheduledAt:J

    return-object p0
.end method

.method public startMs(J)Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    .locals 0

    .line 79
    iput-wide p1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->startMs:J

    return-object p0
.end method

.method public tag(Ljava/lang/String;)Lcom/squareup/backgroundjob/mock/MockParams$Builder;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/backgroundjob/mock/MockParams$Builder;->tag:Ljava/lang/String;

    return-object p0
.end method
