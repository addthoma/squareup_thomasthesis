.class public abstract Lcom/squareup/charts/piechart/renderers/PieChartRenderer;
.super Ljava/lang/Object;
.source "PieChartRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/charts/piechart/renderers/PieChartRenderer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPieChartRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PieChartRenderer.kt\ncom/squareup/charts/piechart/renderers/PieChartRenderer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,128:1\n704#2:129\n777#2,2:130\n1713#2,14:132\n*E\n*S KotlinDebug\n*F\n+ 1 PieChartRenderer.kt\ncom/squareup/charts/piechart/renderers/PieChartRenderer\n*L\n119#1:129\n119#1,2:130\n119#1,14:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008&\u0018\u0000 $2\u00020\u0001:\u0001$B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rH\u0002J\u001e\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0013H\u0002J0\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\t2\u0006\u0010\u001a\u001a\u00020\u001bH\u0004J,\u0010\u001c\u001a\u00020\u000b2\u0006\u0010\u001d\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0013J.\u0010\u001f\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00132\u0006\u0010 \u001a\u00020\tH&J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\u0011H\u0004J\u0010\u0010\"\u001a\u00020\t2\u0006\u0010!\u001a\u00020\u0011H\u0002J\u0008\u0010#\u001a\u00020\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0004X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "",
        "()V",
        "box",
        "Landroid/graphics/RectF;",
        "drawableArea",
        "getDrawableArea",
        "()Landroid/graphics/RectF;",
        "maxSliceThickness",
        "",
        "calculateChartArea",
        "",
        "width",
        "",
        "height",
        "calculateMaxSliceThickness",
        "backgroundSlice",
        "Lcom/squareup/charts/piechart/Slice;",
        "slices",
        "",
        "drawSlice",
        "canvas",
        "Landroid/graphics/Canvas;",
        "sliceBox",
        "start",
        "angle",
        "slicePaint",
        "Landroid/graphics/Paint;",
        "notifyRemeasure",
        "measuredWidth",
        "measuredHeight",
        "onDrawPieChart",
        "chartProgress",
        "slice",
        "sliceThicknessInPx",
        "updateDrawableArea",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/charts/piechart/renderers/PieChartRenderer$Companion;

.field public static final DEFAULT_SLICE_THICKNESS:F = 15.0f


# instance fields
.field private box:Landroid/graphics/RectF;

.field private final drawableArea:Landroid/graphics/RectF;

.field private maxSliceThickness:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->Companion:Lcom/squareup/charts/piechart/renderers/PieChartRenderer$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x41700000    # 15.0f

    .line 18
    iput v0, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->maxSliceThickness:F

    .line 20
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->drawableArea:Landroid/graphics/RectF;

    return-void
.end method

.method private final calculateChartArea(II)V
    .locals 4

    const/4 v0, 0x0

    if-ne p1, p2, :cond_0

    .line 70
    new-instance v1, Landroid/graphics/RectF;

    int-to-float p1, p1

    int-to-float p2, p2

    invoke-direct {v1, v0, v0, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    goto :goto_1

    .line 72
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    if-le p2, v1, :cond_1

    sub-int v3, p2, v1

    int-to-float v3, v3

    div-float/2addr v3, v2

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-le p1, v1, :cond_2

    sub-int v0, p1, v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 86
    :cond_2
    new-instance v1, Landroid/graphics/RectF;

    int-to-float p1, p1

    sub-float/2addr p1, v0

    int-to-float p2, p2

    sub-float/2addr p2, v3

    invoke-direct {v1, v0, v3, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    :goto_1
    return-void
.end method

.method private final calculateMaxSliceThickness(Lcom/squareup/charts/piechart/Slice;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/charts/piechart/Slice;",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)V"
        }
    .end annotation

    .line 117
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/Slice;->getLength()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/charts/piechart/Slice;->getThickness()F

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 119
    :goto_0
    check-cast p2, Ljava/lang/Iterable;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 130
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/charts/piechart/Slice;

    .line 119
    invoke-virtual {v3}, Lcom/squareup/charts/piechart/Slice;->getLength()F

    move-result v3

    cmpl-float v3, v3, v1

    if-lez v3, :cond_2

    const/4 v3, 0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_1

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 132
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    .line 133
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 p2, 0x0

    move-object v0, p2

    goto :goto_3

    .line 134
    :cond_4
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 135
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_3

    .line 136
    :cond_5
    move-object v1, v0

    check-cast v1, Lcom/squareup/charts/piechart/Slice;

    .line 119
    invoke-virtual {v1}, Lcom/squareup/charts/piechart/Slice;->getThickness()F

    move-result v1

    .line 138
    :cond_6
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 139
    move-object v3, v2

    check-cast v3, Lcom/squareup/charts/piechart/Slice;

    .line 119
    invoke-virtual {v3}, Lcom/squareup/charts/piechart/Slice;->getThickness()F

    move-result v3

    .line 140
    invoke-static {v1, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-gez v4, :cond_7

    move-object v0, v2

    move v1, v3

    .line 144
    :cond_7
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 145
    :goto_3
    check-cast v0, Lcom/squareup/charts/piechart/Slice;

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/squareup/charts/piechart/Slice;->getThickness()F

    move-result p2

    goto :goto_4

    :cond_8
    move p2, p1

    .line 121
    :goto_4
    invoke-static {p1, p2}, Ljava/lang/Math;->max(FF)F

    move-result p1

    iput p1, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->maxSliceThickness:F

    return-void
.end method

.method private final sliceThicknessInPx(Lcom/squareup/charts/piechart/Slice;)F
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    const-string v1, "box"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/charts/piechart/Slice;->getThickness()F

    move-result p1

    mul-float v0, v0, p1

    const/high16 p1, 0x43480000    # 200.0f

    div-float/2addr v0, p1

    return v0
.end method

.method private final updateDrawableArea()Landroid/graphics/RectF;
    .locals 4

    .line 104
    iget-object v0, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    const-string v1, "box"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v2, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->maxSliceThickness:F

    mul-float v0, v0, v2

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v0, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v0, v2

    .line 105
    iget-object v2, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->drawableArea:Landroid/graphics/RectF;

    .line 106
    iget-object v3, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v0

    iput v3, v2, Landroid/graphics/RectF;->left:F

    .line 107
    iget-object v3, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    if-nez v3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v3, v0

    iput v3, v2, Landroid/graphics/RectF;->right:F

    .line 108
    iget-object v3, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    if-nez v3, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v0

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 109
    iget-object v3, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->box:Landroid/graphics/RectF;

    if-nez v3, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget v1, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v0

    iput v1, v2, Landroid/graphics/RectF;->bottom:F

    return-object v2
.end method


# virtual methods
.method protected final drawSlice(Landroid/graphics/Canvas;Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sliceBox"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "slicePaint"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p5

    .line 53
    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    return-void
.end method

.method protected final getDrawableArea()Landroid/graphics/RectF;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->drawableArea:Landroid/graphics/RectF;

    return-object v0
.end method

.method public final notifyRemeasure(IILcom/squareup/charts/piechart/Slice;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/squareup/charts/piechart/Slice;",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)V"
        }
    .end annotation

    const-string v0, "backgroundSlice"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "slices"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->calculateChartArea(II)V

    .line 39
    invoke-direct {p0, p3, p4}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->calculateMaxSliceThickness(Lcom/squareup/charts/piechart/Slice;Ljava/util/List;)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->updateDrawableArea()Landroid/graphics/RectF;

    return-void
.end method

.method public abstract onDrawPieChart(Landroid/graphics/Canvas;Lcom/squareup/charts/piechart/Slice;Ljava/util/List;F)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Lcom/squareup/charts/piechart/Slice;",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;F)V"
        }
    .end annotation
.end method

.method protected final slicePaint(Lcom/squareup/charts/piechart/Slice;)Landroid/graphics/Paint;
    .locals 1

    const-string v0, "slice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1}, Lcom/squareup/charts/piechart/Slice;->getColor()Lcom/squareup/charts/piechart/SliceColor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/charts/piechart/SliceColor;->obtainPaint()Landroid/graphics/Paint;

    move-result-object v0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/charts/piechart/renderers/PieChartRenderer;->sliceThicknessInPx(Lcom/squareup/charts/piechart/Slice;)F

    move-result p1

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-object v0
.end method
