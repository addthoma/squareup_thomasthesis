.class public final Lcom/squareup/charts/piechart/SliceColor$GradientColor;
.super Lcom/squareup/charts/piechart/SliceColor;
.source "SliceColor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/charts/piechart/SliceColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GradientColor"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSliceColor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SliceColor.kt\ncom/squareup/charts/piechart/SliceColor$GradientColor\n*L\n1#1,53:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0007\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0014\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0008\u0010\u0015\u001a\u00020\rH\u0002J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\rX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000b\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/SliceColor$GradientColor;",
        "Lcom/squareup/charts/piechart/SliceColor;",
        "startColor",
        "",
        "endColor",
        "angle",
        "",
        "(IIF)V",
        "getAngle",
        "()F",
        "getEndColor",
        "()I",
        "shader",
        "Landroid/graphics/LinearGradient;",
        "getShader$public_release",
        "()Landroid/graphics/LinearGradient;",
        "getStartColor",
        "component1",
        "component2",
        "component3",
        "copy",
        "createGradient",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final angle:F

.field private final endColor:I

.field private final shader:Landroid/graphics/LinearGradient;

.field private final startColor:I


# direct methods
.method public constructor <init>(IIF)V
    .locals 1

    const/4 v0, 0x0

    .line 19
    invoke-direct {p0, v0}, Lcom/squareup/charts/piechart/SliceColor;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    iput p2, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    iput p3, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    .line 21
    iget p1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    const/high16 p2, -0x3c4c0000    # -360.0f

    cmpl-float p2, p1, p2

    if-ltz p2, :cond_0

    const/high16 p2, 0x43b40000    # 360.0f

    cmpg-float p1, p1, p2

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 24
    invoke-direct {p0}, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->createGradient()Landroid/graphics/LinearGradient;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->shader:Landroid/graphics/LinearGradient;

    return-void

    .line 21
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Angle ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string p2, ") has to be between -360 to 360 degrees."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public synthetic constructor <init>(IIFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/high16 p3, 0x42340000    # 45.0f

    .line 18
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/charts/piechart/SliceColor$GradientColor;-><init>(IIF)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/charts/piechart/SliceColor$GradientColor;IIFILjava/lang/Object;)Lcom/squareup/charts/piechart/SliceColor$GradientColor;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->copy(IIF)Lcom/squareup/charts/piechart/SliceColor$GradientColor;

    move-result-object p0

    return-object p0
.end method

.method private final createGradient()Landroid/graphics/LinearGradient;
    .locals 12

    .line 27
    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 30
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x43fa0000    # 500.0f

    mul-float v7, v2, v3

    .line 31
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float v8, v0, v3

    .line 33
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v9, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    iget v10, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    sget-object v11, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v11}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    return v0
.end method

.method public final component3()F
    .locals 1

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    return v0
.end method

.method public final copy(IIF)Lcom/squareup/charts/piechart/SliceColor$GradientColor;
    .locals 1

    new-instance v0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/charts/piechart/SliceColor$GradientColor;-><init>(IIF)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/charts/piechart/SliceColor$GradientColor;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/charts/piechart/SliceColor$GradientColor;

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    iget v1, p1, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    iget v1, p1, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    iget p1, p1, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    invoke-static {v0, p1}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAngle()F
    .locals 1

    .line 18
    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    return v0
.end method

.method public final getEndColor()I
    .locals 1

    .line 17
    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    return v0
.end method

.method public final getShader$public_release()Landroid/graphics/LinearGradient;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->shader:Landroid/graphics/LinearGradient;

    return-object v0
.end method

.method public final getStartColor()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    invoke-static {v1}, L$r8$java8methods$utility$Float$hashCode$IF;->hashCode(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GradientColor(startColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->startColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", endColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->endColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", angle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/charts/piechart/SliceColor$GradientColor;->angle:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
