.class public final Lcom/squareup/charts/piechart/PieChart$Update;
.super Ljava/lang/Object;
.source "PieChart.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/charts/piechart/PieChart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Update"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0012\u0018\u00002\u00020\u0001B-\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\"\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\u0008\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R \u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001a\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/charts/piechart/PieChart$Update;",
        "",
        "backgroundSlice",
        "Lcom/squareup/charts/piechart/Slice;",
        "slices",
        "",
        "animator",
        "Lcom/squareup/charts/piechart/animators/PieChartAnimator;",
        "renderer",
        "Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "(Lcom/squareup/charts/piechart/Slice;Ljava/util/List;Lcom/squareup/charts/piechart/animators/PieChartAnimator;Lcom/squareup/charts/piechart/renderers/PieChartRenderer;)V",
        "getAnimator",
        "()Lcom/squareup/charts/piechart/animators/PieChartAnimator;",
        "setAnimator",
        "(Lcom/squareup/charts/piechart/animators/PieChartAnimator;)V",
        "getBackgroundSlice",
        "()Lcom/squareup/charts/piechart/Slice;",
        "setBackgroundSlice",
        "(Lcom/squareup/charts/piechart/Slice;)V",
        "getRenderer",
        "()Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
        "setRenderer",
        "(Lcom/squareup/charts/piechart/renderers/PieChartRenderer;)V",
        "getSlices",
        "()Ljava/util/List;",
        "setSlices",
        "(Ljava/util/List;)V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

.field private backgroundSlice:Lcom/squareup/charts/piechart/Slice;

.field private renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

.field private slices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/charts/piechart/Slice;Ljava/util/List;Lcom/squareup/charts/piechart/animators/PieChartAnimator;Lcom/squareup/charts/piechart/renderers/PieChartRenderer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/charts/piechart/Slice;",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;",
            "Lcom/squareup/charts/piechart/animators/PieChartAnimator;",
            "Lcom/squareup/charts/piechart/renderers/PieChartRenderer;",
            ")V"
        }
    .end annotation

    const-string v0, "backgroundSlice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "slices"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "animator"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart$Update;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    iput-object p2, p0, Lcom/squareup/charts/piechart/PieChart$Update;->slices:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/charts/piechart/PieChart$Update;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    iput-object p4, p0, Lcom/squareup/charts/piechart/PieChart$Update;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    return-void
.end method


# virtual methods
.method public final getAnimator()Lcom/squareup/charts/piechart/animators/PieChartAnimator;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart$Update;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    return-object v0
.end method

.method public final getBackgroundSlice()Lcom/squareup/charts/piechart/Slice;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart$Update;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    return-object v0
.end method

.method public final getRenderer()Lcom/squareup/charts/piechart/renderers/PieChartRenderer;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart$Update;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    return-object v0
.end method

.method public final getSlices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/charts/piechart/PieChart$Update;->slices:Ljava/util/List;

    return-object v0
.end method

.method public final setAnimator(Lcom/squareup/charts/piechart/animators/PieChartAnimator;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart$Update;->animator:Lcom/squareup/charts/piechart/animators/PieChartAnimator;

    return-void
.end method

.method public final setBackgroundSlice(Lcom/squareup/charts/piechart/Slice;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart$Update;->backgroundSlice:Lcom/squareup/charts/piechart/Slice;

    return-void
.end method

.method public final setRenderer(Lcom/squareup/charts/piechart/renderers/PieChartRenderer;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart$Update;->renderer:Lcom/squareup/charts/piechart/renderers/PieChartRenderer;

    return-void
.end method

.method public final setSlices(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/charts/piechart/Slice;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/squareup/charts/piechart/PieChart$Update;->slices:Ljava/util/List;

    return-void
.end method
