.class public final Lcom/squareup/account/PendingPreferencesCache_Factory;
.super Ljava/lang/Object;
.source "PendingPreferencesCache_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/account/PendingPreferencesCache;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final crashReporterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final dataDirectoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final fileFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final keystoreEncryptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            ">;"
        }
    .end annotation
.end field

.field private final playServicesVersionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;"
        }
    .end annotation
.end field

.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedPreferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->dataDirectoryProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->fileFactoryProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->posBuildProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->gsonProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->crashReporterProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->keystoreEncryptorProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->playServicesVersionsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/account/PendingPreferencesCache_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/AppDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistent/PersistentFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CrashReporter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ">;)",
            "Lcom/squareup/account/PendingPreferencesCache_Factory;"
        }
    .end annotation

    .line 81
    new-instance v12, Lcom/squareup/account/PendingPreferencesCache_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/account/PendingPreferencesCache_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/AppDelegate;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Landroid/content/SharedPreferences;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/util/PosBuild;Lcom/google/gson/Gson;Lcom/squareup/log/CrashReporter;Lcom/squareup/encryption/KeystoreEncryptor;Lcom/squareup/firebase/versions/PlayServicesVersions;)Lcom/squareup/account/PendingPreferencesCache;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/AppDelegate;",
            "Ljava/io/File;",
            "Lcom/squareup/persistent/PersistentFactory;",
            "Landroid/content/SharedPreferences;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Landroid/app/Application;",
            "Lcom/squareup/util/PosBuild;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/log/CrashReporter;",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ")",
            "Lcom/squareup/account/PendingPreferencesCache;"
        }
    .end annotation

    .line 89
    new-instance v12, Lcom/squareup/account/PendingPreferencesCache;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/account/PendingPreferencesCache;-><init>(Lcom/squareup/AppDelegate;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Landroid/content/SharedPreferences;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/util/PosBuild;Lcom/google/gson/Gson;Lcom/squareup/log/CrashReporter;Lcom/squareup/encryption/KeystoreEncryptor;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/account/PendingPreferencesCache;
    .locals 12

    .line 71
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->appDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/AppDelegate;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->dataDirectoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/io/File;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->fileFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/persistent/PersistentFactory;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->sharedPreferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/content/SharedPreferences;

    iget-object v5, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->analyticsProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/PosBuild;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->crashReporterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/log/CrashReporter;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->keystoreEncryptorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/encryption/KeystoreEncryptor;

    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_Factory;->playServicesVersionsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/firebase/versions/PlayServicesVersions;

    invoke-static/range {v1 .. v11}, Lcom/squareup/account/PendingPreferencesCache_Factory;->newInstance(Lcom/squareup/AppDelegate;Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Landroid/content/SharedPreferences;Ljavax/inject/Provider;Landroid/app/Application;Lcom/squareup/util/PosBuild;Lcom/google/gson/Gson;Lcom/squareup/log/CrashReporter;Lcom/squareup/encryption/KeystoreEncryptor;Lcom/squareup/firebase/versions/PlayServicesVersions;)Lcom/squareup/account/PendingPreferencesCache;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/account/PendingPreferencesCache_Factory;->get()Lcom/squareup/account/PendingPreferencesCache;

    move-result-object v0

    return-object v0
.end method
