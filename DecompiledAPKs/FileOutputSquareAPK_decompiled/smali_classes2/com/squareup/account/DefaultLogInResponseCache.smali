.class public Lcom/squareup/account/DefaultLogInResponseCache;
.super Ljava/lang/Object;
.source "DefaultLogInResponseCache.java"

# interfaces
.implements Lcom/squareup/account/LogInResponseCache;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/DefaultLogInResponseCache$NonMatchingCipherTextException;,
        Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    }
.end annotation


# static fields
.field public static final CURRENT_USER_JSON:Ljava/lang/String; = "current-user.json"

.field private static final ENCRYPTED_USER_JSON:Ljava/lang/String; = "encrypted-current-user.enc"

.field static final SHARED_PREFS_KEY_NAME_PREFER_ENCRYPTION:Ljava/lang/String; = "should-prefer-encryption"

.field private static final UNSET:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;


# instance fields
.field private final analytics:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

.field private final context:Landroid/app/Application;

.field private final crashReporter:Lcom/squareup/log/CrashReporter;

.field private final dataDirectory:Ljava/io/File;

.field private encryptedFileCache:Lcom/squareup/persistent/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/persistent/Persistent<",
            "[B>;"
        }
    .end annotation
.end field

.field private final fileFactory:Lcom/squareup/persistent/PersistentFactory;

.field private final gson:Lcom/google/gson/Gson;

.field private final keyStoreEncryptor:Lcom/squareup/encryption/KeystoreEncryptor;

.field private plainTextFileCache:Lcom/squareup/persistent/Persistent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/persistent/Persistent<",
            "Lcom/squareup/account/DefaultLogInResponseCache$CachedData;",
            ">;"
        }
    .end annotation
.end field

.field private final playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

.field private final posBuild:Lcom/squareup/util/PosBuild;

.field protected service:Lcom/squareup/accountstatus/QuietServerPreferences;

.field private final sharedPreferences:Landroid/content/SharedPreferences;

.field private shouldLogWarningToBugsnag:Z

.field private useEncryptedCache:Z

.field private verboseLogging:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 56
    new-instance v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    sget-object v1, Lcom/squareup/server/accountstatus/AccountStatusService;->EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;

    const-string v2, ""

    invoke-direct {v0, v2, v1}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    sput-object v0, Lcom/squareup/account/DefaultLogInResponseCache;->UNSET:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Lcom/squareup/persistent/PersistentFactory;Landroid/content/SharedPreferences;Lcom/squareup/encryption/KeystoreEncryptor;Landroid/app/Application;Ljavax/inject/Provider;Lcom/squareup/util/PosBuild;Lcom/google/gson/Gson;Lcom/squareup/log/CrashReporter;Lcom/squareup/firebase/versions/PlayServicesVersions;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/persistent/PersistentFactory;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/encryption/KeystoreEncryptor;",
            "Landroid/app/Application;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Lcom/squareup/util/PosBuild;",
            "Lcom/google/gson/Gson;",
            "Lcom/squareup/log/CrashReporter;",
            "Lcom/squareup/firebase/versions/PlayServicesVersions;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 75
    iput-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->verboseLogging:Z

    const/4 v0, 0x1

    .line 76
    iput-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->shouldLogWarningToBugsnag:Z

    .line 83
    sget-object v0, Lcom/squareup/account/DefaultLogInResponseCache;->UNSET:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iput-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    .line 93
    iput-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->dataDirectory:Ljava/io/File;

    .line 94
    iput-object p2, p0, Lcom/squareup/account/DefaultLogInResponseCache;->fileFactory:Lcom/squareup/persistent/PersistentFactory;

    .line 95
    iput-object p3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 96
    iput-object p5, p0, Lcom/squareup/account/DefaultLogInResponseCache;->context:Landroid/app/Application;

    .line 97
    iput-object p7, p0, Lcom/squareup/account/DefaultLogInResponseCache;->posBuild:Lcom/squareup/util/PosBuild;

    .line 98
    iput-object p6, p0, Lcom/squareup/account/DefaultLogInResponseCache;->analytics:Ljavax/inject/Provider;

    .line 99
    iput-object p8, p0, Lcom/squareup/account/DefaultLogInResponseCache;->gson:Lcom/google/gson/Gson;

    .line 100
    iput-object p9, p0, Lcom/squareup/account/DefaultLogInResponseCache;->crashReporter:Lcom/squareup/log/CrashReporter;

    .line 101
    iput-object p4, p0, Lcom/squareup/account/DefaultLogInResponseCache;->keyStoreEncryptor:Lcom/squareup/encryption/KeystoreEncryptor;

    .line 102
    iput-object p10, p0, Lcom/squareup/account/DefaultLogInResponseCache;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    return-void
.end method

.method private decryptionIsValid(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 381
    :try_start_0
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->keyStoreEncryptor:Lcom/squareup/encryption/KeystoreEncryptor;

    iget-object v4, p0, Lcom/squareup/account/DefaultLogInResponseCache;->encryptedFileCache:Lcom/squareup/persistent/Persistent;

    .line 382
    invoke-interface {v4}, Lcom/squareup/persistent/Persistent;->getSynchronous()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    invoke-interface {v3, v4}, Lcom/squareup/encryption/KeystoreEncryptor;->decrypt([B)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 383
    iget-object v3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v3, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v0, "Attempt to test decrypt the cached ASR failed."

    .line 386
    invoke-direct {p0, p1, v0}, Lcom/squareup/account/DefaultLogInResponseCache;->logLogLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    new-array p1, v1, [Ljava/lang/Object;

    const-string v1, "Freshly encrypted user json decrypts successfully"

    .line 391
    invoke-direct {p0, v1, p1}, Lcom/squareup/account/DefaultLogInResponseCache;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    return v0
.end method

.method private deletePlainTextCache()V
    .locals 2

    .line 230
    invoke-virtual {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    :try_start_0
    invoke-virtual {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Attempt to delete the plaintext file failed."

    .line 235
    invoke-direct {p0, v0, v1}, Lcom/squareup/account/DefaultLogInResponseCache;->logLogLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void
.end method

.method private getCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 1

    .line 313
    iget-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->keyStoreEncryptor:Lcom/squareup/encryption/KeystoreEncryptor;

    if-eqz v0, :cond_0

    .line 314
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getDecryptedCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 317
    :cond_0
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v0

    return-object v0
.end method

.method private getDecryptedCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 5

    const/4 v0, 0x0

    .line 328
    :try_start_0
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->encryptedFileCache:Lcom/squareup/persistent/Persistent;

    invoke-interface {v1}, Lcom/squareup/persistent/Persistent;->getSynchronous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    if-nez v1, :cond_0

    return-object v0

    .line 333
    :cond_0
    array-length v2, v1

    const/16 v3, 0xc

    const/4 v4, 0x0

    if-ge v2, v3, :cond_1

    .line 334
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Ciphertext did not meet expected length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1, v4}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3}, Lcom/squareup/account/DefaultLogInResponseCache;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    :cond_1
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->keyStoreEncryptor:Lcom/squareup/encryption/KeystoreEncryptor;

    invoke-interface {v3, v1}, Lcom/squareup/encryption/KeystoreEncryptor;->decrypt([B)[B

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    const-string v1, "Decryption of plaintext cache successful"

    new-array v3, v4, [Ljava/lang/Object;

    .line 339
    invoke-direct {p0, v1, v3}, Lcom/squareup/account/DefaultLogInResponseCache;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 341
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->gson:Lcom/google/gson/Gson;

    const-class v3, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    invoke-virtual {v1, v2, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    .line 342
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v2

    if-nez v2, :cond_2

    return-object v1

    .line 352
    :cond_2
    invoke-virtual {v1, v2}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    return-object v1

    .line 355
    :cond_3
    new-instance v1, Lcom/squareup/account/DefaultLogInResponseCache$NonMatchingCipherTextException;

    const-string v2, "Decrypted text did not match plaintext"

    invoke-direct {v1, p0, v2, v0}, Lcom/squareup/account/DefaultLogInResponseCache$NonMatchingCipherTextException;-><init>(Lcom/squareup/account/DefaultLogInResponseCache;Ljava/lang/String;Lcom/squareup/account/DefaultLogInResponseCache$1;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    const-string v2, "Attempt to decrypt the cached ASR data failed."

    .line 358
    invoke-direct {p0, v1, v2}, Lcom/squareup/account/DefaultLogInResponseCache;->logLogLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-object v0
.end method

.method private getEncryptedCache()Lcom/squareup/persistent/Persistent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/persistent/Persistent<",
            "[B>;"
        }
    .end annotation

    .line 213
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->fileFactory:Lcom/squareup/persistent/PersistentFactory;

    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getEncryptedFile()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/persistent/PersistentFactory;->getByteFile(Ljava/io/File;)Lcom/squareup/persistent/Persistent;

    move-result-object v0

    return-object v0
.end method

.method private getEncryptedFile()Ljava/io/File;
    .locals 3

    .line 221
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->dataDirectory:Ljava/io/File;

    const-string v2, "encrypted-current-user.enc"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private getPlainTextCache(Ljava/lang/Class;)Lcom/squareup/persistent/Persistent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/persistent/Persistent<",
            "TT;>;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->fileFactory:Lcom/squareup/persistent/PersistentFactory;

    invoke-virtual {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextFile()Ljava/io/File;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/persistent/PersistentFactory;->getJsonFile(Ljava/io/File;Ljava/lang/reflect/Type;)Lcom/squareup/persistent/Persistent;

    move-result-object p1

    return-object p1
.end method

.method private getPlainTextCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 1

    .line 367
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->plainTextFileCache:Lcom/squareup/persistent/Persistent;

    invoke-interface {v0}, Lcom/squareup/persistent/Persistent;->getSynchronous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    return-object v0
.end method

.method private varargs infoLog(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 477
    iget-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->verboseLogging:Z

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->analytics:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/LoginResponseCacheEvent;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 480
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const-string v3, "DefaultLogInResponseCache: %s"

    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/v1/LoginResponseCacheEvent;-><init>(Ljava/lang/String;)V

    .line 479
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 482
    :cond_0
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method private logLogLog(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 8

    .line 490
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v2, 0x1

    aput-object v4, v0, v2

    const-string v2, "%s - %s"

    .line 491
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 494
    iget-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->shouldLogWarningToBugsnag:Z

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->context:Landroid/app/Application;

    iget-object v2, p0, Lcom/squareup/account/DefaultLogInResponseCache;->crashReporter:Lcom/squareup/log/CrashReporter;

    iget-object v3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->posBuild:Lcom/squareup/util/PosBuild;

    iget-object v5, p0, Lcom/squareup/account/DefaultLogInResponseCache;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    invoke-static {v0, v2, v3, v5}, Lcom/squareup/log/CrashReportingLogger;->logNonInjectedInfo(Landroid/app/Application;Lcom/squareup/log/CrashReporter;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    .line 496
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->crashReporter:Lcom/squareup/log/CrashReporter;

    const-string v2, "eventId"

    invoke-interface {v0, v2, v4}, Lcom/squareup/log/CrashReporter;->logToMiscTab(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->crashReporter:Lcom/squareup/log/CrashReporter;

    invoke-interface {v0, p1}, Lcom/squareup/log/CrashReporter;->warningThrowable(Ljava/lang/Throwable;)V

    .line 500
    iput-boolean v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->shouldLogWarningToBugsnag:Z

    .line 503
    :cond_0
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->analytics:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/account/LoginResponseCacheErrorEvent;

    .line 504
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-boolean v5, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    .line 505
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getEncryptedFile()Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    move-object v1, v7

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/account/LoginResponseCacheErrorEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 504
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private requireEncryptedFileCache()Lcom/squareup/persistent/Persistent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/persistent/Persistent<",
            "[B>;"
        }
    .end annotation

    .line 445
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->encryptedFileCache:Lcom/squareup/persistent/Persistent;

    if-eqz v0, :cond_0

    return-object v0

    .line 446
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You forgot to call init()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private requireFileCache()Lcom/squareup/persistent/Persistent;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/persistent/Persistent<",
            "Lcom/squareup/account/DefaultLogInResponseCache$CachedData;",
            ">;"
        }
    .end annotation

    .line 435
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->plainTextFileCache:Lcom/squareup/persistent/Persistent;

    if-eqz v0, :cond_0

    return-object v0

    .line 436
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You forgot to call init()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static stripNonLockoutNotifications(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 4

    .line 461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 462
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 463
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->notifications:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/Notification;

    .line 464
    iget-object v3, v2, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 465
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 470
    :cond_1
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 471
    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v0

    .line 472
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    .line 473
    invoke-static {p0, v0}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->access$000(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object p0

    return-object p0
.end method

.method private unearthAndSanitizeLogin()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;
    .locals 5

    const/4 v0, 0x0

    .line 112
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getCachedData()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 120
    iget-object v2, v1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-nez v2, :cond_1

    .line 121
    const-class v2, Lcom/squareup/server/account/protos/AccountStatusResponse;

    .line 122
    invoke-direct {p0, v2}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextCache(Ljava/lang/Class;)Lcom/squareup/persistent/Persistent;

    move-result-object v2

    .line 123
    invoke-interface {v2}, Lcom/squareup/persistent/Persistent;->getSynchronous()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-nez v2, :cond_0

    move-object v1, v0

    goto :goto_0

    .line 124
    :cond_0
    invoke-static {v1, v2}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->access$000(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v1

    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 130
    invoke-static {v1}, Lcom/squareup/account/DefaultLogInResponseCache;->stripNonLockoutNotifications(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v0
    :try_end_0
    .catch Lcom/google/gson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_1

    :catch_0
    move-exception v1

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to read current-user.json in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->dataDirectory:Ljava/io/File;

    .line 134
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 133
    invoke-static {v1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_1
    if-nez v0, :cond_3

    .line 137
    sget-object v0, Lcom/squareup/account/DefaultLogInResponseCache;->UNSET:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    return-object v0

    .line 139
    :cond_3
    iget-object v1, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->user:Lcom/squareup/server/account/protos/User;

    .line 140
    iget-object v2, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    if-eqz v1, :cond_6

    iget-object v2, v1, Lcom/squareup/server/account/protos/User;->id:Ljava/lang/String;

    invoke-static {v2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_2

    .line 144
    :cond_4
    iget-object v2, v1, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-nez v2, :cond_5

    .line 147
    iget-object v2, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {v2}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v2

    .line 148
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User;->newBuilder()Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    new-instance v3, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    invoke-direct {v3}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;-><init>()V

    sget-object v4, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    .line 150
    invoke-virtual {v4}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v3

    sget-object v4, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    .line 151
    invoke-static {v4}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes(Ljava/util/List;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v3

    .line 152
    invoke-virtual {v3}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->build()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object v3

    .line 149
    invoke-virtual {v1, v3}, Lcom/squareup/server/account/protos/User$Builder;->locale(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$Builder;

    move-result-object v1

    .line 153
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$Builder;->build()Lcom/squareup/server/account/protos/User;

    move-result-object v1

    .line 148
    invoke-virtual {v2, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v1

    .line 154
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    .line 155
    invoke-static {v0, v1}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->access$000(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v0

    .line 158
    :cond_5
    iget-object v1, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;->populateDefaults()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->access$000(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;Lcom/squareup/server/account/protos/AccountStatusResponse;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object v0

    return-object v0

    .line 141
    :cond_6
    :goto_2
    sget-object v0, Lcom/squareup/account/DefaultLogInResponseCache;->UNSET:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    return-object v0
.end method

.method private updateEncryptionFlags(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)V
    .locals 2

    .line 169
    iget-object p1, p1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    if-nez p1, :cond_0

    .line 171
    new-instance p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    invoke-direct {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    .line 173
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    .line 175
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->verboseLogging:Z

    .line 176
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    .line 180
    iget-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    iget-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    const-string v1, "should-prefer-encryption"

    .line 181
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    .line 182
    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public clearCache()V
    .locals 2

    .line 401
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->requireFileCache()Lcom/squareup/persistent/Persistent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, v1}, Lcom/squareup/persistent/Persistent;->set(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;)V

    .line 402
    iget-boolean v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->requireEncryptedFileCache()Lcom/squareup/persistent/Persistent;

    move-result-object v0

    invoke-interface {v0, v1, v1}, Lcom/squareup/persistent/Persistent;->set(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;)V

    .line 404
    :cond_0
    sget-object v0, Lcom/squareup/account/DefaultLogInResponseCache;->UNSET:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iput-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    return-void
.end method

.method public getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 248
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iget-object v0, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    return-object v0
.end method

.method public getPlainTextFile()Ljava/io/File;
    .locals 3

    .line 226
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->dataDirectory:Ljava/io/File;

    const-string v2, "current-user.json"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public getSessionToken()Ljava/lang/String;
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iget-object v0, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    return-object v0
.end method

.method public init(Lcom/squareup/accountstatus/QuietServerPreferences;)V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->plainTextFileCache:Lcom/squareup/persistent/Persistent;

    if-eqz v0, :cond_0

    return-void

    .line 198
    :cond_0
    iput-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->service:Lcom/squareup/accountstatus/QuietServerPreferences;

    .line 201
    const-class p1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    invoke-direct {p0, p1}, Lcom/squareup/account/DefaultLogInResponseCache;->getPlainTextCache(Ljava/lang/Class;)Lcom/squareup/persistent/Persistent;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->plainTextFileCache:Lcom/squareup/persistent/Persistent;

    .line 202
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->getEncryptedCache()Lcom/squareup/persistent/Persistent;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->encryptedFileCache:Lcom/squareup/persistent/Persistent;

    .line 205
    iget-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v0, 0x0

    const-string v1, "should-prefer-encryption"

    .line 206
    invoke-interface {p1, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    .line 209
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->unearthAndSanitizeLogin()Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    return-void
.end method

.method public onLoggedIn()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public replaceCache(Ljava/lang/String;)V
    .locals 1

    .line 258
    sget-object v0, Lcom/squareup/server/accountstatus/AccountStatusService;->EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/account/DefaultLogInResponseCache;->replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-void
.end method

.method public replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 264
    new-instance v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    invoke-direct {v0, p1, p2}, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;-><init>(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    .line 265
    invoke-static {v0}, Lcom/squareup/account/DefaultLogInResponseCache;->stripNonLockoutNotifications(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    move-result-object p1

    .line 266
    iput-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    .line 267
    invoke-direct {p0, v0}, Lcom/squareup/account/DefaultLogInResponseCache;->updateEncryptionFlags(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)V

    const/4 p2, 0x1

    new-array v1, p2, [Ljava/lang/Object;

    .line 268
    iget-boolean v2, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string/jumbo v2, "useEncrypted cache set to %b"

    invoke-direct {p0, v2, v1}, Lcom/squareup/account/DefaultLogInResponseCache;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    iget-boolean v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object v1, v1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    iget-object v1, v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 273
    :cond_0
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->requireFileCache()Lcom/squareup/persistent/Persistent;

    move-result-object v1

    invoke-interface {v1, p1, v2}, Lcom/squareup/persistent/Persistent;->set(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;)V

    .line 277
    :cond_1
    iget-boolean v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    if-nez v1, :cond_2

    return-void

    .line 282
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v1, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 283
    iget-object v4, p0, Lcom/squareup/account/DefaultLogInResponseCache;->encryptedFileCache:Lcom/squareup/persistent/Persistent;

    iget-object v5, p0, Lcom/squareup/account/DefaultLogInResponseCache;->keyStoreEncryptor:Lcom/squareup/encryption/KeystoreEncryptor;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-interface {v5, v1}, Lcom/squareup/encryption/KeystoreEncryptor;->encrypt([B)[B

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/squareup/persistent/Persistent;->setSynchronous(Ljava/lang/Object;)V

    const-string v1, "Encryption of plaintext cache successful"

    new-array v4, v3, [Ljava/lang/Object;

    .line 284
    invoke-direct {p0, v1, v4}, Lcom/squareup/account/DefaultLogInResponseCache;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    new-array p2, p2, [Ljava/lang/Object;

    .line 287
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    aput-object v4, p2, v3

    const-string v4, "Attempt to encrypt the cached ASR data failed: %s"

    invoke-static {v4, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 286
    invoke-direct {p0, v1, p2}, Lcom/squareup/account/DefaultLogInResponseCache;->logLogLog(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 288
    iput-boolean v3, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    .line 293
    :goto_0
    iget-boolean p2, p0, Lcom/squareup/account/DefaultLogInResponseCache;->useEncryptedCache:Z

    if-eqz p2, :cond_5

    invoke-direct {p0, p1}, Lcom/squareup/account/DefaultLogInResponseCache;->decryptionIsValid(Lcom/squareup/account/DefaultLogInResponseCache$CachedData;)Z

    move-result p2

    if-nez p2, :cond_3

    goto :goto_1

    .line 299
    :cond_3
    iget-object p1, v0, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object p1, p1, Lcom/squareup/server/account/protos/AccountStatusResponse;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions;->regex:Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_4

    new-array p1, v3, [Ljava/lang/Object;

    const-string p2, "Deleting plaintext cache"

    .line 300
    invoke-direct {p0, p2, p1}, Lcom/squareup/account/DefaultLogInResponseCache;->infoLog(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->deletePlainTextCache()V

    :cond_4
    return-void

    .line 294
    :cond_5
    :goto_1
    invoke-direct {p0}, Lcom/squareup/account/DefaultLogInResponseCache;->requireFileCache()Lcom/squareup/persistent/Persistent;

    move-result-object p2

    invoke-interface {p2, p1, v2}, Lcom/squareup/persistent/Persistent;->set(Ljava/lang/Object;Lcom/squareup/persistent/AsyncCallback;)V

    return-void
.end method

.method public setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 410
    iget-object v0, p0, Lcom/squareup/account/DefaultLogInResponseCache;->service:Lcom/squareup/accountstatus/QuietServerPreferences;

    .line 411
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/QuietServerPreferences;->setPreferencesQuietly(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;

    move-result-object v0

    .line 413
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iget-object v1, v1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-nez v1, :cond_1

    .line 415
    sget-object v1, Lcom/squareup/server/accountstatus/AccountStatusService;->EMPTY_ACCOUNT_STATUS_RESPONSE:Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v1, v1, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 417
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The prefs should not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 422
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iget-object v2, v2, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->status:Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-virtual {v2}, Lcom/squareup/server/account/protos/AccountStatusResponse;->newBuilder()Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object v2

    .line 423
    invoke-static {v1, p1}, Lcom/squareup/server/account/PreferenceUtilsKt;->overlayPreferences(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;

    move-result-object p1

    .line 424
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object p1

    .line 426
    iget-object v1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->canonicalStatus:Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    iget-object v1, v1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;->session_token:Ljava/lang/String;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/account/DefaultLogInResponseCache;->replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V

    return-object v0
.end method

.method setShouldLogWarningToBugsnag(Z)V
    .locals 0

    .line 537
    iput-boolean p1, p0, Lcom/squareup/account/DefaultLogInResponseCache;->shouldLogWarningToBugsnag:Z

    return-void
.end method
