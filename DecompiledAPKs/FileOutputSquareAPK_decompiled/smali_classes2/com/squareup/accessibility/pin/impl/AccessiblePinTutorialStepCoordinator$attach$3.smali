.class final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;
.super Lkotlin/jvm/internal/Lambda;
.source "AccessiblePinTutorialStepCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessiblePinTutorialStepCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessiblePinTutorialStepCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,128:1\n1103#2,7:129\n*E\n*S KotlinDebug\n*F\n+ 1 AccessiblePinTutorialStepCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3\n*L\n94#1,7:129\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u001c\u0010\u0002\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;->this$0:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;

    invoke-static {v0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;->access$getCancelButton$p(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator;)Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 129
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3$$special$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$attach$3;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
