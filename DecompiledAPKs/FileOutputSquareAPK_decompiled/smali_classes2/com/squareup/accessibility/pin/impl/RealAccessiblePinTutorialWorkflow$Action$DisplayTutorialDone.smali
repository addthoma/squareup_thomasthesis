.class public final Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;
.super Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;
.source "RealAccessiblePinTutorialWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayTutorialDone"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u0004*\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;",
        "Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 103
    new-instance v0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;

    invoke-direct {v0}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;-><init>()V

    sput-object v0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;->INSTANCE:Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialDone;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 103
    invoke-direct {p0, v0}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
            "-",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget-object v0, Lcom/squareup/accessibility/pin/DisplayingTutorialDone;->INSTANCE:Lcom/squareup/accessibility/pin/DisplayingTutorialDone;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
