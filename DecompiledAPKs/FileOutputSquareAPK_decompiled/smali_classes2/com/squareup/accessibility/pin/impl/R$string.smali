.class public final Lcom/squareup/accessibility/pin/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/accessibility/pin/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final continue_to_enter_pin:I = 0x7f1204d0

.field public static final empty_string:I = 0x7f120a3e

.field public static final exit_tutorial_dialog_message:I = 0x7f120a93

.field public static final exit_tutorial_dialog_title:I = 0x7f120a94

.field public static final go_back:I = 0x7f120b44

.field public static final replay_tutorial:I = 0x7f121676

.field public static final skip:I = 0x7f12180f

.field public static final tutorial_content_description:I = 0x7f121a6f

.field public static final tutorial_p0_message:I = 0x7f121aa6

.field public static final tutorial_p0_title:I = 0x7f121aa7

.field public static final tutorial_p1_message:I = 0x7f121aa8

.field public static final tutorial_p1_title:I = 0x7f121aa9

.field public static final tutorial_p2_message:I = 0x7f121aaa

.field public static final tutorial_p2_title:I = 0x7f121aab

.field public static final tutorial_p3_message:I = 0x7f121aac

.field public static final tutorial_p3_title:I = 0x7f121aad

.field public static final tutorial_p4_message:I = 0x7f121aae

.field public static final tutorial_p4_title:I = 0x7f121aaf

.field public static final tutorial_p5_message:I = 0x7f121ab0

.field public static final tutorial_p5_title:I = 0x7f121ab1

.field public static final tutorial_p6_message:I = 0x7f121ab2

.field public static final tutorial_p6_title:I = 0x7f121ab3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
