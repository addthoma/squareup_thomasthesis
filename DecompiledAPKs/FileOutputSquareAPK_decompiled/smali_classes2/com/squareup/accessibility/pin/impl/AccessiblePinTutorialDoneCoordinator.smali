.class public final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AccessiblePinTutorialDoneCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAccessiblePinTutorialDoneCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AccessiblePinTutorialDoneCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,58:1\n1103#2,7:59\n1103#2,7:66\n1103#2,7:73\n*E\n*S KotlinDebug\n*F\n+ 1 AccessiblePinTutorialDoneCoordinator.kt\ncom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator\n*L\n53#1,7:59\n54#1,7:66\n55#1,7:73\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\u0016B1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0005H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "cancelButton",
        "Landroid/widget/ImageView;",
        "doneButton",
        "Lcom/squareup/noho/NohoButton;",
        "restartButton",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "updateScreen",
        "screen",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private cancelButton:Landroid/widget/ImageView;

.field private doneButton:Lcom/squareup/noho/NohoButton;

.field private restartButton:Lcom/squareup/noho/NohoButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method public static final synthetic access$updateScreen(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->updateScreen(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;)V

    return-void
.end method

.method private final updateScreen(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;)V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->restartButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_0

    const-string v1, "restartButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 59
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$updateScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$updateScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_1

    const-string v1, "doneButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    .line 66
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$updateScreen$$inlined$onClickDebounced$2;

    invoke-direct {v1, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$updateScreen$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->cancelButton:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    const-string v1, "cancelButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    .line 73
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$updateScreen$$inlined$onClickDebounced$3;

    invoke-direct {v1, p1}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$updateScreen$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 43
    sget v1, Lcom/squareup/accessibility/pin/impl/R$id;->restart_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string/jumbo v2, "view.findViewById(R.id.restart_button)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/noho/NohoButton;

    iput-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->restartButton:Lcom/squareup/noho/NohoButton;

    .line 44
    iget-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->restartButton:Lcom/squareup/noho/NohoButton;

    if-nez v1, :cond_0

    const-string v2, "restartButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->replay_tutorial:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 45
    sget v1, Lcom/squareup/accessibility/pin/impl/R$id;->done_button:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string/jumbo v2, "view.findViewById(R.id.done_button)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/noho/NohoButton;

    iput-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    .line 46
    iget-object v1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->doneButton:Lcom/squareup/noho/NohoButton;

    if-nez v1, :cond_1

    const-string v2, "doneButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/accessibility/pin/impl/R$string;->continue_to_enter_pin:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 47
    sget v0, Lcom/squareup/accessibility/pin/impl/R$id;->cancel_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.cancel_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->cancelButton:Landroid/widget/ImageView;

    .line 49
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$attach$1;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
