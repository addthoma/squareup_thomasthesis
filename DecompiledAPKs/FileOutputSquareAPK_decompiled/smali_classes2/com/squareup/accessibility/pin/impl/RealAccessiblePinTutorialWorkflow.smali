.class public final Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealAccessiblePinTutorialWorkflow.kt"

# interfaces
.implements Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAccessiblePinTutorialWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAccessiblePinTutorialWorkflow.kt\ncom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,122:1\n149#2,5:123\n149#2,5:128\n149#2,5:133\n*E\n*S KotlinDebug\n*F\n+ 1 RealAccessiblePinTutorialWorkflow.kt\ncom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow\n*L\n61#1,5:123\n74#1,5:128\n84#1,5:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002:\u0001\u0018B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\nJ\u001f\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\u00032\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016\u00a2\u0006\u0002\u0010\u000fJS\u0010\u0010\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00042\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u0013H\u0016\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0004H\u0016JP\u0010\u0016\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t*$\u0012\u0004\u0012\u00020\u0017\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0017`\tH\u0002\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "toPosLayering",
        "Lcom/squareup/workflow/MainAndModal;",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method

.method private final toPosLayering(Ljava/util/Map;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 119
    sget-object v0, Lcom/squareup/container/PosLayering;->CARD_OVER_SHEET:Lcom/squareup/container/PosLayering;

    sget-object v1, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance p1, Lcom/squareup/accessibility/pin/DisplayingTutorial;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/squareup/accessibility/pin/DisplayingTutorial;-><init>(I)V

    check-cast p1, Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;->render(Lkotlin/Unit;Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of p1, p2, Lcom/squareup/accessibility/pin/DisplayingTutorial;

    const-string v0, ""

    if-eqz p1, :cond_0

    .line 56
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 57
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;

    .line 58
    new-instance v2, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$1;

    invoke-direct {v2, p3}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 59
    new-instance v3, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$2;

    invoke-direct {v3, p3}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$2;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 60
    check-cast p2, Lcom/squareup/accessibility/pin/DisplayingTutorial;

    invoke-virtual {p2}, Lcom/squareup/accessibility/pin/DisplayingTutorial;->getPage()I

    move-result p2

    .line 57
    invoke-direct {v1, v2, v3, p2}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;I)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 124
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 125
    const-class p3, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 126
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 124
    invoke-direct {p2, p3, v1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 56
    invoke-virtual {p1, p2}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 65
    :cond_0
    instance-of p1, p2, Lcom/squareup/accessibility/pin/DisplayingQuitDialog;

    if-eqz p1, :cond_1

    .line 66
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 67
    new-instance v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;

    .line 68
    new-instance v2, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;

    invoke-direct {v2, p3, p2}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;-><init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 67
    invoke-direct {v1, v2}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 129
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 130
    const-class p3, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 131
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 129
    invoke-direct {p2, p3, v1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 66
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    .line 75
    invoke-direct {p0, p1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;->toPosLayering(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 78
    :cond_1
    sget-object p1, Lcom/squareup/accessibility/pin/DisplayingTutorialDone;->INSTANCE:Lcom/squareup/accessibility/pin/DisplayingTutorialDone;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 79
    sget-object p1, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 80
    new-instance p2, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;

    .line 81
    new-instance v1, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$4;

    invoke-direct {v1, p3}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$4;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 82
    new-instance v2, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$5;

    invoke-direct {v2, p3}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$5;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 83
    new-instance v3, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$6;

    invoke-direct {v3, p3}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$6;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 80
    invoke-direct {p2, v1, v2, v3}, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 134
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 135
    const-class v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 136
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 134
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 79
    invoke-virtual {p1, p3}, Lcom/squareup/container/PosLayering$Companion;->bodyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;

    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;->snapshotState(Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
