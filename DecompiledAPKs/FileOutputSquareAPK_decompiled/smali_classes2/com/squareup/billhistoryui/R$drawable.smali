.class public final Lcom/squareup/billhistoryui/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final badge_expiring:I = 0x7f080084

.field public static final badge_expiring_selected:I = 0x7f080085

.field public static final badge_offline_payment_failure:I = 0x7f080086

.field public static final badge_offline_payment_failure_selected:I = 0x7f080087

.field public static final badge_pending:I = 0x7f080088

.field public static final badge_pending_actionbar:I = 0x7f080089

.field public static final badge_pending_actionbar_pressed:I = 0x7f08008a

.field public static final bill_details_empty_state_icon:I = 0x7f0800af

.field public static final bill_details_tender_customer_circle:I = 0x7f0800b0

.field public static final instant_transfer_button_background_blue:I = 0x7f080275

.field public static final instant_transfer_button_background_white:I = 0x7f080276

.field public static final magnifying_glass:I = 0x7f08028b

.field public static final selector_badge_pending_actionbar:I = 0x7f080487

.field public static final selector_payment_badge_expiring:I = 0x7f08048a

.field public static final selector_payment_badge_failure:I = 0x7f08048b

.field public static final selector_payment_badge_pending:I = 0x7f08048c

.field public static final transactions_applet_row_bottom_border:I = 0x7f0804ce

.field public static final transactions_history_list_header_row_background:I = 0x7f0804cf

.field public static final transactions_history_list_row_divider:I = 0x7f0804d0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
