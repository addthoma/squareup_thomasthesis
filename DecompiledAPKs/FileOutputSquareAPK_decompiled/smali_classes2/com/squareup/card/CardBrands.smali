.class public final Lcom/squareup/card/CardBrands;
.super Ljava/lang/Object;
.source "CardBrands.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardBrands.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardBrands.kt\ncom/squareup/card/CardBrands\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0002*\u0004\u0018\u00010\u0004\u001a\u000c\u0010\u0005\u001a\u00020\u0004*\u0004\u0018\u00010\u0002\u001a\u0014\u0010\u0006\u001a\u00020\u0007*\u0004\u0018\u00010\u00022\u0006\u0010\u0008\u001a\u00020\t\u001a\u0014\u0010\u0006\u001a\u00020\u0007*\u0004\u0018\u00010\u00022\u0006\u0010\u0008\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "brandNameId",
        "",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "toBillsBrand",
        "Lcom/squareup/Card$Brand;",
        "toCardBrand",
        "toCharSequence",
        "",
        "res",
        "Landroid/content/res/Resources;",
        "Lcom/squareup/util/Res;",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final brandNameId(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)I
    .locals 0

    .line 26
    invoke-static {p0}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object p0

    iget p0, p0, Lcom/squareup/text/CardBrandResources;->brandNameId:I

    return p0
.end method

.method public static final toBillsBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 6

    .line 13
    invoke-static {}, Lcom/squareup/card/CardBrandMapping;->values()[Lcom/squareup/card/CardBrandMapping;

    move-result-object v0

    .line 14
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/squareup/card/CardBrandMapping;->getCardBrand$proto_utilities_release()Lcom/squareup/Card$Brand;

    move-result-object v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 15
    invoke-virtual {v4}, Lcom/squareup/card/CardBrandMapping;->getBillsBrand$proto_utilities_release()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    :goto_3
    return-object p0
.end method

.method public static final toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;
    .locals 6

    .line 20
    invoke-static {}, Lcom/squareup/card/CardBrandMapping;->values()[Lcom/squareup/card/CardBrandMapping;

    move-result-object v0

    .line 21
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/squareup/card/CardBrandMapping;->getBillsBrand$proto_utilities_release()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    move-result-object v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 22
    invoke-virtual {v4}, Lcom/squareup/card/CardBrandMapping;->getCardBrand$proto_utilities_release()Lcom/squareup/Card$Brand;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    :goto_3
    return-object p0
.end method

.method public static final toCharSequence(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-static {p0}, Lcom/squareup/card/CardBrands;->brandNameId(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)I

    move-result p0

    invoke-virtual {p1, p0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    const-string p1, "res.getText(brandNameId())"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toCharSequence(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {p0}, Lcom/squareup/card/CardBrands;->brandNameId(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)I

    move-result p0

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method
