.class public Lcom/squareup/card/Expiration;
.super Ljava/lang/Object;
.source "Expiration.java"


# static fields
.field static final PIVOT_YEARS:I = 0x14


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static expirationYear(Ljava/lang/String;Ljava/util/Calendar;)I
    .locals 1

    const/4 v0, 0x1

    .line 65
    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    .line 68
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    add-int/lit8 p1, p1, 0x14

    add-int/lit16 p0, p0, 0x7d0

    if-lt p0, p1, :cond_0

    add-int/lit8 p0, p0, -0x64

    :cond_0
    return p0
.end method

.method static expired(IILjava/util/Calendar;)Z
    .locals 4

    const/4 v0, 0x1

    .line 102
    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    .line 103
    invoke-virtual {p2, v2}, Ljava/util/Calendar;->get(I)I

    move-result p2

    sub-int/2addr p1, v0

    const/4 v2, 0x0

    if-ltz p1, :cond_3

    const/16 v3, 0xb

    if-le p1, v3, :cond_0

    goto :goto_1

    :cond_0
    if-gt v1, p0, :cond_2

    if-ne v1, p0, :cond_1

    if-le p2, p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0

    :cond_3
    :goto_1
    return v2
.end method

.method static expired(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Z
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    if-nez p0, :cond_0

    goto :goto_0

    .line 92
    :cond_0
    :try_start_0
    invoke-static {p0, p2}, Lcom/squareup/card/Expiration;->expirationYear(Ljava/lang/String;Ljava/util/Calendar;)I

    move-result p0

    .line 93
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    .line 95
    invoke-static {p0, p1, p2}, Lcom/squareup/card/Expiration;->expired(IILjava/util/Calendar;)Z

    move-result p0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    :cond_1
    :goto_0
    return v0
.end method

.method public static isExpired(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 28
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x2

    .line 31
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 33
    invoke-static {v0, p0}, Lcom/squareup/card/Expiration;->isExpired(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p0

    return p0

    :cond_1
    :goto_0
    return v0
.end method

.method public static isExpired(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 44
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/squareup/card/Expiration;->expired(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Z

    move-result p0

    return p0
.end method

.method public static isExpired(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Z
    .locals 0

    .line 57
    invoke-static {p0, p1, p2}, Lcom/squareup/card/Expiration;->expired(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Z

    move-result p0

    return p0
.end method
