.class public final enum Lcom/squareup/card/CardBrandMapping;
.super Ljava/lang/Enum;
.source "CardBrands.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/card/CardBrandMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/card/CardBrandMapping;",
        "",
        "cardBrand",
        "Lcom/squareup/Card$Brand;",
        "billsBrand",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V",
        "getBillsBrand$proto_utilities_release",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "getCardBrand$proto_utilities_release",
        "()Lcom/squareup/Card$Brand;",
        "VISA",
        "AMEX",
        "MASTERCARD",
        "ALIPAY",
        "CASH_APP",
        "DISCOVER",
        "DISCOVER_DINERS",
        "JCB",
        "UNION_PAY",
        "SQUARE_GIFT_CARD_V2",
        "INTERAC",
        "SQUARE_CAPITAL_CARD",
        "EFTPOS",
        "FELICA",
        "UNKNOWN",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/card/CardBrandMapping;

.field public static final enum ALIPAY:Lcom/squareup/card/CardBrandMapping;

.field public static final enum AMEX:Lcom/squareup/card/CardBrandMapping;

.field public static final enum CASH_APP:Lcom/squareup/card/CardBrandMapping;

.field public static final enum DISCOVER:Lcom/squareup/card/CardBrandMapping;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/card/CardBrandMapping;

.field public static final enum EFTPOS:Lcom/squareup/card/CardBrandMapping;

.field public static final enum FELICA:Lcom/squareup/card/CardBrandMapping;

.field public static final enum INTERAC:Lcom/squareup/card/CardBrandMapping;

.field public static final enum JCB:Lcom/squareup/card/CardBrandMapping;

.field public static final enum MASTERCARD:Lcom/squareup/card/CardBrandMapping;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/card/CardBrandMapping;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/card/CardBrandMapping;

.field public static final enum UNION_PAY:Lcom/squareup/card/CardBrandMapping;

.field public static final enum UNKNOWN:Lcom/squareup/card/CardBrandMapping;

.field public static final enum VISA:Lcom/squareup/card/CardBrandMapping;


# instance fields
.field private final billsBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field private final cardBrand:Lcom/squareup/Card$Brand;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/card/CardBrandMapping;

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 46
    sget-object v2, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    .line 47
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->VISA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x0

    const-string v5, "VISA"

    .line 45
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->VISA:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 50
    sget-object v2, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    .line 51
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x1

    const-string v5, "AMEX"

    .line 49
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->AMEX:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 54
    sget-object v2, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    .line 55
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->MASTERCARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x2

    const-string v5, "MASTERCARD"

    .line 53
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->MASTERCARD:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 58
    sget-object v2, Lcom/squareup/Card$Brand;->ALIPAY:Lcom/squareup/Card$Brand;

    .line 59
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ALIPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x3

    const-string v5, "ALIPAY"

    .line 57
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->ALIPAY:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 62
    sget-object v2, Lcom/squareup/Card$Brand;->CASH_APP:Lcom/squareup/Card$Brand;

    .line 63
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CASH_APP:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x4

    const-string v5, "CASH_APP"

    .line 61
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->CASH_APP:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 66
    sget-object v2, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    .line 67
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x5

    const-string v5, "DISCOVER"

    .line 65
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->DISCOVER:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 70
    sget-object v2, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    .line 71
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->DISCOVER_DINERS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x6

    const-string v5, "DISCOVER_DINERS"

    .line 69
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->DISCOVER_DINERS:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 74
    sget-object v2, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    .line 75
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->JCB:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v4, 0x7

    const-string v5, "JCB"

    .line 73
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->JCB:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 78
    sget-object v2, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    .line 79
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->CHINA_UNIONPAY:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0x8

    const-string v5, "UNION_PAY"

    .line 77
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->UNION_PAY:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 82
    sget-object v2, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 83
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0x9

    const-string v5, "SQUARE_GIFT_CARD_V2"

    .line 81
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->SQUARE_GIFT_CARD_V2:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 86
    sget-object v2, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    .line 87
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->INTERAC:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0xa

    const-string v5, "INTERAC"

    .line 85
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->INTERAC:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 90
    sget-object v2, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    .line 91
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0xb

    const-string v5, "SQUARE_CAPITAL_CARD"

    .line 89
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->SQUARE_CAPITAL_CARD:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 94
    sget-object v2, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    .line 95
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->EFTPOS:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0xc

    const-string v5, "EFTPOS"

    .line 93
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->EFTPOS:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 98
    sget-object v2, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    .line 99
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->FELICA:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0xd

    const-string v5, "FELICA"

    .line 97
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->FELICA:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/card/CardBrandMapping;

    .line 102
    sget-object v2, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    .line 103
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/16 v4, 0xe

    const-string v5, "UNKNOWN"

    .line 101
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/card/CardBrandMapping;-><init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V

    sput-object v1, Lcom/squareup/card/CardBrandMapping;->UNKNOWN:Lcom/squareup/card/CardBrandMapping;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/card/CardBrandMapping;->$VALUES:[Lcom/squareup/card/CardBrandMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/Card$Brand;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card$Brand;",
            "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
            ")V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/card/CardBrandMapping;->cardBrand:Lcom/squareup/Card$Brand;

    iput-object p4, p0, Lcom/squareup/card/CardBrandMapping;->billsBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/card/CardBrandMapping;
    .locals 1

    const-class v0, Lcom/squareup/card/CardBrandMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/card/CardBrandMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/card/CardBrandMapping;
    .locals 1

    sget-object v0, Lcom/squareup/card/CardBrandMapping;->$VALUES:[Lcom/squareup/card/CardBrandMapping;

    invoke-virtual {v0}, [Lcom/squareup/card/CardBrandMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/card/CardBrandMapping;

    return-object v0
.end method


# virtual methods
.method public final getBillsBrand$proto_utilities_release()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/card/CardBrandMapping;->billsBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object v0
.end method

.method public final getCardBrand$proto_utilities_release()Lcom/squareup/Card$Brand;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/card/CardBrandMapping;->cardBrand:Lcom/squareup/Card$Brand;

    return-object v0
.end method
