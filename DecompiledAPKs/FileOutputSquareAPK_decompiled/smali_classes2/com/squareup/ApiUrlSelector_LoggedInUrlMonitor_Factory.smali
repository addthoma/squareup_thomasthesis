.class public final Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;
.super Ljava/lang/Object;
.source "ApiUrlSelector_LoggedInUrlMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final apiUrlSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;->apiUrlSelectorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ApiUrlSelector;",
            ">;)",
            "Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/ApiUrlSelector;)Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;-><init>(Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/ApiUrlSelector;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountstatus/AccountStatusProvider;

    iget-object v1, p0, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;->apiUrlSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ApiUrlSelector;

    invoke-static {v0, v1}, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;->newInstance(Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/ApiUrlSelector;)Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ApiUrlSelector_LoggedInUrlMonitor_Factory;->get()Lcom/squareup/ApiUrlSelector$LoggedInUrlMonitor;

    move-result-object v0

    return-object v0
.end method
