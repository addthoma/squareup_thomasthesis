.class public final Lcom/squareup/catalog/CatalogUtils;
.super Ljava/lang/Object;
.source "CatalogUtils.java"


# static fields
.field static final GLOBAL_DEFAULT_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static checkAttributeForCatalogObject(Lsquareup/items/merchant/CatalogObject;Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lsquareup/items/merchant/CatalogObject;",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "TT;>;",
            "Ljava/lang/String;",
            "TT;)Z"
        }
    .end annotation

    .line 127
    iget-object p0, p0, Lsquareup/items/merchant/CatalogObject;->attribute:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/Attribute;

    .line 128
    iget-object v2, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    iget-object v3, v0, Lsquareup/items/merchant/Attribute;->definition_token:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lsquareup/items/merchant/Attribute;->unit_token:Ljava/lang/String;

    .line 129
    invoke-static {p2, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    .line 130
    invoke-virtual {v0}, Lsquareup/items/merchant/Attribute;->newBuilder()Lsquareup/items/merchant/Attribute$Builder;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/squareup/catalog/AttributeEditor;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    :cond_1
    if-nez p3, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public static countEnabledLocations(Lsquareup/items/merchant/CatalogObject;Ljava/util/Set;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/CatalogObject;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .line 57
    new-instance v0, Lcom/squareup/catalog/CatalogObjectBuilder;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/CatalogObjectBuilder;-><init>(Lsquareup/items/merchant/CatalogObject;)V

    .line 58
    sget-object p0, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-virtual {v0, p0}, Lcom/squareup/catalog/CatalogObjectBuilder;->getUnitValueMapForAttribute(Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;

    move-result-object p0

    .line 60
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    .line 63
    invoke-virtual {p0}, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    .line 69
    :cond_0
    sget-object v1, Lcom/squareup/catalog/CatalogUtils;->GLOBAL_DEFAULT_KEY:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 73
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    sget-object v6, Lcom/squareup/catalog/CatalogUtils;->GLOBAL_DEFAULT_KEY:Ljava/lang/String;

    if-eq v5, v6, :cond_1

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 74
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_5

    .line 82
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_4

    goto :goto_1

    :cond_4
    sub-int/2addr v0, v2

    return v0

    :cond_5
    :goto_1
    return v3
.end method

.method public static createGetItemRequest(Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/GetRequest;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lsquareup/items/merchant/GetRequest;"
        }
    .end annotation

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 27
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 28
    new-instance v2, Lsquareup/items/merchant/Query$Builder;

    invoke-direct {v2}, Lsquareup/items/merchant/Query$Builder;-><init>()V

    sget-object v3, Lsquareup/items/merchant/Query$Type;->LEGACY_ID:Lsquareup/items/merchant/Query$Type;

    .line 29
    invoke-virtual {v2, v3}, Lsquareup/items/merchant/Query$Builder;->type(Lsquareup/items/merchant/Query$Type;)Lsquareup/items/merchant/Query$Builder;

    move-result-object v2

    new-instance v3, Lsquareup/items/merchant/CogsId;

    invoke-direct {v3, v1, p1}, Lsquareup/items/merchant/CogsId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v2, v3}, Lsquareup/items/merchant/Query$Builder;->cogs_id(Lsquareup/items/merchant/CogsId;)Lsquareup/items/merchant/Query$Builder;

    move-result-object v1

    .line 31
    invoke-virtual {v1}, Lsquareup/items/merchant/Query$Builder;->build()Lsquareup/items/merchant/Query;

    move-result-object v1

    .line 28
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 34
    :cond_0
    new-instance p0, Lsquareup/items/merchant/GetRequest$Builder;

    invoke-direct {p0}, Lsquareup/items/merchant/GetRequest$Builder;-><init>()V

    .line 35
    invoke-virtual {p0, v0}, Lsquareup/items/merchant/GetRequest$Builder;->query(Ljava/util/List;)Lsquareup/items/merchant/GetRequest$Builder;

    move-result-object p0

    .line 36
    invoke-virtual {p0}, Lsquareup/items/merchant/GetRequest$Builder;->build()Lsquareup/items/merchant/GetRequest;

    move-result-object p0

    return-object p0
.end method

.method public static disableCatalogObject(Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 2

    .line 93
    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, p1, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/catalog/CatalogObjectBuilder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p0

    return-object p0
.end method

.method public static isObjectEnabledAtLocation(Lsquareup/items/merchant/CatalogObject;Ljava/lang/String;)Z
    .locals 1

    .line 104
    new-instance v0, Lcom/squareup/catalog/CatalogObjectBuilder;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/CatalogObjectBuilder;-><init>(Lsquareup/items/merchant/CatalogObject;)V

    .line 105
    sget-object p0, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-virtual {v0, p0}, Lcom/squareup/catalog/CatalogObjectBuilder;->getUnitValueMapForAttribute(Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;

    move-result-object p0

    .line 107
    sget-object v0, Lcom/squareup/catalog/CatalogUtils;->GLOBAL_DEFAULT_KEY:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 108
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 109
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    :goto_0
    return p0

    :cond_2
    :goto_1
    if-nez p0, :cond_3

    const/4 p0, 0x0

    goto :goto_2

    .line 110
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    :goto_2
    return p0
.end method
