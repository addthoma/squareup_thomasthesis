.class public final Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;
.super Ljava/lang/Object;
.source "CatalogFeatureInteraction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/event/CatalogFeatureInteraction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private details:Ljava/lang/String;

.field private final feature:Lcom/squareup/catalog/event/CatalogFeature;

.field private id:Ljava/lang/Integer;

.field private source:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/catalog/event/CatalogFeature;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->feature:Lcom/squareup/catalog/event/CatalogFeature;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/catalog/event/CatalogFeatureInteraction;
    .locals 8

    .line 93
    new-instance v7, Lcom/squareup/catalog/event/CatalogFeatureInteraction;

    iget-object v1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->feature:Lcom/squareup/catalog/event/CatalogFeature;

    iget-object v2, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->id:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->details:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->source:Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/catalog/event/CatalogFeatureInteraction;-><init>(Lcom/squareup/catalog/event/CatalogFeature;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/catalog/event/CatalogFeatureInteraction$1;)V

    return-object v7
.end method

.method public details(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->details:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/Integer;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->id:Ljava/lang/Integer;

    return-object p0
.end method

.method public source(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->source:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;
    .locals 0

    .line 73
    iput-object p1, p0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
