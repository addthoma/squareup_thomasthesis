.class public final enum Lcom/squareup/catalog/event/CatalogFeature;
.super Ljava/lang/Enum;
.source "CatalogFeature.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/catalog/event/CatalogFeature;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\'\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0003J?\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00032\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"j\u0002\u0008#j\u0002\u0008$j\u0002\u0008%j\u0002\u0008&j\u0002\u0008\'j\u0002\u0008(j\u0002\u0008)j\u0002\u0008*j\u0002\u0008+j\u0002\u0008,j\u0002\u0008-j\u0002\u0008.j\u0002\u0008/j\u0002\u00080j\u0002\u00081j\u0002\u00082j\u0002\u00083\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/catalog/event/CatalogFeature;",
        "",
        "eventValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getEventValue",
        "()Ljava/lang/String;",
        "log",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "token",
        "id",
        "",
        "details",
        "source",
        "(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V",
        "CATEGORY_CREATED",
        "CATEGORY_DELETED",
        "CATEGORY_EDITED",
        "DISCOUNT_CREATED",
        "DISCOUNT_DELETED",
        "DISCOUNT_EDITED",
        "ITEM_CREATED",
        "ITEM_DELETED",
        "ITEM_EDITED",
        "MODIFIER_SET_CREATED",
        "MODIFIER_SET_DELETED",
        "MODIFIER_SET_EDITED",
        "AUTOMATIC_DISCOUNT_VIEWED",
        "VARIATION_CREATED",
        "VARIATION_DELETED",
        "VARIATION_EDITED",
        "MEASUREMENT_UNIT_CREATED",
        "MEASUREMENT_UNIT_DELETED",
        "MEASUREMENT_UNIT_EDITED",
        "VARIATION_CREATED_WITH_UNIT",
        "VARIATION_ADDED_UNIT",
        "VARIATION_REMOVED_UNIT",
        "VARIATION_CHANGED_UNIT",
        "MEASUREMENT_UNIT_PRECISION_CHANGED",
        "MEASUREMENT_UNIT_ABBREVIATION_CHANGED",
        "OPTION_SET_CREATED",
        "OPTION_SET_EDITED",
        "OPTION_SET_DELETED",
        "OPTION_ADDED_TO_OPTION_SET",
        "OPTION_DELETED_FROM_OPTION_SET",
        "OPTION_ORDER_CHANGED_IN_OPTION_SET",
        "VARIATION_CREATED_WITH_OPTIONS",
        "VARIATION_DELETED_WITH_OPTIONS",
        "OPTION_SET_ORDER_CHANGED_ON_ITEM",
        "OPTION_SET_EMPTY_PAGE_LEARN_MORE_LINK_CLICKED",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum AUTOMATIC_DISCOUNT_VIEWED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum CATEGORY_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum CATEGORY_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum CATEGORY_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum DISCOUNT_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum DISCOUNT_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum DISCOUNT_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum ITEM_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum ITEM_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum ITEM_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MEASUREMENT_UNIT_ABBREVIATION_CHANGED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MEASUREMENT_UNIT_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MEASUREMENT_UNIT_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MEASUREMENT_UNIT_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MEASUREMENT_UNIT_PRECISION_CHANGED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MODIFIER_SET_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MODIFIER_SET_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum MODIFIER_SET_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_ADDED_TO_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_DELETED_FROM_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_ORDER_CHANGED_IN_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_SET_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_SET_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_SET_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_SET_EMPTY_PAGE_LEARN_MORE_LINK_CLICKED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum OPTION_SET_ORDER_CHANGED_ON_ITEM:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_ADDED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_CHANGED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_CREATED_WITH_OPTIONS:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_CREATED_WITH_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_DELETED_WITH_OPTIONS:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

.field public static final enum VARIATION_REMOVED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;


# instance fields
.field private final eventValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x23

    new-array v0, v0, [Lcom/squareup/catalog/event/CatalogFeature;

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x0

    const-string v3, "CATEGORY_CREATED"

    const-string v4, "Category Created"

    .line 10
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->CATEGORY_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x1

    const-string v3, "CATEGORY_DELETED"

    const-string v4, "Category Deleted"

    .line 11
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->CATEGORY_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x2

    const-string v3, "CATEGORY_EDITED"

    const-string v4, "Category Edited"

    .line 12
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->CATEGORY_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x3

    const-string v3, "DISCOUNT_CREATED"

    const-string v4, "Discount Created"

    .line 13
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->DISCOUNT_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x4

    const-string v3, "DISCOUNT_DELETED"

    const-string v4, "Discount Deleted"

    .line 14
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->DISCOUNT_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x5

    const-string v3, "DISCOUNT_EDITED"

    const-string v4, "Discount Edited"

    .line 15
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->DISCOUNT_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x6

    const-string v3, "ITEM_CREATED"

    const-string v4, "Item Created"

    .line 16
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->ITEM_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/4 v2, 0x7

    const-string v3, "ITEM_DELETED"

    const-string v4, "Item Deleted"

    .line 17
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->ITEM_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x8

    const-string v3, "ITEM_EDITED"

    const-string v4, "Item Edited"

    .line 18
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->ITEM_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x9

    const-string v3, "MODIFIER_SET_CREATED"

    const-string v4, "Modifier Set Created"

    .line 19
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MODIFIER_SET_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0xa

    const-string v3, "MODIFIER_SET_DELETED"

    const-string v4, "Modifier Set Deleted"

    .line 20
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MODIFIER_SET_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0xb

    const-string v3, "MODIFIER_SET_EDITED"

    const-string v4, "Modifier Set Edited"

    .line 21
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MODIFIER_SET_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0xc

    const-string v3, "AUTOMATIC_DISCOUNT_VIEWED"

    const-string v4, "Automatic Discount: Viewed"

    .line 22
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->AUTOMATIC_DISCOUNT_VIEWED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0xd

    const-string v3, "VARIATION_CREATED"

    const-string v4, "Variation Created"

    .line 23
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0xe

    const-string v3, "VARIATION_DELETED"

    const-string v4, "Variation Deleted"

    .line 24
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_EDITED"

    const/16 v3, 0xf

    const-string v4, "Variation Edited"

    .line 25
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "MEASUREMENT_UNIT_CREATED"

    const/16 v3, 0x10

    const-string v4, "Unit Created"

    .line 26
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "MEASUREMENT_UNIT_DELETED"

    const/16 v3, 0x11

    const-string v4, "Unit Deleted"

    .line 27
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "MEASUREMENT_UNIT_EDITED"

    const/16 v3, 0x12

    const-string v4, "Unit Edited"

    .line 28
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_CREATED_WITH_UNIT"

    const/16 v3, 0x13

    const-string v4, "FQT: Variation Created with Unit"

    .line 29
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CREATED_WITH_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_ADDED_UNIT"

    const/16 v3, 0x14

    const-string v4, "FQT: Variation Added Unit"

    .line 30
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_ADDED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_REMOVED_UNIT"

    const/16 v3, 0x15

    const-string v4, "FQT: Variation Removed Unit"

    .line 31
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_REMOVED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_CHANGED_UNIT"

    const/16 v3, 0x16

    const-string v4, "FQT: Variation Changed Unit"

    .line 32
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CHANGED_UNIT:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "MEASUREMENT_UNIT_PRECISION_CHANGED"

    const/16 v3, 0x17

    const-string v4, "FQT: Unit Precision Changed"

    .line 33
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_PRECISION_CHANGED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "MEASUREMENT_UNIT_ABBREVIATION_CHANGED"

    const/16 v3, 0x18

    const-string v4, "FQT: Unit Abbreviation Changed"

    .line 34
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->MEASUREMENT_UNIT_ABBREVIATION_CHANGED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_SET_CREATED"

    const/16 v3, 0x19

    const-string v4, "Item Options: Option Set Created"

    .line 35
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_CREATED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_SET_EDITED"

    const/16 v3, 0x1a

    const-string v4, "Item Options: Option Set Edited"

    .line 36
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_EDITED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_SET_DELETED"

    const/16 v3, 0x1b

    const-string v4, "Item Options: Option Set Deleted"

    .line 37
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_DELETED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_ADDED_TO_OPTION_SET"

    const/16 v3, 0x1c

    const-string v4, "Item Options: Option Added to Option Set"

    .line 38
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_ADDED_TO_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_DELETED_FROM_OPTION_SET"

    const/16 v3, 0x1d

    const-string v4, "Item Options: Option Deleted from Option Set"

    .line 39
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_DELETED_FROM_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_ORDER_CHANGED_IN_OPTION_SET"

    const/16 v3, 0x1e

    const-string v4, "Item Options: Option Order Changed in Option Set"

    .line 40
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_ORDER_CHANGED_IN_OPTION_SET:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_CREATED_WITH_OPTIONS"

    const/16 v3, 0x1f

    const-string v4, "Item Options: Variation Created with Options"

    .line 41
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_CREATED_WITH_OPTIONS:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "VARIATION_DELETED_WITH_OPTIONS"

    const/16 v3, 0x20

    const-string v4, "Item Options: Variation Deleted with Options"

    .line 42
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->VARIATION_DELETED_WITH_OPTIONS:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_SET_ORDER_CHANGED_ON_ITEM"

    const/16 v3, 0x21

    const-string v4, "Item Options: Option Set Order Changed on Item"

    .line 43
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_ORDER_CHANGED_ON_ITEM:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/catalog/event/CatalogFeature;

    const-string v2, "OPTION_SET_EMPTY_PAGE_LEARN_MORE_LINK_CLICKED"

    const/16 v3, 0x22

    const-string v4, "Item Options: Learn More Link was Clicked"

    .line 44
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/catalog/event/CatalogFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/catalog/event/CatalogFeature;->OPTION_SET_EMPTY_PAGE_LEARN_MORE_LINK_CLICKED:Lcom/squareup/catalog/event/CatalogFeature;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/catalog/event/CatalogFeature;->$VALUES:[Lcom/squareup/catalog/event/CatalogFeature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/catalog/event/CatalogFeature;->eventValue:Ljava/lang/String;

    return-void
.end method

.method public static synthetic log$default(Lcom/squareup/catalog/event/CatalogFeature;Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 60
    move-object p3, v0

    check-cast p3, Ljava/lang/Integer;

    :cond_0
    move-object v4, p3

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    .line 61
    move-object p4, v0

    check-cast p4, Ljava/lang/String;

    :cond_1
    move-object v5, p4

    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    .line 62
    move-object p5, v0

    check-cast p5, Ljava/lang/String;

    :cond_2
    move-object v6, p5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/catalog/event/CatalogFeature;->log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeature;
    .locals 1

    const-class v0, Lcom/squareup/catalog/event/CatalogFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/catalog/event/CatalogFeature;

    return-object p0
.end method

.method public static values()[Lcom/squareup/catalog/event/CatalogFeature;
    .locals 1

    sget-object v0, Lcom/squareup/catalog/event/CatalogFeature;->$VALUES:[Lcom/squareup/catalog/event/CatalogFeature;

    invoke-virtual {v0}, [Lcom/squareup/catalog/event/CatalogFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/catalog/event/CatalogFeature;

    return-object v0
.end method


# virtual methods
.method public final getEventValue()Ljava/lang/String;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/catalog/event/CatalogFeature;->eventValue:Ljava/lang/String;

    return-object v0
.end method

.method public final log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "token"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/squareup/catalog/event/CatalogFeatureInteractionES1;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/event/CatalogFeatureInteractionES1;-><init>(Lcom/squareup/catalog/event/CatalogFeature;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 54
    new-instance v0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;-><init>(Lcom/squareup/catalog/event/CatalogFeature;)V

    invoke-virtual {v0, p2}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->token(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->build()Lcom/squareup/catalog/event/CatalogFeatureInteraction;

    move-result-object p2

    check-cast p2, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public final log(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "token"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/squareup/catalog/event/CatalogFeatureInteractionES1;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/event/CatalogFeatureInteractionES1;-><init>(Lcom/squareup/catalog/event/CatalogFeature;)V

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 65
    new-instance v0, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;-><init>(Lcom/squareup/catalog/event/CatalogFeature;)V

    .line 66
    invoke-virtual {v0, p2}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->token(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    move-result-object p2

    .line 67
    invoke-virtual {p2, p3}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->id(Ljava/lang/Integer;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    move-result-object p2

    .line 68
    invoke-virtual {p2, p4}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->details(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    move-result-object p2

    .line 69
    invoke-virtual {p2, p5}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->source(Ljava/lang/String;)Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;

    move-result-object p2

    .line 70
    invoke-virtual {p2}, Lcom/squareup/catalog/event/CatalogFeatureInteraction$Builder;->build()Lcom/squareup/catalog/event/CatalogFeatureInteraction;

    move-result-object p2

    check-cast p2, Lcom/squareup/eventstream/v2/AppEvent;

    .line 65
    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method
