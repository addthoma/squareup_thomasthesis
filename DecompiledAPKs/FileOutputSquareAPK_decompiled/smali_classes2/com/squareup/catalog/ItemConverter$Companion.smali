.class public final Lcom/squareup/catalog/ItemConverter$Companion;
.super Ljava/lang/Object;
.source "ItemConverter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/ItemConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/catalog/ItemConverter$Companion;",
        "",
        "()V",
        "get",
        "Lcom/squareup/catalog/ItemConverter;",
        "config",
        "Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;",
        "catalog_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/catalog/ItemConverter$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final get()Lcom/squareup/catalog/ItemConverter;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 101
    move-object v0, p0

    check-cast v0, Lcom/squareup/catalog/ItemConverter$Companion;

    new-instance v1, Lcom/squareup/catalog/ItemConverter$Companion$get$1;

    invoke-direct {v1}, Lcom/squareup/catalog/ItemConverter$Companion$get$1;-><init>()V

    check-cast v1, Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;

    invoke-virtual {v0, v1}, Lcom/squareup/catalog/ItemConverter$Companion;->get(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)Lcom/squareup/catalog/ItemConverter;

    move-result-object v0

    return-object v0
.end method

.method public final get(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;)Lcom/squareup/catalog/ItemConverter;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v0, Lcom/squareup/catalog/ItemConverter;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/catalog/ItemConverter;-><init>(Lcom/squareup/catalog/ItemConverter$ItemConverterConfig;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
