.class final Lcom/squareup/catalog/AttributeEditor$3;
.super Ljava/lang/Object;
.source "AttributeEditor.java"

# interfaces
.implements Lcom/squareup/catalog/AttributeEditor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/AttributeEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/catalog/AttributeEditor<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Boolean;
    .locals 4

    .line 37
    iget-object p1, p1, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;
    .locals 0

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/AttributeEditor$3;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Boolean;)V
    .locals 2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    .line 42
    invoke-virtual {p1, p2}, Lsquareup/items/merchant/Attribute$Builder;->int_value(Ljava/lang/Long;)Lsquareup/items/merchant/Attribute$Builder;

    goto :goto_1

    .line 44
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    const-wide/16 v0, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lsquareup/items/merchant/Attribute$Builder;->int_value(Ljava/lang/Long;)Lsquareup/items/merchant/Attribute$Builder;

    :goto_1
    return-void
.end method

.method public bridge synthetic setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/catalog/AttributeEditor$3;->setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Boolean;)V

    return-void
.end method
