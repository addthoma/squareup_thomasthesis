.class final Lcom/squareup/catalog/CatalogAttributeDefinition$1;
.super Lcom/squareup/catalog/WireEnumAttributeEditor;
.source "CatalogAttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/CatalogAttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/catalog/WireEnumAttributeEditor<",
        "Lcom/squareup/api/items/Item$Type;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/catalog/WireEnumAttributeEditor;-><init>()V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Item$Type;
    .locals 0

    .line 28
    invoke-static {p1}, Lcom/squareup/api/items/Item$Type;->fromValue(I)Lcom/squareup/api/items/Item$Type;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 26
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/CatalogAttributeDefinition$1;->fromValue(I)Lcom/squareup/api/items/Item$Type;

    move-result-object p1

    return-object p1
.end method
