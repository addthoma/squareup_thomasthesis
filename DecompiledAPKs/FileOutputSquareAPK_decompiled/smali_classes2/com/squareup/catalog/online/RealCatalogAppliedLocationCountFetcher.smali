.class public final Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;
.super Ljava/lang/Object;
.source "RealCatalogAppliedLocationCountFetcher.kt"

# interfaces
.implements Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCatalogAppliedLocationCountFetcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCatalogAppliedLocationCountFetcher.kt\ncom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,154:1\n704#2:155\n777#2,2:156\n*E\n*S KotlinDebug\n*F\n+ 1 RealCatalogAppliedLocationCountFetcher.kt\ncom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher\n*L\n36#1:155\n36#1,2:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 +2\u00020\u0001:\u0001+B)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u001bH\u0002J\u0010\u0010#\u001a\u00020 2\u0006\u0010\"\u001a\u00020\u001bH\u0016J\u0010\u0010$\u001a\u00020 2\u0006\u0010%\u001a\u00020&H\u0016J\u0008\u0010\'\u001a\u00020 H\u0016J\u0016\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0\u00142\u0006\u0010*\u001a\u00020\u001bH\u0002R\u0014\u0010\u000b\u001a\u00020\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u001e\u0010\u000f\u001a\u0012\u0012\u000c\u0012\n \u0012*\u0004\u0018\u00010\u00110\u0011\u0018\u00010\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00148VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cogs/Cogs;Lio/reactivex/Scheduler;)V",
        "activeLocationCount",
        "",
        "getActiveLocationCount",
        "()I",
        "activeLocations",
        "",
        "Lcom/squareup/server/account/protos/MerchantUnit;",
        "kotlin.jvm.PlatformType",
        "appliedLocationCount",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
        "getAppliedLocationCount",
        "()Lio/reactivex/Observable;",
        "appliedLocationCountRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "currentLocationToken",
        "",
        "fetchAppliedLocationDisposable",
        "Lio/reactivex/disposables/Disposable;",
        "fetchedObjectCogsId",
        "clearFetchAppliedLocationCountSubscription",
        "",
        "doFetchAppliedLocationCount",
        "objectCogsId",
        "fetchAppliedLocationCount",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "retrieveCatalogObject",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
        "objectToken",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$Companion;

.field private static final LOCAL_OBJECT_TOKEN:Ljava/lang/String; = ""

.field private static final NUMBER_OF_RETRIES:I = 0x2

.field private static final RETRY_DELAY_SECONDS:J = 0x2L


# instance fields
.field private final activeLocations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/MerchantUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final appliedLocationCountRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
            ">;"
        }
    .end annotation
.end field

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final currentLocationToken:Ljava/lang/String;

.field private fetchAppliedLocationDisposable:Lio/reactivex/disposables/Disposable;

.field private fetchedObjectCogsId:Ljava/lang/String;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->Companion:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cogs/Cogs;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p3, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p4, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->mainScheduler:Lio/reactivex/Scheduler;

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    const-string p3, "BehaviorRelay.create()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->appliedLocationCountRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 35
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    const-string p3, "settings.userSettings"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->currentLocationToken:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantUnits()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Ljava/lang/Iterable;

    .line 155
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/Collection;

    .line 156
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    move-object p4, p3

    check-cast p4, Lcom/squareup/server/account/protos/MerchantUnit;

    .line 37
    iget-object p4, p4, Lcom/squareup/server/account/protos/MerchantUnit;->active:Ljava/lang/Boolean;

    if-eqz p4, :cond_1

    goto :goto_1

    :cond_1
    sget-object p4, Lcom/squareup/server/account/protos/MerchantUnit;->DEFAULT_ACTIVE:Ljava/lang/Boolean;

    const-string v0, "DEFAULT_ACTIVE"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p4

    if-eqz p4, :cond_0

    invoke-interface {p2, p3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_2
    check-cast p2, Ljava/util/List;

    goto :goto_2

    :cond_3
    const/4 p2, 0x0

    :goto_2
    iput-object p2, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->activeLocations:Ljava/util/List;

    return-void
.end method

.method public static final synthetic access$clearFetchAppliedLocationCountSubscription(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->clearFetchAppliedLocationCountSubscription()V

    return-void
.end method

.method public static final synthetic access$doFetchAppliedLocationCount(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;Ljava/lang/String;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->doFetchAppliedLocationCount(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$getActiveLocations$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/util/List;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->activeLocations:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getAppliedLocationCountRelay$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->appliedLocationCountRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getCurrentLocationToken$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->currentLocationToken:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getFetchedObjectCogsId$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)Ljava/lang/String;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchedObjectCogsId:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$retrieveCatalogObject(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->retrieveCatalogObject(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setFetchedObjectCogsId$p(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;Ljava/lang/String;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchedObjectCogsId:Ljava/lang/String;

    return-void
.end method

.method private final clearFetchAppliedLocationCountSubscription()V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchAppliedLocationDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    :cond_0
    const/4 v0, 0x0

    .line 151
    check-cast v0, Lio/reactivex/disposables/Disposable;

    iput-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchAppliedLocationDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private final doFetchAppliedLocationCount(Ljava/lang/String;)V
    .locals 2

    .line 88
    invoke-direct {p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->clearFetchAppliedLocationCountSubscription()V

    .line 89
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;

    invoke-direct {v1, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { local ->\u2026 LOCAL_OBJECT_TOKEN\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 94
    new-instance v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$2;-><init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMapObservable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 118
    new-instance v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$3;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$3;-><init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    .line 124
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->onErrorResumeNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    .line 125
    new-instance v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$4;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$doFetchAppliedLocationCount$4;-><init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchAppliedLocationDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private final retrieveCatalogObject(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2Object;",
            ">;"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$1;

    invoke-direct {v1, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogOnlineTask;

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingleOnline(Lcom/squareup/shared/catalog/CatalogOnlineTask;)Lio/reactivex/Single;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 134
    sget-object v0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;->INSTANCE:Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$retrieveCatalogObject$2;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 144
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->mainScheduler:Lio/reactivex/Scheduler;

    const/4 v0, 0x2

    const-wide/16 v1, 0x2

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    .line 143
    invoke-static/range {v0 .. v7}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError$default(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/functions/Function;

    move-result-object v0

    .line 142
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "cogs.asSingleOnline { on\u2026r\n            )\n        )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public fetchAppliedLocationCount(Ljava/lang/String;)V
    .locals 3

    const-string v0, "objectCogsId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchedObjectCogsId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 77
    iput-object p1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchedObjectCogsId:Ljava/lang/String;

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->doFetchAppliedLocationCount(Ljava/lang/String;)V

    return-void

    .line 74
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "fetchAppliedLocationCount should be called only when the seller has more than one active location."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "objectCogsId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->fetchedObjectCogsId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " has been fetched. Fetching "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not allowed."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 69
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getActiveLocationCount()I
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->activeLocations:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getAppliedLocationCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCount;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->appliedLocationCountRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 49
    iget-object v1, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {v1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "connectivityMonitor.internetState()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v2, p0, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->appliedLocationCountRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v2, Lio/reactivex/Observable;

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher$onEnterScope$1;-><init>(Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "combineLatest(\n        c\u2026!!)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/catalog/online/RealCatalogAppliedLocationCountFetcher;->clearFetchAppliedLocationCountSubscription()V

    return-void
.end method
