.class public final Lcom/squareup/catalog/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final all_items:I = 0x7f1200c5

.field public static final apply_items:I = 0x7f1200d6

.field public static final apply_items_exact_many:I = 0x7f1200d7

.field public static final apply_items_exact_one:I = 0x7f1200d8

.field public static final apply_items_range:I = 0x7f1200d9

.field public static final buy_items_any:I = 0x7f1201d3

.field public static final buy_items_exact_many:I = 0x7f1201d4

.field public static final buy_items_exact_one:I = 0x7f1201d5

.field public static final buy_items_min_many:I = 0x7f1201d6

.field public static final buy_items_min_one:I = 0x7f1201d7

.field public static final categories_many:I = 0x7f1203d7

.field public static final category_one:I = 0x7f1203df

.field public static final ended_at_label:I = 0x7f120a79

.field public static final ends_at_label:I = 0x7f120a7a

.field public static final fridays:I = 0x7f120ae4

.field public static final item_one:I = 0x7f120e3c

.field public static final items_plural:I = 0x7f120e6f

.field public static final list_three:I = 0x7f120edf

.field public static final list_two:I = 0x7f120ee0

.field public static final mondays:I = 0x7f12101f

.field public static final name:I = 0x7f12104b

.field public static final pricing_rule_description_long:I = 0x7f12149b

.field public static final pricing_rule_description_medium_with_items:I = 0x7f12149c

.field public static final pricing_rule_description_medium_without_items:I = 0x7f12149d

.field public static final rules_many:I = 0x7f1216ad

.field public static final saturdays:I = 0x7f12176e

.field public static final schedule:I = 0x7f12177f

.field public static final schedule_days_of_the_week_daily:I = 0x7f121780

.field public static final schedule_days_of_the_week_many:I = 0x7f121781

.field public static final schedule_days_of_the_week_weekdays:I = 0x7f121782

.field public static final schedule_many:I = 0x7f121783

.field public static final schedule_time_range:I = 0x7f121784

.field public static final schedule_time_range_next_day:I = 0x7f121785

.field public static final specified_products:I = 0x7f121833

.field public static final specified_times:I = 0x7f121834

.field public static final started_at_label:I = 0x7f1218bd

.field public static final starts_at_label:I = 0x7f1218be

.field public static final sundays:I = 0x7f1218d8

.field public static final thursdays:I = 0x7f12196e

.field public static final tuesdays:I = 0x7f121a6a

.field public static final variation_one:I = 0x7f121b95

.field public static final variations_plural:I = 0x7f121b96

.field public static final wednesdays:I = 0x7f121bd5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
