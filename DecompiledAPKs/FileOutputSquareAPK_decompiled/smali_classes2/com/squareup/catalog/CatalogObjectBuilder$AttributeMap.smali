.class Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;
.super Ljava/lang/Object;
.source "CatalogObjectBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/CatalogObjectBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AttributeMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final attributeEditor:Lcom/squareup/catalog/AttributeEditor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/catalog/AttributeEditor<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final attributesByUnitToken:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lsquareup/items/merchant/Attribute$Builder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/catalog/AttributeEditor;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/catalog/AttributeEditor<",
            "TT;>;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lsquareup/items/merchant/Attribute$Builder;",
            ">;)V"
        }
    .end annotation

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    .line 131
    iput-object p2, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributesByUnitToken:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method entrySet()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "Ljava/lang/String;",
            "TT;>;>;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributesByUnitToken:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 144
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 146
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 147
    iget-object v1, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributesByUnitToken:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 148
    new-instance v3, Ljava/util/AbstractMap$SimpleImmutableEntry;

    .line 149
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsquareup/items/merchant/Attribute$Builder;

    invoke-interface {v5, v2}, Lcom/squareup/catalog/AttributeEditor;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 148
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method get(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributesByUnitToken:Ljava/util/Map;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 138
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lsquareup/items/merchant/Attribute$Builder;

    if-nez p1, :cond_1

    goto :goto_0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    invoke-interface {v0, p1}, Lcom/squareup/catalog/AttributeEditor;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method isEmpty()Z
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributesByUnitToken:Ljava/util/Map;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method size()I
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;->attributesByUnitToken:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
