.class public final Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;
.super Ljava/lang/Object;
.source "CommonAppModule_ProvideAudioHandlerThreadFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/os/HandlerThread;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory$InstanceHolder;->access$000()Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideAudioHandlerThread()Landroid/os/HandlerThread;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonAppModule;->provideAudioHandlerThread()Landroid/os/HandlerThread;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/HandlerThread;

    return-object v0
.end method


# virtual methods
.method public get()Landroid/os/HandlerThread;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;->provideAudioHandlerThread()Landroid/os/HandlerThread;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonAppModule_ProvideAudioHandlerThreadFactory;->get()Landroid/os/HandlerThread;

    move-result-object v0

    return-object v0
.end method
