.class Lcom/squareup/caller/ProgressAndFailureRxGlue$1;
.super Ljava/lang/Object;
.source "ProgressAndFailureRxGlue.java"

# interfaces
.implements Lcom/squareup/receiving/ReceivedMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/caller/ProgressAndFailureRxGlue;->lambda$handler$2(Lio/reactivex/functions/Consumer;Lrx/functions/Action3;Lcom/squareup/receiving/ReceivedResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/receiving/ReceivedMapper<",
        "TT;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

.field final synthetic val$onFailure:Lrx/functions/Action3;

.field final synthetic val$onSuccess:Lio/reactivex/functions/Consumer;


# direct methods
.method constructor <init>(Lcom/squareup/caller/ProgressAndFailureRxGlue;Lio/reactivex/functions/Consumer;Lrx/functions/Action3;)V
    .locals 0

    .line 77
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    iput-object p2, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->val$onSuccess:Lio/reactivex/functions/Consumer;

    iput-object p3, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->val$onFailure:Lrx/functions/Action3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic isAccepted(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->isAccepted(Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public isAccepted(Ljava/lang/Object;)Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {v0}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$100(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onSuccess(Ljava/lang/Object;)V

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->val$onSuccess:Lio/reactivex/functions/Consumer;

    invoke-interface {v0, p1}, Lio/reactivex/functions/Consumer;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic isClientError(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 0

    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->isClientError(Ljava/lang/Object;I)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public isClientError(Ljava/lang/Object;I)Ljava/lang/Void;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .line 101
    iget-object p2, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {p2}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$100(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onClientError(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic isNetworkError()Ljava/lang/Object;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->isNetworkError()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public isNetworkError()Ljava/lang/Void;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {v0}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$100(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onNetworkError()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic isRejected(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->isRejected(Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public isRejected(Ljava/lang/Object;)Ljava/lang/Void;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {v0}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$200(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue$RejectedMessageParser;->parse(Ljava/lang/Object;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {v1}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$100(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage$Parts;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage$Parts;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->val$onFailure:Lrx/functions/Action3;

    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage$Parts;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/receiving/FailureMessage$Parts;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v2, v0}, Lrx/functions/Action3;->call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic isServerError(I)Ljava/lang/Object;
    .locals 0

    .line 77
    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->isServerError(I)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public isServerError(I)Ljava/lang/Void;
    .locals 0

    .line 106
    iget-object p1, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$100(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onServerError()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic isSessionExpired()Ljava/lang/Object;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->isSessionExpired()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public isSessionExpired()Ljava/lang/Void;
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureRxGlue$1;->this$0:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    invoke-static {v0}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->access$300(Lcom/squareup/caller/ProgressAndFailureRxGlue;)Lcom/squareup/badbus/BadBus;

    move-result-object v0

    new-instance v1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-direct {v1}, Lcom/squareup/account/AccountEvents$SessionExpired;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->post(Ljava/lang/Object;)V

    const/4 v0, 0x0

    return-object v0
.end method
