.class public Lcom/squareup/caller/ProgressAndFailureView;
.super Ljava/lang/Object;
.source "ProgressAndFailureView.java"

# interfaces
.implements Lcom/squareup/caller/ProgressAndFailurePresenter$View;


# instance fields
.field private final context:Landroid/content/Context;

.field private final failurePopup:Lcom/squareup/caller/FailurePopup;

.field private final progressPopup:Lcom/squareup/caller/ProgressPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/caller/ProgressAndFailureView;->context:Landroid/content/Context;

    .line 14
    new-instance v0, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {v0, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/caller/ProgressAndFailureView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    .line 15
    new-instance v0, Lcom/squareup/caller/FailurePopup;

    invoke-direct {v0, p1}, Lcom/squareup/caller/FailurePopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/caller/ProgressAndFailureView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureView;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getFailurePopup()Lcom/squareup/mortar/Popup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureView;->failurePopup:Lcom/squareup/caller/FailurePopup;

    return-object v0
.end method

.method public getProgressPopup()Lcom/squareup/mortar/Popup;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/caller/ProgressAndFailureView;->progressPopup:Lcom/squareup/caller/ProgressPopup;

    return-object v0
.end method
