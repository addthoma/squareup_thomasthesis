.class public final Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;
.super Ljava/lang/Object;
.source "OrderErrorWorkflowLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderErrorWorkflowLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderErrorWorkflowLayoutRunner.kt\ncom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner\n*L\n1#1,84:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00192\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u000fH\u0002J\u0010\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0018\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "spinnerMessage",
        "Lcom/squareup/widgets/MessageView;",
        "spinnerTitle",
        "Landroid/widget/TextView;",
        "setSpinnerMessageText",
        "",
        "message",
        "",
        "setSpinnerTitleText",
        "title",
        "setTotal",
        "total",
        "",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$Companion;


# instance fields
.field private final buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final spinnerMessage:Lcom/squareup/widgets/MessageView;

.field private final spinnerTitle:Landroid/widget/TextView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->Companion:Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    .line 25
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/checkout/R$id;->buyer_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 26
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/checkout/R$id;->glyph_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->spinnerTitle:Landroid/widget/TextView;

    .line 27
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/checkout/R$id;->glyph_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    .line 32
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget v0, Lcom/squareup/checkout/R$layout;->auth_glyph_view:I

    const/4 v1, 0x0

    .line 31
    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    .line 34
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    sget v1, Lcom/squareup/checkout/R$id;->glyph_container:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 36
    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    .line 31
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.glyph.SquareGlyphView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final setSpinnerMessageText(Ljava/lang/String;)V
    .locals 2

    .line 70
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 74
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->spinnerMessage:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final setSpinnerTitleText(Ljava/lang/String;)V
    .locals 1

    .line 65
    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "title"

    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 66
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->spinnerTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 61
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;->getBuyerActionBarTitle()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->setTotal(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;->getGlyphTitle()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->setSpinnerTitleText(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;->getGlyphMessage()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->setSpinnerMessageText(Ljava/lang/String;)V

    .line 47
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$showRendering$1;-><init>(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 51
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$showRendering$2;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner$showRendering$2;-><init>(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 55
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;->getBuyerActionSubtitle()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 56
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v0, Lcom/squareup/util/ViewString;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    goto :goto_0

    .line 57
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->hideSubtitle()V

    :goto_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowLayoutRunner;->showRendering(Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
