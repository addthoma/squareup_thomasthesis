.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;
.super Ljava/lang/Object;
.source "OrderPaymentViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 30
    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/checkoutflow/core/tip/TipViewFactory;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;-><init>(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/checkoutflow/core/tip/TipViewFactory;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/core/tip/TipViewFactory;

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->newInstance(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/checkoutflow/core/tip/TipViewFactory;Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowViewFactory;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowViewFactory;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory_Factory;->get()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;

    move-result-object v0

    return-object v0
.end method
