.class final Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;
.super Ljava/lang/Object;
.source "CreateCnpPaymentService.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->createCnpPayment(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;",
        "it",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/checkoutfe/CheckoutResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/checkoutfe/CheckoutResponse;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Success;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutfe/CheckoutResponse;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Success;-><init>(Lcom/squareup/checkoutfe/CheckoutResponse;)V

    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;

    goto :goto_0

    .line 82
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutfe/CheckoutResponse;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Rejected;-><init>(Lcom/squareup/checkoutfe/CheckoutResponse;)V

    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;

    goto :goto_0

    .line 83
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Error;

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult$Error;-><init>(Lcom/squareup/receiving/ReceivedResponse$Error;)V

    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;

    :goto_0
    return-object v0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;

    move-result-object p1

    return-object p1
.end method
