.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "OrderPaymentWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$BootstrapScreen;,
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        ">;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 \u00172\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00020\u0003:\u0002\u0016\u0017B\'\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u000e\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0015\u001a\u00020\u0004R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentOutput;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "workflow",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "paymentProcessingCompleter",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;",
        "ordersPaymentViewFactory",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;",
        "(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;)V",
        "getWorkflow",
        "()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;",
        "onEnterScope",
        "",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "startArg",
        "BootstrapScreen",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final paymentProcessingCompleter:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;

.field private final workflow:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->Companion:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;

    .line 45
    const-class v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OrderPaymentWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentProcessingCompleter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersPaymentViewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->NAME:Ljava/lang/String;

    .line 22
    invoke-interface {p2}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 23
    move-object v4, p4

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 20
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->workflow:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->paymentProcessingCompleter:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getPaymentProcessingCompleter$p(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;)Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->paymentProcessingCompleter:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->workflow:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->getWorkflow()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 34
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 36
    sget-object v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->Companion:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public final start(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V
    .locals 1

    const-string v0, "startArg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
