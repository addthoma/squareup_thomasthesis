.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;
.super Ljava/lang/Object;
.source "CreateCnpPaymentService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B1\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00080\u0007\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ.\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0014\u001a\u00020\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;",
        "",
        "paymentService",
        "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
        "cardConverter",
        "Lcom/squareup/payment/CardConverter;",
        "location",
        "Ljavax/inject/Provider;",
        "Landroid/location/Location;",
        "userToken",
        "",
        "(Lcom/squareup/checkoutflow/core/services/CheckoutFeService;Lcom/squareup/payment/CardConverter;Ljavax/inject/Provider;Ljava/lang/String;)V",
        "createCnpPayment",
        "Lio/reactivex/Single;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;",
        "card",
        "Lcom/squareup/Card;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "tip",
        "uuid",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardConverter:Lcom/squareup/payment/CardConverter;

.field private final location:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentService:Lcom/squareup/checkoutflow/core/services/CheckoutFeService;

.field private final userToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/core/services/CheckoutFeService;Lcom/squareup/payment/CardConverter;Ljavax/inject/Provider;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
            "Lcom/squareup/payment/CardConverter;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardConverter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userToken"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->paymentService:Lcom/squareup/checkoutflow/core/services/CheckoutFeService;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->cardConverter:Lcom/squareup/payment/CardConverter;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->location:Ljavax/inject/Provider;

    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->userToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final createCnpPayment(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentResult;",
            ">;"
        }
    .end annotation

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "uuid"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;-><init>()V

    .line 44
    invoke-virtual {v0, p4}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->cardConverter:Lcom/squareup/payment/CardConverter;

    invoke-static {p1, v1}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->access$encodeToPaymentSourceId(Lcom/squareup/Card;Lcom/squareup/payment/CardConverter;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->source_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    .line 46
    invoke-static {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->access$toV2Money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    .line 47
    invoke-static {p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->access$toV2Money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p3

    goto :goto_0

    :cond_0
    move-object p3, v0

    :goto_0
    invoke-virtual {p1, p3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    const/4 p3, 0x1

    .line 50
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->autocomplete(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    .line 51
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->userToken:Ljava/lang/String;

    invoke-virtual {p1, p3}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    .line 52
    iget-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->location:Ljavax/inject/Provider;

    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/location/Location;

    if-eqz p3, :cond_1

    invoke-static {p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->access$toGeoLocation(Landroid/location/Location;)Lcom/squareup/protos/connect/v2/GeoLocation;

    move-result-object v0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location(Lcom/squareup/protos/connect/v2/GeoLocation;)Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object p1

    .line 56
    new-instance p3, Lcom/squareup/orders/model/Order$Builder;

    invoke-direct {p3}, Lcom/squareup/orders/model/Order$Builder;-><init>()V

    .line 57
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->userToken:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/squareup/orders/model/Order$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p3

    .line 60
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Builder;-><init>()V

    .line 61
    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$ItemType;->CUSTOM_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$Builder;->item_type(Lcom/squareup/orders/model/Order$LineItem$ItemType;)Lcom/squareup/orders/model/Order$LineItem$Builder;

    move-result-object v0

    const-string v1, "1"

    .line 62
    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;

    move-result-object v0

    .line 63
    invoke-static {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentServiceKt;->access$toV2Money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/orders/model/Order$LineItem$Builder;->base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;

    move-result-object p2

    .line 64
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$LineItem$Builder;->build()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object p2

    .line 59
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    .line 58
    invoke-virtual {p3, p2}, Lcom/squareup/orders/model/Order$Builder;->line_items(Ljava/util/List;)Lcom/squareup/orders/model/Order$Builder;

    move-result-object p2

    .line 67
    new-instance p3, Lcom/squareup/orders/CreateOrderRequest$Builder;

    invoke-direct {p3}, Lcom/squareup/orders/CreateOrderRequest$Builder;-><init>()V

    .line 68
    invoke-virtual {p3, p4}, Lcom/squareup/orders/CreateOrderRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Builder;

    move-result-object p3

    .line 69
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$Builder;->build()Lcom/squareup/orders/model/Order;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/squareup/orders/CreateOrderRequest$Builder;->order(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/CreateOrderRequest$Builder;

    move-result-object p2

    .line 72
    new-instance p3, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;

    invoke-direct {p3}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;-><init>()V

    .line 73
    invoke-virtual {p2}, Lcom/squareup/orders/CreateOrderRequest$Builder;->build()Lcom/squareup/orders/CreateOrderRequest;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_order_request(Lcom/squareup/orders/CreateOrderRequest;)Lcom/squareup/checkoutfe/CheckoutRequest$Builder;

    move-result-object p2

    .line 74
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->build()Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_payment_request(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;)Lcom/squareup/checkoutfe/CheckoutRequest$Builder;

    move-result-object p1

    .line 76
    iget-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;->paymentService:Lcom/squareup/checkoutflow/core/services/CheckoutFeService;

    .line 77
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->build()Lcom/squareup/checkoutfe/CheckoutRequest;

    move-result-object p1

    const-string p3, "request.build()"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/checkoutflow/core/services/CheckoutFeService;->checkout(Lcom/squareup/checkoutfe/CheckoutRequest;)Lcom/squareup/checkoutflow/core/services/CheckoutFeService$CheckoutStandardResponse;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/services/CheckoutFeService$CheckoutStandardResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object p1

    .line 79
    sget-object p2, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService$createCnpPayment$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "paymentService\n        .\u2026it)\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
