.class public final Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCardProcessingWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCardProcessingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCardProcessingWorkflow.kt\ncom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,126:1\n32#2,12:127\n149#3,5:139\n*E\n*S KotlinDebug\n*F\n+ 1 RealCardProcessingWorkflow.kt\ncom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow\n*L\n43#1,12:127\n99#1,5:139\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001e2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\u001eB\u0017\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0012\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0002J\u001a\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00032\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016JN\u0010\u0019\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u001a\u001a\u00020\u00042\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u0004H\u0016R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "giftCards",
        "Lcom/squareup/giftcard/GiftCards;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "buildAuthMessage",
        "",
        "card",
        "Lcom/squareup/Card;",
        "initialState",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final AUTHORIZATION_WORKER_KEY:Ljava/lang/String; = "authorization"

.field public static final Companion:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$Companion;

.field private static final MIN_DELAY_FOR_SPINNER:J = 0x320L

.field private static final MIN_DELAY_WORKER_KEY:Ljava/lang/String; = "timer"


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->Companion:Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/giftcard/GiftCards;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "giftCards"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method

.method public static final synthetic access$getBuyerLocaleOverride$p(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;)Lcom/squareup/buyer/language/BuyerLocaleOverride;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-object p0
.end method

.method private final buildAuthMessage(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    sget-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    goto :goto_0

    .line 110
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    .line 113
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0}, Lcom/squareup/text/CardBrandResources;->forBrand(Lcom/squareup/Card$Brand;)Lcom/squareup/text/CardBrandResources;

    move-result-object v0

    iget v0, v0, Lcom/squareup/text/CardBrandResources;->buyerBrandNameId:I

    .line 114
    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    .line 112
    invoke-static {v1, v0, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;ILjava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "Cards.formattedBrandAndU\u2026card.unmaskedDigits\n    )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 127
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 132
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 134
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 135
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 136
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 138
    :cond_3
    check-cast v1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 43
    :cond_4
    sget-object v1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->initialState(Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;

    check-cast p2, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->render(Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;",
            "-",
            "Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v2, "props"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {p3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getLocalTender()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 52
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/checkoutflow/core/auth/impl/R$string;->buyer_processing:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 54
    :cond_0
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v2}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v2

    sget v3, Lcom/squareup/checkout/R$string;->buyer_authorizing:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    move-object v8, v2

    .line 58
    sget-object v2, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Authorizing;

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getAuthNetworking()Lcom/squareup/checkoutflow/core/auth/AuthNetworking;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Worker;

    .line 62
    new-instance v2, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;

    invoke-direct {v2, p0}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "authorization"

    .line 59
    invoke-interface {p3, v0, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 77
    new-instance v0, Lcom/squareup/checkoutflow/core/auth/StatusScreen;

    .line 78
    sget-object v1, Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processing;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processing;

    move-object v4, v1

    check-cast v4, Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getBuyerActionTitle()Ljava/lang/String;

    move-result-object v5

    .line 80
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getBuyerActionSubtitle()Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buildAuthMessage(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v7

    move-object v3, v0

    move-object v6, v1

    .line 77
    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;-><init>(Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 85
    :cond_1
    instance-of v0, p2, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState$Success;

    if-eqz v0, :cond_2

    .line 86
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    const-wide/16 v2, 0x320

    const-string v4, "timer"

    invoke-virtual {v0, v2, v3, v4}, Lcom/squareup/workflow/Worker$Companion;->timer(JLjava/lang/String;)Lcom/squareup/workflow/Worker;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v0, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$2;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v7, 0x0

    move-object v0, p3

    move-object v1, v2

    move-object v2, v3

    move-object v3, v4

    move v4, v5

    move-object v5, v7

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 91
    new-instance v0, Lcom/squareup/checkoutflow/core/auth/StatusScreen;

    .line 92
    sget-object v1, Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processed;->INSTANCE:Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState$Processed;

    move-object v4, v1

    check-cast v4, Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState;

    .line 93
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getBuyerActionTitle()Ljava/lang/String;

    move-result-object v5

    .line 94
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getBuyerActionSubtitle()Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->buildAuthMessage(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v7

    move-object v3, v0

    move-object v6, v1

    .line 91
    invoke-direct/range {v3 .. v8}, Lcom/squareup/checkoutflow/core/auth/StatusScreen;-><init>(Lcom/squareup/checkoutflow/core/auth/StatusScreen$StatusScreenState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :goto_1
    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 140
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 141
    const-class v2, Lcom/squareup/checkoutflow/core/auth/StatusScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 142
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 140
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 100
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 91
    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/auth/RealCardProcessingWorkflow;->snapshotState(Lcom/squareup/checkoutflow/core/auth/CardProcessingWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
