.class public final Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealTipWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;"
        }
    .end annotation

    .line 45
    new-instance v6, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;
    .locals 7

    .line 50
    new-instance v6, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyLocaleHelper;

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v4, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;)Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow_Factory;->get()Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    move-result-object v0

    return-object v0
.end method
