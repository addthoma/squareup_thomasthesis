.class final Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealTipWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->render(Lcom/squareup/checkoutflow/core/tip/TipProps;Lcom/squareup/checkoutflow/core/tip/TipState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/core/tip/TipState;",
        "+",
        "Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealTipWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealTipWorkflow.kt\ncom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3\n*L\n1#1,97:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/core/tip/TipState;",
        "Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;",
        "input",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/checkoutflow/core/tip/TipProps;

.field final synthetic this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;Lcom/squareup/checkoutflow/core/tip/TipProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->$props:Lcom/squareup/checkoutflow/core/tip/TipProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/core/tip/TipState;",
            "Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;",
            ">;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->TIP_CUSTOM_AMOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 83
    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 84
    new-instance p1, Lcom/squareup/protos/common/Money;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->access$getCurrencyCode$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-direct {p1, v0, v1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    goto :goto_1

    .line 86
    :cond_1
    new-instance v0, Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->access$getMoneyLocaleHelper$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyExtractor;

    invoke-static {v1, p1}, Lcom/squareup/money/MoneyExtractorKt;->requireMoney(Lcom/squareup/money/MoneyExtractor;Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;

    invoke-static {v1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;->access$getCurrencyCode$p(Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    move-object p1, v0

    .line 89
    :goto_1
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    iget-object v1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-double v1, v1

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->$props:Lcom/squareup/checkoutflow/core/tip/TipProps;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/core/tip/TipProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    long-to-double v3, v3

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/Percentage$Companion;->fromRate(D)Lcom/squareup/util/Percentage;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v2, Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;

    invoke-direct {v2, p1, v0}, Lcom/squareup/checkoutflow/core/tip/TipOutput$TipEntered;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)V

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/core/tip/RealTipWorkflow$render$3;->invoke(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
