.class public final Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;
.super Ljava/lang/Object;
.source "DefaultTenderOptionListsFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/TenderOptionListsFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDefaultTenderOptionListsFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DefaultTenderOptionListsFactory.kt\ncom/squareup/checkoutflow/DefaultTenderOptionListsFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,46:1\n1360#2:47\n1429#2,3:48\n1360#2:51\n1429#2,3:52\n*E\n*S KotlinDebug\n*F\n+ 1 DefaultTenderOptionListsFactory.kt\ncom/squareup/checkoutflow/DefaultTenderOptionListsFactory\n*L\n31#1:47\n31#1,3:48\n32#1:51\n32#1,3:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0008\u0010\u0015\u001a\u00020\u000eH\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;",
        "Lcom/squareup/checkoutflow/TenderOptionListsFactory;",
        "tenderSettingsFactory",
        "Lcom/squareup/tenderpayment/TenderSettingsFactory;",
        "installmentsTenderOptionFactory",
        "Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;",
        "emoneyTenderOptionFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;",
        "onlineCheckoutTenderOptionFactory",
        "Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;",
        "additionalTenderOptions",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;",
        "(Lcom/squareup/tenderpayment/TenderSettingsFactory;Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;)V",
        "buildTenderOptionLists",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;",
        "tenderSettings",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
        "createTenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
        "tender",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;",
        "getTenderOptionLists",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final additionalTenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;

.field private final emoneyTenderOptionFactory:Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;

.field private final installmentsTenderOptionFactory:Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;

.field private final onlineCheckoutTenderOptionFactory:Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;

.field private final tenderSettingsFactory:Lcom/squareup/tenderpayment/TenderSettingsFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/tenderpayment/TenderSettingsFactory;Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tenderSettingsFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installmentsTenderOptionFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyTenderOptionFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onlineCheckoutTenderOptionFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalTenderOptions"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->tenderSettingsFactory:Lcom/squareup/tenderpayment/TenderSettingsFactory;

    iput-object p2, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->installmentsTenderOptionFactory:Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;

    iput-object p3, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->emoneyTenderOptionFactory:Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;

    iput-object p4, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->onlineCheckoutTenderOptionFactory:Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;

    iput-object p5, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->additionalTenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;

    return-void
.end method

.method private final createTenderOption(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;
    .locals 2

    .line 38
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 42
    :goto_0
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$LegacyTenderOption;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$LegacyTenderOption;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)V

    check-cast v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    goto :goto_1

    .line 41
    :cond_1
    iget-object p1, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->onlineCheckoutTenderOptionFactory:Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;

    invoke-interface {p1}, Lcom/squareup/onlinestore/checkoutflow/OnlineCheckoutTenderOptionFactory;->getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    goto :goto_1

    .line 40
    :cond_2
    iget-object p1, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->emoneyTenderOptionFactory:Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyTenderOptionFactory;->getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    goto :goto_1

    .line 39
    :cond_3
    iget-object p1, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->installmentsTenderOptionFactory:Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;->getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    :goto_1
    return-object v0
.end method


# virtual methods
.method public final buildTenderOptionLists(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 6

    const-string v0, "tenderSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->additionalTenderOptions:Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/AdditionalTenderOptions;->getAdditionalTenderOptions()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 31
    iget-object v1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->primary_tender:Ljava/util/List;

    const-string v2, "tenderSettings.primary_tender"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 47
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 48
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const-string v5, "it"

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 49
    check-cast v4, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 31
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->createTenderOption(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 30
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    .line 32
    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings;->secondary_tender:Ljava/util/List;

    const-string v1, "tenderSettings.secondary_tender"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 52
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 53
    check-cast v2, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    .line 32
    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->createTenderOption(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 54
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 34
    new-instance p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    invoke-direct {p1, v0, v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object p1
.end method

.method public getTenderOptionLists()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->tenderSettingsFactory:Lcom/squareup/tenderpayment/TenderSettingsFactory;

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/TenderSettingsFactory;->createTenders()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    .line 26
    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/DefaultTenderOptionListsFactory;->buildTenderOptionLists(Lcom/squareup/protos/client/devicesettings/TenderSettings;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v0

    return-object v0
.end method
