.class public final Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutAdditionalTenderOptions.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;",
        "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;",
        "selectMethodBuyerCheckoutEnabled",
        "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
        "factory",
        "Ldagger/Lazy;",
        "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
        "(Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Ldagger/Lazy;)V",
        "getAdditionalTenderOptions",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final factory:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final selectMethodBuyerCheckoutEnabled:Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;


# direct methods
.method public constructor <init>(Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            "Ldagger/Lazy<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectMethodBuyerCheckoutEnabled"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;->selectMethodBuyerCheckoutEnabled:Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

    iput-object p2, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;->factory:Ldagger/Lazy;

    return-void
.end method


# virtual methods
.method public getAdditionalTenderOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;->selectMethodBuyerCheckoutEnabled:Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

    invoke-interface {v0}, Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;->factory:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;->getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 23
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method
