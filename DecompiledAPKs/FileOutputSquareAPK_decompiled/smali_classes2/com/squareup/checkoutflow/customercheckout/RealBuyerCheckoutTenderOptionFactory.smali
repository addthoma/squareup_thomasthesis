.class public final Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutTenderOptionFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000_\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\r\u0018\u00002\u00020\u0001B-\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0008\u0010\u0017\u001a\u00020\u0018H\u0002J\u001c\u0010\u0019\u001a\u00020\u0014*\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001aH\u0002J\u000c\u0010\u001d\u001a\u00020\u001e*\u00020\u001fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000e\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;",
        "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
        "application",
        "Landroid/app/Application;",
        "buyerCheckoutV2Workflow",
        "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
        "tenderPaymentResultHandler",
        "Ldagger/Lazy;",
        "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "(Landroid/app/Application;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Ldagger/Lazy;Lcom/squareup/CountryCode;)V",
        "workflow",
        "com/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1",
        "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;",
        "getTenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "getTenderOptionKey",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "isEnabled",
        "",
        "data",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "title",
        "",
        "inRange",
        "Lcom/squareup/protos/common/Money;",
        "min",
        "max",
        "toTenderOptionResult",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final application:Landroid/app/Application;

.field private final buyerCheckoutV2Workflow:Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final tenderPaymentResultHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final workflow:Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;Ldagger/Lazy;Lcom/squareup/CountryCode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/CountryCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerCheckoutV2Workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderPaymentResultHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->buyerCheckoutV2Workflow:Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    iput-object p3, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->tenderPaymentResultHandler:Ldagger/Lazy;

    iput-object p4, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->countryCode:Lcom/squareup/CountryCode;

    .line 37
    new-instance p1, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;-><init>(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->workflow:Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;

    return-void
.end method

.method public static final synthetic access$getBuyerCheckoutV2Workflow$p(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;
    .locals 0

    .line 30
    iget-object p0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->buyerCheckoutV2Workflow:Lcom/squareup/buyercheckout/BuyerCheckoutV2Workflow;

    return-object p0
.end method

.method public static final synthetic access$isEnabled(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->isEnabled(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$title(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)Ljava/lang/String;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->title()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toTenderOptionResult(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->toTenderOptionResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    move-result-object p0

    return-object p0
.end method

.method private final inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    .line 95
    invoke-static {p1, p2, p3}, Lcom/squareup/money/MoneyMath;->inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method private final isEnabled(Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;)Z
    .locals 2

    .line 86
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->getTransactionMinimum()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;->getTransactionMaximum()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 86
    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method private final title()Ljava/lang/String;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->countryCode:Lcom/squareup/CountryCode;

    sget-object v1, Lcom/squareup/CountryCode;->JP:Lcom/squareup/CountryCode;

    if-ne v0, v1, :cond_0

    .line 78
    sget v0, Lcom/squareup/checkoutflow/customercheckout/impl/R$string;->credit_debit:I

    goto :goto_0

    .line 80
    :cond_0
    sget v0, Lcom/squareup/checkoutflow/customercheckout/impl/R$string;->tap_insert_swipe:I

    .line 82
    :goto_0
    new-instance v1, Lcom/squareup/resources/ResourceString;

    invoke-direct {v1, v0}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->application:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v1, v0}, Lcom/squareup/resources/ResourceString;->evaluate(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final toTenderOptionResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;
    .locals 1

    .line 58
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowSelectMethod;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    goto :goto_0

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->tenderPaymentResultHandler:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    .line 63
    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;->handlePaymentResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    .line 64
    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 5

    .line 69
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    .line 70
    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CUSTOMER_CHECKOUT_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    .line 71
    new-instance v2, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$getTenderOption$1;

    invoke-direct {v2, p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$getTenderOption$1;-><init>(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 72
    new-instance v3, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$getTenderOption$2;

    invoke-direct {v3, p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$getTenderOption$2;-><init>(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 73
    iget-object v4, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;->workflow:Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory$workflow$1;

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 69
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V

    return-object v0
.end method

.method public getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;
    .locals 3

    .line 97
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "javaClass.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
