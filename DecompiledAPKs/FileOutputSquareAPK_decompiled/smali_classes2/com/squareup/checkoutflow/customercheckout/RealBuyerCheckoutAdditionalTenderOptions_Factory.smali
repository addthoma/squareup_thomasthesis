.class public final Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutAdditionalTenderOptions_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;)",
            "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Ldagger/Lazy;)Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;",
            "Ldagger/Lazy<",
            "Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;",
            ">;)",
            "Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;-><init>(Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;

    iget-object v1, p0, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;->newInstance(Lcom/squareup/buyercheckout/SelectMethodBuyerCheckoutEnabled;Ldagger/Lazy;)Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions_Factory;->get()Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;

    move-result-object v0

    return-object v0
.end method
