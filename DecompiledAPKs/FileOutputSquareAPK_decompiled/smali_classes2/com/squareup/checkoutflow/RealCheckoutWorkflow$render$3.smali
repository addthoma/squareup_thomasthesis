.class final Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckoutWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->render(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "+",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        "output",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
            "Lcom/squareup/checkoutflow/CheckoutResult;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    sget-object v0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput$Cancel;->INSTANCE:Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput$Cancel;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    new-instance v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$1;-><init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 97
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput$Continue;->INSTANCE:Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput$Continue;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->this$0:Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    new-instance v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$2;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3$2;-><init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v2, v0, v1, v2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;->invoke(Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
