.class public final Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;
.super Ljava/lang/Object;
.source "TenderOptionInput.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0013\u0010\u0014\u001a\u00020\u00052\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\r\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Landroid/os/Parcelable;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "isSplitTender",
        "",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getCart",
        "()Lcom/squareup/protos/client/bills/Cart;",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final cart:Lcom/squareup/protos/client/bills/Cart;

.field private final isSplitTender:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput$Creator;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput$Creator;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)V
    .locals 1

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    iput-boolean p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    iput-object p3, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;ILjava/lang/Object;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->copy(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    return v0
.end method

.method public final component3()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;
    .locals 1

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;-><init>(Lcom/squareup/protos/common/Money;ZLcom/squareup/protos/client/bills/Cart;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object p1, p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getCart()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isSplitTender()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TenderOptionInput(amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isSplitTender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-boolean p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->isSplitTender:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
