.class public abstract Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;
.super Ljava/lang/Object;
.source "EmoneyBrand.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;,
        Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;,
        Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "",
        "()V",
        "enabled",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "Id",
        "Quicpay",
        "Suica",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Suica;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Id;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand$Quicpay;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract enabled(Lcom/squareup/protos/common/Money;)Z
.end method
