.class public final Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider$DefaultImpls;
.super Ljava/lang/Object;
.source "ReceiptAutoCloseProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static hasReceiptAutoCloseOverride(Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;)Z
    .locals 0

    .line 12
    invoke-interface {p0}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->getReceiptAutoCloseOverride()Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;

    move-result-object p0

    instance-of p0, p0, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;

    return p0
.end method
