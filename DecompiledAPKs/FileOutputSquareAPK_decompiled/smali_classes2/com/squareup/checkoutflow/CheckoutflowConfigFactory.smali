.class public interface abstract Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;
.super Ljava/lang/Object;
.source "CheckoutflowConfigFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
        "",
        "createCheckoutFlowConfig",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "step",
        "Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;",
        "config",
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createCheckoutFlowConfig(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;
.end method
