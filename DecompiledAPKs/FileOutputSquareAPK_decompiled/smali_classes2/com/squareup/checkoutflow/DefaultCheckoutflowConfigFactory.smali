.class public final Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;
.super Ljava/lang/Object;
.source "DefaultCheckoutflowConfigFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;",
        "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
        "customerCheckoutSettings",
        "Lcom/squareup/buyercheckout/CustomerCheckoutSettings;",
        "tenderOptionListsFactory",
        "Lcom/squareup/checkoutflow/TenderOptionListsFactory;",
        "(Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)V",
        "createCheckoutFlowConfig",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "step",
        "Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;",
        "config",
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

.field private final tenderOptionListsFactory:Lcom/squareup/checkoutflow/TenderOptionListsFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "customerCheckoutSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderOptionListsFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    iput-object p2, p0, Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;->tenderOptionListsFactory:Lcom/squareup/checkoutflow/TenderOptionListsFactory;

    return-void
.end method


# virtual methods
.method public createCheckoutFlowConfig(Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;
    .locals 4

    const-string v0, "step"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    invoke-interface {v0}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->isDefaultCustomerCheckoutEnabled()Z

    move-result v0

    .line 20
    new-instance v1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    .line 21
    new-instance v2, Lcom/squareup/tenderpayment/TenderPaymentConfig;

    .line 23
    iget-object v3, p0, Lcom/squareup/checkoutflow/DefaultCheckoutflowConfigFactory;->tenderOptionListsFactory:Lcom/squareup/checkoutflow/TenderOptionListsFactory;

    invoke-interface {v3}, Lcom/squareup/checkoutflow/TenderOptionListsFactory;->getTenderOptionLists()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v3

    .line 21
    invoke-direct {v2, v0, v3, p1}, Lcom/squareup/tenderpayment/TenderPaymentConfig;-><init>(ZLcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;Lcom/squareup/tenderpayment/TenderPaymentConfig$StartAtStep;)V

    const/4 p1, 0x0

    .line 20
    invoke-direct {v1, v2, p2, p1}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;-><init>(Lcom/squareup/tenderpayment/TenderPaymentConfig;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/checkoutflow/services/NetworkRequestModifier;)V

    return-object v1
.end method
