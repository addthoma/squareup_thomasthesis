.class public final Lcom/squareup/checkoutflow/RealCheckoutWorkflowKt;
.super Ljava/lang/Object;
.source "RealCheckoutWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "allowedInCheckoutflow",
        "",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$allowedInCheckoutflow(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflowKt;->allowedInCheckoutflow(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result p0

    return p0
.end method

.method private static final allowedInCheckoutflow(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 149
    const-class v0, Lcom/squareup/ui/tender/TenderScope;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p0, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/squareup/ui/permissions/PermissionDeniedScreen;->INSTANCE:Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method
