.class public final Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;
.super Ljava/lang/Object;
.source "RealPaymentProcessingEventSink.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/PaymentProcessingEventSink;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0008\u0010\u000b\u001a\u00020\nH\u0016J\u0008\u0010\u000c\u001a\u00020\nH\u0002J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\rH\u0016J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\nH\u0016J\u0008\u0010\u0012\u001a\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;",
        "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
        "()V",
        "awaitingPaymentProcessingComplete",
        "",
        "onPaymentProcessingComplete",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/checkoutflow/PaymentProcessingResult;",
        "kotlin.jvm.PlatformType",
        "collectNextTender",
        "",
        "divertedToOnboarding",
        "markComplete",
        "Lio/reactivex/Observable;",
        "paymentCanceled",
        "reason",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        "paymentCompleted",
        "paymentStarted",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private awaitingPaymentProcessingComplete:Z

.field private final onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/checkoutflow/PaymentProcessingResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<PaymentProcessingResult>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method private final markComplete()V
    .locals 5

    .line 55
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->awaitingPaymentProcessingComplete:Z

    const-string v1, "Checkout flow completed"

    const/4 v2, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 57
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Not currently in the checkout flow."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    aput-object v3, v0, v2

    .line 56
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Object;

    .line 60
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :goto_0
    iput-boolean v2, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->awaitingPaymentProcessingComplete:Z

    return-void
.end method


# virtual methods
.method public collectNextTender()V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/checkoutflow/PaymentProcessingResult$CollectNextTender;->INSTANCE:Lcom/squareup/checkoutflow/PaymentProcessingResult$CollectNextTender;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 48
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->markComplete()V

    return-void
.end method

.method public divertedToOnboarding()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/checkoutflow/PaymentProcessingResult$DivertToOnboarding;->INSTANCE:Lcom/squareup/checkoutflow/PaymentProcessingResult$DivertToOnboarding;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 43
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->markComplete()V

    return-void
.end method

.method public onPaymentProcessingComplete()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/PaymentProcessingResult;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 2

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/checkoutflow/PaymentProcessingResult$PaymentCancelled;

    invoke-direct {v1, p1}, Lcom/squareup/checkoutflow/PaymentProcessingResult$PaymentCancelled;-><init>(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 33
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->markComplete()V

    return-void
.end method

.method public paymentCompleted()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->onPaymentProcessingComplete:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/checkoutflow/PaymentProcessingResult$PaymentCompleted;->INSTANCE:Lcom/squareup/checkoutflow/PaymentProcessingResult$PaymentCompleted;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->markComplete()V

    return-void
.end method

.method public paymentStarted()V
    .locals 4

    .line 23
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->awaitingPaymentProcessingComplete:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    .line 24
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3}, Ljava/lang/IllegalStateException;-><init>()V

    aput-object v3, v0, v2

    const-string v3, "Dangling payment lifecycle event."

    invoke-static {v3, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "Starting checkout flow."

    .line 27
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    iput-boolean v1, p0, Lcom/squareup/checkoutflow/RealPaymentProcessingEventSink;->awaitingPaymentProcessingComplete:Z

    return-void
.end method
