.class public final Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "EmoneyViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B1\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "emoneyBrandSelectionFactory",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$Factory;",
        "emoneyPaymentProcessingFactory",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;",
        "emoneyPaymentProcessingDialogFactory",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$Factory;",
        "miryoCancelDialogFactory",
        "Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$Factory;",
        "emoneyPaymentProcessingScreenInflater",
        "Lcom/squareup/workflow/InflaterDelegate;",
        "(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$Factory;Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$Factory;Lcom/squareup/workflow/InflaterDelegate;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$Factory;Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$Factory;Lcom/squareup/workflow/InflaterDelegate;)V
    .locals 25
    .param p5    # Lcom/squareup/workflow/InflaterDelegate;
        .annotation runtime Lcom/squareup/checkoutflow/emoney/EmoneyPaymentProcessing;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    const-string v4, "emoneyBrandSelectionFactory"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "emoneyPaymentProcessingFactory"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "emoneyPaymentProcessingDialogFactory"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "miryoCancelDialogFactory"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "emoneyPaymentProcessingScreenInflater"

    move-object/from16 v9, p5

    invoke-static {v9, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x4

    new-array v4, v4, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 26
    sget-object v10, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 27
    sget-object v5, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;->Companion:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$Companion;

    invoke-virtual {v5}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v11

    .line 28
    sget v12, Lcom/squareup/checkoutflow/emoney/impl/R$layout;->emoney_brand_selection:I

    .line 29
    new-instance v5, Lcom/squareup/workflow/ScreenHint;

    sget-object v19, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x1df

    const/16 v24, 0x0

    move-object v13, v5

    invoke-direct/range {v13 .. v24}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 30
    new-instance v6, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$1;

    invoke-direct {v6, v0}, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$1;-><init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$Factory;)V

    move-object v15, v6

    check-cast v15, Lkotlin/jvm/functions/Function1;

    const/16 v16, 0x8

    .line 26
    invoke-static/range {v10 .. v17}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v5, 0x0

    aput-object v0, v4, v5

    .line 32
    sget-object v5, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 33
    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen;->Companion:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v6

    .line 34
    sget v7, Lcom/squareup/checkoutflow/emoney/impl/R$layout;->emoney_payment_validation:I

    .line 36
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x19f

    const/16 v21, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 37
    new-instance v8, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$2;

    invoke-direct {v8, v1}, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$2;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCoordinator$Factory;)V

    move-object v10, v8

    check-cast v10, Lkotlin/jvm/functions/Function1;

    move-object v8, v0

    .line 32
    invoke-virtual/range {v5 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout(Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v4, v1

    .line 40
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 41
    sget-object v1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelScreen;->Companion:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 42
    new-instance v5, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$3;

    invoke-direct {v5, v2}, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$3;-><init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingCancelDialogFactory$Factory;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 40
    invoke-virtual {v0, v1, v5}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v4, v1

    .line 44
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 45
    sget-object v1, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen;->Companion:Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 46
    new-instance v2, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$4;

    invoke-direct {v2, v3}, Lcom/squareup/checkoutflow/emoney/EmoneyViewFactory$4;-><init>(Lcom/squareup/checkoutflow/emoney/miryo/MiryoCancelDialogFactory$Factory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v4, v1

    move-object/from16 v0, p0

    .line 25
    invoke-direct {v0, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
