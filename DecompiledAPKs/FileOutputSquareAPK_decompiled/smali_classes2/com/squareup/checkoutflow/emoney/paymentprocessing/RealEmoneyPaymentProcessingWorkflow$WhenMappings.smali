.class public final synthetic Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->values()[Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_INSUFFICIENT_BALANCE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_SEVERAL_SUICA_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_READ_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_WRITE_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_INVALID_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_DIFFERENT_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_COMMON_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_EXCEEDED_LIMIT_AMOUNT:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_EXPIRED_CARD:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_LOCKED_MOBILE_SERVICE:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_WRONG_CARD_ERROR:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->TMN_MSG_POLLING_TIMEOUT:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    return-void
.end method
