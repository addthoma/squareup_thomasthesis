.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;
.super Ljava/lang/Object;
.source "EmoneyDisplayRequestTextHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0018\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0001\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "getErrorMessageForTmnMessage",
        "",
        "tmnMessage",
        "Lcom/squareup/cardreader/lcr/CrsTmnMessage;",
        "getErrorTitleForTmnMessage",
        "defaultErrorTitle",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final getErrorMessageForTmnMessage(Lcom/squareup/cardreader/lcr/CrsTmnMessage;)Ljava/lang/String;
    .locals 1

    const-string v0, "tmnMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p1, 0x0

    goto :goto_0

    .line 43
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_locked_mobile_service:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 42
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_expired_card:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 41
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_exceeded_limit_amount:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 40
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_common_error:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 37
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    .line 38
    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_different_card:I

    .line 37
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 36
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_invalid_card:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 35
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_write_error:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 34
    :pswitch_7
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_read_error:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 33
    :pswitch_8
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_several_suica_card:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 32
    :pswitch_9
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_tmn_msg_insufficient_balance:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getErrorTitleForTmnMessage(Lcom/squareup/cardreader/lcr/CrsTmnMessage;I)Ljava/lang/String;
    .locals 1

    const-string v0, "tmnMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 27
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 26
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyDisplayRequestTextHelper;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/checkoutflow/emoney/impl/R$string;->emoney_reader_timed_out:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
