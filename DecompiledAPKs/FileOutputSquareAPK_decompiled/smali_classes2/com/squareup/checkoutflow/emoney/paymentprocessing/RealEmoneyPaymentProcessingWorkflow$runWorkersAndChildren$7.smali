.class final Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyPaymentProcessingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->runWorkersAndChildren(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Lcom/squareup/workflow/RenderContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/tmn/TmnOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "+",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
        "it",
        "Lcom/squareup/tmn/TmnOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tmn/TmnOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 533
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 535
    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    check-cast p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnOutput$MiryoOutput$ActiveCardReaderDisconnectedInMiryo;->getHasAvailableCardReader()Z

    move-result p1

    sget-object v4, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v4, Lcom/squareup/tmn/Action;

    invoke-direct {v2, v3, p1, v1, v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string p1, "Miryo Reader Disconnected"

    .line 534
    invoke-static {v0, v2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_0

    .line 539
    :cond_0
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput$NetworkErrorInMiryo;

    const-string v2, "Miryo Network Error"

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 540
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getHasValidReaderConnected()Z

    move-result v4

    sget-object v5, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v5, Lcom/squareup/tmn/Action;

    invoke-direct {v0, v3, v4, v1, v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    .line 539
    invoke-static {p1, v0, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 543
    :cond_1
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$MiryoOutput$TmnErrorInMiryo;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 544
    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v4, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getHasValidReaderConnected()Z

    move-result v4

    sget-object v5, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v5, Lcom/squareup/tmn/Action;

    invoke-direct {v0, v3, v4, v1, v5}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    .line 543
    invoke-static {p1, v0, v2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 547
    :cond_2
    instance-of v0, p1, Lcom/squareup/tmn/TmnOutput$CardReaderConnectionChanged;

    if-eqz v0, :cond_3

    .line 548
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->this$0:Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;

    .line 549
    new-instance v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    .line 550
    iget-object v3, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->$state:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;->getBalance()Lcom/squareup/protos/common/Money;

    move-result-object v3

    check-cast p1, Lcom/squareup/tmn/TmnOutput$CardReaderConnectionChanged;

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnOutput$CardReaderConnectionChanged;->getHasAvailableCardReader()Z

    move-result v4

    invoke-virtual {p1}, Lcom/squareup/tmn/TmnOutput$CardReaderConnectionChanged;->getHasAvailableCardReader()Z

    move-result p1

    xor-int/2addr p1, v1

    sget-object v1, Lcom/squareup/tmn/Action$NoOp;->INSTANCE:Lcom/squareup/tmn/Action$NoOp;

    check-cast v1, Lcom/squareup/tmn/Action;

    .line 549
    invoke-direct {v2, v3, v4, p1, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState$InMiryo;-><init>(Lcom/squareup/protos/common/Money;ZZLcom/squareup/tmn/Action;)V

    check-cast v2, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;

    const-string p1, "Miryo Reader Connected"

    .line 548
    invoke-static {v0, v2, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;->access$performEnterState(Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingState;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 555
    :cond_3
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/tmn/TmnOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/RealEmoneyPaymentProcessingWorkflow$runWorkersAndChildren$7;->invoke(Lcom/squareup/tmn/TmnOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
