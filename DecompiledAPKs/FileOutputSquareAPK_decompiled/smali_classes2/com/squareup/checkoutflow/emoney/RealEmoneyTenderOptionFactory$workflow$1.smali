.class public final Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealEmoneyTenderOptionFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00003\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000*\u0001\u0000\u0008\n\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001JF\u0010\t\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\n\u001a\u00020\u00022\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00030\u000cH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "render",
        "input",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 74
    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->access$getEmoneyWorkflow$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/checkoutflow/emoney/EmoneyWorkflow;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 84
    new-instance v3, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;

    .line 85
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    invoke-static {v1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->access$getAccountStatusSettings$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    const-string v4, "accountStatusSettings.paymentSettings"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getEmoneyMin()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v7, "input.amount.currency_code"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v5, v6, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 87
    iget-object v5, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    invoke-static {v5}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->access$getAccountStatusSettings$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v5

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/settings/server/PaymentSettings;->getSuicaMax()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v4, v5, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 84
    invoke-direct {v3, v0, v1, p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    .line 89
    new-instance p1, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1$render$1;-><init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    .line 82
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 98
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
