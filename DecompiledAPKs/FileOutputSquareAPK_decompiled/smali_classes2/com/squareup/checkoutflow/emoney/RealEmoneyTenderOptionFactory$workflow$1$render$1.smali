.class final Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEmoneyTenderOptionFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "it",
        "Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1$render$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$SelectBrand;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1$render$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;

    iget-object v0, v0, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;->access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory;)Ldagger/Lazy;

    move-result-object v0

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    new-instance v1, Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$SelectBrand;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$SelectBrand;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;->handlePaymentResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    .line 93
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$Cancel;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_1
    instance-of p1, p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput$Complete;

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/RealEmoneyTenderOptionFactory$workflow$1$render$1;->invoke(Lcom/squareup/checkoutflow/emoney/EmoneyWorkflowOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
