.class public abstract Lcom/squareup/checkoutflow/receipt/BillReceiptModule;
.super Ljava/lang/Object;
.source "BillReceiptModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static provideReceiptWorkflow(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;)Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;
    .locals 13
    .annotation runtime Lcom/squareup/checkoutflow/datamodels/ForBills;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 29
    new-instance v12, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;)V

    return-object v12
.end method


# virtual methods
.method abstract bindReceiptIntoBuyer(Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Lcom/squareup/ui/buyer/ForBuyer;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
