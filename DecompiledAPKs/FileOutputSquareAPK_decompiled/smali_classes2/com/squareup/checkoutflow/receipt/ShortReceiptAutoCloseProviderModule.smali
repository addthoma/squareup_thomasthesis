.class public abstract Lcom/squareup/checkoutflow/receipt/ShortReceiptAutoCloseProviderModule;
.super Ljava/lang/Object;
.source "ShortReceiptAutoCloseProviderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideReceiptAutoCloseProvider(Lcom/squareup/checkoutflow/receipt/ShortReceiptAutoCloseProvider;)Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
