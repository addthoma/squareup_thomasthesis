.class public final Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealReceiptWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealReceiptWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealReceiptWorkflow.kt\ncom/squareup/checkoutflow/receipt/RealReceiptWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,613:1\n149#2,5:614\n149#2,5:619\n149#2,5:624\n149#2,5:629\n149#2,5:634\n41#3:639\n56#3,2:640\n41#3:643\n56#3,2:644\n276#4:642\n276#4:646\n*E\n*S KotlinDebug\n*F\n+ 1 RealReceiptWorkflow.kt\ncom/squareup/checkoutflow/receipt/RealReceiptWorkflow\n*L\n222#1,5:614\n317#1,5:619\n330#1,5:624\n378#1,5:629\n426#1,5:634\n130#1:639\n130#1,2:640\n146#1:643\n146#1,2:644\n130#1:642\n146#1:646\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f7\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005*\u0001*\u0018\u0000 \\2\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001\\B_\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u00a2\u0006\u0002\u0010!J3\u0010,\u001a\u00020-2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/2\u0006\u00100\u001a\u0002012\u0008\u00102\u001a\u0004\u0018\u000103H\u0002\u00a2\u0006\u0002\u00104J,\u00105\u001a\u0002062\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0002J,\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020:2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/2\u0006\u0010>\u001a\u000208H\u0002J\u001a\u0010?\u001a\u00020@2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010A\u001a\u0004\u0018\u00010$H\u0002J\u0012\u0010B\u001a\u0004\u0018\u00010C2\u0006\u0010D\u001a\u00020:H\u0002JD\u0010E\u001a\u0018\u0012\u0004\u0012\u00020F\u0012\u0004\u0012\u00020G0\u0008j\u0008\u0012\u0004\u0012\u00020F`H2\u0006\u0010I\u001a\u00020\u00032\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/2\u0008\u0010J\u001a\u0004\u0018\u00010&H\u0002J,\u0010K\u001a\u00020L2\u0006\u0010M\u001a\u00020\u00032\u0006\u0010N\u001a\u00020\u00042\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/H\u0002JD\u0010O\u001a\u0018\u0012\u0004\u0012\u00020F\u0012\u0004\u0012\u00020G0\u0008j\u0008\u0012\u0004\u0012\u00020F`H2\u0006\u0010I\u001a\u00020\u00032\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/2\u0008\u0010J\u001a\u0004\u0018\u00010&H\u0002J<\u0010P\u001a\u00020Q2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/2\u0006\u0010R\u001a\u0002082\u0006\u0010S\u001a\u0002082\u0006\u0010T\u001a\u00020:2\u0006\u0010U\u001a\u00020:H\u0002J\u001a\u0010V\u001a\u00020\u00042\u0006\u0010I\u001a\u00020\u00032\u0008\u0010W\u001a\u0004\u0018\u00010XH\u0016JN\u0010Y\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010I\u001a\u00020\u00032\u0006\u0010N\u001a\u00020\u00042\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050/H\u0016J\u001a\u0010Z\u001a\u00020:2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010A\u001a\u0004\u0018\u00010$H\u0002J\u0010\u0010[\u001a\u00020X2\u0006\u0010N\u001a\u00020\u0004H\u0016R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u0008\u0012\u0004\u0012\u00020&0#X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\'\u0010(R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u00020*X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010+R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006]"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "receiptReaderHandler",
        "Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "buyerFlowReceiptManager",
        "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
        "checkoutInformationEventLogger",
        "Lcom/squareup/log/CheckoutInformationEventLogger;",
        "curatedImage",
        "Lcom/squareup/merchantimages/CuratedImage;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "smsMarketingWorkflow",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;",
        "yieldToFlowWorker",
        "Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;",
        "digitalReceiptSender",
        "Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;",
        "paperReceiptSender",
        "Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;",
        "receiptDecliner",
        "Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;",
        "(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;)V",
        "cardListenerWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/cardreader/CardReaderId;",
        "imageWorker",
        "Lcom/squareup/picasso/RequestCreator;",
        "imageWorker$annotations",
        "()V",
        "receiptReaderRegistrar",
        "com/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1",
        "Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;",
        "autoCloseForState",
        "",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "receiptScreenState",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;",
        "autoReceiptCompleteTimeout",
        "",
        "(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Ljava/lang/Long;)V",
        "getAddCardState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
        "cardTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "shouldShowAddCardButton",
        "",
        "getBuyerLanguageState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
        "showLanguageSelection",
        "languageTapName",
        "getCallToActionState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;",
        "cardReaderId",
        "getDesintationIfAutoSendReceipt",
        "",
        "shouldAutoSendReceipt",
        "getEmailInputScreen",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "props",
        "requestCreator",
        "getReceiptOptionsState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;",
        "input",
        "state",
        "getSmsInputScreen",
        "getUpdateCustomerState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
        "viewTapName",
        "addTapName",
        "shouldHideCustomerButton",
        "hasCustomer",
        "initialState",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "shouldRemoveCard",
        "snapshotState",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$Companion;

.field public static final MERCHANT_IMAGE_WORKER:Ljava/lang/String; = "merchant image worker"

.field public static final READER_REGISTRAR_WORKER:Ljava/lang/String; = "reader registrar worker"

.field public static final RECEIPT_COMPLETE_DELAY_WORKER:Ljava/lang/String; = "receipt complete delay worker"

.field public static final RECEIPT_DELAY_WORKER:Ljava/lang/String; = "receipt delay worker"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

.field private final cardListenerWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

.field private final imageWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/picasso/RequestCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final paperReceiptSender:Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;

.field private final receiptDecliner:Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;

.field private final receiptReaderHandler:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

.field private final receiptReaderRegistrar:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;

.field private final smsMarketingWorkflow:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

.field private final yieldToFlowWorker:Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->Companion:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "receiptReaderHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHub"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowReceiptManager"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutInformationEventLogger"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "curatedImage"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smsMarketingWorkflow"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "yieldToFlowWorker"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "digitalReceiptSender"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paperReceiptSender"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptDecliner"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptReaderHandler:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p7, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->smsMarketingWorkflow:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

    iput-object p8, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->yieldToFlowWorker:Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;

    iput-object p9, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    iput-object p10, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->paperReceiptSender:Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;

    iput-object p11, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptDecliner:Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;

    .line 130
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptReaderHandler:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->getCardListenerRelay()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    check-cast p1, Lio/reactivex/Observable;

    .line 639
    sget-object p2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    const-string p2, "this.toFlowable(BUFFER)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    const-string p3, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    if-eqz p1, :cond_1

    .line 641
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 642
    const-class p4, Lcom/squareup/cardreader/CardReaderId;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p4

    new-instance p5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p5, p4, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p5, Lcom/squareup/workflow/Worker;

    .line 639
    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->cardListenerWorker:Lcom/squareup/workflow/Worker;

    .line 132
    new-instance p1, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptReaderRegistrar:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;

    .line 144
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-interface {p1}, Lcom/squareup/merchantimages/CuratedImage;->loadDarkened()Lio/reactivex/Observable;

    move-result-object p1

    .line 145
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object p4

    invoke-virtual {p1, p4}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p4, "curatedImage.loadDarkene\u2026rvable<RequestCreator>())"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 643
    sget-object p4, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {p1, p4}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    if-eqz p1, :cond_0

    .line 645
    invoke-static {p1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 646
    const-class p2, Lcom/squareup/picasso/RequestCreator;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 643
    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->imageWorker:Lcom/squareup/workflow/Worker;

    return-void

    .line 645
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, p3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 641
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, p3}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getCardReaderHub$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/cardreader/CardReaderHub;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-object p0
.end method

.method public static final synthetic access$getCheckoutInformationEventLogger$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/log/CheckoutInformationEventLogger;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    return-object p0
.end method

.method public static final synthetic access$getDigitalReceiptSender$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    return-object p0
.end method

.method public static final synthetic access$getPaperReceiptSender$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->paperReceiptSender:Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;

    return-object p0
.end method

.method public static final synthetic access$getReceiptDecliner$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptDecliner:Lcom/squareup/checkoutflow/receipt/ReceiptDecliner;

    return-object p0
.end method

.method public static final synthetic access$getReceiptReaderHandler$p(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;
    .locals 0

    .line 115
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptReaderHandler:Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    return-object p0
.end method

.method private final autoCloseForState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Ljava/lang/Long;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 435
    instance-of v0, p2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    if-eqz v0, :cond_1

    .line 438
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    goto :goto_0

    :cond_0
    const-wide/32 p2, 0x1d4c0

    :goto_0
    move-wide v2, p2

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/Worker$Companion;->timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;

    move-result-object p2

    .line 440
    new-instance p3, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$autoCloseForState$1;

    invoke-direct {p3, p0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$autoCloseForState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    const-string v0, "receipt complete delay worker"

    .line 437
    invoke-interface {p1, p2, v0, p3}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 448
    :cond_1
    instance-of p2, p2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;

    if-eqz p2, :cond_2

    goto :goto_1

    .line 453
    :cond_2
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    const-wide/32 v1, 0x1d4c0

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/Worker$Companion;->timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;

    move-result-object p2

    .line 454
    new-instance p3, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$autoCloseForState$2;

    invoke-direct {p3, p0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$autoCloseForState$2;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)V

    check-cast p3, Lkotlin/jvm/functions/Function1;

    const-string v0, "receipt delay worker"

    .line 452
    invoke-interface {p1, p2, v0, p3}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    :goto_1
    return-void
.end method

.method private final getAddCardState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Z)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;",
            "Lcom/squareup/analytics/RegisterTapName;",
            "Z)",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;"
        }
    .end annotation

    if-eqz p3, :cond_0

    .line 567
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;

    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getAddCardState$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getAddCardState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    goto :goto_0

    .line 572
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;

    move-object p3, p1

    check-cast p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    :goto_0
    return-object p3
.end method

.method private final getBuyerLanguageState(ZLcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;",
            "Lcom/squareup/analytics/RegisterTapName;",
            ")",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 531
    new-instance p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getBuyerLanguageState$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getBuyerLanguageState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    goto :goto_0

    .line 536
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Hidden;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    :goto_0
    return-object p1
.end method

.method private final getCallToActionState(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;
    .locals 0

    .line 580
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->shouldRemoveCard(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderId;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;->REMOVE_CARD:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;->RECEIPT_PROMPT:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;

    :goto_0
    return-object p1
.end method

.method private final getDesintationIfAutoSendReceipt(Z)Ljava/lang/String;
    .locals 2

    .line 601
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;->getDefaultEmail()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    move-object v1, v0

    :cond_0
    return-object v1
.end method

.method private final getEmailInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;",
            "Lcom/squareup/picasso/RequestCreator;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 345
    new-instance v7, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    .line 346
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object v2

    .line 348
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$EmailInput;

    .line 349
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    invoke-interface {v3}, Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;->getPrefilledEmail()Ljava/lang/String;

    move-result-object v3

    .line 350
    new-instance v4, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;

    invoke-direct {v4, p0, p1, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/picasso/RequestCreator;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 370
    new-instance v5, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$2;

    invoke-direct {v5, p0, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getEmailInputScreen$2;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/picasso/RequestCreator;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 348
    invoke-direct {v0, v3, v4, p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$EmailInput;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    .line 375
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v4

    .line 377
    iget-object v6, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    move-object v0, v7

    move-object v5, p3

    .line 345
    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)V

    check-cast v7, Lcom/squareup/workflow/legacy/V2Screen;

    .line 630
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 631
    const-class p2, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 632
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 630
    invoke-direct {p1, p2, v7, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final getReceiptOptionsState(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;"
        }
    .end annotation

    .line 467
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState$EmailEnabled;

    .line 468
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    invoke-interface {v1}, Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;->getPrefilledEmail()Ljava/lang/String;

    move-result-object v1

    .line 469
    new-instance v2, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$emailState$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$emailState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 467
    invoke-direct {v0, v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState$EmailEnabled;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 475
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getSupportsSms()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 476
    new-instance p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsEnabled;

    .line 477
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    invoke-interface {v1}, Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;->getPrefilledSms()Ljava/lang/String;

    move-result-object v1

    .line 478
    new-instance v2, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$smsState$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$smsState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 476
    invoke-direct {p1, v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsEnabled;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState;

    goto :goto_0

    .line 484
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsDisabled;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState$SmsDisabled;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState;

    .line 488
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->paperReceiptSender:Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;

    invoke-interface {v1}, Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;->supportsPaper()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 489
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;

    .line 490
    new-instance v2, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$paperState$1;

    invoke-direct {v2, p0, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$paperState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 489
    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;

    goto :goto_1

    .line 501
    :cond_1
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperDisabled;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperDisabled;

    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;

    .line 505
    :goto_1
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->paperReceiptSender:Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;

    invoke-interface {v2}, Lcom/squareup/checkoutflow/receipt/PaperReceiptSender;->supportsFormalPaper()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 506
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState$FormalPaperEnabled;

    .line 507
    new-instance v3, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$formalPaperState$1;

    invoke-direct {v3, p0, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getReceiptOptionsState$formalPaperState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 506
    invoke-direct {v2, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState$FormalPaperEnabled;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState;

    goto :goto_2

    .line 519
    :cond_2
    sget-object p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState$FormalPaperDisabled;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState$FormalPaperDisabled;

    move-object v2, p2

    check-cast v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState;

    .line 522
    :goto_2
    new-instance p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;

    check-cast v0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState;

    invoke-direct {p2, v0, p1, v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$EmailState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$SmsState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$FormalPaperState;)V

    return-object p2
.end method

.method private final getSmsInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;",
            "Lcom/squareup/picasso/RequestCreator;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen;"
        }
    .end annotation

    .line 386
    new-instance v7, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    .line 387
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    .line 388
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object v2

    .line 389
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;

    .line 390
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->digitalReceiptSender:Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;

    invoke-interface {v3}, Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;->getPrefilledSms()Ljava/lang/String;

    move-result-object v3

    .line 391
    new-instance v4, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getSmsInputScreen$1;

    invoke-direct {v4, p0, p1, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getSmsInputScreen$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/picasso/RequestCreator;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 418
    new-instance v5, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getSmsInputScreen$2;

    invoke-direct {v5, p0, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getSmsInputScreen$2;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/picasso/RequestCreator;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v5}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 389
    invoke-direct {v0, v3, v4, p2}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState$SmsInput;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;

    .line 423
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v4

    .line 425
    iget-object v6, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    move-object v0, v7

    move-object v5, p3

    .line 386
    invoke-direct/range {v0 .. v6}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$ScreenState;Lcom/squareup/CountryCode;Lcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;)V

    check-cast v7, Lcom/squareup/workflow/legacy/V2Screen;

    .line 635
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 636
    const-class p2, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 637
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 635
    invoke-direct {p1, p2, v7, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method private final getUpdateCustomerState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/analytics/RegisterTapName;ZZ)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;",
            "Lcom/squareup/analytics/RegisterTapName;",
            "Lcom/squareup/analytics/RegisterTapName;",
            "ZZ)",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;"
        }
    .end annotation

    if-eqz p4, :cond_0

    .line 548
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    goto :goto_0

    :cond_0
    if-eqz p5, :cond_1

    .line 550
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;

    new-instance p4, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getUpdateCustomerState$1;

    invoke-direct {p4, p0, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getUpdateCustomerState$1;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, p4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, p3

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    goto :goto_0

    .line 554
    :cond_1
    new-instance p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;

    new-instance p4, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getUpdateCustomerState$2;

    invoke-direct {p4, p0, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$getUpdateCustomerState$2;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, p4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, p2

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    :goto_0
    return-object p1
.end method

.method private static synthetic imageWorker$annotations()V
    .locals 0

    return-void
.end method

.method private final shouldRemoveCard(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    .line 591
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 594
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    const-string p2, "cardReader.cardReaderInfo"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/receipt/ReceiptState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    sget-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptState;->Companion:Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;

    .line 153
    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-interface {v1}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->shouldReceiptScreenSkipSelection()Z

    move-result v1

    .line 154
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShouldAutoSendReceipt()Z

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getDesintationIfAutoSendReceipt(Z)Ljava/lang/String;

    move-result-object p1

    .line 152
    invoke-virtual {v0, p2, v1, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$Companion;->start(Lcom/squareup/workflow/Snapshot;ZLjava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->initialState(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/receipt/ReceiptState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptInput;

    check-cast p2, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->render(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/ReceiptInput;",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/ReceiptResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    const-string v0, "props"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object v0, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->receiptReaderRegistrar:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$receiptReaderRegistrar$1;

    check-cast v0, Lcom/squareup/workflow/Worker;

    const-string v1, "reader registrar worker"

    invoke-static {v9, v0, v1}, Lcom/squareup/workflow/RenderContextKt;->runningWorker(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;)V

    .line 165
    iget-object v0, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->imageWorker:Lcom/squareup/workflow/Worker;

    new-instance v1, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$1;

    invoke-direct {v1, v8}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const-string v2, "merchant image worker"

    invoke-interface {v9, v0, v2, v1}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 169
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getReceiptScreenState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getAutoReceiptCompleteTimeout()Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v6, v9, v0, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->autoCloseForState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Ljava/lang/Long;)V

    .line 171
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getReceiptScreenState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    move-result-object v0

    .line 172
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptSelectionState;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptSelectionState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v10, ""

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->cardListenerWorker:Lcom/squareup/workflow/Worker;

    const/4 v2, 0x0

    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$2;

    invoke-direct {v0, v7, v8}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 185
    new-instance v25, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

    .line 186
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getDisplayName()Ljava/lang/String;

    move-result-object v12

    .line 187
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v13

    .line 188
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object v14

    .line 189
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete()Z

    move-result v15

    .line 190
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v16

    .line 191
    iget-object v11, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    .line 192
    iget-object v0, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getCallToActionState(Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;

    move-result-object v18

    .line 193
    invoke-direct/range {p0 .. p3}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getReceiptOptionsState(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/checkoutflow/receipt/ReceiptState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;

    move-result-object v19

    .line 195
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShowLanguageSelection()Z

    move-result v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 194
    invoke-direct {v6, v0, v9, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getBuyerLanguageState(ZLcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object v20

    .line 199
    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 200
    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 201
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShouldHideCustomerButton()Z

    move-result v4

    .line 202
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getHasCustomer()Z

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    .line 197
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getUpdateCustomerState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/analytics/RegisterTapName;ZZ)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object v21

    .line 206
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SELECTION_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 207
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShowAddCardButton()Z

    move-result v1

    .line 204
    invoke-direct {v6, v9, v0, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getAddCardState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Z)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object v22

    .line 209
    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$3;

    invoke-direct {v0, v6}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v23

    .line 213
    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;

    invoke-direct {v0, v6, v8}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$4;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v24

    move-object v0, v11

    move-object/from16 v11, v25

    move-object/from16 v17, v0

    .line 185
    invoke-direct/range {v11 .. v24}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;-><init>(Ljava/lang/String;Lcom/squareup/CountryCode;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$CallToActionState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v0, v25

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 615
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 616
    const-class v2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 617
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 615
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 223
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_3

    .line 225
    :cond_0
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$EmailInput;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$EmailInput;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 226
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-direct {v6, v7, v9, v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getEmailInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v0, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_3

    .line 228
    :cond_1
    instance-of v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$SmsInput;

    if-eqz v1, :cond_2

    .line 229
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-direct {v6, v7, v9, v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getSmsInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v0, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_3

    .line 231
    :cond_2
    instance-of v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;

    if-eqz v1, :cond_3

    .line 233
    iget-object v1, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->smsMarketingWorkflow:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 234
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;

    .line 235
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShouldHideCustomerButton()Z

    move-result v11

    .line 236
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getHasCustomer()Z

    move-result v12

    .line 237
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShowAddCardButton()Z

    move-result v13

    .line 238
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShowLanguageSelection()Z

    move-result v14

    .line 239
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getBusinessName()Ljava/lang/String;

    move-result-object v15

    .line 240
    move-object v3, v0

    check-cast v3, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;->getSmsDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    move-result-object v16

    .line 241
    invoke-virtual {v3}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;->getAcceptLanguage()Ljava/lang/String;

    move-result-object v17

    move-object v10, v2

    .line 234
    invoke-direct/range {v10 .. v17}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;-><init>(ZZZZLjava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;Ljava/lang/String;)V

    const/4 v10, 0x0

    .line 243
    new-instance v3, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;

    invoke-direct {v3, v0, v8}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$5;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    move-object v11, v3

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object/from16 v7, p3

    move-object v8, v1

    move-object v9, v2

    .line 232
    invoke-static/range {v7 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto/16 :goto_3

    .line 278
    :cond_3
    instance-of v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    if-eqz v1, :cond_5

    .line 279
    iget-object v1, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->yieldToFlowWorker:Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;

    check-cast v1, Lcom/squareup/workflow/Worker;

    new-instance v2, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$6;

    invoke-direct {v2, v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$6;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const-string v3, "Receipt Complete"

    invoke-interface {v9, v1, v3, v2}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 284
    iget-object v1, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->checkoutInformationEventLogger:Lcom/squareup/log/CheckoutInformationEventLogger;

    invoke-interface {v1}, Lcom/squareup/log/CheckoutInformationEventLogger;->updateTentativeEndTime()V

    .line 285
    new-instance v24, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    .line 286
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getDisplayName()Ljava/lang/String;

    move-result-object v12

    .line 287
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getPaymentTypeInfo()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;

    move-result-object v13

    .line 288
    check-cast v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;->getReceiptSelection()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;

    move-result-object v14

    .line 289
    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;->getSmsMarketingResult()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;

    move-result-object v15

    .line 290
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->isPaymentComplete()Z

    move-result v16

    .line 291
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v17

    .line 292
    iget-object v8, v6, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    .line 294
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShowLanguageSelection()Z

    move-result v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 293
    invoke-direct {v6, v0, v9, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getBuyerLanguageState(ZLcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object v19

    .line 297
    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 298
    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 299
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShouldHideCustomerButton()Z

    move-result v4

    .line 300
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getHasCustomer()Z

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    .line 296
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getUpdateCustomerState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/analytics/RegisterTapName;ZZ)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object v20

    .line 303
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_COMPLETE_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 304
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getShowAddCardButton()Z

    move-result v1

    .line 302
    invoke-direct {v6, v9, v0, v1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getAddCardState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Z)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object v21

    .line 306
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/ReceiptInput;->getCanClickToContinue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 307
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState$Enabled;

    sget-object v1, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$7;->INSTANCE:Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState$Enabled;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    goto :goto_0

    .line 311
    :cond_4
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState$Disabled;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState$Disabled;

    check-cast v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;

    :goto_0
    move-object/from16 v22, v0

    .line 313
    new-instance v0, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$8;

    invoke-direct {v0, v6}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$8;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v23

    move-object/from16 v11, v24

    move-object/from16 v18, v8

    .line 285
    invoke-direct/range {v11 .. v23}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;ZLcom/squareup/picasso/RequestCreator;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$ClickAnywhereState;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v0, v24

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 620
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 621
    const-class v2, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 622
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 620
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 318
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto :goto_3

    .line 320
    :cond_5
    instance-of v1, v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;

    if-eqz v1, :cond_8

    .line 321
    sget-object v11, Lcom/squareup/container/PosLayering;->Companion:Lcom/squareup/container/PosLayering$Companion;

    .line 322
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    .line 323
    move-object v2, v0

    check-cast v2, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;->getPreviousInputState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    move-result-object v3

    .line 324
    new-instance v4, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$9;

    invoke-direct {v4, v6, v0, v8}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow$render$9;-><init>(Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;Lcom/squareup/checkoutflow/receipt/ReceiptState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {v9, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 322
    invoke-direct {v1, v3, v0}, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 625
    new-instance v15, Lcom/squareup/workflow/legacy/Screen;

    .line 626
    const-class v0, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 627
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 625
    invoke-direct {v15, v0, v1, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 331
    invoke-virtual {v2}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;->getPreviousInputState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;

    move-result-object v0

    .line 332
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$EmailInput;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$EmailInput;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-direct {v6, v7, v9, v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getEmailInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    :goto_1
    move-object v12, v0

    goto :goto_2

    .line 333
    :cond_6
    sget-object v1, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$SmsInput;->INSTANCE:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState$SmsInput;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-direct {v6, v7, v9, v0}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->getSmsInputScreen(Lcom/squareup/checkoutflow/receipt/ReceiptInput;Lcom/squareup/workflow/RenderContext;Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    goto :goto_1

    :goto_2
    const/16 v16, 0x6

    const/16 v17, 0x0

    .line 321
    invoke-static/range {v11 .. v17}, Lcom/squareup/container/PosLayering$Companion;->dialogStack$default(Lcom/squareup/container/PosLayering$Companion;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;ILjava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    :goto_3
    return-object v0

    .line 333
    :cond_7
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 321
    :cond_8
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/receipt/ReceiptState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 604
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/ReceiptState;->getReceiptScreenState()Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;

    move-result-object p1

    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/checkoutflow/receipt/ReceiptState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/RealReceiptWorkflow;->snapshotState(Lcom/squareup/checkoutflow/receipt/ReceiptState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
