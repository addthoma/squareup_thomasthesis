.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ReceiptSmsMarketingWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptSmsMarketingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptSmsMarketingWorkflow.kt\ncom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,287:1\n32#2,12:288\n85#3:300\n240#4:301\n276#5:302\n149#6,5:303\n149#6,5:308\n149#6,5:313\n*E\n*S KotlinDebug\n*F\n+ 1 ReceiptSmsMarketingWorkflow.kt\ncom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow\n*L\n76#1,12:288\n117#1:300\n117#1:301\n117#1:302\n159#1,5:303\n199#1,5:308\n210#1,5:313\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001B;\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0008\u0001\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u001c\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0014\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0018H\u0002J,\u0010\u001c\u001a\u00020\u001d2\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J,\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020#2\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010\'\u001a\u00020!H\u0002J<\u0010(\u001a\u00020)2\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010*\u001a\u00020!2\u0006\u0010+\u001a\u00020!2\u0006\u0010,\u001a\u00020#2\u0006\u0010-\u001a\u00020#H\u0002J\u001a\u0010.\u001a\u00020\u00032\u0006\u0010/\u001a\u00020\u00022\u0008\u00100\u001a\u0004\u0018\u000101H\u0016J \u00102\u001a\u00020\u00032\u0006\u00103\u001a\u00020\u00022\u0006\u00104\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0003H\u0016JN\u00105\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010/\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u00032\u0012\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001fH\u0016J\u0010\u00106\u001a\u0002012\u0006\u0010\u0019\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "receiptService",
        "Lcom/squareup/checkoutflow/receipt/ReceiptService;",
        "taskQueue",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "smsMarketingInputHelper",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/checkoutflow/receipt/ReceiptService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;Lcom/squareup/settings/server/Features;)V",
        "acceptCouponActionV1",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;",
        "acceptCouponActionV2",
        "getAddCardState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "cardTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "shouldShowAddCardButton",
        "",
        "getBuyerLanguageState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
        "showLanguageSelection",
        "languageTapName",
        "getUpdateCustomerState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
        "viewTapName",
        "addTapName",
        "shouldHideCustomerButton",
        "hasCustomer",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "render",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;

.field private final smsMarketingInputHelper:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/checkoutflow/receipt/ReceiptService;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/queue/retrofit/RetrofitQueue;
        .annotation runtime Lcom/squareup/queue/Tasks;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taskQueue"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smsMarketingInputHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->smsMarketingInputHelper:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;

    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final acceptCouponActionV1(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;"
        }
    .end annotation

    .line 270
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 271
    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingAcceptTask;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;->getSubscriber()Lcom/squareup/protos/postoffice/sms/Subscriber;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;->getInvitationId()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptSmsMarketingAcceptTask;-><init>(Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;)V

    .line 270
    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 274
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$acceptCouponActionV1$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$acceptCouponActionV1$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1, v0}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final acceptCouponActionV2()Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;"
        }
    .end annotation

    .line 279
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$acceptCouponActionV2$1;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$acceptCouponActionV2$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v0

    return-object v0
.end method

.method public static final synthetic access$acceptCouponActionV1(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->acceptCouponActionV1(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$acceptCouponActionV2(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 62
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->acceptCouponActionV2()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getTaskQueue$p(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method private final getAddCardState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Z)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;",
            "Lcom/squareup/analytics/RegisterTapName;",
            "Z)",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;"
        }
    .end annotation

    if-eqz p3, :cond_0

    .line 258
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getAddCardState$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getAddCardState$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Showing;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    goto :goto_0

    .line 263
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState$Hidden;

    move-object p3, p1

    check-cast p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    :goto_0
    return-object p3
.end method

.method private final getBuyerLanguageState(ZLcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;",
            "Lcom/squareup/analytics/RegisterTapName;",
            ")",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 222
    new-instance p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getBuyerLanguageState$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getBuyerLanguageState$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Showing;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    goto :goto_0

    .line 227
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState$Hidden;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    :goto_0
    return-object p1
.end method

.method private final getUpdateCustomerState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/analytics/RegisterTapName;ZZ)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;",
            "Lcom/squareup/analytics/RegisterTapName;",
            "Lcom/squareup/analytics/RegisterTapName;",
            "ZZ)",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;"
        }
    .end annotation

    if-eqz p4, :cond_0

    .line 239
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$Hidden;

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    goto :goto_0

    :cond_0
    if-eqz p5, :cond_1

    .line 241
    new-instance p3, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;

    new-instance p4, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getUpdateCustomerState$1;

    invoke-direct {p4, p0, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getUpdateCustomerState$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, p4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$ViewCustomer;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, p3

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    goto :goto_0

    .line 245
    :cond_1
    new-instance p2, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;

    new-instance p4, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getUpdateCustomerState$2;

    invoke-direct {p4, p0, p3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$getUpdateCustomerState$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/analytics/RegisterTapName;)V

    check-cast p4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, p4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState$AddCustomer;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, p2

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 288
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 293
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 295
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 296
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 297
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 299
    :cond_3
    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 76
    :cond_4
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingLoading;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingLoading;

    move-object v1, p1

    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->initialState(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getAcceptLanguage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getAcceptLanguage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    .line 84
    sget-object p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingLoading;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingLoading;

    move-object p3, p1

    check-cast p3, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    :cond_0
    return-object p3
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;

    check-cast p2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;

    check-cast p3, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->onPropsChanged(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;

    check-cast p2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->render(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    const-string v0, "props"

    move-object/from16 v9, p1

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    instance-of v0, v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingLoading;

    const-string v10, ""

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 97
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getSmsDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;->getDestinationValue()Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getSmsDestination()Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;->getManualInput()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    new-instance v2, Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-direct {v2, v1, v0, v1}, Lcom/squareup/protos/postoffice/sms/Subscriber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_0
    new-instance v2, Lcom/squareup/protos/postoffice/sms/Subscriber;

    invoke-direct {v2, v0, v1, v1}, Lcom/squareup/protos/postoffice/sms/Subscriber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :goto_0
    iget-object v0, v6, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->receiptService:Lcom/squareup/checkoutflow/receipt/ReceiptService;

    .line 105
    new-instance v3, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;

    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getAcceptLanguage()Ljava/lang/String;

    move-result-object v4

    .line 105
    invoke-direct {v3, v1, v2, v4}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;)V

    .line 104
    invoke-interface {v0, v3}, Lcom/squareup/checkoutflow/receipt/ReceiptService;->getSmsMarketingInvitation(Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v3, "receiptService.getSmsMar\u2026          .toObservable()"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v3, 0x320

    .line 114
    sget-object v5, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-string v7, "MIN_SPINNER_TIME_UNIT"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v7, v6, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    .line 113
    invoke-static {v0, v3, v4, v5, v7}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v3, "receiptService.getSmsMar\u2026          .firstOrError()"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    sget-object v3, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v3, v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 301
    invoke-static {v3}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 302
    const-class v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    sget-object v3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v4, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v1, v3

    check-cast v1, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 118
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;

    invoke-direct {v0, v6, v2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/protos/postoffice/sms/Subscriber;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p3

    move-object v2, v3

    move-object v3, v4

    move v4, v5

    move-object v5, v7

    .line 103
    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 142
    new-instance v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;

    .line 145
    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 146
    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 147
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getHideCustomerButton()Z

    move-result v4

    .line 148
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getHasCustomer()Z

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    .line 143
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->getUpdateCustomerState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/analytics/RegisterTapName;ZZ)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object v0

    .line 152
    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getShowAddCardButton()Z

    move-result v2

    .line 150
    invoke-direct {v6, v8, v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->getAddCardState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Z)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object v1

    .line 155
    new-instance v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$2;

    invoke-direct {v2, v6}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 142
    invoke-direct {v7, v0, v1, v2}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;)V

    check-cast v7, Lcom/squareup/workflow/legacy/V2Screen;

    .line 304
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    .line 305
    const-class v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 306
    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    .line 304
    invoke-direct {v0, v1, v7, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 160
    sget-object v1, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v0, v1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_1

    .line 162
    :cond_1
    instance-of v0, v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;

    if-eqz v0, :cond_2

    .line 163
    new-instance v20, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    .line 164
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getBusinessName()Ljava/lang/String;

    move-result-object v12

    .line 165
    move-object v0, v7

    check-cast v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;->getCouponInfo()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    move-result-object v13

    .line 167
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getShowLanguageSelection()Z

    move-result v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_LANGUAGE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    .line 166
    invoke-direct {v6, v0, v8, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->getBuyerLanguageState(ZLcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    move-result-object v14

    .line 171
    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_VIEW_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 172
    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CUSTOMER:Lcom/squareup/analytics/RegisterTapName;

    .line 173
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getHideCustomerButton()Z

    move-result v4

    .line 174
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getHasCustomer()Z

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    .line 169
    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->getUpdateCustomerState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/analytics/RegisterTapName;ZZ)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    move-result-object v15

    .line 178
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->RECEIPT_SMS_MARKETING_ADD_CARD:Lcom/squareup/analytics/RegisterTapName;

    .line 179
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingProps;->getShowAddCardButton()Z

    move-result v1

    .line 176
    invoke-direct {v6, v8, v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->getAddCardState(Lcom/squareup/workflow/RenderContext;Lcom/squareup/analytics/RegisterTapName;Z)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    move-result-object v16

    .line 181
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$3;

    invoke-direct {v0, v6}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v17

    .line 185
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$4;

    invoke-direct {v0, v6, v7}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$4;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v18

    .line 192
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$5;

    invoke-direct {v0, v6, v7}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$5;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v19

    move-object/from16 v11, v20

    .line 163
    invoke-direct/range {v11 .. v19}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v0, v20

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 309
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 310
    const-class v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 311
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 309
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 200
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto :goto_1

    .line 202
    :cond_2
    instance-of v0, v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingInput;

    if-eqz v0, :cond_3

    .line 203
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen;

    .line 204
    iget-object v2, v6, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->smsMarketingInputHelper:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;

    invoke-virtual {v2, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputHelper;->screenTextData(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen$TextData;

    move-result-object v1

    .line 208
    sget-object v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$6;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 209
    sget-object v3, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$7;->INSTANCE:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$7;

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 203
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen$TextData;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 314
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 315
    const-class v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v10}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 316
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 314
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 211
    sget-object v0, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 285
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;->snapshotState(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
