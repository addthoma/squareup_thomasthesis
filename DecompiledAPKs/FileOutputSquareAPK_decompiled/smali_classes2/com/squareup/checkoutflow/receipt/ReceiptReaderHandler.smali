.class public final Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;
.super Ljava/lang/Object;
.source "ReceiptReaderHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012R\u001f\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;",
        "",
        "emvDipScreenHandler",
        "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
        "(Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;)V",
        "cardListenerRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/cardreader/CardReaderId;",
        "kotlin.jvm.PlatformType",
        "getCardListenerRelay",
        "()Lcom/jakewharton/rxrelay2/PublishRelay;",
        "cardReaderAttachListener",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
        "emvCardInsertRemoveProcessor",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "register",
        "",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "unregister",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardListenerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

.field private emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "emvDipScreenHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 26
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<CardReaderId>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->cardListenerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method


# virtual methods
.method public final getCardListenerRelay()Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->cardListenerRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public final register(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 3

    const-string v0, "cardReaderHub"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$1;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$1;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;)V

    check-cast v0, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    .line 45
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    if-nez v1, :cond_0

    const-string v2, "emvCardInsertRemoveProcessor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 47
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler$register$2;-><init>(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;)V

    check-cast v0, Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    iput-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    .line 58
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    if-nez v0, :cond_1

    const-string v1, "cardReaderAttachListener"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method

.method public final unregister(Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 3

    const-string v0, "cardReaderHub"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->emvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    if-nez v1, :cond_0

    const-string v2, "emvCardInsertRemoveProcessor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 66
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;->cardReaderAttachListener:Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;

    if-nez v0, :cond_1

    const-string v1, "cardReaderAttachListener"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    return-void
.end method
