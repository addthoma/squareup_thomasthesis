.class public final Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "ReceiptScreenViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "receiptSelectionFactory",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;",
        "receiptInputFactory",
        "Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;",
        "receiptCompleteFactory",
        "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;",
        "receiptSmsMarketingSpinnerLayoutRunnerFactory",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;",
        "receiptSmsMarketingPromptFactory",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;",
        "receiptErrorDialogFactoryFactory",
        "Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;",
        "smsMarketingInputFactory",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;",
        "(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;)V
    .locals 27
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    const-string v7, "receiptSelectionFactory"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "receiptInputFactory"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "receiptCompleteFactory"

    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "receiptSmsMarketingSpinnerLayoutRunnerFactory"

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "receiptSmsMarketingPromptFactory"

    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "receiptErrorDialogFactoryFactory"

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "smsMarketingInputFactory"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x7

    new-array v7, v7, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 32
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 33
    sget-object v9, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$Companion;

    invoke-virtual {v9}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 34
    sget v10, Lcom/squareup/checkoutflow/receipt/impl/R$layout;->receipt_selection_view:I

    .line 35
    new-instance v11, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$1;

    invoke-direct {v11, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$1;-><init>(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionCoordinator$Factory;)V

    move-object v13, v11

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 36
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v20, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x1df

    const/16 v25, 0x0

    move-object v14, v0

    invoke-direct/range {v14 .. v25}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v12, 0x0

    const/16 v14, 0x8

    move-object v11, v0

    .line 32
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v8, 0x0

    aput-object v0, v7, v8

    .line 38
    sget-object v9, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 39
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v10

    .line 40
    sget v11, Lcom/squareup/checkout/R$layout;->buyer_input_view:I

    .line 41
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$2;

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$2;-><init>(Lcom/squareup/checkoutflow/receipt/receiptinput/ReceiptInputCoordinator$Factory;)V

    move-object v14, v0

    check-cast v14, Lkotlin/jvm/functions/Function1;

    .line 42
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v21, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x1df

    const/16 v26, 0x0

    move-object v15, v0

    invoke-direct/range {v15 .. v26}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v13, 0x0

    const/16 v15, 0x8

    move-object v12, v0

    .line 38
    invoke-static/range {v9 .. v16}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v7, v1

    .line 44
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 45
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 46
    sget v10, Lcom/squareup/checkoutflow/receipt/impl/R$layout;->receipt_complete_view:I

    .line 47
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$3;

    invoke-direct {v0, v2}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$3;-><init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 48
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v20, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v21, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x1df

    const/16 v25, 0x0

    move-object v14, v0

    invoke-direct/range {v14 .. v25}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v12, 0x0

    const/16 v14, 0x8

    move-object v11, v0

    .line 44
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v7, v1

    .line 50
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 51
    const-class v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v9

    .line 52
    sget v10, Lcom/squareup/checkoutflow/receipt/impl/R$layout;->receipt_sms_marketing_spinner_view:I

    .line 53
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$4;

    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$4;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x0

    const/16 v14, 0xc

    .line 50
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x3

    aput-object v0, v7, v1

    .line 55
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 56
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v9

    .line 57
    sget v10, Lcom/squareup/checkoutflow/receipt/impl/R$layout;->receipt_sms_marketing_prompt_view:I

    .line 58
    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$5;

    invoke-direct {v0, v4}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$5;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptCoordinator$Factory;)V

    move-object v13, v0

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 59
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v20, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    move-object v14, v0

    invoke-direct/range {v14 .. v25}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v14, 0x8

    move-object v11, v0

    .line 55
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x4

    aput-object v0, v7, v1

    .line 61
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 62
    sget-object v1, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 63
    new-instance v2, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$6;

    invoke-direct {v2, v5}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$6;-><init>(Lcom/squareup/checkoutflow/receipt/receipterrordialog/ReceiptErrorDialogFactory$Factory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 61
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x5

    aput-object v0, v7, v1

    .line 65
    sget-object v8, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 66
    const-class v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/ReceiptSmsMarketingInputScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v9

    .line 67
    sget v10, Lcom/squareup/checkout/R$layout;->buyer_input_view:I

    .line 68
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v17, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1df

    const/16 v22, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v22}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 69
    new-instance v1, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$7;

    invoke-direct {v1, v6}, Lcom/squareup/checkoutflow/receipt/ReceiptScreenViewFactory$7;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketinginput/SmsMarketingInputLayoutRunner$Factory;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    const/16 v14, 0x8

    .line 65
    invoke-static/range {v8 .. v15}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x6

    aput-object v0, v7, v1

    move-object/from16 v0, p0

    .line 31
    invoke-direct {v0, v7}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
