.class public final Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;
.super Ljava/lang/Object;
.source "InstallmentsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/InstallmentsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstallmentsState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstallmentsState.kt\ncom/squareup/checkoutflow/installments/InstallmentsState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,80:1\n180#2:81\n*E\n*S KotlinDebug\n*F\n+ 1 InstallmentsState.kt\ncom/squareup/checkoutflow/installments/InstallmentsState$Companion\n*L\n58#1:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "byteString",
        "Lokio/ByteString;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/checkoutflow/installments/InstallmentsState;
    .locals 3

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 59
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 61
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;

    .line 62
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 61
    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto/16 :goto_0

    .line 64
    :cond_0
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsLinkOptions;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsLinkOptions;

    .line 65
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 64
    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsLinkOptions;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto/16 :goto_0

    .line 67
    :cond_1
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoading;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoading;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoading;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto :goto_0

    .line 68
    :cond_2
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;

    .line 69
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    .line 68
    invoke-direct {v0, v1, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto :goto_0

    .line 71
    :cond_3
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto :goto_0

    .line 72
    :cond_4
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto :goto_0

    .line 73
    :cond_5
    const-class v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsSent;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsSent;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsSent;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    :goto_0
    return-object v0

    .line 74
    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
