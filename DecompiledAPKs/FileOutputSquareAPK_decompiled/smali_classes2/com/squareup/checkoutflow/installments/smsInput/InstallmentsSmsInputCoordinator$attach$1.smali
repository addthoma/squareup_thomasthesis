.class final Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;
.super Ljava/lang/Object;
.source "InstallmentsSmsInputCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;->this$0:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;->$view:Landroid/view/View;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->access$update(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;->accept(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V

    return-void
.end method
