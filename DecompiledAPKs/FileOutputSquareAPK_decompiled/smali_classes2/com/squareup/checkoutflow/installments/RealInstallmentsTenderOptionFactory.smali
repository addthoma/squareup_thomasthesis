.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;
.super Ljava/lang/Object;
.source "RealInstallmentsTenderOptionFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000I\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0011\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\u000f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0012\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "tenderPaymentResultHandler",
        "Ldagger/Lazy;",
        "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
        "installmentsWorkflow",
        "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
        "(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)V",
        "enabledStrategy",
        "Lkotlin/Function1;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
        "",
        "titleStrategy",
        "",
        "workflow",
        "com/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1",
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;",
        "getTenderOption",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;",
        "getTenderOptionKey",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final enabledStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final installmentsWorkflow:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;

.field private final res:Lcom/squareup/util/Res;

.field private final tenderPaymentResultHandler:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final titleStrategy:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption$DisplayData;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final workflow:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderPaymentResultHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installmentsWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->tenderPaymentResultHandler:Ldagger/Lazy;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->installmentsWorkflow:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;

    .line 36
    new-instance p1, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$titleStrategy$1;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->titleStrategy:Lkotlin/jvm/functions/Function1;

    .line 49
    sget-object p1, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$enabledStrategy$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$enabledStrategy$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->enabledStrategy:Lkotlin/jvm/functions/Function1;

    .line 53
    new-instance p1, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->workflow:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;

    return-void
.end method

.method public static final synthetic access$getInstallmentsWorkflow$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->installmentsWorkflow:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/util/Res;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Ldagger/Lazy;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->tenderPaymentResultHandler:Ldagger/Lazy;

    return-object p0
.end method


# virtual methods
.method public getTenderOption()Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;
    .locals 5

    .line 88
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;

    .line 89
    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_ROW_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->titleStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->enabledStrategy:Lkotlin/jvm/functions/Function1;

    iget-object v4, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->workflow:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 88
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutflow/selecttender/tenderoption/UpdatedTenderOption;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/Workflow;)V

    return-object v0
.end method

.method public getTenderOptionKey()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;
    .locals 3

    .line 92
    new-instance v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "javaClass.name"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOption$TenderOptionKey;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
