.class public final Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;
.super Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.source "InstallmentsScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstallmentsQRCodeScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;,
        Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u00112\u00020\u0001:\u0002\u0011\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;",
        "qrState",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;",
        "(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V",
        "getQrState",
        "()Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "QrState",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;

    .line 51
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$Companion;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V
    .locals 1

    const-string v0, "qrState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;ILjava/lang/Object;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->copy(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;
    .locals 1

    const-string v0, "qrState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    iget-object p1, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getQrState()Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InstallmentsQRCodeScreenData(qrState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;->qrState:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
