.class public abstract Lcom/squareup/checkoutflow/installments/InstallmentsState;
.super Ljava/lang/Object;
.source "InstallmentsState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsLinkOptions;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoading;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsSent;,
        Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \t2\u00020\u0001:\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\u0008R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0007\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "",
        "()V",
        "token",
        "",
        "getToken",
        "()Ljava/lang/String;",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Companion",
        "ShowingInstallmentsLinkOptions",
        "ShowingInstallmentsOptions",
        "ShowingQRCodeError",
        "ShowingQRCodeLoaded",
        "ShowingQRCodeLoading",
        "ShowingSmsInput",
        "ShowingSmsSent",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsLinkOptions;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoading;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsSent;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsState;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/checkoutflow/installments/InstallmentsState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getToken()Ljava/lang/String;
.end method

.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 45
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/installments/InstallmentsState$toSnapshot$1;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
