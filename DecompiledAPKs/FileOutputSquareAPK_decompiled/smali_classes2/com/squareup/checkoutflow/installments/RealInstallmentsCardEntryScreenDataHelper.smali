.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;
.super Ljava/lang/Object;
.source "RealInstallmentsCardEntryScreenDataHelper.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0006H\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "formatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V",
        "getScreenData",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;",
        "amount",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;->formatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method public getScreenData(Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;
    .locals 7

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v1, Lcom/squareup/cnp/LinkSpanData;

    .line 21
    new-instance v2, Lcom/squareup/cnp/LinkSpanData$PatternData;

    .line 22
    sget v3, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_manual_card_entry_hint:I

    .line 24
    sget v4, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_manual_card_entry_url:I

    .line 25
    sget v5, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_manual_card_entry_square_support:I

    const-string v6, "help_center"

    .line 21
    invoke-direct {v2, v3, v6, v4, v5}, Lcom/squareup/cnp/LinkSpanData$PatternData;-><init>(ILjava/lang/String;II)V

    const/4 v3, 0x0

    .line 20
    invoke-direct {v1, v2, v3}, Lcom/squareup/cnp/LinkSpanData;-><init>(Lcom/squareup/cnp/LinkSpanData$PatternData;Lcom/squareup/cnp/LinkSpanData$PutData;)V

    .line 29
    new-instance v2, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    .line 30
    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_options_amount:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 31
    iget-object v4, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {v4, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 30
    invoke-virtual {v3, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x1

    .line 29
    invoke-direct {v2, p1, v1, v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;-><init>(Ljava/lang/String;Lcom/squareup/cnp/LinkSpanData;Z)V

    return-object v2
.end method
