.class public interface abstract Lcom/squareup/checkoutflow/installments/InstallmentsService;
.super Ljava/lang/Object;
.source "InstallmentsService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\tH\'\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
        "",
        "generateQr",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;",
        "request",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;",
        "sendSms",
        "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSResponse;",
        "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract generateQr(Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/capital/consumer/api/v1/installments/get-qr-code"
    .end annotation
.end method

.method public abstract sendSms(Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/capital/consumer/api/v1/installments/send-sms"
    .end annotation
.end method
