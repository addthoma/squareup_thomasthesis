.class public final Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;
.super Ljava/lang/Object;
.source "OrderOrBillPaymentProcessingStarter.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
        "paymentProcessDecider",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;",
        "ordersPaymentStarter",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;",
        "billPaymentProcessingStarter",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;",
        "orderPaymentTippingFactory",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;",
        "(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;)V",
        "startPayment",
        "",
        "paymentProcessingInput",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billPaymentProcessingStarter:Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;

.field private final orderPaymentTippingFactory:Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;

.field private final ordersPaymentStarter:Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;

.field private final paymentProcessDecider:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "paymentProcessDecider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersPaymentStarter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billPaymentProcessingStarter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPaymentTippingFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->paymentProcessDecider:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;

    iput-object p2, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->ordersPaymentStarter:Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;

    iput-object p3, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->billPaymentProcessingStarter:Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;

    iput-object p4, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->orderPaymentTippingFactory:Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;

    return-void
.end method


# virtual methods
.method public startPayment(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;)V
    .locals 5

    const-string v0, "paymentProcessingInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->paymentProcessDecider:Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessDecider;->shouldProcessPaymentUsingPaymentV2()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 24
    instance-of v0, p1, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType$ManualCardEntry;

    move-object v1, p1

    check-cast v1, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType$ManualCardEntry;-><init>(Lcom/squareup/Card;)V

    .line 27
    iget-object v1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->ordersPaymentStarter:Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;

    .line 28
    new-instance v2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;

    .line 29
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 30
    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType;

    .line 31
    iget-object v4, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->orderPaymentTippingFactory:Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;

    invoke-virtual {v4, p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderPaymentTippingFactory;->calculateTipping(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;)Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;

    move-result-object p1

    .line 28
    invoke-direct {v2, v3, v0, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType;Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;)V

    .line 27
    invoke-interface {v1, v2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersPaymentStarter;->start(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;)V

    goto :goto_0

    .line 24
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/OrderOrBillPaymentProcessingStarter;->billPaymentProcessingStarter:Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->startPayment(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;)V

    :goto_0
    return-void
.end method
