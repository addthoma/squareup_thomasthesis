.class public final Lcom/squareup/checkoutflow/orderbillpaymentfork/RealPaymentProcessingCompleter;
.super Ljava/lang/Object;
.source "RealPaymentProcessingCompleter.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/RealPaymentProcessingCompleter;",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V",
        "completePayment",
        "",
        "successful",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLocaleOverride"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/RealPaymentProcessingCompleter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/RealPaymentProcessingCompleter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    return-void
.end method


# virtual methods
.method public completePayment(Z)V
    .locals 0

    if-eqz p1, :cond_0

    .line 16
    iget-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/RealPaymentProcessingCompleter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 20
    :cond_0
    iget-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/RealPaymentProcessingCompleter;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {p1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->reset()V

    return-void
.end method
