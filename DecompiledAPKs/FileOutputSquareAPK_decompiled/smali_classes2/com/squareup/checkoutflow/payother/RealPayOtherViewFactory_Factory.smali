.class public final Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealPayOtherViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;",
            ">;)",
            "Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;)Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;-><init>(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;

    invoke-static {v0}, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;->newInstance(Lcom/squareup/checkoutflow/payother/PayOtherLayoutRunner$Factory;)Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory_Factory;->get()Lcom/squareup/checkoutflow/payother/RealPayOtherViewFactory;

    move-result-object v0

    return-object v0
.end method
