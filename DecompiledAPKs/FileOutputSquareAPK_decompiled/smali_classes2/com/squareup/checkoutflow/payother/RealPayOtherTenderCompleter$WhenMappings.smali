.class public final synthetic Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->values()[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherTenderCompleter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    return-void
.end method
