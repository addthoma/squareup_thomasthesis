.class public final Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealPayOtherWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/checkoutflow/payother/PayOtherInput;",
        "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealPayOtherWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealPayOtherWorkflow.kt\ncom/squareup/checkoutflow/payother/RealPayOtherWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,59:1\n149#2,5:60\n*E\n*S KotlinDebug\n*F\n+ 1 RealPayOtherWorkflow.kt\ncom/squareup/checkoutflow/payother/RealPayOtherWorkflow\n*L\n36#1,5:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0003J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0003JF\u0010\u0010\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0011\u001a\u00020\u00032\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00040\u0013H\u0016\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;",
        "Lcom/squareup/checkoutflow/payother/PayOtherWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/checkoutflow/payother/PayOtherInput;",
        "Lcom/squareup/checkoutflow/payother/PayOtherOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "disclaimerTextForPayOtherTenderScreen",
        "",
        "otherTenderType",
        "Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;",
        "helperIdForPayOtherTenderScreen",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method

.method private final disclaimerTextForPayOtherTenderScreen(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)I
    .locals 1

    .line 42
    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 46
    sget p1, Lcom/squareup/checkoutflow/payother/impl/R$string;->record_other_tender_disclaimer:I

    goto :goto_0

    .line 45
    :cond_0
    sget p1, Lcom/squareup/payment/R$string;->third_party_card_disclaimer:I

    :goto_0
    return p1
.end method

.method private final helperIdForPayOtherTenderScreen(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)I
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    .line 55
    :cond_0
    sget p1, Lcom/squareup/payment/R$string;->any_amount_over:I

    goto :goto_0

    .line 53
    :cond_1
    sget p1, Lcom/squareup/checkoutflow/payother/impl/R$string;->record_other_gift_card:I

    goto :goto_0

    .line 52
    :cond_2
    sget p1, Lcom/squareup/checkoutflow/payother/impl/R$string;->record_miscellaneous_forms_of_payment:I

    :goto_0
    return p1
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/checkoutflow/payother/PayOtherInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;->render(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/payother/PayOtherInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/payother/PayOtherInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/checkoutflow/payother/PayOtherScreen;

    .line 27
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 28
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherInput;->getTitle()Ljava/lang/String;

    move-result-object v3

    .line 29
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherInput;->getType()Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;->disclaimerTextForPayOtherTenderScreen(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)I

    move-result v4

    .line 30
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/payother/PayOtherInput;->getType()Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;->helperIdForPayOtherTenderScreen(Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;)I

    move-result v5

    .line 31
    new-instance p1, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow$render$screen$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow$render$screen$1;-><init>(Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 32
    new-instance p1, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow$render$screen$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow$render$screen$2;-><init>(Lcom/squareup/checkoutflow/payother/RealPayOtherWorkflow;Lcom/squareup/workflow/RenderContext;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    move-object v1, v0

    .line 26
    invoke-direct/range {v1 .. v7}, Lcom/squareup/checkoutflow/payother/PayOtherScreen;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;IILkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 36
    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 61
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 62
    const-class p2, Lcom/squareup/checkoutflow/payother/PayOtherScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 63
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 61
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 37
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
