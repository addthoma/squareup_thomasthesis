.class public final Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/manualcardentry/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ce_card_number_hint_with_dip:I = 0x7f1203e4

.field public static final ce_card_number_hint_with_dip_and_tap:I = 0x7f1203e5

.field public static final ce_card_number_hint_with_only_dip_and_tap:I = 0x7f1203e6

.field public static final ce_card_number_hint_with_only_swipe_and_dip:I = 0x7f1203e7

.field public static final gift_card_on_file:I = 0x7f120b20

.field public static final invalid_16_digit_pan:I = 0x7f120c4f

.field public static final pay_card_charge_button:I = 0x7f12139c

.field public static final pay_gift_card_cnp_hint2:I = 0x7f1213a7

.field public static final pay_gift_card_hint:I = 0x7f1213a8

.field public static final pay_gift_card_hint_no_swipe:I = 0x7f1213a9

.field public static final pay_gift_card_title:I = 0x7f1213ab

.field public static final payment_type_below_minimum_gift_card:I = 0x7f121401

.field public static final swipe_only_hint:I = 0x7f1218f5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
