.class public final Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;
.super Ljava/lang/Object;
.source "PosBuildModule_RegisterVersionCodeFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;->posBuildProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;)",
            "Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;

    invoke-direct {v0, p0}, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static registerVersionCode(Lcom/squareup/util/PosBuild;)I
    .locals 0

    .line 34
    invoke-static {p0}, Lcom/squareup/android/util/PosBuildModule;->registerVersionCode(Lcom/squareup/util/PosBuild;)I

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Integer;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/PosBuild;

    invoke-static {v0}, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;->registerVersionCode(Lcom/squareup/util/PosBuild;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/android/util/PosBuildModule_RegisterVersionCodeFactory;->get()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
