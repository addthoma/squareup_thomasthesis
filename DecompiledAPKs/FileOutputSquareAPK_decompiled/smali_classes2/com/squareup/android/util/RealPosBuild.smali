.class public final Lcom/squareup/android/util/RealPosBuild;
.super Ljava/lang/Object;
.source "RealPosBuild.kt"

# interfaces
.implements Lcom/squareup/util/PosBuild;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001e\u0010\u000f\u001a\n \u0011*\u0004\u0018\u00010\u00100\u0010X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0015\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u000eR\u0014\u0010\u0017\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u000eR\u0014\u0010\u0019\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u000e\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/android/util/RealPosBuild;",
        "Lcom/squareup/util/PosBuild;",
        "context",
        "Landroid/app/Application;",
        "(Landroid/app/Application;)V",
        "buildFlavor",
        "Lcom/squareup/util/BuildFlavor;",
        "getBuildFlavor",
        "()Lcom/squareup/util/BuildFlavor;",
        "getContext",
        "()Landroid/app/Application;",
        "gitSha",
        "",
        "getGitSha",
        "()Ljava/lang/String;",
        "registerVersionCode",
        "",
        "kotlin.jvm.PlatformType",
        "getRegisterVersionCode",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "registerVersionName",
        "getRegisterVersionName",
        "rikerVersion",
        "getRikerVersion",
        "sdkSuffix",
        "getSdkSuffix",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buildFlavor:Lcom/squareup/util/BuildFlavor;

.field private final context:Landroid/app/Application;

.field private final gitSha:Ljava/lang/String;

.field private final registerVersionCode:Ljava/lang/Integer;

.field private final registerVersionName:Ljava/lang/String;

.field private final rikerVersion:Ljava/lang/String;

.field private final sdkSuffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    .line 15
    iget-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    sget v0, Lcom/squareup/android/util/impl/R$string;->git_sha:I

    invoke-virtual {p1, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.string.git_sha)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->gitSha:Ljava/lang/String;

    .line 16
    iget-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    sget v0, Lcom/squareup/android/util/impl/R$string;->riker_version:I

    invoke-virtual {p1, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.string.riker_version)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->rikerVersion:Ljava/lang/String;

    .line 17
    iget-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    sget v0, Lcom/squareup/android/util/impl/R$string;->sdk_suffix:I

    invoke-virtual {p1, v0}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "context.getString(R.string.sdk_suffix)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->sdkSuffix:Ljava/lang/String;

    .line 18
    sget-object p1, Lcom/squareup/android/util/impl/BuildConfig;->REGISTER_VERSION_CODE:Ljava/lang/Integer;

    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->registerVersionCode:Ljava/lang/Integer;

    .line 19
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "5.35"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    sget v1, Lcom/squareup/android/util/impl/R$string;->version_name_suffix:I

    invoke-virtual {v0, v1}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->registerVersionName:Ljava/lang/String;

    .line 21
    iget-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    invoke-virtual {p1}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "com.squareup.invoicesapp"

    .line 26
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/util/BuildFlavor;->RELEASE:Lcom/squareup/util/BuildFlavor;

    goto :goto_1

    :sswitch_1
    const-string v0, "com.squareup"

    .line 22
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/util/BuildFlavor;->RELEASE:Lcom/squareup/util/BuildFlavor;

    goto :goto_1

    :sswitch_2
    const-string v0, "com.squareup.beta"

    .line 23
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/util/BuildFlavor;->BETA:Lcom/squareup/util/BuildFlavor;

    goto :goto_1

    :sswitch_3
    const-string v0, "com.squareup.apos"

    .line 24
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/util/BuildFlavor;->RELEASE:Lcom/squareup/util/BuildFlavor;

    goto :goto_1

    :sswitch_4
    const-string v0, "com.squareup.invoicesapp.beta"

    .line 27
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/util/BuildFlavor;->BETA:Lcom/squareup/util/BuildFlavor;

    goto :goto_1

    :sswitch_5
    const-string v0, "com.squareup.apos.beta"

    .line 25
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/util/BuildFlavor;->BETA:Lcom/squareup/util/BuildFlavor;

    goto :goto_1

    .line 28
    :cond_1
    :goto_0
    sget-object p1, Lcom/squareup/util/BuildFlavor;->DEVELOPMENT:Lcom/squareup/util/BuildFlavor;

    .line 21
    :goto_1
    iput-object p1, p0, Lcom/squareup/android/util/RealPosBuild;->buildFlavor:Lcom/squareup/util/BuildFlavor;

    return-void

    :sswitch_data_0
    .sparse-switch
        -0x4fffd7be -> :sswitch_5
        -0x3ff2b0f4 -> :sswitch_4
        -0xfcbffa4 -> :sswitch_3
        -0xfcbb407 -> :sswitch_2
        0x1682645 -> :sswitch_1
        0x10484352 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getBuildFlavor()Lcom/squareup/util/BuildFlavor;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->buildFlavor:Lcom/squareup/util/BuildFlavor;

    return-object v0
.end method

.method public final getContext()Landroid/app/Application;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->context:Landroid/app/Application;

    return-object v0
.end method

.method public getGitSha()Ljava/lang/String;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->gitSha:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getRegisterVersionCode()I
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/android/util/RealPosBuild;->getRegisterVersionCode()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getRegisterVersionCode()Ljava/lang/Integer;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->registerVersionCode:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRegisterVersionName()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->registerVersionName:Ljava/lang/String;

    return-object v0
.end method

.method public getRikerVersion()Ljava/lang/String;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->rikerVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getSdkSuffix()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/android/util/RealPosBuild;->sdkSuffix:Ljava/lang/String;

    return-object v0
.end method
