.class final Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;
.super Lkotlin/jvm/internal/Lambda;
.source "XmlResourceParsing.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/android/xml/TagVisitor;->visitStyledAttributes(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/res/TypedArray;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Landroid/content/res/TypedArray;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $block:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/android/xml/TagVisitor;


# direct methods
.method constructor <init>(Lcom/squareup/android/xml/TagVisitor;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    iput-object p2, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->$block:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Landroid/content/res/TypedArray;

    invoke-virtual {p0, p1}, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->invoke(Landroid/content/res/TypedArray;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/content/res/TypedArray;)V
    .locals 3

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-static {v0}, Lcom/squareup/android/xml/TagVisitor;->access$getAttributeVisitor$p(Lcom/squareup/android/xml/TagVisitor;)Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->setMaybeStyled(Landroid/content/res/TypedArray;)V

    .line 153
    iget-object p1, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-virtual {p1}, Lcom/squareup/android/xml/TagVisitor;->getParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object p1

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-static {v1}, Lcom/squareup/android/xml/TagVisitor;->access$getAttributeVisitor$p(Lcom/squareup/android/xml/TagVisitor;)Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->setIndex(I)V

    .line 155
    iget-object v1, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->$block:Lkotlin/jvm/functions/Function1;

    iget-object v2, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-static {v2}, Lcom/squareup/android/xml/TagVisitor;->access$getAttributeVisitor$p(Lcom/squareup/android/xml/TagVisitor;)Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    move-result-object v2

    invoke-interface {v1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    iget-object p1, p0, Lcom/squareup/android/xml/TagVisitor$visitStyledAttributes$1;->this$0:Lcom/squareup/android/xml/TagVisitor;

    invoke-static {p1}, Lcom/squareup/android/xml/TagVisitor;->access$getAttributeVisitor$p(Lcom/squareup/android/xml/TagVisitor;)Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;

    move-result-object p1

    const/4 v0, 0x0

    check-cast v0, Landroid/content/res/TypedArray;

    invoke-virtual {p1, v0}, Lcom/squareup/android/xml/TagVisitor$attributeVisitor$1;->setMaybeStyled(Landroid/content/res/TypedArray;)V

    return-void
.end method
