.class public interface abstract Lcom/squareup/android/xml/AttributesVisitor;
.super Ljava/lang/Object;
.source "XmlResourceParsing.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0007\u0008f\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0005R\u0012\u0010\u000c\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\t\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/android/xml/AttributesVisitor;",
        "",
        "index",
        "",
        "getIndex",
        "()I",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "nameResource",
        "getNameResource",
        "value",
        "getValue",
        "android-xml_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getIndex()I
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNameResource()I
.end method

.method public abstract getValue()Ljava/lang/String;
.end method
