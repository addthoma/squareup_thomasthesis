.class public final Lleakcanary/KeyedWeakReference;
.super Ljava/lang/ref/WeakReference;
.source "KeyedWeakReference.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lleakcanary/KeyedWeakReference$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ref/WeakReference<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0018\u0000 \u00152\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B3\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00020\n\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u001a\u0010\u000f\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0011\u00a8\u0006\u0016"
    }
    d2 = {
        "Lleakcanary/KeyedWeakReference;",
        "Ljava/lang/ref/WeakReference;",
        "",
        "referent",
        "key",
        "",
        "description",
        "watchUptimeMillis",
        "",
        "referenceQueue",
        "Ljava/lang/ref/ReferenceQueue;",
        "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;JLjava/lang/ref/ReferenceQueue;)V",
        "getDescription",
        "()Ljava/lang/String;",
        "getKey",
        "retainedUptimeMillis",
        "getRetainedUptimeMillis",
        "()J",
        "setRetainedUptimeMillis",
        "(J)V",
        "getWatchUptimeMillis",
        "Companion",
        "leakcanary-object-watcher"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lleakcanary/KeyedWeakReference$Companion;

.field private static volatile heapDumpUptimeMillis:J


# instance fields
.field private final description:Ljava/lang/String;

.field private final key:Ljava/lang/String;

.field private volatile retainedUptimeMillis:J

.field private final watchUptimeMillis:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lleakcanary/KeyedWeakReference$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lleakcanary/KeyedWeakReference$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lleakcanary/KeyedWeakReference;->Companion:Lleakcanary/KeyedWeakReference$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;JLjava/lang/ref/ReferenceQueue;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/ref/ReferenceQueue<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "referent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "referenceQueue"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p1, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    iput-object p2, p0, Lleakcanary/KeyedWeakReference;->key:Ljava/lang/String;

    iput-object p3, p0, Lleakcanary/KeyedWeakReference;->description:Ljava/lang/String;

    iput-wide p4, p0, Lleakcanary/KeyedWeakReference;->watchUptimeMillis:J

    const-wide/16 p1, -0x1

    .line 44
    iput-wide p1, p0, Lleakcanary/KeyedWeakReference;->retainedUptimeMillis:J

    return-void
.end method

.method public static final synthetic access$getHeapDumpUptimeMillis$cp()J
    .locals 2

    .line 30
    sget-wide v0, Lleakcanary/KeyedWeakReference;->heapDumpUptimeMillis:J

    return-wide v0
.end method

.method public static final synthetic access$setHeapDumpUptimeMillis$cp(J)V
    .locals 0

    .line 30
    sput-wide p0, Lleakcanary/KeyedWeakReference;->heapDumpUptimeMillis:J

    return-void
.end method

.method public static final getHeapDumpUptimeMillis()J
    .locals 2

    sget-object v0, Lleakcanary/KeyedWeakReference;->Companion:Lleakcanary/KeyedWeakReference$Companion;

    sget-wide v0, Lleakcanary/KeyedWeakReference;->heapDumpUptimeMillis:J

    return-wide v0
.end method

.method public static final setHeapDumpUptimeMillis(J)V
    .locals 1

    sget-object v0, Lleakcanary/KeyedWeakReference;->Companion:Lleakcanary/KeyedWeakReference$Companion;

    sput-wide p0, Lleakcanary/KeyedWeakReference;->heapDumpUptimeMillis:J

    return-void
.end method


# virtual methods
.method public final getDescription()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lleakcanary/KeyedWeakReference;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lleakcanary/KeyedWeakReference;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final getRetainedUptimeMillis()J
    .locals 2

    .line 44
    iget-wide v0, p0, Lleakcanary/KeyedWeakReference;->retainedUptimeMillis:J

    return-wide v0
.end method

.method public final getWatchUptimeMillis()J
    .locals 2

    .line 34
    iget-wide v0, p0, Lleakcanary/KeyedWeakReference;->watchUptimeMillis:J

    return-wide v0
.end method

.method public final setRetainedUptimeMillis(J)V
    .locals 0

    .line 44
    iput-wide p1, p0, Lleakcanary/KeyedWeakReference;->retainedUptimeMillis:J

    return-void
.end method
