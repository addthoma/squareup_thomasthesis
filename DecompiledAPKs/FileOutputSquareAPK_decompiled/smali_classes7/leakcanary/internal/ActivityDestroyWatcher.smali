.class public final Lleakcanary/internal/ActivityDestroyWatcher;
.super Ljava/lang/Object;
.source "ActivityDestroyWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lleakcanary/internal/ActivityDestroyWatcher$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0004*\u0001\t\u0008\u0000\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u001d\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lleakcanary/internal/ActivityDestroyWatcher;",
        "",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "configProvider",
        "Lkotlin/Function0;",
        "Lleakcanary/AppWatcher$Config;",
        "(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V",
        "lifecycleCallbacks",
        "leakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1",
        "Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;",
        "Companion",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lleakcanary/internal/ActivityDestroyWatcher$Companion;


# instance fields
.field private final configProvider:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;"
        }
    .end annotation
.end field

.field private final lifecycleCallbacks:Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;

.field private final objectWatcher:Lleakcanary/ObjectWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lleakcanary/internal/ActivityDestroyWatcher$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lleakcanary/internal/ActivityDestroyWatcher$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lleakcanary/internal/ActivityDestroyWatcher;->Companion:Lleakcanary/internal/ActivityDestroyWatcher$Companion;

    return-void
.end method

.method private constructor <init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lleakcanary/ObjectWatcher;",
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lleakcanary/internal/ActivityDestroyWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    iput-object p2, p0, Lleakcanary/internal/ActivityDestroyWatcher;->configProvider:Lkotlin/jvm/functions/Function0;

    .line 30
    new-instance p1, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;

    invoke-direct {p1, p0}, Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;-><init>(Lleakcanary/internal/ActivityDestroyWatcher;)V

    iput-object p1, p0, Lleakcanary/internal/ActivityDestroyWatcher;->lifecycleCallbacks:Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;

    return-void
.end method

.method public synthetic constructor <init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Lleakcanary/internal/ActivityDestroyWatcher;-><init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$getConfigProvider$p(Lleakcanary/internal/ActivityDestroyWatcher;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 24
    iget-object p0, p0, Lleakcanary/internal/ActivityDestroyWatcher;->configProvider:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$getLifecycleCallbacks$p(Lleakcanary/internal/ActivityDestroyWatcher;)Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;
    .locals 0

    .line 24
    iget-object p0, p0, Lleakcanary/internal/ActivityDestroyWatcher;->lifecycleCallbacks:Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;

    return-object p0
.end method

.method public static final synthetic access$getObjectWatcher$p(Lleakcanary/internal/ActivityDestroyWatcher;)Lleakcanary/ObjectWatcher;
    .locals 0

    .line 24
    iget-object p0, p0, Lleakcanary/internal/ActivityDestroyWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    return-object p0
.end method
