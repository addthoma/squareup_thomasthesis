.class public final Lsquareup/spe/R12DeviceManifest;
.super Lcom/squareup/wire/Message;
.source "R12DeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;,
        Lsquareup/spe/R12DeviceManifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/R12DeviceManifest;",
        "Lsquareup/spe/R12DeviceManifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/R12DeviceManifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_HWID:Lokio/ByteString;

.field public static final DEFAULT_MAX_COMPRESSION_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_MLB_SERIAL_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_PTS_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_SERIAL_NUMBER:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final ble_manifest:Lsquareup/spe/TICC2640Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.TICC2640Manifest#ADAPTER"
        tag = 0x6a
    .end annotation
.end field

.field public final comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.CommsProtocolVersion#ADAPTER"
        tag = 0x67
    .end annotation
.end field

.field public final crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.CRQGT4Manifest#ADAPTER"
        tag = 0x6e
    .end annotation
.end field

.field public final hwid:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x65
    .end annotation
.end field

.field public final k21_manifest:Lsquareup/spe/K21Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.K21Manifest#ADAPTER"
        tag = 0x69
    .end annotation
.end field

.field public final k400_manifest:Lsquareup/spe/K400Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.K400Manifest#ADAPTER"
        tag = 0x68
    .end annotation
.end field

.field public final max_compression_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x6b
    .end annotation
.end field

.field public final mlb_serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6d
    .end annotation
.end field

.field public final pts_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x6c
    .end annotation
.end field

.field public final serial_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x66
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;

    invoke-direct {v0}, Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;-><init>()V

    sput-object v0, Lsquareup/spe/R12DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lsquareup/spe/R12DeviceManifest;->DEFAULT_HWID:Lokio/ByteString;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lsquareup/spe/R12DeviceManifest;->DEFAULT_MAX_COMPRESSION_VERSION:Ljava/lang/Integer;

    .line 36
    sput-object v0, Lsquareup/spe/R12DeviceManifest;->DEFAULT_PTS_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K400Manifest;Lsquareup/spe/K21Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lsquareup/spe/CRQGT4Manifest;)V
    .locals 12

    .line 104
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lsquareup/spe/R12DeviceManifest;-><init>(Lokio/ByteString;Ljava/lang/String;Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K400Manifest;Lsquareup/spe/K21Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lsquareup/spe/CRQGT4Manifest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/String;Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K400Manifest;Lsquareup/spe/K21Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lsquareup/spe/CRQGT4Manifest;Lokio/ByteString;)V
    .locals 1

    .line 112
    sget-object v0, Lsquareup/spe/R12DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 113
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    .line 114
    iput-object p2, p0, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    .line 115
    iput-object p3, p0, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 116
    iput-object p4, p0, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    .line 117
    iput-object p5, p0, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    .line 118
    iput-object p6, p0, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 119
    iput-object p7, p0, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    .line 120
    iput-object p8, p0, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    .line 121
    iput-object p9, p0, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    .line 122
    iput-object p10, p0, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 145
    :cond_0
    instance-of v1, p1, Lsquareup/spe/R12DeviceManifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 146
    :cond_1
    check-cast p1, Lsquareup/spe/R12DeviceManifest;

    .line 147
    invoke-virtual {p0}, Lsquareup/spe/R12DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/R12DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    .line 152
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    iget-object p1, p1, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    .line 157
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 162
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 164
    invoke-virtual {p0}, Lsquareup/spe/R12DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/spe/CommsProtocolVersion;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lsquareup/spe/K400Manifest;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/spe/K21Manifest;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/spe/TICC2640Manifest;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lsquareup/spe/CRQGT4Manifest;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 175
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lsquareup/spe/R12DeviceManifest;->newBuilder()Lsquareup/spe/R12DeviceManifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 2

    .line 127
    new-instance v0, Lsquareup/spe/R12DeviceManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/R12DeviceManifest$Builder;-><init>()V

    .line 128
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->hwid:Lokio/ByteString;

    .line 129
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->serial_number:Ljava/lang/String;

    .line 130
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 131
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest:Lsquareup/spe/K400Manifest;

    .line 132
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest:Lsquareup/spe/K21Manifest;

    .line 133
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 134
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    .line 135
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->pts_version:Ljava/lang/Integer;

    .line 136
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->mlb_serial_number:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    iput-object v1, v0, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    .line 138
    invoke-virtual {p0}, Lsquareup/spe/R12DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/R12DeviceManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", hwid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 184
    :cond_0
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    :cond_1
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v1, :cond_2

    const-string v1, ", comms_protocol_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_2
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    if-eqz v1, :cond_3

    const-string v1, ", k400_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_3
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    if-eqz v1, :cond_4

    const-string v1, ", k21_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_4
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    if-eqz v1, :cond_5

    const-string v1, ", ble_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_5
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", max_compression_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_6
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", pts_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_7
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", mlb_serial_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_8
    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    if-eqz v1, :cond_9

    const-string v1, ", crq_gt4_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "R12DeviceManifest{"

    .line 193
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
