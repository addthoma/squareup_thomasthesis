.class public final Lsquareup/spe/AssetManifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AssetManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/AssetManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/AssetManifest;",
        "Lsquareup/spe/AssetManifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public git_hash:Lokio/ByteString;

.field public image_hash:Lokio/ByteString;

.field public percent_complete:Ljava/lang/Integer;

.field public product_id:Ljava/lang/String;

.field public valid:Ljava/lang/Boolean;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 167
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 154
    invoke-virtual {p0}, Lsquareup/spe/AssetManifest$Builder;->build()Lsquareup/spe/AssetManifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/AssetManifest;
    .locals 9

    .line 208
    new-instance v8, Lsquareup/spe/AssetManifest;

    iget-object v1, p0, Lsquareup/spe/AssetManifest$Builder;->version:Ljava/lang/Integer;

    iget-object v2, p0, Lsquareup/spe/AssetManifest$Builder;->valid:Ljava/lang/Boolean;

    iget-object v3, p0, Lsquareup/spe/AssetManifest$Builder;->product_id:Ljava/lang/String;

    iget-object v4, p0, Lsquareup/spe/AssetManifest$Builder;->percent_complete:Ljava/lang/Integer;

    iget-object v5, p0, Lsquareup/spe/AssetManifest$Builder;->image_hash:Lokio/ByteString;

    iget-object v6, p0, Lsquareup/spe/AssetManifest$Builder;->git_hash:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lsquareup/spe/AssetManifest;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;Lokio/ByteString;Lokio/ByteString;)V

    return-object v8
.end method

.method public git_hash(Lokio/ByteString;)Lsquareup/spe/AssetManifest$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lsquareup/spe/AssetManifest$Builder;->git_hash:Lokio/ByteString;

    return-object p0
.end method

.method public image_hash(Lokio/ByteString;)Lsquareup/spe/AssetManifest$Builder;
    .locals 0

    .line 194
    iput-object p1, p0, Lsquareup/spe/AssetManifest$Builder;->image_hash:Lokio/ByteString;

    return-object p0
.end method

.method public percent_complete(Ljava/lang/Integer;)Lsquareup/spe/AssetManifest$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lsquareup/spe/AssetManifest$Builder;->percent_complete:Ljava/lang/Integer;

    return-object p0
.end method

.method public product_id(Ljava/lang/String;)Lsquareup/spe/AssetManifest$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lsquareup/spe/AssetManifest$Builder;->product_id:Ljava/lang/String;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lsquareup/spe/AssetManifest$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lsquareup/spe/AssetManifest$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lsquareup/spe/AssetManifest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lsquareup/spe/AssetManifest$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
