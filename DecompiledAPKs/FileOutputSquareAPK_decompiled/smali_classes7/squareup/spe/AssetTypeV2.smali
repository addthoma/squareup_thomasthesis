.class public final enum Lsquareup/spe/AssetTypeV2;
.super Ljava/lang/Enum;
.source "AssetTypeV2.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/AssetTypeV2$ProtoAdapter_AssetTypeV2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/spe/AssetTypeV2;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/spe/AssetTypeV2;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/AssetTypeV2;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ASSET_TYPE_UNKNOWN:Lsquareup/spe/AssetTypeV2;

.field public static final enum BOOTLOADER_CPU0:Lsquareup/spe/AssetTypeV2;

.field public static final enum BOOTLOADER_CPU1:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_B:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_BLE:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CPU0:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CPU0_A:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CPU0_B:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CPU1:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CPU1_A:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CPU1_B:Lsquareup/spe/AssetTypeV2;

.field public static final enum FIRMWARE_CRQ_GT4:Lsquareup/spe/AssetTypeV2;

.field public static final enum FPGA:Lsquareup/spe/AssetTypeV2;

.field public static final enum RAM_PROGRAM_FACTORY:Lsquareup/spe/AssetTypeV2;

.field public static final enum RAM_PROGRAM_UNLOCK:Lsquareup/spe/AssetTypeV2;

.field public static final enum TMS_CAPKS:Lsquareup/spe/AssetTypeV2;

.field public static final enum TMS_CAPK_A:Lsquareup/spe/AssetTypeV2;

.field public static final enum TMS_CAPK_B:Lsquareup/spe/AssetTypeV2;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 14
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v1, 0x1

    const-string v2, "FIRMWARE_A"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    .line 16
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x2

    const-string v3, "FIRMWARE_B"

    invoke-direct {v0, v3, v1, v2}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_B:Lsquareup/spe/AssetTypeV2;

    .line 18
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v3, 0x3

    const-string v4, "FIRMWARE_CPU0"

    invoke-direct {v0, v4, v2, v3}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0:Lsquareup/spe/AssetTypeV2;

    .line 20
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v4, 0x4

    const-string v5, "FIRMWARE_CPU1"

    invoke-direct {v0, v5, v3, v4}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1:Lsquareup/spe/AssetTypeV2;

    .line 22
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v5, 0x5

    const-string v6, "TMS_CAPKS"

    invoke-direct {v0, v6, v4, v5}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->TMS_CAPKS:Lsquareup/spe/AssetTypeV2;

    .line 24
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v6, 0x6

    const-string v7, "FPGA"

    invoke-direct {v0, v7, v5, v6}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FPGA:Lsquareup/spe/AssetTypeV2;

    .line 30
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/4 v7, 0x7

    const-string v8, "BOOTLOADER_CPU0"

    invoke-direct {v0, v8, v6, v7}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->BOOTLOADER_CPU0:Lsquareup/spe/AssetTypeV2;

    .line 32
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v8, 0x8

    const-string v9, "BOOTLOADER_CPU1"

    invoke-direct {v0, v9, v7, v8}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->BOOTLOADER_CPU1:Lsquareup/spe/AssetTypeV2;

    .line 37
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v9, 0x9

    const-string v10, "FIRMWARE_BLE"

    invoke-direct {v0, v10, v8, v9}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_BLE:Lsquareup/spe/AssetTypeV2;

    .line 42
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v10, 0xa

    const-string v11, "FIRMWARE_CPU0_A"

    invoke-direct {v0, v11, v9, v10}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0_A:Lsquareup/spe/AssetTypeV2;

    .line 44
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v11, 0xb

    const-string v12, "FIRMWARE_CPU0_B"

    invoke-direct {v0, v12, v10, v11}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0_B:Lsquareup/spe/AssetTypeV2;

    .line 46
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v12, 0xc

    const-string v13, "FIRMWARE_CPU1_A"

    invoke-direct {v0, v13, v11, v12}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1_A:Lsquareup/spe/AssetTypeV2;

    .line 48
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v13, 0xd

    const-string v14, "FIRMWARE_CPU1_B"

    invoke-direct {v0, v14, v12, v13}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1_B:Lsquareup/spe/AssetTypeV2;

    .line 53
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v14, 0xe

    const-string v15, "RAM_PROGRAM_UNLOCK"

    invoke-direct {v0, v15, v13, v14}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->RAM_PROGRAM_UNLOCK:Lsquareup/spe/AssetTypeV2;

    .line 58
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const/16 v15, 0xf

    const-string v13, "RAM_PROGRAM_FACTORY"

    invoke-direct {v0, v13, v14, v15}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->RAM_PROGRAM_FACTORY:Lsquareup/spe/AssetTypeV2;

    .line 60
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const-string v13, "TMS_CAPK_A"

    const/16 v14, 0x10

    invoke-direct {v0, v13, v15, v14}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->TMS_CAPK_A:Lsquareup/spe/AssetTypeV2;

    .line 62
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const-string v13, "TMS_CAPK_B"

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->TMS_CAPK_B:Lsquareup/spe/AssetTypeV2;

    .line 67
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const-string v13, "FIRMWARE_CRQ_GT4"

    const/16 v14, 0x11

    const/16 v15, 0x12

    invoke-direct {v0, v13, v14, v15}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CRQ_GT4:Lsquareup/spe/AssetTypeV2;

    .line 69
    new-instance v0, Lsquareup/spe/AssetTypeV2;

    const-string v13, "ASSET_TYPE_UNKNOWN"

    const/16 v14, 0x12

    const/16 v15, 0xff

    invoke-direct {v0, v13, v14, v15}, Lsquareup/spe/AssetTypeV2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->ASSET_TYPE_UNKNOWN:Lsquareup/spe/AssetTypeV2;

    const/16 v0, 0x13

    new-array v0, v0, [Lsquareup/spe/AssetTypeV2;

    .line 13
    sget-object v13, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    const/4 v14, 0x0

    aput-object v13, v0, v14

    sget-object v13, Lsquareup/spe/AssetTypeV2;->FIRMWARE_B:Lsquareup/spe/AssetTypeV2;

    aput-object v13, v0, v1

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v3

    sget-object v1, Lsquareup/spe/AssetTypeV2;->TMS_CAPKS:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v4

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FPGA:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v5

    sget-object v1, Lsquareup/spe/AssetTypeV2;->BOOTLOADER_CPU0:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v6

    sget-object v1, Lsquareup/spe/AssetTypeV2;->BOOTLOADER_CPU1:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v7

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_BLE:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v8

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0_A:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v9

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0_B:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v10

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1_A:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v11

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1_B:Lsquareup/spe/AssetTypeV2;

    aput-object v1, v0, v12

    sget-object v1, Lsquareup/spe/AssetTypeV2;->RAM_PROGRAM_UNLOCK:Lsquareup/spe/AssetTypeV2;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/AssetTypeV2;->RAM_PROGRAM_FACTORY:Lsquareup/spe/AssetTypeV2;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/AssetTypeV2;->TMS_CAPK_A:Lsquareup/spe/AssetTypeV2;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/AssetTypeV2;->TMS_CAPK_B:Lsquareup/spe/AssetTypeV2;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CRQ_GT4:Lsquareup/spe/AssetTypeV2;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/spe/AssetTypeV2;->ASSET_TYPE_UNKNOWN:Lsquareup/spe/AssetTypeV2;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lsquareup/spe/AssetTypeV2;->$VALUES:[Lsquareup/spe/AssetTypeV2;

    .line 71
    new-instance v0, Lsquareup/spe/AssetTypeV2$ProtoAdapter_AssetTypeV2;

    invoke-direct {v0}, Lsquareup/spe/AssetTypeV2$ProtoAdapter_AssetTypeV2;-><init>()V

    sput-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 75
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 76
    iput p3, p0, Lsquareup/spe/AssetTypeV2;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/spe/AssetTypeV2;
    .locals 1

    const/16 v0, 0xff

    if-eq p0, v0, :cond_0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 101
    :pswitch_0
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CRQ_GT4:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 100
    :pswitch_1
    sget-object p0, Lsquareup/spe/AssetTypeV2;->TMS_CAPK_B:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 99
    :pswitch_2
    sget-object p0, Lsquareup/spe/AssetTypeV2;->TMS_CAPK_A:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 98
    :pswitch_3
    sget-object p0, Lsquareup/spe/AssetTypeV2;->RAM_PROGRAM_FACTORY:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 97
    :pswitch_4
    sget-object p0, Lsquareup/spe/AssetTypeV2;->RAM_PROGRAM_UNLOCK:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 96
    :pswitch_5
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1_B:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 95
    :pswitch_6
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1_A:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 94
    :pswitch_7
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0_B:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 93
    :pswitch_8
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0_A:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 92
    :pswitch_9
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_BLE:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 91
    :pswitch_a
    sget-object p0, Lsquareup/spe/AssetTypeV2;->BOOTLOADER_CPU1:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 90
    :pswitch_b
    sget-object p0, Lsquareup/spe/AssetTypeV2;->BOOTLOADER_CPU0:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 89
    :pswitch_c
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FPGA:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 88
    :pswitch_d
    sget-object p0, Lsquareup/spe/AssetTypeV2;->TMS_CAPKS:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 87
    :pswitch_e
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU1:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 86
    :pswitch_f
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_CPU0:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 85
    :pswitch_10
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_B:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 84
    :pswitch_11
    sget-object p0, Lsquareup/spe/AssetTypeV2;->FIRMWARE_A:Lsquareup/spe/AssetTypeV2;

    return-object p0

    .line 102
    :cond_0
    sget-object p0, Lsquareup/spe/AssetTypeV2;->ASSET_TYPE_UNKNOWN:Lsquareup/spe/AssetTypeV2;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/spe/AssetTypeV2;
    .locals 1

    .line 13
    const-class v0, Lsquareup/spe/AssetTypeV2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/spe/AssetTypeV2;

    return-object p0
.end method

.method public static values()[Lsquareup/spe/AssetTypeV2;
    .locals 1

    .line 13
    sget-object v0, Lsquareup/spe/AssetTypeV2;->$VALUES:[Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0}, [Lsquareup/spe/AssetTypeV2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/spe/AssetTypeV2;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 109
    iget v0, p0, Lsquareup/spe/AssetTypeV2;->value:I

    return v0
.end method
