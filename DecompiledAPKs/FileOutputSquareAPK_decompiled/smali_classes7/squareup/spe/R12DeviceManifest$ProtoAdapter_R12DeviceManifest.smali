.class final Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "R12DeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/R12DeviceManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_R12DeviceManifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/R12DeviceManifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 278
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/R12DeviceManifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 276
    invoke-virtual {p0, p1}, Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/R12DeviceManifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/R12DeviceManifest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 313
    new-instance v0, Lsquareup/spe/R12DeviceManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/R12DeviceManifest$Builder;-><init>()V

    .line 314
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 315
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 328
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 326
    :pswitch_0
    sget-object v3, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/CRQGT4Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest(Lsquareup/spe/CRQGT4Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 325
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->mlb_serial_number(Ljava/lang/String;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 324
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->pts_version(Ljava/lang/Integer;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 323
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->max_compression_version(Ljava/lang/Integer;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 322
    :pswitch_4
    sget-object v3, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/TICC2640Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest(Lsquareup/spe/TICC2640Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 321
    :pswitch_5
    sget-object v3, Lsquareup/spe/K21Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/K21Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest(Lsquareup/spe/K21Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 320
    :pswitch_6
    sget-object v3, Lsquareup/spe/K400Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/K400Manifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest(Lsquareup/spe/K400Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 319
    :pswitch_7
    sget-object v3, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version(Lsquareup/spe/CommsProtocolVersion;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 318
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->serial_number(Ljava/lang/String;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto :goto_0

    .line 317
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/R12DeviceManifest$Builder;->hwid(Lokio/ByteString;)Lsquareup/spe/R12DeviceManifest$Builder;

    goto/16 :goto_0

    .line 332
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/R12DeviceManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 333
    invoke-virtual {v0}, Lsquareup/spe/R12DeviceManifest$Builder;->build()Lsquareup/spe/R12DeviceManifest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 276
    check-cast p2, Lsquareup/spe/R12DeviceManifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/R12DeviceManifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/R12DeviceManifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 298
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    const/16 v2, 0x65

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 299
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    const/16 v2, 0x66

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 300
    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/16 v2, 0x67

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 301
    sget-object v0, Lsquareup/spe/K400Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    const/16 v2, 0x68

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 302
    sget-object v0, Lsquareup/spe/K21Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    const/16 v2, 0x69

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 303
    sget-object v0, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    const/16 v2, 0x6a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    const/16 v2, 0x6b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    const/16 v2, 0x6c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 306
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    const/16 v2, 0x6d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    sget-object v0, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    const/16 v2, 0x6e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 308
    invoke-virtual {p2}, Lsquareup/spe/R12DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 276
    check-cast p1, Lsquareup/spe/R12DeviceManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;->encodedSize(Lsquareup/spe/R12DeviceManifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/R12DeviceManifest;)I
    .locals 4

    .line 283
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12DeviceManifest;->hwid:Lokio/ByteString;

    const/16 v2, 0x65

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->serial_number:Ljava/lang/String;

    const/16 v3, 0x66

    .line 284
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/16 v3, 0x67

    .line 285
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/K400Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->k400_manifest:Lsquareup/spe/K400Manifest;

    const/16 v3, 0x68

    .line 286
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/K21Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->k21_manifest:Lsquareup/spe/K21Manifest;

    const/16 v3, 0x69

    .line 287
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    const/16 v3, 0x6a

    .line 288
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    const/16 v3, 0x6b

    .line 289
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->pts_version:Ljava/lang/Integer;

    const/16 v3, 0x6c

    .line 290
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->mlb_serial_number:Ljava/lang/String;

    const/16 v3, 0x6d

    .line 291
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/R12DeviceManifest;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    const/16 v3, 0x6e

    .line 292
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    invoke-virtual {p1}, Lsquareup/spe/R12DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 276
    check-cast p1, Lsquareup/spe/R12DeviceManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/R12DeviceManifest$ProtoAdapter_R12DeviceManifest;->redact(Lsquareup/spe/R12DeviceManifest;)Lsquareup/spe/R12DeviceManifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/R12DeviceManifest;)Lsquareup/spe/R12DeviceManifest;
    .locals 2

    .line 338
    invoke-virtual {p1}, Lsquareup/spe/R12DeviceManifest;->newBuilder()Lsquareup/spe/R12DeviceManifest$Builder;

    move-result-object p1

    .line 339
    iget-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/CommsProtocolVersion;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/CommsProtocolVersion;

    iput-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 340
    :cond_0
    iget-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest:Lsquareup/spe/K400Manifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/K400Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest:Lsquareup/spe/K400Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/K400Manifest;

    iput-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest:Lsquareup/spe/K400Manifest;

    .line 341
    :cond_1
    iget-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest:Lsquareup/spe/K21Manifest;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/spe/K21Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest:Lsquareup/spe/K21Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/K21Manifest;

    iput-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest:Lsquareup/spe/K21Manifest;

    .line 342
    :cond_2
    iget-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    if-eqz v0, :cond_3

    sget-object v0, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/TICC2640Manifest;

    iput-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    .line 343
    :cond_3
    iget-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    if-eqz v0, :cond_4

    sget-object v0, Lsquareup/spe/CRQGT4Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/CRQGT4Manifest;

    iput-object v0, p1, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    .line 344
    :cond_4
    invoke-virtual {p1}, Lsquareup/spe/R12DeviceManifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 345
    invoke-virtual {p1}, Lsquareup/spe/R12DeviceManifest$Builder;->build()Lsquareup/spe/R12DeviceManifest;

    move-result-object p1

    return-object p1
.end method
