.class final Lsquareup/items/merchant/BatchResponse$ProtoAdapter_BatchResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BatchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/BatchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BatchResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/BatchResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 97
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/BatchResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/BatchResponse$ProtoAdapter_BatchResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/BatchResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    new-instance v0, Lsquareup/items/merchant/BatchResponse$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/BatchResponse$Builder;-><init>()V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 116
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 120
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 118
    :cond_0
    iget-object v3, v0, Lsquareup/items/merchant/BatchResponse$Builder;->response:Ljava/util/List;

    sget-object v4, Lsquareup/items/merchant/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/BatchResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 125
    invoke-virtual {v0}, Lsquareup/items/merchant/BatchResponse$Builder;->build()Lsquareup/items/merchant/BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    check-cast p2, Lsquareup/items/merchant/BatchResponse;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/BatchResponse$ProtoAdapter_BatchResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/BatchResponse;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/BatchResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    sget-object v0, Lsquareup/items/merchant/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/BatchResponse;->response:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 109
    invoke-virtual {p2}, Lsquareup/items/merchant/BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 95
    check-cast p1, Lsquareup/items/merchant/BatchResponse;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/BatchResponse$ProtoAdapter_BatchResponse;->encodedSize(Lsquareup/items/merchant/BatchResponse;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/BatchResponse;)I
    .locals 3

    .line 102
    sget-object v0, Lsquareup/items/merchant/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lsquareup/items/merchant/BatchResponse;->response:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 103
    invoke-virtual {p1}, Lsquareup/items/merchant/BatchResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lsquareup/items/merchant/BatchResponse;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/BatchResponse$ProtoAdapter_BatchResponse;->redact(Lsquareup/items/merchant/BatchResponse;)Lsquareup/items/merchant/BatchResponse;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/BatchResponse;)Lsquareup/items/merchant/BatchResponse;
    .locals 2

    .line 130
    invoke-virtual {p1}, Lsquareup/items/merchant/BatchResponse;->newBuilder()Lsquareup/items/merchant/BatchResponse$Builder;

    move-result-object p1

    .line 131
    iget-object v0, p1, Lsquareup/items/merchant/BatchResponse$Builder;->response:Ljava/util/List;

    sget-object v1, Lsquareup/items/merchant/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 132
    invoke-virtual {p1}, Lsquareup/items/merchant/BatchResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 133
    invoke-virtual {p1}, Lsquareup/items/merchant/BatchResponse$Builder;->build()Lsquareup/items/merchant/BatchResponse;

    move-result-object p1

    return-object p1
.end method
