.class public final Lsquareup/items/merchant/PutResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PutResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/PutResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/PutResponse;",
        "Lsquareup/items/merchant/PutResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_definition:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public catalog_object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public invalid_catalog_object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/InvalidCatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public lock_token:Ljava/lang/String;

.field public modification_timestamp:Ljava/lang/Long;

.field public status:Lsquareup/items/merchant/PutResponse$Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 172
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 173
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/PutResponse$Builder;->catalog_object:Ljava/util/List;

    .line 174
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/PutResponse$Builder;->invalid_catalog_object:Ljava/util/List;

    .line 175
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/PutResponse$Builder;->attribute_definition:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_definition(Ljava/util/List;)Lsquareup/items/merchant/PutResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;)",
            "Lsquareup/items/merchant/PutResponse$Builder;"
        }
    .end annotation

    .line 219
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 220
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse$Builder;->attribute_definition:Ljava/util/List;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 159
    invoke-virtual {p0}, Lsquareup/items/merchant/PutResponse$Builder;->build()Lsquareup/items/merchant/PutResponse;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/PutResponse;
    .locals 9

    .line 226
    new-instance v8, Lsquareup/items/merchant/PutResponse;

    iget-object v1, p0, Lsquareup/items/merchant/PutResponse$Builder;->status:Lsquareup/items/merchant/PutResponse$Status;

    iget-object v2, p0, Lsquareup/items/merchant/PutResponse$Builder;->catalog_object:Ljava/util/List;

    iget-object v3, p0, Lsquareup/items/merchant/PutResponse$Builder;->invalid_catalog_object:Ljava/util/List;

    iget-object v4, p0, Lsquareup/items/merchant/PutResponse$Builder;->modification_timestamp:Ljava/lang/Long;

    iget-object v5, p0, Lsquareup/items/merchant/PutResponse$Builder;->lock_token:Ljava/lang/String;

    iget-object v6, p0, Lsquareup/items/merchant/PutResponse$Builder;->attribute_definition:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lsquareup/items/merchant/PutResponse;-><init>(Lsquareup/items/merchant/PutResponse$Status;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v8
.end method

.method public catalog_object(Ljava/util/List;)Lsquareup/items/merchant/PutResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;)",
            "Lsquareup/items/merchant/PutResponse$Builder;"
        }
    .end annotation

    .line 184
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 185
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse$Builder;->catalog_object:Ljava/util/List;

    return-object p0
.end method

.method public invalid_catalog_object(Ljava/util/List;)Lsquareup/items/merchant/PutResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/InvalidCatalogObject;",
            ">;)",
            "Lsquareup/items/merchant/PutResponse$Builder;"
        }
    .end annotation

    .line 190
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 191
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse$Builder;->invalid_catalog_object:Ljava/util/List;

    return-object p0
.end method

.method public lock_token(Ljava/lang/String;)Lsquareup/items/merchant/PutResponse$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse$Builder;->lock_token:Ljava/lang/String;

    return-object p0
.end method

.method public modification_timestamp(Ljava/lang/Long;)Lsquareup/items/merchant/PutResponse$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse$Builder;->modification_timestamp:Ljava/lang/Long;

    return-object p0
.end method

.method public status(Lsquareup/items/merchant/PutResponse$Status;)Lsquareup/items/merchant/PutResponse$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lsquareup/items/merchant/PutResponse$Builder;->status:Lsquareup/items/merchant/PutResponse$Status;

    return-object p0
.end method
