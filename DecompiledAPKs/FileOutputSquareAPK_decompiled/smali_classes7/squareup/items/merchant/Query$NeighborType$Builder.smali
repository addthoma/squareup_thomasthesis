.class public final Lsquareup/items/merchant/Query$NeighborType$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$NeighborType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Query$NeighborType;",
        "Lsquareup/items/merchant/Query$NeighborType$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_definition_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public object_type:Lsquareup/items/merchant/CatalogObjectType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1148
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1149
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/Query$NeighborType$Builder;->attribute_definition_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_definition_tokens(Ljava/util/List;)Lsquareup/items/merchant/Query$NeighborType$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lsquareup/items/merchant/Query$NeighborType$Builder;"
        }
    .end annotation

    .line 1164
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1165
    iput-object p1, p0, Lsquareup/items/merchant/Query$NeighborType$Builder;->attribute_definition_tokens:Ljava/util/List;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1143
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$NeighborType$Builder;->build()Lsquareup/items/merchant/Query$NeighborType;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Query$NeighborType;
    .locals 4

    .line 1171
    new-instance v0, Lsquareup/items/merchant/Query$NeighborType;

    iget-object v1, p0, Lsquareup/items/merchant/Query$NeighborType$Builder;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    iget-object v2, p0, Lsquareup/items/merchant/Query$NeighborType$Builder;->attribute_definition_tokens:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lsquareup/items/merchant/Query$NeighborType;-><init>(Lsquareup/items/merchant/CatalogObjectType;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public object_type(Lsquareup/items/merchant/CatalogObjectType;)Lsquareup/items/merchant/Query$NeighborType$Builder;
    .locals 0

    .line 1156
    iput-object p1, p0, Lsquareup/items/merchant/Query$NeighborType$Builder;->object_type:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0
.end method
