.class public Lsrc/main/java/com/squareup/shared/pricing/engine/catalog/client/TaxRuleWrapper;
.super Ljava/lang/Object;
.source "TaxRuleWrapper.java"

# interfaces
.implements Lcom/squareup/shared/pricing/engine/catalog/models/TaxRuleFacade;


# instance fields
.field private rule:Lcom/squareup/shared/catalog/models/CatalogTaxRule;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogTaxRule;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lsrc/main/java/com/squareup/shared/pricing/engine/catalog/client/TaxRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogTaxRule;

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lsrc/main/java/com/squareup/shared/pricing/engine/catalog/client/TaxRuleWrapper;->rule:Lcom/squareup/shared/catalog/models/CatalogTaxRule;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTaxRule;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
