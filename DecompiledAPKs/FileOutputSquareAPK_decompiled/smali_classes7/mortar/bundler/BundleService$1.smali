.class Lmortar/bundler/BundleService$1;
.super Ljava/lang/Object;
.source "BundleService.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmortar/bundler/BundleService;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmortar/bundler/BundleService;


# direct methods
.method constructor <init>(Lmortar/bundler/BundleService;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 83
    iget-object v0, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v0, v0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v0, v0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    iget-object v1, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v1, v1, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    invoke-virtual {v1, p1}, Lmortar/bundler/BundleServiceRunner;->bundleKey(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onExitScope()V
    .locals 3

    .line 87
    iget-object v0, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v0, v0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v0, v0, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v0, v0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v0, v0, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    iget-object v1, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v1, v1, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v2, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v2, v2, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleServiceRunner;->bundleKey(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 88
    :cond_0
    iget-object v0, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v0, v0, Lmortar/bundler/BundleService;->bundlers:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/bundler/Bundler;

    invoke-interface {v1}, Lmortar/bundler/Bundler;->onExitScope()V

    goto :goto_0

    .line 89
    :cond_1
    iget-object v0, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v0, v0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v0, v0, Lmortar/bundler/BundleServiceRunner;->scopedServices:Ljava/util/Map;

    iget-object v1, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v1, v1, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v2, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v2, v2, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {v1, v2}, Lmortar/bundler/BundleServiceRunner;->bundleKey(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v0, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    iget-object v0, v0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v0, v0, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    iget-object v1, p0, Lmortar/bundler/BundleService$1;->this$0:Lmortar/bundler/BundleService;

    invoke-interface {v0, v1}, Ljava/util/NavigableSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method
