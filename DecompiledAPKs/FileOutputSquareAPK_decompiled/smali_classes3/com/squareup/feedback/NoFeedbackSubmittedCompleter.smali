.class public final Lcom/squareup/feedback/NoFeedbackSubmittedCompleter;
.super Ljava/lang/Object;
.source "FeedbackSubmittedCompleter.kt"

# interfaces
.implements Lcom/squareup/feedback/FeedbackSubmittedCompleter;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFeedbackSubmittedCompleter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FeedbackSubmittedCompleter.kt\ncom/squareup/feedback/NoFeedbackSubmittedCompleter\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,15:1\n151#2,2:16\n*E\n*S KotlinDebug\n*F\n+ 1 FeedbackSubmittedCompleter.kt\ncom/squareup/feedback/NoFeedbackSubmittedCompleter\n*L\n14#1,2:16\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004H\u0096\u0001\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/feedback/NoFeedbackSubmittedCompleter;",
        "Lcom/squareup/feedback/FeedbackSubmittedCompleter;",
        "()V",
        "exitFeedback",
        "",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/feedback/FeedbackSubmittedCompleter;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 16
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 17
    const-class v2, Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    iput-object v0, p0, Lcom/squareup/feedback/NoFeedbackSubmittedCompleter;->$$delegate_0:Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    return-void
.end method


# virtual methods
.method public exitFeedback()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/feedback/NoFeedbackSubmittedCompleter;->$$delegate_0:Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    invoke-interface {v0}, Lcom/squareup/feedback/FeedbackSubmittedCompleter;->exitFeedback()V

    return-void
.end method
