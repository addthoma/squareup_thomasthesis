.class public Lcom/squareup/merchantimages/SingleImagePicassoFactory;
.super Ljava/lang/Object;
.source "SingleImagePicassoFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/merchantimages/SingleImagePicassoFactory$OkHttp3DownloaderFactory;,
        Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;
    }
.end annotation


# static fields
.field private static final LOCAL_SETTING_KEY_PREFIX:Ljava/lang/String; = "single-image-picasso-"


# instance fields
.field private final cacheDir:Ljava/io/File;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final context:Landroid/content/Context;

.field private final downloaderFactory:Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;

.field private final gson:Lcom/google/gson/Gson;

.field private final memoryCache:Lcom/squareup/picasso/Cache;

.field private final preferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

.field private final requestTransformer:Lcom/squareup/picasso/Picasso$RequestTransformer;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/picasso/Cache;Ljava/io/File;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->context:Landroid/content/Context;

    .line 53
    iput-object p2, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->memoryCache:Lcom/squareup/picasso/Cache;

    .line 54
    new-instance p1, Ljava/io/File;

    const-string p2, "merchant-images"

    invoke-direct {p1, p3, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->cacheDir:Ljava/io/File;

    .line 55
    iput-object p4, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->preferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    .line 56
    iput-object p5, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->gson:Lcom/google/gson/Gson;

    .line 57
    iput-object p6, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 58
    iput-object p7, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->requestTransformer:Lcom/squareup/picasso/Picasso$RequestTransformer;

    .line 59
    iput-object p8, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->downloaderFactory:Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;)Lcom/squareup/merchantimages/SingleImagePicasso;
    .locals 5

    const-string v0, "cacheName"

    .line 69
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 70
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->cacheDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->downloaderFactory:Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;

    const-wide/32 v2, 0x500000

    invoke-interface {v1, v0, v2, v3}, Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;->createDownloader(Ljava/io/File;J)Lcom/squareup/picasso/Downloader;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->preferences:Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "single-image-picasso-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    sget-object v2, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;->DEFAULT:Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride;

    new-instance v3, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;

    iget-object v4, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->gson:Lcom/google/gson/Gson;

    invoke-direct {v3, v4}, Lcom/squareup/merchantimages/SingleImagePicasso$UriOverride$Converter;-><init>(Lcom/google/gson/Gson;)V

    .line 74
    invoke-virtual {v1, p1, v2, v3}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    .line 77
    new-instance v1, Lcom/squareup/picasso/Picasso$Builder;

    iget-object v2, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/picasso/Picasso$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->memoryCache:Lcom/squareup/picasso/Cache;

    .line 78
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/Picasso$Builder;->memoryCache(Lcom/squareup/picasso/Cache;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v1

    .line 79
    invoke-virtual {v1, v0}, Lcom/squareup/picasso/Picasso$Builder;->downloader(Lcom/squareup/picasso/Downloader;)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 80
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso$Builder;->indicatorsEnabled(Z)Lcom/squareup/picasso/Picasso$Builder;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/squareup/merchantimages/SingleImagePicasso;

    iget-object v2, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v3, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->requestTransformer:Lcom/squareup/picasso/Picasso$RequestTransformer;

    invoke-direct {v1, v0, p1, v2, v3}, Lcom/squareup/merchantimages/SingleImagePicasso;-><init>(Lcom/squareup/picasso/Picasso$Builder;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/picasso/Picasso$RequestTransformer;)V

    return-object v1
.end method
