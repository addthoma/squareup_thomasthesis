.class public final Lcom/squareup/merchantimages/SingleImagePicassoFactory$OkHttp3DownloaderFactory;
.super Ljava/lang/Object;
.source "SingleImagePicassoFactory.java"

# interfaces
.implements Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/merchantimages/SingleImagePicassoFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OkHttp3DownloaderFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createDownloader(Ljava/io/File;J)Lcom/squareup/picasso/Downloader;
    .locals 1

    .line 33
    new-instance v0, Lcom/jakewharton/picasso/OkHttp3Downloader;

    invoke-direct {v0, p1, p2, p3}, Lcom/jakewharton/picasso/OkHttp3Downloader;-><init>(Ljava/io/File;J)V

    return-object v0
.end method
