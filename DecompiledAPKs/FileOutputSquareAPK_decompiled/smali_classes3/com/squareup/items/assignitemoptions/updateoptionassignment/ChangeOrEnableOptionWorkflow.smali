.class public final Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "ChangeOrEnableOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChangeOrEnableOptionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChangeOrEnableOptionWorkflow.kt\ncom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,248:1\n32#2,12:249\n1360#3:261\n1429#3,2:262\n1550#3,3:264\n1431#3:267\n*E\n*S KotlinDebug\n*F\n+ 1 ChangeOrEnableOptionWorkflow.kt\ncom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow\n*L\n47#1,12:249\n52#1:261\n52#1,2:262\n52#1,3:264\n52#1:267\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001\u0018B\u0017\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00022\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016JN\u0010\u0013\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00032\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0003H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "selectOptionValuesWorkflow",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;",
        "changeOptionValuesWorkflow",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;",
        "(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final changeOptionValuesWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;

.field private final selectOptionValuesWorkflow:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "selectOptionValuesWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "changeOptionValuesWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->selectOptionValuesWorkflow:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->changeOptionValuesWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;
    .locals 11

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_4

    .line 249
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p2}, Lokio/ByteString;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    if-eqz p2, :cond_3

    .line 254
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v3, "Parcel.obtain()"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Lokio/ByteString;->toByteArray()[B

    move-result-object p2

    .line 256
    array-length v3, p2

    invoke-virtual {v0, p2, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 257
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 258
    const-class p2, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v3, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_2

    :cond_3
    move-object p2, v0

    .line 260
    :goto_2
    move-object v0, p2

    check-cast v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;

    :cond_4
    if-eqz v0, :cond_5

    return-object v0

    .line 51
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getOriginalSelectedValues()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    xor-int/2addr p2, v1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 261
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 262
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 263
    move-object v6, v4

    check-cast v6, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz p2, :cond_9

    .line 54
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getOriginalSelectedValues()Ljava/util/List;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    .line 264
    instance-of v5, v4, Ljava/util/Collection;

    if-eqz v5, :cond_7

    move-object v5, v4

    check-cast v5, Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    :cond_6
    const/4 v7, 0x0

    goto :goto_4

    .line 265
    :cond_7
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 54
    invoke-virtual {v5}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_9
    const/4 v7, 0x1

    .line 58
    :goto_4
    new-instance v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v5, v4

    invoke-direct/range {v5 .. v10}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 267
    :cond_a
    check-cast v3, Ljava/util/List;

    .line 61
    new-instance p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    invoke-direct {p1, v3}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->render(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    instance-of v0, p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    .line 73
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v3

    .line 74
    check-cast p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->getInitialValueSelections()Ljava/util/List;

    move-result-object v4

    .line 75
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isChange()Z

    move-result v5

    .line 76
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->isIncrementalUpdate()Z

    move-result v6

    move-object v1, v0

    .line 71
    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Ljava/util/List;ZZ)V

    .line 78
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->selectOptionValuesWorkflow:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;

    move-object v2, p2

    check-cast v2, Lcom/squareup/workflow/Workflow;

    const/4 v4, 0x0

    new-instance p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$render$1;

    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$render$1;-><init>(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    move-object v3, v0

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 99
    :cond_0
    instance-of v0, p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->changeOptionValuesWorkflow:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 101
    new-instance v3, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;

    .line 102
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getItemName()Ljava/lang/String;

    move-result-object v0

    .line 103
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v1

    .line 104
    check-cast p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$UpdateVariations;->getValueToExtendExistingVariations()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p2

    .line 101
    invoke-direct {v3, v0, v1, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ChangeOptionValuesProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Lcom/squareup/cogs/itemoptions/ItemOptionValue;)V

    const/4 v4, 0x0

    .line 106
    new-instance p2, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$render$2;

    invoke-direct {p2, p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow$render$2;-><init>(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionProps;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 99
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
