.class final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectVariationsToCreateWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$BackToListWithNewExtendValue;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectVariationsToCreateWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectVariationsToCreateWorkflow.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,225:1\n1370#2:226\n1401#2,4:227\n*E\n*S KotlinDebug\n*F\n+ 1 SelectVariationsToCreateWorkflow.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3\n*L\n107#1:226\n107#1,4:227\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$BackToListWithNewExtendValue;",
        "output",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

.field final synthetic $state:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$state:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$props:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$BackToListWithNewExtendValue;
    .locals 9

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;->getSelection()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v0

    .line 102
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;->getSelection()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$state:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    check-cast v2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;->getOptionValueToExtend()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$state:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState;

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateState$ExtendValueSelection;->getOptionValueCombinationSelections()Ljava/util/List;

    move-result-object p1

    goto :goto_2

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$props:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v1

    .line 106
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;->getSelection()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateCombinations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;)Ljava/util/List;

    move-result-object p1

    const-string v1, "props.assignmentEngine\n \u2026(output.selection.idPair)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 226
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 228
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    if-gez v3, :cond_1

    .line 229
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v4, Lcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;

    .line 108
    new-instance v6, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;

    .line 109
    iget-object v7, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$props:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    invoke-virtual {v7}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v7

    .line 110
    move-object v8, v4

    check-cast v8, Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;

    invoke-virtual {v7, v8}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->generateVariationNameWithOptionValueCombination(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueCombination;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "props.assignmentEngine\n \u2026eCombination(combination)"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v8, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->$props:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    invoke-virtual {v8}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;->getMaxNumberOfCombinationsToSelect()I

    move-result v8

    if-ge v3, v8, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    const-string v8, "combination"

    .line 112
    invoke-static {v4, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-direct {v6, v7, v3, v4}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/OptionValueCombinationSelection;-><init>(Ljava/lang/String;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/PrimitiveOptionValueCombination;)V

    .line 113
    invoke-interface {v1, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v3, v5

    goto :goto_0

    .line 230
    :cond_3
    move-object p1, v1

    check-cast p1, Ljava/util/List;

    .line 100
    :goto_2
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$BackToListWithNewExtendValue;

    invoke-direct {v1, v0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$BackToListWithNewExtendValue;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/util/List;)V

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$render$3;->invoke(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionOutput;)Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow$Action$BackToListWithNewExtendValue;

    move-result-object p1

    return-object p1
.end method
