.class public final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;
.super Ljava/lang/Object;
.source "UpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$Factory;,
        Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,100:1\n49#2:101\n50#2,3:107\n53#2:118\n599#3,4:102\n601#3:106\n310#4,6:110\n43#5,2:116\n*E\n*S KotlinDebug\n*F\n+ 1 UpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner\n*L\n32#1:101\n32#1,3:107\n32#1:118\n32#1,4:102\n32#1:106\n32#1,6:110\n32#1,2:116\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0015\u0016B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \n*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Factory",
        "OptionValueRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->view:Landroid/view/View;

    .line 29
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 31
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->update_existing_variations_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 32
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 102
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 107
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 108
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 33
    sget-object p2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 111
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 39
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 48
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$recycler$1$2$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$recycler$1$2$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 110
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 116
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 51
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 116
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 105
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 102
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 3

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 58
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 60
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    .line 61
    sget v2, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_variations_to_create_workflow_update_existing_variations_text:I

    .line 60
    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    .line 59
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 64
    sget-object v1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$1;

    invoke-direct {v2, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 67
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    .line 81
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$3;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$3;-><init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
