.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;
.super Ljava/lang/Object;
.source "SelectOptionsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/editoption/EditOptionEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;"
        }
    .end annotation

    .line 53
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;Lcom/squareup/items/editoption/EditOptionEventLogger;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;
    .locals 8

    .line 59
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;-><init>(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;Lcom/squareup/items/editoption/EditOptionEventLogger;Ljava/lang/String;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/items/editoption/EditOptionEventLogger;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v1 .. v6}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->newInstance(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;Lcom/squareup/items/assignitemoptions/AssignItemOptionEventLogger;Lcom/squareup/items/editoption/EditOptionEventLogger;Ljava/lang/String;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow_Factory;->get()Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;

    move-result-object v0

    return-object v0
.end method
