.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 SelectOptionsLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1\n*L\n1#1,87:1\n161#2,5:88\n164#2:93\n163#2,13:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context$inlined:Landroid/content/Context;

.field final synthetic $this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$context$inlined:Landroid/content/Context;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;

    .line 88
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$context$inlined:Landroid/content/Context;

    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$string;->combined_name_and_display_name:I

    invoke-static {v0, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 94
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "option_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 93
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "option_display_name"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 88
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoRowKt;->singleLineLabel(Lcom/squareup/noho/NohoRow;Landroid/text/TextUtils$TruncateAt;)V

    .line 101
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;->isSelected()Z

    move-result p1

    if-nez p1, :cond_1

    .line 102
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    sget-object v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$1$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 105
    :cond_1
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1;->$this_create$inlined:Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1$lambda$1;

    invoke-direct {v0, p2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$recycler$1$6$1$$special$$inlined$bind$1$lambda$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$SelectOptionsRow$AvailableOptionRow;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method
