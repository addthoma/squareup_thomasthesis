.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "SelectOptionsViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "selectOptionsLayoutRunnerFactory",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Factory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Factory;Lcom/squareup/util/Res;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Factory;Lcom/squareup/util/Res;)V
    .locals 21
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "selectOptionsLayoutRunnerFactory"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "res"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 14
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 15
    const-class v4, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsScreen;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    .line 16
    sget v5, Lcom/squareup/items/assignitemoptions/impl/R$layout;->select_options:I

    .line 17
    new-instance v6, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory$1;

    invoke-direct {v6, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsLayoutRunner$Factory;)V

    move-object v8, v6

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 18
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1df

    const/16 v20, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x0

    const/16 v9, 0x8

    move-object v6, v0

    .line 14
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 20
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 21
    const-class v3, Lcom/squareup/items/assignitemoptions/selectoptions/TooManyAssignedOptionsScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    .line 22
    new-instance v4, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory$2;

    invoke-direct {v4, v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsViewFactory$2;-><init>(Lcom/squareup/util/Res;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 20
    invoke-virtual {v0, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x1

    aput-object v0, v2, v1

    move-object/from16 v0, p0

    .line 13
    invoke-direct {v0, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
