.class public final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;
.super Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;
.source "SelectOptionsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TapOption"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionsWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,442:1\n1360#2:443\n1429#2,3:444\n1360#2:447\n1429#2,3:448\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionsWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption\n*L\n363#1:443\n363#1,3:444\n374#1:447\n374#1,3:448\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J7\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\u0018\u0010\u001f\u001a\u00020 *\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020#0!H\u0016R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;",
        "tappedOption",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "optionsInUse",
        "",
        "perItemOptionLimit",
        "",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "getOptionsInUse",
        "()Ljava/util/List;",
        "getPerItemOptionLimit",
        "()I",
        "getTappedOption",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final optionsInUse:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private final perItemOptionLimit:I

.field private final tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;I",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            ")V"
        }
    .end annotation

    const-string v0, "tappedOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionsInUse"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 361
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    iput p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    iput-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 443
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 444
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 445
    check-cast v3, Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 363
    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 446
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 363
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 364
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    if-lt v0, v1, :cond_1

    .line 365
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;

    .line 366
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    .line 367
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    .line 365
    invoke-direct {v0, v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$TooManyAssignedOptionsError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 372
    :cond_1
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getItemOptionValuesInUseByOptionIds()Ljava/util/Map;

    move-result-object v0

    .line 373
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    .line 372
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_3

    check-cast v0, Ljava/lang/Iterable;

    .line 447
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 448
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 449
    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;

    const-string v3, "it"

    .line 375
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValueKt;->toItemOptionValue(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOptionValue;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 450
    :cond_2
    check-cast v1, Ljava/util/List;

    move-object v6, v1

    goto :goto_2

    .line 376
    :cond_3
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    .line 377
    :goto_2
    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;

    iget-object v4, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v3, v0

    invoke-direct/range {v3 .. v9}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState$ChangeOrEnableOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;ZLjava/util/List;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    return v0
.end method

.method public final component4()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;I",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            ")",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;"
        }
    .end annotation

    const-string v0, "tappedOption"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionsInUse"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/util/List;ILcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    iget v1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getOptionsInUse()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOption;",
            ">;"
        }
    .end annotation

    .line 358
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    return-object v0
.end method

.method public final getPerItemOptionLimit()I
    .locals 1

    .line 359
    iget v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    return v0
.end method

.method public final getTappedOption()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 357
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TapOption(tappedOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->tappedOption:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", optionsInUse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->optionsInUse:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", perItemOptionLimit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->perItemOptionLimit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
