.class public final Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;
.super Ljava/lang/Object;
.source "FetchAllVariationsFailedDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$Companion;


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;->Companion:Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$Companion;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;Landroid/content/Context;Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;)Landroid/app/AlertDialog;
    .locals 2

    .line 31
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    sget p1, Lcom/squareup/common/strings/R$string;->retry:I

    .line 34
    new-instance v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$createDialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$createDialog$1;-><init>(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    .line 32
    invoke-virtual {v0, p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 36
    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 37
    new-instance v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$createDialog$2;

    invoke-direct {v1, p2}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$createDialog$2;-><init>(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedScreen;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    .line 35
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 38
    sget p2, Lcom/squareup/items/assignitemoptions/impl/R$string;->assign_option_to_item_fetch_all_variations_failed_dialog_title:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 39
    sget p2, Lcom/squareup/items/assignitemoptions/impl/R$string;->assign_option_to_item_fetch_all_variations_failed_dialog_message:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 40
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026(false)\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 22
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory$create$1;-><init>(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsFailedDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
