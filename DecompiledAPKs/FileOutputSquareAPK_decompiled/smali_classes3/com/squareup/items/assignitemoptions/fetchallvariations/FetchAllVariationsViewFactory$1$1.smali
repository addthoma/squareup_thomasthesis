.class public final Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1$1;
.super Ljava/lang/Object;
.source "FetchAllVariationsViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1;->invoke(Landroid/view/View;)Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0006\u001a\u00020\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1$1",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "containerHints"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchAllVariationsViewFactory$1$1;->showRendering(Lcom/squareup/items/assignitemoptions/fetchallvariations/FetchInProgressScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
