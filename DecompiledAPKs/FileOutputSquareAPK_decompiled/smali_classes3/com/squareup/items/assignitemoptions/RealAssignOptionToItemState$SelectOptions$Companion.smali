.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;
.super Ljava/lang/Object;
.source "RealAssignOptionToItemState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAssignOptionToItemState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAssignOptionToItemState.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion\n*L\n1#1,22:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;",
        "",
        "()V",
        "from",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final from(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;
    .locals 1

    const-string v0, "assignmentEngine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllVariations()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "assignmentEngine.allVariations[0]"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getAllItemOptionValues()Ljava/util/List;

    move-result-object p1

    const-string v0, "assignmentEngine.allVari\u2026ns[0].allItemOptionValues"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 16
    new-instance v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState$SelectOptions;-><init>(Z)V

    return-object v0
.end method
