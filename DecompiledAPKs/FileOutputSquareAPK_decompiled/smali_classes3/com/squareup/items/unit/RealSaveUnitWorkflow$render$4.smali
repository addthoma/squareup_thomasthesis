.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow;->render(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/items/unit/SaveUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/shared/catalog/sync/SyncResult<",
        "Ljava/lang/Integer;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "+",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "result",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/items/unit/SaveUnitState;

.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/items/unit/SaveUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    iput-object p2, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->$state:Lcom/squareup/items/unit/SaveUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/SaveUnitState;",
            "Lcom/squareup/items/unit/SaveUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-nez v0, :cond_2

    .line 159
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    goto :goto_0

    .line 160
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 161
    :cond_1
    :goto_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 162
    new-instance v3, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    const-string v4, "count"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    iget-object v4, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v4, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-virtual {v4}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;-><init>(ILcom/squareup/items/unit/SaveUnitState$Saving;)V

    .line 161
    invoke-static {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 165
    :cond_2
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 166
    new-instance v3, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    .line 167
    iget-object v4, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v4, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    .line 168
    iget-object v5, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow;

    .line 169
    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    const-string v6, "result.error"

    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v6, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-virtual {v6}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v6

    .line 168
    invoke-static {v5, p1, v6}, Lcom/squareup/items/unit/RealSaveUnitWorkflow;->access$getUnitSaveFailedAlertScreenConfiguration(Lcom/squareup/items/unit/RealSaveUnitWorkflow;Lcom/squareup/shared/catalog/sync/SyncError;Lcom/squareup/items/unit/SaveUnitState$Saving;)Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p1

    .line 166
    invoke-direct {v3, v4, p1}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V

    .line 165
    invoke-static {v0, v3, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$4;->invoke(Lcom/squareup/shared/catalog/sync/SyncResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
