.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow;->render(Lcom/squareup/items/unit/SaveUnitInput;Lcom/squareup/items/unit/SaveUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "+",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/SaveUnitState;",
        "Lcom/squareup/items/unit/SaveUnitResult;",
        "event",
        "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/items/unit/SaveUnitState;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/SaveUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/SaveUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/SaveUnitState;",
            "Lcom/squareup/items/unit/SaveUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    instance-of v0, p1, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event$Retry;

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/SaveUnitState;

    .line 145
    instance-of v0, p1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$SavingFailed;->getPreviousSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 146
    :cond_0
    instance-of p1, p1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;->$state:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 148
    :cond_2
    instance-of p1, p1, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event$Cancel;

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/items/unit/SaveUnitResult$SaveDiscarded;->INSTANCE:Lcom/squareup/items/unit/SaveUnitResult$SaveDiscarded;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 74
    check-cast p1, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$render$3;->invoke(Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
