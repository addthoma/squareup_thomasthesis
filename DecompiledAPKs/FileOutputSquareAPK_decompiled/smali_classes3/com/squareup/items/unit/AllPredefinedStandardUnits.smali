.class public final Lcom/squareup/items/unit/AllPredefinedStandardUnits;
.super Ljava/lang/Object;
.source "AllPredefinedStandardUnits.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/items/unit/AllPredefinedStandardUnits;",
        "",
        "()V",
        "units",
        "",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "getUnits",
        "()Ljava/util/List;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final units:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x26

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 48
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_ACRE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 49
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 50
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 51
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 52
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->IMPERIAL_SQUARE_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 53
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 54
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 55
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;->METRIC_SQUARE_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->area_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 57
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 58
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 59
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 60
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->IMPERIAL_MILE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 61
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_MILLIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0xc

    aput-object v1, v0, v2

    .line 62
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_CENTIMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0xd

    aput-object v1, v0, v2

    .line 63
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_METER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0xe

    aput-object v1, v0, v2

    .line 64
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;->METRIC_KILOMETER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->length_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0xf

    aput-object v1, v0, v2

    .line 66
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x10

    aput-object v1, v0, v2

    .line 67
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x11

    aput-object v1, v0, v2

    .line 68
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x12

    aput-object v1, v0, v2

    .line 69
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x13

    aput-object v1, v0, v2

    .line 70
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->time_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x14

    aput-object v1, v0, v2

    .line 72
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x15

    aput-object v1, v0, v2

    .line 73
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x16

    aput-object v1, v0, v2

    .line 74
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x17

    aput-object v1, v0, v2

    .line 75
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x18

    aput-object v1, v0, v2

    .line 76
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x19

    aput-object v1, v0, v2

    .line 77
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    .line 78
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    .line 79
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    .line 80
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    .line 81
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    .line 82
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->volume_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    .line 84
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_WEIGHT_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x20

    aput-object v1, v0, v2

    .line 85
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_POUND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x21

    aput-object v1, v0, v2

    .line 86
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->IMPERIAL_STONE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x22

    aput-object v1, v0, v2

    .line 87
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_MILLIGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x23

    aput-object v1, v0, v2

    .line 88
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_GRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x24

    aput-object v1, v0, v2

    .line 89
    new-instance v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;->METRIC_KILOGRAM:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->weight_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Builder;->build()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v1

    const/16 v2, 0x25

    aput-object v1, v0, v2

    .line 46
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/items/unit/AllPredefinedStandardUnits;->units:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getUnits()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/items/unit/AllPredefinedStandardUnits;->units:Ljava/util/List;

    return-object v0
.end method
