.class final Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/connectivity/InternetState;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/EditUnitState;",
        "+",
        "Lcom/squareup/items/unit/EditUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "internetState",
        "Lcom/squareup/connectivity/InternetState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/items/unit/EditUnitState;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/EditUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;->$state:Lcom/squareup/items/unit/EditUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/connectivity/InternetState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/InternetState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "internetState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    sget-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/connectivity/InternetState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eq p1, v0, :cond_1

    if-ne p1, v1, :cond_0

    .line 364
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    iget-object v3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v3, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    invoke-virtual {v3}, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;->getPendingState()Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;-><init>(Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 363
    :cond_1
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;->getPendingState()Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Lcom/squareup/items/unit/EditUnitState;

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.items.unit.EditUnitState"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;->invoke(Lcom/squareup/connectivity/InternetState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
