.class public final Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit$DefaultImpls;
.super Ljava/lang/Object;
.source "EditUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static isCreatingNewUnit(Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;)Z
    .locals 0

    .line 72
    invoke-interface {p0}, Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/items/unit/SaveUnitWorkflowKt;->isNew(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Z

    move-result p0

    return p0
.end method

.method public static shouldShowBackButton(Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Z
    .locals 1

    const-string/jumbo v0, "unitInEditing"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-interface {p0}, Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;->isCreatingNewUnit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    const-string/jumbo v0, "unitInEditing.build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    const-string/jumbo v0, "unitInEditing.build().measurementUnit"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-interface {p0}, Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;->isDefaultUnit()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
