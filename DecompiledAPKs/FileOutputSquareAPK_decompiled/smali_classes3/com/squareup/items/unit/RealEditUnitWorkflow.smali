.class public final Lcom/squareup/items/unit/RealEditUnitWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lcom/squareup/items/unit/EditUnitWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/unit/EditUnitInput;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/workflow/MainAndModal;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/items/unit/EditUnitWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditUnitWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditUnitWorkflow.kt\ncom/squareup/items/unit/RealEditUnitWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,531:1\n149#2,5:532\n149#2,5:537\n149#2,5:542\n149#2,5:547\n149#2,5:552\n149#2,5:557\n149#2,5:562\n149#2,5:570\n149#2,5:575\n85#3:567\n240#4:568\n276#5:569\n1360#6:580\n1429#6,3:581\n1866#6,7:584\n747#6:591\n769#6:592\n1360#6:593\n1429#6,3:594\n770#6:597\n1360#6:598\n1429#6,3:599\n704#6:602\n777#6,2:603\n1550#6,3:605\n250#6,2:608\n*E\n*S KotlinDebug\n*F\n+ 1 EditUnitWorkflow.kt\ncom/squareup/items/unit/RealEditUnitWorkflow\n*L\n166#1,5:532\n263#1,5:537\n272#1,5:542\n283#1,5:547\n295#1,5:552\n311#1,5:557\n351#1,5:562\n368#1,5:570\n377#1,5:575\n360#1:567\n360#1:568\n360#1:569\n409#1:580\n409#1,3:581\n415#1,7:584\n466#1:591\n466#1:592\n466#1:593\n466#1,3:594\n466#1:597\n470#1:598\n470#1,3:599\n478#1:602\n478#1,2:603\n502#1,3:605\n509#1,2:608\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002B7\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010 \u001a\u00020!H\u0002J$\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u001cH\u0002J(\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u001e2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020,0+2\u0008\u0010-\u001a\u0004\u0018\u00010\u001eH\u0002J\u0016\u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010/\u001a\u00020\u001eH\u0002J\u001a\u00100\u001a\u00020\u00042\u0006\u00101\u001a\u00020\u00032\u0008\u00102\u001a\u0004\u0018\u000103H\u0016J\"\u00104\u001a\u0002052\u0006\u00106\u001a\u0002072\u0006\u00108\u001a\u0002092\u0008\u0010:\u001a\u0004\u0018\u00010;H\u0002J \u0010<\u001a\u0004\u0018\u00010!2\u0006\u0010)\u001a\u00020\u001e2\u000c\u0010=\u001a\u0008\u0012\u0004\u0012\u00020!0\u0019H\u0002J\u0010\u0010>\u001a\u0002052\u0006\u00101\u001a\u00020\u0003H\u0002JN\u0010?\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u00101\u001a\u00020\u00032\u0006\u0010$\u001a\u00020\u00042\u0012\u0010@\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050AH\u0016J\u0010\u0010B\u001a\u0002032\u0006\u0010$\u001a\u00020\u0004H\u0016R\u0014\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006C"
    }
    d2 = {
        "Lcom/squareup/items/unit/RealEditUnitWorkflow;",
        "Lcom/squareup/items/unit/EditUnitWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/unit/EditUnitInput;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "saveUnitWorkflow",
        "Lcom/squareup/items/unit/SaveUnitWorkflow;",
        "allStandardUnits",
        "Lcom/squareup/items/unit/AllPredefinedStandardUnits;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "eventLogger",
        "Lcom/squareup/items/unit/EditUnitEventLogger;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/util/Res;Lcom/squareup/items/unit/SaveUnitWorkflow;Lcom/squareup/items/unit/AllPredefinedStandardUnits;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/items/unit/EditUnitEventLogger;Lcom/squareup/settings/server/Features;)V",
        "allAvailableStandardUnits",
        "",
        "Lcom/squareup/items/unit/LocalizedStandardUnit;",
        "buildNewCustomUnit",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;",
        "customUnitName",
        "",
        "buildNewStandardUnit",
        "standardUnit",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
        "discardAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "state",
        "Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;",
        "currentUnitInEditing",
        "doesNewNameDuplicateExistingNames",
        "",
        "newUnitName",
        "unitsInUse",
        "",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "unitInEditingId",
        "getFilteredStandardUnits",
        "searchQuery",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "logMeasurementUnitSave",
        "",
        "unitSaved",
        "Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;",
        "saving",
        "Lcom/squareup/items/unit/EditUnitState$Saving;",
        "unitCreationSource",
        "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
        "matchingStandardUnit",
        "allPredefinedStandardUnits",
        "populateAllAvailableStandardUnits",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private allAvailableStandardUnits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/LocalizedStandardUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final allStandardUnits:Lcom/squareup/items/unit/AllPredefinedStandardUnits;

.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final eventLogger:Lcom/squareup/items/unit/EditUnitEventLogger;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final res:Lcom/squareup/util/Res;

.field private final saveUnitWorkflow:Lcom/squareup/items/unit/SaveUnitWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/items/unit/SaveUnitWorkflow;Lcom/squareup/items/unit/AllPredefinedStandardUnits;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/items/unit/EditUnitEventLogger;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "saveUnitWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allStandardUnits"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventLogger"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->saveUnitWorkflow:Lcom/squareup/items/unit/SaveUnitWorkflow;

    iput-object p3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allStandardUnits:Lcom/squareup/items/unit/AllPredefinedStandardUnits;

    iput-object p4, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p5, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->eventLogger:Lcom/squareup/items/unit/EditUnitEventLogger;

    iput-object p6, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$buildNewCustomUnit(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 0

    .line 116
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->buildNewCustomUnit(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$buildNewStandardUnit(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 0

    .line 116
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->buildNewStandardUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$discardAction(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 116
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->discardAction(Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$doesNewNameDuplicateExistingNames(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Z
    .locals 0

    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->doesNewNameDuplicateExistingNames(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$getAllStandardUnits$p(Lcom/squareup/items/unit/RealEditUnitWorkflow;)Lcom/squareup/items/unit/AllPredefinedStandardUnits;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allStandardUnits:Lcom/squareup/items/unit/AllPredefinedStandardUnits;

    return-object p0
.end method

.method public static final synthetic access$getFilteredStandardUnits(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;)Ljava/util/List;
    .locals 0

    .line 116
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->getFilteredStandardUnits(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$logMeasurementUnitSave(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;Lcom/squareup/items/unit/EditUnitState$Saving;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V
    .locals 0

    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->logMeasurementUnitSave(Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;Lcom/squareup/items/unit/EditUnitState$Saving;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V

    return-void
.end method

.method public static final synthetic access$matchingStandardUnit(Lcom/squareup/items/unit/RealEditUnitWorkflow;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 0

    .line 116
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->matchingStandardUnit(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p0

    return-object p0
.end method

.method private final buildNewCustomUnit(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 2

    .line 485
    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    const-string v1, ""

    invoke-direct {v0, p1, v1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final buildNewStandardUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;
    .locals 1

    .line 489
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->area_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Area;)V

    goto :goto_0

    .line 490
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->length_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Length;)V

    goto :goto_0

    .line 491
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->weight_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Weight;)V

    goto :goto_0

    .line 492
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->volume_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;)V

    goto :goto_0

    .line 493
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->time_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-direct {v0, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;)V

    :goto_0
    return-object v0

    .line 494
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "No standard unit found."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final discardAction(Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    .line 458
    invoke-interface {p1, p2}, Lcom/squareup/items/unit/EditUnitState$HasOriginalUnit;->shouldShowBackButton(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 459
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance p2, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allAvailableStandardUnits:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "allAvailableStandardUnits"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p2, v0}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;-><init>(Ljava/util/List;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, p2, v1, v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 461
    :cond_1
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object p2, Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;->INSTANCE:Lcom/squareup/items/unit/EditUnitResult$EditDiscarded;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final doesNewNameDuplicateExistingNames(Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .line 502
    check-cast p2, Ljava/lang/Iterable;

    .line 605
    instance-of v0, p2, Ljava/util/Collection;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 606
    :cond_0
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/unit/SelectableUnit;

    .line 503
    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "null cannot be cast to non-null type kotlin.CharSequence"

    if-eqz v3, :cond_6

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "null cannot be cast to non-null type java.lang.String"

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    const-string v6, "(this as java.lang.String).toLowerCase()"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_4

    move-object v4, p1

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/squareup/items/unit/SelectableUnit;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v5}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v4}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_1
    return v2
.end method

.method private final getFilteredStandardUnits(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/items/unit/LocalizedStandardUnit;",
            ">;"
        }
    .end annotation

    .line 478
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allAvailableStandardUnits:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "allAvailableStandardUnits"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Ljava/lang/Iterable;

    .line 602
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 603
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 479
    invoke-virtual {v3}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 480
    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x1

    .line 479
    invoke-static {v4, v5, v6}, Lkotlin/text/StringsKt;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v4

    if-nez v4, :cond_3

    .line 481
    invoke-virtual {v3}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getAbbreviation()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-static {v3, v5, v6}, Lkotlin/text/StringsKt;->contains(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :cond_3
    :goto_1
    if-eqz v6, :cond_1

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 604
    :cond_4
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method private final logMeasurementUnitSave(Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;Lcom/squareup/items/unit/EditUnitState$Saving;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V
    .locals 2

    .line 518
    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitResult$UnitSaved;->getSavedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    .line 519
    invoke-virtual {p2}, Lcom/squareup/items/unit/EditUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v0

    sget-object v1, Lcom/squareup/items/unit/RealEditUnitWorkflow$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitAction;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p2, 0x2

    if-eq v0, p2, :cond_0

    goto :goto_0

    .line 527
    :cond_0
    iget-object p2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->eventLogger:Lcom/squareup/items/unit/EditUnitEventLogger;

    invoke-virtual {p2, p1}, Lcom/squareup/items/unit/EditUnitEventLogger;->logMeasurementUnitDelete(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    goto :goto_0

    .line 521
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/items/unit/EditUnitState$Saving;->isCreatingNewUnit()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 522
    iget-object p2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->eventLogger:Lcom/squareup/items/unit/EditUnitEventLogger;

    if-nez p3, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {p2, p1, p3}, Lcom/squareup/items/unit/EditUnitEventLogger;->logMeasurementUnitCreate(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;)V

    goto :goto_0

    .line 524
    :cond_3
    iget-object p3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->eventLogger:Lcom/squareup/items/unit/EditUnitEventLogger;

    invoke-virtual {p2}, Lcom/squareup/items/unit/EditUnitState$Saving;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lcom/squareup/items/unit/EditUnitEventLogger;->logMeasurementUnitEdit(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    :goto_0
    return-void
.end method

.method private final matchingStandardUnit(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit;"
        }
    .end annotation

    .line 509
    check-cast p2, Ljava/lang/Iterable;

    .line 608
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 510
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v2}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type java.lang.String"

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "(this as java.lang.String).toLowerCase()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_2

    move-object v4, p1

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    const/4 v0, 0x0

    .line 609
    :goto_0
    check-cast v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    return-object v0
.end method

.method private final populateAllAvailableStandardUnits(Lcom/squareup/items/unit/EditUnitInput;)V
    .locals 7

    .line 465
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allStandardUnits:Lcom/squareup/items/unit/AllPredefinedStandardUnits;

    invoke-virtual {v0}, Lcom/squareup/items/unit/AllPredefinedStandardUnits;->getUnits()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 591
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 592
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/16 v3, 0xa

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 468
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput;->getUnitsInUse()Ljava/util/Set;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 593
    new-instance v6, Ljava/util/ArrayList;

    invoke-static {v5, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 594
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 595
    check-cast v5, Lcom/squareup/items/unit/SelectableUnit;

    .line 468
    invoke-virtual {v5}, Lcom/squareup/items/unit/SelectableUnit;->getBackingUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 596
    :cond_1
    check-cast v6, Ljava/util/List;

    .line 468
    invoke-interface {v6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 597
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 598
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 599
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 600
    check-cast v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 471
    new-instance v2, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 472
    iget-object v3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v3}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->res:Lcom/squareup/util/Res;

    invoke-static {v1, v4}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedAbbreviation(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    .line 471
    invoke-direct {v2, v1, v3, v4}, Lcom/squareup/items/unit/LocalizedStandardUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 601
    :cond_3
    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allAvailableStandardUnits:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/unit/EditUnitState;
    .locals 7

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 386
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_c

    .line 389
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->populateAllAvailableStandardUnits(Lcom/squareup/items/unit/EditUnitInput;)V

    .line 390
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allAvailableStandardUnits:Ljava/util/List;

    const-string v3, "allAvailableStandardUnits"

    if-nez v0, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v2

    .line 392
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput;->getPrepopulatedName()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_4

    if-eqz v0, :cond_3

    .line 396
    new-instance p1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allAvailableStandardUnits:Ljava/util/List;

    if-nez v0, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p1, v0}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto/16 :goto_5

    :cond_3
    const-string p1, ""

    .line 399
    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->buildNewCustomUnit(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 400
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 401
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    const-string v3, "newCustomUnitBuilder.build()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 400
    invoke-direct {v0, v2, p1, v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto/16 :goto_5

    .line 408
    :cond_4
    iget-object v0, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->allAvailableStandardUnits:Ljava/util/List;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Ljava/lang/Iterable;

    .line 580
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v0, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 581
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 582
    check-cast v4, Lcom/squareup/items/unit/LocalizedStandardUnit;

    .line 410
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput;->getPrepopulatedName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v2}, Lkotlin/text/StringsKt;->equals(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 411
    invoke-virtual {v4}, Lcom/squareup/items/unit/LocalizedStandardUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v4

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    .line 413
    :goto_2
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 583
    :cond_7
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 584
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 585
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 586
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 587
    :cond_8
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 588
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    check-cast v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v3, :cond_8

    move-object v2, v3

    goto :goto_3

    .line 408
    :cond_9
    check-cast v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v2, :cond_a

    .line 418
    invoke-direct {p0, v2}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->buildNewStandardUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    goto :goto_4

    .line 419
    :cond_a
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput;->getPrepopulatedName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->buildNewCustomUnit(Ljava/lang/String;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 420
    :goto_4
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 421
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    const-string/jumbo v3, "unitBuilder.build()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 420
    invoke-direct {v0, v2, p1, v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto :goto_5

    .line 585
    :cond_b
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Empty collection can\'t be reduced."

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 428
    :cond_c
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;

    if-eqz v0, :cond_d

    .line 429
    check-cast p1, Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput$AddDefaultStandardUnit;->getDefaultStandardUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->buildNewStandardUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    .line 430
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 431
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->build()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    const-string v3, "defaultStandardUnitToAddBuilder.build()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 430
    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto :goto_5

    .line 437
    :cond_d
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;

    if-eqz v0, :cond_10

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 438
    check-cast p1, Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;->getUnitToEdit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    new-instance v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput$ModifyExistingUnit;->getUnitToEdit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object p1

    invoke-direct {v3, p1}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 437
    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    .line 442
    :goto_5
    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    if-eqz v0, :cond_e

    .line 443
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    check-cast p1, Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    invoke-direct {v0, p1}, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;-><init>(Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    :cond_e
    if-eqz p2, :cond_f

    .line 448
    sget-object v0, Lcom/squareup/items/unit/EditUnitState;->Companion:Lcom/squareup/items/unit/EditUnitState$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/items/unit/EditUnitState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/items/unit/EditUnitState;

    move-result-object p2

    if-eqz p2, :cond_f

    move-object p1, p2

    :cond_f
    return-object p1

    .line 437
    :cond_10
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/EditUnitInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->initialState(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/unit/EditUnitState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/EditUnitInput;

    check-cast p2, Lcom/squareup/items/unit/EditUnitState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/EditUnitInput;",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "-",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    instance-of v0, p2, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    const-string v1, ""

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/items/unit/StandardUnitsListScreen;

    .line 139
    check-cast p2, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    new-instance v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;

    invoke-direct {v0, p0}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$1;-><init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 138
    invoke-direct {p1, p2, p3}, Lcom/squareup/items/unit/StandardUnitsListScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 533
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 534
    const-class p3, Lcom/squareup/items/unit/StandardUnitsListScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 535
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    .line 533
    invoke-direct {p2, p3, p1, v0}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 166
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 168
    :cond_0
    instance-of v0, p2, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/items/unit/EditUnitScreen;

    move-object v2, p2

    check-cast v2, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    new-instance v3, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;

    invoke-direct {v3, p0, p2, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$3;-><init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/items/unit/EditUnitInput;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-direct {v0, v2, p1}, Lcom/squareup/items/unit/EditUnitScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$EditUnit;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 538
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 539
    const-class p2, Lcom/squareup/items/unit/EditUnitScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 540
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 538
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 263
    sget-object p2, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    invoke-virtual {p2, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 265
    :cond_1
    instance-of v0, p2, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 266
    new-instance v0, Lcom/squareup/items/unit/EditUnitScreen;

    .line 267
    new-instance v3, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 268
    move-object v4, p2

    check-cast v4, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v4}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    .line 269
    new-instance v6, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    invoke-virtual {v4}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->getUpdatedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 270
    invoke-virtual {v4}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->isDefaultUnit()Z

    move-result v7

    .line 267
    invoke-direct {v3, v5, v6, v7}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    .line 266
    invoke-direct {v0, v3, v2}, Lcom/squareup/items/unit/EditUnitScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$EditUnit;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 543
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 544
    const-class v3, Lcom/squareup/items/unit/EditUnitScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 545
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 543
    invoke-direct {v2, v3, v0, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 273
    new-instance v0, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen;

    new-instance v3, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;

    invoke-direct {v3, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;-><init>(Lcom/squareup/items/unit/EditUnitState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {v0, v4, p2}, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 548
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 549
    const-class p3, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 550
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 548
    invoke-direct {p2, p3, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 265
    invoke-virtual {p1, v2, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 286
    :cond_2
    instance-of v0, p2, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    if-eqz v0, :cond_3

    .line 287
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 288
    new-instance v0, Lcom/squareup/items/unit/EditUnitScreen;

    .line 289
    new-instance v3, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 290
    move-object v4, p2

    check-cast v4, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    invoke-virtual {v4}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    .line 291
    new-instance v6, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    invoke-virtual {v4}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;->getUpdatedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 292
    invoke-virtual {v4}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;->isDefaultUnit()Z

    move-result v7

    .line 289
    invoke-direct {v3, v5, v6, v7}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    .line 288
    invoke-direct {v0, v3, v2}, Lcom/squareup/items/unit/EditUnitScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$EditUnit;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 553
    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    .line 554
    const-class v3, Lcom/squareup/items/unit/EditUnitScreen;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 555
    sget-object v5, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v5}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v5

    .line 553
    invoke-direct {v2, v3, v0, v5}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 296
    new-instance v0, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;

    new-instance v3, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$6;

    invoke-direct {v3, p0, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$6;-><init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {v0, v4, p2}, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 558
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 559
    const-class p3, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 560
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 558
    invoke-direct {p2, p3, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 287
    invoke-virtual {p1, v2, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 314
    :cond_3
    instance-of v0, p2, Lcom/squareup/items/unit/EditUnitState$Saving;

    if-eqz v0, :cond_5

    .line 315
    new-instance v5, Lcom/squareup/items/unit/SaveUnitInput;

    .line 316
    move-object v0, p2

    check-cast v0, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getUnitToSave()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getSaveUnitAction()Lcom/squareup/items/unit/SaveUnitAction;

    move-result-object v4

    .line 315
    invoke-direct {v5, v2, v3, v4}, Lcom/squareup/items/unit/SaveUnitInput;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/SaveUnitAction;)V

    .line 320
    iget-object v2, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->saveUnitWorkflow:Lcom/squareup/items/unit/SaveUnitWorkflow;

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    const/4 v6, 0x0

    .line 321
    new-instance v2, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$renderingOfSaveUnitWorkflow$1;-><init>(Lcom/squareup/items/unit/RealEditUnitWorkflow;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/items/unit/EditUnitInput;)V

    move-object v7, v2

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v3, p3

    .line 319
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    .line 341
    invoke-static {p1}, Lkotlin/collections/MapsKt;->toMutableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    .line 344
    sget-object p2, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    if-nez p2, :cond_4

    .line 345
    new-instance p2, Lcom/squareup/items/unit/EditUnitScreen;

    .line 346
    new-instance v2, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 347
    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    .line 348
    new-instance v4, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 349
    invoke-virtual {v0}, Lcom/squareup/items/unit/EditUnitState$Saving;->isDefaultUnit()Z

    move-result v0

    .line 346
    invoke-direct {v2, v3, v4, v0}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    .line 350
    sget-object v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$editUnitScreen$1;->INSTANCE:Lcom/squareup/items/unit/RealEditUnitWorkflow$render$editUnitScreen$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p3

    .line 345
    invoke-direct {p2, v2, p3}, Lcom/squareup/items/unit/EditUnitScreen;-><init>(Lcom/squareup/items/unit/EditUnitState$EditUnit;Lkotlin/jvm/functions/Function1;)V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 563
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 564
    const-class v0, Lcom/squareup/items/unit/EditUnitScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 565
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 563
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 352
    sget-object p2, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    :cond_4
    invoke-static {p1}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    goto/16 :goto_0

    .line 358
    :cond_5
    instance-of p1, p2, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    if-eqz p1, :cond_6

    .line 360
    iget-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-interface {p1}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->first(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "connectivityMonitor.inte\u2026tState().first(CONNECTED)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 567
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$$inlined$asWorker$1;

    invoke-direct {v0, p1, v2}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 568
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 569
    const-class v0, Lcom/squareup/connectivity/InternetState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 361
    new-instance p1, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;

    invoke-direct {p1, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$7;-><init>(Lcom/squareup/items/unit/EditUnitState;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 359
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 368
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    new-instance p2, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;

    invoke-direct {p2}, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;-><init>()V

    check-cast p2, Lcom/squareup/workflow/legacy/V2Screen;

    .line 571
    new-instance p3, Lcom/squareup/workflow/legacy/Screen;

    .line 572
    const-class v0, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    .line 573
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 571
    invoke-direct {p3, v0, p2, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 368
    invoke-virtual {p1, p3}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 371
    :cond_6
    instance-of p1, p2, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    if-eqz p1, :cond_7

    sget-object p1, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 372
    new-instance v0, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen;

    new-instance v2, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$8;

    invoke-direct {v2, p2}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$8;-><init>(Lcom/squareup/items/unit/EditUnitState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 576
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    .line 577
    const-class p3, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    invoke-static {p3, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p3

    .line 578
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 576
    invoke-direct {p2, p3, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 371
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/items/unit/EditUnitState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 452
    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow;->snapshotState(Lcom/squareup/items/unit/EditUnitState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
