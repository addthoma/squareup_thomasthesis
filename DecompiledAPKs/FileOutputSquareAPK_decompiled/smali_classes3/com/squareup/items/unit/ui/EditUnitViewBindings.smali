.class public final Lcom/squareup/items/unit/ui/EditUnitViewBindings;
.super Ljava/lang/Object;
.source "EditUnitViewBindings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\u0007\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0006\u0008\u0001\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/items/unit/ui/EditUnitViewBindings;",
        "",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V",
        "getBindings",
        "",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getRecyclerFactory$p(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)Lcom/squareup/recycler/RecyclerFactory;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)Lcom/squareup/util/Res;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/items/unit/ui/EditUnitViewBindings;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public final getBindings()Ljava/util/List;
    .locals 22
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    const/16 v1, 0x9

    new-array v1, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 32
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 33
    sget-object v3, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen;->Companion:Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 34
    new-instance v4, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$1;

    invoke-direct {v4, v0}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$1;-><init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 32
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 36
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 37
    sget-object v2, Lcom/squareup/items/unit/StandardUnitsListScreen;->Companion:Lcom/squareup/items/unit/StandardUnitsListScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/items/unit/StandardUnitsListScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    sget v6, Lcom/squareup/editunit/R$layout;->standard_units_list:I

    .line 38
    new-instance v2, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$2;

    invoke-direct {v2, v0}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$2;-><init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0xc

    const/4 v11, 0x0

    .line 36
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 42
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 43
    sget-object v2, Lcom/squareup/items/unit/EditUnitScreen;->Companion:Lcom/squareup/items/unit/EditUnitScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/items/unit/EditUnitScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    sget v6, Lcom/squareup/editunit/R$layout;->edit_unit:I

    .line 44
    new-instance v2, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$3;

    invoke-direct {v2, v0}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$3;-><init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 42
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 48
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 49
    sget-object v3, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;->Companion:Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 50
    sget-object v4, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$4;->INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$4;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 48
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 52
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 53
    sget-object v2, Lcom/squareup/items/unit/SaveInProgressScreen;->Companion:Lcom/squareup/items/unit/SaveInProgressScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/items/unit/SaveInProgressScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    sget v6, Lcom/squareup/editunit/R$layout;->edit_unit_save_in_progress:I

    .line 54
    sget-object v2, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$5;->INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$5;

    move-object v9, v2

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 55
    new-instance v2, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x1df

    const/16 v21, 0x0

    move-object v10, v2

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/16 v10, 0x8

    move-object v7, v2

    .line 52
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    .line 57
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 58
    sget-object v3, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen;->Companion:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 59
    sget-object v4, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$6;->INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$6;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 57
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v1, v3

    .line 61
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 62
    sget-object v3, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen;->Companion:Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/items/unit/CountOfAffectedVariationsAlertScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 63
    new-instance v4, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$7;

    invoke-direct {v4, v0}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$7;-><init>(Lcom/squareup/items/unit/ui/EditUnitViewBindings;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 61
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v1, v3

    .line 65
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 66
    sget-object v3, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen;->Companion:Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/items/unit/InternetNotAvailableAlertScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 67
    sget-object v4, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$8;->INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$8;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 65
    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/4 v3, 0x7

    aput-object v2, v1, v3

    .line 69
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object v3, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;->Companion:Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;

    invoke-virtual {v3}, Lcom/squareup/items/unit/CheckInternetAvailabilityScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    sget-object v4, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;->INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v2

    const/16 v3, 0x8

    aput-object v2, v1, v3

    .line 31
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method
