.class final Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitViewBindings.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/EditUnitViewBindings;->getBindings()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002*\u0001\u0001\u0010\u0000\u001a\u00020\u00012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "com/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1",
        "it",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/CheckInternetAvailabilityScreen;",
        "",
        "invoke",
        "(Lio/reactivex/Observable;)Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;

    invoke-direct {v0}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;-><init>()V

    sput-object v0, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;->INSTANCE:Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance p1, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;

    invoke-direct {p1}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;-><init>()V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9;->invoke(Lio/reactivex/Observable;)Lcom/squareup/items/unit/ui/EditUnitViewBindings$getBindings$9$1;

    move-result-object p1

    return-object p1
.end method
