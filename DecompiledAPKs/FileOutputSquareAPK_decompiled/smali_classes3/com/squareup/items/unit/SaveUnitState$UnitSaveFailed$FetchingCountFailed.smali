.class public final Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;
.super Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;
.source "SaveUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FetchingCountFailed"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;",
        "Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;",
        "previousFetchingCount",
        "Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;",
        "configuration",
        "Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;",
        "(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V",
        "getConfiguration",
        "()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;",
        "getPreviousFetchingCount",
        "()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configuration:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

.field private final previousFetchingCount:Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;


# direct methods
.method public constructor <init>(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V
    .locals 1

    const-string v0, "previousFetchingCount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, v0, p1, p2, v0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$Saving;Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->previousFetchingCount:Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    iput-object p2, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->configuration:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;ILjava/lang/Object;)Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->copy(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;
    .locals 1

    const-string v0, "previousFetchingCount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;-><init>(Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->configuration:Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    return-object v0
.end method

.method public getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->previousFetchingCount:Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FetchingCountFailed(previousFetchingCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getPreviousFetchingCount()Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed$FetchingCountFailed;->getConfiguration()Lcom/squareup/items/unit/UnitSaveFailedAlertScreen$UnitSaveFailedAlertScreenConfiguration;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
