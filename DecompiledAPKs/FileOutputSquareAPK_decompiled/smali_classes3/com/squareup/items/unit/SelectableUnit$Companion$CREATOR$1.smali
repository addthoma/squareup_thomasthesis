.class public final Lcom/squareup/items/unit/SelectableUnit$Companion$CREATOR$1;
.super Ljava/lang/Object;
.source "SelectableUnit.kt"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/SelectableUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/items/unit/SelectableUnit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectableUnit.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectableUnit.kt\ncom/squareup/items/unit/SelectableUnit$Companion$CREATOR$1\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,89:1\n180#2:90\n*E\n*S KotlinDebug\n*F\n+ 1 SelectableUnit.kt\ncom/squareup/items/unit/SelectableUnit$Companion$CREATOR$1\n*L\n53#1:90\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u001d\u0010\u0006\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/items/unit/SelectableUnit$Companion$CREATOR$1",
        "Landroid/os/Parcelable$Creator;",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "createFromParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "newArray",
        "",
        "size",
        "",
        "(I)[Lcom/squareup/items/unit/SelectableUnit;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/items/unit/SelectableUnit;
    .locals 7

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 49
    new-array v2, v0, [B

    .line 50
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readByteArray([B)V

    .line 52
    sget-object v1, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x3

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lokio/ByteString$Companion;->of$default(Lokio/ByteString$Companion;[BIIILjava/lang/Object;)Lokio/ByteString;

    move-result-object p1

    .line 90
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 54
    sget-object v0, Lcom/squareup/items/unit/SelectableUnit;->Companion:Lcom/squareup/items/unit/SelectableUnit$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/items/unit/SelectableUnit$Companion;->readSelectableUnit(Lokio/BufferedSource;)Lcom/squareup/items/unit/SelectableUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/SelectableUnit$Companion$CREATOR$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/items/unit/SelectableUnit;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/items/unit/SelectableUnit;
    .locals 0

    .line 58
    new-array p1, p1, [Lcom/squareup/items/unit/SelectableUnit;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/SelectableUnit$Companion$CREATOR$1;->newArray(I)[Lcom/squareup/items/unit/SelectableUnit;

    move-result-object p1

    return-object p1
.end method
