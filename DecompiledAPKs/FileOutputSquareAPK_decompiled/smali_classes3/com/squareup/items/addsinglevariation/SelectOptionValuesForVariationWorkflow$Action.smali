.class public abstract Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;
.super Ljava/lang/Object;
.source "SelectOptionValuesForVariationWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$Exit;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$TapOption;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$AddVariation;,
        Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0006\u0005\u0006\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0006\u000b\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "()V",
        "AddVariation",
        "ConfirmDuplicateVariationWarning",
        "CreateCustomVariation",
        "Exit",
        "SelectMoreOptionValues",
        "TapOption",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$Exit;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$SelectMoreOptionValues;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$TapOption;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$CreateCustomVariation;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$AddVariation;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action$ConfirmDuplicateVariationWarning;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 242
    invoke-direct {p0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 242
    invoke-virtual {p0, p1}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
