.class public abstract Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;
.super Ljava/lang/Object;
.source "SelectSingleOptionValueForVariationState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;,
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;,
        Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0013\u0014\u0015B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006R\u0014\u0010\t\u001a\u0004\u0018\u00010\nX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0012\u0010\r\u001a\u00020\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u0012\u0010\u0011\u001a\u00020\u000eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0010\u0082\u0001\u0003\u0016\u0017\u0018\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;",
        "Landroid/os/Parcelable;",
        "()V",
        "newValueInEdit",
        "",
        "getNewValueInEdit",
        "()Ljava/lang/String;",
        "searchText",
        "getSearchText",
        "selectedValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "getSelectedValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "shouldFocusOnNewValueRowAndShowKeyboard",
        "",
        "getShouldFocusOnNewValueRowAndShowKeyboard",
        "()Z",
        "shouldHighlightDuplicateName",
        "getShouldHighlightDuplicateName",
        "DuplicateOptionValueNameState",
        "DuplicateVariationState",
        "SelectOptionValueState",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$SelectOptionValueState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateOptionValueNameState;",
        "Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState$DuplicateVariationState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getNewValueInEdit()Ljava/lang/String;
.end method

.method public abstract getSearchText()Ljava/lang/String;
.end method

.method public abstract getSelectedValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
.end method

.method public abstract getShouldFocusOnNewValueRowAndShowKeyboard()Z
.end method

.method public abstract getShouldHighlightDuplicateName()Z
.end method
