.class public final Lcom/squareup/items/editoption/ui/EditOptionValueRow;
.super Landroid/widget/FrameLayout;
.source "EditOptionValueRow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditOptionValueRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditOptionValueRow.kt\ncom/squareup/items/editoption/ui/EditOptionValueRow\n*L\n1#1,82:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u001a\u001a\u00020\u00152\u0019\u0008\u0004\u0010\u001b\u001a\u0013\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00150\u001c\u00a2\u0006\u0002\u0008\u001dH\u0086\u0008J\u000f\u0010\u001e\u001a\u0004\u0018\u00010\u0015H\u0002\u00a2\u0006\u0002\u0010\u001fJ\u000e\u0010 \u001a\u00020\u00152\u0006\u0010!\u001a\u00020\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u00020\u000e8\u0000X\u0081\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000f\u0010\u0010\u001a\u0004\u0008\u0011\u0010\u0012R\"\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\"\u0004\u0008\u0018\u0010\u0019\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/items/editoption/ui/EditOptionValueRow;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "alreadyPromoted",
        "",
        "deleteIconPlugin",
        "Lcom/squareup/items/editoption/ui/DeleteIconPlugin;",
        "dragHandlePlugin",
        "Lcom/squareup/noho/IconPlugin;",
        "dragHandleView",
        "Landroid/widget/ImageView;",
        "editRow",
        "Lcom/squareup/noho/NohoEditRow;",
        "editRow$annotations",
        "()V",
        "getEditRow",
        "()Lcom/squareup/noho/NohoEditRow;",
        "onDelete",
        "Lkotlin/Function0;",
        "",
        "getOnDelete",
        "()Lkotlin/jvm/functions/Function0;",
        "setOnDelete",
        "(Lkotlin/jvm/functions/Function0;)V",
        "configureEdit",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "onClickDelete",
        "()Lkotlin/Unit;",
        "setPromoted",
        "shouldBePromoted",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private alreadyPromoted:Z

.field private deleteIconPlugin:Lcom/squareup/items/editoption/ui/DeleteIconPlugin;

.field private dragHandlePlugin:Lcom/squareup/noho/IconPlugin;

.field private final dragHandleView:Landroid/widget/ImageView;

.field private final editRow:Lcom/squareup/noho/NohoEditRow;

.field private onDelete:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 28
    new-instance v0, Lcom/squareup/noho/NohoEditRow;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x6

    const/4 v8, 0x0

    move-object v3, v0

    move-object v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/squareup/noho/NohoEditRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    .line 29
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoEditRow;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    check-cast v0, Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    check-cast v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v3}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 31
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 32
    sget p1, Lcom/squareup/items/editoption/impl/R$id;->dragHandle:I

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setId(I)V

    .line 33
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 31
    iput-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandleView:Landroid/widget/ImageView;

    return-void
.end method

.method public static final synthetic access$onClickDelete(Lcom/squareup/items/editoption/ui/EditOptionValueRow;)Lkotlin/Unit;
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->onClickDelete()Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic editRow$annotations()V
    .locals 0

    return-void
.end method

.method private final onClickDelete()Lkotlin/Unit;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->onDelete:Lkotlin/jvm/functions/Function0;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public final configureEdit(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/NohoEditRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getEditRow()Lcom/squareup/noho/NohoEditRow;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final getEditRow()Lcom/squareup/noho/NohoEditRow;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    return-object v0
.end method

.method public final getOnDelete()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->onDelete:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final setOnDelete(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 18
    iput-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->onDelete:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setPromoted(Z)V
    .locals 9

    if-eqz p1, :cond_4

    .line 39
    iget-boolean p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->alreadyPromoted:Z

    if-nez p1, :cond_9

    .line 40
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->deleteIconPlugin:Lcom/squareup/items/editoption/ui/DeleteIconPlugin;

    const-string v0, "context"

    if-nez p1, :cond_0

    .line 41
    new-instance p1, Lcom/squareup/items/editoption/ui/DeleteIconPlugin;

    invoke-virtual {p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/items/editoption/ui/EditOptionValueRow$setPromoted$1;

    invoke-direct {v2, p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow$setPromoted$1;-><init>(Lcom/squareup/items/editoption/ui/EditOptionValueRow;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p1, v1, v2}, Lcom/squareup/items/editoption/ui/DeleteIconPlugin;-><init>(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->deleteIconPlugin:Lcom/squareup/items/editoption/ui/DeleteIconPlugin;

    .line 44
    :cond_0
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandlePlugin:Lcom/squareup/noho/IconPlugin;

    if-nez p1, :cond_1

    .line 45
    new-instance p1, Lcom/squareup/noho/IconPlugin;

    .line 46
    invoke-virtual {p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget-object v3, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    .line 48
    sget v4, Lcom/squareup/vectoricons/R$drawable;->icon_menu_3:I

    .line 49
    sget v5, Lcom/squareup/noho/R$dimen;->noho_edit_default_margin:I

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p1

    .line 45
    invoke-direct/range {v1 .. v8}, Lcom/squareup/noho/IconPlugin;-><init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandlePlugin:Lcom/squareup/noho/IconPlugin;

    .line 51
    :cond_1
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->deleteIconPlugin:Lcom/squareup/items/editoption/ui/DeleteIconPlugin;

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast v1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 52
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandlePlugin:Lcom/squareup/noho/IconPlugin;

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    check-cast v1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget v1, Lcom/squareup/vectoricons/R$drawable;->icon_menu_3:I

    .line 53
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->getDrawableCompat(Landroid/content/Context;I)Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/vectordrawable/graphics/drawable/VectorDrawableCompat;->getIntrinsicWidth()I

    move-result p1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_edit_default_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr p1, v0

    .line 57
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandleView:Landroid/widget/ImageView;

    check-cast v0, Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, p1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v1}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p1, 0x1

    .line 60
    iput-boolean p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->alreadyPromoted:Z

    goto :goto_0

    .line 63
    :cond_4
    iget-boolean p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->alreadyPromoted:Z

    if-eqz p1, :cond_9

    .line 64
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->deleteIconPlugin:Lcom/squareup/items/editoption/ui/DeleteIconPlugin;

    if-eqz p1, :cond_6

    .line 65
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->removePlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 67
    :cond_6
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandlePlugin:Lcom/squareup/noho/IconPlugin;

    if-eqz p1, :cond_8

    .line 68
    iget-object v0, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->editRow:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditRow;->removePlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 70
    :cond_8
    iget-object p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->dragHandleView:Landroid/widget/ImageView;

    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->removeView(Landroid/view/View;)V

    const/4 p1, 0x0

    .line 71
    iput-boolean p1, p0, Lcom/squareup/items/editoption/ui/EditOptionValueRow;->alreadyPromoted:Z

    :cond_9
    :goto_0
    return-void
.end method
