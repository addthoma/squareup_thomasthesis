.class public final Lcom/squareup/items/editoption/EditOptionLayoutRunner;
.super Ljava/lang/Object;
.source "EditOptionLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;,
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$Factory;,
        Lcom/squareup/items/editoption/EditOptionLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/editoption/EditOptionScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditOptionLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditOptionLayoutRunner.kt\ncom/squareup/items/editoption/EditOptionLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,309:1\n1360#2:310\n1429#2,3:311\n49#3:314\n50#3,3:320\n53#3:355\n599#4,4:315\n601#4:319\n310#5,6:323\n310#5,6:329\n310#5,6:335\n310#5,6:341\n310#5,6:347\n43#6,2:353\n*E\n*S KotlinDebug\n*F\n+ 1 EditOptionLayoutRunner.kt\ncom/squareup/items/editoption/EditOptionLayoutRunner\n*L\n243#1:310\n243#1,3:311\n51#1:314\n51#1,3:320\n51#1:355\n51#1,4:315\n51#1:319\n51#1,6:323\n51#1,6:329\n51#1,6:335\n51#1,6:341\n51#1,6:347\n51#1,2:353\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 \u001a2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u001a\u001b\u001cB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0002H\u0002J\u0010\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0002H\u0002R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u000b\u001a\u0016\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \n*\u0004\u0018\u00010\u00130\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/editoption/EditOptionScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "moveHandler",
        "Lkotlin/Function2;",
        "",
        "",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateRecycler",
        "Companion",
        "EditOptionRow",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/editoption/EditOptionLayoutRunner$Companion;

.field private static final numStaticRowsBeforeOptionList:I = 0x3


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private moveHandler:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->Companion:Lcom/squareup/items/editoption/EditOptionLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->view:Landroid/view/View;

    .line 48
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 50
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/editoption/impl/R$id;->edit_option_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 51
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 315
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 320
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 321
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 52
    sget-object p2, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 324
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 58
    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 324
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 323
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 330
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 77
    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 330
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 329
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 336
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 96
    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$4$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$4$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 336
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 335
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 342
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 118
    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 149
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$2;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$5$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 341
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 348
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$row$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 153
    sget-object v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$6$1;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$recycler$1$6$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 348
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 347
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 173
    new-instance p2, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {p2, p0}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$$special$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/items/editoption/EditOptionLayoutRunner;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p2}, Lcom/squareup/cycler/RecyclerMutationsKt;->enableMutations(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V

    .line 353
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 188
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 353
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 318
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 315
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getMoveHandler$p(Lcom/squareup/items/editoption/EditOptionLayoutRunner;)Lkotlin/jvm/functions/Function2;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->moveHandler:Lkotlin/jvm/functions/Function2;

    return-object p0
.end method

.method public static final synthetic access$setMoveHandler$p(Lcom/squareup/items/editoption/EditOptionLayoutRunner;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->moveHandler:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/items/editoption/EditOptionScreen;)V
    .locals 6

    .line 221
    iget-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 203
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 204
    new-instance v2, Lcom/squareup/resources/ResourceString;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->isNewOptionSet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 205
    sget v3, Lcom/squareup/items/editoption/impl/R$string;->edit_option_create_screen_title:I

    goto :goto_0

    .line 207
    :cond_0
    sget v3, Lcom/squareup/items/editoption/impl/R$string;->edit_option_edit_screen_title:I

    .line 204
    :goto_0
    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 209
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/items/editoption/EditOptionScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 211
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 212
    new-instance v3, Lcom/squareup/resources/ResourceString;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->isNewOptionSet()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 213
    sget v4, Lcom/squareup/common/strings/R$string;->create:I

    goto :goto_1

    .line 215
    :cond_1
    sget v4, Lcom/squareup/common/strings/R$string;->done:I

    .line 212
    :goto_1
    invoke-direct {v3, v4}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 217
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->isCreateButtonEnabled()Z

    move-result v4

    .line 218
    new-instance v5, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateActionBar$2;

    invoke-direct {v5, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateActionBar$2;-><init>(Lcom/squareup/items/editoption/EditOptionScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 210
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateRecycler(Lcom/squareup/items/editoption/EditOptionScreen;)V
    .locals 11

    .line 227
    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$1;-><init>(Lcom/squareup/items/editoption/EditOptionScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    iput-object v0, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->moveHandler:Lkotlin/jvm/functions/Function2;

    .line 232
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 235
    new-instance v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionNameInputRow;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getNameValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionNameInputRow;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    new-instance v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionDisplayNameInputRow;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getDisplayNameValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionDisplayNameInputRow;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    new-instance v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueSectionHeaderRow;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getDisplayNameValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueSectionHeaderRow;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getAddedOptionValues()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 310
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 311
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 312
    check-cast v3, Lcom/squareup/items/editoption/OptionValue;

    .line 244
    new-instance v10, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    .line 245
    invoke-virtual {v3}, Lcom/squareup/items/editoption/OptionValue;->getId()Ljava/lang/String;

    move-result-object v5

    .line 246
    invoke-virtual {v3}, Lcom/squareup/items/editoption/OptionValue;->getNameValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v6

    const/4 v7, 0x0

    .line 248
    invoke-virtual {v3}, Lcom/squareup/items/editoption/OptionValue;->isDuplicate()Z

    move-result v8

    .line 249
    new-instance v4, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$$inlined$map$lambda$1;

    invoke-direct {v4, v3, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$$inlined$map$lambda$1;-><init>(Lcom/squareup/items/editoption/OptionValue;Lcom/squareup/items/editoption/EditOptionScreen;)V

    move-object v9, v4

    check-cast v9, Lkotlin/jvm/functions/Function0;

    move-object v4, v10

    .line 244
    invoke-direct/range {v4 .. v9}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;-><init>(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V

    .line 252
    invoke-interface {v2, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 313
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    .line 243
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 256
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getHasReachedOptionValueNumberLimit()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 257
    sget-object p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueNumberLimitHelpTextRow;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$OptionValueNumberLimitHelpTextRow;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 259
    :cond_1
    new-instance v7, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;

    .line 260
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getAddValueId()Ljava/lang/String;

    move-result-object v2

    .line 261
    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionScreen;->getAddNewOptionValue()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 264
    sget-object p1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$3;->INSTANCE:Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$3;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    move-object v1, v7

    .line 259
    invoke-direct/range {v1 .. v6}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$EditOptionRow$AddedOptionValueRow;-><init>(Ljava/lang/String;Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    :goto_1
    iget-object p1, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$4;

    invoke-direct {v1, v0}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$updateRecycler$4;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/editoption/EditOptionScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-direct {p0, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->updateActionBar(Lcom/squareup/items/editoption/EditOptionScreen;)V

    .line 196
    invoke-direct {p0, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->updateRecycler(Lcom/squareup/items/editoption/EditOptionScreen;)V

    .line 197
    iget-object p2, p0, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/editoption/EditOptionLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/editoption/EditOptionLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/editoption/EditOptionScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/items/editoption/EditOptionScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/EditOptionLayoutRunner;->showRendering(Lcom/squareup/items/editoption/EditOptionScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
