.class public final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;
.super Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.source "EditOptionValueWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChangeOptionValueName"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001J\u001c\u0010\u0010\u001a\u00020\u0011*\u0012\u0012\u0008\u0012\u00060\u0013j\u0002`\u0014\u0012\u0004\u0012\u00020\u00150\u0012H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;",
        "newName",
        "",
        "(Ljava/lang/String;)V",
        "getNewName",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueState;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "newName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->copy(Ljava/lang/String;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            "-",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    iget-object v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3, v2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;->copy$default(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;
    .locals 1

    const-string v0, "newName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;

    invoke-direct {v0, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;

    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNewName()Ljava/lang/String;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ChangeOptionValueName(newName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$ChangeOptionValueName;->newName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
