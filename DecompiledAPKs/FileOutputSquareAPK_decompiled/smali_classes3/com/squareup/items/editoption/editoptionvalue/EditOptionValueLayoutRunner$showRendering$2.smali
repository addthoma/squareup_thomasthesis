.class final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;
.super Ljava/lang/Object;
.source "EditOptionValueLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/register/widgets/PickColorGrid$OnColorChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->showRendering(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "onColorChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

.field final synthetic this$0:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;->this$0:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;

    iput-object p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onColorChanged(Ljava/lang/String;)V
    .locals 3

    .line 68
    iget-object v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;

    invoke-virtual {v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueScreen;->getOnChangeColor()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$showRendering$2;->this$0:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, p1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;->access$toRgbWithHash(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
