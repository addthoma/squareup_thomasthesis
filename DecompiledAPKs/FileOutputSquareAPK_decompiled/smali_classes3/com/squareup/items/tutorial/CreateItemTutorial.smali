.class public final Lcom/squareup/items/tutorial/CreateItemTutorial;
.super Ljava/lang/Object;
.source "CreateItemTutorial.kt"

# interfaces
.implements Lcom/squareup/tutorialv2/Tutorial;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;,
        Lcom/squareup/items/tutorial/CreateItemTutorial$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateItemTutorial.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateItemTutorial.kt\ncom/squareup/items/tutorial/CreateItemTutorial\n*L\n1#1,244:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 )2\u00020\u0001:\u0002)*B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019J\u0010\u0010\u001a\u001a\u00020\u00172\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0008\u0010\u001d\u001a\u00020\u0017H\u0016J\u0008\u0010\u001e\u001a\u00020\u0017H\u0016J\u001a\u0010\u001f\u001a\u00020\u00172\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u0018\u0010$\u001a\u00020\u00172\u0006\u0010 \u001a\u00020!2\u0006\u0010%\u001a\u00020&H\u0016J\u000e\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00150(H\u0016R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorial;",
        "Lcom/squareup/tutorialv2/Tutorial;",
        "device",
        "Lcom/squareup/util/Device;",
        "appletsDrawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "tabletTutorial",
        "Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;",
        "phoneTutorial",
        "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;",
        "lockScreenMonitor",
        "Lcom/squareup/permissions/ui/LockScreenMonitor;",
        "squareDeviceTourSettings",
        "Lcom/squareup/SquareDeviceTourSettings;",
        "eventLocker",
        "Lcom/squareup/register/tutorial/TutorialEventLocker;",
        "(Lcom/squareup/util/Device;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/register/tutorial/TutorialEventLocker;)V",
        "activeEventHandler",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler;",
        "output",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "init",
        "",
        "startedFromSupportApplet",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitRequested",
        "onExitScope",
        "onTutorialEvent",
        "name",
        "",
        "value",
        "",
        "onTutorialPendingActionEvent",
        "pendingAction",
        "Lcom/squareup/tutorialv2/TutorialCore$PendingAction;",
        "tutorialState",
        "Lio/reactivex/Observable;",
        "Companion",
        "Creator",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATE_ITEM_TUTORIAL_COMPLETED_FLAG:Ljava/lang/String; = "createItemTutorialCompleted"

.field public static final Companion:Lcom/squareup/items/tutorial/CreateItemTutorial$Companion;

.field public static final TAG:Ljava/lang/String; = "CREATE_ITEM_TUTORIAL"


# instance fields
.field private activeEventHandler:Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

.field private final appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private final device:Lcom/squareup/util/Device;

.field private final eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

.field private final lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

.field private final output:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

.field private final squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

.field private final tabletTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorial$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorial$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorial;->Companion:Lcom/squareup/items/tutorial/CreateItemTutorial$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/SquareDeviceTourSettings;Lcom/squareup/register/tutorial/TutorialEventLocker;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletsDrawerRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tabletTutorial"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneTutorial"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lockScreenMonitor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "squareDeviceTourSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventLocker"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->device:Lcom/squareup/util/Device;

    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->tabletTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    iput-object p4, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->phoneTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    iput-object p5, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

    iput-object p6, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

    iput-object p7, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    .line 74
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 75
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->tabletTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    check-cast p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->activeEventHandler:Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    return-void
.end method


# virtual methods
.method public final init(Z)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->activeEventHandler:Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->init(Z)V

    const-string p1, "Start Tutorial"

    const/4 v0, 0x0

    .line 79
    invoke-virtual {p0, p1, v0}, Lcom/squareup/items/tutorial/CreateItemTutorial;->onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->tabletTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->phoneTutorial:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;

    :goto_0
    check-cast v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    iput-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->activeEventHandler:Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    .line 85
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletsDrawerRunner;->drawerStatePublisher()Lio/reactivex/Observable;

    move-result-object v0

    .line 86
    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/items/tutorial/CreateItemTutorial$onEnterScope$1;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorial;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "appletsDrawerRunner.draw\u2026ll)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->squareDeviceTourSettings:Lcom/squareup/SquareDeviceTourSettings;

    invoke-virtual {p1}, Lcom/squareup/SquareDeviceTourSettings;->shouldShowTour()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 95
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    const-string v0, "Leaving Device or Feature Tour"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/tutorial/TutorialEventLocker;->lockUntil([Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onExitRequested()V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->activeEventHandler:Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->showSkipModal()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTutorialEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

    invoke-interface {v0}, Lcom/squareup/permissions/ui/LockScreenMonitor;->showLockScreen()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->eventLocker:Lcom/squareup/register/tutorial/TutorialEventLocker;

    invoke-virtual {v0, p1}, Lcom/squareup/register/tutorial/TutorialEventLocker;->isLocked(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->activeEventHandler:Lcom/squareup/items/tutorial/CreateItemTutorialHandler;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 114
    iget-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    return-void

    .line 109
    :cond_2
    :goto_0
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object p2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onTutorialPendingActionEvent(Ljava/lang/String;Lcom/squareup/tutorialv2/TutorialCore$PendingAction;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "pendingAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public tutorialState()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/tutorialv2/TutorialState;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorial;->output:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
