.class public final Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;
.super Lcom/squareup/items/tutorial/CreateItemTutorialHandler;
.source "CreateItemTutorialPhoneHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019BA\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u001e\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler;",
        "appletsDrawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "dialogFactory",
        "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
        "tooltipFactory",
        "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
        "flow",
        "Lflow/Flow;",
        "tutorialApi",
        "Lcom/squareup/register/tutorial/TutorialApi;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "preferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "(Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V",
        "currentApplet",
        "Lcom/squareup/applet/Applet;",
        "handleEvent",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "name",
        "",
        "value",
        "",
        "Companion",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$Companion;

.field public static final SHOW_TUTORIAL_COMPLETE:Ljava/lang/String; = "Tutorial Complete"


# instance fields
.field private final appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private currentApplet:Lcom/squareup/applet/Applet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->Companion:Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V
    .locals 7
    .param p7    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "appletsDrawerRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialogFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialApi"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferences"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 58
    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    return-void
.end method


# virtual methods
.method public handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 10

    const-string v0, "CREATE_ITEM_TUTORIAL"

    .line 72
    invoke-static {v0}, Ltimber/log/Timber;->tag(Ljava/lang/String;)Ltimber/log/Timber$Tree;

    move-result-object v0

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleEvent (Phone) -- State: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", Name: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\', Value: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ltimber/log/Timber$Tree;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    .line 75
    move-object v1, v0

    check-cast v1, Lcom/squareup/tutorialv2/TutorialState;

    const-string v2, "Applet Selected"

    .line 77
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p2, :cond_0

    .line 78
    move-object v3, p2

    check-cast v3, Lcom/squareup/applet/Applet;

    iput-object v3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->currentApplet:Lcom/squareup/applet/Applet;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.applet.Applet"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 81
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v3

    sget-object v4, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v3

    aget v3, v4, v3

    const-string v4, "Applets Drawer Opened"

    const-string v5, "TutorialV2DialogScreen primary tapped"

    const-string v6, "TutorialV2DialogScreen secondary tapped"

    const v7, -0x3ff4162f

    const-string v8, "Items Applet Loaded"

    const-string v9, "Items Section List Add Button Shown"

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_3

    .line 257
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleSkip(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :pswitch_1
    if-nez p1, :cond_2

    goto/16 :goto_3

    .line 235
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const v0, -0x45ba1ef6

    if-eq p2, v0, :cond_5

    const v0, -0x2a30eac4

    if-eq p2, v0, :cond_4

    const v0, 0x768473bb

    if-eq p2, v0, :cond_3

    goto/16 :goto_3

    :cond_3
    const-string p2, "Tutorial Complete"

    .line 237
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 238
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getFlow()Lflow/Flow;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getDialogFactory()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->completeModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    move-result-object p2

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 242
    :cond_4
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 243
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_MODAL_CONTINUE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 244
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    .line 245
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getTutorialApi()Lcom/squareup/register/tutorial/TutorialApi;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/tutorial/TutorialApi;->forceStartFirstPaymentTutorial()V

    .line 246
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->markAsCompleted()V

    goto/16 :goto_3

    .line 250
    :cond_5
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 251
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SKIP_MODAL_END_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 252
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    .line 253
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->markAsCompleted()V

    goto/16 :goto_3

    .line 232
    :pswitch_2
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleItemDialogSadPath(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    .line 230
    :pswitch_3
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleAdjustInventory(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    .line 228
    :pswitch_4
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEditCategory(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    .line 223
    :pswitch_5
    sget-object v5, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;

    invoke-direct {v0, p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$handleEvent$1;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function0;

    const-string v6, "Items Section List Add Button Shown"

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v2 .. v7}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleItemSaveEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    .line 220
    :pswitch_6
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1, p2, v0, v9}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleItemPriceEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    .line 217
    :pswitch_7
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1, p2, v0, v9}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleItemNameEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :pswitch_8
    if-nez p1, :cond_6

    goto/16 :goto_3

    .line 209
    :cond_6
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const v0, 0x432bc149

    if-eq p2, v0, :cond_7

    goto/16 :goto_3

    :cond_7
    const-string p2, "Items Section List Search Ended"

    .line 211
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 212
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    goto/16 :goto_3

    :pswitch_9
    if-nez p1, :cond_8

    goto/16 :goto_3

    .line 199
    :cond_8
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    const v2, -0x18f9bc2a

    if-eq p2, v2, :cond_a

    const v2, 0x4c63e0b3    # 5.973678E7f

    if-eq p2, v2, :cond_9

    goto/16 :goto_3

    :cond_9
    const-string p2, "Item Changes Discarded"

    .line 201
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    goto :goto_1

    :cond_a
    const-string p2, "Edit Item Save Pressed"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 203
    :goto_1
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 204
    invoke-virtual {p0, v9, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :pswitch_a
    if-nez p1, :cond_b

    goto/16 :goto_3

    .line 167
    :cond_b
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto/16 :goto_3

    .line 169
    :sswitch_0
    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 170
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 171
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->selectCreateItemPhone()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :sswitch_1
    const-string p2, "Items Section List Search Started"

    .line 175
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 176
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION_SAD_PATH_SEARCH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 177
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_3

    .line 186
    :sswitch_2
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 187
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_APPLET:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 188
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :sswitch_3
    const-string p2, "Edit Item Shown"

    .line 192
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 193
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 194
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_3

    :sswitch_4
    const-string p2, "Items Section List Add Button Tapped"

    .line 181
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 182
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_NAME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    goto/16 :goto_3

    :pswitch_b
    if-nez p1, :cond_c

    goto/16 :goto_3

    .line 141
    :cond_c
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    const v2, -0x6c47cbb3

    if-eq v0, v2, :cond_f

    if-eq v0, v7, :cond_e

    const v2, 0x4eae60b

    if-eq v0, v2, :cond_d

    goto/16 :goto_3

    .line 143
    :cond_d
    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 144
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->selectAllItemsFromItemsApplet()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    .line 160
    :cond_e
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 161
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->APPLETS_DRAWER:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 162
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :cond_f
    const-string v0, "Items Applet Section Tapped"

    .line 148
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 149
    instance-of p1, p2, Lcom/squareup/ui/items/ItemsAppletSection$AllItems;

    if-eqz p1, :cond_10

    .line 151
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_ALL_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 152
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    goto/16 :goto_3

    .line 155
    :cond_10
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_3

    :pswitch_c
    if-nez p1, :cond_11

    goto/16 :goto_3

    .line 109
    :cond_11
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v5, -0x54a1fd2c

    if-eq v3, v5, :cond_15

    if-eq v3, v7, :cond_14

    const v0, 0x7cf2cba1

    if-eq v3, v0, :cond_12

    goto/16 :goto_3

    .line 116
    :cond_12
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 118
    instance-of p1, p2, Lcom/squareup/ui/items/ItemsApplet;

    if-eqz p1, :cond_13

    .line 119
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_ITEMS_NAVIGATION:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 120
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_APPLET:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 124
    :cond_13
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_3

    .line 111
    :cond_14
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 112
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 113
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->selectItemsFromApplet()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto/16 :goto_3

    :cond_15
    const-string p2, "Applets Drawer Closed"

    .line 127
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 128
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->currentApplet:Lcom/squareup/applet/Applet;

    if-eqz p1, :cond_16

    instance-of p1, p1, Lcom/squareup/ui/items/ItemsApplet;

    if-eqz p1, :cond_16

    .line 131
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_APPLET:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 132
    invoke-virtual {p0, v8, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_3

    .line 135
    :cond_16
    sget-object v1, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto :goto_3

    :pswitch_d
    if-nez p1, :cond_17

    goto :goto_3

    .line 83
    :cond_17
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p2

    sparse-switch p2, :sswitch_data_1

    goto :goto_3

    .line 85
    :sswitch_5
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    goto :goto_2

    :sswitch_6
    const-string p2, "Leaving Device or Feature Tour"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    goto :goto_2

    .line 98
    :sswitch_7
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 99
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->APPLETS_DRAWER:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {p0, p1}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 100
    iget-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletsDrawerRunner;->toggleDrawer()V

    goto :goto_3

    :sswitch_8
    const-string p2, "Start Tutorial"

    .line 85
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    goto :goto_2

    .line 103
    :sswitch_9
    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 104
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->skipTutorial()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v1

    goto :goto_3

    :sswitch_a
    const-string p2, "Dismissed EnterPasscodeToUnlockScreen"

    .line 85
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 92
    :goto_2
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->logStartTutorialEvent()V

    .line 93
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->logModalShowEvent()V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getFlow()Lflow/Flow;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler;->getDialogFactory()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->welcomeModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    move-result-object p2

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_18
    :goto_3
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x607d00cc -> :sswitch_4
        -0xb378286 -> :sswitch_3
        0x4eae60b -> :sswitch_2
        0x15f0cf50 -> :sswitch_1
        0x57aefb69 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x7fe47a36 -> :sswitch_a
        -0x45ba1ef6 -> :sswitch_9
        -0x305cba64 -> :sswitch_8
        -0x2a30eac4 -> :sswitch_7
        -0x154ab8ff -> :sswitch_6
        0x7cf2cba1 -> :sswitch_5
    .end sparse-switch
.end method
