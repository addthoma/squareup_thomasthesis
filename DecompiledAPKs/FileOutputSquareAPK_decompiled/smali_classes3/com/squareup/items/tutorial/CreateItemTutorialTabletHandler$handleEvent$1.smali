.class final Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateItemTutorialTabletHandler.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/tutorialv2/TutorialState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;


# direct methods
.method constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/tutorialv2/TutorialState;
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_SAVE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;->this$0:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 258
    sget-object v0, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;->invoke()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v0

    return-object v0
.end method
