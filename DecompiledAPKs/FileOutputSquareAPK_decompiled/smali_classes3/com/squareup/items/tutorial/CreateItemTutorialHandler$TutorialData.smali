.class public final Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;
.super Ljava/lang/Object;
.source "CreateItemTutorialHandler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/tutorial/CreateItemTutorialHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "TutorialData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\t\u0008\u0004\u0018\u00002\u00020\u0001B\'\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0002\u0010\u0007R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;",
        "",
        "state",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
        "eventName",
        "",
        "eventValue",
        "(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V",
        "getEventName",
        "()Ljava/lang/String;",
        "getEventValue",
        "()Ljava/lang/Object;",
        "getState",
        "()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final eventName:Ljava/lang/String;

.field private final eventValue:Ljava/lang/Object;

.field private final state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    iput-object p2, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->eventName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->eventValue:Ljava/lang/Object;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    .line 104
    sget-object p1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_1

    .line 105
    move-object p2, v0

    check-cast p2, Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    move-object p3, v0

    .line 106
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getEventName()Ljava/lang/String;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->eventName:Ljava/lang/String;

    return-object v0
.end method

.method public final getEventValue()Ljava/lang/Object;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->eventValue:Ljava/lang/Object;

    return-object v0
.end method

.method public final getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$TutorialData;->state:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-object v0
.end method
