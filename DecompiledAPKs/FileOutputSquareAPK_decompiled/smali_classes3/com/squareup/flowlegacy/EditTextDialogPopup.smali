.class public Lcom/squareup/flowlegacy/EditTextDialogPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "EditTextDialogPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field protected editor:Lcom/squareup/widgets/SelectableEditText;

.field protected root:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/flowlegacy/EditTextDialogPopup;)Ljava/lang/String;
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->getResult()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private getResult()Ljava/lang/String;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 72
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->createDialog(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 35
    invoke-static {p1}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->access$000(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)Ljava/lang/String;

    move-result-object p2

    .line 36
    invoke-static {p1}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->access$100(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)Ljava/lang/String;

    move-result-object v0

    .line 37
    invoke-static {p1}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->access$200(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)I

    move-result v1

    .line 38
    invoke-static {p1}, Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;->access$300(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;)I

    move-result p1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->getLayout()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->root:Landroid/view/View;

    .line 43
    iget-object v2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->root:Landroid/view/View;

    sget v3, Lcom/squareup/flowlegacy/R$id;->editor:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/widgets/SelectableEditText;

    iput-object v2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    .line 44
    iget-object v2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v2, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 46
    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    new-instance v0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;-><init>(Lcom/squareup/flowlegacy/EditTextDialogPopup;Lcom/squareup/mortar/PopupPresenter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SelectableEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    .line 60
    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    .line 63
    :cond_0
    new-instance p1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v0, Lcom/squareup/flowlegacy/-$$Lambda$EditTextDialogPopup$0AFKBj1Vw1eHCapn75zOeqZZ79E;

    invoke-direct {v0, p0, p3}, Lcom/squareup/flowlegacy/-$$Lambda$EditTextDialogPopup$0AFKBj1Vw1eHCapn75zOeqZZ79E;-><init>(Lcom/squareup/flowlegacy/EditTextDialogPopup;Lcom/squareup/mortar/PopupPresenter;)V

    .line 64
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->done:I

    new-instance v0, Lcom/squareup/flowlegacy/-$$Lambda$EditTextDialogPopup$yMXBRNVREvJNDo4wDN-PyLJVE3g;

    invoke-direct {v0, p0, p3}, Lcom/squareup/flowlegacy/-$$Lambda$EditTextDialogPopup$yMXBRNVREvJNDo4wDN-PyLJVE3g;-><init>(Lcom/squareup/flowlegacy/EditTextDialogPopup;Lcom/squareup/mortar/PopupPresenter;)V

    .line 68
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/flowlegacy/-$$Lambda$EditTextDialogPopup$YHbUolc7zOL5HrcxEsx0pZoYwZw;

    invoke-direct {p2, p3}, Lcom/squareup/flowlegacy/-$$Lambda$EditTextDialogPopup$YHbUolc7zOL5HrcxEsx0pZoYwZw;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 72
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->root:Landroid/view/View;

    .line 73
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p2

    const/4 p3, 0x4

    invoke-virtual {p2, p3}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object p1
.end method

.method protected getLayout()I
    .locals 1

    .line 87
    sget v0, Lcom/squareup/flowlegacy/R$layout;->editor_dialog:I

    return v0
.end method

.method public synthetic lambda$createDialog$0$EditTextDialogPopup(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p2, 0x0

    .line 65
    invoke-virtual {p1, p2}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$createDialog$1$EditTextDialogPopup(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->getResult()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method
