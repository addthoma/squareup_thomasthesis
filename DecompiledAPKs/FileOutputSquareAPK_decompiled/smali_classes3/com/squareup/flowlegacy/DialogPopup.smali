.class public abstract Lcom/squareup/flowlegacy/DialogPopup;
.super Ljava/lang/Object;
.source "DialogPopup.java"

# interfaces
.implements Lcom/squareup/mortar/Popup;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Landroid/os/Parcelable;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/mortar/Popup<",
        "TD;TR;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private dialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/flowlegacy/DialogPopup;->context:Landroid/content/Context;

    return-void
.end method

.method private getFragileDialogs()Lcom/squareup/ui/DialogDestroyer;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "TD;TR;>;)",
            "Landroid/app/Dialog;"
        }
    .end annotation
.end method

.method public dismiss()V
    .locals 2

    .line 45
    invoke-virtual {p0}, Lcom/squareup/flowlegacy/DialogPopup;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 46
    :cond_0
    invoke-direct {p0}, Lcom/squareup/flowlegacy/DialogPopup;->getFragileDialogs()Lcom/squareup/ui/DialogDestroyer;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DialogDestroyer;->dismiss(Landroid/app/Dialog;)V

    const/4 v0, 0x0

    .line 47
    iput-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->context:Landroid/content/Context;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {v0, p1}, Landroid/app/Dialog;->onRestoreInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Bundle;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public show(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "TD;TR;>;)V"
        }
    .end annotation

    .line 27
    invoke-virtual {p0}, Lcom/squareup/flowlegacy/DialogPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/squareup/flowlegacy/DialogPopup;->getFragileDialogs()Lcom/squareup/ui/DialogDestroyer;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DialogDestroyer;->dismiss(Landroid/app/Dialog;)V

    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    .line 36
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/flowlegacy/DialogPopup;->createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    .line 37
    invoke-direct {p0}, Lcom/squareup/flowlegacy/DialogPopup;->getFragileDialogs()Lcom/squareup/ui/DialogDestroyer;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/flowlegacy/DialogPopup;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/DialogDestroyer;->show(Landroid/app/Dialog;)V

    return-void
.end method
