.class final synthetic Lcom/squareup/dagger/Components$findComponent$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "Components.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/dagger/Components;->findComponent(Lmortar/MortarScope;Ljava/lang/Class;Z)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lmortar/MortarScope;",
        "Lmortar/MortarScope;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u00042,\u0010\u0005\u001a( \u0002*\u0013\u0018\u00010\u0001\u00a2\u0006\u000c\u0008\u0006\u0012\u0008\u0008\u0007\u0012\u0004\u0008\u0008(\u00080\u0001\u00a2\u0006\u000c\u0008\u0006\u0012\u0008\u0008\u0007\u0012\u0004\u0008\u0008(\u0008\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lmortar/MortarScope;",
        "kotlin.jvm.PlatformType",
        "T",
        "",
        "p1",
        "Lkotlin/ParameterName;",
        "name",
        "mortarScope",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/dagger/Components$findComponent$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/dagger/Components$findComponent$1;

    invoke-direct {v0}, Lcom/squareup/dagger/Components$findComponent$1;-><init>()V

    sput-object v0, Lcom/squareup/dagger/Components$findComponent$1;->INSTANCE:Lcom/squareup/dagger/Components$findComponent$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "parentScope"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lmortar/ScopeSpy;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "parentScope(Lmortar/MortarScope;)Lmortar/MortarScope;"

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lmortar/MortarScope;

    invoke-virtual {p0, p1}, Lcom/squareup/dagger/Components$findComponent$1;->invoke(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lmortar/MortarScope;)Lmortar/MortarScope;
    .locals 0

    .line 142
    invoke-static {p1}, Lmortar/ScopeSpy;->parentScope(Lmortar/MortarScope;)Lmortar/MortarScope;

    move-result-object p1

    return-object p1
.end method
