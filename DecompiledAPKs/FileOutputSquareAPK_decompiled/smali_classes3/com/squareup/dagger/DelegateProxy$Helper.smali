.class public Lcom/squareup/dagger/DelegateProxy$Helper;
.super Ljava/lang/Object;
.source "DelegateProxy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/dagger/DelegateProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Helper"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/dagger/DelegateProxy$Helper$DelegateInvocationHandler;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static proxyDelegate(Ljava/lang/Class;)Lcom/squareup/dagger/DelegateProxy;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/dagger/DelegateProxy<",
            "TT;>;"
        }
    .end annotation

    .line 24
    new-instance v0, Lcom/squareup/dagger/DelegateProxy$Helper$DelegateInvocationHandler;

    invoke-direct {v0, p0}, Lcom/squareup/dagger/DelegateProxy$Helper$DelegateInvocationHandler;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method
