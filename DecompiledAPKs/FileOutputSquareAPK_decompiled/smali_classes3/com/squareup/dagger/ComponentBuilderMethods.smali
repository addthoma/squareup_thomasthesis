.class final Lcom/squareup/dagger/ComponentBuilderMethods;
.super Ljava/lang/Object;
.source "Components.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0008\u0008\u0002\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0007R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/dagger/ComponentBuilderMethods;",
        "",
        "builder",
        "Ljava/lang/reflect/Method;",
        "config",
        "",
        "build",
        "(Ljava/lang/reflect/Method;Ljava/util/List;Ljava/lang/reflect/Method;)V",
        "getBuild",
        "()Ljava/lang/reflect/Method;",
        "getBuilder",
        "getConfig",
        "()Ljava/util/List;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final build:Ljava/lang/reflect/Method;

.field private final builder:Ljava/lang/reflect/Method;

.field private final config:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;Ljava/util/List;Ljava/lang/reflect/Method;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "Ljava/util/List<",
            "Ljava/lang/reflect/Method;",
            ">;",
            "Ljava/lang/reflect/Method;",
            ")V"
        }
    .end annotation

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "build"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/dagger/ComponentBuilderMethods;->builder:Ljava/lang/reflect/Method;

    iput-object p2, p0, Lcom/squareup/dagger/ComponentBuilderMethods;->config:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/dagger/ComponentBuilderMethods;->build:Ljava/lang/reflect/Method;

    return-void
.end method


# virtual methods
.method public final getBuild()Ljava/lang/reflect/Method;
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/squareup/dagger/ComponentBuilderMethods;->build:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public final getBuilder()Ljava/lang/reflect/Method;
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/dagger/ComponentBuilderMethods;->builder:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public final getConfig()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation

    .line 321
    iget-object v0, p0, Lcom/squareup/dagger/ComponentBuilderMethods;->config:Ljava/util/List;

    return-object v0
.end method
