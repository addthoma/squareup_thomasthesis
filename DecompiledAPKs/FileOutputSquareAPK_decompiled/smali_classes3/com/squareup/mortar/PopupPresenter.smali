.class public abstract Lcom/squareup/mortar/PopupPresenter;
.super Lmortar/Presenter;
.source "PopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Landroid/os/Parcelable;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Lmortar/Presenter<",
        "Lcom/squareup/mortar/Popup<",
        "TD;TR;>;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final WITH_FLOURISH:Z = true


# instance fields
.field private whatToShow:Landroid/os/Parcelable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private final whatToShowKey:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const-string v0, ""

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/mortar/PopupPresenter;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 31
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShowKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 61
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    .line 63
    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->dismiss()V

    :cond_1
    return-void
.end method

.method public dropView(Lcom/squareup/mortar/Popup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "TD;TR;>;)V"
        }
    .end annotation

    .line 107
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    .line 113
    instance-of v0, p1, Lcom/squareup/flowlegacy/DialogPopup;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->dismiss()V

    :cond_0
    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "TD;TR;>;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    .line 82
    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public isShowing()Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final onDismissed(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 69
    iput-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onResult(Ljava/lang/Object;)V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 87
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShowKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 90
    :cond_0
    iget-object p1, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-nez p1, :cond_1

    return-void

    .line 92
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->hasView()Z

    move-result p1

    if-nez p1, :cond_2

    return-void

    .line 93
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/mortar/Popup;

    .line 95
    invoke-interface {p1}, Lcom/squareup/mortar/Popup;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 96
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1, p0}, Lcom/squareup/mortar/Popup;->show(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)V

    :cond_3
    return-void
.end method

.method protected abstract onPopupResult(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation
.end method

.method public final onResult(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .line 76
    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onPopupResult(Ljava/lang/Object;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShowKey:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public show(Landroid/os/Parcelable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    if-eq v0, p1, :cond_2

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    iput-object p1, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    .line 55
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->hasView()Z

    move-result p1

    if-nez p1, :cond_1

    return-void

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/mortar/PopupPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/mortar/Popup;

    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1, p0}, Lcom/squareup/mortar/Popup;->show(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public showing()Landroid/os/Parcelable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/mortar/PopupPresenter;->whatToShow:Landroid/os/Parcelable;

    return-object v0
.end method
