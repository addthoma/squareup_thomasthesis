.class public final Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "NotificationCenterAppletScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$ParentComponent;,
        Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationCenterAppletScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationCenterAppletScope.kt\ncom/squareup/notificationcenter/applet/NotificationCenterAppletScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,44:1\n35#2:45\n35#2:46\n24#3,4:47\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationCenterAppletScope.kt\ncom/squareup/notificationcenter/applet/NotificationCenterAppletScope\n*L\n17#1:45\n22#1:46\n42#1,4:47\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nH\u0016R\u001c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0006\u0010\u0003\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "register",
        "",
        "scope",
        "Component",
        "ParentComponent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;

    invoke-direct {v0}, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;->INSTANCE:Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;

    .line 47
    new-instance v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 50
    sput-object v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 46
    const-class v1, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 23
    check-cast p1, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$ParentComponent;

    .line 25
    invoke-interface {p1}, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$ParentComponent;->notificationCenterWorkflowRunner()Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;

    move-result-object p1

    const-string v1, "scopeBuilder"

    .line 26
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/notificationcenterlauncher/NotificationCenterWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026vices(scopeBuilder)\n    }"

    .line 22
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    const-class v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 17
    check-cast v0, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$Component;

    .line 18
    invoke-interface {v0}, Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScope$Component;->notificationsAppletScopeRunner()Lcom/squareup/notificationcenter/applet/NotificationCenterAppletScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
