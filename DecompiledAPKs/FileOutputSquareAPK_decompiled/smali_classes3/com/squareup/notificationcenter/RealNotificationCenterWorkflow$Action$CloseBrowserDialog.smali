.class public final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;
.super Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
.source "RealNotificationCenterWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CloseBrowserDialog"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealNotificationCenterWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealNotificationCenterWorkflow.kt\ncom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog\n*L\n1#1,366:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0014\u0010\u001b\u001a\u0004\u0018\u00010\u001c*\u0008\u0012\u0004\u0012\u00020\u001e0\u001dH\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;",
        "Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;",
        "currentState",
        "Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;",
        "selectedNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "analytics",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V",
        "getAnalytics",
        "()Lcom/squareup/notificationcenter/NotificationCenterAnalytics;",
        "getCurrentState",
        "()Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;",
        "getSelectedNotification",
        "()Lcom/squareup/notificationcenterdata/Notification;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

.field private final currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

.field private final selectedNotification:Lcom/squareup/notificationcenterdata/Notification;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V
    .locals 1

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedNotification"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 296
    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    iput-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;ILjava/lang/Object;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->copy(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/notificationcenter/NotificationCenterOutput;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            ">;)",
            "Lcom/squareup/notificationcenter/NotificationCenterOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    invoke-interface {v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterAnalytics;->logWebBrowserDialogCanceled(Lcom/squareup/notificationcenterdata/Notification;)V

    .line 299
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    .line 300
    new-instance v1, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;

    .line 301
    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getImportantNotifications()Ljava/util/List;

    move-result-object v2

    .line 302
    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getGeneralNotifications()Ljava/util/List;

    move-result-object v3

    .line 303
    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v4

    .line 304
    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getShowError()Z

    move-result v0

    .line 300
    invoke-direct {v1, v2, v3, v4, v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingNotificationsState;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/notificationcenter/NotificationCenterTab;Z)V

    .line 299
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 292
    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/notificationcenter/NotificationCenterOutput;

    move-result-object p1

    return-object p1
.end method

.method public final component1()Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    return-object v0
.end method

.method public final component2()Lcom/squareup/notificationcenterdata/Notification;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    return-object v0
.end method

.method public final component3()Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-object v0
.end method

.method public final copy(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;
    .locals 1

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedNotification"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;-><init>(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    iget-object v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    iget-object v1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    iget-object p1, p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnalytics()Lcom/squareup/notificationcenter/NotificationCenterAnalytics;
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    return-object v0
.end method

.method public final getCurrentState()Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;
    .locals 1

    .line 293
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    return-object v0
.end method

.method public final getSelectedNotification()Lcom/squareup/notificationcenterdata/Notification;
    .locals 1

    .line 294
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CloseBrowserDialog(currentState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->currentState:Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedNotification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->selectedNotification:Lcom/squareup/notificationcenterdata/Notification;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;->analytics:Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
