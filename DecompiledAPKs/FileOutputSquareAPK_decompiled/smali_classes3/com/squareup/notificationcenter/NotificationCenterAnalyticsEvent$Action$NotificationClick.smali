.class public final Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;
.super Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
.source "RealNotificationCenterAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NotificationClick"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0015\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001b\u001a\u00020\tH\u00c6\u0003JO\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\t2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0011\u0010\n\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\r\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;",
        "notification_id",
        "",
        "client_action",
        "url",
        "tab",
        "source",
        "opened_internally",
        "",
        "opened_externally",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V",
        "getClient_action",
        "()Ljava/lang/String;",
        "getNotification_id",
        "getOpened_externally",
        "()Z",
        "getOpened_internally",
        "getSource",
        "getTab",
        "getUrl",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final client_action:Ljava/lang/String;

.field private final notification_id:Ljava/lang/String;

.field private final opened_externally:Z

.field private final opened_internally:Z

.field private final source:Ljava/lang/String;

.field private final tab:Ljava/lang/String;

.field private final url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2

    const-string v0, "notification_id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client_action"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "url"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tab"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_NOTIFICATION_CLICK:Lcom/squareup/analytics/RegisterActionName;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;-><init>(Lcom/squareup/analytics/RegisterActionName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    iput-boolean p7, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILjava/lang/Object;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    :cond_5
    move v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-boolean p7, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    :cond_6
    move v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    move p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;
    .locals 9

    const-string v0, "notification_id"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "client_action"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "url"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tab"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "source"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;

    move-object v1, v0

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    iget-boolean v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    iget-boolean p1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getClient_action()Ljava/lang/String;
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    return-object v0
.end method

.method public final getNotification_id()Ljava/lang/String;
    .locals 1

    .line 195
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    return-object v0
.end method

.method public final getOpened_externally()Z
    .locals 1

    .line 201
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    return v0
.end method

.method public final getOpened_internally()Z
    .locals 1

    .line 200
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    return v0
.end method

.method public final getSource()Ljava/lang/String;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    return-object v0
.end method

.method public final getTab()Ljava/lang/String;
    .locals 1

    .line 198
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NotificationClick(notification_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->notification_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", client_action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->client_action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tab="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->tab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->source:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", opened_internally="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_internally:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", opened_externally="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$NotificationClick;->opened_externally:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
