.class public final Lcom/squareup/marin/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final borders:I = 0x7f040068

.field public static final buttonText:I = 0x7f04008a

.field public static final button_text:I = 0x7f04008e

.field public static final chevronColor:I = 0x7f0400a8

.field public static final chevronVisibility:I = 0x7f0400a9

.field public static final dividerOffsetFromTop:I = 0x7f04012e

.field public static final dividerWidth:I = 0x7f040131

.field public static final equalizeLines:I = 0x7f04015f

.field public static final failureGlyph:I = 0x7f04017c

.field public static final failureGlyphColor:I = 0x7f04017d

.field public static final glyph:I = 0x7f0401a0

.field public static final glyphColor:I = 0x7f0401a1

.field public static final glyphFontSizeOverride:I = 0x7f0401a2

.field public static final glyphPosition:I = 0x7f0401a3

.field public static final glyphSaveEnabled:I = 0x7f0401a4

.field public static final glyphShadowColor:I = 0x7f0401a5

.field public static final glyphShadowDx:I = 0x7f0401a6

.field public static final glyphShadowDy:I = 0x7f0401a7

.field public static final glyphShadowRadius:I = 0x7f0401a8

.field public static final glyphVisible:I = 0x7f0401ab

.field public static final helperText:I = 0x7f0401b3

.field public static final indicatorDiameter:I = 0x7f04020d

.field public static final indicatorHighlightColor:I = 0x7f04020f

.field public static final indicatorMatteColor:I = 0x7f040210

.field public static final indicatorSpacing:I = 0x7f040211

.field public static final marinActionBarPrimaryButtonStyle:I = 0x7f040295

.field public static final marinActionBarSecondaryButtonStyle:I = 0x7f040296

.field public static final marinActionBarStyle:I = 0x7f040297

.field public static final marinActionBarTheme:I = 0x7f040298

.field public static final marinActionBarUpButtonStyle:I = 0x7f040299

.field public static final marinBorderWidth:I = 0x7f04029a

.field public static final marinButtonBackground:I = 0x7f04029b

.field public static final marinButtonTextColor:I = 0x7f04029c

.field public static final marinGlyphMessageTitleColor:I = 0x7f04029e

.field public static final marinGlyphTextViewStyle:I = 0x7f04029f

.field public static final marinOrderEntryTabButton:I = 0x7f0402a0

.field public static final marinSpinnerGlyphStyle:I = 0x7f0402a1

.field public static final message:I = 0x7f0402bf

.field public static final messageColor:I = 0x7f0402c0

.field public static final pointingDown:I = 0x7f040315

.field public static final ratio:I = 0x7f040329

.field public static final sq_glyphColor:I = 0x7f0403c4

.field public static final transitionDuration:I = 0x7f040479


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
