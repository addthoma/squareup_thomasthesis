.class Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;
.super Ljava/lang/Object;
.source "MarinVerticalCaretDrawable.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .line 132
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p1

    .line 133
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-static {v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$000(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    const v3, 0x3ed70a3d    # 0.42f

    const v4, 0x3f147ae1    # 0.58f

    invoke-static {v2, p1, v4, v3}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$100(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;FFF)F

    move-result v2

    mul-float v1, v1, v2

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    .line 134
    invoke-static {v2}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$000(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;)I

    move-result v2

    int-to-float v2, v2

    iget-object v5, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    const v6, 0x3eae147b    # 0.34f

    const v7, 0x3f28f5c3    # 0.66f

    invoke-static {v5, p1, v6, v7}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$100(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;FFF)F

    move-result v5

    mul-float v2, v2, v5

    iget-object v5, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    .line 135
    invoke-static {v5}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$000(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;)I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-static {v6, p1, v4, v3}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$100(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;FFF)F

    move-result p1

    mul-float v5, v5, p1

    .line 133
    invoke-static {v0, v1, v2, v5}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->access$200(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;FFF)V

    .line 136
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;->this$0:Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->invalidateSelf()V

    return-void
.end method
