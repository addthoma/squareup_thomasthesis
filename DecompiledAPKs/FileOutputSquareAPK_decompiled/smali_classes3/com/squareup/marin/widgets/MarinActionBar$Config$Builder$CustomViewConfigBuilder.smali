.class Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CustomViewConfigBuilder"
.end annotation


# instance fields
.field private command:Ljava/lang/Runnable;

.field private customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

.field private enabled:Z

.field private tooltip:Ljava/lang/CharSequence;

.field private visible:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 654
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->enabled:Z

    const/4 v0, 0x0

    .line 655
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->visible:Z

    return-void
.end method

.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;)V
    .locals 1

    .line 660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 654
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->enabled:Z

    const/4 v0, 0x0

    .line 655
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->visible:Z

    .line 661
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    .line 662
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->tooltip:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->tooltip:Ljava/lang/CharSequence;

    .line 663
    iget-object v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->command:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->command:Ljava/lang/Runnable;

    .line 664
    iget-boolean v0, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->enabled:Z

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->enabled:Z

    .line 665
    iget-boolean p1, p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->visible:Z

    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->visible:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Z
    .locals 0

    .line 650
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->enabled:Z

    return p0
.end method

.method static synthetic access$1002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Z)Z
    .locals 0

    .line 650
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->enabled:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Z
    .locals 0

    .line 650
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->visible:Z

    return p0
.end method

.method static synthetic access$1102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Z)Z
    .locals 0

    .line 650
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->visible:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;
    .locals 0

    .line 650
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    return-object p0
.end method

.method static synthetic access$1202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;)Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    return-object p1
.end method

.method static synthetic access$800(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Ljava/lang/CharSequence;
    .locals 0

    .line 650
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->tooltip:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$802(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->tooltip:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$900(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Ljava/lang/Runnable;
    .locals 0

    .line 650
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->command:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->command:Ljava/lang/Runnable;

    return-object p1
.end method
