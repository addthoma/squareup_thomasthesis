.class public Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "MarinVerticalCaretDrawable.java"


# static fields
.field private static final DOWN_Y_ENDS:F = 0.42f

.field private static final DOWN_Y_TIP:F = 0.66f

.field private static final FLIP_DURATION_MS:I = 0xc8

.field private static final LEFT_X:F = 0.26f

.field private static final RIGHT_X:F = 0.74f

.field private static final TIP_X:F = 0.5f

.field private static final UP_Y_ENDS:F = 0.58f

.field private static final UP_Y_TIP:F = 0.34f


# instance fields
.field private animator:Landroid/animation/ValueAnimator;

.field private arrow:Landroid/graphics/Path;

.field private final dimen:I

.field private final disabledColor:I

.field private final enabledColor:I

.field private final linePaint:Landroid/graphics/Paint;

.field private pointingDown:Z


# direct methods
.method public constructor <init>(ZLandroid/content/res/Resources;)V
    .locals 3

    .line 39
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 40
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    .line 41
    sget v0, Lcom/squareup/marin/R$dimen;->glyph_vertical_caret:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 43
    new-instance v0, Landroid/graphics/Paint;

    const/16 v1, 0x81

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    .line 44
    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->enabledColor:I

    .line 45
    sget v0, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->disabledColor:I

    .line 46
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->enabledColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    sget v1, Lcom/squareup/glyph/R$dimen;->glyph_stroke_width:I

    .line 50
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float p2, p2

    .line 49
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 51
    new-instance p2, Landroid/graphics/Path;

    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    iput-object p2, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->arrow:Landroid/graphics/Path;

    if-eqz p1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrowDown()V

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrowUp()V

    :goto_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;)I
    .locals 0

    .line 19
    iget p0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;FFF)F
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->interpolate(FFF)F

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;FFF)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrow(FFF)V

    return-void
.end method

.method private animate()V
    .locals 3

    .line 127
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->cancelAnimator()V

    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 128
    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    .line 129
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 130
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;

    invoke-direct {v1, p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable$1;-><init>(Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private cancelAnimator()V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method private interpolate(FFF)F
    .locals 2

    .line 160
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move v1, p3

    move p3, p2

    move p2, v1

    :goto_0
    sub-float/2addr p2, p3

    mul-float p2, p2, p1

    add-float/2addr p2, p3

    return p2
.end method

.method private setArrow(FFF)V
    .locals 3

    .line 144
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->arrow:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 146
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->arrow:Landroid/graphics/Path;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    int-to-float v1, v1

    const v2, 0x3e851eb8    # 0.26f

    mul-float v1, v1, v2

    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 148
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->arrow:Landroid/graphics/Path;

    iget v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float v0, v0, v1

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 150
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->arrow:Landroid/graphics/Path;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    int-to-float p2, p2

    const v0, 0x3f3d70a4    # 0.74f

    mul-float p2, p2, v0

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Path;->lineTo(FF)V

    return-void
.end method


# virtual methods
.method public animateArrowDown()V
    .locals 1

    const/4 v0, 0x1

    .line 118
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    .line 119
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animate()V

    return-void
.end method

.method public animateArrowUp()V
    .locals 1

    const/4 v0, 0x0

    .line 112
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    .line 113
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animate()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->arrow:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isPointingDown()Z
    .locals 1

    .line 123
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    return v0
.end method

.method public setAlpha(I)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->invalidateSelf()V

    return-void
.end method

.method public setArrowDown()V
    .locals 5

    .line 98
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->cancelAnimator()V

    .line 99
    iget v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    int-to-float v1, v0

    const v2, 0x3ed70a3d    # 0.42f

    mul-float v1, v1, v2

    int-to-float v3, v0

    const v4, 0x3f28f5c3    # 0.66f

    mul-float v3, v3, v4

    int-to-float v0, v0

    mul-float v0, v0, v2

    invoke-direct {p0, v1, v3, v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrow(FFF)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->invalidateSelf()V

    return-void
.end method

.method public setArrowUp()V
    .locals 5

    .line 105
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->cancelAnimator()V

    .line 106
    iget v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->dimen:I

    int-to-float v1, v0

    const v2, 0x3f147ae1    # 0.58f

    mul-float v1, v1, v2

    int-to-float v3, v0

    const v4, 0x3eae147b    # 0.34f

    mul-float v3, v3, v4

    int-to-float v0, v0

    mul-float v0, v0, v2

    invoke-direct {p0, v1, v3, v0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->setArrow(FFF)V

    .line 107
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->invalidateSelf()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->linePaint:Landroid/graphics/Paint;

    if-eqz p1, :cond_0

    iget p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->enabledColor:I

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->disabledColor:I

    :goto_0
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->invalidateSelf()V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 92
    iget-boolean v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->pointingDown:Z

    .line 93
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinVerticalCaretDrawable;->animate()V

    return-void
.end method
