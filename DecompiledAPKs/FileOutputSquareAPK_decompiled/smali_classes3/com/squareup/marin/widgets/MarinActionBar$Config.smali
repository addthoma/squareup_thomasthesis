.class public Lcom/squareup/marin/widgets/MarinActionBar$Config;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/widgets/MarinActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;,
        Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;,
        Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;
    }
.end annotation


# instance fields
.field public final background:I

.field public final canShowBadges:Z

.field public final contentDescription:Ljava/lang/String;

.field public final customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;

.field public final fourthButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

.field public final hidden:Z

.field public final primaryButtonCommand:Ljava/lang/Runnable;

.field public final primaryButtonEnabled:Z

.field public final primaryButtonText:Ljava/lang/CharSequence;

.field public final primaryButtonVisible:Z

.field public final secondaryButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

.field public final thirdButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

.field public final upButtonCentered:Z

.field public final upButtonCommand:Ljava/lang/Runnable;

.field public final upButtonEnabled:Z

.field public final upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final upButtonGlyphColor:I

.field public final upButtonRightPadded:Z

.field public final upButtonScalable:Z

.field public final upButtonText:Ljava/lang/CharSequence;

.field public final upButtonTextColor:I

.field public final upButtonTooltip:Ljava/lang/CharSequence;

.field public final upButtonVisible:Z


# direct methods
.method private constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V
    .locals 2

    .line 538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->hidden:Z

    .line 541
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->background:I

    .line 542
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->contentDescription:Ljava/lang/String;

    .line 544
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1600(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->canShowBadges:Z

    .line 546
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1700(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 547
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1800(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyphColor:I

    .line 548
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1900(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonText:Ljava/lang/CharSequence;

    .line 549
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTextColor:I

    .line 550
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTooltip:Ljava/lang/CharSequence;

    .line 551
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonCommand:Ljava/lang/Runnable;

    .line 552
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonVisible:Z

    .line 553
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonEnabled:Z

    .line 554
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonScalable:Z

    .line 555
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2600(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonRightPadded:Z

    .line 556
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2700(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonCentered:Z

    .line 558
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2800(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonText:Ljava/lang/CharSequence;

    .line 559
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2900(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonCommand:Ljava/lang/Runnable;

    .line 560
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonEnabled:Z

    .line 561
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonVisible:Z

    .line 563
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->secondaryButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    .line 564
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->thirdButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    .line 565
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->fourthButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    .line 566
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;

    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$1;)V
    .locals 0

    .line 466
    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 3

    .line 570
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 572
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->hidden:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 574
    iget v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->background:I

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;I)I

    .line 575
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->contentDescription:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/String;)Ljava/lang/String;

    .line 577
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->canShowBadges:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 579
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1702(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 580
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$1902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 581
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonTooltip:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 582
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonCommand:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 583
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 584
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonEnabled:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 585
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->upButtonScalable:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 587
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonText:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2802(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 588
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonCommand:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$2902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 589
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonEnabled:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 590
    iget-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->primaryButtonVisible:Z

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z

    .line 592
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->secondaryButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    invoke-direct {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;)V

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    .line 594
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->thirdButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    invoke-direct {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;)V

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    .line 595
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->fourthButtonConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;

    invoke-direct {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;)V

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    .line 596
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;

    invoke-direct {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;)V

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->access$3502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    return-object v0
.end method
