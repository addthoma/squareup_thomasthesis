.class public Lcom/squareup/marin/widgets/MarinActionBarView;
.super Lcom/squareup/marin/widgets/MarinBaseActionBarView;
.source "MarinActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinActionBarView$Component;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field presenter:Lcom/squareup/marin/widgets/MarinActionBar;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/MarinBaseActionBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/marin/widgets/MarinActionBarView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinActionBarView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/marin/widgets/MarinActionBarView$Component;->inject(Lcom/squareup/marin/widgets/MarinActionBarView;)V

    return-void
.end method


# virtual methods
.method protected getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBarView;->presenter:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method
