.class public Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;,
        Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    }
.end annotation


# instance fields
.field private background:I

.field private button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

.field private canShowBadges:Z

.field private contentDescription:Ljava/lang/String;

.field private customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

.field private hidden:Z

.field private primaryButtonCommand:Ljava/lang/Runnable;

.field private primaryButtonEnabled:Z

.field private primaryButtonText:Ljava/lang/CharSequence;

.field private primaryButtonVisible:Z

.field private secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

.field private thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

.field private upButtonCentered:Z

.field private upButtonCommand:Ljava/lang/Runnable;

.field private upButtonEnabled:Z

.field private upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private upButtonGlyphColor:I

.field private upButtonScalable:Z

.field private upButtonText:Ljava/lang/CharSequence;

.field private upButtonTextColor:I

.field private upButtonTextRightPadded:Z

.field private upButtonTooltip:Ljava/lang/CharSequence;

.field private upButtonVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 604
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->canShowBadges:Z

    .line 606
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 612
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonEnabled:Z

    .line 614
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    .line 615
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTextRightPadded:Z

    const/4 v1, 0x0

    .line 616
    iput-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCentered:Z

    .line 622
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonEnabled:Z

    .line 623
    iput-boolean v1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonVisible:Z

    .line 669
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    .line 670
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    .line 671
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    .line 672
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;-><init>()V

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    return-void
.end method

.method static synthetic access$1300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidden:Z

    return p0
.end method

.method static synthetic access$1302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidden:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)I
    .locals 0

    .line 600
    iget p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->background:I

    return p0
.end method

.method static synthetic access$1402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;I)I
    .locals 0

    .line 600
    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->background:I

    return p1
.end method

.method static synthetic access$1500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/String;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->contentDescription:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->contentDescription:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->canShowBadges:Z

    return p0
.end method

.method static synthetic access$1602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->canShowBadges:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method static synthetic access$1702(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)I
    .locals 0

    .line 600
    iget p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyphColor:I

    return p0
.end method

.method static synthetic access$1900(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$1902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$2000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)I
    .locals 0

    .line 600
    iget p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTextColor:I

    return p0
.end method

.method static synthetic access$2100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTooltip:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$2102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTooltip:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/Runnable;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$2202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    return p0
.end method

.method static synthetic access$2302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonEnabled:Z

    return p0
.end method

.method static synthetic access$2402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonEnabled:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonScalable:Z

    return p0
.end method

.method static synthetic access$2502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonScalable:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTextRightPadded:Z

    return p0
.end method

.method static synthetic access$2700(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCentered:Z

    return p0
.end method

.method static synthetic access$2800(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$2802(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonText:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Ljava/lang/Runnable;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$2902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$3000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonEnabled:Z

    return p0
.end method

.method static synthetic access$3002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonEnabled:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Z
    .locals 0

    .line 600
    iget-boolean p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonVisible:Z

    return p0
.end method

.method static synthetic access$3102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Z)Z
    .locals 0

    .line 600
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonVisible:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    return-object p0
.end method

.method static synthetic access$3202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    return-object p0
.end method

.method static synthetic access$3302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    return-object p0
.end method

.method static synthetic access$3402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;
    .locals 0

    .line 600
    iget-object p0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    return-object p0
.end method

.method static synthetic access$3502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    return-object p1
.end method


# virtual methods
.method public build()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 948
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;-><init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Lcom/squareup/marin/widgets/MarinActionBar$1;)V

    return-object v0
.end method

.method public hide()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 943
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidden:Z

    return-object p0
.end method

.method public hideButton4()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 900
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    .line 901
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public hideCustomView()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 920
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Z)Z

    .line 921
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 803
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonVisible:Z

    const/4 v0, 0x0

    .line 804
    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 855
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    .line 856
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public hideThirdButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 873
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    .line 874
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 770
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    const/4 v0, 0x0

    .line 771
    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public setBackground(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 686
    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->background:I

    return-object p0
.end method

.method public setButton4GlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 906
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 907
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 908
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setCanShowBadges(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 679
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->canShowBadges:Z

    return-object p0
.end method

.method public setContentDescription(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 691
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->contentDescription:Ljava/lang/String;

    return-object p0
.end method

.method public setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 926
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;)Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    .line 927
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    invoke-static {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$802(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setCustomViewEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 932
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Z)Z

    return-object p0
.end method

.method public setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 809
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonEnabled:Z

    return-object p0
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 792
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setSecondaryButtonBackgroundColor(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 844
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$702(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;I)I

    return-object p0
.end method

.method public setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 820
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    return-object p0
.end method

.method public setSecondaryButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 825
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 826
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 827
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setSecondaryButtonTextColors(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 839
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$202(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;I)I

    return-object p0
.end method

.method public setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 832
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 833
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 834
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setThirdButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 862
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$502(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    return-object p0
.end method

.method public setThirdButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 886
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 887
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 888
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setThirdButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 879
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    .line 880
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$002(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 881
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$302(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 706
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 711
    invoke-virtual {p0, v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 776
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonEnabled:Z

    return-object p0
.end method

.method public setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 715
    iput-object p2, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    .line 716
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public setUpButtonGlyphColor(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 701
    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyphColor:I

    return-object p0
.end method

.method public setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 751
    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    .line 752
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 753
    iput-object p2, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTooltip:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setUpButtonScalable(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 814
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonScalable:Z

    return-object p0
.end method

.method public setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 726
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    .line 727
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 p1, 0x1

    .line 728
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    return-object p0
.end method

.method public setUpButtonTextBackArrow(ZLjava/lang/CharSequence;Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 742
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 743
    invoke-virtual {p0, p3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 745
    :cond_0
    invoke-virtual {p0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setTitleNoUpButton(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    :goto_0
    return-object p0
.end method

.method public setUpButtonTextCentered(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 786
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCentered:Z

    return-object p0
.end method

.method public setUpButtonTextColor(I)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 696
    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTextColor:I

    return-object p0
.end method

.method public setUpButtonTextRightPadded(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 781
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonTextRightPadded:Z

    return-object p0
.end method

.method public setUpButtonTextX(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 733
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    .line 734
    sget-object p1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 p1, 0x1

    .line 735
    iput-boolean p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    return-object p0
.end method

.method public show()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x0

    .line 938
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidden:Z

    return-object p0
.end method

.method public showButton4(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 894
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    .line 895
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->button4Config:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 914
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1102(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Z)Z

    .line 915
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->customViewConfig:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$902(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 797
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonVisible:Z

    .line 798
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 849
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    .line 850
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->secondaryButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public showThirdButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 2

    .line 867
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$602(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Z)Z

    .line 868
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->thirdButtonConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;

    invoke-static {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$402(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-object p0
.end method

.method public showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 758
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    .line 759
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public showUpButtonNotClickable()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 764
    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonVisible:Z

    const/4 v0, 0x0

    .line 765
    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public updateUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 0

    .line 721
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method
