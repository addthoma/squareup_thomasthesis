.class public interface abstract Lcom/squareup/intermission/IntermissionHelper;
.super Ljava/lang/Object;
.source "IntermissionHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/intermission/IntermissionHelper$DefaultImpls;,
        Lcom/squareup/intermission/IntermissionHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fJ\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0006\u0010\t\u001a\u00020\nH&J\u001a\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/intermission/IntermissionHelper;",
        "",
        "getGapTimeInfo",
        "Lcom/squareup/intermission/GapTimeInfo;",
        "variation",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        "intermissions",
        "",
        "Lcom/squareup/api/items/Intermission;",
        "duration",
        "Lorg/threeten/bp/Duration;",
        "startDurationPicker",
        "",
        "allowZeroDuration",
        "",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/intermission/IntermissionHelper$Companion;

.field public static final MIN_SERVICE_DURATION_WITH_GAP_TIME:J = 0xfL


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/intermission/IntermissionHelper$Companion;->$$INSTANCE:Lcom/squareup/intermission/IntermissionHelper$Companion;

    sput-object v0, Lcom/squareup/intermission/IntermissionHelper;->Companion:Lcom/squareup/intermission/IntermissionHelper$Companion;

    return-void
.end method


# virtual methods
.method public abstract getGapTimeInfo(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/intermission/GapTimeInfo;
.end method

.method public abstract getGapTimeInfo(Ljava/util/List;Lorg/threeten/bp/Duration;)Lcom/squareup/intermission/GapTimeInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;",
            "Lorg/threeten/bp/Duration;",
            ")",
            "Lcom/squareup/intermission/GapTimeInfo;"
        }
    .end annotation
.end method

.method public abstract startDurationPicker(Lorg/threeten/bp/Duration;Z)J
.end method
