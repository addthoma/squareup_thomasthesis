.class final Lcom/squareup/mailorder/OrderCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderCoordinator.kt\ncom/squareup/mailorder/OrderCoordinator$attach$1\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/RxKotlinKt\n*L\n1#1,259:1\n505#2:260\n*E\n*S KotlinDebug\n*F\n+ 1 OrderCoordinator.kt\ncom/squareup/mailorder/OrderCoordinator$attach$1\n*L\n127#1:260\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderCoordinator$attach$1;->this$0:Lcom/squareup/mailorder/OrderCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator$attach$1;->this$0:Lcom/squareup/mailorder/OrderCoordinator;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderCoordinator;->access$getPhone$p(Lcom/squareup/mailorder/OrderCoordinator;)Lcom/squareup/noho/NohoEditText;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnEditorAction(Landroid/widget/TextView;I)Lio/reactivex/Observable;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator$attach$1;->this$0:Lcom/squareup/mailorder/OrderCoordinator;

    invoke-static {v1}, Lcom/squareup/mailorder/OrderCoordinator;->access$getScreens$p(Lcom/squareup/mailorder/OrderCoordinator;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/mailorder/OrderCoordinator$attach$1$1;->INSTANCE:Lcom/squareup/mailorder/OrderCoordinator$attach$1$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "screens.map { it.workflow }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 260
    new-instance v2, Lcom/squareup/mailorder/OrderCoordinator$attach$1$$special$$inlined$withLatestFrom$1;

    invoke-direct {v2}, Lcom/squareup/mailorder/OrderCoordinator$attach$1$$special$$inlined$withLatestFrom$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v1, "withLatestFrom(other, Bi\u2026 combiner.invoke(t, u) })"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v1, Lcom/squareup/mailorder/OrderCoordinator$attach$1$3;

    invoke-direct {v1, p0}, Lcom/squareup/mailorder/OrderCoordinator$attach$1$3;-><init>(Lcom/squareup/mailorder/OrderCoordinator$attach$1;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "phone.debouncedOnEditorA\u2026subscribe { onOrder(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 57
    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderCoordinator$attach$1;->invoke()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
