.class public final Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;
.super Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;
.source "SelectCorrectedAddressScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectAddressData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000b\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "originalAddress",
        "Lcom/squareup/address/Address;",
        "correctedAddress",
        "(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V",
        "getContactInfo",
        "()Lcom/squareup/mailorder/ContactInfo;",
        "getCorrectedAddress",
        "()Lcom/squareup/address/Address;",
        "getOriginalAddress",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contactInfo:Lcom/squareup/mailorder/ContactInfo;

.field private final correctedAddress:Lcom/squareup/address/Address;

.field private final originalAddress:Lcom/squareup/address/Address;


# direct methods
.method public constructor <init>(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V
    .locals 1

    const-string v0, "contactInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalAddress"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "correctedAddress"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iput-object p2, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    iput-object p3, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;ILjava/lang/Object;)Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->copy(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final component2()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final component3()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final copy(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;
    .locals 1

    const-string v0, "contactInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalAddress"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "correctedAddress"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;-><init>(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iget-object v1, p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    iget-object v1, p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    iget-object p1, p1, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContactInfo()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final getCorrectedAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final getOriginalAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectAddressData(contactInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", originalAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->originalAddress:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", correctedAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;->correctedAddress:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
