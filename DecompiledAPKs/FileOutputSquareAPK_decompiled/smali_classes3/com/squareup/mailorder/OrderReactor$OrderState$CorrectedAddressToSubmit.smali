.class public final Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;
.super Lcom/squareup/mailorder/OrderReactor$OrderState;
.source "OrderReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor$OrderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorrectedAddressToSubmit"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\nH\u00c6\u0003JE\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00032\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\nH\u00c6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\u0013\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u00d6\u0003J\t\u0010$\u001a\u00020\u001fH\u00d6\u0001J\t\u0010%\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u001fH\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0014\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "itemToken",
        "",
        "cardCustomizationOption",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "verifiedAddressToken",
        "originalAddress",
        "Lcom/squareup/address/Address;",
        "correctedAddress",
        "(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V",
        "getCardCustomizationOption",
        "()Lcom/squareup/mailorder/CardCustomizationOption;",
        "getContactInfo",
        "()Lcom/squareup/mailorder/ContactInfo;",
        "getCorrectedAddress",
        "()Lcom/squareup/address/Address;",
        "getItemToken",
        "()Ljava/lang/String;",
        "getOriginalAddress",
        "getVerifiedAddressToken",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

.field private final contactInfo:Lcom/squareup/mailorder/ContactInfo;

.field private final correctedAddress:Lcom/squareup/address/Address;

.field private final itemToken:Ljava/lang/String;

.field private final originalAddress:Lcom/squareup/address/Address;

.field private final verifiedAddressToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit$Creator;

    invoke-direct {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit$Creator;-><init>()V

    sput-object v0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V
    .locals 1

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifiedAddressToken"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalAddress"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "correctedAddress"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 154
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->itemToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    iput-object p6, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;ILjava/lang/Object;)Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getItemToken()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object p2

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getItemToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final component6()Lcom/squareup/address/Address;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;
    .locals 8

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "verifiedAddressToken"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalAddress"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "correctedAddress"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;Ljava/lang/String;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getItemToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iget-object v1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    iget-object v1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    return-object v0
.end method

.method public final getContactInfo()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final getCorrectedAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public getItemToken()Ljava/lang/String;
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->itemToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getOriginalAddress()Lcom/squareup/address/Address;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    return-object v0
.end method

.method public final getVerifiedAddressToken()Ljava/lang/String;
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getItemToken()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CorrectedAddressToSubmit(itemToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardCustomizationOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contactInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", verifiedAddressToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", originalAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", correctedAddress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->itemToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->verifiedAddressToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->originalAddress:Lcom/squareup/address/Address;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->correctedAddress:Lcom/squareup/address/Address;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
