.class public final Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;
.super Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.source "OrderServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServiceError"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\n\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;",
        "Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;",
        "res",
        "Landroid/content/res/Resources;",
        "warning",
        "Lcom/squareup/widgets/warning/Warning;",
        "(Landroid/content/res/Resources;Lcom/squareup/widgets/warning/Warning;)V",
        "message",
        "",
        "getMessage",
        "()Ljava/lang/String;",
        "title",
        "getTitle",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final message:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/squareup/widgets/warning/Warning;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "warning"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 143
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 148
    invoke-interface {p2, p1}, Lcom/squareup/widgets/warning/Warning;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;

    move-result-object p1

    .line 149
    iget-object p2, p1, Lcom/squareup/widgets/warning/Warning$Strings;->title:Ljava/lang/String;

    const-string v0, "strings.title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;->title:Ljava/lang/String;

    .line 150
    iget-object p1, p1, Lcom/squareup/widgets/warning/Warning$Strings;->body:Ljava/lang/String;

    const-string p2, "strings.body"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/mailorder/OrderServiceHelper$VerificationResult$ServiceError;->title:Ljava/lang/String;

    return-object v0
.end method
