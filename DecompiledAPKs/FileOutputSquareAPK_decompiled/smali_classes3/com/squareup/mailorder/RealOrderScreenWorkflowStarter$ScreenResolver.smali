.class public final Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;
.super Ljava/lang/Object;
.source "RealOrderScreenWorkflowStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenResolver"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderScreenWorkflowStarter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderScreenWorkflowStarter.kt\ncom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver\n*L\n1#1,179:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J:\u0010\u000f\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00112\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0005\u001a\u00020\u0006H\u0002JN\u0010\u0019\u001a$\u0012\u0004\u0012\u00020\u001b\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00110\u001aj\u0008\u0012\u0004\u0012\u00020\u001b`\u001c2\u0006\u0010\u0005\u001a\u00020\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;",
        "",
        "()V",
        "confirmOrderScreenData",
        "Lcom/squareup/mailorder/OrderConfirmation$ScreenData;",
        "state",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "confirmUnverifiedAddressScreenData",
        "Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;",
        "messageDialogScreenData",
        "Lcom/squareup/widgets/warning/WarningStrings;",
        "nonWarningScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/mailorder/OrderEvent;",
        "orderScreenData",
        "Lcom/squareup/mailorder/Order$ScreenData;",
        "selectAddressScreenData",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "toScreenStack",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;-><init>()V

    return-void
.end method

.method private final confirmOrderScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/mailorder/OrderConfirmation$ScreenData;
    .locals 1

    .line 168
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;

    if-eqz v0, :cond_1

    .line 170
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->REORDER_READER_IN_APP_EMAIL_CONFIRM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    .line 171
    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p2

    const-string p3, "settings.userSettings"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, ""

    .line 169
    :goto_0
    new-instance p3, Lcom/squareup/mailorder/OrderConfirmation$ScreenData;

    invoke-direct {p3, p1, p2}, Lcom/squareup/mailorder/OrderConfirmation$ScreenData;-><init>(ZLjava/lang/String;)V

    return-object p3

    .line 174
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Can\'t convert "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " to a ConfirmationScreenData"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final confirmUnverifiedAddressScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;
    .locals 3

    .line 156
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$ConfirmAddress;-><init>(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;)V

    check-cast v0, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;

    goto :goto_0

    .line 157
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$InProgress;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData$InProgress;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v0, Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;

    :goto_0
    return-object v0

    .line 158
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 159
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to Unverified Address Screen."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 158
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final messageDialogScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/widgets/warning/WarningStrings;
    .locals 3

    .line 130
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_2
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_3
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 135
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to a Warning"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 135
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final nonWarningScreen(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/mailorder/OrderEvent;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;"
        }
    .end annotation

    .line 90
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;

    if-eqz v0, :cond_0

    .line 91
    move-object v0, p0

    check-cast v0, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {v0, p1, p3, p4}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->confirmOrderScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/mailorder/OrderConfirmation$ScreenData;

    move-result-object p1

    .line 90
    invoke-static {p1, p2}, Lcom/squareup/mailorder/OrderConfirmationScreenKt;->OrderConfirmationScreen(Lcom/squareup/mailorder/OrderConfirmation$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto/16 :goto_0

    .line 93
    :cond_0
    instance-of p3, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    if-eqz p3, :cond_1

    .line 94
    move-object p3, p0

    check-cast p3, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {p3, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->selectAddressScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;

    move-result-object p1

    .line 93
    invoke-static {p1, p2}, Lcom/squareup/mailorder/SelectCorrectedAddressScreenKt;->SelectCorrectedAddressScreen(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 96
    :cond_1
    instance-of p3, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;

    if-eqz p3, :cond_2

    .line 97
    move-object p3, p0

    check-cast p3, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {p3, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->confirmUnverifiedAddressScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;

    move-result-object p1

    .line 96
    invoke-static {p1, p2}, Lcom/squareup/mailorder/UnverifiedAddressScreenKt;->UnverifiedAddressScreen(Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 99
    :cond_2
    instance-of p3, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    if-eqz p3, :cond_6

    .line 100
    move-object p3, p1

    check-cast p3, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {p3}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getOriginationFlow()Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    move-result-object p3

    sget-object p4, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p3}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;->ordinal()I

    move-result p3

    aget p3, p4, p3

    const/4 p4, 0x1

    if-eq p3, p4, :cond_5

    const/4 p4, 0x2

    if-eq p3, p4, :cond_4

    const/4 p4, 0x3

    if-ne p3, p4, :cond_3

    .line 106
    move-object p3, p0

    check-cast p3, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {p3, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->confirmUnverifiedAddressScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;

    move-result-object p1

    .line 105
    invoke-static {p1, p2}, Lcom/squareup/mailorder/UnverifiedAddressScreenKt;->UnverifiedAddressScreen(Lcom/squareup/mailorder/UnverifiedAddress$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 103
    :cond_4
    move-object p3, p0

    check-cast p3, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {p3, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->selectAddressScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;

    move-result-object p1

    .line 102
    invoke-static {p1, p2}, Lcom/squareup/mailorder/SelectCorrectedAddressScreenKt;->SelectCorrectedAddressScreen(Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 101
    :cond_5
    move-object p3, p0

    check-cast p3, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {p3, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->orderScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/Order$ScreenData;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/mailorder/OrderScreenKt;->OrderScreen(Lcom/squareup/mailorder/Order$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 110
    :cond_6
    move-object p3, p0

    check-cast p3, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {p3, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->orderScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/Order$ScreenData;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/mailorder/OrderScreenKt;->OrderScreen(Lcom/squareup/mailorder/Order$ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final orderScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/Order$ScreenData;
    .locals 3

    .line 114
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto/16 :goto_1

    .line 115
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto/16 :goto_1

    .line 116
    :cond_1
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/mailorder/Order$ScreenData$InProgress;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/Order$ScreenData$InProgress;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto :goto_1

    .line 117
    :cond_2
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/squareup/mailorder/Order$ScreenData$Correction;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$CorrectedAddress;->getAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/Order$ScreenData$Correction;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;)V

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto :goto_1

    .line 118
    :cond_3
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$UncorrectableAddress;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/mailorder/Order$ScreenData$Uncorrectable;->INSTANCE:Lcom/squareup/mailorder/Order$ScreenData$Uncorrectable;

    move-object v0, p1

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto :goto_1

    .line 119
    :cond_4
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$MissingFieldsError;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    goto :goto_0

    .line 120
    :cond_5
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$VerificationError;

    if-eqz v0, :cond_6

    :goto_0
    new-instance p1, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    invoke-direct {p1, v2, v1, v2}, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;-><init>(Lcom/squareup/mailorder/ContactInfo;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto :goto_1

    .line 121
    :cond_6
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning$OrderError;

    if-eqz v0, :cond_7

    new-instance p1, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    invoke-direct {p1, v2, v1, v2}, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;-><init>(Lcom/squareup/mailorder/ContactInfo;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto :goto_1

    .line 122
    :cond_7
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;

    if-eqz v0, :cond_8

    new-instance p1, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    invoke-direct {p1, v2, v1, v2}, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;-><init>(Lcom/squareup/mailorder/ContactInfo;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    goto :goto_1

    .line 123
    :cond_8
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/squareup/mailorder/Order$ScreenData$InProgress;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/Order$ScreenData$InProgress;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v0, Lcom/squareup/mailorder/Order$ScreenData;

    :goto_1
    return-object v0

    .line 124
    :cond_9
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to Order Screen Data"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 124
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final selectAddressScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;
    .locals 3

    .line 143
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;

    .line 144
    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getOriginalAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCorrectedAddress()Lcom/squareup/address/Address;

    move-result-object p1

    .line 143
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$SelectAddressData;-><init>(Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Lcom/squareup/address/Address;)V

    check-cast v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;

    goto :goto_0

    .line 146
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$InProgress;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData$InProgress;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    check-cast v0, Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;

    :goto_0
    return-object v0

    .line 147
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 148
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to Correct Address Screen."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 147
    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public final toScreenStack(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/mailorder/OrderEvent;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    .line 77
    move-object v1, p0

    check-cast v1, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->nonWarningScreen(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p3

    .line 78
    sget-object p4, Lcom/squareup/mailorder/OrderWarningDialog;->INSTANCE:Lcom/squareup/mailorder/OrderWarningDialog;

    invoke-direct {v1, p1}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->messageDialogScreenData(Lcom/squareup/mailorder/OrderReactor$OrderState;)Lcom/squareup/widgets/warning/WarningStrings;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    invoke-virtual {p4, p1, p2}, Lcom/squareup/mailorder/OrderWarningDialog;->createScreen(Lcom/squareup/widgets/warning/Warning;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 76
    invoke-virtual {v0, p3, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->stack(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 81
    :cond_0
    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    move-object v1, p0

    check-cast v1, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/mailorder/RealOrderScreenWorkflowStarter$ScreenResolver;->nonWarningScreen(Lcom/squareup/mailorder/OrderReactor$OrderState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1
.end method
