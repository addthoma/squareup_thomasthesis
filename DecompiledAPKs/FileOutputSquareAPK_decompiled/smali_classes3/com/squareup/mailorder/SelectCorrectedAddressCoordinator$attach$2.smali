.class final Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectCorrectedAddressCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000120\u0010\u0002\u001a,\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\u0004\u0018\u0001`\u00060\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
        "Lcom/squareup/mailorder/SelectCorrectedAddressScreen;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    iput-object p2, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/SelectCorrectedAddress$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent;",
            ">;)V"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    const-string v1, "screen"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-static {v0, p1, v1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->access$populateLayout(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$attach$2;->this$0:Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;

    invoke-static {v0}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->access$selectedItem(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;)I

    move-result v1

    invoke-static {v0, p1, v1}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;->access$observeClicks(Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator;Lcom/squareup/workflow/legacy/Screen;I)V

    return-void
.end method
