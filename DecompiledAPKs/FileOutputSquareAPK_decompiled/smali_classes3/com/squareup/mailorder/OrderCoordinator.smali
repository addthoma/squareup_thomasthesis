.class public final Lcom/squareup/mailorder/OrderCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderCoordinator$Factory;,
        Lcom/squareup/mailorder/OrderCoordinator$Configuration;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderCoordinator.kt\ncom/squareup/mailorder/OrderCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,259:1\n1103#2,7:260\n704#3:267\n777#3,2:268\n37#4,2:270\n*E\n*S KotlinDebug\n*F\n+ 1 OrderCoordinator.kt\ncom/squareup/mailorder/OrderCoordinator\n*L\n159#1,7:260\n227#1:267\n227#1,2:268\n227#1,2:270\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002=>BE\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u001c\u0010\n\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000f0\u000b\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010$\u001a\u00020%2\u0006\u0010\u0008\u001a\u00020\tH\u0002J\u0010\u0010&\u001a\u00020%2\u0006\u0010\'\u001a\u00020\u001bH\u0016J\u0010\u0010(\u001a\u00020%2\u0006\u0010\'\u001a\u00020\u001bH\u0002J\u0008\u0010)\u001a\u00020%H\u0002J\u0010\u0010*\u001a\u00020%2\u0006\u0010+\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020%2\u0006\u0010.\u001a\u00020/H\u0002J\u0008\u00100\u001a\u00020%H\u0002J\u0016\u00101\u001a\u00020%2\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u00020\u000e03H\u0002J\u0010\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\rH\u0002J(\u00107\u001a\u00020%2\u0006\u0010\'\u001a\u00020\u001b2\u0016\u00108\u001a\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000fH\u0002J \u00109\u001a\u00020%2\u0016\u00108\u001a\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000fH\u0002J\u0010\u0010:\u001a\u00020%2\u0006\u0010;\u001a\u00020<H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\n\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\u000cj\u0002`\u000f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "scrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "res",
        "Lcom/squareup/util/Res;",
        "spinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "configuration",
        "Lcom/squareup/mailorder/OrderCoordinator$Configuration;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/Order$ScreenData;",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
        "Lcom/squareup/mailorder/OrderScreen;",
        "(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "addressLayout",
        "Lcom/squareup/address/AddressLayout;",
        "glyph",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "messageForNoTitle",
        "Lcom/squareup/widgets/MessageView;",
        "messageForTitle",
        "modifiedWarning",
        "Landroid/view/View;",
        "orderButton",
        "Lcom/squareup/noho/NohoButton;",
        "phone",
        "Lcom/squareup/noho/NohoEditText;",
        "shippingName",
        "Landroid/widget/EditText;",
        "title",
        "uncorrectableWarning",
        "applyConfiguration",
        "",
        "attach",
        "view",
        "bindViews",
        "indicateCorrection",
        "makeCorrection",
        "address",
        "Lcom/squareup/address/Address;",
        "onAddressCorrection",
        "correction",
        "Lcom/squareup/protos/common/location/GlobalAddress;",
        "onAddressUncorrectable",
        "onOrder",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "update",
        "screen",
        "updateActionBarBack",
        "updateFields",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "Configuration",
        "Factory",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private addressLayout:Lcom/squareup/address/AddressLayout;

.field private final configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

.field private glyph:Lcom/squareup/glyph/SquareGlyphView;

.field private messageForNoTitle:Lcom/squareup/widgets/MessageView;

.field private messageForTitle:Lcom/squareup/widgets/MessageView;

.field private modifiedWarning:Landroid/view/View;

.field private orderButton:Lcom/squareup/noho/NohoButton;

.field private phone:Lcom/squareup/noho/NohoEditText;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/Order$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final scrubber:Lcom/squareup/text/InsertingScrubber;

.field private shippingName:Landroid/widget/EditText;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private title:Lcom/squareup/widgets/MessageView;

.field private uncorrectableWarning:Landroid/view/View;


# direct methods
.method private constructor <init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/InsertingScrubber;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lcom/squareup/mailorder/OrderCoordinator$Configuration;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/Order$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
            ">;>;)V"
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/mailorder/OrderCoordinator;->configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    iput-object p5, p0, Lcom/squareup/mailorder/OrderCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 57
    invoke-direct/range {p0 .. p5}, Lcom/squareup/mailorder/OrderCoordinator;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/util/Res;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/OrderCoordinator$Configuration;Lio/reactivex/Observable;)V

    return-void
.end method

.method public static final synthetic access$getPhone$p(Lcom/squareup/mailorder/OrderCoordinator;)Lcom/squareup/noho/NohoEditText;
    .locals 1

    .line 57
    iget-object p0, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    if-nez p0, :cond_0

    const-string v0, "phone"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getScreens$p(Lcom/squareup/mailorder/OrderCoordinator;)Lio/reactivex/Observable;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/mailorder/OrderCoordinator;->screens:Lio/reactivex/Observable;

    return-object p0
.end method

.method public static final synthetic access$onOrder(Lcom/squareup/mailorder/OrderCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->onOrder(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$setPhone$p(Lcom/squareup/mailorder/OrderCoordinator;Lcom/squareup/noho/NohoEditText;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/mailorder/OrderCoordinator;Lcom/squareup/mailorder/Order$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->spinnerData(Lcom/squareup/mailorder/Order$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/mailorder/OrderCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/squareup/mailorder/OrderCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final applyConfiguration(Lcom/squareup/mailorder/OrderCoordinator$Configuration;)V
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->shippingName:Landroid/widget/EditText;

    if-nez v0, :cond_0

    const-string v1, "shippingName"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getShowName()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    .line 140
    iget-object v2, p0, Lcom/squareup/mailorder/OrderCoordinator;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    const-string v3, "glyph"

    if-nez v2, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 141
    iget-object v2, p0, Lcom/squareup/mailorder/OrderCoordinator;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 143
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getTitle()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    .line 144
    :goto_0
    iget-object v2, p0, Lcom/squareup/mailorder/OrderCoordinator;->title:Lcom/squareup/widgets/MessageView;

    const-string/jumbo v3, "title"

    if-nez v2, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v2, Landroid/view/View;

    invoke-static {v2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v0, :cond_7

    .line 146
    iget-object v2, p0, Lcom/squareup/mailorder/OrderCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v2, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v3, p0, Lcom/squareup/mailorder/OrderCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getTitle()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    if-eqz v0, :cond_8

    .line 148
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->messageForTitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_9

    const-string v2, "messageForTitle"

    :goto_1
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->messageForNoTitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_9

    const-string v2, "messageForNoTitle"

    goto :goto_1

    .line 149
    :cond_9
    :goto_2
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 150
    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getMessage()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->orderButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_a

    const-string v1, "orderButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getOrderButtonLabel()I

    move-result p1

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 246
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 247
    sget v0, Lcom/squareup/mailorder/R$id;->shipping_address:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->addressLayout:Lcom/squareup/address/AddressLayout;

    .line 248
    sget v0, Lcom/squareup/mailorder/R$id;->shipping_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->shippingName:Landroid/widget/EditText;

    .line 249
    sget v0, Lcom/squareup/mailorder/R$id;->phone_number:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    .line 250
    sget v0, Lcom/squareup/mailorder/R$id;->modified_warning:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->modifiedWarning:Landroid/view/View;

    .line 251
    sget v0, Lcom/squareup/mailorder/R$id;->uncorrectable_warning:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->uncorrectableWarning:Landroid/view/View;

    .line 252
    sget v0, Lcom/squareup/mailorder/R$id;->order_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->glyph:Lcom/squareup/glyph/SquareGlyphView;

    .line 253
    sget v0, Lcom/squareup/mailorder/R$id;->order_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 254
    sget v0, Lcom/squareup/mailorder/R$id;->order_message_for_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->messageForTitle:Lcom/squareup/widgets/MessageView;

    .line 255
    sget v0, Lcom/squareup/mailorder/R$id;->order_message_for_no_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->messageForNoTitle:Lcom/squareup/widgets/MessageView;

    .line 256
    sget v0, Lcom/squareup/mailorder/R$id;->order_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.order_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/mailorder/OrderCoordinator;->orderButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final indicateCorrection()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->modifiedWarning:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "modifiedWarning"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->uncorrectableWarning:Landroid/view/View;

    if-nez v0, :cond_1

    const-string/jumbo v1, "uncorrectableWarning"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final makeCorrection(Lcom/squareup/address/Address;)V
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->addressLayout:Lcom/squareup/address/AddressLayout;

    if-nez v0, :cond_0

    const-string v1, "addressLayout"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    return-void
.end method

.method private final onAddressCorrection(Lcom/squareup/protos/common/location/GlobalAddress;)V
    .locals 1

    .line 193
    sget-object v0, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/address/Address$Companion;->fromGlobalAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->makeCorrection(Lcom/squareup/address/Address;)V

    .line 194
    invoke-direct {p0}, Lcom/squareup/mailorder/OrderCoordinator;->indicateCorrection()V

    return-void
.end method

.method private final onAddressUncorrectable()V
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->uncorrectableWarning:Landroid/view/View;

    if-nez v0, :cond_0

    const-string/jumbo v1, "uncorrectableWarning"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->modifiedWarning:Landroid/view/View;

    if-nez v0, :cond_1

    const-string v1, "modifiedWarning"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private final onOrder(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/EditText;

    .line 227
    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator;->shippingName:Landroid/widget/EditText;

    const-string v2, "shippingName"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v3, 0x0

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    const-string v4, "phone"

    if-nez v1, :cond_1

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Landroid/widget/EditText;

    const/4 v5, 0x1

    aput-object v1, v0, v5

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 268
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Landroid/widget/EditText;

    .line 227
    invoke-virtual {v7}, Landroid/widget/EditText;->getVisibility()I

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x1

    goto :goto_1

    :cond_3
    const/4 v7, 0x0

    :goto_1
    if-eqz v7, :cond_2

    invoke-interface {v1, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 269
    :cond_4
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    new-array v0, v3, [Landroid/widget/EditText;

    .line 271
    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    check-cast v0, [Landroid/widget/TextView;

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/widget/TextView;

    .line 227
    invoke-static {v0}, Lcom/squareup/util/Views;->getEmptyView([Landroid/widget/TextView;)Landroid/widget/TextView;

    move-result-object v0

    if-nez v0, :cond_a

    .line 228
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->addressLayout:Lcom/squareup/address/AddressLayout;

    const-string v1, "addressLayout"

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->getEmptyField()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_6

    goto :goto_2

    .line 239
    :cond_6
    new-instance v0, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;

    .line 240
    new-instance v3, Lcom/squareup/mailorder/ContactInfo;

    iget-object v5, p0, Lcom/squareup/mailorder/OrderCoordinator;->shippingName:Landroid/widget/EditText;

    if-nez v5, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    if-nez v5, :cond_8

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v5}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/mailorder/OrderCoordinator;->addressLayout:Lcom/squareup/address/AddressLayout;

    if-nez v5, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v5}, Lcom/squareup/address/AddressLayout;->getAddress()Lcom/squareup/address/Address;

    move-result-object v1

    const-string v5, "addressLayout.address"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v2, v4, v1}, Lcom/squareup/mailorder/ContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/address/Address;)V

    .line 239
    invoke-direct {v0, v3}, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;-><init>(Lcom/squareup/mailorder/ContactInfo;)V

    .line 238
    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void

    .line 230
    :cond_a
    :goto_2
    new-instance v0, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$MissingFields;

    .line 231
    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/mailorder/R$string;->order_missing_info_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 232
    iget-object v2, p0, Lcom/squareup/mailorder/OrderCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/mailorder/R$string;->order_missing_info_message:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 230
    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$MissingFields;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void

    .line 271
    :cond_b
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final spinnerData(Lcom/squareup/mailorder/Order$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 2

    .line 212
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 213
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    .line 214
    check-cast p1, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;

    invoke-virtual {p1}, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    sget p1, Lcom/squareup/common/strings/R$string;->loading:I

    .line 213
    invoke-virtual {v0, v1, p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_2

    .line 217
    :cond_1
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$InProgress;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    sget v0, Lcom/squareup/mailorder/R$string;->validating_address:I

    invoke-virtual {p1, v1, v0}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    goto :goto_2

    .line 218
    :cond_2
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    if-eqz v0, :cond_3

    goto :goto_1

    .line 219
    :cond_3
    instance-of v0, p1, Ljava/lang/Error;

    if-eqz v0, :cond_4

    goto :goto_1

    .line 220
    :cond_4
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$Correction;

    if-eqz v0, :cond_5

    goto :goto_1

    .line 221
    :cond_5
    instance-of p1, p1, Lcom/squareup/mailorder/Order$ScreenData$Uncorrectable;

    if-eqz p1, :cond_6

    :goto_1
    sget-object p1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->hideSpinner()Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/Order$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 158
    sget v0, Lcom/squareup/mailorder/R$id;->order_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 260
    new-instance v1, Lcom/squareup/mailorder/OrderCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/mailorder/OrderCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/mailorder/OrderCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    new-instance v0, Lcom/squareup/mailorder/OrderCoordinator$update$2;

    invoke-direct {v0, p2}, Lcom/squareup/mailorder/OrderCoordinator$update$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 163
    iget-object p1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/mailorder/Order$ScreenData;

    .line 164
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;

    invoke-virtual {p1}, Lcom/squareup/mailorder/Order$ScreenData$InitialLoad;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->updateFields(Lcom/squareup/mailorder/ContactInfo;)V

    goto :goto_0

    .line 165
    :cond_0
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;

    invoke-virtual {p1}, Lcom/squareup/mailorder/Order$ScreenData$ReadyForInput;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->updateFields(Lcom/squareup/mailorder/ContactInfo;)V

    goto :goto_0

    .line 166
    :cond_1
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$InProgress;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/mailorder/Order$ScreenData$InProgress;

    invoke-virtual {p1}, Lcom/squareup/mailorder/Order$ScreenData$InProgress;->getContactInfo()Lcom/squareup/mailorder/ContactInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->updateFields(Lcom/squareup/mailorder/ContactInfo;)V

    goto :goto_0

    .line 167
    :cond_2
    instance-of v0, p1, Lcom/squareup/mailorder/Order$ScreenData$Correction;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/mailorder/Order$ScreenData$Correction;

    invoke-virtual {p1}, Lcom/squareup/mailorder/Order$ScreenData$Correction;->getAddress()Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->onAddressCorrection(Lcom/squareup/protos/common/location/GlobalAddress;)V

    goto :goto_0

    .line 168
    :cond_3
    instance-of p1, p1, Lcom/squareup/mailorder/Order$ScreenData$Uncorrectable;

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lcom/squareup/mailorder/OrderCoordinator;->onAddressUncorrectable()V

    .line 170
    :cond_4
    :goto_0
    invoke-direct {p0, p2}, Lcom/squareup/mailorder/OrderCoordinator;->updateActionBarBack(Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final updateActionBarBack(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/mailorder/Order$ScreenData;",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 177
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 174
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 175
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    iget-object v3, p0, Lcom/squareup/mailorder/OrderCoordinator;->configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    invoke-virtual {v3}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;->getActionBarTitle()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 176
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/mailorder/OrderCoordinator$updateActionBarBack$1;

    invoke-direct {v3, p1}, Lcom/squareup/mailorder/OrderCoordinator$updateActionBarBack$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateFields(Lcom/squareup/mailorder/ContactInfo;)V
    .locals 4

    .line 181
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 182
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->shippingName:Landroid/widget/EditText;

    if-nez v0, :cond_1

    const-string v3, "shippingName"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    sget-object v3, Lcom/squareup/address/Address;->EMPTY:Lcom/squareup/address/Address;

    if-eq v0, v3, :cond_4

    .line 185
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->addressLayout:Lcom/squareup/address/AddressLayout;

    if-nez v0, :cond_3

    const-string v3, "addressLayout"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    .line 187
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getPhone()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_5

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    .line 188
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_6

    const-string v1, "phone"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/mailorder/ContactInfo;->getPhone()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/mailorder/OrderCoordinator;->bindViews(Landroid/view/View;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    const-string v1, "phone"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v3, p0, Lcom/squareup/mailorder/OrderCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v4, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v4, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v2, v3, v4}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v2, "spinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 120
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->configuration:Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderCoordinator;->applyConfiguration(Lcom/squareup/mailorder/OrderCoordinator$Configuration;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->phone:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/mailorder/OrderCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/mailorder/OrderCoordinator$attach$1;-><init>(Lcom/squareup/mailorder/OrderCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/mailorder/OrderCoordinator;->screens:Lio/reactivex/Observable;

    .line 132
    iget-object v1, p0, Lcom/squareup/mailorder/OrderCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/mailorder/OrderCoordinator$attach$2;

    invoke-direct {v2, p0}, Lcom/squareup/mailorder/OrderCoordinator$attach$2;-><init>(Lcom/squareup/mailorder/OrderCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 133
    new-instance v1, Lcom/squareup/mailorder/OrderCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/mailorder/OrderCoordinator$attach$3;-><init>(Lcom/squareup/mailorder/OrderCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026-> update(view, screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
