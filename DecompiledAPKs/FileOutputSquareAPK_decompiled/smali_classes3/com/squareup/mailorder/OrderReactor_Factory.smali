.class public final Lcom/squareup/mailorder/OrderReactor_Factory;
.super Ljava/lang/Object;
.source "OrderReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/OrderReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/MailOrderAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderReactor$Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private final orderServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderReactor$Configuration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/MailOrderAnalytics;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor_Factory;->orderServiceHelperProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor_Factory;->configurationProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/mailorder/OrderReactor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/mailorder/OrderReactor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/OrderReactor$Configuration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/MailOrderAnalytics;",
            ">;)",
            "Lcom/squareup/mailorder/OrderReactor_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/mailorder/OrderReactor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/mailorder/OrderReactor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/OrderReactor$Configuration;Lcom/squareup/mailorder/MailOrderAnalytics;)Lcom/squareup/mailorder/OrderReactor;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/mailorder/OrderReactor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/mailorder/OrderReactor;-><init>(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/OrderReactor$Configuration;Lcom/squareup/mailorder/MailOrderAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/OrderReactor;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor_Factory;->orderServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mailorder/OrderServiceHelper;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor_Factory;->configurationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mailorder/OrderReactor$Configuration;

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/mailorder/MailOrderAnalytics;

    invoke-static {v0, v1, v2}, Lcom/squareup/mailorder/OrderReactor_Factory;->newInstance(Lcom/squareup/mailorder/OrderServiceHelper;Lcom/squareup/mailorder/OrderReactor$Configuration;Lcom/squareup/mailorder/MailOrderAnalytics;)Lcom/squareup/mailorder/OrderReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor_Factory;->get()Lcom/squareup/mailorder/OrderReactor;

    move-result-object v0

    return-object v0
.end method
