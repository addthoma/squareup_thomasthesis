.class public interface abstract Lcom/squareup/crm/CustomerManagementSettings;
.super Ljava/lang/Object;
.source "CustomerManagementSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0002\u0008\t\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0008\u0010\n\u001a\u00020\u0003H&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003H&J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003H&J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003H&J\u0010\u0010\u0011\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0003H&J\u0008\u0010\u0012\u001a\u00020\u0003H&J\u0018\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0003H&\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/crm/CustomerManagementSettings;",
        "",
        "isAddCustomerToSaleEnabled",
        "",
        "isAfterCheckoutEnabled",
        "isAllowed",
        "isBeforeCheckoutEnabled",
        "isCardOnFileEnabled",
        "isCardOnFileFeatureEnabled",
        "isEmvCardOnFileFeatureEnabled",
        "isSaveCardEnabled",
        "isSaveCardPostTransactionEnabled",
        "setAfterCheckoutEnabled",
        "",
        "value",
        "setBeforeCheckoutEnabled",
        "setSaveCardEnabled",
        "setSaveCardPostTransactionEnabled",
        "shouldDisplaySettingSectionOn",
        "showCardButtonEnabledForTenderType",
        "isMagStripeTenderInFlight",
        "isEmvTenderInFlight",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract isAddCustomerToSaleEnabled()Z
.end method

.method public abstract isAfterCheckoutEnabled()Z
.end method

.method public abstract isAllowed()Z
.end method

.method public abstract isBeforeCheckoutEnabled()Z
.end method

.method public abstract isCardOnFileEnabled()Z
.end method

.method public abstract isCardOnFileFeatureEnabled()Z
.end method

.method public abstract isEmvCardOnFileFeatureEnabled()Z
.end method

.method public abstract isSaveCardEnabled()Z
.end method

.method public abstract isSaveCardPostTransactionEnabled()Z
.end method

.method public abstract setAfterCheckoutEnabled(Z)V
.end method

.method public abstract setBeforeCheckoutEnabled(Z)V
.end method

.method public abstract setSaveCardEnabled(Z)V
.end method

.method public abstract setSaveCardPostTransactionEnabled(Z)V
.end method

.method public abstract shouldDisplaySettingSectionOn()Z
.end method

.method public abstract showCardButtonEnabledForTenderType(ZZ)Z
.end method
