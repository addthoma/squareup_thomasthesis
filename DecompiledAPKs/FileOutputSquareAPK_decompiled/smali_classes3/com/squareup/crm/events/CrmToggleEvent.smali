.class public Lcom/squareup/crm/events/CrmToggleEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CrmToggleEvent.java"


# instance fields
.field private final crm_register_event:Ljava/lang/String;

.field private final crm_register_toggle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Z)V
    .locals 1

    const-string v0, "crm_register"

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 15
    iget-object p1, p1, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/crm/events/CrmToggleEvent;->crm_register_event:Ljava/lang/String;

    if-eqz p2, :cond_0

    const-string p1, "ON"

    goto :goto_0

    :cond_0
    const-string p1, "OFF"

    .line 16
    :goto_0
    iput-object p1, p0, Lcom/squareup/crm/events/CrmToggleEvent;->crm_register_toggle:Ljava/lang/String;

    return-void
.end method
