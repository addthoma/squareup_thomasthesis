.class final Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealChooseGroupsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->render(Lcom/squareup/crm/groups/choose/ChooseGroupsProps;Lcom/squareup/crm/groups/choose/ChooseGroupsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/V2Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

.field final synthetic this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/workflow/RenderContext;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;

    iput-object p2, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p3, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->this$0:Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;

    iget-object v2, p0, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow$render$2;->$state:Lcom/squareup/crm/groups/choose/ChooseGroupsState;

    invoke-static {v1, v2}, Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;->access$showCreateGroupAction(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
