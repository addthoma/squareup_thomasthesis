.class public final Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;
.super Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.source "ContactAttribute.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomTextAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContactAttribute.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContactAttribute.kt\ncom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute\n*L\n1#1,322:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u000f\u0008\u0010\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B1\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\u0008\u001a\u00020\u0002\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\n\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u000bJ\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0002H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0002H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0002H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0002H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0004H\u00c4\u0003J?\u0010\u0018\u001a\u00020\u00002\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00022\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00022\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00022\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00022\u0008\u0008\u0002\u0010\n\u001a\u00020\u0004H\u00c6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\u0013\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\t\u0010\u001f\u001a\u00020\u001aH\u00d6\u0001J\u0008\u0010 \u001a\u00020\u0004H\u0016J\t\u0010!\u001a\u00020\u0002H\u00d6\u0001J\u0019\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u001aH\u00d6\u0001R\u0016\u0010\t\u001a\u0004\u0018\u00010\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0014\u0010\u0007\u001a\u00020\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\rR\u0014\u0010\u0008\u001a\u00020\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\rR\u0014\u0010\n\u001a\u00020\u0004X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\r\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;",
        "",
        "attribute",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V",
        "value",
        "key",
        "name",
        "fallbackValue",
        "original",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V",
        "getFallbackValue",
        "()Ljava/lang/String;",
        "getKey",
        "getName",
        "getOriginal",
        "()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "getValue",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toAttribute",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final fallbackValue:Ljava/lang/String;

.field private final key:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute$Creator;

    invoke-direct {v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute$Creator;-><init>()V

    sput-object v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 7

    const-string v0, "attribute"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    .line 172
    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    const-string v0, "attribute.key"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v4, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    const-string v0, "attribute.name"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v5, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    move-object v1, p0

    move-object v6, p1

    .line 170
    invoke-direct/range {v1 .. v6}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 177
    sget-object v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->Companion:Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;

    sget-object v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->TEXT:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-static {v0, p1, v1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;->access$requireType(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 1

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "original"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 169
    invoke-direct {p0, v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->value:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->key:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->name:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->fallbackValue:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getKey()Ljava/lang/String;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p5

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final component5()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;
    .locals 7

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "original"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getFallbackValue()Ljava/lang/String;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->fallbackValue:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->name:Ljava/lang/String;

    return-object v0
.end method

.method protected getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->value:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getKey()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 3

    .line 180
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->text(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 182
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    const-string v1, "original.newBuilder()\n  \u2026d() })\n          .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomTextAttribute(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", fallbackValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", original="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->value:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->key:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->fallbackValue:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomTextAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
