.class public final Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;
.super Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.source "ContactAttribute.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomEnumAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute<",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nContactAttribute.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ContactAttribute.kt\ncom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute\n*L\n1#1,322:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0019\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\u0017\u0008\u0010\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008BM\u0012\u000e\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0011J\u0011\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002H\u00c6\u0003J\u000f\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u000cH\u00c6\u0003J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010#\u001a\u00020\u0005H\u00c4\u0003J_\u0010$\u001a\u00020\u00002\u0010\u0008\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00022\u000e\u0008\u0002\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u00032\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0005H\u00c6\u0001J\t\u0010%\u001a\u00020&H\u00d6\u0001J\u0013\u0010\'\u001a\u00020\u000c2\u0008\u0010(\u001a\u0004\u0018\u00010)H\u00d6\u0003J\t\u0010*\u001a\u00020&H\u00d6\u0001J\u0008\u0010+\u001a\u00020\u0005H\u0016J\t\u0010,\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020&H\u00d6\u0001R\u0017\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\r\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0017R\u0014\u0010\u000e\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0017R\u0014\u0010\u0010\u001a\u00020\u0005X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u001c\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0013\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;",
        "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;",
        "",
        "",
        "attribute",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "definition",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V",
        "value",
        "allValues",
        "allowMultiple",
        "",
        "key",
        "name",
        "fallbackValue",
        "original",
        "(Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V",
        "getAllValues",
        "()Ljava/util/List;",
        "getAllowMultiple",
        "()Z",
        "getFallbackValue",
        "()Ljava/lang/String;",
        "getKey",
        "getName",
        "getOriginal",
        "()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
        "getValue",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toAttribute",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final allValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final allowMultiple:Z

.field private final fallbackValue:Ljava/lang/String;

.field private final key:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private final original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field private final value:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute$Creator;

    invoke-direct {v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute$Creator;-><init>()V

    sput-object v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)V
    .locals 9

    const-string v0, "attribute"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "definition"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v2, v0

    .line 197
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_1
    move-object v3, v0

    .line 198
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    const-string v0, "definition.enum_allow_multiple"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 199
    iget-object v5, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->key:Ljava/lang/String;

    const-string p2, "attribute.key"

    invoke-static {v5, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    iget-object v6, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->name:Ljava/lang/String;

    const-string p2, "attribute.name"

    invoke-static {v6, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    iget-object v7, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->fallback_value:Ljava/lang/String;

    move-object v1, p0

    move-object v8, p1

    .line 195
    invoke-direct/range {v1 .. v8}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;-><init>(Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    .line 204
    sget-object p2, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;->Companion:Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;

    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->ENUM:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-static {p2, p1, v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;->access$requireType(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$Companion;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ")V"
        }
    .end annotation

    const-string v0, "allValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "original"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 194
    invoke-direct {p0, v0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->value:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    iput-object p4, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->key:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->name:Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->fallbackValue:Ljava/lang/String;

    iput-object p7, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;ILjava/lang/Object;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getKey()Ljava/lang/String;

    move-result-object p4

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object p5

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object p6

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p7

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->copy(Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    return v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component6()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final component7()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;",
            ")",
            "Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;"
        }
    .end annotation

    const-string v0, "allValues"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "original"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    move-object v1, v0

    move-object v2, p1

    move v4, p3

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;-><init>(Ljava/util/List;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    iget-boolean v1, p1, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    return-object v0
.end method

.method public final getAllowMultiple()Z
    .locals 1

    .line 189
    iget-boolean v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    return v0
.end method

.method public getFallbackValue()Ljava/lang/String;
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->fallbackValue:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->name:Ljava/lang/String;

    return-object v0
.end method

.method protected getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    return-object v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->value:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getKey()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 3

    .line 207
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    const-string v1, "original.newBuilder()\n  \u2026d() })\n          .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomEnumAttribute(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getValue()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allowMultiple="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", fallbackValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getFallbackValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", original="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->getOriginal()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->value:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allValues:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-boolean p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->allowMultiple:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->key:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->fallbackValue:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/crm/model/ContactAttribute$CustomAttribute$CustomEnumAttribute;->original:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
