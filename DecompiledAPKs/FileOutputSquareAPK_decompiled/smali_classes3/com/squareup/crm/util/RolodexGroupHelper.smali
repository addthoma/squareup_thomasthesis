.class public final Lcom/squareup/crm/util/RolodexGroupHelper;
.super Ljava/lang/Object;
.source "RolodexGroupHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRolodexGroupHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RolodexGroupHelper.kt\ncom/squareup/crm/util/RolodexGroupHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,90:1\n704#2:91\n777#2,2:92\n950#2:94\n704#2:95\n777#2,2:96\n950#2:98\n*E\n*S KotlinDebug\n*F\n+ 1 RolodexGroupHelper.kt\ncom/squareup/crm/util/RolodexGroupHelper\n*L\n23#1:91\n23#1,2:92\n24#1:94\n32#1:95\n32#1,2:96\n33#1:98\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\u001a3\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u001a3\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00012\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\t0\u0005\"\u00020\t\u00a2\u0006\u0002\u0010\n\u001a\u0006\u0010\u000b\u001a\u00020\u000c\u001a\n\u0010\r\u001a\u00020\u000e*\u00020\u0002\u001a\n\u0010\u000f\u001a\u00020\u0010*\u00020\u0002\u001a\n\u0010\u0011\u001a\u00020\u0002*\u00020\u0008\u001a\n\u0010\u0012\u001a\u00020\u0008*\u00020\u0002\u001a\u0012\u0010\u0013\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "filterByTypeAndSort",
        "",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "groups",
        "groupTypes",
        "",
        "Lcom/squareup/protos/client/rolodex/GroupType;",
        "(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;",
        "Lcom/squareup/protos/client/rolodex/GroupV2;",
        "Lcom/squareup/protos/client/rolodex/GroupV2$Type;",
        "(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupV2$Type;)Ljava/util/List;",
        "newGroupBuilder",
        "Lcom/squareup/protos/client/rolodex/Group$Builder;",
        "canBeSaved",
        "",
        "getGroupMembershipCount",
        "",
        "toGroup",
        "toGroupV2",
        "withGroupName",
        "name",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final canBeSaved(Lcom/squareup/protos/client/rolodex/Group;)Z
    .locals 1

    const-string v0, "$this$canBeSaved"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    check-cast p0, Ljava/lang/CharSequence;

    const/4 v0, 0x1

    if-eqz p0, :cond_1

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    xor-int/2addr p0, v0

    return p0
.end method

.method public static final varargs filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;[",
            "Lcom/squareup/protos/client/rolodex/GroupType;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    const-string v0, "groups"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    check-cast p0, Ljava/lang/Iterable;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 92
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/rolodex/Group;

    .line 23
    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-static {p1, v2}, Lkotlin/collections/ArraysKt;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 94
    new-instance p0, Lcom/squareup/crm/util/RolodexGroupHelper$filterByTypeAndSort$$inlined$sortedBy$1;

    invoke-direct {p0}, Lcom/squareup/crm/util/RolodexGroupHelper$filterByTypeAndSort$$inlined$sortedBy$1;-><init>()V

    check-cast p0, Ljava/util/Comparator;

    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final varargs filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupV2$Type;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ">;[",
            "Lcom/squareup/protos/client/rolodex/GroupV2$Type;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/GroupV2;",
            ">;"
        }
    .end annotation

    const-string v0, "groups"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupTypes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    check-cast p0, Ljava/lang/Iterable;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 96
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/rolodex/GroupV2;

    .line 32
    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    invoke-static {p1, v2}, Lkotlin/collections/ArraysKt;->contains([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    :cond_1
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 98
    new-instance p0, Lcom/squareup/crm/util/RolodexGroupHelper$filterByTypeAndSort$$inlined$sortedBy$2;

    invoke-direct {p0}, Lcom/squareup/crm/util/RolodexGroupHelper$filterByTypeAndSort$$inlined$sortedBy$2;-><init>()V

    check-cast p0, Ljava/util/Comparator;

    invoke-static {v0, p0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final getGroupMembershipCount(Lcom/squareup/protos/client/rolodex/Group;)I
    .locals 2

    const-string v0, "$this$getGroupMembershipCount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int p0, v0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final newGroupBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;
    .locals 2

    .line 37
    new-instance v0, Lcom/squareup/protos/client/rolodex/Group$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type(Lcom/squareup/protos/client/rolodex/GroupType;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    const-string v1, "Builder().group_type(MANUAL_GROUP)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public static final toGroup(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/Group;
    .locals 3

    const-string v0, "$this$toGroup"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->type:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->SMART:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 77
    :goto_0
    new-instance v1, Lcom/squareup/protos/client/rolodex/Group$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;-><init>()V

    .line 78
    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->group_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 79
    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    goto :goto_1

    :cond_1
    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    :goto_1
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/rolodex/Group$Builder;->group_type(Lcom/squareup/protos/client/rolodex/GroupType;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v1

    if-eqz v0, :cond_2

    .line 80
    sget-object v0, Lcom/squareup/protos/client/rolodex/AudienceType;->GROUP_V2_SMART:Lcom/squareup/protos/client/rolodex/AudienceType;

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/squareup/protos/client/rolodex/AudienceType;->NONE:Lcom/squareup/protos/client/rolodex/AudienceType;

    :goto_2
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->audience_type(Lcom/squareup/protos/client/rolodex/AudienceType;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->counts:Lcom/squareup/protos/client/rolodex/GroupV2Counts;

    if-eqz v1, :cond_3

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/GroupV2Counts;->num_customers:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    goto :goto_3

    :cond_3
    const-wide/16 v1, 0x0

    :goto_3
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->num_customers(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    .line 83
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/GroupV2;->filters:Ljava/util/List;

    if-eqz p0, :cond_4

    goto :goto_4

    :cond_4
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_4
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p0

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p0

    const-string v0, "Builder()\n      .group_t\u2026orEmpty())\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toGroupV2(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/GroupV2;
    .locals 3

    const-string v0, "$this$toGroupV2"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    new-instance v0, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->group_type:Lcom/squareup/protos/client/rolodex/GroupType;

    sget-object v2, Lcom/squareup/protos/client/rolodex/GroupType;->AUDIENCE_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->SMART:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupV2$Type;->MANUAL:Lcom/squareup/protos/client/rolodex/GroupV2$Type;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->type(Lcom/squareup/protos/client/rolodex/GroupV2$Type;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v0

    .line 59
    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 60
    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->server_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 61
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v1

    .line 62
    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->group_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object v0

    .line 64
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->filters:Ljava/util/List;

    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->filters(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/GroupV2$Builder;

    move-result-object p0

    .line 65
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GroupV2$Builder;->build()Lcom/squareup/protos/client/rolodex/GroupV2;

    move-result-object p0

    const-string v0, "GroupV2.Builder()\n      \u2026orEmpty())\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withGroupName(Lcom/squareup/protos/client/rolodex/Group;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group;
    .locals 1

    const-string v0, "$this$withGroupName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group;->newBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p0

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/Group$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object p0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p0

    const-string p1, "newBuilder()\n        .di\u2026me(name)\n        .build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method
