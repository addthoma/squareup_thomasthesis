.class public final Lcom/squareup/crm/RolodexContactLoader$Input;
.super Ljava/lang/Object;
.source "RolodexContactLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/RolodexContactLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Input"
.end annotation


# instance fields
.field public final filterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field public final groupToken:Ljava/lang/String;

.field public final searchTerm:Ljava/lang/String;

.field public final sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            ")V"
        }
    .end annotation

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    .line 87
    iput-object p2, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    .line 88
    iput-object p3, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    .line 89
    iput-object p4, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 94
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/crm/RolodexContactLoader$Input;

    .line 97
    iget-object v2, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    .line 99
    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    iget-object p1, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 100
    invoke-static {v2, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 111
    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Inputs{groupToken=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', searchTerm=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', filterList=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', sortType=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->sortType:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
