.class final Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRolodexServiceHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRolodexServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRolodexServiceHelper.kt\ncom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1$1\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,772:1\n132#2,3:773\n*E\n*S KotlinDebug\n*F\n+ 1 RealRolodexServiceHelper.kt\ncom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1$1\n*L\n498#1,3:773\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "kotlin.jvm.PlatformType",
        "response",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1$1;->this$0:Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;
    .locals 2

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 497
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->merge_proposals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1$1;->this$0:Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;

    iget-object v1, v1, Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1;->$limit:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;->paging_key:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 498
    check-cast p1, Lcom/squareup/wire/Message;

    .line 774
    invoke-virtual {p1}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object p1

    if-eqz p1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;

    const/4 v1, 0x0

    .line 499
    check-cast v1, Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->paging_key:Ljava/lang/String;

    .line 775
    invoke-virtual {p1}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    goto :goto_1

    .line 774
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type B"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 120
    check-cast p1, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RealRolodexServiceHelper$listMergeProposals$1$1;->invoke(Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    move-result-object p1

    return-object p1
.end method
