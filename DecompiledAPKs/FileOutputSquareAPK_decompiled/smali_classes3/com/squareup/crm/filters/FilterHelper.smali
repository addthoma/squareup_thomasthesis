.class public Lcom/squareup/crm/filters/FilterHelper;
.super Ljava/lang/Object;
.source "FilterHelper.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexGroupLoader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/crm/filters/FilterHelper;->res:Lcom/squareup/util/Res;

    .line 37
    iput-object p2, p0, Lcom/squareup/crm/filters/FilterHelper;->features:Lcom/squareup/settings/server/Features;

    .line 38
    iput-object p3, p0, Lcom/squareup/crm/filters/FilterHelper;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    return-void
.end method

.method private formatGroupNames(Ljava/util/List;Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 208
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 209
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Group;

    .line 210
    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 215
    :cond_1
    iget-object p1, p0, Lcom/squareup/crm/filters/FilterHelper;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/crm/applet/R$string;->crm_filter_disjunction_separator_pattern:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    .line 216
    invoke-virtual {p1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 217
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private formatOptions(Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 199
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 202
    :cond_0
    iget-object p1, p0, Lcom/squareup/crm/filters/FilterHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_filter_disjunction_separator_pattern:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/ListPhrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/util/ListPhrase;

    move-result-object p1

    .line 203
    invoke-virtual {p1, v0}, Lcom/squareup/util/ListPhrase;->format(Ljava/lang/Iterable;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 204
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getFilterTypeWithSubtype(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;
    .locals 3

    .line 163
    sget-object v0, Lcom/squareup/crm/filters/FilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const-string v1, "/"

    packed-switch v0, :pswitch_data_0

    .line 192
    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected filter type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 189
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 186
    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 183
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 180
    :pswitch_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 177
    :pswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 174
    :pswitch_6
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->name()Ljava/lang/String;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic lambda$null$1(Ljava/util/List;)Ljava/lang/Boolean;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/GroupType;

    .line 130
    sget-object v1, Lcom/squareup/protos/client/rolodex/GroupType;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/GroupType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p0, v0}, Lcom/squareup/crm/util/RolodexGroupHelper;->filterByTypeAndSort(Ljava/util/List;[Lcom/squareup/protos/client/rolodex/GroupType;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public displayValueOf(Lcom/squareup/protos/client/rolodex/Filter;)Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 42
    sget-object v0, Lcom/squareup/crm/filters/FilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 104
    :pswitch_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 101
    :pswitch_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cs_creation_source_types:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/crm/filters/FilterHelper;->formatOptions(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 95
    :pswitch_2
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hvl_locations:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/crm/filters/FilterHelper;->formatOptions(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 92
    :pswitch_3
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 89
    :pswitch_4
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 86
    :pswitch_5
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->caen_attribute_values:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/crm/filters/FilterHelper;->formatOptions(Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 83
    :pswitch_6
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 80
    :pswitch_7
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 77
    :pswitch_8
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 74
    :pswitch_9
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 71
    :pswitch_a
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 68
    :pswitch_b
    invoke-static {}, Lrx/Observable;->empty()Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 65
    :pswitch_c
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 62
    :pswitch_d
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 51
    :pswitch_e
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_filter_visit_frequency_value_pattern:I

    .line 53
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    .line 54
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "minimum_visits"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    const-string/jumbo v1, "time_period"

    .line 55
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 57
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 52
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 44
    :pswitch_f
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$o23QbxCxy6QwKohRuzzyN_aZagE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$o23QbxCxy6QwKohRuzzyN_aZagE;-><init>(Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 46
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1

    :cond_0
    :goto_0
    const-string p1, ""

    .line 108
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public excludeFilterTemplates(Ljava/util/List;)Lio/reactivex/ObservableTransformer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;)",
            "Lio/reactivex/ObservableTransformer<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;>;"
        }
    .end annotation

    .line 121
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 122
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 123
    invoke-direct {p0, v1}, Lcom/squareup/crm/filters/FilterHelper;->getFilterTypeWithSubtype(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_0
    new-instance p1, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$Y2ww3ptCANDR5jY0BnNTI3OPMGQ;

    invoke-direct {p1, p0, v0}, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$Y2ww3ptCANDR5jY0BnNTI3OPMGQ;-><init>(Lcom/squareup/crm/filters/FilterHelper;Ljava/util/Set;)V

    return-object p1
.end method

.method public synthetic lambda$displayValueOf$0$FilterHelper(Lcom/squareup/protos/client/rolodex/Filter;Ljava/util/List;)Ljava/lang/String;
    .locals 0

    .line 46
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->mg_group_tokens:Ljava/util/List;

    invoke-direct {p0, p1, p2}, Lcom/squareup/crm/filters/FilterHelper;->formatGroupNames(Ljava/util/List;Ljava/util/List;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$excludeFilterTemplates$3$FilterHelper(Ljava/util/Set;Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->groupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 129
    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->success()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$fFS9N5gItacUA6JE8kJ8WMzZ5u4;->INSTANCE:Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$fFS9N5gItacUA6JE8kJ8WMzZ5u4;

    .line 130
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$hjQetjFggxql7BuRuexO_b6FNJ0;

    invoke-direct {v1, p0, p1}, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$hjQetjFggxql7BuRuexO_b6FNJ0;-><init>(Lcom/squareup/crm/filters/FilterHelper;Ljava/util/Set;)V

    .line 127
    invoke-static {p2, v0, v1}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$nameAndValueSingeLine$4$FilterHelper(Lcom/squareup/protos/client/rolodex/Filter;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 153
    iget-object v0, p0, Lcom/squareup/crm/filters/FilterHelper;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_filter_bubble_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    const-string v1, "name"

    .line 154
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string/jumbo v0, "value"

    .line 155
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$2$FilterHelper(Ljava/util/Set;Ljava/util/List;Ljava/lang/Boolean;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter;

    .line 135
    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter$Type;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

    if-ne v2, v3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 140
    :cond_1
    iget-object v2, v1, Lcom/squareup/protos/client/rolodex/Filter;->allow_multiples_in_conjunction:Ljava/lang/Boolean;

    sget-object v3, Lcom/squareup/protos/client/rolodex/Filter;->DEFAULT_ALLOW_MULTIPLES_IN_CONJUNCTION:Ljava/lang/Boolean;

    invoke-static {v2, v3}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 142
    invoke-direct {p0, v1}, Lcom/squareup/crm/filters/FilterHelper;->getFilterTypeWithSubtype(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 143
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public nameAndValueSingeLine(Lcom/squareup/protos/client/rolodex/Filter;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ")",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 151
    invoke-virtual {p0, p1}, Lcom/squareup/crm/filters/FilterHelper;->displayValueOf(Lcom/squareup/protos/client/rolodex/Filter;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$bEikBgzd3PfuflCbeEe4X6mfmtM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/crm/filters/-$$Lambda$FilterHelper$bEikBgzd3PfuflCbeEe4X6mfmtM;-><init>(Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 152
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
