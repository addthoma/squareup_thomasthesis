.class public Lcom/squareup/crm/filters/SingleOptionFilterHelper;
.super Ljava/lang/Object;
.source "SingleOptionFilterHelper.java"


# static fields
.field private static final FILTER_BOOLEAN_OPTION_FALSE:Ljava/lang/String; = "false"

.field static final FILTER_BOOLEAN_OPTION_TRUE:Ljava/lang/String; = "true"


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/crm/filters/SingleOptionFilterHelper;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getAvailableOptions(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter$Option;",
            ">;"
        }
    .end annotation

    .line 26
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 49
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 46
    :pswitch_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile_options:Ljava/util/List;

    return-object p1

    .line 43
    :pswitch_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty_options:Ljava/util/List;

    return-object p1

    .line 40
    :pswitch_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type_options:Ljava/util/List;

    return-object p1

    .line 37
    :pswitch_3
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_options:Ljava/util/List;

    return-object p1

    .line 34
    :pswitch_4
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file_options:Ljava/util/List;

    return-object p1

    .line 31
    :pswitch_5
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days_options:Ljava/util/List;

    return-object p1

    .line 28
    :pswitch_6
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days_options:Ljava/util/List;

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSelected(Lcom/squareup/protos/client/rolodex/Filter;)Lcom/squareup/protos/client/rolodex/Filter$Option;
    .locals 2

    .line 64
    sget-object v0, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    .line 95
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 90
    :pswitch_0
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    .line 91
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->iip_is_instant_profile:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_0
    return-object v1

    .line 86
    :pswitch_1
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hl_has_loyalty:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_1
    return-object v1

    .line 82
    :pswitch_2
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->fb_sentiment_type:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_2
    return-object v1

    .line 78
    :pswitch_3
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cab_attribute_value:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_3
    return-object v1

    .line 74
    :pswitch_4
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->hcof_has_card_on_file:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_4
    return-object v1

    .line 70
    :pswitch_5
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->npilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_5
    return-object v1

    .line 66
    :pswitch_6
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->lpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    :cond_6
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public isSelected(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Z
    .locals 0

    .line 54
    invoke-virtual {p0, p1}, Lcom/squareup/crm/filters/SingleOptionFilterHelper;->getSelected(Lcom/squareup/protos/client/rolodex/Filter;)Lcom/squareup/protos/client/rolodex/Filter$Option;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 55
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isValid(Lcom/squareup/protos/client/rolodex/Filter;)Z
    .locals 0

    .line 59
    invoke-virtual {p0, p1}, Lcom/squareup/crm/filters/SingleOptionFilterHelper;->getSelected(Lcom/squareup/protos/client/rolodex/Filter;)Lcom/squareup/protos/client/rolodex/Filter$Option;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public select(Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 2

    .line 100
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object v0

    .line 102
    sget-object v1, Lcom/squareup/crm/filters/SingleOptionFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    .line 132
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 128
    :pswitch_0
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->iip_is_instant_profile(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 124
    :pswitch_1
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hl_has_loyalty(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 120
    :pswitch_2
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->fb_sentiment_type(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 116
    :pswitch_3
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cab_attribute_value(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 112
    :pswitch_4
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->hcof_has_card_on_file(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 108
    :pswitch_5
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->npilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 104
    :pswitch_6
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->lpilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    .line 135
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
