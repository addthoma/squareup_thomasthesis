.class public final Lcom/squareup/crm/RolodexContactLoader$SearchTerm;
.super Ljava/lang/Object;
.source "RolodexContactLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/RolodexContactLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchTerm"
.end annotation


# instance fields
.field public final debounce:Z

.field public final term:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->term:Ljava/lang/String;

    .line 61
    iput-boolean p2, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->debounce:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 68
    :cond_1
    check-cast p1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    .line 69
    iget-object v2, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->term:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->term:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->debounce:Z

    iget-boolean p1, p1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->debounce:Z

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 74
    iget-object v1, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->term:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;->debounce:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
