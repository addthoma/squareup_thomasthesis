.class final Lcom/squareup/crashnado/CrashnadoNative$1;
.super Ljava/lang/Thread;
.source "CrashnadoNative.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crashnado/CrashnadoNative;->reportPreviousCrash(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$crashDirectory:Ljava/io/File;

.field final synthetic val$crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/io/File;Lcom/squareup/crashnado/CrashnadoReporter;)V
    .locals 0

    .line 46
    iput-object p2, p0, Lcom/squareup/crashnado/CrashnadoNative$1;->val$crashDirectory:Ljava/io/File;

    iput-object p3, p0, Lcom/squareup/crashnado/CrashnadoNative$1;->val$crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/squareup/crashnado/CrashnadoNative$1;->val$crashDirectory:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 50
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 51
    iget-object v4, p0, Lcom/squareup/crashnado/CrashnadoNative$1;->val$crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    invoke-static {v4, v3}, Lcom/squareup/crashnado/CrashnadoNative;->access$000(Lcom/squareup/crashnado/CrashnadoReporter;Ljava/io/File;)V

    .line 52
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_0

    .line 53
    iget-object v3, p0, Lcom/squareup/crashnado/CrashnadoNative$1;->val$crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    const-string v4, "Failed to delete prior crash"

    invoke-interface {v3, v4}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
