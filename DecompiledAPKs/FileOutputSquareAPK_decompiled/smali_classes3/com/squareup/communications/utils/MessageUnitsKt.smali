.class public final Lcom/squareup/communications/utils/MessageUnitsKt;
.super Ljava/lang/Object;
.source "MessageUnits.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMessageUnits.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MessageUnits.kt\ncom/squareup/communications/utils/MessageUnitsKt\n*L\n1#1,127:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u001b\u001a\u00020\u001c*\u00020\rH\u0000\u001a\u000c\u0010\u001d\u001a\u00020\u001e*\u00020\u001fH\u0002\u001a\u000c\u0010 \u001a\u00020!*\u00020\"H\u0002\"\u001a\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u001a\u0010\u0005\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\"\u001a\u0010\u0007\u001a\u0004\u0018\u00010\u0008*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\"\u0018\u0010\u000b\u001a\u00020\u000c*\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\"\u0018\u0010\u0010\u001a\u00020\u0011*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0012\"\u001a\u0010\u0013\u001a\u0004\u0018\u00010\u0014*\u00020\r8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016\"\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0004\"\u001a\u0010\u0019\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u0004\u00a8\u0006#"
    }
    d2 = {
        "bodyOrNull",
        "",
        "Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;",
        "getBodyOrNull",
        "(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;",
        "browserDialogBodyOrNull",
        "getBrowserDialogBodyOrNull",
        "clientActionOrNull",
        "Lcom/squareup/protos/client/ClientAction;",
        "getClientActionOrNull",
        "(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/client/ClientAction;",
        "content",
        "Lcom/squareup/communications/Message$Content;",
        "Lcom/squareup/protos/messageservice/service/MessageUnit;",
        "getContent",
        "(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message$Content;",
        "isAggregated",
        "",
        "(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z",
        "notificationContentOrNull",
        "Lcom/squareup/communications/Message$Content$NotificationContent;",
        "getNotificationContentOrNull",
        "(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message$Content$NotificationContent;",
        "titleOrNull",
        "getTitleOrNull",
        "urlOrNull",
        "getUrlOrNull",
        "toMessage",
        "Lcom/squareup/communications/Message;",
        "toMessageState",
        "Lcom/squareup/communications/Message$State;",
        "Lcom/squareup/protos/messageservice/service/State;",
        "toType",
        "Lcom/squareup/communications/Message$Type;",
        "Lcom/squareup/protos/messageservice/service/MessageClass;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final getBodyOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;
    .locals 1

    .line 108
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_body:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->body:Ljava/lang/String;

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const-string p0, ""

    .line 109
    :goto_1
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x0

    :cond_2
    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method private static final getBrowserDialogBodyOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;
    .locals 1

    .line 124
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_browser_dialog_body:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->browser_dialog_body:Ljava/lang/String;

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const-string p0, ""

    .line 125
    :goto_1
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x0

    :cond_2
    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method private static final getClientActionOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/client/ClientAction;
    .locals 1

    .line 119
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_client_action:Lcom/squareup/protos/client/ClientAction;

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->client_action:Lcom/squareup/protos/client/ClientAction;

    :goto_0
    return-object p0
.end method

.method private static final getContent(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message$Content;
    .locals 0

    .line 77
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getNotificationContentOrNull(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message$Content$NotificationContent;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/communications/Message$Content$UnsupportedContent;->INSTANCE:Lcom/squareup/communications/Message$Content$UnsupportedContent;

    :goto_0
    check-cast p0, Lcom/squareup/communications/Message$Content;

    return-object p0
.end method

.method private static final getNotificationContentOrNull(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message$Content$NotificationContent;
    .locals 9

    .line 85
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->notification_message_format:Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    .line 87
    new-instance v8, Lcom/squareup/communications/Message$Content$NotificationContent;

    .line 88
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z

    move-result v2

    .line 89
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getTitleOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 90
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getBodyOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 91
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getUrlOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;

    move-result-object v5

    .line 92
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getClientActionOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Lcom/squareup/protos/client/ClientAction;

    move-result-object v6

    .line 93
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getBrowserDialogBodyOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;

    move-result-object v7

    move-object v1, v8

    .line 87
    invoke-direct/range {v1 .. v7}, Lcom/squareup/communications/Message$Content$NotificationContent;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ClientAction;Ljava/lang/String;)V

    return-object v8

    :cond_0
    return-object v0
.end method

.method private static final getTitleOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;
    .locals 1

    .line 102
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->title:Ljava/lang/String;

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const-string p0, ""

    .line 103
    :goto_1
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x0

    :cond_2
    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method private static final getUrlOrNull(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Ljava/lang/String;
    .locals 1

    .line 114
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregated_url:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->url:Ljava/lang/String;

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const-string p0, ""

    .line 115
    :goto_1
    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x0

    :cond_2
    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method private static final isAggregated(Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;)Z
    .locals 0

    .line 98
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/NotificationMessageFormat;->aggregatable:Ljava/lang/Boolean;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final toMessage(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message;
    .locals 10

    const-string v0, "$this$toMessage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/communications/Message;

    .line 46
    iget-object v2, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_unit_token:Ljava/lang/String;

    const-string v1, "message_unit_token"

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v4, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_id:Ljava/lang/String;

    const-string v1, "message_id"

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v3, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->request_token:Ljava/lang/String;

    const-string v1, "request_token"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p0}, Lcom/squareup/communications/utils/MessageUnitsKt;->getContent(Lcom/squareup/protos/messageservice/service/MessageUnit;)Lcom/squareup/communications/Message$Content;

    move-result-object v5

    .line 50
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->state:Lcom/squareup/protos/messageservice/service/State;

    const-string v6, "state"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/communications/utils/MessageUnitsKt;->toMessageState(Lcom/squareup/protos/messageservice/service/State;)Lcom/squareup/communications/Message$State;

    move-result-object v6

    .line 51
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->message_class:Lcom/squareup/protos/messageservice/service/MessageClass;

    const-string v7, "message_class"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/communications/utils/MessageUnitsKt;->toType(Lcom/squareup/protos/messageservice/service/MessageClass;)Lcom/squareup/communications/Message$Type;

    move-result-object v7

    .line 52
    iget-object p0, p0, Lcom/squareup/protos/messageservice/service/MessageUnit;->created_at:Ljava/lang/Long;

    const-string v1, "created_at"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Lorg/threeten/bp/Instant;->ofEpochMilli(J)Lorg/threeten/bp/Instant;

    move-result-object v8

    const-string p0, "Instant.ofEpochMilli(created_at)"

    invoke-static {v8, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    .line 45
    invoke-direct/range {v1 .. v8}, Lcom/squareup/communications/Message;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/communications/Message$Content;Lcom/squareup/communications/Message$State;Lcom/squareup/communications/Message$Type;Lorg/threeten/bp/Instant;)V

    return-object v0
.end method

.method private static final toMessageState(Lcom/squareup/protos/messageservice/service/State;)Lcom/squareup/communications/Message$State;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/State;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 58
    sget-object p0, Lcom/squareup/communications/Message$State$Dismissed;->INSTANCE:Lcom/squareup/communications/Message$State$Dismissed;

    check-cast p0, Lcom/squareup/communications/Message$State;

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 57
    :cond_1
    sget-object p0, Lcom/squareup/communications/Message$State$Read;->INSTANCE:Lcom/squareup/communications/Message$State$Read;

    check-cast p0, Lcom/squareup/communications/Message$State;

    goto :goto_0

    .line 56
    :cond_2
    sget-object p0, Lcom/squareup/communications/Message$State$Unread;->INSTANCE:Lcom/squareup/communications/Message$State$Unread;

    check-cast p0, Lcom/squareup/communications/Message$State;

    :goto_0
    return-object p0
.end method

.method private static final toType(Lcom/squareup/protos/messageservice/service/MessageClass;)Lcom/squareup/communications/Message$Type;
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/communications/utils/MessageUnitsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/MessageClass;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 74
    sget-object p0, Lcom/squareup/communications/Message$Type$Unsupported;->INSTANCE:Lcom/squareup/communications/Message$Type$Unsupported;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 73
    :pswitch_0
    sget-object p0, Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;->INSTANCE:Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 72
    :pswitch_1
    sget-object p0, Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;->INSTANCE:Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 71
    :pswitch_2
    sget-object p0, Lcom/squareup/communications/Message$Type$MarketingToCustomersYouWantToAcquire;->INSTANCE:Lcom/squareup/communications/Message$Type$MarketingToCustomersYouWantToAcquire;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 70
    :pswitch_3
    sget-object p0, Lcom/squareup/communications/Message$Type$AlertSupportCenter;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSupportCenter;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 69
    :pswitch_4
    sget-object p0, Lcom/squareup/communications/Message$Type$AlertSetupPos;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSetupPos;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 68
    :pswitch_5
    sget-object p0, Lcom/squareup/communications/Message$Type$AlertResolveDispute;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertResolveDispute;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 67
    :pswitch_6
    sget-object p0, Lcom/squareup/communications/Message$Type$AlertHighPriority;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertHighPriority;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 66
    :pswitch_7
    sget-object p0, Lcom/squareup/communications/Message$Type$AlertCustomerComms;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertCustomerComms;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    goto :goto_0

    .line 65
    :pswitch_8
    sget-object p0, Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;

    check-cast p0, Lcom/squareup/communications/Message$Type;

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
