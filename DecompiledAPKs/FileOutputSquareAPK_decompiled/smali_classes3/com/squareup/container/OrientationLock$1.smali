.class synthetic Lcom/squareup/container/OrientationLock$1;
.super Ljava/lang/Object;
.source "OrientationLock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/OrientationLock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 137
    invoke-static {}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->values()[Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    :try_start_0
    sget-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_LANDSCAPE:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/container/OrientationLock$1;->$SwitchMap$com$squareup$workflow$WorkflowViewFactory$Orientation:[I

    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v1}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    return-void
.end method
