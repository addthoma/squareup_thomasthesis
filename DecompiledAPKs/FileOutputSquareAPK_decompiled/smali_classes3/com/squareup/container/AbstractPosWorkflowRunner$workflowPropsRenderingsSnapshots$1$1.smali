.class final Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1;->invoke(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0004\"\u0004\u0008\u0002\u0010\u00032\u0018\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00080\u0007H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "P",
        "R",
        "",
        "O",
        "it",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;

    invoke-direct {v0}, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;-><init>()V

    sput-object v0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;->INSTANCE:Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/RenderingAndSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering<",
            "TP;TR;>;>;)",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TP;TR;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 480
    new-instance v0, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    .line 481
    invoke-virtual {p1}, Lcom/squareup/workflow/RenderingAndSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;

    invoke-virtual {v1}, Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;->getProps()Ljava/lang/Object;

    move-result-object v1

    .line 482
    invoke-virtual {p1}, Lcom/squareup/workflow/RenderingAndSnapshot;->getRendering()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;

    invoke-virtual {v2}, Lcom/squareup/container/RenderedPropsWorkflow$PropsAndRendering;->getWrappedRendering()Ljava/lang/Object;

    move-result-object v2

    .line 483
    invoke-virtual {p1}, Lcom/squareup/workflow/RenderingAndSnapshot;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    .line 480
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/workflow/RenderingAndSnapshot;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowPropsRenderingsSnapshots$1$1;->apply(Lcom/squareup/workflow/RenderingAndSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    move-result-object p1

    return-object p1
.end method
