.class public final Lcom/squareup/container/PosWorkflowRunnerKt;
.super Ljava/lang/Object;
.source "PosWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PosWorkflowRunner.kt\ncom/squareup/container/PosWorkflowRunnerKt\n*L\n1#1,73:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u0002H\u0001\"\u000e\u0008\u0000\u0010\u0001\u0018\u0001*\u0006\u0012\u0002\u0008\u00030\u0002*\u00020\u0003H\u0086\u0008\u00a2\u0006\u0002\u0010\u0004\u001a%\u0010\u0000\u001a\u0002H\u0001\"\u000c\u0008\u0000\u0010\u0001*\u0006\u0012\u0002\u0008\u00030\u0002*\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "getWorkflowRunner",
        "W",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lmortar/MortarScope;",
        "(Lmortar/MortarScope;)Lcom/squareup/container/PosWorkflowRunner;",
        "name",
        "",
        "(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic getWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/container/PosWorkflowRunner;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<W::",
            "Lcom/squareup/container/PosWorkflowRunner<",
            "*>;>(",
            "Lmortar/MortarScope;",
            ")TW;"
        }
    .end annotation

    const-string v0, "$this$getWorkflowRunner"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    const-string v1, "W"

    .line 72
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Lcom/squareup/container/PosWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "W::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/container/PosWorkflowRunnerKt;->getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;

    move-result-object p0

    return-object p0
.end method

.method public static final getWorkflowRunner(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/PosWorkflowRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<W::",
            "Lcom/squareup/container/PosWorkflowRunner<",
            "*>;>(",
            "Lmortar/MortarScope;",
            "Ljava/lang/String;",
            ")TW;"
        }
    .end annotation

    const-string v0, "$this$getWorkflowRunner"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0, p1}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/container/PosWorkflowRunner;

    return-object p0

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Required value was null."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
