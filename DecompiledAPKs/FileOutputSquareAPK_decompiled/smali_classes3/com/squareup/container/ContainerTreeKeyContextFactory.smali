.class public Lcom/squareup/container/ContainerTreeKeyContextFactory;
.super Ljava/lang/Object;
.source "ContainerTreeKeyContextFactory.java"

# interfaces
.implements Lflow/path/PathContextFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;
    }
.end annotation


# instance fields
.field private final flowProvider:Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;


# direct methods
.method private constructor <init>(Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/container/ContainerTreeKeyContextFactory;->flowProvider:Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;

    return-void
.end method

.method static forContainerPresenter(Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;)Lcom/squareup/container/ContainerTreeKeyContextFactory;
    .locals 2

    .line 35
    new-instance v0, Lcom/squareup/container/ContainerTreeKeyContextFactory;

    const-string v1, "flowProvider"

    invoke-static {p0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;

    invoke-direct {v0, p0}, Lcom/squareup/container/ContainerTreeKeyContextFactory;-><init>(Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;)V

    return-object v0
.end method

.method public static forLegacyFlows()Lcom/squareup/container/ContainerTreeKeyContextFactory;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/container/ContainerTreeKeyContextFactory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/ContainerTreeKeyContextFactory;-><init>(Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;)V

    return-object v0
.end method

.method private getScreenScope(Landroid/content/Context;Lcom/squareup/container/ContainerTreeKey;)Lmortar/MortarScope;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">(",
            "Landroid/content/Context;",
            "TT;)",
            "Lmortar/MortarScope;"
        }
    .end annotation

    .line 56
    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    .line 58
    invoke-virtual {p2}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, "New scope %s"

    .line 61
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    invoke-virtual {p2, p1}, Lcom/squareup/container/ContainerTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p2

    .line 65
    iget-object v1, p0, Lcom/squareup/container/ContainerTreeKeyContextFactory;->flowProvider:Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;

    if-eqz v1, :cond_0

    const-string v1, "flow.Flow.FLOW_SERVICE"

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 66
    iget-object p1, p0, Lcom/squareup/container/ContainerTreeKeyContextFactory;->flowProvider:Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;

    invoke-interface {p1}, Lcom/squareup/container/ContainerTreeKeyContextFactory$FlowProvider;->getFlow()Lflow/Flow;

    move-result-object p1

    const-string v2, "flow"

    invoke-static {p1, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    .line 67
    invoke-virtual {p2, v1, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 70
    :cond_0
    invoke-virtual {p2, v0}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v1

    :cond_1
    return-object v1
.end method


# virtual methods
.method public setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    .line 43
    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    .line 44
    invoke-direct {p0, p2, p1}, Lcom/squareup/container/ContainerTreeKeyContextFactory;->getScreenScope(Landroid/content/Context;Lcom/squareup/container/ContainerTreeKey;)Lmortar/MortarScope;

    move-result-object p1

    invoke-virtual {p1, p2}, Lmortar/MortarScope;->createContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1
.end method

.method public tearDownContext(Landroid/content/Context;)V
    .locals 3

    .line 48
    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 49
    invoke-virtual {p1}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Destroying scope %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-virtual {p1}, Lmortar/MortarScope;->destroy()V

    return-void
.end method
