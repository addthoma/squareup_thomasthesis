.class public final Lcom/squareup/container/WorkflowCardOverSheetKey;
.super Lcom/squareup/container/WorkflowLayoutKey;
.source "WorkflowCardOverSheetKey.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/WorkflowCardOverSheetKey$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowCardOverSheetKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowCardOverSheetKey.kt\ncom/squareup/container/WorkflowCardOverSheetKey\n+ 2 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,28:1\n150#2:29\n24#3,4:30\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowCardOverSheetKey.kt\ncom/squareup/container/WorkflowCardOverSheetKey\n*L\n25#1:29\n25#1,4:30\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014BK\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0005H\u0016\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowCardOverSheetKey;",
        "Lcom/squareup/container/WorkflowLayoutKey;",
        "runnerServiceName",
        "",
        "parent",
        "Lcom/squareup/container/ContainerTreeKey;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "",
        "(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V",
        "reparent",
        "newParent",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/WorkflowCardOverSheetKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/container/WorkflowCardOverSheetKey$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/WorkflowCardOverSheetKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/WorkflowCardOverSheetKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/WorkflowCardOverSheetKey;->Companion:Lcom/squareup/container/WorkflowCardOverSheetKey$Companion;

    .line 25
    sget-object v0, Lcom/squareup/container/WorkflowTreeKey;->Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;

    .line 30
    new-instance v0, Lcom/squareup/container/WorkflowCardOverSheetKey$$special$$inlined$creator$1;

    invoke-direct {v0}, Lcom/squareup/container/WorkflowCardOverSheetKey$$special$$inlined$creator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 33
    sput-object v0, Lcom/squareup/container/WorkflowCardOverSheetKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)V"
        }
    .end annotation

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapshot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct/range {p0 .. p7}, Lcom/squareup/container/WorkflowLayoutKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-void
.end method


# virtual methods
.method public reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowCardOverSheetKey;
    .locals 9

    const-string v0, "newParent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/squareup/container/WorkflowCardOverSheetKey;

    .line 21
    iget-object v2, p0, Lcom/squareup/container/WorkflowCardOverSheetKey;->runnerServiceName:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/container/WorkflowCardOverSheetKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowCardOverSheetKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v5

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowCardOverSheetKey;->getProps()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowCardOverSheetKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v7

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowCardOverSheetKey;->isPersistent()Z

    move-result v8

    move-object v1, v0

    move-object v3, p1

    .line 20
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowCardOverSheetKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-object v0
.end method

.method public bridge synthetic reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;
    .locals 0

    .line 9
    invoke-virtual {p0, p1}, Lcom/squareup/container/WorkflowCardOverSheetKey;->reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowCardOverSheetKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    return-object p1
.end method
