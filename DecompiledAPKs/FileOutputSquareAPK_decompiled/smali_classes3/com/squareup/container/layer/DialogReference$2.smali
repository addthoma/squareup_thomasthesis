.class final Lcom/squareup/container/layer/DialogReference$2;
.super Ljava/lang/Object;
.source "DialogReference.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/layer/DialogReference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$onViewDetachedFromWindow$0(Lflow/path/Path;Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 42
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/path/Path;

    .line 43
    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 44
    invoke-static {p1}, Lcom/squareup/container/Command;->goBack(Lflow/History;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0

    .line 47
    :cond_0
    sget-object p0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, p0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    .line 41
    invoke-static {p1}, Lflow/Flow;->get(Landroid/view/View;)Lflow/Flow;

    move-result-object p1

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/container/layer/-$$Lambda$DialogReference$2$PV_bPE2IeQf08dWeaYp6VWptuyk;

    invoke-direct {v2, v0}, Lcom/squareup/container/layer/-$$Lambda$DialogReference$2$PV_bPE2IeQf08dWeaYp6VWptuyk;-><init>(Lflow/path/Path;)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
