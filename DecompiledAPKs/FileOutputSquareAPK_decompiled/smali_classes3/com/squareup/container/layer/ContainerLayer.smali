.class public interface abstract Lcom/squareup/container/layer/ContainerLayer;
.super Ljava/lang/Object;
.source "ContainerLayer.java"


# virtual methods
.method public abstract as(Ljava/lang/Class;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract cleanUp()V
.end method

.method public varargs abstract dispatchLayer(Lflow/Traversal;Lflow/TraversalCallback;[Lflow/path/PathContext;)V
.end method

.method public abstract findTopmostScreenToShow(Lflow/History;)Lcom/squareup/container/ContainerTreeKey;
.end method

.method public abstract getVisibleContext()Landroid/content/Context;
.end method

.method public abstract is(Ljava/lang/Class;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)Z"
        }
    .end annotation
.end method

.method public abstract isDefault()Z
.end method

.method public abstract isShowing()Z
.end method

.method public abstract owns(Lcom/squareup/container/ContainerTreeKey;)Z
.end method
