.class public Lcom/squareup/container/TraversalCallbackSet;
.super Ljava/lang/Object;
.source "TraversalCallbackSet.java"


# instance fields
.field private final callbacks:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lflow/TraversalCallback;",
            ">;"
        }
    .end annotation
.end field

.field private released:Z

.field private final wrapped:Lflow/TraversalCallback;


# direct methods
.method public constructor <init>(Lflow/TraversalCallback;)V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/container/TraversalCallbackSet;->callbacks:Ljava/util/Set;

    .line 36
    iput-object p1, p0, Lcom/squareup/container/TraversalCallbackSet;->wrapped:Lflow/TraversalCallback;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/container/TraversalCallbackSet;)Ljava/util/Set;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/container/TraversalCallbackSet;->callbacks:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/container/TraversalCallbackSet;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/container/TraversalCallbackSet;->maybeFire()V

    return-void
.end method

.method private maybeFire()V
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/squareup/container/TraversalCallbackSet;->released:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/TraversalCallbackSet;->callbacks:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/squareup/container/TraversalCallbackSet;->wrapped:Lflow/TraversalCallback;

    invoke-interface {v0}, Lflow/TraversalCallback;->onTraversalCompleted()V

    :cond_0
    return-void
.end method


# virtual methods
.method public add()Lflow/TraversalCallback;
    .locals 2

    .line 40
    new-instance v0, Lcom/squareup/container/TraversalCallbackSet$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/TraversalCallbackSet$1;-><init>(Lcom/squareup/container/TraversalCallbackSet;)V

    .line 48
    iget-object v1, p0, Lcom/squareup/container/TraversalCallbackSet;->callbacks:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public add(Lflow/TraversalCallback;)Lflow/TraversalCallback;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/container/TraversalCallbackSet$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/container/TraversalCallbackSet$2;-><init>(Lcom/squareup/container/TraversalCallbackSet;Lflow/TraversalCallback;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/container/TraversalCallbackSet;->callbacks:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public release()V
    .locals 1

    const/4 v0, 0x1

    .line 68
    iput-boolean v0, p0, Lcom/squareup/container/TraversalCallbackSet;->released:Z

    .line 69
    invoke-direct {p0}, Lcom/squareup/container/TraversalCallbackSet;->maybeFire()V

    return-void
.end method
