.class public final Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "StatelessWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractV1PosWorkflowRunner;->getWorkflow()Lcom/squareup/workflow/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "TO;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "+",
        "Ljava/lang/Object;",
        "+",
        "Ljava/lang/Object;",
        ">;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStatelessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt$stateless$1\n+ 2 AbstractV1PosWorkflowRunner.kt\ncom/squareup/container/AbstractV1PosWorkflowRunner\n*L\n1#1,147:1\n64#2,14:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J)\u0010\u0002\u001a\u00028\u00022\u0006\u0010\u0003\u001a\u00028\u00002\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00028\u00010\u0005H\u0016\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/workflow/StatelessWorkflowKt$stateless$1",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $eventsWorker$inlined:Lcom/squareup/workflow/Worker;

.field final synthetic this$0:Lcom/squareup/container/AbstractV1PosWorkflowRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/container/AbstractV1PosWorkflowRunner;Lcom/squareup/workflow/Worker;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;->this$0:Lcom/squareup/container/AbstractV1PosWorkflowRunner;

    iput-object p2, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;->$eventsWorker$inlined:Lcom/squareup/workflow/Worker;

    .line 86
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "+",
            "Ljava/lang/Object;",
            "+",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    check-cast p1, Lkotlin/Unit;

    .line 148
    iget-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;->this$0:Lcom/squareup/container/AbstractV1PosWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->access$getWorkflowAdapter$p(Lcom/squareup/container/AbstractV1PosWorkflowRunner;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    sget-object p1, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$1$screenState$1;->INSTANCE:Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$1$screenState$1;

    move-object v3, p1

    check-cast v3, Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    .line 150
    iget-object v1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;->$eventsWorker$inlined:Lcom/squareup/workflow/Worker;

    new-instance v0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1$lambda$1;

    invoke-direct {v0, p1}, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1$lambda$1;-><init>(Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;)V

    move-object v3, v0

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object v0, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 157
    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/workflow/ScreenState;

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/workflow/ScreenState;->screen:Ljava/lang/Object;

    check-cast p2, Ljava/util/Map;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p2

    .line 161
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->isSnapshotEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p2, p1}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
