.class Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;
.super Lcom/squareup/container/spot/Spot;
.source "Spots.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/spot/Spots;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RightStableActionBarSpot"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;

    invoke-direct {v0}, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;-><init>()V

    sput-object v0, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;

    .line 91
    sget-object v0, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;

    invoke-static {v0}, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/container/spot/Spot;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/container/spot/Spots$1;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/container/spot/Spots$RightStableActionBarSpot;-><init>()V

    return-void
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 71
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 72
    invoke-virtual {v0, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 73
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideInFromLeft()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 74
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 80
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 81
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 82
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideOutToRight()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 83
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->hide()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 84
    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .line 52
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 54
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideInFromRight()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 55
    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->skipExchange(I)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .line 62
    invoke-static {}, Lcom/squareup/container/spot/ExchangeSet;->createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0, p4}, Lcom/squareup/container/spot/ExchangeSet$Builder;->standardDuration(Landroid/view/View;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 64
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->slideOutToLeft()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/spot/ExchangeSet$Builder;->defaultExchange(Lcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    .line 65
    invoke-static {}, Lcom/squareup/container/spot/Exchangers;->hide()Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/container/spot/ExchangeSet$Builder;->exchange(ILcom/squareup/container/spot/ExchangeSet$Exchanger;)Lcom/squareup/container/spot/ExchangeSet$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0, p1, p2, p4, p3}, Lcom/squareup/container/spot/ExchangeSet$Builder;->start(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public skipTemporaryBackground()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
