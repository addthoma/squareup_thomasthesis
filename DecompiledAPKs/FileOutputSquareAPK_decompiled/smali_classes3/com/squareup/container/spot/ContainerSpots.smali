.class public final Lcom/squareup/container/spot/ContainerSpots;
.super Ljava/lang/Object;
.source "ContainerSpots.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u000bJ\"\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00020\u000bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/container/spot/ContainerSpots;",
        "",
        "()V",
        "helper",
        "Lcom/squareup/container/spot/SpotHelper;",
        "isEnteringFullScreen",
        "",
        "fromScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "toScreen",
        "direction",
        "Lflow/Direction;",
        "whereAreWeGoing",
        "Lcom/squareup/container/spot/Spot;",
        "oldChild",
        "Landroid/view/View;",
        "newChild",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/container/spot/ContainerSpots;

.field private static final helper:Lcom/squareup/container/spot/SpotHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 10
    new-instance v0, Lcom/squareup/container/spot/ContainerSpots;

    invoke-direct {v0}, Lcom/squareup/container/spot/ContainerSpots;-><init>()V

    sput-object v0, Lcom/squareup/container/spot/ContainerSpots;->INSTANCE:Lcom/squareup/container/spot/ContainerSpots;

    .line 11
    new-instance v0, Lcom/squareup/container/spot/SpotHelper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v1}, Lcom/squareup/container/spot/SpotHelper;-><init>(Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/spot/ContainerSpots;->helper:Lcom/squareup/container/spot/SpotHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final whereAreWeGoing(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Lcom/squareup/container/spot/Spot;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "newChild"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/container/spot/ContainerSpots;->helper:Lcom/squareup/container/spot/SpotHelper;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/container/spot/SpotHelper;->whereAreWeGoing(Landroid/view/View;Landroid/view/View;Lflow/Direction;)Lcom/squareup/container/spot/Spot;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final isEnteringFullScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)Z
    .locals 1

    const-string/jumbo v0, "toScreen"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "direction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/squareup/container/spot/ContainerSpots;->helper:Lcom/squareup/container/spot/SpotHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/container/spot/SpotHelper;->isEnteringFullScreen(Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)Z

    move-result p1

    return p1
.end method
