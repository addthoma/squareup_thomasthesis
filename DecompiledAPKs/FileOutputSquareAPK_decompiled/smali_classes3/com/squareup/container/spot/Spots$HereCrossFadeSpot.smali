.class Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;
.super Lcom/squareup/container/spot/Spot;
.source "Spots.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/spot/Spots;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HereCrossFadeSpot"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 190
    new-instance v0, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;

    invoke-direct {v0}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;-><init>()V

    sput-object v0, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;

    .line 219
    sget-object v0, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->INSTANCE:Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;

    invoke-static {v0}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 192
    invoke-direct {p0}, Lcom/squareup/container/spot/Spot;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/container/spot/Spots$1;)V
    .locals 0

    .line 189
    invoke-direct {p0}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;-><init>()V

    return-void
.end method


# virtual methods
.method protected backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 211
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 216
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method protected forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 197
    sget-object p2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 p4, 0x2

    new-array p4, p4, [F

    fill-array-data p4, :array_0

    invoke-static {p3, p2, p4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    .line 198
    invoke-static {p3}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->shortDuration(Landroid/view/View;)I

    move-result p3

    int-to-long p3, p3

    invoke-virtual {p2, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 199
    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 204
    sget-object p2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 p3, 0x2

    new-array p3, p3, [F

    fill-array-data p3, :array_0

    invoke-static {p4, p2, p3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object p2

    .line 205
    invoke-static {p4}, Lcom/squareup/container/spot/Spots$HereCrossFadeSpot;->shortDuration(Landroid/view/View;)I

    move-result p3

    int-to-long p3, p3

    invoke-virtual {p2, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 206
    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
