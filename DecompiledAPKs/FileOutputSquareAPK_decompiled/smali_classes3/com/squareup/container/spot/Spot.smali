.class public abstract Lcom/squareup/container/spot/Spot;
.super Ljava/lang/Object;
.source "Spot.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/spot/Spot$Constants;,
        Lcom/squareup/container/spot/Spot$Direction;
    }
.end annotation


# static fields
.field public static SUPPRESS_ANIMATIONS_FOR_TESTS:Z


# instance fields
.field public final isOverlay:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/Spot;-><init>(Z)V

    return-void
.end method

.method protected constructor <init>(Z)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-boolean p1, p0, Lcom/squareup/container/spot/Spot;->isOverlay:Z

    return-void
.end method

.method public static forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/container/spot/Spot;",
            ">(TT;)",
            "Landroid/os/Parcelable$Creator<",
            "TT;>;"
        }
    .end annotation

    .line 104
    new-instance v0, Lcom/squareup/container/spot/Spot$1;

    invoke-direct {v0, p0}, Lcom/squareup/container/spot/Spot$1;-><init>(Lcom/squareup/container/spot/Spot;)V

    return-object v0
.end method

.method static shortDuration(Landroid/view/View;)I
    .locals 1

    .line 88
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const/high16 v0, 0x10e0000

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p0

    return p0
.end method


# virtual methods
.method public final addEnterAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/container/spot/Spot$Direction;->FORWARD:Lcom/squareup/container/spot/Spot$Direction;

    if-ne p5, v0, :cond_0

    .line 46
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/Spot;->forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 48
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/Spot;->backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method public final addExitAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Lcom/squareup/container/spot/Spot$Direction;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/container/spot/Spot$Direction;->FORWARD:Lcom/squareup/container/spot/Spot$Direction;

    if-ne p5, v0, :cond_0

    .line 55
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/Spot;->forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/Spot;->backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    :goto_0
    return-void
.end method

.method protected abstract backwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
.end method

.method protected abstract backwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public forceOutgoingViewOnTop(Lcom/squareup/container/spot/Spot$Direction;Landroid/view/View;Landroid/view/View;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method protected abstract forwardIncomingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
.end method

.method protected abstract forwardOutgoingAnimation(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
.end method

.method public skipTemporaryBackground()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    return-void
.end method
