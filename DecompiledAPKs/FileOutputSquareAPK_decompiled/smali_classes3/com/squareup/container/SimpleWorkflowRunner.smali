.class public Lcom/squareup/container/SimpleWorkflowRunner;
.super Lcom/squareup/container/AbstractV1PosWorkflowRunner;
.source "SimpleWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/container/AbstractV1PosWorkflowRunner<",
        "TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use this only if you need to run a legacy workflow. Use WorkflowV2Runner for your v2 workflows."
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0017\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0004BA\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\r\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010R\u001d\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\n\u001a\u00020\u000bX\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/container/SimpleWorkflowRunner;",
        "E",
        "",
        "O",
        "Lcom/squareup/container/AbstractV1PosWorkflowRunner;",
        "serviceName",
        "",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "viewFactory",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "starter",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "getStarter",
        "()Lcom/squareup/container/PosWorkflowStarter;",
        "getViewFactory",
        "()Lcom/squareup/workflow/WorkflowViewFactory;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final starter:Lcom/squareup/container/PosWorkflowStarter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;TO;>;"
        }
    .end annotation
.end field

.field private final viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;",
            "Lcom/squareup/workflow/WorkflowViewFactory;",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;+TO;>;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")V"
        }
    .end annotation

    const-string v0, "serviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "starter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1, p2, p5}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;)V

    iput-object p3, p0, Lcom/squareup/container/SimpleWorkflowRunner;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    iput-object p4, p0, Lcom/squareup/container/SimpleWorkflowRunner;->starter:Lcom/squareup/container/PosWorkflowStarter;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 31
    invoke-static {}, Lcom/squareup/container/CoroutineDispatcherKt;->getMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p5

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/container/SimpleWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;Lcom/squareup/container/PosWorkflowStarter;Lkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method


# virtual methods
.method public final getStarter()Lcom/squareup/container/PosWorkflowStarter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;TO;>;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/container/SimpleWorkflowRunner;->starter:Lcom/squareup/container/PosWorkflowStarter;

    return-object v0
.end method

.method protected final getViewFactory()Lcom/squareup/workflow/WorkflowViewFactory;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/container/SimpleWorkflowRunner;->viewFactory:Lcom/squareup/workflow/WorkflowViewFactory;

    return-object v0
.end method
