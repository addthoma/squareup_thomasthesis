.class public final Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner$Companion;
.super Ljava/lang/Object;
.source "DynamicPropsComponentWorkflowV2Runner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JH\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0008\u0008\u0003\u0010\u0005*\u00020\u0001\"\u0008\u0008\u0004\u0010\u0006*\u00020\u0001\"\u0008\u0008\u0005\u0010\u0007*\u00020\u00012\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner$Companion;",
        "",
        "()V",
        "get",
        "Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;",
        "C",
        "P",
        "O",
        "scope",
        "Lmortar/MortarScope;",
        "name",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final get(Lmortar/MortarScope;Ljava/lang/String;)Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C:",
            "Ljava/lang/Object;",
            "P:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lmortar/MortarScope;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner<",
            "TC;TP;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1, p2}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    const-string p2, "scope.getService(name)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/container/DynamicPropsComponentWorkflowV2Runner;

    return-object p1
.end method
