.class public interface abstract Lcom/squareup/container/ContainerBackgroundsProvider;
.super Ljava/lang/Object;
.source "ContainerBackgroundsProvider.java"


# virtual methods
.method public abstract getCardScreenBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getCardScreenScrimColor(Landroid/content/Context;)I
.end method

.method public abstract getWindowBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
.end method
