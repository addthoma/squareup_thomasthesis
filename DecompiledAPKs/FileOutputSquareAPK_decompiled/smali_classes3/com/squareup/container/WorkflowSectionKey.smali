.class public final Lcom/squareup/container/WorkflowSectionKey;
.super Lcom/squareup/container/WorkflowLayoutKey;
.source "WorkflowSectionKey.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/WorkflowSectionKey$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowSectionKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowSectionKey.kt\ncom/squareup/container/WorkflowSectionKey\n+ 2 WorkflowTreeKey.kt\ncom/squareup/container/WorkflowTreeKey$Companion\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,38:1\n150#2:39\n24#3,4:40\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowSectionKey.kt\ncom/squareup/container/WorkflowSectionKey\n*L\n35#1:39\n35#1,4:40\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001d2\u00020\u00012\u00020\u0002:\u0001\u001dBK\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00002\u0006\u0010\u001c\u001a\u00020\u0006H\u0016R\u0018\u0010\u0013\u001a\u0006\u0012\u0002\u0008\u00030\u0014X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/container/WorkflowSectionKey;",
        "Lcom/squareup/container/WorkflowLayoutKey;",
        "Lcom/squareup/container/layer/InSection;",
        "runnerServiceName",
        "",
        "parent",
        "Lcom/squareup/container/ContainerTreeKey;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "props",
        "",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "isPersistent",
        "",
        "(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "context",
        "Landroid/content/Context;",
        "reparent",
        "newParent",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/WorkflowSectionKey;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/container/WorkflowSectionKey$Companion;


# instance fields
.field private final section:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/WorkflowSectionKey$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/WorkflowSectionKey$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/WorkflowSectionKey;->Companion:Lcom/squareup/container/WorkflowSectionKey$Companion;

    .line 35
    sget-object v0, Lcom/squareup/container/WorkflowTreeKey;->Companion:Lcom/squareup/container/WorkflowTreeKey$Companion;

    .line 40
    new-instance v0, Lcom/squareup/container/WorkflowSectionKey$$special$$inlined$creator$1;

    invoke-direct {v0}, Lcom/squareup/container/WorkflowSectionKey$$special$$inlined$creator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 43
    sput-object v0, Lcom/squareup/container/WorkflowSectionKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ScreenHint;",
            "Z)V"
        }
    .end annotation

    const-string v0, "runnerServiceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapshot"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "props"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hint"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct/range {p0 .. p7}, Lcom/squareup/container/WorkflowLayoutKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    .line 26
    invoke-virtual {p6}, Lcom/squareup/workflow/ScreenHint;->getSection()Ljava/lang/Class;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iput-object p1, p0, Lcom/squareup/container/WorkflowSectionKey;->section:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/container/WorkflowSectionKey;->section:Ljava/lang/Class;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/container/WorkflowSectionKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/workflow/ScreenHint;->getSpot()Lcom/squareup/container/spot/Spot;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE:Lcom/squareup/container/spot/Spot;

    const-string v0, "Spots.HERE"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowSectionKey;
    .locals 9

    const-string v0, "newParent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/container/WorkflowSectionKey;

    .line 30
    iget-object v2, p0, Lcom/squareup/container/WorkflowSectionKey;->runnerServiceName:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/container/WorkflowSectionKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowSectionKey;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v5

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowSectionKey;->getProps()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowSectionKey;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v7

    invoke-virtual {p0}, Lcom/squareup/container/WorkflowSectionKey;->isPersistent()Z

    move-result v8

    move-object v1, v0

    move-object v3, p1

    .line 29
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowSectionKey;-><init>(Ljava/lang/String;Lcom/squareup/container/ContainerTreeKey;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lcom/squareup/workflow/ScreenHint;Z)V

    return-object v0
.end method

.method public bridge synthetic reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowTreeKey;
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/container/WorkflowSectionKey;->reparent(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/WorkflowSectionKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/WorkflowTreeKey;

    return-object p1
.end method
