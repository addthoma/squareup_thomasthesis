.class final Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ContainerPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerPresenter;->getVisiblePathContextsAround(I)[Lflow/path/PathContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/container/layer/ContainerLayer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "index",
        "",
        "layer",
        "Lcom/squareup/container/layer/ContainerLayer;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $indexOfInterest:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    iput p1, p0, Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$1;->$indexOfInterest:I

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/container/layer/ContainerLayer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$1;->invoke(ILcom/squareup/container/layer/ContainerLayer;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(ILcom/squareup/container/layer/ContainerLayer;)Z
    .locals 1

    const-string v0, "layer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 483
    iget v0, p0, Lcom/squareup/container/ContainerPresenter$getVisiblePathContextsAround$1;->$indexOfInterest:I

    if-eq p1, v0, :cond_0

    invoke-interface {p2}, Lcom/squareup/container/layer/ContainerLayer;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
