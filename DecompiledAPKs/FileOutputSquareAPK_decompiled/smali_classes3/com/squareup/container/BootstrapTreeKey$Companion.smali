.class public final Lcom/squareup/container/BootstrapTreeKey$Companion;
.super Ljava/lang/Object;
.source "BootstrapTreeKey.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/container/BootstrapTreeKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBootstrapTreeKey.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BootstrapTreeKey.kt\ncom/squareup/container/BootstrapTreeKey$Companion\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n+ 3 Histories.kt\ncom/squareup/container/Histories\n*L\n1#1,113:1\n415#2,2:114\n136#3:116\n150#3:117\n161#3,8:118\n*E\n*S KotlinDebug\n*F\n+ 1 BootstrapTreeKey.kt\ncom/squareup/container/BootstrapTreeKey$Companion\n*L\n73#1,2:114\n85#1:116\n85#1:117\n85#1,8:118\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/container/BootstrapTreeKey$Companion;",
        "",
        "()V",
        "redirectToRemoveSelfAndOverrideDirection",
        "Lcom/squareup/container/RedirectStep$Result;",
        "traversal",
        "Lflow/Traversal;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final redirectToRemoveSelfAndOverrideDirection(Lflow/Traversal;)Lcom/squareup/container/RedirectStep$Result;
    .locals 12

    const-string/jumbo v0, "traversal"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->framesFromTop()Ljava/lang/Iterable;

    move-result-object v0

    const-string/jumbo v1, "traversal.origin.framesFromTop<Any>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 115
    sget-object v1, Lcom/squareup/container/BootstrapTreeKey$Companion$redirectToRemoveSelfAndOverrideDirection$$inlined$filterIsInstance$1;->INSTANCE:Lcom/squareup/container/BootstrapTreeKey$Companion$redirectToRemoveSelfAndOverrideDirection$$inlined$filterIsInstance$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 74
    invoke-static {v0}, Lkotlin/sequences/SequencesKt;->firstOrNull(Lkotlin/sequences/Sequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/BootstrapTreeKey;

    const/4 v1, 0x0

    if-eqz v0, :cond_8

    .line 79
    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v2}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v2

    if-ne v0, v2, :cond_0

    return-object v1

    .line 83
    :cond_0
    invoke-static {v0}, Lcom/squareup/container/BootstrapTreeKey;->access$getHasRegistered$p(Lcom/squareup/container/BootstrapTreeKey;)Z

    move-result v2

    if-nez v2, :cond_1

    return-object v1

    .line 85
    :cond_1
    iget-object v2, p1, Lflow/Traversal;->destination:Lflow/History;

    const-string/jumbo v3, "traversal.destination"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v2

    const-string v3, "buildUpon()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    new-instance v3, Ljava/util/ArrayDeque;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Ljava/util/ArrayDeque;-><init>(I)V

    .line 120
    :goto_0
    invoke-virtual {v2}, Lflow/History$Builder;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v2}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v5

    const-string v6, "peek()"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 121
    invoke-virtual {v2}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_0

    .line 89
    :cond_2
    invoke-virtual {v2}, Lflow/History$Builder;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    return-object v1

    .line 91
    :cond_3
    invoke-virtual {v2}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_4

    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    .line 124
    :goto_2
    move-object v1, v3

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v4

    if-eqz v1, :cond_5

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_2

    .line 116
    :cond_5
    invoke-virtual {v2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v1

    const-string v2, "buildUpon().withPoppedUntil(key, block).build()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v0}, Lcom/squareup/container/BootstrapTreeKey;->getDirectionOverride()Lflow/Direction;

    move-result-object v2

    if-nez v2, :cond_6

    .line 99
    new-instance v0, Lcom/squareup/container/RedirectStep$Result;

    .line 101
    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-static {v1, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    const-string v1, "Redirecting to remove BootstrapTreeKey"

    .line 99
    invoke-direct {v0, v1, p1}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    goto :goto_3

    .line 104
    :cond_6
    new-instance v2, Lcom/squareup/container/RedirectStep$Result;

    .line 105
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Redirecting to remove BootstrapTreeKey and override direction "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "(s/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    iget-object p1, p1, Lflow/Traversal;->direction:Lflow/Direction;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2f

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/container/BootstrapTreeKey;->getDirectionOverride()Lflow/Direction;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "/)"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 107
    invoke-virtual {v0}, Lcom/squareup/container/BootstrapTreeKey;->getDirectionOverride()Lflow/Direction;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object v0

    .line 104
    invoke-direct {v2, p1, v0}, Lcom/squareup/container/RedirectStep$Result;-><init>(Ljava/lang/String;Lcom/squareup/container/Command;)V

    move-object v0, v2

    :goto_3
    return-object v0

    .line 92
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected destination history to contain "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ".\n\t"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {v2}, Lflow/History$Builder;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v3

    const-string v0, "framesFromBottom<Any>()"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "\n\t"

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x3e

    const/4 v11, 0x0

    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 91
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_8
    return-object v1

    .line 115
    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type kotlin.sequences.Sequence<R>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
