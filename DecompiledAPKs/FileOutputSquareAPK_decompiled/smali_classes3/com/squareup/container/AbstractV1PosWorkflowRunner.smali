.class public abstract Lcom/squareup/container/AbstractV1PosWorkflowRunner;
.super Lcom/squareup/container/AbstractPosWorkflowRunner;
.source "AbstractV1PosWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "O:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/container/AbstractPosWorkflowRunner<",
        "Lkotlin/Unit;",
        "TO;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractV1PosWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractV1PosWorkflowRunner.kt\ncom/squareup/container/AbstractV1PosWorkflowRunner\n+ 2 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt\n*L\n1#1,106:1\n318#2,2:107\n276#2:110\n328#2:111\n203#3:109\n86#4,6:112\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractV1PosWorkflowRunner.kt\ncom/squareup/container/AbstractV1PosWorkflowRunner\n*L\n61#1,2:107\n61#1:110\n61#1:111\n61#1:109\n63#1,6:112\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u0002H\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0004B%\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\\\u0010$\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2,\u0010%\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\nH\u0004J\u0015\u0010&\u001a\u00020\u00052\u0006\u0010\'\u001a\u00028\u0000H\u0004\u00a2\u0006\u0002\u0010(J4\u0010)\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H+0*\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0002\"\u0004\u0008\u0003\u0010+*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H+0*H\u0004R\u0016\u0010\u0013\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0016X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018RL\u0010\u0019\u001a:\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u0001\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u001a8DX\u0084\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001b\u0010\u001cR\u0091\u0001\u0010\u001d\u001ax\u0012\u0004\u0012\u00020\u0005\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u001f\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u001f0\u001e8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\"\u0010#\u001a\u0004\u0008 \u0010!\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/container/AbstractV1PosWorkflowRunner;",
        "E",
        "",
        "O",
        "Lcom/squareup/container/AbstractPosWorkflowRunner;",
        "",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "serviceName",
        "",
        "nextHistory",
        "Lio/reactivex/Observable;",
        "Lflow/History;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "eventsChannel",
        "Lkotlinx/coroutines/channels/Channel;",
        "starter",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "getStarter",
        "()Lcom/squareup/container/PosWorkflowStarter;",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "workflowAdapter",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "Lcom/squareup/workflow/ScreenState;",
        "getWorkflowAdapter",
        "()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "workflowAdapter$delegate",
        "Lkotlin/Lazy;",
        "renderingToLayering",
        "rendering",
        "sendEvent",
        "event",
        "(Ljava/lang/Object;)V",
        "dedupForLegacyWorkflows",
        "Lcom/squareup/workflow/rx2/RxWorkflowHost;",
        "R",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private eventsChannel:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final workflowAdapter$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Lflow/History;",
            ">;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")V"
        }
    .end annotation

    const-string v0, "serviceName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextHistory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 30
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/container/AbstractPosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;ZLkotlinx/coroutines/CoroutineDispatcher;)V

    .line 37
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 55
    new-instance p1, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflowAdapter$2;

    invoke-direct {p1, p0}, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflowAdapter$2;-><init>(Lcom/squareup/container/AbstractV1PosWorkflowRunner;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->workflowAdapter$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 29
    invoke-static {}, Lcom/squareup/container/CoroutineDispatcherKt;->getMainImmediateDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lkotlinx/coroutines/CoroutineDispatcher;)V

    return-void
.end method

.method public static final synthetic access$getWorkflowAdapter$p(Lcom/squareup/container/AbstractV1PosWorkflowRunner;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->getWorkflowAdapter()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object p0

    return-object p0
.end method

.method private final getWorkflowAdapter()Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;TE;TO;",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->workflowAdapter$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-object v0
.end method


# virtual methods
.method protected final dedupForLegacyWorkflows(Lcom/squareup/workflow/rx2/RxWorkflowHost;)Lcom/squareup/workflow/rx2/RxWorkflowHost;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "+TO;TR;>;)",
            "Lcom/squareup/workflow/rx2/RxWorkflowHost<",
            "TO;TR;>;"
        }
    .end annotation

    const-string v0, "$this$dedupForLegacyWorkflows"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    new-instance v0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;

    invoke-direct {v0, p1}, Lcom/squareup/container/AbstractV1PosWorkflowRunner$dedupForLegacyWorkflows$1;-><init>(Lcom/squareup/workflow/rx2/RxWorkflowHost;)V

    check-cast v0, Lcom/squareup/workflow/rx2/RxWorkflowHost;

    return-object v0
.end method

.method public abstract getStarter()Lcom/squareup/container/PosWorkflowStarter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TE;TO;>;"
        }
    .end annotation
.end method

.method protected final getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "Lkotlin/Unit;",
            "TO;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const v0, 0x7fffffff

    .line 59
    invoke-static {v0}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->eventsChannel:Lkotlinx/coroutines/channels/Channel;

    .line 61
    check-cast v0, Lkotlinx/coroutines/channels/ReceiveChannel;

    .line 108
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$asWorker$1;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$asWorker$1;-><init>(Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    .line 109
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 110
    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    .line 63
    sget-object v0, Lcom/squareup/workflow/Workflow;->Companion:Lcom/squareup/workflow/Workflow$Companion;

    .line 112
    new-instance v0, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;

    invoke-direct {v0, p0, v2}, Lcom/squareup/container/AbstractV1PosWorkflowRunner$workflow$$inlined$stateless$1;-><init>(Lcom/squareup/container/AbstractV1PosWorkflowRunner;Lcom/squareup/workflow/Worker;)V

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public bridge synthetic renderingToLayering(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0

    .line 26
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->renderingToLayering(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method protected final renderingToLayering(Ljava/util/Map;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected final sendEvent(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/squareup/container/AbstractV1PosWorkflowRunner;->eventsChannel:Lkotlinx/coroutines/channels/Channel;

    if-eqz v0, :cond_0

    .line 87
    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/Channel;->offer(Ljava/lang/Object;)Z

    return-void

    .line 86
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Expected eventsChannel to be set."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 85
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Workflow must be running to receive events."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
