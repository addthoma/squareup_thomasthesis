.class public abstract Lcom/squareup/jedi/JediHelpScreenData;
.super Ljava/lang/Object;
.source "JediHelpScreenData.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;,
        Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;,
        Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;,
        Lcom/squareup/jedi/JediHelpScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00032\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0007\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "Landroid/os/Parcelable;",
        "()V",
        "Companion",
        "JediHelpErrorScreenData",
        "JediHelpLoadingScreenData",
        "JediHelpPanelScreenData",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/jedi/JediHelpScreenData$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/jedi/JediHelpScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/jedi/JediHelpScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/jedi/JediHelpScreenData;->Companion:Lcom/squareup/jedi/JediHelpScreenData$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/jedi/JediHelpScreenData;-><init>()V

    return-void
.end method
