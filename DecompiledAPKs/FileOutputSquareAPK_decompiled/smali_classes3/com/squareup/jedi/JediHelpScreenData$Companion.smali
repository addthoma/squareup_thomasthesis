.class public final Lcom/squareup/jedi/JediHelpScreenData$Companion;
.super Ljava/lang/Object;
.source "JediHelpScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/JediHelpScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediHelpScreenData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediHelpScreenData.kt\ncom/squareup/jedi/JediHelpScreenData$Companion\n*L\n1#1,127:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J0\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/jedi/JediHelpScreenData$Companion;",
        "",
        "()V",
        "createComponentItem",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        "index",
        "",
        "component",
        "Lcom/squareup/protos/jedi/service/Component;",
        "resources",
        "Landroid/content/res/Resources;",
        "canCall",
        "",
        "screenDataFromPanel",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "panel",
        "Lcom/squareup/protos/jedi/service/Panel;",
        "transitionId",
        "",
        "jediHelpSettings",
        "Lcom/squareup/jedi/JediHelpSettings;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/squareup/jedi/JediHelpScreenData$Companion;-><init>()V

    return-void
.end method

.method private final createComponentItem(ILcom/squareup/protos/jedi/service/Component;Landroid/content/res/Resources;Z)Lcom/squareup/jedi/ui/JediComponentItem;
    .locals 2

    .line 106
    iget-object v0, p2, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    if-eqz v0, :cond_1

    sget-object v1, Lcom/squareup/jedi/JediHelpScreenData$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 119
    :pswitch_0
    new-instance p1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    :pswitch_1
    const/4 p3, 0x1

    if-le p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 117
    :goto_0
    new-instance p1, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;Ljava/lang/Boolean;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 113
    :pswitch_2
    new-instance p1, Lcom/squareup/jedi/ui/components/JediRowComponentItem;

    invoke-direct {p1, p2, p4}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;Z)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 112
    :pswitch_3
    new-instance p1, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 111
    :pswitch_4
    new-instance p1, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;

    invoke-direct {p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediInstantAnswerComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;Landroid/content/res/Resources;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 110
    :pswitch_5
    new-instance p1, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 109
    :pswitch_6
    new-instance p1, Lcom/squareup/jedi/ui/components/JediIconComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediIconComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 108
    :pswitch_7
    new-instance p1, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    goto :goto_1

    .line 107
    :pswitch_8
    new-instance p1, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;

    invoke-direct {p1, p2}, Lcom/squareup/jedi/ui/components/JediButtonBaseComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    check-cast p1, Lcom/squareup/jedi/ui/JediComponentItem;

    :goto_1
    return-object p1

    .line 121
    :cond_1
    :goto_2
    new-instance p1, Lcom/squareup/jedi/JediServiceException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Component with kind: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " is not supported"

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/jedi/JediServiceException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final screenDataFromPanel(Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Z)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 15

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    const-string v2, "panel"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "jediHelpSettings"

    move-object/from16 v3, p3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "resources"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v6, v2

    check-cast v6, Ljava/util/List;

    const/4 v2, 0x0

    .line 65
    move-object v4, v2

    check-cast v4, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    .line 66
    iget-object v5, v0, Lcom/squareup/protos/jedi/service/Panel;->display_search_bar:Ljava/lang/Boolean;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    .line 67
    check-cast v2, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    .line 69
    iget-object v8, v0, Lcom/squareup/protos/jedi/service/Panel;->components:Ljava/util/List;

    if-eqz v8, :cond_4

    const/4 v8, 0x0

    .line 70
    iget-object v9, v0, Lcom/squareup/protos/jedi/service/Panel;->components:Ljava/util/List;

    const-string v10, "panel.components"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v9, Ljava/lang/Iterable;

    invoke-interface {v9}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/protos/jedi/service/Component;

    .line 71
    iget-object v11, v10, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    if-eqz v11, :cond_3

    sget-object v12, Lcom/squareup/jedi/JediHelpScreenData$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v11}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v11

    aget v11, v12, v11

    if-eq v11, v7, :cond_2

    const/4 v12, 0x2

    if-eq v11, v12, :cond_1

    const/4 v12, 0x3

    if-eq v11, v12, :cond_0

    .line 84
    move-object v11, p0

    check-cast v11, Lcom/squareup/jedi/JediHelpScreenData$Companion;

    const-string v12, "component"

    invoke-static {v10, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v12, p5

    invoke-direct {v11, v8, v10, v1, v12}, Lcom/squareup/jedi/JediHelpScreenData$Companion;->createComponentItem(ILcom/squareup/protos/jedi/service/Component;Landroid/content/res/Resources;Z)Lcom/squareup/jedi/ui/JediComponentItem;

    move-result-object v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    move/from16 v12, p5

    .line 81
    new-instance v4, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-direct {v4, v10}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    goto :goto_1

    :cond_1
    move/from16 v12, p5

    const/4 v5, 0x1

    goto :goto_1

    :cond_2
    move/from16 v12, p5

    .line 73
    new-instance v2, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;

    invoke-direct {v2, v10}, Lcom/squareup/jedi/ui/components/JediBannerComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 83
    :cond_3
    new-instance v0, Lcom/squareup/jedi/JediServiceException;

    const-string v1, "Could not parse component kind properly."

    invoke-direct {v0, v1}, Lcom/squareup/jedi/JediServiceException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    move-object v9, v2

    move-object v7, v4

    move v8, v5

    .line 89
    new-instance v1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    .line 91
    iget-object v5, v0, Lcom/squareup/protos/jedi/service/Panel;->session_token:Ljava/lang/String;

    const-string v0, "panel.session_token"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-interface/range {p3 .. p3}, Lcom/squareup/jedi/JediHelpSettings;->getShowInlineLinks()Z

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x180

    const/4 v14, 0x0

    move-object v3, v1

    move-object/from16 v4, p2

    .line 89
    invoke-direct/range {v3 .. v14}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v1
.end method
