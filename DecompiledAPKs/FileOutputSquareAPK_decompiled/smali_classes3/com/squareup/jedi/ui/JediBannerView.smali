.class public Lcom/squareup/jedi/ui/JediBannerView;
.super Landroid/widget/LinearLayout;
.source "JediBannerView.java"


# instance fields
.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field private final labelChanged:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private labelView:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/jedi/ui/JediBannerView;->labelChanged:Lrx/subjects/BehaviorSubject;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 47
    sget v0, Lcom/squareup/jedi/impl/R$id;->label:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/jedi/ui/JediBannerView;->labelView:Lcom/squareup/marketfont/MarketTextView;

    .line 48
    sget v0, Lcom/squareup/jedi/impl/R$id;->close:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/jedi/ui/JediBannerView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$1$JediBannerView(Ljava/lang/String;)V
    .locals 1

    .line 37
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 38
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediBannerView;->labelView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$JediBannerView(Landroid/view/View;)V
    .locals 0

    const/16 p1, 0x8

    .line 34
    invoke-virtual {p0, p1}, Lcom/squareup/jedi/ui/JediBannerView;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$JediBannerView()Lrx/Subscription;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediBannerView;->labelChanged:Lrx/subjects/BehaviorSubject;

    new-instance v1, Lcom/squareup/jedi/ui/-$$Lambda$JediBannerView$aItaIrS5vncvgWn4NMiaB8Vw4SQ;

    invoke-direct {v1, p0}, Lcom/squareup/jedi/ui/-$$Lambda$JediBannerView$aItaIrS5vncvgWn4NMiaB8Vw4SQ;-><init>(Lcom/squareup/jedi/ui/JediBannerView;)V

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 29
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/jedi/ui/JediBannerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/jedi/impl/R$layout;->jedi_banner_view:I

    invoke-static {v0, v1, p0}, Lcom/squareup/jedi/ui/JediBannerView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 32
    invoke-direct {p0, v0}, Lcom/squareup/jedi/ui/JediBannerView;->bindViews(Landroid/view/View;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediBannerView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/jedi/ui/-$$Lambda$JediBannerView$2C4qc1sTp7Bpphkp-cvnuWyfRIM;

    invoke-direct {v1, p0}, Lcom/squareup/jedi/ui/-$$Lambda$JediBannerView$2C4qc1sTp7Bpphkp-cvnuWyfRIM;-><init>(Lcom/squareup/jedi/ui/JediBannerView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    new-instance v0, Lcom/squareup/jedi/ui/-$$Lambda$JediBannerView$9uswHc3d326Rjwm0-vqxAOSy7vY;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/ui/-$$Lambda$JediBannerView$9uswHc3d326Rjwm0-vqxAOSy7vY;-><init>(Lcom/squareup/jedi/ui/JediBannerView;)V

    invoke-static {p0, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/jedi/ui/JediBannerView;->labelChanged:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
