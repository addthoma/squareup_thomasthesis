.class public Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediSectionHeaderComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 16
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_section_header_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 2

    .line 21
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/jedi/impl/R$id;->section_header:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 23
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;->label()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItem;->applyPadding()Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 29
    iget-object p1, p0, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p3, Lcom/squareup/marin/R$dimen;->marin_gap_large:I

    .line 30
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iget-object p3, p0, Lcom/squareup/jedi/ui/components/JediSectionHeaderComponentItemViewHolder;->itemView:Landroid/view/View;

    .line 31
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    .line 32
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p3

    sub-int/2addr p1, p3

    .line 33
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result p3

    .line 34
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingRight()I

    move-result p1

    .line 35
    invoke-virtual {p2}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v1

    .line 33
    invoke-virtual {p2, p3, v0, p1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_0
    return-void
.end method
