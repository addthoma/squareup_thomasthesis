.class public Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;
.super Lcom/squareup/jedi/ui/JediComponentItem;
.source "JediInputConfirmationComponentItem.java"


# direct methods
.method public constructor <init>(Lcom/squareup/protos/jedi/service/Component;)V
    .locals 1

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/jedi/ui/JediComponentItem;-><init>(Lcom/squareup/protos/jedi/service/Component;)V

    .line 12
    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->kind:Lcom/squareup/protos/jedi/service/ComponentKind;

    sget-object v0, Lcom/squareup/protos/jedi/service/ComponentKind;->INPUT_CONFIRMATION:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/ComponentKind;->equals(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    return-void
.end method


# virtual methods
.method public label()Ljava/lang/String;
    .locals 1

    const-string v0, "label"

    .line 20
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public text()Ljava/lang/String;
    .locals 1

    const-string v0, "text"

    .line 16
    invoke-virtual {p0, v0}, Lcom/squareup/jedi/ui/components/JediInputConfirmationComponentItem;->getStringParameterOrEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
