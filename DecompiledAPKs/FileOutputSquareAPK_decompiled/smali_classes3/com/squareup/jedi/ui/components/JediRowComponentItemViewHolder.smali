.class public Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediRowComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediRowComponentItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lcom/squareup/jedi/ui/JediPanelView;)V
    .locals 1

    .line 23
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_row_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    .line 24
    iput-object p2, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onBind$0$JediRowComponentItemViewHolder(Lcom/squareup/jedi/ui/components/JediRowComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Landroid/view/View;)V
    .locals 1

    .line 68
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canCallPhone()Z

    move-result p4

    if-eqz p4, :cond_0

    .line 69
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->url()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/jedi/JediComponentInputHandler;->onCall(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canMessageUs()Z

    move-result p4

    if-eqz p4, :cond_1

    .line 71
    invoke-interface {p2}, Lcom/squareup/jedi/JediComponentInputHandler;->onMessageUs()V

    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->isSearchResult()Z

    move-result p4

    if-eqz p4, :cond_2

    .line 75
    iget-object p4, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    invoke-virtual {p4}, Lcom/squareup/jedi/ui/JediPanelView;->clearFocus()V

    .line 76
    iget-object p4, p1, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object p4, p4, Lcom/squareup/protos/jedi/service/Component;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->linkPanelToken()Ljava/lang/String;

    move-result-object p1

    .line 77
    invoke-virtual {p3}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getTransitionId()Ljava/lang/String;

    move-result-object p3

    .line 76
    invoke-interface {p2, p4, p1, v0, p3}, Lcom/squareup/jedi/JediComponentInputHandler;->onSearchResultButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasUrl()Z

    move-result p4

    if-eqz p4, :cond_3

    .line 79
    iget-object p3, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->url()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p3, p1}, Lcom/squareup/jedi/JediComponentInputHandler;->onLink(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :cond_3
    iget-object p1, p1, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object p1, p1, Lcom/squareup/protos/jedi/service/Component;->name:Ljava/lang/String;

    invoke-virtual {p3}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSessionToken()Ljava/lang/String;

    move-result-object p4

    .line 82
    invoke-virtual {p3}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getTransitionId()Ljava/lang/String;

    move-result-object p3

    .line 81
    invoke-interface {p2, p1, p4, p3}, Lcom/squareup/jedi/JediComponentInputHandler;->onButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/jedi/ui/components/JediRowComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediRowComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediRowComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 4

    .line 29
    iget-object v0, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/jedi/impl/R$id;->row:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 32
    iget-object v1, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 33
    sget v2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    .line 34
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 35
    invoke-virtual {v0}, Lcom/squareup/ui/account/view/SmartLineRow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 36
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasSublabel()Z

    move-result v3

    if-nez v3, :cond_0

    .line 37
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 39
    :cond_0
    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->label()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 42
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->sublabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasSublabel()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 44
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->valuelabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 45
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasValuelabel()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 47
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setClickable(Z)V

    .line 48
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setFocusable(Z)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->chevronVisibility()Lcom/squareup/marin/widgets/ChevronVisibility;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 52
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    .line 55
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canCallPhone()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->hasUrl()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    const/4 v2, 0x0

    .line 58
    sget-object v3, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->PHONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->HOURS:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;->TOKEN:Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->valueType()Lcom/squareup/jedi/ui/components/JediRowComponentItem$ValueType;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 62
    sget v1, Lcom/squareup/marin/R$color;->marin_black:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    goto :goto_1

    .line 57
    :cond_3
    :goto_0
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 66
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->actionable()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canCallPhone()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediRowComponentItem;->canMessageUs()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 67
    :cond_5
    new-instance v1, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/squareup/jedi/ui/components/-$$Lambda$JediRowComponentItemViewHolder$_VAIW8piDj8yZfRgHne8pNuxaIg;-><init>(Lcom/squareup/jedi/ui/components/JediRowComponentItemViewHolder;Lcom/squareup/jedi/ui/components/JediRowComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    return-void
.end method
