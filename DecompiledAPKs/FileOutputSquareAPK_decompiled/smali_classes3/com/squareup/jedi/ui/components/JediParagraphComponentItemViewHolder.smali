.class public Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediParagraphComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 20
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_paragraph_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 5

    .line 25
    iget-object p3, p0, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/jedi/impl/R$id;->border_layout:I

    invoke-static {p3, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/marin/widgets/BorderedLinearLayout;

    .line 26
    iget-object v0, p0, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/jedi/impl/R$id;->paragraph:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 28
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->isHtml()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    iget-boolean p2, p2, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->showInlineLinks:Z

    if-eqz p2, :cond_0

    .line 30
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->htmlText()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->htmlTextWithoutLinks()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->text()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    :goto_0
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 39
    sget v1, Lcom/squareup/marin/R$dimen;->marin_gap_large:I

    .line 40
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 41
    sget v2, Lcom/squareup/marin/R$dimen;->marin_text_help:I

    .line 42
    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    int-to-float p2, p2

    .line 44
    sget-object v2, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder$1;->$SwitchMap$com$squareup$jedi$ui$components$JediParagraphComponentItem$Emphasis:[I

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem;->emphasis()Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediParagraphComponentItem$Emphasis;->ordinal()I

    move-result p1

    aget p1, v2, p1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq p1, v2, :cond_4

    const/4 v2, 0x2

    const/16 v4, 0x11

    if-eq p1, v2, :cond_3

    const/4 v1, 0x3

    if-eq p1, v1, :cond_2

    goto :goto_1

    .line 56
    :cond_2
    invoke-virtual {p3, v3}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addBorder(I)V

    .line 57
    invoke-virtual {p3, v3, v3, v3, v3}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setPadding(IIII)V

    .line 58
    iget-object p1, p0, Lcom/squareup/jedi/ui/components/JediParagraphComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget p3, Lcom/squareup/marin/R$style;->Widget_Marin_Element_HelpText:I

    invoke-virtual {v0, p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 60
    invoke-virtual {v0, v3, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 61
    invoke-virtual {v0, v4}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    goto :goto_1

    :cond_3
    const/16 p1, 0xf

    .line 51
    invoke-virtual {p3, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addBorder(I)V

    .line 52
    invoke-virtual {p3, v1, v1, v1, v1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setPadding(IIII)V

    .line 53
    invoke-virtual {v0, v4}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    goto :goto_1

    .line 46
    :cond_4
    invoke-virtual {p3, v3}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->addBorder(I)V

    .line 47
    invoke-virtual {p3, v3, v3, v3, v3}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->setPadding(IIII)V

    const p1, 0x800003

    .line 48
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    :goto_1
    return-void
.end method
