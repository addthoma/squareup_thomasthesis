.class public Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediTextFieldComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;",
        ">;"
    }
.end annotation


# static fields
.field private static final inputTypeFlags:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 30
    sget-object v1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;->TEXT:Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    sget-object v1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;->NUMBER:Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    sget-object v1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;->DATE:Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->inputTypeFlags:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 37
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_text_field_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->updateText(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Ljava/lang/String;)V

    return-void
.end method

.method private updateText(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Ljava/lang/String;)V
    .locals 1

    .line 71
    iget-object v0, p1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object v0, v0, Lcom/squareup/protos/jedi/service/Component;->name:Ljava/lang/String;

    invoke-interface {p2, v0, p3}, Lcom/squareup/jedi/JediComponentInputHandler;->onTextInput(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->required()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    iget-object v0, v0, Lcom/squareup/protos/jedi/service/Component;->name:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->minLength()I

    move-result p1

    invoke-interface {p2, v0, p3, p1}, Lcom/squareup/jedi/JediComponentInputHandler;->onRequiredTextInput(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 3

    .line 42
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/jedi/impl/R$id;->label:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    .line 43
    iget-object v0, p0, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/jedi/impl/R$id;->text_field:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketEditText;

    .line 45
    iget-object v1, p0, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->label()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->hasError()Z

    move-result v2

    if-eqz v2, :cond_0

    sget v2, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    :cond_0
    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    :goto_0
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->text()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketEditText;->setText(Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->placeholder()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->multiline()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketEditText;->setSingleLine(Z)V

    .line 55
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->hasError()Z

    move-result p2

    if-eqz p2, :cond_1

    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_edit_text_border_red:I

    goto :goto_1

    :cond_1
    sget p2, Lcom/squareup/marin/R$drawable;->marin_selector_edit_text_border:I

    :goto_1
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketEditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 59
    sget-object p2, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->inputTypeFlags:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->type()Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem$InputType;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketEditText;->setInputType(I)V

    .line 61
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;->text()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p3, p2}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;->updateText(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;Ljava/lang/String;)V

    .line 62
    new-instance p2, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder$1;

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder$1;-><init>(Lcom/squareup/jedi/ui/components/JediTextFieldComponentItemViewHolder;Lcom/squareup/jedi/ui/components/JediTextFieldComponentItem;Lcom/squareup/jedi/JediComponentInputHandler;)V

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method
