.class public final Lcom/squareup/jedi/JediWorkflowService;
.super Ljava/lang/Object;
.source "JediWorkflowService.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediWorkflowService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediWorkflowService.kt\ncom/squareup/jedi/JediWorkflowService\n*L\n1#1,208:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0014\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0018\u001a\u00020\u0019J:\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u00162\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001d2\u000c\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 2\u0008\u0010\"\u001a\u0004\u0018\u00010\u001dH\u0002J\u001e\u0010#\u001a\u0008\u0012\u0004\u0012\u00020$0\u00162\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u001dH\u0002J\u0010\u0010(\u001a\u00020\u00172\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010+\u001a\u00020\u00172\u0006\u0010,\u001a\u00020\u001bH\u0002J \u0010-\u001a\u00020&2\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u001d2\u0006\u0010.\u001a\u00020$H\u0002J\u001c\u0010/\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u001dJ\u000e\u00100\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u0016H\u0002J\u0016\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00162\u0008\u0010\"\u001a\u0004\u0018\u00010\u001dR\u0014\u0010\u0011\u001a\u00020\u00128BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/jedi/JediWorkflowService;",
        "",
        "jediHelpSettings",
        "Lcom/squareup/jedi/JediHelpSettings;",
        "userSettingsProvider",
        "Lcom/squareup/settings/server/UserSettingsProvider;",
        "jediService",
        "Lcom/squareup/server/help/JediService;",
        "locale",
        "Ljava/util/Locale;",
        "customJediComponentItemsFactory",
        "Lcom/squareup/jedi/CustomJediComponentItemsFactory;",
        "resources",
        "Landroid/content/res/Resources;",
        "telephonyManager",
        "Landroid/telephony/TelephonyManager;",
        "(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/server/help/JediService;Ljava/util/Locale;Lcom/squareup/jedi/CustomJediComponentItemsFactory;Landroid/content/res/Resources;Landroid/telephony/TelephonyManager;)V",
        "canCall",
        "",
        "getCanCall",
        "()Z",
        "getNextPanel",
        "Lio/reactivex/Single;",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "jediInput",
        "Lcom/squareup/jedi/JediInput;",
        "getPanelForSession",
        "Lcom/squareup/protos/jedi/service/PanelResponse;",
        "sessionToken",
        "",
        "transitionId",
        "inputs",
        "",
        "Lcom/squareup/protos/jedi/service/Input;",
        "panelToken",
        "getSearchPanel",
        "Lcom/squareup/protos/jedi/service/SearchResponse;",
        "screenData",
        "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
        "searchText",
        "resumeFromErrors",
        "throwable",
        "",
        "screenDataFromPanelResponse",
        "panelResponse",
        "screenDataFromSuccessfulSearchResponse",
        "response",
        "searching",
        "startNewSession",
        "startSessionAndGetPanel",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final customJediComponentItemsFactory:Lcom/squareup/jedi/CustomJediComponentItemsFactory;

.field private final jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

.field private final jediService:Lcom/squareup/server/help/JediService;

.field private final locale:Ljava/util/Locale;

.field private final resources:Landroid/content/res/Resources;

.field private final telephonyManager:Landroid/telephony/TelephonyManager;

.field private final userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/jedi/JediHelpSettings;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/squareup/server/help/JediService;Ljava/util/Locale;Lcom/squareup/jedi/CustomJediComponentItemsFactory;Landroid/content/res/Resources;Landroid/telephony/TelephonyManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "jediHelpSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "userSettingsProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jediService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customJediComponentItemsFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "telephonyManager"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowService;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    iput-object p2, p0, Lcom/squareup/jedi/JediWorkflowService;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    iput-object p3, p0, Lcom/squareup/jedi/JediWorkflowService;->jediService:Lcom/squareup/server/help/JediService;

    iput-object p4, p0, Lcom/squareup/jedi/JediWorkflowService;->locale:Ljava/util/Locale;

    iput-object p5, p0, Lcom/squareup/jedi/JediWorkflowService;->customJediComponentItemsFactory:Lcom/squareup/jedi/CustomJediComponentItemsFactory;

    iput-object p6, p0, Lcom/squareup/jedi/JediWorkflowService;->resources:Landroid/content/res/Resources;

    iput-object p7, p0, Lcom/squareup/jedi/JediWorkflowService;->telephonyManager:Landroid/telephony/TelephonyManager;

    return-void
.end method

.method public static final synthetic access$getPanelForSession(Lcom/squareup/jedi/JediWorkflowService;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/jedi/JediWorkflowService;->getPanelForSession(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$resumeFromErrors(Lcom/squareup/jedi/JediWorkflowService;Ljava/lang/Throwable;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/jedi/JediWorkflowService;->resumeFromErrors(Ljava/lang/Throwable;)Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$screenDataFromPanelResponse(Lcom/squareup/jedi/JediWorkflowService;Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/jedi/JediWorkflowService;->screenDataFromPanelResponse(Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/jedi/JediHelpScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$screenDataFromSuccessfulSearchResponse(Lcom/squareup/jedi/JediWorkflowService;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/jedi/JediWorkflowService;->screenDataFromSuccessfulSearchResponse(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p0

    return-object p0
.end method

.method private final getCanCall()Z
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService;->telephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getPanelForSession(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/jedi/service/PanelResponse;",
            ">;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;-><init>()V

    .line 109
    invoke-virtual {v0, p2}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->transition_id(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object p2

    .line 110
    invoke-virtual {p2, p3}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->inputs(Ljava/util/List;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object p2

    .line 111
    invoke-virtual {p2, p1}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object p1

    .line 112
    invoke-virtual {p1, p4}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->panel_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->build()Lcom/squareup/protos/jedi/service/PanelRequest;

    move-result-object p1

    .line 114
    iget-object p2, p0, Lcom/squareup/jedi/JediWorkflowService;->jediService:Lcom/squareup/server/help/JediService;

    const-string p3, "panelRequest"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/help/JediService;->getPanel(Lcom/squareup/protos/jedi/service/PanelRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;

    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/squareup/server/help/JediService$JediServiceStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 116
    sget-object p2, Lcom/squareup/jedi/JediWorkflowService$getPanelForSession$1;->INSTANCE:Lcom/squareup/jedi/JediWorkflowService$getPanelForSession$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "jediService.getPanel(pan\u2026.\")\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getSearchPanel(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/jedi/service/SearchResponse;",
            ">;"
        }
    .end annotation

    .line 131
    new-instance v0, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;-><init>()V

    .line 132
    invoke-virtual {p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSessionToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/SearchRequest$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1, p2}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;->search_text(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/SearchRequest$Builder;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;->build()Lcom/squareup/protos/jedi/service/SearchRequest;

    move-result-object p1

    .line 135
    iget-object p2, p0, Lcom/squareup/jedi/JediWorkflowService;->jediService:Lcom/squareup/server/help/JediService;

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p2, p1}, Lcom/squareup/server/help/JediService;->searchText(Lcom/squareup/protos/jedi/service/SearchRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;

    move-result-object p1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/server/help/JediService$JediServiceStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 137
    sget-object p2, Lcom/squareup/jedi/JediWorkflowService$getSearchPanel$1;->INSTANCE:Lcom/squareup/jedi/JediWorkflowService$getSearchPanel$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "jediService.searchText(r\u2026.\")\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final resumeFromErrors(Ljava/lang/Throwable;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 3

    .line 187
    new-instance v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    .line 189
    const-class v1, Lcom/squareup/jedi/JediServiceException;

    invoke-static {v1, p1}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Can\'t handle support content"

    .line 190
    invoke-static {p1, v2, v1}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    iget-object p1, p0, Lcom/squareup/jedi/JediWorkflowService;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/jedi/impl/R$string;->jedi_content_unavailable_text:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 194
    :cond_0
    const-class v1, Lretrofit/RetrofitError;

    invoke-static {v1, p1}, Lcom/squareup/util/Throwables;->matches(Ljava/lang/Class;Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 195
    const-class v1, Lretrofit/RetrofitError;

    invoke-static {v1, p1}, Lcom/squareup/util/Throwables;->getMatchingThrowable(Ljava/lang/Class;Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    check-cast p1, Lretrofit/RetrofitError;

    .line 196
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object p1

    sget-object v1, Lretrofit/RetrofitError$Kind;->NETWORK:Lretrofit/RetrofitError$Kind;

    if-ne p1, v1, :cond_1

    .line 197
    iget-object p1, p0, Lcom/squareup/jedi/JediWorkflowService;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/jedi/impl/R$string;->jedi_network_unavailable_text:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 199
    :cond_1
    iget-object p1, p0, Lcom/squareup/jedi/JediWorkflowService;->resources:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/jedi/impl/R$string;->jedi_content_unavailable_text:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    const-string/jumbo v1, "when {\n          matches\u2026gate(throwable)\n        }"

    .line 188
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-direct {v0, p1}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/jedi/JediHelpScreenData;

    return-object v0

    .line 203
    :cond_2
    invoke-static {p1}, Lcom/squareup/util/Throwables;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final screenDataFromPanelResponse(Lcom/squareup/protos/jedi/service/PanelResponse;)Lcom/squareup/jedi/JediHelpScreenData;
    .locals 6

    .line 151
    sget-object v0, Lcom/squareup/jedi/JediHelpScreenData;->Companion:Lcom/squareup/jedi/JediHelpScreenData$Companion;

    .line 152
    iget-object v1, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    const-string v2, "panelResponse.panel"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v2, p1, Lcom/squareup/protos/jedi/service/PanelResponse;->transition_id:Ljava/lang/String;

    .line 154
    iget-object v3, p0, Lcom/squareup/jedi/JediWorkflowService;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    .line 155
    iget-object v4, p0, Lcom/squareup/jedi/JediWorkflowService;->resources:Landroid/content/res/Resources;

    .line 156
    invoke-direct {p0}, Lcom/squareup/jedi/JediWorkflowService;->getCanCall()Z

    move-result v5

    .line 151
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/jedi/JediHelpScreenData$Companion;->screenDataFromPanel(Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Z)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p1

    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData;

    return-object p1
.end method

.method private final screenDataFromSuccessfulSearchResponse(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Lcom/squareup/protos/jedi/service/SearchResponse;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;
    .locals 12

    .line 169
    sget-object v0, Lcom/squareup/jedi/JediHelpScreenData;->Companion:Lcom/squareup/jedi/JediHelpScreenData$Companion;

    .line 170
    iget-object v1, p3, Lcom/squareup/protos/jedi/service/SearchResponse;->panel:Lcom/squareup/protos/jedi/service/Panel;

    const-string p3, "response.panel"

    invoke-static {v1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iget-object v3, p0, Lcom/squareup/jedi/JediWorkflowService;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    .line 173
    iget-object v4, p0, Lcom/squareup/jedi/JediWorkflowService;->resources:Landroid/content/res/Resources;

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 169
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/jedi/JediHelpScreenData$Companion;->screenDataFromPanel(Lcom/squareup/protos/jedi/service/Panel;Ljava/lang/String;Lcom/squareup/jedi/JediHelpSettings;Landroid/content/res/Resources;Z)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p3

    .line 178
    iget-object v0, p3, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 179
    iget-object p3, p3, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->components:Ljava/util/List;

    goto :goto_0

    .line 181
    :cond_0
    iget-object p3, p0, Lcom/squareup/jedi/JediWorkflowService;->customJediComponentItemsFactory:Lcom/squareup/jedi/CustomJediComponentItemsFactory;

    invoke-virtual {p3}, Lcom/squareup/jedi/CustomJediComponentItemsFactory;->getNoResults()Ljava/util/List;

    move-result-object p3

    :goto_0
    move-object v9, p3

    const/16 v10, 0x7f

    const/4 v11, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-object v8, p2

    .line 176
    invoke-static/range {v0 .. v11}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->copy$default(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;ZLcom/squareup/jedi/ui/components/JediBannerComponentItem;ZLjava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object p1

    return-object p1
.end method

.method private final startNewSession()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowService;->jediHelpSettings:Lcom/squareup/jedi/JediHelpSettings;

    invoke-interface {v0}, Lcom/squareup/jedi/JediHelpSettings;->getJediClientKey()Ljava/lang/String;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowService;->userSettingsProvider:Lcom/squareup/settings/server/UserSettingsProvider;

    invoke-interface {v1}, Lcom/squareup/settings/server/UserSettingsProvider;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 84
    :cond_0
    new-instance v2, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;-><init>()V

    .line 85
    invoke-virtual {v2, v0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->client(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/protos/common/countries/Country;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/countries/Country;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowService;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "locale.language"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "(this as java.lang.String).toUpperCase()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/protos/common/languages/Language;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/languages/Language;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;

    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->build()Lcom/squareup/protos/jedi/service/StartSessionRequest;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/squareup/jedi/JediWorkflowService;->jediService:Lcom/squareup/server/help/JediService;

    const-string v2, "startSessionRequest"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/help/JediService;->startSession(Lcom/squareup/protos/jedi/service/StartSessionRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/squareup/server/help/JediService$JediServiceStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 91
    sget-object v1, Lcom/squareup/jedi/JediWorkflowService$startNewSession$1;->INSTANCE:Lcom/squareup/jedi/JediWorkflowService$startNewSession$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "jediService.startSession\u2026.\")\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 87
    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final getNextPanel(Lcom/squareup/jedi/JediInput;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jedi/JediInput;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "jediInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Lcom/squareup/jedi/JediInput;->getSessionToken()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-virtual {p1}, Lcom/squareup/jedi/JediInput;->getTransitionId()Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/jedi/JediInput;->getInputs()Ljava/util/List;

    move-result-object v2

    .line 60
    invoke-virtual {p1}, Lcom/squareup/jedi/JediInput;->getPanelToken()Ljava/lang/String;

    move-result-object p1

    .line 56
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/squareup/jedi/JediWorkflowService;->getPanelForSession(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 62
    new-instance v0, Lcom/squareup/jedi/JediWorkflowService$getNextPanel$1;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/JediWorkflowService$getNextPanel$1;-><init>(Lcom/squareup/jedi/JediWorkflowService;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 63
    new-instance v0, Lcom/squareup/jedi/JediWorkflowService$getNextPanel$2;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/JediWorkflowService$getNextPanel$2;-><init>(Lcom/squareup/jedi/JediWorkflowService;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "getPanelForSession(\n    \u2026meFromErrors(throwable) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final searching(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "searchText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/squareup/jedi/JediWorkflowService;->getSearchPanel(Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/jedi/JediWorkflowService$searching$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/jedi/JediWorkflowService$searching$1;-><init>(Lcom/squareup/jedi/JediWorkflowService;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "getSearchPanel(screenDat\u2026 = response\n      )\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final startSessionAndGetPanel(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/squareup/jedi/JediWorkflowService;->startNewSession()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$1;-><init>(Lcom/squareup/jedi/JediWorkflowService;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 49
    new-instance v0, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$2;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/JediWorkflowService$startSessionAndGetPanel$2;-><init>(Lcom/squareup/jedi/JediWorkflowService;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "startNewSession().flatMa\u2026onse(panelResponse)\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
