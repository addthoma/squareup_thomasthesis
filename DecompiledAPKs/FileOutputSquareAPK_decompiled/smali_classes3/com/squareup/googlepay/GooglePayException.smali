.class public abstract Lcom/squareup/googlepay/GooglePayException;
.super Ljava/lang/Exception;
.source "GooglePayException.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/googlepay/GooglePayException$GooglePayUnavailableException;,
        Lcom/squareup/googlepay/GooglePayException$WalletIdNotFoundException;,
        Lcom/squareup/googlepay/GooglePayException$HardwareIdNotFoundException;,
        Lcom/squareup/googlepay/GooglePayException$WalletIdCouldNotBeCreatedException;,
        Lcom/squareup/googlepay/GooglePayException$CouldNotConnectToPayException;,
        Lcom/squareup/googlepay/GooglePayException$UnknownException;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00060\u0001j\u0002`\u0002:\u0006\u0006\u0007\u0008\t\n\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005\u0082\u0001\u0006\u000c\r\u000e\u000f\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/googlepay/GooglePayException;",
        "Ljava/lang/Exception;",
        "Lkotlin/Exception;",
        "message",
        "",
        "(Ljava/lang/String;)V",
        "CouldNotConnectToPayException",
        "GooglePayUnavailableException",
        "HardwareIdNotFoundException",
        "UnknownException",
        "WalletIdCouldNotBeCreatedException",
        "WalletIdNotFoundException",
        "Lcom/squareup/googlepay/GooglePayException$GooglePayUnavailableException;",
        "Lcom/squareup/googlepay/GooglePayException$WalletIdNotFoundException;",
        "Lcom/squareup/googlepay/GooglePayException$HardwareIdNotFoundException;",
        "Lcom/squareup/googlepay/GooglePayException$WalletIdCouldNotBeCreatedException;",
        "Lcom/squareup/googlepay/GooglePayException$CouldNotConnectToPayException;",
        "Lcom/squareup/googlepay/GooglePayException$UnknownException;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/squareup/googlepay/GooglePayException;-><init>(Ljava/lang/String;)V

    return-void
.end method
