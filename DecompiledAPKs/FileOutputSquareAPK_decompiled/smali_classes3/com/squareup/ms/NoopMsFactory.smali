.class public final Lcom/squareup/ms/NoopMsFactory;
.super Ljava/lang/Object;
.source "NoopMsFactory.kt"

# interfaces
.implements Lcom/squareup/ms/MsFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ms/NoopMsFactory;",
        "Lcom/squareup/ms/MsFactory;",
        "()V",
        "getMinesweeper",
        "Lcom/squareup/ms/Minesweeper;",
        "initialize",
        "",
        "isInitialized",
        "",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ms/NoopMsFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 3
    new-instance v0, Lcom/squareup/ms/NoopMsFactory;

    invoke-direct {v0}, Lcom/squareup/ms/NoopMsFactory;-><init>()V

    sput-object v0, Lcom/squareup/ms/NoopMsFactory;->INSTANCE:Lcom/squareup/ms/NoopMsFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMinesweeper()Lcom/squareup/ms/Minesweeper;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/ms/NoopMinesweeper;->INSTANCE:Lcom/squareup/ms/NoopMinesweeper;

    check-cast v0, Lcom/squareup/ms/Minesweeper;

    return-object v0
.end method

.method public initialize()V
    .locals 0

    return-void
.end method

.method public isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
