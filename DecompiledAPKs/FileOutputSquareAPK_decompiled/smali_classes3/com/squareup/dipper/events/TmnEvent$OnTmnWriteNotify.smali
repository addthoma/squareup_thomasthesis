.class public final Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;
.super Lcom/squareup/dipper/events/TmnEvent;
.source "TmnEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/dipper/events/TmnEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnTmnWriteNotify"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "balanceBefore",
        "",
        "amount",
        "data",
        "",
        "(II[B)V",
        "getAmount",
        "()I",
        "getBalanceBefore",
        "getData",
        "()[B",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:I

.field private final balanceBefore:I

.field private final data:[B


# direct methods
.method public constructor <init>(II[B)V
    .locals 1

    const-string v0, "data"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, v0}, Lcom/squareup/dipper/events/TmnEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    iput p2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    iput-object p3, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;II[BILjava/lang/Object;)Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->copy(II[B)Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    return v0
.end method

.method public final component3()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    return-object v0
.end method

.method public final copy(II[B)Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;
    .locals 1

    const-string v0, "data"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;-><init>(II[B)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;

    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    iget v1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    iget v1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    iget-object p1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    return v0
.end method

.method public final getBalanceBefore()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    return v0
.end method

.method public final getData()[B
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    if-eqz v1, :cond_0

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnTmnWriteNotify(balanceBefore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->balanceBefore:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->amount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnWriteNotify;->data:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
