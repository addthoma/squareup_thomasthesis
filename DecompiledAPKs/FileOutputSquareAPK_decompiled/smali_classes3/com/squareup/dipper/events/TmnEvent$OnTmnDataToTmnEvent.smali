.class public final Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;
.super Lcom/squareup/dipper/events/TmnEvent;
.source "TmnEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/dipper/events/TmnEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnTmnDataToTmnEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "transactionId",
        "",
        "tmnData",
        "",
        "(Ljava/lang/String;[B)V",
        "getTmnData",
        "()[B",
        "getTransactionId",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tmnData:[B

.field private final transactionId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[B)V
    .locals 1

    const-string/jumbo v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tmnData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/dipper/events/TmnEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;Ljava/lang/String;[BILjava/lang/Object;)Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->copy(Ljava/lang/String;[B)Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()[B
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    return-object v0
.end method

.method public final copy(Ljava/lang/String;[B)Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;
    .locals 1

    const-string/jumbo v0, "transactionId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tmnData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    invoke-direct {v0, p1, p2}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;-><init>(Ljava/lang/String;[B)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    iget-object p1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getTmnData()[B
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    return-object v0
.end method

.method public final getTransactionId()Ljava/lang/String;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnTmnDataToTmnEvent(transactionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->transactionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tmnData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDataToTmnEvent;->tmnData:[B

    invoke-static {v1}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
