.class public abstract Lcom/squareup/dipper/events/CardReaderConnectionEvent;
.super Ljava/lang/Object;
.source "CardReaderConnectionEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderConnected;,
        Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderDisconnected;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/dipper/events/CardReaderConnectionEvent;",
        "",
        "cardReader",
        "Lcom/squareup/cardreader/CardReader;",
        "(Lcom/squareup/cardreader/CardReader;)V",
        "getCardReader",
        "()Lcom/squareup/cardreader/CardReader;",
        "CardReaderConnected",
        "CardReaderDisconnected",
        "Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderConnected;",
        "Lcom/squareup/dipper/events/CardReaderConnectionEvent$CardReaderDisconnected;",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cardReader:Lcom/squareup/cardreader/CardReader;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/dipper/events/CardReaderConnectionEvent;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/cardreader/CardReader;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 5
    invoke-direct {p0, p1}, Lcom/squareup/dipper/events/CardReaderConnectionEvent;-><init>(Lcom/squareup/cardreader/CardReader;)V

    return-void
.end method


# virtual methods
.method public getCardReader()Lcom/squareup/cardreader/CardReader;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/squareup/dipper/events/CardReaderConnectionEvent;->cardReader:Lcom/squareup/cardreader/CardReader;

    return-object v0
.end method
