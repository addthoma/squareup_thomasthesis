.class public final Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;
.super Lcom/squareup/dipper/events/TmnEvent;
.source "TmnEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/dipper/events/TmnEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnTmnDisplayRequestEvent"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0007H\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;",
        "Lcom/squareup/dipper/events/TmnEvent;",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "displayRequest",
        "Lcom/squareup/cardreader/lcr/CrsTmnMessage;",
        "amount",
        "",
        "balance",
        "(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V",
        "getAmount",
        "()Ljava/lang/String;",
        "getBalance",
        "getCardReaderInfo",
        "()Lcom/squareup/cardreader/CardReaderInfo;",
        "getDisplayRequest",
        "()Lcom/squareup/cardreader/lcr/CrsTmnMessage;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Ljava/lang/String;

.field private final balance:Ljava/lang/String;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayRequest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balance"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0, v0}, Lcom/squareup/dipper/events/TmnEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iput-object p2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    iput-object p3, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->copy(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object v0
.end method

.method public final component2()Lcom/squareup/cardreader/lcr/CrsTmnMessage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;
    .locals 1

    const-string v0, "cardReaderInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayRequest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "balance"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;-><init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    iget-object v1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    iget-object v1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Ljava/lang/String;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final getBalance()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    return-object v0
.end method

.method public final getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object v0
.end method

.method public final getDisplayRequest()Lcom/squareup/cardreader/lcr/CrsTmnMessage;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnTmnDisplayRequestEvent(cardReaderInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", displayRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->displayRequest:Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->amount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/dipper/events/TmnEvent$OnTmnDisplayRequestEvent;->balance:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
