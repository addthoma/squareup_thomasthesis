.class public final Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;
.super Ljava/lang/Object;
.source "InstantDepositAnalytics.kt"

# interfaces
.implements Lcom/squareup/instantdeposit/InstantDepositAnalytics;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/instantdeposit/InstantDepositAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoInstantDepositAnalytics"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstantDepositAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstantDepositAnalytics.kt\ncom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,107:1\n151#2,2:108\n*E\n*S KotlinDebug\n*F\n+ 1 InstantDepositAnalytics.kt\ncom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics\n*L\n105#1,2:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0019\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001J!\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0096\u0001J\u0011\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001J\t\u0010\u000c\u001a\u00020\u0004H\u0096\u0001J\t\u0010\r\u001a\u00020\u0004H\u0096\u0001J!\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0096\u0001J\u0011\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0012\u001a\u00020\u0013H\u0096\u0001J\u0011\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0006H\u0096\u0001J\u0011\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0006H\u0096\u0001J\u0011\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\tH\u0096\u0001J\t\u0010\u001a\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u001b\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u001c\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u001d\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u001e\u001a\u00020\u0004H\u0096\u0001J\t\u0010\u001f\u001a\u00020\u0004H\u0096\u0001J\t\u0010 \u001a\u00020\u0004H\u0096\u0001J\t\u0010!\u001a\u00020\u0004H\u0096\u0001J\t\u0010\"\u001a\u00020\u0004H\u0096\u0001J\t\u0010#\u001a\u00020\u0004H\u0096\u0001J\t\u0010$\u001a\u00020\u0004H\u0096\u0001J\t\u0010%\u001a\u00020\u0004H\u0096\u0001J\t\u0010&\u001a\u00020\u0004H\u0096\u0001J\t\u0010\'\u001a\u00020\u0004H\u0096\u0001J\t\u0010(\u001a\u00020\u0004H\u0096\u0001J\u0019\u0010)\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0096\u0001J\t\u0010*\u001a\u00020\u0004H\u0096\u0001J!\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020\u0006H\u0096\u0001J!\u00101\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH\u0096\u0001J\t\u00102\u001a\u00020\u0004H\u0096\u0001J\t\u00103\u001a\u00020\u0004H\u0096\u0001J\t\u00104\u001a\u00020\u0004H\u0096\u0001J\u0019\u00105\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u0006H\u0096\u0001\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "()V",
        "cancelCardLinking",
        "",
        "hasLinkedCard",
        "",
        "cardLinkFailure",
        "title",
        "",
        "message",
        "cardLinkSuccess",
        "headerDisplayedShowingAvailableBalance",
        "headerDisplayedShowingError",
        "instantDepositFailed",
        "registerErrorName",
        "Lcom/squareup/analytics/RegisterErrorName;",
        "instantDepositSucceeded",
        "registerViewName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "instantDepositToggled",
        "checked",
        "logBalanceAppletAddDebitCardAttempt",
        "isSuccessful",
        "logBalanceAppletAddDebitCardFailure",
        "failureMessage",
        "logBalanceAppletAddDebitCardSuccess",
        "logBalanceAppletDisplayedAvailableBalance",
        "logBalanceAppletInstantTransferClick",
        "logBalanceAppletInstantTransferSuccess",
        "logBalanceAppletLinkDebitCardCancel",
        "logBalanceAppletSetUpInstantTransfer",
        "logBalanceAppletTransferEstimatedFeesClick",
        "logBalanceAppletTransferEstimatedFeesLearnMoreClick",
        "logBalanceAppletTransferReportsDisplayedAvailableBalance",
        "logBalanceAppletTransferReportsInstantTransferClick",
        "logBalanceAppletTransferReportsInstantTransferSuccess",
        "logTransferReportsActiveSalesClick",
        "logTransferReportsPendingDepositClick",
        "logTransferReportsSentDepositClick",
        "logTransferReportsSummaryView",
        "resendEmailFailure",
        "resendEmailSuccess",
        "tapDepositAvailableFunds",
        "registerTapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "isTransferReports",
        "tapLearnMore",
        "tapLinkDifferentDebitCard",
        "transferReportsDisplayedAvailableBalance",
        "transferReportsDisplayedError",
        "tryToLinkCard",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 108
    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 109
    const-class v2, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    const/4 v3, 0x0

    const/4 v4, 0x4

    invoke-static {v2, v1, v3, v4, v0}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    iput-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    return-void
.end method


# virtual methods
.method public cancelCardLinking(Z)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->cancelCardLinking(Z)V

    return-void
.end method

.method public cardLinkFailure(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->cardLinkFailure(ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public cardLinkSuccess(Z)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->cardLinkSuccess(Z)V

    return-void
.end method

.method public headerDisplayedShowingAvailableBalance()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->headerDisplayedShowingAvailableBalance()V

    return-void
.end method

.method public headerDisplayedShowingError()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->headerDisplayedShowingError()V

    return-void
.end method

.method public instantDepositFailed(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "registerErrorName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->instantDepositFailed(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public instantDepositSucceeded(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 1

    const-string v0, "registerViewName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->instantDepositSucceeded(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public instantDepositToggled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->instantDepositToggled(Z)V

    return-void
.end method

.method public logBalanceAppletAddDebitCardAttempt(Z)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardAttempt(Z)V

    return-void
.end method

.method public logBalanceAppletAddDebitCardFailure(Ljava/lang/String;)V
    .locals 1

    const-string v0, "failureMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardFailure(Ljava/lang/String;)V

    return-void
.end method

.method public logBalanceAppletAddDebitCardSuccess()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletAddDebitCardSuccess()V

    return-void
.end method

.method public logBalanceAppletDisplayedAvailableBalance()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletDisplayedAvailableBalance()V

    return-void
.end method

.method public logBalanceAppletInstantTransferClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletInstantTransferClick()V

    return-void
.end method

.method public logBalanceAppletInstantTransferSuccess()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletInstantTransferSuccess()V

    return-void
.end method

.method public logBalanceAppletLinkDebitCardCancel()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletLinkDebitCardCancel()V

    return-void
.end method

.method public logBalanceAppletSetUpInstantTransfer()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletSetUpInstantTransfer()V

    return-void
.end method

.method public logBalanceAppletTransferEstimatedFeesClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletTransferEstimatedFeesClick()V

    return-void
.end method

.method public logBalanceAppletTransferEstimatedFeesLearnMoreClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletTransferEstimatedFeesLearnMoreClick()V

    return-void
.end method

.method public logBalanceAppletTransferReportsDisplayedAvailableBalance()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletTransferReportsDisplayedAvailableBalance()V

    return-void
.end method

.method public logBalanceAppletTransferReportsInstantTransferClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletTransferReportsInstantTransferClick()V

    return-void
.end method

.method public logBalanceAppletTransferReportsInstantTransferSuccess()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logBalanceAppletTransferReportsInstantTransferSuccess()V

    return-void
.end method

.method public logTransferReportsActiveSalesClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logTransferReportsActiveSalesClick()V

    return-void
.end method

.method public logTransferReportsPendingDepositClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logTransferReportsPendingDepositClick()V

    return-void
.end method

.method public logTransferReportsSentDepositClick()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logTransferReportsSentDepositClick()V

    return-void
.end method

.method public logTransferReportsSummaryView()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->logTransferReportsSummaryView()V

    return-void
.end method

.method public resendEmailFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->resendEmailFailure(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public resendEmailSuccess()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->resendEmailSuccess()V

    return-void
.end method

.method public tapDepositAvailableFunds(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;Z)V
    .locals 1

    const-string v0, "registerTapName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tapDepositAvailableFunds(Lcom/squareup/analytics/RegisterTapName;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method public tapLearnMore(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "registerErrorName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tapLearnMore(Lcom/squareup/analytics/RegisterErrorName;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public tapLinkDifferentDebitCard()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tapLinkDifferentDebitCard()V

    return-void
.end method

.method public transferReportsDisplayedAvailableBalance()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->transferReportsDisplayedAvailableBalance()V

    return-void
.end method

.method public transferReportsDisplayedError()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->transferReportsDisplayedError()V

    return-void
.end method

.method public tryToLinkCard(ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/instantdeposit/InstantDepositAnalytics$NoInstantDepositAnalytics;->$$delegate_0:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0, p1, p2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tryToLinkCard(ZZ)V

    return-void
.end method
