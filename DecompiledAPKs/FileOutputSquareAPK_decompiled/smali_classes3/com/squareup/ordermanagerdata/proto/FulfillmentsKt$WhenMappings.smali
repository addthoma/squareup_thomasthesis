.class public final synthetic Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/squareup/orders/model/Order$Fulfillment$State;->values()[Lcom/squareup/orders/model/Order$Fulfillment$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->PROPOSED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->RESERVED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->PREPARED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->FAILED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v7, 0x7

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$FulfillmentType;->values()[Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DIGITAL:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v6, v0, v1

    return-void
.end method
