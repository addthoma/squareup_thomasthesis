.class final Lcom/squareup/glyph/SquareGlyphView$SavedState$1;
.super Ljava/lang/Object;
.source "SquareGlyphView.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/SquareGlyphView$SavedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/glyph/SquareGlyphView$SavedState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/glyph/SquareGlyphView$SavedState;
    .locals 2

    .line 247
    new-instance v0, Lcom/squareup/glyph/SquareGlyphView$SavedState;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/glyph/SquareGlyphView$SavedState;-><init>(Landroid/os/Parcel;Lcom/squareup/glyph/SquareGlyphView$1;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 245
    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView$SavedState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/glyph/SquareGlyphView$SavedState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/glyph/SquareGlyphView$SavedState;
    .locals 0

    .line 251
    new-array p1, p1, [Lcom/squareup/glyph/SquareGlyphView$SavedState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 245
    invoke-virtual {p0, p1}, Lcom/squareup/glyph/SquareGlyphView$SavedState$1;->newArray(I)[Lcom/squareup/glyph/SquareGlyphView$SavedState;

    move-result-object p1

    return-object p1
.end method
