.class public final Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;
.super Ljava/lang/Object;
.source "ConnectedPrintersLoggingProvider.kt"

# interfaces
.implements Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConnectedPrintersLoggingProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConnectedPrintersLoggingProvider.kt\ncom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,33:1\n1360#2:34\n1429#2,3:35\n*E\n*S KotlinDebug\n*F\n+ 1 ConnectedPrintersLoggingProvider.kt\ncom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider\n*L\n15#1:34\n15#1,3:35\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016J\u0012\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0002R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralsLoggingProvider;",
        "hardwarePrinterTracker",
        "Lcom/squareup/print/HardwarePrinterTracker;",
        "(Lcom/squareup/print/HardwarePrinterTracker;)V",
        "getHardwarePrinterTracker",
        "()Lcom/squareup/print/HardwarePrinterTracker;",
        "getConnectedPeripherals",
        "",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
        "getConnectionType",
        "Lcom/squareup/connectedperipherals/ConnectedPeripheralType;",
        "connectionType",
        "Lcom/squareup/print/ConnectionType;",
        "connected-peripherals_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;


# direct methods
.method public constructor <init>(Lcom/squareup/print/HardwarePrinterTracker;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "hardwarePrinterTracker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    return-void
.end method

.method private final getConnectionType(Lcom/squareup/print/ConnectionType;)Lcom/squareup/connectedperipherals/ConnectedPeripheralType;
    .locals 1

    if-nez p1, :cond_0

    goto :goto_0

    .line 25
    :cond_0
    sget-object v0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/print/ConnectionType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    .line 29
    :goto_0
    sget-object p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->UNKNOWN:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    goto :goto_1

    .line 28
    :cond_1
    sget-object p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->NETWORK:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    goto :goto_1

    .line 27
    :cond_2
    sget-object p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->USB:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    goto :goto_1

    .line 26
    :cond_3
    sget-object p1, Lcom/squareup/connectedperipherals/ConnectedPeripheralType;->BLUETOOTH:Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    :goto_1
    return-object p1
.end method


# virtual methods
.method public getConnectedPeripherals()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/connectedperipherals/ConnectedPeripheralData;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinterTracker;->getAllAvailableHardwarePrinters()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "hardwarePrinterTracker.a\u2026AvailableHardwarePrinters"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 35
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 36
    check-cast v2, Lcom/squareup/print/HardwarePrinter;

    const-string v3, "it"

    .line 16
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v2

    .line 17
    new-instance v9, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;

    .line 18
    invoke-virtual {v2}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object v4

    sget-object v3, Lcom/squareup/connectedperipherals/DeviceType;->Printer:Lcom/squareup/connectedperipherals/DeviceType;

    invoke-virtual {v3}, Lcom/squareup/connectedperipherals/DeviceType;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v2, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->model:Ljava/lang/String;

    iget-object v3, v2, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    invoke-direct {p0, v3}, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;->getConnectionType(Lcom/squareup/print/ConnectionType;)Lcom/squareup/connectedperipherals/ConnectedPeripheralType;

    move-result-object v7

    iget-object v8, v2, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->manufacturer:Ljava/lang/String;

    move-object v3, v9

    .line 17
    invoke-direct/range {v3 .. v8}, Lcom/squareup/connectedperipherals/ConnectedPeripheralData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/connectedperipherals/ConnectedPeripheralType;Ljava/lang/String;)V

    .line 20
    invoke-interface {v1, v9}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 37
    :cond_0
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getHardwarePrinterTracker()Lcom/squareup/print/HardwarePrinterTracker;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/connectedperipherals/ConnectedPrintersLoggingProvider;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    return-object v0
.end method
