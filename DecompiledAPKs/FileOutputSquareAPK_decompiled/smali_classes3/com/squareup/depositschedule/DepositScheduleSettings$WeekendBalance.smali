.class public final enum Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;
.super Ljava/lang/Enum;
.source "DepositScheduleSettings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/depositschedule/DepositScheduleSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WeekendBalance"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;",
        "",
        "(Ljava/lang/String;I)V",
        "UNCHANGED_WEEKEND_BALANCE",
        "ENABLE_WEEKEND_BALANCE",
        "DISABLE_WEEKEND_BALANCE",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

.field public static final enum DISABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

.field public static final enum ENABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

.field public static final enum UNCHANGED_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    new-instance v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    const/4 v2, 0x0

    const-string v3, "UNCHANGED_WEEKEND_BALANCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->UNCHANGED_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    const/4 v2, 0x1

    const-string v3, "ENABLE_WEEKEND_BALANCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->ENABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    const/4 v2, 0x2

    const-string v3, "DISABLE_WEEKEND_BALANCE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->DISABLE_WEEKEND_BALANCE:Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->$VALUES:[Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;
    .locals 1

    const-class v0, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    return-object p0
.end method

.method public static values()[Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;
    .locals 1

    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->$VALUES:[Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    invoke-virtual {v0}, [Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/depositschedule/DepositScheduleSettings$WeekendBalance;

    return-object v0
.end method
