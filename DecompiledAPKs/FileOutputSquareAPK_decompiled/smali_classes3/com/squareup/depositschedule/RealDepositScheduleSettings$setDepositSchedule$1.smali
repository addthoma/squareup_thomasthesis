.class final Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;
.super Ljava/lang/Object;
.source "RealDepositScheduleSettings.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/depositschedule/RealDepositScheduleSettings;->setDepositSchedule(Lcom/squareup/protos/client/depositsettings/SetDepositScheduleRequest;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

.field final synthetic this$0:Lcom/squareup/depositschedule/RealDepositScheduleSettings;


# direct methods
.method constructor <init>(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;->this$0:Lcom/squareup/depositschedule/RealDepositScheduleSettings;

    iput-object p2, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;->$depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/depositsettings/SetDepositScheduleResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;->this$0:Lcom/squareup/depositschedule/RealDepositScheduleSettings;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;->$depositSpeed:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;

    invoke-static {v0, p1, v1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->access$onSuccess(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Ljava/lang/Object;Lcom/squareup/depositschedule/DepositScheduleSettings$DepositSpeed;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 226
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;->this$0:Lcom/squareup/depositschedule/RealDepositScheduleSettings;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings;->access$onFailure(Lcom/squareup/depositschedule/RealDepositScheduleSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/depositschedule/RealDepositScheduleSettings$setDepositSchedule$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
