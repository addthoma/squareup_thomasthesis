.class public final Lcom/squareup/compvoidcontroller/RealVoidController;
.super Ljava/lang/Object;
.source "RealVoidController.kt"

# interfaces
.implements Lcom/squareup/compvoidcontroller/VoidController;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealVoidController.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealVoidController.kt\ncom/squareup/compvoidcontroller/RealVoidController\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,133:1\n713#2,10:134\n1651#2,2:144\n723#2,2:146\n1653#2:148\n725#2:149\n1380#2,11:150\n1651#2,3:161\n1391#2:164\n*E\n*S KotlinDebug\n*F\n+ 1 RealVoidController.kt\ncom/squareup/compvoidcontroller/RealVoidController\n*L\n117#1,10:134\n117#1,2:144\n117#1,2:146\n117#1:148\n117#1:149\n118#1,11:150\n118#1,3:161\n118#1:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001Bc\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0008\u0008\u0001\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J,\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u001a\u0010#\u001a\u00020$2\u0006\u0010\u001d\u001a\u00020\u001e2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0016J$\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&*\u00020(2\u0006\u0010\u001d\u001a\u00020\u001e2\u0008\u0010!\u001a\u0004\u0018\u00010\"H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/compvoidcontroller/RealVoidController;",
        "Lcom/squareup/compvoidcontroller/VoidController;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "compVoidTicketPaymentFactory",
        "Lcom/squareup/payment/CompVoidTicketPaymentFactory;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "res",
        "Lcom/squareup/util/Res;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/CompVoidTicketPaymentFactory;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/Transaction;)V",
        "voidTicket",
        "Lcom/squareup/protos/client/IdPair;",
        "openTicket",
        "Lcom/squareup/tickets/OpenTicket;",
        "voidReason",
        "",
        "baseOrder",
        "Lcom/squareup/payment/OrderSnapshot;",
        "employee",
        "Lcom/squareup/protos/client/Employee;",
        "voidTransactionTicket",
        "",
        "voidAllItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "Lcom/squareup/payment/Order;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final compVoidTicketPaymentFactory:Lcom/squareup/payment/CompVoidTicketPaymentFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/payment/CompVoidTicketPaymentFactory;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/payment/Transaction;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p10    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "compVoidTicketPaymentFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainThread"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "threadEnforcer"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->compVoidTicketPaymentFactory:Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    iput-object p3, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->features:Lcom/squareup/settings/server/Features;

    iput-object p4, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p5, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iput-object p6, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iput-object p7, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->res:Lcom/squareup/util/Res;

    iput-object p8, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p9, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    iput-object p10, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p11, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method public static final synthetic access$getOrderPrintingDispatcher$p(Lcom/squareup/compvoidcontroller/RealVoidController;)Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    return-object p0
.end method

.method private final voidAllItems(Lcom/squareup/payment/Order;Ljava/lang/String;Lcom/squareup/protos/client/Employee;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/Order;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/Employee;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 115
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    const-string v3, "items"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    .line 134
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 145
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const-string v7, "cartItem"

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    add-int/lit8 v8, v5, 0x1

    if-gez v5, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    .line 146
    :cond_0
    move-object v9, v6

    check-cast v9, Lcom/squareup/checkout/CartItem;

    if-ne v5, v0, :cond_2

    .line 117
    invoke-static {v9, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v5

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_3

    invoke-interface {v3, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_3
    move v5, v8

    goto :goto_0

    .line 149
    :cond_4
    check-cast v3, Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 162
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v4, 0x1

    if-gez v4, :cond_5

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    .line 160
    :cond_5
    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 119
    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v5

    if-nez v5, :cond_7

    iget-boolean v5, v2, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v5, :cond_6

    goto :goto_4

    .line 122
    :cond_6
    invoke-virtual {p1, v4, p3, p2}, Lcom/squareup/payment/Order;->voidItem(ILcom/squareup/protos/client/Employee;Ljava/lang/String;)V

    goto :goto_5

    :cond_7
    :goto_4
    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_8

    .line 160
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_8
    move v4, v3

    goto :goto_3

    .line 164
    :cond_9
    check-cast v0, Ljava/util/List;

    .line 128
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->invalidate()V

    .line 129
    iget-object p1, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->VOID_CART_VOIDED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-object v0
.end method


# virtual methods
.method public voidTicket(Lcom/squareup/tickets/OpenTicket;Ljava/lang/String;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/IdPair;
    .locals 2

    const-string v0, "openTicket"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidReason"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "baseOrder"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->forbid()V

    .line 90
    check-cast p3, Lcom/squareup/payment/Order;

    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->res:Lcom/squareup/util/Res;

    const/4 v1, 0x1

    invoke-static {p3, p1, v0, v1}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromOrder(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object p3

    .line 91
    invoke-virtual {p3}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 94
    :cond_0
    invoke-direct {p0, p3, p2, p4}, Lcom/squareup/compvoidcontroller/RealVoidController;->voidAllItems(Lcom/squareup/payment/Order;Ljava/lang/String;Lcom/squareup/protos/client/Employee;)Ljava/util/List;

    move-result-object p2

    .line 97
    iget-object p4, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p4, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p4

    .line 95
    invoke-virtual {p1, p3, p4}, Lcom/squareup/tickets/OpenTicket;->updateAndSetWriteOnlyDeletes(Lcom/squareup/payment/Order;Z)V

    .line 100
    iget-object p4, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->compVoidTicketPaymentFactory:Lcom/squareup/payment/CompVoidTicketPaymentFactory;

    invoke-virtual {p4, v1, p3}, Lcom/squareup/payment/CompVoidTicketPaymentFactory;->create(ZLcom/squareup/payment/Order;)Lcom/squareup/payment/BillPayment;

    move-result-object p4

    const-string v0, "billPayment"

    .line 101
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-static {p4, v0, v1}, Lcom/squareup/compvoidcontroller/BillPaymentsKt;->addZeroAmountLocalTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-static {p4, v0}, Lcom/squareup/compvoidcontroller/BillPaymentsKt;->capture(Lcom/squareup/payment/BillPayment;Lio/reactivex/Scheduler;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/squareup/compvoidcontroller/RealVoidController$voidTicket$3;-><init>(Lcom/squareup/compvoidcontroller/RealVoidController;Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->post(Ljava/lang/Runnable;)V

    .line 106
    new-instance p1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 107
    invoke-virtual {p4}, Lcom/squareup/payment/BillPayment;->getUniqueClientId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p1

    return-object p1

    .line 102
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Void ticket payment must be capturable."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public voidTransactionTicket(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 4

    const-string/jumbo v0, "voidReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 48
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49
    iget-object v0, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getCurrentTicket()Lcom/squareup/tickets/OpenTicket;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 51
    iget-object v1, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->deleteCurrentTicketAndReset()V

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->popUninterestingItem()Lcom/squareup/checkout/CartItem;

    const-string v2, "order"

    .line 58
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, p1, p2}, Lcom/squareup/compvoidcontroller/RealVoidController;->voidAllItems(Lcom/squareup/payment/Order;Ljava/lang/String;Lcom/squareup/protos/client/Employee;)Ljava/util/List;

    move-result-object p1

    .line 66
    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p2

    const-string/jumbo v2, "transaction.startSingleTenderBillPayment()"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v3, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-static {p2, v2, v3}, Lcom/squareup/compvoidcontroller/BillPaymentsKt;->addZeroAmountLocalTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;)V

    .line 68
    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->captureLocalPaymentAndResetInSellerFlow()V

    .line 69
    iget-object p2, p0, Lcom/squareup/compvoidcontroller/RealVoidController;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p2, v1, v0, p1}, Lcom/squareup/print/OrderPrintingDispatcher;->printVoidTicket(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Ljava/util/List;)V

    return-void

    .line 49
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 48
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Must load ticket to void transaction ticket."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
