.class public interface abstract Lcom/squareup/development/drawer/DevelopmentDrawerSectionComparator;
.super Ljava/lang/Object;
.source "DevelopmentDrawerSectionComparator.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/development/drawer/DevelopmentDrawerSectionComparator;",
        "Ljava/util/Comparator;",
        "Lcom/squareup/development/drawer/DevelopmentDrawerSection;",
        "Lkotlin/Comparator;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
