.class public Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;
.super Ljava/lang/Object;
.source "LocalizerImpl.java"

# interfaces
.implements Lcom/squareup/shared/i18n/LocalizedStringBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crossplatform/i18n/LocalizerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BaseLocalizedStringBuilder"
.end annotation


# instance fields
.field final phrase:Lcom/squareup/phrase/Phrase;


# direct methods
.method public constructor <init>(Lcom/squareup/phrase/Phrase;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;->phrase:Lcom/squareup/phrase/Phrase;

    return-void
.end method


# virtual methods
.method public format()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;->phrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;->phrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    return-object p0
.end method

.method public bridge synthetic put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;->put(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;

    move-result-object p1

    return-object p1
.end method

.method public putInt(Ljava/lang/String;I)Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;->phrase:Lcom/squareup/phrase/Phrase;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    return-object p0
.end method

.method public bridge synthetic putInt(Ljava/lang/String;I)Lcom/squareup/shared/i18n/LocalizedStringBuilder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;->putInt(Ljava/lang/String;I)Lcom/squareup/crossplatform/i18n/LocalizerImpl$BaseLocalizedStringBuilder;

    move-result-object p1

    return-object p1
.end method
