.class public Lcom/squareup/locale/LocaleOverrideFactory;
.super Ljava/lang/Object;
.source "LocaleOverrideFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLocaleOverrideFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LocaleOverrideFactory.kt\ncom/squareup/locale/LocaleOverrideFactory\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0016\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\nX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0016\u001a\u00020\u00178VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u001b\u001a\u0004\u0008\u0018\u0010\u0019R\u001a\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0013R\u001b\u0010\u001f\u001a\u00020 8VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u001b\u001a\u0004\u0008!\u0010\"R\u0014\u0010$\u001a\u00020%8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008&\u0010\'R!\u0010(\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008*\u0010\u001b\u001a\u0004\u0008)\u0010\u0013\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/locale/LocaleOverrideFactory;",
        "",
        "context",
        "Landroid/content/Context;",
        "locale",
        "Ljava/util/Locale;",
        "(Landroid/content/Context;Ljava/util/Locale;)V",
        "getLocale",
        "()Ljava/util/Locale;",
        "localeFormatter",
        "Lcom/squareup/locale/LocaleFormatter;",
        "getLocaleFormatter",
        "()Lcom/squareup/locale/LocaleFormatter;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "getMoneyFormatter",
        "()Lcom/squareup/text/Formatter;",
        "moneyLocaleFormatter",
        "Lcom/squareup/money/MoneyLocaleFormatter;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "getPerUnitFormatter",
        "()Lcom/squareup/quantity/PerUnitFormatter;",
        "perUnitFormatter$delegate",
        "Lkotlin/Lazy;",
        "percentageFormatter",
        "Lcom/squareup/util/Percentage;",
        "getPercentageFormatter",
        "res",
        "Lcom/squareup/util/Res;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "res$delegate",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "shortMoneyFormatter",
        "getShortMoneyFormatter",
        "shortMoneyFormatter$delegate",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final locale:Ljava/util/Locale;

.field private final localeFormatter:Lcom/squareup/locale/LocaleFormatter;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

.field private final perUnitFormatter$delegate:Lkotlin/Lazy;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final res$delegate:Lkotlin/Lazy;

.field private final shortMoneyFormatter$delegate:Lkotlin/Lazy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Locale;)V
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/locale/LocaleOverrideFactory;->locale:Ljava/util/Locale;

    .line 28
    new-instance p2, Lcom/squareup/money/MoneyLocaleFormatter;

    invoke-direct {p2}, Lcom/squareup/money/MoneyLocaleFormatter;-><init>()V

    iput-object p2, p0, Lcom/squareup/locale/LocaleOverrideFactory;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    .line 29
    new-instance p2, Lcom/squareup/locale/LocaleOverrideFactory$localeProvider$1;

    invoke-direct {p2, p0}, Lcom/squareup/locale/LocaleOverrideFactory$localeProvider$1;-><init>(Lcom/squareup/locale/LocaleOverrideFactory;)V

    check-cast p2, Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/locale/LocaleOverrideFactory;->localeProvider:Ljavax/inject/Provider;

    .line 30
    new-instance p2, Landroid/content/res/Configuration;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "context.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p2, v0}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/res/Configuration;->setLocale(Ljava/util/Locale;)V

    .line 32
    invoke-virtual {p1, p2}, Landroid/content/Context;->createConfigurationContext(Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object p1

    const-string p2, "context.createConfigurationContext(it)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p2, "Configuration(context.re\u2026onfigurationContext(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->context:Landroid/content/Context;

    .line 35
    new-instance p1, Lcom/squareup/locale/LocaleOverrideFactory$res$2;

    invoke-direct {p1, p0}, Lcom/squareup/locale/LocaleOverrideFactory$res$2;-><init>(Lcom/squareup/locale/LocaleOverrideFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->res$delegate:Lkotlin/Lazy;

    .line 37
    new-instance p1, Lcom/squareup/money/MoneyFormatter;

    iget-object v1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->localeProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/locale/LocaleOverrideFactory;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/text/Formatter;

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 38
    new-instance p1, Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;

    invoke-direct {p1, p0}, Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;-><init>(Lcom/squareup/locale/LocaleOverrideFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->shortMoneyFormatter$delegate:Lkotlin/Lazy;

    .line 42
    new-instance p1, Lcom/squareup/text/PercentageFormatter;

    iget-object p2, p0, Lcom/squareup/locale/LocaleOverrideFactory;->localeProvider:Ljavax/inject/Provider;

    invoke-direct {p1, p2}, Lcom/squareup/text/PercentageFormatter;-><init>(Ljavax/inject/Provider;)V

    check-cast p1, Lcom/squareup/text/Formatter;

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 43
    new-instance p1, Lcom/squareup/locale/LocaleFormatter;

    invoke-virtual {p0}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/locale/LocaleFormatter;-><init>(Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->localeFormatter:Lcom/squareup/locale/LocaleFormatter;

    .line 44
    new-instance p1, Lcom/squareup/locale/LocaleOverrideFactory$perUnitFormatter$2;

    invoke-direct {p1, p0}, Lcom/squareup/locale/LocaleOverrideFactory$perUnitFormatter$2;-><init>(Lcom/squareup/locale/LocaleOverrideFactory;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory;->perUnitFormatter$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getLocaleProvider$p(Lcom/squareup/locale/LocaleOverrideFactory;)Ljavax/inject/Provider;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->localeProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method public static final synthetic access$getMoneyLocaleFormatter$p(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/money/MoneyLocaleFormatter;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->moneyLocaleFormatter:Lcom/squareup/money/MoneyLocaleFormatter;

    return-object p0
.end method


# virtual methods
.method public getLocale()Ljava/util/Locale;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getLocaleFormatter()Lcom/squareup/locale/LocaleFormatter;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->localeFormatter:Lcom/squareup/locale/LocaleFormatter;

    return-object v0
.end method

.method public getMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public getPerUnitFormatter()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->perUnitFormatter$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    return-object v0
.end method

.method public getPercentageFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->res$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 2

    .line 34
    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "context.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getShortMoneyFormatter()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory;->shortMoneyFormatter$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    return-object v0
.end method
