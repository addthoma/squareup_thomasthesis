.class final Lcom/squareup/locale/LocaleOverrideFactory$localeProvider$1;
.super Ljava/lang/Object;
.source "LocaleOverrideFactory.kt"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Ljava/util/Locale;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/Locale;",
        "get"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/locale/LocaleOverrideFactory;


# direct methods
.method constructor <init>(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory$localeProvider$1;->this$0:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/locale/LocaleOverrideFactory$localeProvider$1;->get()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method

.method public final get()Ljava/util/Locale;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/locale/LocaleOverrideFactory$localeProvider$1;->this$0:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {v0}, Lcom/squareup/locale/LocaleOverrideFactory;->getLocale()Ljava/util/Locale;

    move-result-object v0

    return-object v0
.end method
