.class final Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LocaleOverrideFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/locale/LocaleOverrideFactory;-><init>(Landroid/content/Context;Ljava/util/Locale;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/money/MoneyFormatter;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/money/MoneyFormatter;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/locale/LocaleOverrideFactory;


# direct methods
.method constructor <init>(Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;->this$0:Lcom/squareup/locale/LocaleOverrideFactory;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/money/MoneyFormatter;
    .locals 4

    .line 39
    new-instance v0, Lcom/squareup/money/MoneyFormatter;

    iget-object v1, p0, Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;->this$0:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-static {v1}, Lcom/squareup/locale/LocaleOverrideFactory;->access$getLocaleProvider$p(Lcom/squareup/locale/LocaleOverrideFactory;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;->this$0:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-static {v2}, Lcom/squareup/locale/LocaleOverrideFactory;->access$getMoneyLocaleFormatter$p(Lcom/squareup/locale/LocaleOverrideFactory;)Lcom/squareup/money/MoneyLocaleFormatter;

    move-result-object v2

    sget-object v3, Lcom/squareup/money/MoneyLocaleFormatter$Mode;->SHORTER:Lcom/squareup/money/MoneyLocaleFormatter$Mode;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/money/MoneyFormatter;-><init>(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;Lcom/squareup/money/MoneyLocaleFormatter$Mode;)V

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/locale/LocaleOverrideFactory$shortMoneyFormatter$2;->invoke()Lcom/squareup/money/MoneyFormatter;

    move-result-object v0

    return-object v0
.end method
