.class public final Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;
.super Ljava/lang/Object;
.source "HttpModule_ProvideOkHttpClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lokhttp3/OkHttpClient;",
        ">;"
    }
.end annotation


# instance fields
.field private final authInterceptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/AuthHttpInterceptor;",
            ">;"
        }
    .end annotation
.end field

.field private final builderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient$Builder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient$Builder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/AuthHttpInterceptor;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->builderProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->authInterceptorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient$Builder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/interceptor/AuthHttpInterceptor;",
            ">;)",
            "Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideOkHttpClient(Lokhttp3/OkHttpClient$Builder;Lcom/squareup/http/interceptor/AuthHttpInterceptor;)Lokhttp3/OkHttpClient;
    .locals 0

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/http/HttpModule;->provideOkHttpClient(Lokhttp3/OkHttpClient$Builder;Lcom/squareup/http/interceptor/AuthHttpInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lokhttp3/OkHttpClient;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->get()Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->builderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient$Builder;

    iget-object v1, p0, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->authInterceptorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/http/interceptor/AuthHttpInterceptor;

    invoke-static {v0, v1}, Lcom/squareup/http/HttpModule_ProvideOkHttpClientFactory;->provideOkHttpClient(Lokhttp3/OkHttpClient$Builder;Lcom/squareup/http/interceptor/AuthHttpInterceptor;)Lokhttp3/OkHttpClient;

    move-result-object v0

    return-object v0
.end method
