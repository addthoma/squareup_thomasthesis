.class public final Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;
.super Ljava/lang/Object;
.source "HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lretrofit/client/Client;",
        ">;"
    }
.end annotation


# instance fields
.field private final clientProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;->clientProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lokhttp3/OkHttpClient;",
            ">;)",
            "Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;

    invoke-direct {v0, p0}, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideDiagnosticsRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/http/HttpRetrofit1Module;->provideDiagnosticsRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lretrofit/client/Client;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;->get()Lretrofit/client/Client;

    move-result-object v0

    return-object v0
.end method

.method public get()Lretrofit/client/Client;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;->clientProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokhttp3/OkHttpClient;

    invoke-static {v0}, Lcom/squareup/http/HttpRetrofit1Module_ProvideDiagnosticsRetrofitClientFactory;->provideDiagnosticsRetrofitClient(Lokhttp3/OkHttpClient;)Lretrofit/client/Client;

    move-result-object v0

    return-object v0
.end method
