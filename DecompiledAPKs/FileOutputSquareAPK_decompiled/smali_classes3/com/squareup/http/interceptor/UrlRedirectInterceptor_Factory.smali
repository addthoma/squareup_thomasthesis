.class public final Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;
.super Ljava/lang/Object;
.source "UrlRedirectInterceptor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/UrlRedirectInterceptor;",
        ">;"
    }
.end annotation


# instance fields
.field private final redirectUrlSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/RedirectUrlSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final redirectingServerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field

.field private final settingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/RedirectUrlSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->redirectingServerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->redirectUrlSelectorProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->settingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/RedirectUrlSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/UrlRedirectSetting;",
            ">;)",
            "Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/http/Server;Lcom/squareup/http/RedirectUrlSelector;Lcom/squareup/http/UrlRedirectSetting;)Lcom/squareup/http/interceptor/UrlRedirectInterceptor;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/http/interceptor/UrlRedirectInterceptor;-><init>(Lcom/squareup/http/Server;Lcom/squareup/http/RedirectUrlSelector;Lcom/squareup/http/UrlRedirectSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/UrlRedirectInterceptor;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->redirectingServerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/Server;

    iget-object v1, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->redirectUrlSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/http/RedirectUrlSelector;

    iget-object v2, p0, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->settingProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/http/UrlRedirectSetting;

    invoke-static {v0, v1, v2}, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->newInstance(Lcom/squareup/http/Server;Lcom/squareup/http/RedirectUrlSelector;Lcom/squareup/http/UrlRedirectSetting;)Lcom/squareup/http/interceptor/UrlRedirectInterceptor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/UrlRedirectInterceptor_Factory;->get()Lcom/squareup/http/interceptor/UrlRedirectInterceptor;

    move-result-object v0

    return-object v0
.end method
