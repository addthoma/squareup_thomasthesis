.class Lcom/squareup/http/interceptor/ProfilingInterceptor;
.super Ljava/lang/Object;
.source "ProfilingInterceptor.java"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field private profiler:Lcom/squareup/http/HttpProfiler;


# direct methods
.method constructor <init>(Lcom/squareup/http/HttpProfiler;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/http/interceptor/ProfilingInterceptor;->profiler:Lcom/squareup/http/HttpProfiler;

    return-void
.end method

.method static getCallInformation(Lokhttp3/Request;J)Lcom/squareup/http/HttpProfiler$CallInformation;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 44
    invoke-virtual {p0}, Lokhttp3/Request;->isHttps()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "https"

    goto :goto_0

    :cond_0
    const-string v0, "http"

    :goto_0
    move-object v3, v0

    .line 45
    invoke-virtual {p0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->encodedPath()Ljava/lang/String;

    move-result-object v5

    .line 46
    invoke-virtual {p0}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/HttpUrl;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 48
    invoke-virtual {p0}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/RequestBody;->contentLength()J

    move-result-wide v0

    :goto_1
    move-wide v6, v0

    const-string v0, "Content-Type"

    .line 49
    invoke-virtual {p0, v0}, Lokhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 51
    new-instance v0, Lcom/squareup/http/HttpProfiler$CallInformation;

    invoke-virtual {p0}, Lokhttp3/Request;->method()Ljava/lang/String;

    move-result-object v2

    move-object v1, v0

    move-wide v8, p1

    invoke-direct/range {v1 .. v10}, Lcom/squareup/http/HttpProfiler$CallInformation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/http/interceptor/ProfilingInterceptor;->profiler:Lcom/squareup/http/HttpProfiler;

    invoke-interface {v0}, Lcom/squareup/http/HttpProfiler;->beforeCall()Ljava/lang/Object;

    move-result-object v0

    .line 21
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v7

    .line 22
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    .line 24
    :try_start_0
    invoke-interface {p1, v7}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object p1

    .line 25
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    .line 26
    invoke-virtual {v7}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v3

    if-nez v3, :cond_0

    const-wide/16 v3, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v3

    invoke-virtual {v3}, Lokhttp3/ResponseBody;->contentLength()J

    move-result-wide v3

    .line 27
    :goto_0
    invoke-static {v7, v3, v4}, Lcom/squareup/http/interceptor/ProfilingInterceptor;->getCallInformation(Lokhttp3/Request;J)Lcom/squareup/http/HttpProfiler$CallInformation;

    move-result-object v3

    .line 29
    iget-object v4, p0, Lcom/squareup/http/interceptor/ProfilingInterceptor;->profiler:Lcom/squareup/http/HttpProfiler;

    sub-long v5, v1, v8

    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result v10

    move-object v1, v4

    move-object v2, v3

    move-wide v3, v5

    move v5, v10

    move-object v6, v0

    invoke-interface/range {v1 .. v6}, Lcom/squareup/http/HttpProfiler;->afterCall(Lcom/squareup/http/HttpProfiler$CallInformation;JILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 33
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    .line 34
    invoke-static {v7, v3, v4}, Lcom/squareup/http/interceptor/ProfilingInterceptor;->getCallInformation(Lokhttp3/Request;J)Lcom/squareup/http/HttpProfiler$CallInformation;

    move-result-object v3

    .line 36
    iget-object v4, p0, Lcom/squareup/http/interceptor/ProfilingInterceptor;->profiler:Lcom/squareup/http/HttpProfiler;

    sub-long v5, v1, v8

    move-object v1, v4

    move-object v2, v3

    move-object v3, p1

    move-wide v4, v5

    move-object v6, v0

    invoke-interface/range {v1 .. v6}, Lcom/squareup/http/HttpProfiler;->afterError(Lcom/squareup/http/HttpProfiler$CallInformation;Ljava/lang/Exception;JLjava/lang/Object;)V

    .line 38
    throw p1
.end method
