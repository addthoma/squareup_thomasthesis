.class public final Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;
.super Ljava/lang/Object;
.source "UserAgentModule_ProvideUserAgentIdFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;)",
            "Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;

    invoke-direct {v0, p0}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideUserAgentId(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 34
    invoke-static {p0}, Lcom/squareup/http/useragent/UserAgentModule;->provideUserAgentId(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/squareup/http/useragent/UserAgentModule_ProvideUserAgentIdFactory;->provideUserAgentId(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
