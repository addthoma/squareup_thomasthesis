.class final Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;
.super Lkotlin/jvm/internal/Lambda;
.source "UserAgentProvider.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/http/useragent/UserAgentProvider;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;Lcom/squareup/http/useragent/EnvironmentDiscovery;Ljava/util/Locale;Ljava/lang/String;Lcom/squareup/util/PosBuild;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $posBuild:Lcom/squareup/util/PosBuild;

.field final synthetic this$0:Lcom/squareup/http/useragent/UserAgentProvider;


# direct methods
.method constructor <init>(Lcom/squareup/http/useragent/UserAgentProvider;Lcom/squareup/util/PosBuild;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    iput-object p2, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->$posBuild:Lcom/squareup/util/PosBuild;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->invoke()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/lang/String;
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/http/UserAgentBuilder;

    invoke-direct {v0}, Lcom/squareup/http/UserAgentBuilder;-><init>()V

    .line 27
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getUserAgentId$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->userAgentId(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getProductVersionName$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->productVersionName(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getVersionName$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->posSdkVersionName(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 30
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->$posBuild:Lcom/squareup/util/PosBuild;

    invoke-interface {v1}, Lcom/squareup/util/PosBuild;->getGitSha()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->buildSha(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getReaderSdkBucketBinIdProvider$p(Lcom/squareup/http/useragent/UserAgentProvider;)Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/http/useragent/ReaderSdkBucketBinIdProvider;->getBucketBinId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->readerSdkBucketBinId(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 32
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getEnvironmentDiscovery$p(Lcom/squareup/http/useragent/UserAgentProvider;)Lcom/squareup/http/useragent/EnvironmentDiscovery;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/http/useragent/EnvironmentDiscovery;->getEnvironment()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->environment(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getDeviceInformation$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->deviceInformation(Ljava/lang/String;)Lcom/squareup/http/UserAgentBuilder;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/squareup/http/useragent/UserAgentProvider$userAgentString$2;->this$0:Lcom/squareup/http/useragent/UserAgentProvider;

    invoke-static {v1}, Lcom/squareup/http/useragent/UserAgentProvider;->access$getLocale$p(Lcom/squareup/http/useragent/UserAgentProvider;)Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/http/UserAgentBuilder;->build(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
