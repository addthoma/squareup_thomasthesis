.class public final Lcom/squareup/configure/item/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final configure_item_buttons:I = 0x7f0d00ef

.field public static final configure_item_checkable_row:I = 0x7f0d00f0

.field public static final configure_item_checkablegroup:I = 0x7f0d00f1

.field public static final configure_item_checkablegroup_noho:I = 0x7f0d00f2

.field public static final configure_item_checkablegroup_section:I = 0x7f0d00f3

.field public static final configure_item_description:I = 0x7f0d00f4

.field public static final configure_item_detail_view:I = 0x7f0d00f5

.field public static final configure_item_discounts:I = 0x7f0d00f6

.field public static final configure_item_duration_section:I = 0x7f0d00f7

.field public static final configure_item_fixed_price_override_section:I = 0x7f0d00f8

.field public static final configure_item_gap_time_section:I = 0x7f0d00f9

.field public static final configure_item_notes_section:I = 0x7f0d00fa

.field public static final configure_item_price_view:I = 0x7f0d00fb

.field public static final configure_item_quantity_section:I = 0x7f0d00fc

.field public static final configure_item_taxes:I = 0x7f0d00fd

.field public static final configure_item_title_section:I = 0x7f0d00fe

.field public static final configure_item_variable_price_item_variation_section:I = 0x7f0d00ff

.field public static final configure_item_variable_price_section:I = 0x7f0d0100

.field public static final void_comp_view:I = 0x7f0d0588


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
