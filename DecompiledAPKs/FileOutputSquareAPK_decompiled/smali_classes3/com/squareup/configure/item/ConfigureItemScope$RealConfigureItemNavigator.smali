.class public Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;
.super Ljava/lang/Object;
.source "ConfigureItemScope.java"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemNavigator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RealConfigureItemNavigator"
.end annotation


# instance fields
.field private final configureItemScope:Lcom/squareup/configure/item/ConfigureItemScope;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/configure/item/ConfigureItemScope;)V
    .locals 0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->device:Lcom/squareup/util/Device;

    .line 78
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    .line 79
    iput-object p3, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->configureItemScope:Lcom/squareup/configure/item/ConfigureItemScope;

    return-void
.end method


# virtual methods
.method public exit()V
    .locals 4

    .line 87
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/squareup/configure/item/InConfigureItemScope;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/configure/item/InConfigureItemScope;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method public goBack()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goToCompScreen(Ljava/lang/String;)V
    .locals 3

    .line 99
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/configure/item/ConfigureItemCompScreen;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->configureItemScope:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/configure/item/ConfigureItemCompScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToPriceScreen()V
    .locals 3

    .line 95
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/configure/item/ConfigureItemPriceScreen;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->configureItemScope:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v1, v2}, Lcom/squareup/configure/item/ConfigureItemPriceScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToVoidScreen(Ljava/lang/String;)V
    .locals 3

    .line 103
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/configure/item/ConfigureItemVoidScreen;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;->configureItemScope:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/configure/item/ConfigureItemVoidScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
