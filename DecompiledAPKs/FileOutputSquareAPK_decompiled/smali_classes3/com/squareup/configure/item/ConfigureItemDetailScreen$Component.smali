.class public interface abstract Lcom/squareup/configure/item/ConfigureItemDetailScreen$Component;
.super Ljava/lang/Object;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBarView$Component;
.implements Lcom/squareup/configure/item/ConfigureItemDetailView$Component;


# annotations
.annotation runtime Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen;
.end annotation

.annotation runtime Lcom/squareup/marin/widgets/MarinActionBarModule$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/configure/item/ConfigureItemDetailScreen$GreaterThanZeroAmountValidationModule;,
        Lcom/squareup/marin/widgets/MarinActionBarModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Component;",
        "Lcom/squareup/marin/widgets/MarinActionBarView$Component;",
        "Lcom/squareup/configure/item/ConfigureItemDetailView$Component;",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
