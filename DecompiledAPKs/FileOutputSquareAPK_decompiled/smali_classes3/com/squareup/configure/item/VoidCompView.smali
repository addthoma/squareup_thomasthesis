.class public Lcom/squareup/configure/item/VoidCompView;
.super Landroid/widget/LinearLayout;
.source "VoidCompView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/VoidCompView$Component;
    }
.end annotation


# instance fields
.field private actionBarCancelButton:Lcom/squareup/glyph/SquareGlyphView;

.field private actionBarCancelButtonText:Landroid/widget/TextView;

.field private actionBarPrimaryButton:Landroid/widget/TextView;

.field private helpText:Landroid/widget/TextView;

.field presenter:Lcom/squareup/configure/item/VoidCompPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private reasonHeader:Landroid/widget/TextView;

.field private reasonRadios:Lcom/squareup/widgets/CheckableGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const-class p2, Lcom/squareup/configure/item/VoidCompView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/configure/item/VoidCompView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/configure/item/VoidCompView$Component;->inject(Lcom/squareup/configure/item/VoidCompView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 119
    sget v0, Lcom/squareup/configure/item/R$id;->void_comp_cancel_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarCancelButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 120
    sget v0, Lcom/squareup/configure/item/R$id;->void_comp_cancel_button_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarCancelButtonText:Landroid/widget/TextView;

    .line 121
    sget v0, Lcom/squareup/configure/item/R$id;->void_comp_primary_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarPrimaryButton:Landroid/widget/TextView;

    .line 122
    sget v0, Lcom/squareup/configure/item/R$id;->void_comp_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->helpText:Landroid/widget/TextView;

    .line 123
    sget v0, Lcom/squareup/configure/item/R$id;->void_comp_reason_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->reasonHeader:Landroid/widget/TextView;

    .line 124
    sget v0, Lcom/squareup/configure/item/R$id;->void_comp_reason_radios:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->reasonRadios:Lcom/squareup/widgets/CheckableGroup;

    return-void
.end method


# virtual methods
.method public getCheckedReasonIndex()I
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->reasonRadios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    return v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 115
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public synthetic lambda$onFinishInflate$0$VoidCompView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 65
    iget-object p1, p0, Lcom/squareup/configure/item/VoidCompView;->presenter:Lcom/squareup/configure/item/VoidCompPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/configure/item/VoidCompPresenter;->onCheckedIndexChanged(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 69
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 70
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->presenter:Lcom/squareup/configure/item/VoidCompPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/configure/item/VoidCompPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->presenter:Lcom/squareup/configure/item/VoidCompPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/configure/item/VoidCompPresenter;->dropView(Ljava/lang/Object;)V

    .line 75
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    invoke-direct {p0}, Lcom/squareup/configure/item/VoidCompView;->bindViews()V

    .line 52
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarCancelButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/configure/item/VoidCompView$1;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/VoidCompView$1;-><init>(Lcom/squareup/configure/item/VoidCompView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarPrimaryButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/configure/item/VoidCompView$2;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/VoidCompView$2;-><init>(Lcom/squareup/configure/item/VoidCompView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->reasonRadios:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/configure/item/-$$Lambda$VoidCompView$GcLnjLK8AmF0AzTAX3cD6fdgZi8;

    invoke-direct {v1, p0}, Lcom/squareup/configure/item/-$$Lambda$VoidCompView$GcLnjLK8AmF0AzTAX3cD6fdgZi8;-><init>(Lcom/squareup/configure/item/VoidCompView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public populateReasons(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 107
    invoke-virtual {p0}, Lcom/squareup/configure/item/VoidCompView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/VoidCompView;->reasonRadios:Lcom/squareup/widgets/CheckableGroup;

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/ui/CheckableGroups;->addAsRows(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;Ljava/util/List;Z)V

    return-void
.end method

.method public setActionBarCancelButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarCancelButtonText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setCheckedIndex(I)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->reasonRadios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method

.method public setHelpText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->helpText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setHelpTextVisibleOrGone(Z)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->helpText:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setPrimaryButtonEnabled(Z)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarPrimaryButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->actionBarPrimaryButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setReasonHeader(Ljava/lang/CharSequence;)V
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/configure/item/VoidCompView;->reasonHeader:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
