.class final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;
.super Ljava/lang/Object;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateDurationRow(Lcom/squareup/configure/item/ConfigureItemDetailView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .line 829
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getRandom$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/util/Random;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$setDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/Long;)V

    .line 830
    iget-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getDurationPickerRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    move-result-object p1

    .line 831
    sget v0, Lcom/squareup/widgets/pos/R$string;->duration_title:I

    .line 832
    new-instance v1, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getScopeRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    const-string v3, "scopeRunner.state"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemState;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object v2

    const-string v3, "scopeRunner.state.duration"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateDurationRow$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v3}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getDurationToken$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Ljava/lang/Long;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    const/4 v2, 0x1

    .line 830
    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-void
.end method
