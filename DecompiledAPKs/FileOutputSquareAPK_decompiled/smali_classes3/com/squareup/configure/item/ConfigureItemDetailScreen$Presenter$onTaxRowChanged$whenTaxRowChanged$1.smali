.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ConfigureItemDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onTaxRowChanged(Landroid/widget/Checkable;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "failure",
        "",
        "success",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $checked:Z

.field final synthetic $taxId:Ljava/lang/String;

.field final synthetic $view:Landroid/widget/Checkable;

.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Ljava/lang/String;ZLandroid/widget/Checkable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Landroid/widget/Checkable;",
            ")V"
        }
    .end annotation

    .line 1317
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$taxId:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$checked:Z

    iput-object p4, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$view:Landroid/widget/Checkable;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 3

    .line 1323
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$hasView(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1324
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$setIgnoreListenerChanges$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Z)V

    .line 1325
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$view:Landroid/widget/Checkable;

    iget-boolean v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$checked:Z

    xor-int/2addr v1, v2

    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1326
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$setIgnoreListenerChanges$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;Z)V

    :cond_0
    return-void
.end method

.method public success()V
    .locals 3

    .line 1319
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getScopeRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v1}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->getAvailableCartTaxesInOrder()Ljava/util/Map;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$taxId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax;

    iget-boolean v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onTaxRowChanged$whenTaxRowChanged$1;->$checked:Z

    invoke-virtual {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemState;->setTaxApplied(Lcom/squareup/checkout/Tax;Z)V

    return-void
.end method
