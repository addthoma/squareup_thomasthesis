.class public final Lcom/squareup/configure/item/ConfigureItemScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ConfigureItemScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/configure/item/ConfigureItemScope$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemScope$Component;,
        Lcom/squareup/configure/item/ConfigureItemScope$ComponentFactory;,
        Lcom/squareup/configure/item/ConfigureItemScope$Module;,
        Lcom/squareup/configure/item/ConfigureItemScope$RealConfigureItemNavigator;,
        Lcom/squareup/configure/item/ConfigureItemScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/configure/item/ConfigureItemScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final cartItem:Z

.field final indexToEdit:I

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field final workingItem:Lcom/squareup/configure/item/WorkingItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/configure/item/-$$Lambda$ConfigureItemScope$DJxgzHAiH9mv9MilY7n9Uq0v2ok;->INSTANCE:Lcom/squareup/configure/item/-$$Lambda$ConfigureItemScope$DJxgzHAiH9mv9MilY7n9Uq0v2ok;

    .line 149
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 50
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/configure/item/ConfigureItemScope;->cartItem:Z

    const/4 p1, 0x0

    .line 52
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    const/4 p1, -0x1

    .line 53
    iput p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->indexToEdit:I

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 p1, 0x0

    .line 42
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 43
    iput p2, p0, Lcom/squareup/configure/item/ConfigureItemScope;->indexToEdit:I

    const/4 p1, 0x1

    .line 44
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->cartItem:Z

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 35
    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemScope;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    const/4 p1, -0x1

    .line 36
    iput p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->indexToEdit:I

    const/4 p1, 0x0

    .line 37
    iput-boolean p1, p0, Lcom/squareup/configure/item/ConfigureItemScope;->cartItem:Z

    return-void
.end method

.method public static synthetic lambda$DJxgzHAiH9mv9MilY7n9Uq0v2ok(Landroid/os/Parcel;)Lcom/squareup/configure/item/ConfigureItemScope;
    .locals 1

    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 67
    iget-boolean p2, p0, Lcom/squareup/configure/item/ConfigureItemScope;->cartItem:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getWorkingItem()Lcom/squareup/configure/item/WorkingItem;
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemScope;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 61
    const-class v0, Lcom/squareup/configure/item/ConfigureItemScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/configure/item/ConfigureItemScope$Component;

    .line 62
    invoke-interface {v0}, Lcom/squareup/configure/item/ConfigureItemScope$Component;->scopeRunner()Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
