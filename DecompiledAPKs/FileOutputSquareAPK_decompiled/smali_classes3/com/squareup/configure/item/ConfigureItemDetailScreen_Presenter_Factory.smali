.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ConfigureItemDetailScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final durationPickerRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hostProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;"
        }
    .end annotation
.end field

.field private final intermissionHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final itemAmountValidatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final navigatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;"
        }
    .end annotation
.end field

.field private final perUnitFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scaleTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScaleTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScaleTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 100
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 101
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 102
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 103
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 104
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 105
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 106
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 107
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 108
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 109
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 110
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 111
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->hostProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 112
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 113
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 114
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 115
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->durationPickerRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 116
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->itemAmountValidatorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 117
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 118
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 119
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 120
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->scaleTrackerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 121
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->intermissionHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 122
    iput-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScaleTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/intermission/IntermissionHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;)",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 149
    new-instance v24, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v24
.end method

.method public static newInstance(Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/text/DurationFormatter;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/cogs/Cogs;Lcom/squareup/scales/ScaleTracker;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;
    .locals 25
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/configure/item/ConfigureItemNavigator;",
            "Lcom/squareup/configure/item/ConfigureItemScopeRunner;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/configure/item/ConfigureItemHost;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/scales/ScaleTracker;",
            "Lcom/squareup/intermission/IntermissionHelper;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")",
            "Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    .line 163
    new-instance v24, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    move-object/from16 v0, v24

    invoke-direct/range {v0 .. v23}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;-><init>(Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/text/DurationFormatter;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/cogs/Cogs;Lcom/squareup/scales/ScaleTracker;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/permissions/EmployeeManagement;)V

    return-object v24
.end method


# virtual methods
.method public get()Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;
    .locals 25

    move-object/from16 v0, p0

    .line 127
    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->navigatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/configure/item/ConfigureItemNavigator;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->perUnitFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v7, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->hostProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/configure/item/ConfigureItemHost;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/text/DurationFormatter;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->durationPickerRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/register/widgets/NohoDurationPickerRunner;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->itemAmountValidatorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/catalogapi/CatalogIntegrationController;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->scaleTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/scales/ScaleTracker;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->intermissionHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/intermission/IntermissionHelper;

    iget-object v1, v0, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/permissions/EmployeeManagement;

    invoke-static/range {v2 .. v24}, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->newInstance(Lcom/squareup/configure/item/ConfigureItemNavigator;Lcom/squareup/configure/item/ConfigureItemScopeRunner;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Ljavax/inject/Provider;Lcom/squareup/util/Res;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/configure/item/ConfigureItemHost;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/text/DurationFormatter;Lcom/squareup/register/widgets/NohoDurationPickerRunner;Lcom/squareup/configure/item/ConfigureItemDetailScreen$ItemAmountValidator;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/cogs/Cogs;Lcom/squareup/scales/ScaleTracker;Lcom/squareup/intermission/IntermissionHelper;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen_Presenter_Factory;->get()Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
