.class abstract Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;
.super Ljava/lang/Object;
.source "ConfigureItemCheckableRow.kt"

# interfaces
.implements Landroid/widget/Checkable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemCheckableRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "ConfigureItemCheckableRowAdapter"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\"\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u001a\u001a\u00020\u0007H\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016J\u0010\u0010\u001e\u001a\u00020\u001c2\u0008\u0010\u001f\u001a\u0004\u0018\u00010 J\u0010\u0010!\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020\u0007H&J\u0010\u0010#\u001a\u00020\u001c2\u0008\u0010$\u001a\u0004\u0018\u00010%J\u0008\u0010&\u001a\u00020\u001cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\u00020\u0007X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0006\u0010\u0008\"\u0004\u0008\t\u0010\nR\u0018\u0010\u000b\u001a\u00020\u000cX\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u0018\u0010\u0011\u001a\u00020\u0012X\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u000cX\u00a6\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0018\u0010\u000e\"\u0004\u0008\u0019\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;",
        "Landroid/widget/Checkable;",
        "view",
        "Landroid/view/View;",
        "checkable",
        "(Landroid/view/View;Landroid/widget/Checkable;)V",
        "isEnabled",
        "",
        "()Z",
        "setEnabled",
        "(Z)V",
        "label",
        "",
        "getLabel",
        "()Ljava/lang/String;",
        "setLabel",
        "(Ljava/lang/String;)V",
        "rowId",
        "",
        "getRowId",
        "()I",
        "setRowId",
        "(I)V",
        "value",
        "getValue",
        "setValue",
        "isChecked",
        "setChecked",
        "",
        "checked",
        "setOnClickListener",
        "l",
        "Landroid/view/View$OnClickListener;",
        "setSingleSelect",
        "isSingleSelect",
        "startAnimation",
        "animation",
        "Landroid/view/animation/Animation;",
        "toggle",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final checkable:Landroid/widget/Checkable;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/widget/Checkable;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkable"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->checkable:Landroid/widget/Checkable;

    return-void
.end method


# virtual methods
.method public abstract getLabel()Ljava/lang/String;
.end method

.method public abstract getRowId()I
.end method

.method public abstract getValue()Ljava/lang/String;
.end method

.method public isChecked()Z
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->checkable:Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    return v0
.end method

.method public abstract isEnabled()Z
.end method

.method public setChecked(Z)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->checkable:Landroid/widget/Checkable;

    invoke-interface {v0, p1}, Landroid/widget/Checkable;->setChecked(Z)V

    return-void
.end method

.method public abstract setEnabled(Z)V
.end method

.method public abstract setLabel(Ljava/lang/String;)V
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->view:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public abstract setRowId(I)V
.end method

.method public abstract setSingleSelect(Z)V
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method

.method public final startAnimation(Landroid/view/animation/Animation;)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->view:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;->checkable:Landroid/widget/Checkable;

    invoke-interface {v0}, Landroid/widget/Checkable;->toggle()V

    return-void
.end method
