.class public interface abstract Lcom/squareup/opentickets/PredefinedTickets;
.super Ljava/lang/Object;
.source "PredefinedTickets.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/opentickets/PredefinedTickets$NoOp;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\rJ\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003H&J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00040\u0003H&J\u001c\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\t\u001a\u00020\nH&J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\t\u001a\u00020\nH&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/opentickets/PredefinedTickets;",
        "Lmortar/Scoped;",
        "getAllAvailableTicketTemplates",
        "Lio/reactivex/Single;",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
        "getAllTicketGroups",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "getAvailableTicketTemplatesForGroup",
        "ticketGroupId",
        "",
        "updateAvailableTemplateCountForGroup",
        "",
        "NoOp",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getAllAvailableTicketTemplates()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getAllTicketGroups()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getAvailableTicketTemplatesForGroup(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract updateAvailableTemplateCountForGroup(Ljava/lang/String;)V
.end method
