.class final Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealUnsyncedOpenTicketsSpinner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->setUpSpinner(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;


# direct methods
.method constructor <init>(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;->this$0:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 3

    .line 19
    iget-object v0, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;->this$0:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;

    invoke-static {v0}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->access$getOnCheckOpenTicketCount$p(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1;->this$0:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;

    invoke-static {v1}, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;->access$getSpinner$p(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v1

    sget-object v2, Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;->INSTANCE:Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner$setUpSpinner$1$1;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 23
    invoke-virtual {v0}, Lrx/Observable;->subscribe()Lrx/Subscription;

    move-result-object v0

    const-string v1, "onCheckOpenTicketCount\n \u2026 )\n          .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
