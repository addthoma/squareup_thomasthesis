.class public final Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;
.super Ljava/lang/Object;
.source "RealTicketsSweeperManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/opentickets/RealTicketsSweeperManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final deleteSweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketDeleteClosedSweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final syncSweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSyncSweeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSyncSweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketDeleteClosedSweeper;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;->syncSweeperProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;->deleteSweeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSyncSweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketDeleteClosedSweeper;",
            ">;)",
            "Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/opentickets/TicketsSyncSweeper;Lcom/squareup/opentickets/TicketDeleteClosedSweeper;)Lcom/squareup/opentickets/RealTicketsSweeperManager;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/opentickets/RealTicketsSweeperManager;

    invoke-direct {v0, p0, p1}, Lcom/squareup/opentickets/RealTicketsSweeperManager;-><init>(Lcom/squareup/opentickets/TicketsSyncSweeper;Lcom/squareup/opentickets/TicketDeleteClosedSweeper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/opentickets/RealTicketsSweeperManager;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;->syncSweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/opentickets/TicketsSyncSweeper;

    iget-object v1, p0, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;->deleteSweeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/opentickets/TicketDeleteClosedSweeper;

    invoke-static {v0, v1}, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;->newInstance(Lcom/squareup/opentickets/TicketsSyncSweeper;Lcom/squareup/opentickets/TicketDeleteClosedSweeper;)Lcom/squareup/opentickets/RealTicketsSweeperManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealTicketsSweeperManager_Factory;->get()Lcom/squareup/opentickets/RealTicketsSweeperManager;

    move-result-object v0

    return-object v0
.end method
