.class public final Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;
.super Ljava/lang/Object;
.source "RealTicketsListScheduler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/opentickets/RealTicketsListScheduler;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final delegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final localTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final registrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushServiceRegistrar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushServiceRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->localTicketsProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->registrarProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->delegateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets$InternalTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushServiceRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;)",
            "Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/util/Clock;Lcom/squareup/pushmessages/PushServiceRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/pushmessages/PushMessageDelegate;)Lcom/squareup/opentickets/RealTicketsListScheduler;
    .locals 8

    .line 63
    new-instance v7, Lcom/squareup/opentickets/RealTicketsListScheduler;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/opentickets/RealTicketsListScheduler;-><init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/util/Clock;Lcom/squareup/pushmessages/PushServiceRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/pushmessages/PushMessageDelegate;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/opentickets/RealTicketsListScheduler;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->localTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tickets/Tickets$InternalTickets;

    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->registrarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/pushmessages/PushServiceRegistrar;

    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->delegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/pushmessages/PushMessageDelegate;

    invoke-static/range {v1 .. v6}, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->newInstance(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tickets/Tickets$InternalTickets;Lcom/squareup/util/Clock;Lcom/squareup/pushmessages/PushServiceRegistrar;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/pushmessages/PushMessageDelegate;)Lcom/squareup/opentickets/RealTicketsListScheduler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/opentickets/RealTicketsListScheduler_Factory;->get()Lcom/squareup/opentickets/RealTicketsListScheduler;

    move-result-object v0

    return-object v0
.end method
