.class public final Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;
.super Ljava/lang/Object;
.source "NoOpAvailableTemplateCountCache.kt"

# interfaces
.implements Lcom/squareup/opentickets/AvailableTemplateCountCache;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0018\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0006H\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;",
        "Lcom/squareup/opentickets/AvailableTemplateCountCache;",
        "()V",
        "clear",
        "",
        "getAvailableTemplateCountForGroup",
        "",
        "ticketGroup",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "updateAvailableTemplateCountForGroup",
        "ticketGroupId",
        "",
        "count",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;

    invoke-direct {v0}, Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;-><init>()V

    sput-object v0, Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;->INSTANCE:Lcom/squareup/opentickets/NoOpAvailableTemplateCountCache;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 0

    return-void
.end method

.method public getAvailableTemplateCountForGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)I
    .locals 1

    const-string/jumbo v0, "ticketGroup"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public updateAvailableTemplateCountForGroup(Ljava/lang/String;I)V
    .locals 0

    const-string/jumbo p2, "ticketGroupId"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
