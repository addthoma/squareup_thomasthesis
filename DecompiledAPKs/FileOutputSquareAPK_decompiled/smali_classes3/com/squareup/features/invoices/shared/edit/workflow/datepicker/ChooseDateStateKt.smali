.class public final Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;
.super Ljava/lang/Object;
.source "ChooseDateState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDateState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDateState.kt\ncom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,113:1\n158#2,3:114\n161#2:119\n158#2,3:120\n161#2:125\n165#2:126\n165#2:130\n1591#3,2:117\n1591#3,2:123\n56#4:127\n61#4:128\n61#4:129\n56#4:131\n56#4:132\n56#4:133\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDateState.kt\ncom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt\n*L\n74#1,3:114\n74#1:119\n85#1,3:120\n85#1:125\n97#1:126\n107#1:130\n74#1,2:117\n85#1,2:123\n98#1:127\n99#1:128\n100#1:129\n108#1:131\n109#1:132\n110#1:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0004*\u00020\u0002H\u0002\u001a\u0014\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\t\u001a\u00020\u0006*\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0004H\u0002\u00a8\u0006\n"
    }
    d2 = {
        "readChooseDateScreenData",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "Lokio/BufferedSource;",
        "readCustomDateScreenData",
        "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
        "writeChooseDateScreenData",
        "",
        "Lokio/BufferedSink;",
        "data",
        "writeCustomDateScreenData",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$readChooseDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->readChooseDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$readCustomDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->readCustomDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeChooseDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->writeChooseDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    return-void
.end method

.method public static final synthetic access$writeCustomDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->writeCustomDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)V

    return-void
.end method

.method private static final readChooseDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;
    .locals 8

    .line 96
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 126
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    .line 97
    sget-object v4, Lcom/squareup/invoices/workflow/edit/DateOption;->Companion:Lcom/squareup/invoices/workflow/edit/DateOption$Companion;

    invoke-virtual {v4, p0}, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;->readDateOption(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/DateOption;

    move-result-object v4

    .line 126
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    check-cast v2, Ljava/util/List;

    .line 127
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 128
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    const/4 v4, 0x0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v5, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v5}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v4

    :goto_1
    move-object v5, v0

    check-cast v5, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 129
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v4, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v4}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v4

    :cond_2
    move-object v6, v4

    check-cast v6, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 101
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p0

    .line 95
    new-instance v7, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-object v0, v7

    move-object v4, v5

    move-object v5, v6

    move v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object v7
.end method

.method private static final readCustomDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;
    .locals 7

    .line 106
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_0

    .line 107
    sget-object v4, Lcom/squareup/invoices/workflow/edit/DateOption;->Companion:Lcom/squareup/invoices/workflow/edit/DateOption$Companion;

    invoke-virtual {v4, p0}, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;->readDateOption(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/DateOption;

    move-result-object v4

    .line 130
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    check-cast v2, Ljava/util/List;

    .line 131
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 132
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v4, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v4}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 133
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v5, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v5}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 111
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v6

    .line 105
    new-instance p0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Z)V

    return-object p0
.end method

.method private static final writeChooseDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V
    .locals 3

    .line 75
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 76
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getDateOptions()Ljava/util/List;

    move-result-object v0

    .line 115
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p0, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 116
    check-cast v0, Ljava/lang/Iterable;

    .line 117
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 116
    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 76
    sget-object v2, Lcom/squareup/invoices/workflow/edit/DateOption;->Companion:Lcom/squareup/invoices/workflow/edit/DateOption$Companion;

    invoke-virtual {v2, p0, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;->writeDateOption(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/DateOption;)V

    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 78
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getCustomDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 80
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;->getShouldShowActualDate()Z

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    return-void
.end method

.method private static final writeCustomDateScreenData(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)V
    .locals 3

    .line 86
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 87
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getDateOptions()Ljava/util/List;

    move-result-object v0

    .line 121
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p0, v1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 122
    check-cast v0, Ljava/lang/Iterable;

    .line 123
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 122
    check-cast v1, Lcom/squareup/invoices/workflow/edit/DateOption;

    .line 87
    sget-object v2, Lcom/squareup/invoices/workflow/edit/DateOption;->Companion:Lcom/squareup/invoices/workflow/edit/DateOption$Companion;

    invoke-virtual {v2, p0, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;->writeDateOption(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/DateOption;)V

    goto :goto_0

    .line 88
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 89
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 90
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getMaxDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getShouldShowActualDate()Z

    move-result p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    return-void
.end method
