.class public final Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;
.super Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local;
.source "InvoiceAttachmentState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LocalFile"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\t\u001a\u00020\nH\u00d6\u0001J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\nH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0019\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\nH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local;",
        "file",
        "Ljava/io/File;",
        "(Ljava/io/File;)V",
        "getFile",
        "()Ljava/io/File;",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final file:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile$Creator;

    invoke-direct {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile$Creator;-><init>()V

    sput-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 214
    invoke-direct {p0, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;Ljava/io/File;ILjava/lang/Object;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->copy(Ljava/io/File;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    return-object v0
.end method

.method public final copy(Ljava/io/File;)Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;
    .locals 1

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    iget-object p1, p1, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFile()Ljava/io/File;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LocalFile(file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/AttachmentStatus$Local$LocalFile;->file:Ljava/io/File;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
