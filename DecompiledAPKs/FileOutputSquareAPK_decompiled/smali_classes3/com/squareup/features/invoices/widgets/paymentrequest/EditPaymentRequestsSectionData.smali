.class public final Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;
.super Ljava/lang/Object;
.source "EditPaymentRequestsSectionData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;,
        Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002\u001d\u001eB+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0008H\u00c6\u0003J7\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00082\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;",
        "",
        "config",
        "Lcom/squareup/invoices/PaymentRequestsConfig;",
        "requests",
        "",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
        "useDepositRemainderClickListeners",
        "",
        "remainderClickable",
        "(Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZ)V",
        "getConfig",
        "()Lcom/squareup/invoices/PaymentRequestsConfig;",
        "getRemainderClickable",
        "()Z",
        "getRequests",
        "()Ljava/util/List;",
        "getUseDepositRemainderClickListeners",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Factory",
        "Request",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final config:Lcom/squareup/invoices/PaymentRequestsConfig;

.field private final remainderClickable:Z

.field private final requests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;"
        }
    .end annotation
.end field

.field private final useDepositRemainderClickListeners:Z


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/PaymentRequestsConfig;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requests"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    iput-boolean p4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZILjava/lang/Object;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-boolean p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->copy(Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZ)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/PaymentRequestsConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZ)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/PaymentRequestsConfig;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;ZZ)",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "requests"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;-><init>(Lcom/squareup/invoices/PaymentRequestsConfig;Ljava/util/List;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    iget-boolean v1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    iget-boolean p1, p1, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConfig()Lcom/squareup/invoices/PaymentRequestsConfig;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    return-object v0
.end method

.method public final getRemainderClickable()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    return v0
.end method

.method public final getRequests()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Request;",
            ">;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    return-object v0
.end method

.method public final getUseDepositRemainderClickListeners()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditPaymentRequestsSectionData(config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->config:Lcom/squareup/invoices/PaymentRequestsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", requests="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->requests:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", useDepositRemainderClickListeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->useDepositRemainderClickListeners:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", remainderClickable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;->remainderClickable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
