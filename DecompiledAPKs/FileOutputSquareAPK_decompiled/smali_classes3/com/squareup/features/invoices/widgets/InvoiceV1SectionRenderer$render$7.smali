.class final Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;
.super Ljava/lang/Object;
.source "InvoiceV1SectionRenderer.kt"

# interfaces
.implements Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onConfirm"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onEvent:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_render:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;


# direct methods
.method constructor <init>(Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;->$this_render:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;->$onEvent:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onConfirm()V
    .locals 3

    .line 249
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;->$onEvent:Lkotlin/jvm/functions/Function1;

    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer$render$7;->$this_render:Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/widgets/SectionElement$ConfirmButtonData;->getEvent()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
