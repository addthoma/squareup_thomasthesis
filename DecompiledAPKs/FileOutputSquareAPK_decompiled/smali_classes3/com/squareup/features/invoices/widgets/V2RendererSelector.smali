.class public final Lcom/squareup/features/invoices/widgets/V2RendererSelector;
.super Ljava/lang/Object;
.source "V2RendererSelector.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/V2RendererSelector;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "v1SectionRenderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
        "v2SectionRenderer",
        "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;)V",
        "get",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final v1SectionRenderer:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

.field private final v2SectionRenderer:Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "v1SectionRenderer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "v2SectionRenderer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;->v1SectionRenderer:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;->v2SectionRenderer:Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    return-void
.end method


# virtual methods
.method public final get()Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 14
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;->v2SectionRenderer:Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    goto :goto_0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;->v1SectionRenderer:Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    :goto_0
    return-object v0
.end method
