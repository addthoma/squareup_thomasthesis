.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;
.super Ljava/lang/Object;
.source "InvoiceTimelineCta.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J8\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u00012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rH\u0002J*\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u00012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rJ*\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u000b\u001a\u00020\u00012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;",
        "",
        "timelineCtaEnabled",
        "Lcom/squareup/TimelineCtaEnabled;",
        "(Lcom/squareup/TimelineCtaEnabled;)V",
        "create",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;",
        "defaultString",
        "",
        "callToAction",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "key",
        "onClick",
        "Lkotlin/Function1;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;",
        "",
        "createForSectionData",
        "event",
        "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
        "timeline",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final timelineCtaEnabled:Lcom/squareup/TimelineCtaEnabled;


# direct methods
.method public constructor <init>(Lcom/squareup/TimelineCtaEnabled;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "timelineCtaEnabled"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;->timelineCtaEnabled:Lcom/squareup/TimelineCtaEnabled;

    return-void
.end method

.method private final create(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
            "Ljava/lang/Object;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;"
        }
    .end annotation

    if-nez p2, :cond_0

    .line 106
    new-instance p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$None;

    invoke-direct {p2, p1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$None;-><init>(Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    goto :goto_0

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;->timelineCtaEnabled:Lcom/squareup/TimelineCtaEnabled;

    invoke-interface {v0, p2}, Lcom/squareup/TimelineCtaEnabled;->isEnabled(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 109
    new-instance v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;

    .line 111
    new-instance v1, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory$create$1;

    invoke-direct {v1, p4, p3, p2}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory$create$1;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 109
    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Lkotlin/jvm/functions/Function0;)V

    move-object p2, v0

    check-cast p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    goto :goto_0

    .line 113
    :cond_1
    new-instance p3, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->text:Ljava/lang/String;

    const-string p4, "callToAction.text"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p3, p1, p2}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Disabled;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, p3

    check-cast p2, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    :goto_0
    return-object p2
.end method


# virtual methods
.method public final createForSectionData(Lcom/squareup/protos/client/invoice/InvoiceEvent;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            "Ljava/lang/Object;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClick"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceEvent;->description:Ljava/lang/String;

    .line 79
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceEvent;->event_call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    .line 77
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;->create(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    move-result-object p1

    return-object p1
.end method

.method public final createForSectionData(Lcom/squareup/protos/client/invoice/InvoiceTimeline;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline;",
            "Ljava/lang/Object;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;"
        }
    .end annotation

    const-string/jumbo v0, "timeline"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClick"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->secondary_status:Ljava/lang/String;

    .line 92
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline;->call_to_action:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    .line 90
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Factory;->create(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;

    move-result-object p1

    return-object p1
.end method
