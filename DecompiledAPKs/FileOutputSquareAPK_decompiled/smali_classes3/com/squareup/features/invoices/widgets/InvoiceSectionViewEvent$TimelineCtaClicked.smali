.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;
.super Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;
.source "InvoiceSectionViewEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimelineCtaClicked"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;",
        "key",
        "",
        "callToAction",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "(Ljava/lang/Object;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)V",
        "getCallToAction",
        "()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final callToAction:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;)V
    .locals 1

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "callToAction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;-><init>(Ljava/lang/Object;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;->callToAction:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    return-void
.end method


# virtual methods
.method public final getCallToAction()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;->callToAction:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    return-object v0
.end method
