.class public final Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;
.super Ljava/lang/Object;
.source "InvoiceV2SectionRenderer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;"
        }
    .end annotation

    .line 47
    new-instance v6, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Landroid/content/res/Resources;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;)Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;
    .locals 7

    .line 53
    new-instance v6, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;-><init>(Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Landroid/content/res/Resources;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;

    iget-object v3, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v4, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->newInstance(Lcom/squareup/features/invoices/widgets/InvoiceV1SectionRenderer;Landroid/content/res/Resources;Lcom/squareup/features/invoices/widgets/cart/InvoiceV2CartEntryViewsFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsViewFactory;)Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer_Factory;->get()Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;

    move-result-object v0

    return-object v0
.end method
