.class public final Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;
.super Ljava/lang/Object;
.source "V2WidgetsModule_Companion_InvoiceSectionRendererFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;",
        ">;"
    }
.end annotation


# instance fields
.field private final selectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/V2RendererSelector;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/V2RendererSelector;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;->selectorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/features/invoices/widgets/V2RendererSelector;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;

    invoke-direct {v0, p0}, Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static invoiceSectionRenderer(Lcom/squareup/features/invoices/widgets/V2RendererSelector;)Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/features/invoices/widgets/V2WidgetsModule;->Companion:Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/features/invoices/widgets/V2WidgetsModule$Companion;->invoiceSectionRenderer(Lcom/squareup/features/invoices/widgets/V2RendererSelector;)Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;->selectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/V2RendererSelector;

    invoke-static {v0}, Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;->invoiceSectionRenderer(Lcom/squareup/features/invoices/widgets/V2RendererSelector;)Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/V2WidgetsModule_Companion_InvoiceSectionRendererFactory;->get()Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;

    move-result-object v0

    return-object v0
.end method
