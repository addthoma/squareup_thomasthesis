.class public final enum Lcom/squareup/marketfont/MarketFont$Weight;
.super Ljava/lang/Enum;
.source "MarketFont.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marketfont/MarketFont;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Weight"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/marketfont/MarketFont$Weight;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/marketfont/MarketFont$Weight;

.field public static final enum BOLD:Lcom/squareup/marketfont/MarketFont$Weight;

.field public static final DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

.field public static final enum LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

.field public static final enum MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

.field public static final enum REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;


# instance fields
.field public final italicId:I

.field public final name:Ljava/lang/String;

.field public final normalId:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 10
    new-instance v6, Lcom/squareup/marketfont/MarketFont$Weight;

    sget v4, Lcom/squareup/fonts/market/R$raw;->sqmarket_regular:I

    sget v5, Lcom/squareup/fonts/market/R$raw;->sqmarket_regular_italic:I

    const-string v1, "REGULAR"

    const/4 v2, 0x0

    const-string v3, "Regular"

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/marketfont/MarketFont$Weight;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v6, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 11
    new-instance v0, Lcom/squareup/marketfont/MarketFont$Weight;

    sget v11, Lcom/squareup/fonts/market/R$raw;->sqmarket_light:I

    sget v12, Lcom/squareup/fonts/market/R$raw;->sqmarket_light_italic:I

    const-string v8, "LIGHT"

    const/4 v9, 0x1

    const-string v10, "Light"

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/marketfont/MarketFont$Weight;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 12
    new-instance v0, Lcom/squareup/marketfont/MarketFont$Weight;

    sget v5, Lcom/squareup/fonts/market/R$raw;->sqmarket_medium:I

    sget v6, Lcom/squareup/fonts/market/R$raw;->sqmarket_medium_italic:I

    const-string v2, "MEDIUM"

    const/4 v3, 0x2

    const-string v4, "Medium"

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/marketfont/MarketFont$Weight;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 13
    new-instance v0, Lcom/squareup/marketfont/MarketFont$Weight;

    sget v11, Lcom/squareup/fonts/market/R$raw;->sqmarket_bold:I

    sget v12, Lcom/squareup/fonts/market/R$raw;->sqmarket_bold_italic:I

    const-string v8, "BOLD"

    const/4 v9, 0x3

    const-string v10, "Bold"

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/marketfont/MarketFont$Weight;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->BOLD:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/marketfont/MarketFont$Weight;

    .line 9
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x2

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->BOLD:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x3

    aput-object v2, v0, v3

    sput-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->$VALUES:[Lcom/squareup/marketfont/MarketFont$Weight;

    .line 15
    sput-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcom/squareup/marketfont/MarketFont$Weight;->name:Ljava/lang/String;

    .line 40
    iput p4, p0, Lcom/squareup/marketfont/MarketFont$Weight;->normalId:I

    .line 41
    iput p5, p0, Lcom/squareup/marketfont/MarketFont$Weight;->italicId:I

    return-void
.end method

.method public static resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I
    .locals 3

    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    .line 20
    :cond_1
    invoke-static {p0, v0, v1}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;ZZ)I

    move-result p0

    return p0
.end method

.method public static resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;ZZ)I
    .locals 0

    if-eqz p1, :cond_0

    .line 29
    sget-object p0, Lcom/squareup/marketfont/MarketFont$Weight;->BOLD:Lcom/squareup/marketfont/MarketFont$Weight;

    :cond_0
    if-eqz p2, :cond_1

    .line 31
    iget p0, p0, Lcom/squareup/marketfont/MarketFont$Weight;->italicId:I

    goto :goto_0

    :cond_1
    iget p0, p0, Lcom/squareup/marketfont/MarketFont$Weight;->normalId:I

    :goto_0
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/marketfont/MarketFont$Weight;

    return-object p0
.end method

.method public static values()[Lcom/squareup/marketfont/MarketFont$Weight;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->$VALUES:[Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0}, [Lcom/squareup/marketfont/MarketFont$Weight;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/marketfont/MarketFont$Weight;

    return-object v0
.end method
