.class public interface abstract Lcom/squareup/core/location/monitors/LocationMonitor;
.super Ljava/lang/Object;
.source "LocationMonitor.java"


# virtual methods
.method public abstract getBestLastKnownLocation()Landroid/location/Location;
.end method

.method public abstract hasPermission()Z
.end method

.method public abstract isEnabled()Z
.end method

.method public abstract isGpsEnabled()Z
.end method

.method public abstract isNetworkEnabled()Z
.end method
