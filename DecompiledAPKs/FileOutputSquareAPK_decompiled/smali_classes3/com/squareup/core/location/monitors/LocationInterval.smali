.class public final enum Lcom/squareup/core/location/monitors/LocationInterval;
.super Ljava/lang/Enum;
.source "LocationInterval.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/core/location/monitors/LocationInterval;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/core/location/monitors/LocationInterval;

.field public static final enum FREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;

.field public static final enum STANDARD:Lcom/squareup/core/location/monitors/LocationInterval;

.field public static final enum X2_INFREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;


# instance fields
.field private final interval:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 11
    new-instance v0, Lcom/squareup/core/location/monitors/LocationInterval;

    const/4 v1, 0x0

    const-string v2, "STANDARD"

    const-wide/32 v3, 0xea60

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/squareup/core/location/monitors/LocationInterval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/core/location/monitors/LocationInterval;->STANDARD:Lcom/squareup/core/location/monitors/LocationInterval;

    .line 16
    new-instance v0, Lcom/squareup/core/location/monitors/LocationInterval;

    const/4 v2, 0x1

    const-string v3, "FREQUENT"

    const-wide/16 v4, 0x4e20

    invoke-direct {v0, v3, v2, v4, v5}, Lcom/squareup/core/location/monitors/LocationInterval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/core/location/monitors/LocationInterval;->FREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;

    .line 21
    new-instance v0, Lcom/squareup/core/location/monitors/LocationInterval;

    const/4 v3, 0x2

    const-string v4, "X2_INFREQUENT"

    const-wide/32 v5, 0x2932e00

    invoke-direct {v0, v4, v3, v5, v6}, Lcom/squareup/core/location/monitors/LocationInterval;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Lcom/squareup/core/location/monitors/LocationInterval;->X2_INFREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/core/location/monitors/LocationInterval;

    .line 6
    sget-object v4, Lcom/squareup/core/location/monitors/LocationInterval;->STANDARD:Lcom/squareup/core/location/monitors/LocationInterval;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/core/location/monitors/LocationInterval;->FREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/core/location/monitors/LocationInterval;->X2_INFREQUENT:Lcom/squareup/core/location/monitors/LocationInterval;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/core/location/monitors/LocationInterval;->$VALUES:[Lcom/squareup/core/location/monitors/LocationInterval;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-wide p3, p0, Lcom/squareup/core/location/monitors/LocationInterval;->interval:J

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/core/location/monitors/LocationInterval;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/core/location/monitors/LocationInterval;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/core/location/monitors/LocationInterval;

    return-object p0
.end method

.method public static values()[Lcom/squareup/core/location/monitors/LocationInterval;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/core/location/monitors/LocationInterval;->$VALUES:[Lcom/squareup/core/location/monitors/LocationInterval;

    invoke-virtual {v0}, [Lcom/squareup/core/location/monitors/LocationInterval;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/core/location/monitors/LocationInterval;

    return-object v0
.end method


# virtual methods
.method public interval()J
    .locals 2

    .line 30
    iget-wide v0, p0, Lcom/squareup/core/location/monitors/LocationInterval;->interval:J

    return-wide v0
.end method
