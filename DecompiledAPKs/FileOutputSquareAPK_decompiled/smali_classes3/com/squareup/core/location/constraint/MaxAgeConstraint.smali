.class public Lcom/squareup/core/location/constraint/MaxAgeConstraint;
.super Ljava/lang/Object;
.source "MaxAgeConstraint.java"

# interfaces
.implements Lcom/squareup/core/location/constraint/LocationConstraint;


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final maxAgeMillis:J


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;J)V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/core/location/constraint/MaxAgeConstraint;->clock:Lcom/squareup/util/Clock;

    .line 14
    iput-wide p2, p0, Lcom/squareup/core/location/constraint/MaxAgeConstraint;->maxAgeMillis:J

    return-void
.end method


# virtual methods
.method public isSatisfied(Landroid/location/Location;)Z
    .locals 3

    .line 18
    iget-object v0, p0, Lcom/squareup/core/location/constraint/MaxAgeConstraint;->clock:Lcom/squareup/util/Clock;

    iget-wide v1, p0, Lcom/squareup/core/location/constraint/MaxAgeConstraint;->maxAgeMillis:J

    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/util/Clock;->withinPast(Landroid/location/Location;J)Z

    move-result p1

    return p1
.end method
