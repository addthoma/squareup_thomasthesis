.class Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;
.super Ljava/lang/Object;
.source "LastBestLocationStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/core/location/LastBestLocationStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LocationStruct"
.end annotation


# instance fields
.field final accuracy:F

.field final latitude:D

.field final longitude:D

.field final provider:Ljava/lang/String;

.field final time:J


# direct methods
.method constructor <init>(Landroid/location/Location;)V
    .locals 2

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->latitude:D

    .line 71
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->longitude:D

    .line 72
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iput v0, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->accuracy:F

    .line 73
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->provider:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->time:J

    return-void
.end method


# virtual methods
.method toLocation()Landroid/location/Location;
    .locals 3

    .line 78
    new-instance v0, Landroid/location/Location;

    iget-object v1, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->provider:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 79
    iget v1, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->accuracy:F

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 80
    iget-wide v1, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->latitude:D

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 81
    iget-wide v1, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->longitude:D

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 82
    iget-wide v1, p0, Lcom/squareup/core/location/LastBestLocationStore$LocationStruct;->time:J

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    return-object v0
.end method
