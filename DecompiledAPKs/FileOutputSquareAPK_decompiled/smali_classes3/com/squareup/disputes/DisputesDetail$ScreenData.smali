.class public final Lcom/squareup/disputes/DisputesDetail$ScreenData;
.super Ljava/lang/Object;
.source "DisputesDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
        "",
        "disputedPayment",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "(Lcom/squareup/protos/client/cbms/DisputedPayment;)V",
        "getDisputedPayment",
        "()Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V
    .locals 1

    const-string v0, "disputedPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesDetail$ScreenData;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    return-void
.end method


# virtual methods
.method public final getDisputedPayment()Lcom/squareup/protos/client/cbms/DisputedPayment;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/disputes/DisputesDetail$ScreenData;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    return-object v0
.end method
