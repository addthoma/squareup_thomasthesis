.class public final Lcom/squareup/disputes/Starter_Factory;
.super Ljava/lang/Object;
.source "Starter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/Starter;",
        ">;"
    }
.end annotation


# instance fields
.field private final reactorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesReactor;",
            ">;"
        }
    .end annotation
.end field

.field private final rendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesRenderer;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/disputes/Starter_Factory;->reactorProvider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/disputes/Starter_Factory;->rendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/Starter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesReactor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesRenderer;",
            ">;)",
            "Lcom/squareup/disputes/Starter_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/disputes/Starter_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/disputes/Starter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesRenderer;)Lcom/squareup/disputes/Starter;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/disputes/Starter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/disputes/Starter;-><init>(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesRenderer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/Starter;
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/disputes/Starter_Factory;->reactorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/disputes/DisputesReactor;

    iget-object v1, p0, Lcom/squareup/disputes/Starter_Factory;->rendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/disputes/DisputesRenderer;

    invoke-static {v0, v1}, Lcom/squareup/disputes/Starter_Factory;->newInstance(Lcom/squareup/disputes/DisputesReactor;Lcom/squareup/disputes/DisputesRenderer;)Lcom/squareup/disputes/Starter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/disputes/Starter_Factory;->get()Lcom/squareup/disputes/Starter;

    move-result-object v0

    return-object v0
.end method
