.class public final Lcom/squareup/disputes/DisputesTutorialCreator_Factory;
.super Ljava/lang/Object;
.source "DisputesTutorialCreator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/DisputesTutorialCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final appIdlingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;"
        }
    .end annotation
.end field

.field private final handlesDisputesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/DisputesTutorialCreator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorial;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;)",
            "Lcom/squareup/disputes/DisputesTutorialCreator_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/disputes/api/HandlesDisputes;Ljavax/inject/Provider;Lcom/squareup/ui/main/AppIdling;)Lcom/squareup/disputes/DisputesTutorialCreator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorial;",
            ">;",
            "Lcom/squareup/ui/main/AppIdling;",
            ")",
            "Lcom/squareup/disputes/DisputesTutorialCreator;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/disputes/DisputesTutorialCreator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/disputes/DisputesTutorialCreator;-><init>(Lcom/squareup/disputes/api/HandlesDisputes;Ljavax/inject/Provider;Lcom/squareup/ui/main/AppIdling;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/DisputesTutorialCreator;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/disputes/api/HandlesDisputes;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->tutorialProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/AppIdling;

    invoke-static {v0, v1, v2}, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->newInstance(Lcom/squareup/disputes/api/HandlesDisputes;Ljavax/inject/Provider;Lcom/squareup/ui/main/AppIdling;)Lcom/squareup/disputes/DisputesTutorialCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesTutorialCreator_Factory;->get()Lcom/squareup/disputes/DisputesTutorialCreator;

    move-result-object v0

    return-object v0
.end method
