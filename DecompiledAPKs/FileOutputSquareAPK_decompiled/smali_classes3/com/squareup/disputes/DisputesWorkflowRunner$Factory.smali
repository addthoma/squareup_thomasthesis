.class public final Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;
.super Ljava/lang/Object;
.source "DisputesWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u00020\n2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u000cR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;",
        "",
        "viewFactoryFactory",
        "Lcom/squareup/disputes/DisputesViewFactory$Factory;",
        "starter",
        "Lcom/squareup/disputes/Starter;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Lcom/squareup/disputes/DisputesViewFactory$Factory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)V",
        "create",
        "Lcom/squareup/disputes/DisputesWorkflowRunner;",
        "section",
        "Ljava/lang/Class;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final starter:Lcom/squareup/disputes/Starter;

.field private final viewFactoryFactory:Lcom/squareup/disputes/DisputesViewFactory$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/DisputesViewFactory$Factory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "starter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->viewFactoryFactory:Lcom/squareup/disputes/DisputesViewFactory$Factory;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->starter:Lcom/squareup/disputes/Starter;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->container:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;Ljava/lang/Class;ILjava/lang/Object;)Lcom/squareup/disputes/DisputesWorkflowRunner;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 38
    check-cast p1, Ljava/lang/Class;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->create(Ljava/lang/Class;)Lcom/squareup/disputes/DisputesWorkflowRunner;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Ljava/lang/Class;)Lcom/squareup/disputes/DisputesWorkflowRunner;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/disputes/DisputesWorkflowRunner;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/disputes/DisputesWorkflowRunner;

    .line 40
    iget-object v1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->viewFactoryFactory:Lcom/squareup/disputes/DisputesViewFactory$Factory;

    invoke-virtual {v1, p1}, Lcom/squareup/disputes/DisputesViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/disputes/DisputesViewFactory;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->starter:Lcom/squareup/disputes/Starter;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->container:Lcom/squareup/ui/main/PosContainer;

    const/4 v3, 0x0

    .line 39
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/disputes/DisputesWorkflowRunner;-><init>(Lcom/squareup/disputes/DisputesViewFactory;Lcom/squareup/disputes/Starter;Lcom/squareup/ui/main/PosContainer;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
