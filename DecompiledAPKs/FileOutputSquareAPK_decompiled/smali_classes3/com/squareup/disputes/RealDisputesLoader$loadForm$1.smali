.class final Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;
.super Ljava/lang/Object;
.source "DisputesLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/RealDisputesLoader;->loadForm(Lcom/squareup/protos/client/cbms/DisputedPayment;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/irf/GetFormResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

.field final synthetic this$0:Lcom/squareup/disputes/RealDisputesLoader;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/RealDisputesLoader;Lcom/squareup/protos/client/cbms/DisputedPayment;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    iput-object p2, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->$disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/irf/GetFormResponse;",
            ">;)",
            "Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    const-string v1, "successOrFailure.response"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/protos/client/irf/GetFormResponse;

    iget-object v1, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->$disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-direct {v0, p1, v1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;-><init>(Lcom/squareup/protos/client/irf/GetFormResponse;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    .line 94
    check-cast v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;

    return-object v0

    .line 96
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->this$0:Lcom/squareup/disputes/RealDisputesLoader;

    invoke-static {v0}, Lcom/squareup/disputes/RealDisputesLoader;->access$getMessages$p(Lcom/squareup/disputes/RealDisputesLoader;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/disputes/R$string;->disputes_error_title:I

    new-instance v2, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;

    invoke-direct {v2, p0}, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1$1;-><init>(Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->component1()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->component2()Ljava/lang/String;

    move-result-object p1

    .line 103
    new-instance v1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    iget-object v2, p0, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->$disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-direct {v1, v0, p1, v2}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;

    return-object v1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/RealDisputesLoader$loadForm$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/disputes/DisputesState$ChallengeSummaryState;

    move-result-object p1

    return-object p1
.end method
