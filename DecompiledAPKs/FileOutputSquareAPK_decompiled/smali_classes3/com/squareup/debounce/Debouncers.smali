.class public final Lcom/squareup/debounce/Debouncers;
.super Ljava/lang/Object;
.source "Debouncers.java"


# static fields
.field private static final enableAgain:Ljava/lang/Runnable;

.field private static volatile enabled:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/debounce/-$$Lambda$Debouncers$R3jSHhJvSMn4kqRTl4WR3dDxIYc;->INSTANCE:Lcom/squareup/debounce/-$$Lambda$Debouncers$R3jSHhJvSMn4kqRTl4WR3dDxIYc;

    sput-object v0, Lcom/squareup/debounce/Debouncers;->enableAgain:Ljava/lang/Runnable;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static attemptPerform(Landroid/view/View;)Z
    .locals 2

    .line 35
    sget-boolean v0, Lcom/squareup/debounce/Debouncers;->enabled:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 36
    sput-boolean v1, Lcom/squareup/debounce/Debouncers;->enabled:Z

    .line 37
    sget-object v0, Lcom/squareup/debounce/Debouncers;->enableAgain:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    const/4 p0, 0x1

    return p0

    :cond_0
    return v1
.end method

.method public static canPerform()Z
    .locals 1

    .line 22
    sget-boolean v0, Lcom/squareup/debounce/Debouncers;->enabled:Z

    return v0
.end method

.method public static debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/debounce/Debouncers$1;

    invoke-direct {v0, p0}, Lcom/squareup/debounce/Debouncers$1;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public static debounceItemClick(Landroid/widget/AdapterView$OnItemClickListener;)Lcom/squareup/debounce/DebouncedOnItemClickListener;
    .locals 1

    .line 63
    new-instance v0, Lcom/squareup/debounce/Debouncers$2;

    invoke-direct {v0, p0}, Lcom/squareup/debounce/Debouncers$2;-><init>(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method public static debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;
    .locals 1

    .line 77
    new-instance v0, Lcom/squareup/debounce/Debouncers$3;

    invoke-direct {v0, p0}, Lcom/squareup/debounce/Debouncers$3;-><init>(Ljava/lang/Runnable;)V

    return-object v0
.end method

.method static synthetic lambda$static$0()V
    .locals 1

    const/4 v0, 0x1

    .line 14
    sput-boolean v0, Lcom/squareup/debounce/Debouncers;->enabled:Z

    return-void
.end method

.method public static setEnabledForTesting(Z)V
    .locals 0

    .line 26
    sput-boolean p0, Lcom/squareup/debounce/Debouncers;->enabled:Z

    return-void
.end method
