.class public interface abstract Lcom/squareup/log/OhSnapLogger;
.super Ljava/lang/Object;
.source "OhSnapLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/OhSnapLogger$EventType;
    }
.end annotation


# virtual methods
.method public abstract getLastEventsAsString()Ljava/lang/String;
.end method

.method public abstract log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V
.end method

.method public abstract logExtraDataForCrash()V
.end method

.method public abstract logFull(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V
.end method
