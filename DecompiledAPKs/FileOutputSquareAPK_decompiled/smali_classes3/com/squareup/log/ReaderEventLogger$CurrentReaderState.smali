.class public Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CurrentReaderState"
.end annotation


# instance fields
.field private currentState:Lcom/squareup/dipper/events/BleConnectionState;

.field private hardwareSerialNumber:Ljava/lang/String;

.field private previousState:Lcom/squareup/dipper/events/BleConnectionState;

.field final synthetic this$0:Lcom/squareup/log/ReaderEventLogger;

.field private timeOfLastCommunicationFromReader:J

.field private timeOfLastConnectionAttempt:J

.field private timeSinceLastSuccessfulConnection:J


# direct methods
.method protected constructor <init>(Lcom/squareup/log/ReaderEventLogger;)V
    .locals 2

    .line 1411
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->this$0:Lcom/squareup/log/ReaderEventLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1412
    sget-object p1, Lcom/squareup/dipper/events/BleConnectionState;->NONE:Lcom/squareup/dipper/events/BleConnectionState;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->previousState:Lcom/squareup/dipper/events/BleConnectionState;

    .line 1413
    sget-object p1, Lcom/squareup/dipper/events/BleConnectionState;->NONE:Lcom/squareup/dipper/events/BleConnectionState;

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->currentState:Lcom/squareup/dipper/events/BleConnectionState;

    const-wide/16 v0, -0x1

    .line 1415
    iput-wide v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeOfLastConnectionAttempt:J

    .line 1416
    iput-wide v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeSinceLastSuccessfulConnection:J

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;)Ljava/lang/String;
    .locals 0

    .line 1411
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->hardwareSerialNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1411
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->hardwareSerialNumber:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getCurrentState()Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    .line 1428
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->currentState:Lcom/squareup/dipper/events/BleConnectionState;

    return-object v0
.end method

.method public getHardwareSerialNumber()Ljava/lang/String;
    .locals 1

    .line 1460
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->hardwareSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getPreviousState()Lcom/squareup/dipper/events/BleConnectionState;
    .locals 1

    .line 1420
    iget-object v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->previousState:Lcom/squareup/dipper/events/BleConnectionState;

    return-object v0
.end method

.method public getTimeOfLastCommunicationFromReader()J
    .locals 2

    .line 1436
    iget-wide v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeOfLastCommunicationFromReader:J

    return-wide v0
.end method

.method public getTimeOfLastConnectionAttempt()J
    .locals 2

    .line 1444
    iget-wide v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeOfLastConnectionAttempt:J

    return-wide v0
.end method

.method public getTimeSinceLastSuccessfulConnection()J
    .locals 2

    .line 1452
    iget-wide v0, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeSinceLastSuccessfulConnection:J

    return-wide v0
.end method

.method public setCurrentState(Lcom/squareup/dipper/events/BleConnectionState;)V
    .locals 0

    .line 1432
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->currentState:Lcom/squareup/dipper/events/BleConnectionState;

    return-void
.end method

.method public setHardwareSerialNumber(Ljava/lang/String;)V
    .locals 0

    .line 1464
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->hardwareSerialNumber:Ljava/lang/String;

    return-void
.end method

.method public setPreviousState(Lcom/squareup/dipper/events/BleConnectionState;)V
    .locals 0

    .line 1424
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->previousState:Lcom/squareup/dipper/events/BleConnectionState;

    return-void
.end method

.method public setTimeOfLastCommunicationFromReader(J)V
    .locals 0

    .line 1440
    iput-wide p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeOfLastCommunicationFromReader:J

    return-void
.end method

.method public setTimeOfLastConnectionAttempt(J)V
    .locals 0

    .line 1448
    iput-wide p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeOfLastConnectionAttempt:J

    return-void
.end method

.method public setTimeSinceLastSuccessfulConnection(J)V
    .locals 0

    .line 1456
    iput-wide p1, p0, Lcom/squareup/log/ReaderEventLogger$CurrentReaderState;->timeSinceLastSuccessfulConnection:J

    return-void
.end method
