.class Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BluetoothStatusEvent"
.end annotation


# instance fields
.field final bluetoothStatus:Ljava/lang/String;

.field final isBleSupported:Z

.field final isBluetoothSupported:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;ZZ)V
    .locals 2

    .line 1537
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/ReaderEventName;->BLUETOOTH_STATE_CHANGED:Lcom/squareup/analytics/ReaderEventName;

    iget-object v1, v1, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 1538
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;->bluetoothStatus:Ljava/lang/String;

    .line 1539
    iput-boolean p2, p0, Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;->isBleSupported:Z

    .line 1540
    iput-boolean p3, p0, Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;->isBluetoothSupported:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ZZLcom/squareup/log/ReaderEventLogger$1;)V
    .locals 0

    .line 1530
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/log/ReaderEventLogger$BluetoothStatusEvent;-><init>(Ljava/lang/String;ZZ)V

    return-void
.end method
