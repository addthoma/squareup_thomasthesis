.class public final Lcom/squareup/log/OhSnapEvent;
.super Ljava/lang/Object;
.source "OhSnapEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/OhSnapEvent$Orientation;
    }
.end annotation


# static fields
.field public static final MAX_MESSAGE_LENGTH:I = 0x7d0

.field private static final sdf:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/text/SimpleDateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private static final serial:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public final eventType:Lcom/squareup/log/OhSnapLogger$EventType;

.field public final id:Ljava/lang/String;

.field public final message:Ljava/lang/String;

.field private final orientation:Lcom/squareup/log/OhSnapEvent$Orientation;

.field public final time:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 9
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/squareup/log/OhSnapEvent;->serial:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 13
    new-instance v0, Lcom/squareup/log/OhSnapEvent$1;

    invoke-direct {v0}, Lcom/squareup/log/OhSnapEvent$1;-><init>()V

    sput-object v0, Lcom/squareup/log/OhSnapEvent;->sdf:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V
    .locals 1

    .line 32
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;Ljava/util/Date;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;Ljava/util/Date;)V
    .locals 4

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lcom/squareup/log/OhSnapEvent;->serial:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "%09d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/log/OhSnapEvent;->id:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/squareup/log/OhSnapEvent;->eventType:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 38
    invoke-static {p3}, Lcom/squareup/log/OhSnapEvent;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/OhSnapEvent;->message:Ljava/lang/String;

    .line 39
    iput-object p4, p0, Lcom/squareup/log/OhSnapEvent;->time:Ljava/util/Date;

    .line 40
    iput-object p2, p0, Lcom/squareup/log/OhSnapEvent;->orientation:Lcom/squareup/log/OhSnapEvent$Orientation;

    return-void
.end method

.method static trim(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 57
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method public getOrientation()Lcom/squareup/log/OhSnapEvent$Orientation;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/log/OhSnapEvent;->orientation:Lcom/squareup/log/OhSnapEvent$Orientation;

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    :cond_0
    return-object v0
.end method

.method public getTimeString()Ljava/lang/String;
    .locals 2

    .line 53
    sget-object v0, Lcom/squareup/log/OhSnapEvent;->sdf:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/squareup/log/OhSnapEvent;->time:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/log/OhSnapEvent;->getTimeString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/log/OhSnapEvent;->eventType:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lcom/squareup/log/OhSnapEvent;->getOrientation()Lcom/squareup/log/OhSnapEvent$Orientation;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/log/OhSnapEvent;->message:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "%s - %s - %s - %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
