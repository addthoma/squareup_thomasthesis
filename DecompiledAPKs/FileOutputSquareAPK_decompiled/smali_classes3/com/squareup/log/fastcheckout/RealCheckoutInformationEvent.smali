.class public Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "RealCheckoutInformationEvent.java"

# interfaces
.implements Lcom/squareup/log/fastcheckout/CheckoutInformationEvent;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;
    }
.end annotation


# instance fields
.field final _payment_token:Ljava/lang/String;

.field final email_on_file:Z

.field final local_hour:I

.field final number_of_milliseconds:I

.field final number_of_screens:I

.field final number_of_taps:I

.field final skip_receipt_screen_enabled:Z

.field final skip_signature_enabled:Z

.field final tender_type:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;ILjava/lang/String;)V
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CHECKOUT_INFORMATION:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 24
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$000(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->number_of_taps:I

    .line 25
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$100(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->number_of_screens:I

    .line 26
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$200(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->_payment_token:Ljava/lang/String;

    .line 27
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$300(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->skip_receipt_screen_enabled:Z

    .line 28
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$400(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->skip_signature_enabled:Z

    .line 29
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$500(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->email_on_file:Z

    .line 30
    invoke-static {p1}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;->access$600(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;)I

    move-result p1

    iput p1, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->local_hour:I

    .line 31
    iput p2, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->number_of_milliseconds:I

    .line 32
    iput-object p3, p0, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;->tender_type:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;ILjava/lang/String;Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$1;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent;-><init>(Lcom/squareup/log/fastcheckout/RealCheckoutInformationEvent$Builder;ILjava/lang/String;)V

    return-void
.end method
