.class public Lcom/squareup/log/fastcheckout/SkipReceiptScreenToggleEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "SkipReceiptScreenToggleEvent.java"


# instance fields
.field public final skip_receipt_toggle_enabled:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->SKIP_RECEIPT_SCREEN_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 13
    iput-boolean p1, p0, Lcom/squareup/log/fastcheckout/SkipReceiptScreenToggleEvent;->skip_receipt_toggle_enabled:Z

    return-void
.end method
