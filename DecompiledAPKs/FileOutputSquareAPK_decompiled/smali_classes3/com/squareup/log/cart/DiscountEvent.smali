.class public Lcom/squareup/log/cart/DiscountEvent;
.super Lcom/squareup/log/cart/CartInteractionEvent;
.source "DiscountEvent.java"


# instance fields
.field private final discount_type:Lcom/squareup/api/items/Discount$DiscountType;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;Lcom/squareup/api/items/Discount$DiscountType;)V
    .locals 7

    .line 14
    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_DISCOUNT:Lcom/squareup/analytics/RegisterTimingName;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/log/cart/CartInteractionEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;)V

    .line 15
    iput-object p6, p0, Lcom/squareup/log/cart/DiscountEvent;->discount_type:Lcom/squareup/api/items/Discount$DiscountType;

    return-void
.end method
