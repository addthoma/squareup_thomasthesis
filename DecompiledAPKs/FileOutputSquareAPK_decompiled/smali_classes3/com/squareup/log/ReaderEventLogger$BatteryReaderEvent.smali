.class public Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;
.super Lcom/squareup/log/ReaderEvent;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BatteryReaderEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;
    }
.end annotation


# instance fields
.field public final percentage:I


# direct methods
.method public constructor <init>(Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;)V
    .locals 0

    .line 1119
    invoke-direct {p0, p1}, Lcom/squareup/log/ReaderEvent;-><init>(Lcom/squareup/log/ReaderEvent$Builder;)V

    .line 1120
    iget p1, p1, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent$Builder;->percentage:I

    iput p1, p0, Lcom/squareup/log/ReaderEventLogger$BatteryReaderEvent;->percentage:I

    return-void
.end method
