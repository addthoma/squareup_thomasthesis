.class public interface abstract Lcom/squareup/log/CheckoutInformationEventLogger;
.super Ljava/lang/Object;
.source "CheckoutInformationEventLogger.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\t\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\"\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\nH&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\nH&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0008\u0010\u000e\u001a\u00020\u0003H&J\u0008\u0010\u000f\u001a\u00020\u0003H&J\u0008\u0010\u0010\u001a\u00020\u0003H&J\u001a\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/log/CheckoutInformationEventLogger;",
        "",
        "cancelCheckout",
        "",
        "finishCheckout",
        "tenderType",
        "Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;",
        "billServerId",
        "",
        "forceUpdateEndTime",
        "",
        "setEmailOnFile",
        "emailOnFile",
        "showScreen",
        "startCheckoutIfHasNot",
        "tap",
        "updateTentativeEndTime",
        "userProvideEmail",
        "transactionAmount",
        "",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelCheckout()V
.end method

.method public abstract finishCheckout(Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;Ljava/lang/String;Z)V
.end method

.method public abstract setEmailOnFile(Z)V
.end method

.method public abstract showScreen()V
.end method

.method public abstract startCheckoutIfHasNot()V
.end method

.method public abstract tap()V
.end method

.method public abstract updateTentativeEndTime()V
.end method

.method public abstract userProvideEmail(JLjava/lang/String;)V
.end method
