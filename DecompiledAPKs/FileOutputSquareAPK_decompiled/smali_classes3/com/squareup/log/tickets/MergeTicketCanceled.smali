.class public Lcom/squareup/log/tickets/MergeTicketCanceled;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "MergeTicketCanceled.java"


# instance fields
.field private final number_of_items_in_parent_transaction:I

.field private final number_of_items_in_transactions_to_merge:I

.field private final number_of_transactions_to_merge:I

.field private final parent_transaction_client_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIILjava/lang/String;)V
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MERGE_TICKETS_CANCELED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 18
    iput p1, p0, Lcom/squareup/log/tickets/MergeTicketCanceled;->number_of_transactions_to_merge:I

    .line 19
    iput p2, p0, Lcom/squareup/log/tickets/MergeTicketCanceled;->number_of_items_in_transactions_to_merge:I

    .line 20
    iput p3, p0, Lcom/squareup/log/tickets/MergeTicketCanceled;->number_of_items_in_parent_transaction:I

    .line 21
    iput-object p4, p0, Lcom/squareup/log/tickets/MergeTicketCanceled;->parent_transaction_client_id:Ljava/lang/String;

    return-void
.end method
