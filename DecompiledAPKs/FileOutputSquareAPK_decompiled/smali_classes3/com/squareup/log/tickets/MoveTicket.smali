.class public Lcom/squareup/log/tickets/MoveTicket;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "MoveTicket.java"


# instance fields
.field public final moved_to_existing_ticket:Z

.field public final number_of_tickets_moved:I


# direct methods
.method public constructor <init>(ZI)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->OPEN_TICKETS_MOVE_TICKETS:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 14
    iput-boolean p1, p0, Lcom/squareup/log/tickets/MoveTicket;->moved_to_existing_ticket:Z

    .line 15
    iput p2, p0, Lcom/squareup/log/tickets/MoveTicket;->number_of_tickets_moved:I

    return-void
.end method
