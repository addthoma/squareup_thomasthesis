.class public Lcom/squareup/log/tickets/OpenTicketsLogger;
.super Ljava/lang/Object;
.source "OpenTicketsLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;
    }
.end annotation


# static fields
.field private static final UNSET_INT:I = -0x1

.field private static final UNSET_STRING:Ljava/lang/String;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final clock:Lcom/squareup/util/Clock;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private listResponseToTicketListStartedAtMs:J

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfTicketsInResponse:I

.field private ticketClientId:Ljava/lang/String;

.field private ticketInCartUiStartedAtMs:J

.field private ticketSelectedToCartUiStartedAtMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    .line 52
    iput-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketInCartUiStartedAtMs:J

    const/4 v2, -0x1

    .line 54
    iput v2, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->numberOfTicketsInResponse:I

    .line 55
    iput-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->listResponseToTicketListStartedAtMs:J

    .line 57
    sget-object v2, Lcom/squareup/log/tickets/OpenTicketsLogger;->UNSET_STRING:Ljava/lang/String;

    iput-object v2, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketClientId:Ljava/lang/String;

    .line 58
    iput-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketSelectedToCartUiStartedAtMs:J

    .line 63
    iput-object p1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    .line 64
    iput-object p2, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->clock:Lcom/squareup/util/Clock;

    .line 65
    iput-object p3, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 66
    iput-object p4, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method private getDurationMs(J)I
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, p1

    long-to-int p1, v0

    return p1
.end method


# virtual methods
.method public logExitEditTicket(Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;)V
    .locals 1

    .line 82
    new-instance v0, Lcom/squareup/log/tickets/ExitEditTicket;

    invoke-virtual {p1}, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->stringValue()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/log/tickets/ExitEditTicket;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logTicketAction(Lcom/squareup/analytics/event/v1/ActionEvent;)V

    return-void
.end method

.method public logMergeTickets(IILjava/util/List;Ljava/lang/String;ILcom/squareup/protos/common/Money;ILcom/squareup/protos/common/Money;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/lang/String;",
            "I",
            "Lcom/squareup/protos/common/Money;",
            "I",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 96
    iget-object v1, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 97
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/common/Money;

    .line 99
    invoke-static {v1, v5}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    goto :goto_0

    :cond_0
    if-nez p6, :cond_1

    .line 103
    iget-object v4, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    goto :goto_1

    :cond_1
    move-object/from16 v4, p6

    :goto_1
    if-nez p8, :cond_2

    .line 106
    iget-object v5, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v2, v3, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    goto :goto_2

    :cond_2
    move-object/from16 v2, p8

    .line 109
    :goto_2
    iget-object v3, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v12, Lcom/squareup/log/tickets/MergeTicketBefore;

    iget-object v5, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 112
    invoke-interface {v5, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 113
    invoke-interface {v1, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v5, v12

    move v6, p1

    move v7, p2

    move/from16 v8, p5

    move-object/from16 v9, p4

    invoke-direct/range {v5 .. v11}, Lcom/squareup/log/tickets/MergeTicketBefore;-><init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-interface {v3, v12}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 115
    iget-object v1, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/log/tickets/MergeTicketAfter;

    iget-object v4, v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 117
    invoke-interface {v4, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v4, p4

    move/from16 v5, p7

    invoke-direct {v3, v5, v4, v2}, Lcom/squareup/log/tickets/MergeTicketAfter;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 115
    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logMergeTicketsCanceled(IIILjava/lang/String;)V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/tickets/MergeTicketCanceled;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/log/tickets/MergeTicketCanceled;-><init>(IIILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logTap(Lcom/squareup/analytics/RegisterTapName;)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public logTicketAction(Lcom/squareup/analytics/RegisterActionName;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public logTicketAction(Lcom/squareup/analytics/event/v1/ActionEvent;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public startListResponseToTicketList(I)V
    .locals 2

    .line 171
    iput p1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->numberOfTicketsInResponse:I

    .line 172
    iget-object p1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->listResponseToTicketListStartedAtMs:J

    return-void
.end method

.method public startTicketInCartUi()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketInCartUiStartedAtMs:J

    return-void
.end method

.method public startTicketSelectedToCartUi(Ljava/lang/String;)V
    .locals 2

    .line 150
    iput-object p1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketClientId:Ljava/lang/String;

    .line 151
    iget-object p1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->clock:Lcom/squareup/util/Clock;

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getUptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketSelectedToCartUiStartedAtMs:J

    return-void
.end method

.method public stopListResponseToTicketList()V
    .locals 6

    .line 177
    iget-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->listResponseToTicketListStartedAtMs:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->numberOfTicketsInResponse:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    goto :goto_0

    .line 182
    :cond_0
    new-instance v4, Lcom/squareup/log/tickets/ListResponseToTicketList;

    .line 183
    invoke-direct {p0, v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->getDurationMs(J)I

    move-result v0

    iget v1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->numberOfTicketsInResponse:I

    invoke-direct {v4, v0, v1}, Lcom/squareup/log/tickets/ListResponseToTicketList;-><init>(II)V

    .line 185
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 188
    iput v5, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->numberOfTicketsInResponse:I

    .line 189
    iput-wide v2, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->listResponseToTicketListStartedAtMs:J

    :cond_1
    :goto_0
    return-void
.end method

.method public stopTicketInCartUi()V
    .locals 5

    .line 138
    iget-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketInCartUiStartedAtMs:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    .line 142
    :cond_0
    new-instance v4, Lcom/squareup/log/tickets/TicketInCartUi;

    invoke-direct {p0, v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->getDurationMs(J)I

    move-result v0

    invoke-direct {v4, v0}, Lcom/squareup/log/tickets/TicketInCartUi;-><init>(I)V

    .line 143
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 146
    iput-wide v2, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketInCartUiStartedAtMs:J

    return-void
.end method

.method public stopTicketSelectedToCartUi()V
    .locals 6

    .line 156
    iget-wide v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketSelectedToCartUiStartedAtMs:J

    const-wide/16 v2, -0x1

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketClientId:Ljava/lang/String;

    sget-object v5, Lcom/squareup/log/tickets/OpenTicketsLogger;->UNSET_STRING:Ljava/lang/String;

    if-ne v4, v5, :cond_0

    goto :goto_0

    .line 160
    :cond_0
    new-instance v4, Lcom/squareup/log/tickets/TicketSelectedToCartUi;

    .line 161
    invoke-direct {p0, v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->getDurationMs(J)I

    move-result v0

    iget-object v1, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketClientId:Ljava/lang/String;

    invoke-direct {v4, v0, v1}, Lcom/squareup/log/tickets/TicketSelectedToCartUi;-><init>(ILjava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 166
    sget-object v0, Lcom/squareup/log/tickets/OpenTicketsLogger;->UNSET_STRING:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketClientId:Ljava/lang/String;

    .line 167
    iput-wide v2, p0, Lcom/squareup/log/tickets/OpenTicketsLogger;->ticketSelectedToCartUiStartedAtMs:J

    :cond_1
    :goto_0
    return-void
.end method
