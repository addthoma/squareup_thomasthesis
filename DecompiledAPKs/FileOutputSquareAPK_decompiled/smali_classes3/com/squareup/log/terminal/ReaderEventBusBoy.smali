.class public Lcom/squareup/log/terminal/ReaderEventBusBoy;
.super Ljava/lang/Object;
.source "ReaderEventBusBoy.java"

# interfaces
.implements Lcom/squareup/badbus/BadBusRegistrant;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/SwipeBus;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->analytics:Lcom/squareup/analytics/Analytics;

    .line 23
    iput-object p2, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    return-void
.end method

.method private onBadReader()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->DEAD_READER:Lcom/squareup/analytics/RegisterReaderName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V

    return-void
.end method

.method private onSwipeFailed()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->SWIPE_FAILED:Lcom/squareup/analytics/RegisterReaderName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V

    return-void
.end method

.method private onSwipeSuccess()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterReaderName;->SWIPE_SUCCEEDED:Lcom/squareup/analytics/RegisterReaderName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logReaderEvent(Lcom/squareup/analytics/RegisterReaderName;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$registerOnBus$2$ReaderEventBusBoy(Lcom/squareup/wavpool/swipe/SwipeEvents$ContactSupport;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Lcom/squareup/log/terminal/ReaderEventBusBoy;->onBadReader()V

    return-void
.end method

.method public synthetic lambda$registerOnSwipeBus$0$ReaderEventBusBoy(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/squareup/log/terminal/ReaderEventBusBoy;->onSwipeSuccess()V

    return-void
.end method

.method public synthetic lambda$registerOnSwipeBus$1$ReaderEventBusBoy(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/squareup/log/terminal/ReaderEventBusBoy;->onSwipeFailed()V

    return-void
.end method

.method public registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 33
    const-class v0, Lcom/squareup/wavpool/swipe/SwipeEvents$ContactSupport;

    invoke-virtual {p1, v0}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/log/terminal/-$$Lambda$ReaderEventBusBoy$htyOj70BhgQake7xBLn0fhy0D70;

    invoke-direct {v0, p0}, Lcom/squareup/log/terminal/-$$Lambda$ReaderEventBusBoy$htyOj70BhgQake7xBLn0fhy0D70;-><init>(Lcom/squareup/log/terminal/ReaderEventBusBoy;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public registerOnSwipeBus()Lio/reactivex/disposables/Disposable;
    .locals 4

    .line 27
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x2

    new-array v1, v1, [Lio/reactivex/disposables/Disposable;

    iget-object v2, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    .line 28
    invoke-virtual {v2}, Lcom/squareup/wavpool/swipe/SwipeBus;->DANGERdoNotUseDirectly()Lcom/squareup/wavpool/swipe/SwipeBus$Danger;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/wavpool/swipe/SwipeBus$Danger;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/log/terminal/-$$Lambda$ReaderEventBusBoy$PGLy7D_RwDfUZzkE0DIs-FLYCfY;

    invoke-direct {v3, p0}, Lcom/squareup/log/terminal/-$$Lambda$ReaderEventBusBoy$PGLy7D_RwDfUZzkE0DIs-FLYCfY;-><init>(Lcom/squareup/log/terminal/ReaderEventBusBoy;)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy;->swipeBus:Lcom/squareup/wavpool/swipe/SwipeBus;

    .line 29
    invoke-virtual {v2}, Lcom/squareup/wavpool/swipe/SwipeBus;->DANGERdoNotUseDirectly()Lcom/squareup/wavpool/swipe/SwipeBus$Danger;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/wavpool/swipe/SwipeBus$Danger;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/log/terminal/-$$Lambda$ReaderEventBusBoy$t9cW4XiqarRIdIUw7QEeCYIAzz4;

    invoke-direct {v3, p0}, Lcom/squareup/log/terminal/-$$Lambda$ReaderEventBusBoy$t9cW4XiqarRIdIUw7QEeCYIAzz4;-><init>(Lcom/squareup/log/terminal/ReaderEventBusBoy;)V

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>([Lio/reactivex/disposables/Disposable;)V

    return-object v0
.end method
