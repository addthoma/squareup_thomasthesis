.class public final Lcom/squareup/log/deposits/CardLinkingAttemptEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "CardLinkingAttemptEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/log/deposits/CardLinkingAttemptEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "deposits_card_linking_attempts_description",
        "",
        "deposits_card_linking_attempts_is_successful",
        "",
        "(Ljava/lang/String;Z)V",
        "feature-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deposits_card_linking_attempts_description:Ljava/lang/String;

.field private final deposits_card_linking_attempts_is_successful:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "deposits_card_linking_attempts_description"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deposits_card_linking_attempts"

    .line 14
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/log/deposits/CardLinkingAttemptEvent;->deposits_card_linking_attempts_description:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/squareup/log/deposits/CardLinkingAttemptEvent;->deposits_card_linking_attempts_is_successful:Z

    return-void
.end method
