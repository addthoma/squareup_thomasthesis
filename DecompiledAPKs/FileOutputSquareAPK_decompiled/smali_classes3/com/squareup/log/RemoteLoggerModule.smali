.class public abstract Lcom/squareup/log/RemoteLoggerModule;
.super Ljava/lang/Object;
.source "RemoteLoggerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideRemoteLogger(Lcom/squareup/log/CrashReportingLogger;)Lcom/squareup/logging/RemoteLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
