.class public Lcom/squareup/log/accuracy/OtherTenderEvent;
.super Lcom/squareup/log/accuracy/LocalTenderEvent;
.source "OtherTenderEvent.java"


# instance fields
.field public final _tender_name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/OtherTender;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/log/accuracy/LocalTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseLocalTender;Lcom/squareup/payment/BillPayment;)V

    .line 16
    invoke-virtual {p3}, Lcom/squareup/payment/tender/OtherTender;->getOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/log/accuracy/OtherTenderEvent;->nonLocalizedOtherTenderType(Lcom/squareup/server/account/protos/OtherTenderType;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/accuracy/OtherTenderEvent;->_tender_name:Ljava/lang/String;

    return-void
.end method

.method private nonLocalizedOtherTenderType(Lcom/squareup/server/account/protos/OtherTenderType;)Ljava/lang/String;
    .locals 0

    .line 25
    iget-object p1, p1, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 26
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 25
    invoke-static {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->fromValue(I)Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    move-result-object p1

    .line 26
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->name()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method protected paymentType()Ljava/lang/String;
    .locals 1

    const-string v0, "other"

    return-object v0
.end method
