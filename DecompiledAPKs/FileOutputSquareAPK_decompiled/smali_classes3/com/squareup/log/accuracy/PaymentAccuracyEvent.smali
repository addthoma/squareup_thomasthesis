.class public Lcom/squareup/log/accuracy/PaymentAccuracyEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PaymentAccuracyEvent.java"


# instance fields
.field public final _bill_client_id:Ljava/lang/String;

.field public final _bill_remote_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/Payment;)V
    .locals 0

    .line 28
    invoke-virtual {p3}, Lcom/squareup/payment/Payment;->getBillId()Lcom/squareup/protos/client/IdPair;

    move-result-object p3

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/protos/client/IdPair;)V
    .locals 0

    .line 33
    iget-object p2, p2, Lcom/squareup/analytics/RegisterPaymentAccuracyName;->value:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    if-nez p3, :cond_0

    .line 34
    new-instance p1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object p3

    .line 35
    :cond_0
    iget-object p1, p3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;->_bill_client_id:Ljava/lang/String;

    .line 36
    iget-object p1, p3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/log/accuracy/PaymentAccuracyEvent;->_bill_remote_id:Ljava/lang/String;

    return-void
.end method
