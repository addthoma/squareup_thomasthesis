.class public Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public cardPresenceRequired:Ljava/lang/Boolean;

.field public clientTenderId:Ljava/lang/String;

.field public paymentSessionId:Ljava/lang/String;

.field public serverTenderId:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1332
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    const/4 v0, 0x0

    .line 1333
    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->clientTenderId:Ljava/lang/String;

    .line 1334
    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->serverTenderId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1326
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;
    .locals 1

    .line 1358
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;)V

    return-object v0
.end method

.method public cardPresenceRequired(Ljava/lang/Boolean;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
    .locals 0

    .line 1353
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->cardPresenceRequired:Ljava/lang/Boolean;

    return-object p0
.end method

.method public clientTenderId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
    .locals 0

    .line 1338
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->clientTenderId:Ljava/lang/String;

    return-object p0
.end method

.method public paymentSessionId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
    .locals 0

    .line 1343
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->paymentSessionId:Ljava/lang/String;

    return-object p0
.end method

.method public serverTenderId(Ljava/lang/String;)Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;
    .locals 0

    .line 1348
    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$WithPaymentIdsReaderEvent$Builder;->serverTenderId:Ljava/lang/String;

    return-object p0
.end method
