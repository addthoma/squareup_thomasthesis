.class public abstract Lcom/squareup/invoicesappletapi/InvoicesApplet;
.super Lcom/squareup/applet/HistoryFactoryApplet;
.source "InvoicesApplet.kt"

# interfaces
.implements Lcom/squareup/ui/main/Home;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoicesappletapi/InvoicesApplet$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008&\u0018\u0000 \u00112\u00020\u00012\u00020\u0002:\u0001\u0011B\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\u001a\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00022\u0008\u0010\n\u001a\u0004\u0018\u00010\u0008H&J\n\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000eH&J\u0008\u0010\u000f\u001a\u00020\u000eH&J\u0008\u0010\u0010\u001a\u00020\u000eH&\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
        "Lcom/squareup/applet/HistoryFactoryApplet;",
        "Lcom/squareup/ui/main/Home;",
        "container",
        "Ldagger/Lazy;",
        "Lcom/squareup/ui/main/PosContainer;",
        "(Ldagger/Lazy;)V",
        "createInvoiceHistoryToSingleInvoiceDetail",
        "Lflow/History;",
        "home",
        "currentHistory",
        "getIntentScreenExtra",
        "",
        "returnAfterInvoicePayment",
        "",
        "returnAfterInvoicePaymentCanceled",
        "showFullInvoiceDetailFromReadOnly",
        "Companion",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoicesappletapi/InvoicesApplet$Companion;

.field public static final INTENT_SCREEN_EXTRA:Ljava/lang/String; = "INVOICES"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoicesappletapi/InvoicesApplet$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoicesappletapi/InvoicesApplet$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoicesappletapi/InvoicesApplet;->Companion:Lcom/squareup/invoicesappletapi/InvoicesApplet$Companion;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0, p1}, Lcom/squareup/applet/HistoryFactoryApplet;-><init>(Ldagger/Lazy;)V

    return-void
.end method


# virtual methods
.method public abstract createInvoiceHistoryToSingleInvoiceDetail(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
.end method

.method public getIntentScreenExtra()Ljava/lang/String;
    .locals 1

    const-string v0, "INVOICES"

    return-object v0
.end method

.method public abstract returnAfterInvoicePayment()V
.end method

.method public abstract returnAfterInvoicePaymentCanceled()V
.end method

.method public abstract showFullInvoiceDetailFromReadOnly()V
.end method
