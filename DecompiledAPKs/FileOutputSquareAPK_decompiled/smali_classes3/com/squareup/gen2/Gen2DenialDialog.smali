.class public final Lcom/squareup/gen2/Gen2DenialDialog;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "Gen2DenialDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/gen2/Gen2DenialDialog$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/gen2/Gen2DenialDialog$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/gen2/Gen2DenialDialog$Component;,
        Lcom/squareup/gen2/Gen2DenialDialog$ParentComponent;,
        Lcom/squareup/gen2/Gen2DenialDialog$Factory;,
        Lcom/squareup/gen2/Gen2DenialDialog$Result;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/gen2/Gen2DenialDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/gen2/Gen2DenialDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/gen2/Gen2DenialDialog;

    invoke-direct {v0}, Lcom/squareup/gen2/Gen2DenialDialog;-><init>()V

    sput-object v0, Lcom/squareup/gen2/Gen2DenialDialog;->INSTANCE:Lcom/squareup/gen2/Gen2DenialDialog;

    .line 69
    sget-object v0, Lcom/squareup/gen2/Gen2DenialDialog;->INSTANCE:Lcom/squareup/gen2/Gen2DenialDialog;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/gen2/Gen2DenialDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method
