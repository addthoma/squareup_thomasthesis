.class Lcom/squareup/orderentry/PaymentPadView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "PaymentPadView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/PaymentPadView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/PaymentPadView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/PaymentPadView;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadView$1;->this$0:Lcom/squareup/orderentry/PaymentPadView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 38
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView$1;->this$0:Lcom/squareup/orderentry/PaymentPadView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadView;->getPresenter()Lcom/squareup/orderentry/PaymentPadPresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadPresenter;->isAnimating()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 41
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView$1;->this$0:Lcom/squareup/orderentry/PaymentPadView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadView;->getPresenter()Lcom/squareup/orderentry/PaymentPadPresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/orderentry/PaymentPadPresenter;->onDoneEditingClicked()V

    return-void
.end method
