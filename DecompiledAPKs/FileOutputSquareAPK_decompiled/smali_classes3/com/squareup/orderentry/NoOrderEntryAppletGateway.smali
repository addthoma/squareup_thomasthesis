.class public final Lcom/squareup/orderentry/NoOrderEntryAppletGateway;
.super Ljava/lang/Object;
.source "OrderEntryAppletGateway.kt"

# interfaces
.implements Lcom/squareup/orderentry/OrderEntryAppletGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016J\u0008\u0010\u0005\u001a\u00020\u0006H\u0002J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0008H\u0016J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0012\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0016J\u0008\u0010\u0011\u001a\u00020\u0004H\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/orderentry/NoOrderEntryAppletGateway;",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "()V",
        "activateApplet",
        "",
        "appletNotAvailable",
        "",
        "hasFavoritesEditor",
        "",
        "hasOrderEntryApplet",
        "historyFactoryForMode",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "mode",
        "Lcom/squareup/orderentry/OrderEntryMode;",
        "historyForFavoritesEditor",
        "Lflow/History;",
        "currentHistory",
        "selectApplet",
        "order-entry-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/orderentry/NoOrderEntryAppletGateway;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;

    invoke-direct {v0}, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;->INSTANCE:Lcom/squareup/orderentry/NoOrderEntryAppletGateway;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final appletNotAvailable()Ljava/lang/Void;
    .locals 2

    .line 68
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Order Entry Applet is unavailable"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public activateApplet()V
    .locals 1

    .line 52
    invoke-direct {p0}, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;->appletNotAvailable()Ljava/lang/Void;

    const/4 v0, 0x0

    throw v0
.end method

.method public hasFavoritesEditor()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasOrderEntryApplet()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-direct {p0}, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;->appletNotAvailable()Ljava/lang/Void;

    const/4 p1, 0x0

    throw p1
.end method

.method public historyForFavoritesEditor(Lflow/History;)Lflow/History;
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;->appletNotAvailable()Ljava/lang/Void;

    const/4 p1, 0x0

    throw p1
.end method

.method public selectApplet()V
    .locals 0

    return-void
.end method
