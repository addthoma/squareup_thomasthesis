.class final Lcom/squareup/orderentry/OrderEntryAsHomeHistoryFactory;
.super Ljava/lang/Object;
.source "RealOrderEntryAppletGateway.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/orderentry/OrderEntryAsHomeHistoryFactory;",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "history",
        "Lflow/History;",
        "(Lflow/History;)V",
        "createHistory",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final history:Lflow/History;


# direct methods
.method public constructor <init>(Lflow/History;)V
    .locals 1

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryAsHomeHistoryFactory;->history:Lflow/History;

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 0

    const-string p2, "home"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryAsHomeHistoryFactory;->history:Lflow/History;

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 72
    invoke-static {p0}, Lcom/squareup/ui/main/HistoryFactory$DefaultImpls;->getPermissions(Lcom/squareup/ui/main/HistoryFactory;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
