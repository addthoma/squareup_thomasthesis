.class public final synthetic Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function4;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;

    invoke-direct {v0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreen$Presenter$1W8ghBzMR6rnhN6JDFtTOzH4-IM;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlin/Unit;

    check-cast p2, Ljava/lang/Boolean;

    check-cast p3, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    check-cast p4, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-static {p1, p2, p3, p4}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->lambda$onEnterScope$1(Lkotlin/Unit;Ljava/lang/Boolean;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Lkotlin/Pair;

    move-result-object p1

    return-object p1
.end method
