.class public Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
.super Ljava/lang/Object;
.source "KeypadEntryScreen.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/KeypadEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KeypadInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private money:Lcom/squareup/protos/common/Money;

.field private percentage:Lcom/squareup/util/Percentage;

.field private primaryButtonText:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

.field private unitAbbreviation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 185
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$1;

    invoke-direct {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$1;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, ""

    .line 94
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    .line 100
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    const/4 p1, 0x0

    .line 101
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->percentage:Lcom/squareup/util/Percentage;

    .line 102
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->title:Ljava/lang/String;

    .line 103
    iput-object p3, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->primaryButtonText:Ljava/lang/String;

    .line 104
    iput-object p4, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->unitAbbreviation:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Percentage;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->PERCENTAGE:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    const/4 v0, 0x0

    .line 109
    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    .line 110
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->percentage:Lcom/squareup/util/Percentage;

    .line 111
    iput-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->title:Ljava/lang/String;

    .line 112
    iput-object p3, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->primaryButtonText:Ljava/lang/String;

    const-string p1, ""

    .line 113
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->unitAbbreviation:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getPercentage()Lcom/squareup/util/Percentage;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->percentage:Lcom/squareup/util/Percentage;

    return-object v0
.end method

.method public getPrimaryButtonText()Ljava/lang/String;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->primaryButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    return-object v0
.end method

.method public getUnitAbbreviation()Ljava/lang/String;
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->unitAbbreviation:Ljava/lang/String;

    return-object v0
.end method

.method public setMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 1

    .line 141
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    .line 142
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    const/4 p1, 0x0

    .line 143
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->percentage:Lcom/squareup/util/Percentage;

    return-object p0
.end method

.method public setPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 1

    .line 152
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->PERCENTAGE:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    const/4 v0, 0x0

    .line 153
    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    .line 154
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->percentage:Lcom/squareup/util/Percentage;

    return-object p0
.end method

.method public setPercentage(Ljava/lang/String;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 1

    .line 148
    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-static {p1, v0}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->setPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    move-result-object p1

    return-object p1
.end method

.method public setPrimaryButtonText(Ljava/lang/String;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->primaryButtonText:Ljava/lang/String;

    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->title:Ljava/lang/String;

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 173
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    invoke-virtual {p2}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 174
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->title:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->primaryButtonText:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 176
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->unitAbbreviation:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 177
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->type:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    sget-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    if-ne p2, v0, :cond_0

    .line 178
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 179
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->money:Lcom/squareup/protos/common/Money;

    iget-object p2, p2, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {p2}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_0
    iget-object p2, p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->percentage:Lcom/squareup/util/Percentage;

    invoke-virtual {p2}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    :goto_0
    return-void
.end method
