.class Lcom/squareup/orderentry/OrderEntryView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "OrderEntryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/OrderEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/OrderEntryView;

.field final synthetic val$minFlingDistance:I


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryView;I)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryView$1;->this$0:Lcom/squareup/orderentry/OrderEntryView;

    iput p2, p0, Lcom/squareup/orderentry/OrderEntryView$1;->val$minFlingDistance:I

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 0

    .line 85
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result p2

    sub-float/2addr p1, p2

    .line 86
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p2

    iget p3, p0, Lcom/squareup/orderentry/OrderEntryView$1;->val$minFlingDistance:I

    int-to-float p3, p3

    const/4 p4, 0x1

    cmpl-float p2, p2, p3

    if-ltz p2, :cond_1

    .line 87
    iget-object p2, p0, Lcom/squareup/orderentry/OrderEntryView$1;->this$0:Lcom/squareup/orderentry/OrderEntryView;

    iget-object p2, p2, Lcom/squareup/orderentry/OrderEntryView;->presenter:Lcom/squareup/orderentry/OrderEntryScreen$Presenter;

    const/4 p3, 0x0

    cmpg-float p1, p1, p3

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p2, p1}, Lcom/squareup/orderentry/OrderEntryScreen$Presenter;->onTwoFingerFling(Z)V

    :cond_1
    return p4
.end method
