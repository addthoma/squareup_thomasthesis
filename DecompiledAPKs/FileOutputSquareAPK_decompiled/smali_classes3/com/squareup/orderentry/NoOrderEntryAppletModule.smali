.class public abstract Lcom/squareup/orderentry/NoOrderEntryAppletModule;
.super Ljava/lang/Object;
.source "NoOrderEntryAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/ui/ticket/api/NoOpenTicketsModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideOrderEntryAppletGateway()Lcom/squareup/orderentry/OrderEntryAppletGateway;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/orderentry/NoOrderEntryAppletGateway;->INSTANCE:Lcom/squareup/orderentry/NoOrderEntryAppletGateway;

    return-object v0
.end method
