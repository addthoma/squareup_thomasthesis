.class public final Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;
.super Ljava/lang/Object;
.source "OrderEntryScreenEmvCardStatusProcessor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;",
        ">;"
    }
.end annotation


# instance fields
.field private final emvDipScreenHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final keyboardHiderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->keyboardHiderProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p9, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;",
            ">;)",
            "Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;"
        }
    .end annotation

    .line 73
    new-instance v10, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;)Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;
    .locals 11

    .line 82
    new-instance v10, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;
    .locals 10

    .line 62
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/main/PosContainer;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->keyboardHiderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/main/BadKeyboardHider;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->emvDipScreenHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-static/range {v1 .. v9}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;)Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor_Factory;->get()Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;

    move-result-object v0

    return-object v0
.end method
