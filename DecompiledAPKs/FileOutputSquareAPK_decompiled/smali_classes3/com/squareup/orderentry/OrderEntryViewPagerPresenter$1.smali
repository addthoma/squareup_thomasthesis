.class Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "OrderEntryViewPagerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrolled(IFI)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$000(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$000(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 4

    .line 164
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$100(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Lcom/squareup/orderentry/pages/OrderEntryPageList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPageList;->panelAtIndex(I)Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    move-result-object v0

    .line 165
    iget-boolean v1, v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isInLibrary:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {v1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$200(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {v1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$300(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 168
    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "Tab selected: %s at index %d"

    invoke-static {p1, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$400(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 170
    sget-object p1, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->KEYPAD:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eq v0, p1, :cond_1

    .line 171
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter$1;->this$0:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {p1}, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;->access$500(Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)Lcom/squareup/tutorialv2/TutorialCore;

    move-result-object p1

    const-string v1, "Favorites Tab Selected"

    invoke-interface {p1, v1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method
