.class public final Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;
.super Ljava/lang/Object;
.source "OrderEntryPages_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/pages/OrderEntryPages;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementModeDeciderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final pageListCacheKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;>;"
        }
    .end annotation
.end field

.field private final pageListFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final shouldPreLoadOrderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;",
            ">;)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p4, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->libraryStateProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p5, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->pageListFactoryProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p6, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->pageListCacheKeyProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p7, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->preferencesProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p8, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p9, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p10, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p11, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p12, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->shouldPreLoadOrderEntryPagesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;",
            ">;)",
            "Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;"
        }
    .end annotation

    .line 91
    new-instance v13, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListStateManager;Ljava/lang/Object;Lcom/squareup/BundleKey;Landroid/content/SharedPreferences;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;)Lcom/squareup/orderentry/pages/OrderEntryPages;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Ljava/lang/Object;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages$PageListCache;",
            ">;",
            "Landroid/content/SharedPreferences;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeManagementModeDecider;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;",
            ")",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;"
        }
    .end annotation

    .line 101
    new-instance v13, Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-object/from16 v5, p4

    check-cast v5, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/orderentry/pages/OrderEntryPages;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;Lcom/squareup/BundleKey;Landroid/content/SharedPreferences;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/pages/OrderEntryPages;
    .locals 13

    .line 77
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->libraryStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/librarylist/LibraryListStateManager;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->pageListFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->pageListCacheKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/BundleKey;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->employeeManagementModeDeciderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/EmployeeManagementModeDecider;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->shouldPreLoadOrderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;

    invoke-static/range {v1 .. v12}, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Ljavax/inject/Provider;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListStateManager;Ljava/lang/Object;Lcom/squareup/BundleKey;Landroid/content/SharedPreferences;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/analytics/Analytics;Lcom/squareup/orderentry/pages/ShouldPreLoadOrderEntryPages;)Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/orderentry/pages/OrderEntryPages_Factory;->get()Lcom/squareup/orderentry/pages/OrderEntryPages;

    move-result-object v0

    return-object v0
.end method
