.class public Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;
.super Ljava/lang/Object;
.source "OrderEntryPagesRootScoped.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;


# direct methods
.method constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/orderentry/OrderEntryScreenState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->badBus:Lcom/squareup/badbus/BadBus;

    .line 40
    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->cogs:Lcom/squareup/cogs/Cogs;

    .line 41
    iput-object p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    .line 42
    iput-object p4, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    .line 43
    iput-object p5, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    return-void
.end method

.method public static synthetic lambda$jB7wZhnEbNe7pdqKcGykfHyxsbQ(Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0}, Lcom/squareup/orderentry/pages/OrderEntryPages;->interestingCatalogTypes()[Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 48
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {p1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object p1

    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 49
    :goto_0
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->loadAndPost(Ljava/lang/Runnable;Z)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected interactionModeUpdated(Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 4

    .line 70
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->isReady()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    .line 71
    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->SYNCING:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    .line 72
    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->INITIALIZING:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne v0, v1, :cond_0

    goto :goto_2

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 81
    iget-object v2, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v2, v0}, Lcom/squareup/cogs/Cogs;->releaseSyncLock(Lcom/squareup/shared/catalog/sync/CatalogSyncLock;)V

    .line 82
    iput-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    .line 84
    :cond_1
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    const/4 v2, 0x0

    if-ne p1, v0, :cond_2

    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    .line 86
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v0}, Lcom/squareup/cogs/Cogs;->preventSync()Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    goto :goto_1

    .line 88
    :cond_3
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncTasks;->explodeOnException()Lcom/squareup/shared/catalog/sync/SyncCallback;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Lcom/squareup/cogs/Cogs;->sync(Lcom/squareup/shared/catalog/sync/SyncCallback;Z)V

    .line 90
    :goto_1
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/orderentry/pages/OrderEntryPages;->loadAndPost(Ljava/lang/Runnable;Z)V

    return-void

    .line 73
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->catalogSyncLock:Lcom/squareup/shared/catalog/sync/CatalogSyncLock;

    if-nez p1, :cond_5

    return-void

    .line 74
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "CogsLock must be null in this state."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPagesRootScoped$jB7wZhnEbNe7pdqKcGykfHyxsbQ;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPagesRootScoped$jB7wZhnEbNe7pdqKcGykfHyxsbQ;-><init>(Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 57
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 60
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/pages/-$$Lambda$FxIwr89XwfIxojUGSkgs3a_NOqg;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/pages/-$$Lambda$FxIwr89XwfIxojUGSkgs3a_NOqg;-><init>(Lcom/squareup/orderentry/pages/OrderEntryPagesRootScoped;)V

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 59
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
