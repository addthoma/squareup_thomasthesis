.class public Lcom/squareup/orderentry/pages/OrderEntryPage;
.super Ljava/lang/Object;
.source "OrderEntryPage.java"


# instance fields
.field private final contentDescription:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final favoritesPageId:Ljava/lang/String;

.field private final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final key:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field private final name:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljavax/inject/Provider;Ljava/lang/String;Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/CharSequence;",
            ">;",
            "Lcom/squareup/glyph/GlyphTypeface$Glyph;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/orderentry/pages/OrderEntryPageKey;",
            ")V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p2, :cond_1

    .line 27
    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 28
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Either glyph or name needs to be set."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 30
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->contentDescription:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 32
    iput-object p3, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->name:Ljavax/inject/Provider;

    .line 33
    iput-object p5, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->key:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    .line 34
    iput-object p4, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->favoritesPageId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->contentDescription:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFavoritesPageId()Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->favoritesPageId:Ljava/lang/String;

    return-object v0
.end method

.method public getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public getKey()Lcom/squareup/orderentry/pages/OrderEntryPageKey;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->key:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->name:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public hasFavoritesPageId()Z
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->favoritesPageId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasGlyph()Z
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isVisibleInEditMode()Z
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->key:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    iget-boolean v0, v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->isFavoritesGrid:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OrderEntryPage{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->name:Ljavax/inject/Provider;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/OrderEntryPage;->key:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
