.class public abstract Lcom/squareup/orderentry/PaymentPadView;
.super Landroid/widget/FrameLayout;
.source "PaymentPadView.java"


# static fields
.field protected static final SLIDE_DURATION_MS:J = 0xfaL


# instance fields
.field protected editFrame:Landroid/view/View;

.field protected saleFrame:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadView;->inject()V

    return-void
.end method


# virtual methods
.method abstract animateToCartList()V
.end method

.method abstract animateToEditMode()V
.end method

.method abstract getPresenter()Lcom/squareup/orderentry/PaymentPadPresenter;
.end method

.method protected abstract inject()V
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 30
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 31
    sget v0, Lcom/squareup/orderentry/R$id;->sale_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadView;->saleFrame:Landroid/view/View;

    .line 32
    sget v0, Lcom/squareup/orderentry/R$id;->edit_frame:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadView;->editFrame:Landroid/view/View;

    .line 34
    sget v0, Lcom/squareup/orderentry/R$id;->done_editing:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/squareup/orderentry/PaymentPadView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadView$1;-><init>(Lcom/squareup/orderentry/PaymentPadView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showEditMode(Z)V
    .locals 1

    if-nez p1, :cond_0

    .line 54
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 55
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView;->editFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadView;->animateToEditMode()V

    :goto_0
    return-void
.end method

.method showSaleMode(Z)V
    .locals 1

    if-nez p1, :cond_0

    .line 63
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setX(F)V

    .line 64
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView;->saleFrame:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/View;->setY(F)V

    .line 65
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView;->saleFrame:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object p1, p0, Lcom/squareup/orderentry/PaymentPadView;->editFrame:Landroid/view/View;

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadView;->animateToCartList()V

    :goto_0
    return-void
.end method
