.class public final Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;
.super Ljava/lang/Object;
.source "PaymentPadTabletLandscapePresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cartMenuDropDownPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySellerCartBannerFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->busProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->cartMenuDropDownPresenterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->loyaltySellerCartBannerFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            ">;)",
            "Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;)Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v2, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->cartMenuDropDownPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;

    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v4, p0, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->loyaltySellerCartBannerFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->newInstance(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/badbus/BadBus;Lcom/squareup/ui/cart/menu/CartMenuDropDownPresenter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;)Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter_Factory;->get()Lcom/squareup/orderentry/PaymentPadTabletLandscapePresenter;

    move-result-object v0

    return-object v0
.end method
