.class Lcom/squareup/orderentry/ClearCardPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "ClearCardPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/ui/Showing;",
        "Lcom/squareup/flowlegacy/YesNo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic lambda$createDialog$0(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 23
    sget-object p1, Lcom/squareup/flowlegacy/YesNo;->YES:Lcom/squareup/flowlegacy/YesNo;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$1(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 25
    sget-object p1, Lcom/squareup/flowlegacy/YesNo;->NO:Lcom/squareup/flowlegacy/YesNo;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$createDialog$2(Lcom/squareup/mortar/PopupPresenter;Landroid/content/DialogInterface;)V
    .locals 0

    .line 26
    sget-object p1, Lcom/squareup/flowlegacy/YesNo;->NO:Lcom/squareup/flowlegacy/YesNo;

    invoke-virtual {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/Showing;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/ui/Showing;",
            "Lcom/squareup/flowlegacy/YesNo;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    .line 22
    new-instance p1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/squareup/orderentry/ClearCardPopup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p2, Lcom/squareup/orderentry/R$string;->clear:I

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$ClearCardPopup$LfNs7OZNBcOvWc-yZRV2ZthAars;

    invoke-direct {v0, p3}, Lcom/squareup/orderentry/-$$Lambda$ClearCardPopup$LfNs7OZNBcOvWc-yZRV2ZthAars;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 23
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$ClearCardPopup$vPoZjFCgFFenUzoyIlNTfa2x2ko;

    invoke-direct {v0, p3}, Lcom/squareup/orderentry/-$$Lambda$ClearCardPopup$vPoZjFCgFFenUzoyIlNTfa2x2ko;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 24
    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    new-instance p2, Lcom/squareup/orderentry/-$$Lambda$ClearCardPopup$oHWa5hZ3RIFhlf07qa0ICgr6YNc;

    invoke-direct {p2, p3}, Lcom/squareup/orderentry/-$$Lambda$ClearCardPopup$oHWa5hZ3RIFhlf07qa0ICgr6YNc;-><init>(Lcom/squareup/mortar/PopupPresenter;)V

    .line 26
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->are_you_sure:I

    .line 27
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget p2, Lcom/squareup/orderentry/R$string;->clear_card:I

    .line 28
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/ui/Showing;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/orderentry/ClearCardPopup;->createDialog(Lcom/squareup/ui/Showing;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
