.class Lcom/squareup/orderentry/FavoritePageView$2;
.super Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/FavoritePageView;->createEmptyView()Lcom/squareup/orderentry/EmptyTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/FavoritePageView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$2;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;-><init>(Lcom/squareup/orderentry/FavoritePageView$1;)V

    return-void
.end method


# virtual methods
.method public click(Landroid/view/View;)V
    .locals 2

    .line 331
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView$2;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    invoke-static {v0, p1}, Lcom/squareup/orderentry/FavoritePageView;->access$000(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;)Landroid/graphics/Point;

    move-result-object p1

    .line 332
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageView$2;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    iget-object v0, v0, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget p1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->onEmptyViewClicked(II)V

    return-void
.end method
