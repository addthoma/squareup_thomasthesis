.class public final Lcom/squareup/orderentry/category/ItemListView_MembersInjector;
.super Ljava/lang/Object;
.source "ItemListView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/category/ItemListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final itemPhotosProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/category/ItemListScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/category/ItemListScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->priceFormatterProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/category/ItemListScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/category/ItemListView;",
            ">;"
        }
    .end annotation

    .line 79
    new-instance v11, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static injectCurrencyCode(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method public static injectDurationFormatter(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/text/DurationFormatter;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method

.method public static injectItemPhotos(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-void
.end method

.method public static injectPercentageFormatter(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/orderentry/category/ItemListView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;)V"
        }
    .end annotation

    .line 114
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/orderentry/category/ItemListView;Ljava/lang/Object;)V
    .locals 0

    .line 97
    check-cast p1, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->presenter:Lcom/squareup/orderentry/category/ItemListScreen$Presenter;

    return-void
.end method

.method public static injectPriceFormatter(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static injectSettings(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static injectTileAppearanceSettings(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method

.method public static injectTransactionInteractionsLogger(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/log/cart/TransactionInteractionsLogger;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListView;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/category/ItemListView;)V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/category/ItemListView;Ljava/lang/Object;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->itemPhotosProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectItemPhotos(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->priceFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectPriceFormatter(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/quantity/PerUnitFormatter;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectPercentageFormatter(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/text/Formatter;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/DurationFormatter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectDurationFormatter(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/text/DurationFormatter;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectCurrencyCode(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectSettings(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectTileAppearanceSettings(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectTransactionInteractionsLogger(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/log/cart/TransactionInteractionsLogger;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectRes(Lcom/squareup/orderentry/category/ItemListView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/category/ItemListView_MembersInjector;->injectMembers(Lcom/squareup/orderentry/category/ItemListView;)V

    return-void
.end method
