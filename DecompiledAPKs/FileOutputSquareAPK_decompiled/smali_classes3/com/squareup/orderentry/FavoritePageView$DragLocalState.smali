.class Lcom/squareup/orderentry/FavoritePageView$DragLocalState;
.super Ljava/lang/Object;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritePageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DragLocalState"
.end annotation


# instance fields
.field private draggedView:Landroid/view/View;

.field private pageOfDrag:Lcom/squareup/orderentry/FavoritePageView;

.field final synthetic this$0:Lcom/squareup/orderentry/FavoritePageView;


# direct methods
.method private constructor <init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 0

    .line 1044
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1045
    iput-object p2, p0, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->draggedView:Landroid/view/View;

    .line 1046
    iput-object p3, p0, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->pageOfDrag:Lcom/squareup/orderentry/FavoritePageView;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/orderentry/FavoritePageView;Lcom/squareup/orderentry/FavoritePageView$1;)V
    .locals 0

    .line 1040
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;-><init>(Lcom/squareup/orderentry/FavoritePageView;Landroid/view/View;Lcom/squareup/orderentry/FavoritePageView;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/orderentry/FavoritePageView$DragLocalState;)Landroid/view/View;
    .locals 0

    .line 1040
    iget-object p0, p0, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->draggedView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/orderentry/FavoritePageView$DragLocalState;)Lcom/squareup/orderentry/FavoritePageView;
    .locals 0

    .line 1040
    iget-object p0, p0, Lcom/squareup/orderentry/FavoritePageView$DragLocalState;->pageOfDrag:Lcom/squareup/orderentry/FavoritePageView;

    return-object p0
.end method
