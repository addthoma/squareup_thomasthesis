.class public Lcom/squareup/orderentry/KeypadEntryCardView;
.super Lcom/squareup/register/widgets/KeypadEntryView;
.source "KeypadEntryCardView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field presenter:Lcom/squareup/orderentry/KeypadEntryCardPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/register/widgets/KeypadEntryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/orderentry/KeypadEntryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/KeypadEntryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/KeypadEntryScreen$Component;->inject(Lcom/squareup/orderentry/KeypadEntryCardView;)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 35
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onAttachedToWindow()V

    .line 36
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardView;->presenter:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardView;->presenter:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardView;->presenter:Lcom/squareup/orderentry/KeypadEntryCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/KeypadEntryCardPresenter;->dropView(Ljava/lang/Object;)V

    .line 41
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 30
    invoke-super {p0}, Lcom/squareup/register/widgets/KeypadEntryView;->onFinishInflate()V

    .line 31
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/orderentry/KeypadEntryCardView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
