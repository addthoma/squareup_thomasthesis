.class public Lcom/squareup/orderentry/ClockSkewScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "ClockSkewScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/orderentry/ClockSkewScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/ClockSkewScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/orderentry/ClockSkewScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/orderentry/ClockSkewScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/orderentry/ClockSkewScreen;

    invoke-direct {v0}, Lcom/squareup/orderentry/ClockSkewScreen;-><init>()V

    sput-object v0, Lcom/squareup/orderentry/ClockSkewScreen;->INSTANCE:Lcom/squareup/orderentry/ClockSkewScreen;

    .line 19
    sget-object v0, Lcom/squareup/orderentry/ClockSkewScreen;->INSTANCE:Lcom/squareup/orderentry/ClockSkewScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/orderentry/ClockSkewScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 22
    sget v0, Lcom/squareup/orderentry/R$layout;->clock_skew_view:I

    return v0
.end method
