.class public final enum Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;
.super Ljava/lang/Enum;
.source "KeypadEntryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

.field public static final enum MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

.field public static final enum PERCENTAGE:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 82
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    const/4 v1, 0x0

    const-string v2, "MONEY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    .line 83
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    const/4 v2, 0x1

    const-string v3, "PERCENTAGE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->PERCENTAGE:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    .line 81
    sget-object v3, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->MONEY:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->PERCENTAGE:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->$VALUES:[Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;
    .locals 1

    .line 81
    const-class v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->$VALUES:[Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    return-object v0
.end method
