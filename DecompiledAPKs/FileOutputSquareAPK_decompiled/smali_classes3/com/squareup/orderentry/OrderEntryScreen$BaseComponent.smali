.class public interface abstract Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;
.super Ljava/lang/Object;
.source "OrderEntryScreen.java"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListView$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseComponent"
.end annotation


# virtual methods
.method public abstract homeScreenEmvCardStatusProcessor()Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;
.end method

.method public abstract inject(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/CheckoutLibraryListView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/KeypadPanel;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/OrderEntryBadgeView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/OrderEntryDrawerButton;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/OrderEntryView;)V
.end method

.method public abstract inject(Lcom/squareup/orderentry/OrderEntryViewPager;)V
.end method

.method public abstract inject(Lcom/squareup/ui/permissions/EmployeeLockButton;)V
.end method

.method public abstract inject(Lcom/squareup/ui/seller/StartCashDrawerShiftLayout;)V
.end method
