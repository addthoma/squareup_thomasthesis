.class public final Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;
.super Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;
.source "FavoritesTileItemSelectionEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CatalogItemSelected"
.end annotation


# instance fields
.field private final catalogObjectId:Ljava/lang/String;

.field private final catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$ItemSelectionResult;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 32
    iput-object p2, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 44
    instance-of v0, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 48
    :cond_0
    check-cast p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;

    .line 49
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectId:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectId:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object p1, p1, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public getCatalogObjectId()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectId:Ljava/lang/String;

    return-object v0
.end method

.method public getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 54
    iget-object v1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectType:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents$CatalogItemSelected;->catalogObjectId:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
