.class public final Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;
.super Ljava/lang/Object;
.source "SwitchEmployeesEducationPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final employeesThatHaveSeenTooltipSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/StringSetLocalSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tooltipEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/StringSetLocalSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->tooltipEnabledProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p2, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->employeesThatHaveSeenTooltipSettingProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/StringSetLocalSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;"
        }
    .end annotation

    .line 59
    new-instance v7, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/StringSetLocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/settings/StringSetLocalSetting;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;"
        }
    .end annotation

    .line 67
    new-instance v7, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/StringSetLocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;
    .locals 7

    .line 50
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->tooltipEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->employeesThatHaveSeenTooltipSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/StringSetLocalSetting;

    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->badMaybeSquareDeviceCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    invoke-static/range {v1 .. v6}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->newInstance(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/StringSetLocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter_Factory;->get()Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;

    move-result-object v0

    return-object v0
.end method
