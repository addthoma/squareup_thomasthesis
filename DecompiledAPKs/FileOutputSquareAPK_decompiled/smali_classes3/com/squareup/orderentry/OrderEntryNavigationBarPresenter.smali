.class public Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;
.super Lmortar/ViewPresenter;
.source "OrderEntryNavigationBarPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 32
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-ne p2, v0, :cond_0

    .line 33
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->showEditActionbar(Z)V

    goto :goto_0

    .line 35
    :cond_0
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;->showSaleActionbar(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public synthetic lambda$onLoad$1$OrderEntryNavigationBarPresenter(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 30
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/orderentry/-$$Lambda$OrderEntryNavigationBarPresenter$6Uxdqwk6tu_-LtVX0DwUOiMKTvM;

    invoke-direct {v2, p1, v0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryNavigationBarPresenter$6Uxdqwk6tu_-LtVX0DwUOiMKTvM;-><init>(Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 31
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const/4 v1, 0x1

    .line 38
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 23
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;

    .line 28
    new-instance v0, Lcom/squareup/orderentry/-$$Lambda$OrderEntryNavigationBarPresenter$XyOjJZxJcGztkYc1lXy26tdUlV8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryNavigationBarPresenter$XyOjJZxJcGztkYc1lXy26tdUlV8;-><init>(Lcom/squareup/orderentry/OrderEntryNavigationBarPresenter;Lcom/squareup/orderentry/OrderEntryNavigationBarContainer;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
