.class public Lcom/squareup/orderentry/FavoritePageTileCreator;
.super Ljava/lang/Object;
.source "FavoritePageTileCreator.java"


# instance fields
.field private final cogs:Lcom/squareup/cogs/Cogs;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageTileCreator;->cogs:Lcom/squareup/cogs/Cogs;

    return-void
.end method

.method private updateCatalogMembership(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;ILjava/util/Collection;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Lcom/squareup/shared/catalog/models/CatalogObjectType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/graphics/Point;",
            "I",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/Collection<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 94
    invoke-interface {p1, p4}, Lcom/squareup/shared/catalog/Catalog$Local;->findPageTiles(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/PageTiles;

    move-result-object p1

    .line 95
    iget-object p1, p1, Lcom/squareup/shared/catalog/models/PageTiles;->tiles:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/Tile;

    .line 96
    iget-object v1, v0, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    invoke-virtual {v1, p6}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getColumnIndex(I)I

    move-result v1

    .line 97
    iget-object v2, v0, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    invoke-virtual {v2, p6}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->getRowIndex(I)I

    move-result v2

    .line 98
    invoke-virtual {p5, v1, v2}, Landroid/graphics/Point;->equals(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object p1, v0, Lcom/squareup/shared/catalog/models/Tile;->pageMembership:Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    invoke-interface {p7, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_1
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object p1

    .line 106
    invoke-virtual {p2, p3}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p2

    .line 107
    sget-object p3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    invoke-virtual {p3, p4}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object p3

    .line 108
    new-instance p4, Lcom/squareup/api/items/ItemPageMembership$Builder;

    invoke-direct {p4}, Lcom/squareup/api/items/ItemPageMembership$Builder;-><init>()V

    .line 109
    invoke-virtual {p4, p1}, Lcom/squareup/api/items/ItemPageMembership$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p4

    iget p7, p5, Landroid/graphics/Point;->x:I

    iget p5, p5, Landroid/graphics/Point;->y:I

    .line 110
    invoke-static {p7, p5, p6}, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;->coordinatesToPosition(III)I

    move-result p5

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/api/items/ItemPageMembership$Builder;->position(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p4

    .line 111
    invoke-virtual {p4, p2}, Lcom/squareup/api/items/ItemPageMembership$Builder;->item(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p2

    .line 112
    invoke-virtual {p2, p3}, Lcom/squareup/api/items/ItemPageMembership$Builder;->page(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/ItemPageMembership$Builder;

    move-result-object p2

    .line 113
    invoke-virtual {p2}, Lcom/squareup/api/items/ItemPageMembership$Builder;->build()Lcom/squareup/api/items/ItemPageMembership;

    move-result-object p2

    .line 115
    sget-object p3, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_PAGE_MEMBERSHIP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 116
    invoke-virtual {p3, p1, p2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemPageMembership;

    .line 117
    invoke-interface {p8, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public createTile(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V
    .locals 9

    .line 53
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageTileCreator;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v8, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$sE8AlVCbi6xXiyrRhRWSYJQGo7s;-><init>(Lcom/squareup/orderentry/FavoritePageTileCreator;Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V

    .line 86
    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object p1

    .line 53
    invoke-interface {v0, v8, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public createTile(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V
    .locals 9

    .line 37
    iget-object v0, p0, Lcom/squareup/orderentry/FavoritePageTileCreator;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v8, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$JiNThMcrpcudwa0LVa3-n-4uG1I;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/orderentry/-$$Lambda$FavoritePageTileCreator$JiNThMcrpcudwa0LVa3-n-4uG1I;-><init>(Lcom/squareup/orderentry/FavoritePageTileCreator;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;I)V

    .line 48
    invoke-static {v0}, Lcom/squareup/shared/catalog/CatalogTasks;->syncWhenFinished(Lcom/squareup/shared/catalog/Catalog;)Lcom/squareup/shared/catalog/CatalogCallback;

    move-result-object p1

    .line 37
    invoke-interface {v0, v8, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public synthetic lambda$createTile$0$FavoritePageTileCreator(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;ILcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 11

    .line 38
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 39
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    move-object/from16 v1, p6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v6, p5

    move-object v7, v9

    move-object v8, v10

    .line 41
    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/FavoritePageTileCreator;->updateCatalogMembership(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;ILjava/util/Collection;Ljava/util/Collection;)V

    move-object/from16 v0, p6

    .line 46
    invoke-interface {v0, v10, v9}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public synthetic lambda$createTile$1$FavoritePageTileCreator(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;ILcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Void;
    .locals 12

    move-object v0, p1

    .line 54
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 55
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 58
    invoke-interface/range {p6 .. p6}, Lcom/squareup/shared/catalog/Catalog$Local;->readAllPlaceholders()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v11, 0x0

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;

    .line 59
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;->getPlaceholderType()Lcom/squareup/api/items/Placeholder$PlaceholderType;

    move-result-object v3

    if-ne v3, v0, :cond_0

    goto :goto_0

    :cond_1
    move-object v2, v11

    :goto_0
    if-nez v2, :cond_2

    .line 66
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v1

    .line 71
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    new-instance v3, Lcom/squareup/api/items/Placeholder$Builder;

    invoke-direct {v3}, Lcom/squareup/api/items/Placeholder$Builder;-><init>()V

    invoke-virtual {v3, v1}, Lcom/squareup/api/items/Placeholder$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/Placeholder$Builder;

    move-result-object v3

    move-object v4, p2

    .line 72
    invoke-virtual {v3, p2}, Lcom/squareup/api/items/Placeholder$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/Placeholder$Builder;

    move-result-object v3

    .line 73
    invoke-virtual {v3, p1}, Lcom/squareup/api/items/Placeholder$Builder;->placeholder_type(Lcom/squareup/api/items/Placeholder$PlaceholderType;)Lcom/squareup/api/items/Placeholder$Builder;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/squareup/api/items/Placeholder$Builder;->build()Lcom/squareup/api/items/Placeholder;

    move-result-object v0

    .line 71
    invoke-virtual {v2, v1, v0}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->newObjectFromMessage(Ljava/lang/String;Lcom/squareup/wire/Message;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;

    .line 76
    invoke-interface {v9, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 78
    :cond_2
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogPlaceholder;->getId()Ljava/lang/String;

    move-result-object v3

    .line 79
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->PLACEHOLDER:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-object v0, p0

    move-object/from16 v1, p6

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object v7, v10

    move-object v8, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/orderentry/FavoritePageTileCreator;->updateCatalogMembership(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Point;ILjava/util/Collection;Ljava/util/Collection;)V

    move-object/from16 v0, p6

    .line 84
    invoke-interface {v0, v9, v10}, Lcom/squareup/shared/catalog/Catalog$Local;->write(Ljava/util/Collection;Ljava/util/Collection;)V

    return-object v11
.end method
