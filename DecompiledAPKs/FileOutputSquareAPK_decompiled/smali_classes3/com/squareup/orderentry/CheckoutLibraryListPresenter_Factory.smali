.class public final Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;
.super Ljava/lang/Object;
.source "CheckoutLibraryListPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/CheckoutLibraryListPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogLocalizerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final discountBundleFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final discountCursorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final durationFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemPhotosProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListAssistantProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final priceFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 103
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 104
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 105
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 106
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 107
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 108
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->itemPhotosProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 109
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 110
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 111
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->libraryStateProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 112
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->priceFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 113
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 114
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 115
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 116
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 117
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 118
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 119
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->discountCursorFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 120
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->discountBundleFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 121
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 122
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->configurationProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 123
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 124
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->catalogLocalizerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 125
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 126
    iput-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/DurationFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/shared/i18n/Localizer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 151
    new-instance v25, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v25
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/badbus/BadBus;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Device;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/orderentry/OrderEntryScreenState;Ljavax/inject/Provider;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/util/Clock;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/orderentry/CheckoutLibraryListAssistant;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/CheckoutLibraryListPresenter;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/librarylist/LibraryListStateManager;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;",
            "Lcom/squareup/orderentry/CheckoutLibraryListAssistant;",
            "Lcom/squareup/shared/i18n/Localizer;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")",
            "Lcom/squareup/orderentry/CheckoutLibraryListPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 166
    new-instance v25, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/badbus/BadBus;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Device;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/orderentry/OrderEntryScreenState;Ljavax/inject/Provider;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/util/Clock;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/orderentry/CheckoutLibraryListAssistant;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v25
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/CheckoutLibraryListPresenter;
    .locals 26

    move-object/from16 v0, p0

    .line 131
    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/main/CheckoutEntryHandler;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->itemPhotosProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v9, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->libraryStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/librarylist/LibraryListStateManager;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->priceFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->durationFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/text/DurationFormatter;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->discountCursorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->discountBundleFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->configurationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->catalogLocalizerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/shared/i18n/Localizer;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static/range {v2 .. v25}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/badbus/BadBus;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Device;Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/orderentry/OrderEntryScreenState;Ljavax/inject/Provider;Lcom/squareup/librarylist/LibraryListStateManager;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;Lcom/squareup/shared/catalog/utils/DiscountBundle$Factory;Lcom/squareup/util/Clock;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/orderentry/CheckoutLibraryListAssistant;Lcom/squareup/shared/i18n/Localizer;Lcom/squareup/analytics/Analytics;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/orderentry/CheckoutLibraryListPresenter_Factory;->get()Lcom/squareup/orderentry/CheckoutLibraryListPresenter;

    move-result-object v0

    return-object v0
.end method
