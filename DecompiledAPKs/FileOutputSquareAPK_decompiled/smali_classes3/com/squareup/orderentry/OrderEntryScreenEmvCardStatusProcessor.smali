.class public Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;
.super Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;
.source "OrderEntryScreenEmvCardStatusProcessor.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private lastScreen:Lcom/squareup/container/ContainerTreeKey;

.field private final nextScreen:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v7, p0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object/from16 v6, p8

    .line 64
    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;Lcom/squareup/ui/main/BadKeyboardHider;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentHudToaster;)V

    move-object v0, p1

    .line 66
    iput-object v0, v7, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->transaction:Lcom/squareup/payment/Transaction;

    move-object v0, p2

    .line 67
    iput-object v0, v7, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    move-object/from16 v0, p9

    .line 68
    iput-object v0, v7, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 69
    invoke-static {p4}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, v7, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->nextScreen:Lio/reactivex/Observable;

    return-void
.end method

.method private shouldOverrideEmvBehavior(Lcom/squareup/container/ContainerTreeKey;)Z
    .locals 1

    .line 129
    instance-of v0, p1, Lcom/squareup/orderentry/OrderEntryScreen;

    if-nez v0, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/cart/CartContainerScreen;

    if-nez v0, :cond_1

    instance-of p1, p1, Lcom/squareup/configure/item/ConfigureItemDetailScreen;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$OrderEntryScreenEmvCardStatusProcessor(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->lastScreen:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->shouldOverrideEmvBehavior(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 111
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->shouldOverrideEmvBehavior(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->addEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 115
    :cond_1
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->lastScreen:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->nextScreen:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreenEmvCardStatusProcessor$NGXG3VeBHiM5wp9D5-6cfCSmiec;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/-$$Lambda$OrderEntryScreenEmvCardStatusProcessor$NGXG3VeBHiM5wp9D5-6cfCSmiec;-><init>(Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->lastScreen:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0, v0}, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->shouldOverrideEmvBehavior(Lcom/squareup/container/ContainerTreeKey;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    :cond_0
    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 73
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasCard()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    :cond_0
    return-void

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenEmvCardStatusProcessor;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_2

    return-void

    .line 92
    :cond_2
    invoke-super {p0, p1}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-super {p0, p1}, Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;->processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    :cond_0
    return-void
.end method
