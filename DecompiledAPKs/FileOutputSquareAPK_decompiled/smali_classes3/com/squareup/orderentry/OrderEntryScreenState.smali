.class public Lcom/squareup/orderentry/OrderEntryScreenState;
.super Ljava/lang/Object;
.source "OrderEntryScreenState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;,
        Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;,
        Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;,
        Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;
    }
.end annotation


# static fields
.field public static final FAVORITES_GRID_EDIT_MODE_ENTERED:Ljava/lang/String; = "Favorites Grid Edit Mode Entered"


# instance fields
.field private final flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

.field private hasLibrarySearchText:Z

.field private final interactionMode:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;",
            ">;"
        }
    .end annotation
.end field

.field private final isInteractionModeAnimating:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final isLibrarySearchActive:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isShowingCashDrawerDialog:Z

.field private final isTicketSavedAlertPending:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private lastItemEntryMethod:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    .line 80
    invoke-static {v0}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    const/4 v0, 0x0

    .line 82
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating:Lio/reactivex/subjects/BehaviorSubject;

    .line 84
    invoke-static {v0}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive:Lio/reactivex/subjects/BehaviorSubject;

    .line 86
    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isTicketSavedAlertPending:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 88
    invoke-static {}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->buildEmptyAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    .line 96
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/OrderEntryScreenState;)Lio/reactivex/subjects/BehaviorSubject;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating:Lio/reactivex/subjects/BehaviorSubject;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/OrderEntryScreenState;)Lio/reactivex/subjects/BehaviorSubject;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive:Lio/reactivex/subjects/BehaviorSubject;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/orderentry/OrderEntryScreenState;)Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;
    .locals 0

    .line 75
    iget-object p0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    return-object p0
.end method

.method private setFlyByAnimationData(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/graphics/drawable/Drawable;)V
    .locals 7

    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 250
    invoke-virtual {p3, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 251
    iget-object v1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    .line 252
    invoke-virtual {p3}, Landroid/view/View;->getWidth()I

    move-result v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    .line 251
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->setData(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;[II)V

    return-void
.end method


# virtual methods
.method public clearFlyByAnimationData()V
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->clearData()V

    return-void
.end method

.method public clearPendingTicketSavedAlert()V
    .locals 2

    .line 363
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 364
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isTicketSavedAlertPending:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public endEditing()V
    .locals 2

    .line 407
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 409
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Attempting to end EDIT mode while in SALE mode, oops!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 412
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->SALE:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public enqueueFlyByAnimationForDiscount(Landroid/view/View;Z)V
    .locals 2

    if-nez p1, :cond_0

    .line 235
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_1

    .line 239
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forDiscount(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    goto :goto_0

    .line 241
    :cond_1
    invoke-static {p1}, Lcom/squareup/util/Views;->tryCopyToBitmapDrawable(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object p2

    .line 243
    :goto_0
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->NULL_FOR_DISCOUNT:Ljava/lang/String;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->NULL_FOR_DISCOUNT:Ljava/lang/String;

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/squareup/orderentry/OrderEntryScreenState;->setFlyByAnimationData(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void
.end method

.method public enqueueFlyByAnimationForItem(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 1

    if-nez p3, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    goto :goto_0

    .line 218
    :cond_0
    instance-of v0, p3, Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 219
    move-object v0, p3

    check-cast v0, Landroid/widget/ImageView;

    .line 220
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->setFlyByAnimationData(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 222
    :cond_1
    invoke-static {p3}, Lcom/squareup/util/Views;->tryCopyToBitmapDrawable(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 223
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->setFlyByAnimationData(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public getBundler()Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;
    .locals 1

    .line 416
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;-><init>(Lcom/squareup/orderentry/OrderEntryScreenState;)V

    return-object v0
.end method

.method public getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;
    .locals 1

    .line 261
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    return-object v0
.end method

.method public getInteractionMode()Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;
    .locals 1

    .line 112
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 113
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    return-object v0
.end method

.method public getLastItemEntryMethod()Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->lastItemEntryMethod:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    return-object v0
.end method

.method public hasLibrarySearchText()Z
    .locals 1

    .line 323
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->hasLibrarySearchText:Z

    return v0
.end method

.method public interactionMode()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public interactionModeIsAnimating()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isFlyByAnimationEnqueued()Z
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->flyByAnimationData:Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->hasUsableData()Z

    move-result v0

    return v0
.end method

.method public isInteractionModeAnimating()Z
    .locals 1

    .line 133
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 134
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isLibrarySearchActive()Z
    .locals 1

    .line 298
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 299
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isShowingCashDrawerDialog()Z
    .locals 1

    .line 372
    iget-boolean v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isShowingCashDrawerDialog:Z

    return v0
.end method

.method public librarySearchIsActive()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 282
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setHasLibrarySearchText(Z)V
    .locals 0

    .line 331
    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->hasLibrarySearchText:Z

    return-void
.end method

.method public setInteractionModeAnimating(Z)V
    .locals 1

    .line 144
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 145
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isInteractionModeAnimating:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setLastItemEntryMethod(Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;)V
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->lastItemEntryMethod:Lcom/squareup/orderentry/OrderEntryScreenState$ItemEntryMethod;

    return-void
.end method

.method public setLibrarySearchActive(Z)V
    .locals 1

    .line 315
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 316
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public setPendingTicketSavedAlert()V
    .locals 2

    .line 352
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 353
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isTicketSavedAlertPending:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public setShowingCashDrawerDialog(Z)V
    .locals 0

    .line 380
    iput-boolean p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isShowingCashDrawerDialog:Z

    return-void
.end method

.method public startEditing()V
    .locals 2

    .line 391
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 393
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Attempting to enter EDIT mode while in EDIT mode, oops!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->interactionMode:Lio/reactivex/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;->EDIT:Lcom/squareup/orderentry/OrderEntryScreenState$InteractionMode;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 397
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Favorites Grid Edit Mode Entered"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public ticketSavedAlertIsPending()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 342
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState;->isTicketSavedAlertPending:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
