.class public Lcom/squareup/hudtoaster/RealHudToaster;
.super Ljava/lang/Object;
.source "RealHudToaster.java"

# interfaces
.implements Lcom/squareup/hudtoaster/HudToaster;


# instance fields
.field private final application:Landroid/app/Application;

.field private final hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private toastRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/widget/Toast;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/app/Application;Lcom/squareup/util/ToastFactory;Lcom/squareup/hudtoaster/log/HudToasterLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/hudtoaster/RealHudToaster;->application:Landroid/app/Application;

    .line 48
    iput-object p2, p0, Lcom/squareup/hudtoaster/RealHudToaster;->toastFactory:Lcom/squareup/util/ToastFactory;

    .line 49
    iput-object p3, p0, Lcom/squareup/hudtoaster/RealHudToaster;->hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

    return-void
.end method

.method private setHudText(Landroid/view/View;IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 132
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 133
    sget v1, Lcom/squareup/widgets/pos/R$id;->hud_text:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz p4, :cond_0

    goto :goto_0

    .line 134
    :cond_0
    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p4

    :goto_0
    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    sget p2, Lcom/squareup/widgets/pos/R$id;->hud_message_text:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p3, :cond_1

    if-nez p5, :cond_1

    const/16 p2, 0x8

    .line 138
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_1
    if-eqz p5, :cond_2

    goto :goto_1

    .line 140
    :cond_2
    invoke-virtual {v0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p5

    :goto_1
    invoke-virtual {p1, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private toastHud(IIILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z
    .locals 9

    .line 119
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->application:Landroid/app/Application;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->hud:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/squareup/hudtoaster/RealHudToaster;->application:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 121
    sget v2, Lcom/squareup/cardreader/vector/icons/R$style;->Cardreader_Light:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 122
    iget-object v2, p0, Lcom/squareup/hudtoaster/RealHudToaster;->application:Landroid/app/Application;

    invoke-static {v2, p1, v1}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/Context;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 123
    sget v1, Lcom/squareup/widgets/pos/R$id;->hud_vector:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v2, 0x0

    .line 124
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v3, p0

    move-object v4, v0

    move v5, p2

    move v6, p3

    move-object v7, p4

    move-object v8, p5

    .line 126
    invoke-direct/range {v3 .. v8}, Lcom/squareup/hudtoaster/RealHudToaster;->setHudText(Landroid/view/View;IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {p0, v0, p6}, Lcom/squareup/hudtoaster/RealHudToaster;->doToast(Landroid/view/View;I)Z

    move-result p1

    return p1
.end method

.method private toastHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;IILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z
    .locals 9

    .line 104
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->application:Landroid/app/Application;

    sget v1, Lcom/squareup/widgets/pos/R$layout;->hud:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 105
    sget v1, Lcom/squareup/widgets/pos/R$id;->hud_glyph:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/glyph/SquareGlyphView;

    const/4 v2, 0x0

    .line 106
    invoke-virtual {v1, v2}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 107
    invoke-virtual {v1, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    move-object v3, p0

    move-object v4, v0

    move v5, p2

    move v6, p3

    move-object v7, p4

    move-object v8, p5

    .line 108
    invoke-direct/range {v3 .. v8}, Lcom/squareup/hudtoaster/RealHudToaster;->setHudText(Landroid/view/View;IILjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 109
    invoke-virtual {p0, v0, p6}, Lcom/squareup/hudtoaster/RealHudToaster;->doToast(Landroid/view/View;I)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->toastRef:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

    invoke-virtual {v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logCancelNullToastRef()V

    return-void

    .line 150
    :cond_0
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Toast;

    if-eqz v0, :cond_1

    .line 152
    iget-object v1, p0, Lcom/squareup/hudtoaster/RealHudToaster;->hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

    invoke-virtual {v1, v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logCancelToast(Landroid/widget/Toast;)V

    .line 153
    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    goto :goto_0

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

    invoke-virtual {v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logCancelNullToast()V

    .line 157
    :goto_0
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->toastRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    const/4 v0, 0x0

    .line 158
    iput-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->toastRef:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public doToast(Landroid/view/View;I)Z
    .locals 2

    .line 87
    invoke-virtual {p0}, Lcom/squareup/hudtoaster/RealHudToaster;->cancel()V

    .line 88
    iget-object v0, p0, Lcom/squareup/hudtoaster/RealHudToaster;->toastFactory:Lcom/squareup/util/ToastFactory;

    invoke-interface {v0}, Lcom/squareup/util/ToastFactory;->makeToast()Landroid/widget/Toast;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 90
    iget-object p1, p0, Lcom/squareup/hudtoaster/RealHudToaster;->hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

    invoke-virtual {p1}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logDisplayToastFailed()V

    return v1

    .line 93
    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 94
    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setDuration(I)V

    const/16 p1, 0x11

    .line 95
    invoke-virtual {v0, p1, v1, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 96
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 97
    new-instance p1, Ljava/lang/ref/WeakReference;

    invoke-direct {p1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/hudtoaster/RealHudToaster;->toastRef:Ljava/lang/ref/WeakReference;

    .line 98
    iget-object p1, p0, Lcom/squareup/hudtoaster/RealHudToaster;->hudToasterLogger:Lcom/squareup/hudtoaster/log/HudToasterLogger;

    invoke-virtual {p1, v0}, Lcom/squareup/hudtoaster/log/HudToasterLogger;->logDisplayToastSuccess(Landroid/widget/Toast;)V

    const/4 p1, 0x1

    return p1
.end method

.method public toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 78
    invoke-direct/range {v0 .. v6}, Lcom/squareup/hudtoaster/RealHudToaster;->toastHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;IILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1
.end method

.method public toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    .line 83
    invoke-direct/range {v0 .. v6}, Lcom/squareup/hudtoaster/RealHudToaster;->toastHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;IILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1
.end method

.method public toastShortHud(III)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    .line 63
    invoke-direct/range {v0 .. v6}, Lcom/squareup/hudtoaster/RealHudToaster;->toastHud(IIILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1
.end method

.method public toastShortHud(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v4, p2

    move-object v5, p3

    .line 68
    invoke-direct/range {v0 .. v6}, Lcom/squareup/hudtoaster/RealHudToaster;->toastHud(IIILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1
.end method

.method public toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z
    .locals 7

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    .line 58
    invoke-direct/range {v0 .. v6}, Lcom/squareup/hudtoaster/RealHudToaster;->toastHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;IILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1
.end method

.method public toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    .line 73
    invoke-direct/range {v0 .. v6}, Lcom/squareup/hudtoaster/RealHudToaster;->toastHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;IILjava/lang/CharSequence;Ljava/lang/CharSequence;I)Z

    move-result p1

    return p1
.end method

.method public toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z
    .locals 2

    .line 53
    invoke-interface {p1}, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;->glyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    invoke-interface {p1}, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;->titleId()I

    move-result v1

    invoke-interface {p1}, Lcom/squareup/hudtoaster/HudToaster$ToastBundle;->messageId()I

    move-result p1

    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/hudtoaster/RealHudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;II)Z

    move-result p1

    return p1
.end method
