.class Lcom/squareup/navigation/ScreenNavigationLogger$Worker;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "ScreenNavigationLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/navigation/ScreenNavigationLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation


# instance fields
.field private final entryScreeName:Ljava/lang/String;

.field private final exitScreenName:Ljava/lang/String;

.field private justRestoredState:Z

.field private lastScreenName:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/navigation/ScreenNavigationLogger;


# direct methods
.method constructor <init>(Lcom/squareup/navigation/ScreenNavigationLogger;Ljava/lang/String;)V
    .locals 1

    .line 59
    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->this$0:Lcom/squareup/navigation/ScreenNavigationLogger;

    .line 60
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-class v0, Lcom/squareup/navigation/ScreenNavigationLogger;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    .line 61
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " Entry Point"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->entryScreeName:Ljava/lang/String;

    .line 62
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " Exit Point"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->exitScreenName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/navigation/ScreenNavigationLogger$Worker;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->logNavigatedTo(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private logEndOfNavigation()V
    .locals 4

    .line 150
    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    const-string v1, "lastScreenName"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    .line 152
    iget-object v1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->exitScreenName:Ljava/lang/String;

    .line 153
    invoke-static {v0, v1}, Lcom/squareup/analytics/event/ViewEvent;->createNavigationEvent(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/analytics/event/ViewEvent;

    move-result-object v2

    .line 154
    new-instance v3, Lcom/squareup/navigation/ScreenNavigationLogger$LegacyViewAppearedEvent;

    invoke-direct {v3, v0, v1}, Lcom/squareup/navigation/ScreenNavigationLogger$LegacyViewAppearedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->this$0:Lcom/squareup/navigation/ScreenNavigationLogger;

    invoke-static {v0}, Lcom/squareup/navigation/ScreenNavigationLogger;->access$000(Lcom/squareup/navigation/ScreenNavigationLogger;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->this$0:Lcom/squareup/navigation/ScreenNavigationLogger;

    invoke-static {v0}, Lcom/squareup/navigation/ScreenNavigationLogger;->access$000(Lcom/squareup/navigation/ScreenNavigationLogger;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private logNavigatedTo(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    const-string v1, "lastScreenName"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 106
    instance-of v0, p1, Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 107
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 108
    invoke-virtual {v0}, Lcom/squareup/ui/main/RegisterTreeKey;->getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;

    move-result-object v0

    const-string v2, "analyticsName"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/RegisterViewName;

    .line 109
    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    if-eq v0, v2, :cond_2

    .line 110
    iget-object v1, v0, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    goto :goto_0

    .line 112
    :cond_0
    instance-of v0, p1, Lcom/squareup/container/WorkflowTreeKey;

    if-eqz v0, :cond_2

    .line 113
    move-object v0, p1

    check-cast v0, Lcom/squareup/container/WorkflowTreeKey;

    .line 114
    invoke-virtual {v0}, Lcom/squareup/container/WorkflowTreeKey;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 115
    invoke-virtual {v0}, Lcom/squareup/container/WorkflowTreeKey;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 117
    :cond_1
    iget-object v0, v0, Lcom/squareup/container/WorkflowTreeKey;->screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/Screen$Key;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    :goto_0
    if-nez v1, :cond_3

    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unmapped: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 124
    :cond_3
    iget-boolean p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->justRestoredState:Z

    if-eqz p1, :cond_5

    const/4 p1, 0x0

    .line 125
    iput-boolean p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->justRestoredState:Z

    .line 126
    iget-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    return-void

    .line 132
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " SAVED IN STATE"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    .line 136
    :cond_5
    iget-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/squareup/analytics/event/ViewEvent;->createNavigationEvent(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/analytics/event/ViewEvent;

    move-result-object p1

    .line 137
    new-instance v0, Lcom/squareup/navigation/ScreenNavigationLogger$LegacyViewAppearedEvent;

    iget-object v2, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    invoke-direct {v0, v2, v1}, Lcom/squareup/navigation/ScreenNavigationLogger$LegacyViewAppearedEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v2, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->this$0:Lcom/squareup/navigation/ScreenNavigationLogger;

    invoke-static {v2}, Lcom/squareup/navigation/ScreenNavigationLogger;->access$000(Lcom/squareup/navigation/ScreenNavigationLogger;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 141
    iget-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->this$0:Lcom/squareup/navigation/ScreenNavigationLogger;

    invoke-static {p1}, Lcom/squareup/navigation/ScreenNavigationLogger;->access$000(Lcom/squareup/navigation/ScreenNavigationLogger;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 142
    iput-object v1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onExitScope()V
    .locals 1

    .line 81
    invoke-direct {p0}, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->logEndOfNavigation()V

    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    .line 69
    iget-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->entryScreeName:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v0, "LAST_SCREEN_NAME"

    .line 71
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    const/4 p1, 0x1

    .line 72
    iput-boolean p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->justRestoredState:Z

    :goto_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->lastScreenName:Ljava/lang/String;

    const-string v1, "LAST_SCREEN_NAME"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
