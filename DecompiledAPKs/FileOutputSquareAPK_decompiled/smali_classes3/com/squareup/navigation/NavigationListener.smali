.class public Lcom/squareup/navigation/NavigationListener;
.super Ljava/lang/Object;
.source "NavigationListener.java"


# instance fields
.field private final ohSnap:Lcom/squareup/log/OhSnapLogger;

.field private final screenChangeLedgerManager:Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;


# direct methods
.method public constructor <init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/navigation/NavigationListener;->ohSnap:Lcom/squareup/log/OhSnapLogger;

    .line 24
    iput-object p2, p0, Lcom/squareup/navigation/NavigationListener;->screenChangeLedgerManager:Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;

    return-void
.end method


# virtual methods
.method public onDispatch(Lflow/Traversal;)V
    .locals 7

    .line 32
    iget-object v0, p0, Lcom/squareup/navigation/NavigationListener;->ohSnap:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SHOW_SCREEN:Lcom/squareup/log/OhSnapLogger$EventType;

    const/4 v2, 0x2

    new-array v3, v2, [Ljava/lang/Object;

    iget-object v4, p1, Lflow/Traversal;->origin:Lflow/History;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p1, Lflow/Traversal;->direction:Lflow/Direction;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-string v4, "Out: %s, %s"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/navigation/NavigationListener;->ohSnap:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->SHOW_SCREEN:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " In: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 35
    iget-object v0, p1, Lflow/Traversal;->destination:Lflow/History;

    invoke-virtual {v0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 36
    iget-object p1, p1, Lflow/Traversal;->origin:Lflow/History;

    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/ContainerTreeKey;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v5

    .line 38
    invoke-virtual {p1}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v6

    invoke-virtual {v0}, Lcom/squareup/container/ContainerTreeKey;->getName()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "%s: Out with %s, in with %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 39
    iget-object v0, p0, Lcom/squareup/navigation/NavigationListener;->screenChangeLedgerManager:Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;

    invoke-interface {v0, p1}, Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;->logShowScreen(Ljava/lang/String;)V

    return-void
.end method
