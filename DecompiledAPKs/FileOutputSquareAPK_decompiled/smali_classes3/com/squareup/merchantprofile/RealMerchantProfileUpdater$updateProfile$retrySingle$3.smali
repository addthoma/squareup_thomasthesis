.class final Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;
.super Ljava/lang/Object;
.source "RealMerchantProfileUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/merchantprofile/RealMerchantProfileUpdater;->updateProfile(Lcom/squareup/merchantprofile/MerchantProfileSnapshot;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lio/reactivex/disposables/Disposable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lio/reactivex/disposables/Disposable;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;

    invoke-direct {v0}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;-><init>()V

    sput-object v0, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;->INSTANCE:Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lio/reactivex/disposables/Disposable;)V
    .locals 1

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "[MerchantProfileUpdater] Merchant profile update canceled."

    .line 183
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 61
    check-cast p1, Lio/reactivex/disposables/Disposable;

    invoke-virtual {p0, p1}, Lcom/squareup/merchantprofile/RealMerchantProfileUpdater$updateProfile$retrySingle$3;->accept(Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
