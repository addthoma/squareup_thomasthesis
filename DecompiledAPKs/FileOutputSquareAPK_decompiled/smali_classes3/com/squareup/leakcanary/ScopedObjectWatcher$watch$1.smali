.class public final Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;
.super Ljava/lang/Object;
.source "ScopedObjectWatcher.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/leakcanary/ScopedObjectWatcher;->watch(Lmortar/MortarScope;Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0008\u0010\u0006\u001a\u00020\u0003H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/leakcanary/ScopedObjectWatcher$watch$1",
        "Lmortar/Scoped;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $ref:Ljava/lang/Object;

.field final synthetic $scope:Lmortar/MortarScope;

.field final synthetic this$0:Lcom/squareup/leakcanary/ScopedObjectWatcher;


# direct methods
.method constructor <init>(Lcom/squareup/leakcanary/ScopedObjectWatcher;Ljava/lang/Object;Lmortar/MortarScope;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lmortar/MortarScope;",
            ")V"
        }
    .end annotation

    .line 15
    iput-object p1, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->this$0:Lcom/squareup/leakcanary/ScopedObjectWatcher;

    iput-object p2, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->$ref:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->$scope:Lmortar/MortarScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 4

    .line 19
    iget-object v0, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->this$0:Lcom/squareup/leakcanary/ScopedObjectWatcher;

    invoke-static {v0}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->access$getObjectWatcher$p(Lcom/squareup/leakcanary/ScopedObjectWatcher;)Lleakcanary/ObjectWatcher;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->$ref:Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->$ref:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/leakcanary/ScopedObjectWatcher$watch$1;->$scope:Lmortar/MortarScope;

    invoke-virtual {v3}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lleakcanary/ObjectWatcher;->watch(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
