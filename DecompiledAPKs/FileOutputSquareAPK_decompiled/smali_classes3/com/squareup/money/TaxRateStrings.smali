.class public final Lcom/squareup/money/TaxRateStrings;
.super Ljava/lang/Object;
.source "TaxRateStrings.java"


# static fields
.field public static final PRECISION:I = 0x5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static format(Lcom/squareup/util/Percentage;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x5

    .line 34
    invoke-static {p0, v0}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static format(Lcom/squareup/util/Percentage;I)Ljava/lang/String;
    .locals 1

    .line 53
    invoke-static {}, Lcom/squareup/currency_db/NumberFormats;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;ILjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static format(Lcom/squareup/util/Percentage;ILjava/text/NumberFormat;)Ljava/lang/String;
    .locals 1

    .line 58
    sget-object v0, Lcom/squareup/util/Percentage;->ONE_HUNDRED:Lcom/squareup/util/Percentage;

    invoke-virtual {p0, v0}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v0

    if-lez v0, :cond_0

    const-string p0, "0"

    return-object p0

    .line 62
    :cond_0
    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-virtual {p0, v0}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v0

    if-gez v0, :cond_1

    const-string p0, ""

    return-object p0

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/util/Percentage;->bigDecimalValue()Ljava/math/BigDecimal;

    move-result-object p0

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p0

    .line 68
    invoke-virtual {p2, p1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 69
    invoke-virtual {p2, p0}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static format(Lcom/squareup/util/Percentage;Ljava/text/NumberFormat;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x5

    .line 39
    invoke-static {p0, v0, p1}, Lcom/squareup/money/TaxRateStrings;->format(Lcom/squareup/util/Percentage;ILjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parse(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;
    .locals 0

    .line 82
    invoke-static {p0, p1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method
