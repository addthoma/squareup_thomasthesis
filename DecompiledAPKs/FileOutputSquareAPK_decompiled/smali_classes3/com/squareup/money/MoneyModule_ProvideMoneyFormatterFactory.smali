.class public final Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;
.super Ljava/lang/Object;
.source "MoneyModule_ProvideMoneyFormatterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/text/Formatter<",
        "Lcom/squareup/protos/common/Money;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyLocaleFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->moneyLocaleFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ">;)",
            "Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/money/MoneyLocaleFormatter;",
            ")",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-static {p0, p1}, Lcom/squareup/money/MoneyModule;->provideMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/text/Formatter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/text/Formatter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->moneyLocaleFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/money/MoneyLocaleFormatter;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->provideMoneyFormatter(Ljavax/inject/Provider;Lcom/squareup/money/MoneyLocaleFormatter;)Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/money/MoneyModule_ProvideMoneyFormatterFactory;->get()Lcom/squareup/text/Formatter;

    move-result-object v0

    return-object v0
.end method
