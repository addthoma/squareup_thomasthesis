.class public final Lcom/squareup/money/WholeUnitMoneyHelper;
.super Ljava/lang/Object;
.source "WholeUnitMoneyHelper.java"

# interfaces
.implements Lcom/squareup/money/MoneyExtractor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyLocaleDigitsKeyListener:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/money/ForMoney;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/text/method/DigitsKeyListener;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->moneyLocaleDigitsKeyListener:Ljavax/inject/Provider;

    .line 56
    iput-object p2, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    .line 57
    iput-object p3, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;J)V
    .locals 7

    .line 61
    iget-object v0, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->moneyLocaleDigitsKeyListener:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/method/KeyListener;

    invoke-interface {p1, v0}, Lcom/squareup/text/HasSelectableText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 62
    new-instance v0, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;

    iget-object v2, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    move-object v1, v0

    move-wide v4, p3

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/money/WholeUnitMoneyHelper$MoneyScrubber;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    .line 64
    new-instance p2, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-direct {p2, v0, p1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    .line 65
    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 66
    invoke-interface {p1, p2}, Lcom/squareup/text/HasSelectableText;->addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    return-void
.end method

.method public extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;
    .locals 4

    .line 80
    invoke-static {p1}, Lcom/squareup/util/Strings;->removeNonDigits(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 81
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 83
    :cond_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p1

    int-to-long v2, p1

    mul-long v0, v0, v2

    .line 84
    iget-object p1, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public setHintToWholeMoney(Landroid/widget/TextView;J)V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0}, Lcom/squareup/currency_db/Currencies;->getSubunitsPerUnit(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result v0

    int-to-long v0, v0

    mul-long p2, p2, v0

    .line 72
    iget-object v0, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->shortMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/money/WholeUnitMoneyHelper;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p2, p3, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method
