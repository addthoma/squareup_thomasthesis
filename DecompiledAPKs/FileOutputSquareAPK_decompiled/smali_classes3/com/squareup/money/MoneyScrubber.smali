.class public Lcom/squareup/money/MoneyScrubber;
.super Lcom/squareup/money/WholeUnitAmountScrubber;
.source "MoneyScrubber.java"


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;J",
            "Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;",
            ")V"
        }
    .end annotation

    .line 14
    invoke-direct {p0, p3, p4, p5}, Lcom/squareup/money/WholeUnitAmountScrubber;-><init>(JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    .line 15
    iput-object p1, p0, Lcom/squareup/money/MoneyScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 16
    iput-object p2, p0, Lcom/squareup/money/MoneyScrubber;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method


# virtual methods
.method protected formatAmount(J)Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/money/MoneyScrubber;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, p2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 21
    iget-object p2, p0, Lcom/squareup/money/MoneyScrubber;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
