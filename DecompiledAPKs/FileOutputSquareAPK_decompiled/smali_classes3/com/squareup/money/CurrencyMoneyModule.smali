.class public Lcom/squareup/money/CurrencyMoneyModule;
.super Ljava/lang/Object;
.source "CurrencyMoneyModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$provideMoneyDigitsKeyListenerFactory$0(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;
    .locals 1

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    invoke-static {p0, p1}, Lcom/squareup/currency_db/NumberFormats;->getAllowedDigits(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 34
    invoke-static {p0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object p0

    return-object p0
.end method

.method static provideMoneyDigitsKeyListener(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Landroid/text/method/DigitsKeyListener;
    .locals 0
    .annotation runtime Lcom/squareup/money/ForMoney;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 27
    invoke-static {p1, p0}, Lcom/squareup/currency_db/NumberFormats;->getAllowedDigits(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object p0

    .line 28
    invoke-static {p0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object p0

    return-object p0
.end method

.method static provideMoneyDigitsKeyListenerFactory(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;)Lcom/squareup/money/MoneyDigitsKeyListenerFactory;
    .locals 1
    .annotation runtime Lcom/squareup/money/ForMoney;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;

    invoke-direct {v0, p1, p0}, Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;-><init>(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method static provideMoneyScrubber(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;)Lcom/squareup/text/SelectableTextScrubber;
    .locals 7
    .annotation runtime Lcom/squareup/money/ForMoney;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/text/SelectableTextScrubber;"
        }
    .end annotation

    .line 21
    new-instance v6, Lcom/squareup/money/MoneyScrubber;

    sget-wide v3, Lcom/squareup/money/MaxMoneyScrubber;->MAX_MONEY:J

    sget-object v5, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/money/MoneyScrubber;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;JLcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)V

    return-object v6
.end method
