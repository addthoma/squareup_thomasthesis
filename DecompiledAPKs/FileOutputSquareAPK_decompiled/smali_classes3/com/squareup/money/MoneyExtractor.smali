.class public interface abstract Lcom/squareup/money/MoneyExtractor;
.super Ljava/lang/Object;
.source "MoneyExtractor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/money/MoneyExtractor;",
        "",
        "extractMoney",
        "Lcom/squareup/protos/common/Money;",
        "text",
        "",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;
.end method
