.class final Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;
.super Ljava/lang/Object;
.source "MoneyLocaleFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/money/MoneyLocaleFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DecimalFormatCache"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "locale",
        "Ljava/util/Locale;",
        "decimalFormat",
        "Ljava/text/DecimalFormat;",
        "(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Ljava/text/DecimalFormat;)V",
        "getCurrencyCode",
        "()Lcom/squareup/protos/common/CurrencyCode;",
        "getDecimalFormat",
        "()Ljava/text/DecimalFormat;",
        "getLocale",
        "()Ljava/util/Locale;",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final decimalFormat:Ljava/text/DecimalFormat;

.field private final locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/Locale;Ljava/text/DecimalFormat;)V
    .locals 1

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "decimalFormat"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->locale:Ljava/util/Locale;

    iput-object p3, p0, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->decimalFormat:Ljava/text/DecimalFormat;

    return-void
.end method


# virtual methods
.method public final getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public final getDecimalFormat()Ljava/text/DecimalFormat;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->decimalFormat:Ljava/text/DecimalFormat;

    return-object v0
.end method

.method public final getLocale()Ljava/util/Locale;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/money/MoneyLocaleFormatter$DecimalFormatCache;->locale:Ljava/util/Locale;

    return-object v0
.end method
