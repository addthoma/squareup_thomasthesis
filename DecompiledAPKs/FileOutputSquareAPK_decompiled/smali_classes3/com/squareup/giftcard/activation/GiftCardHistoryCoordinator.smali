.class public final Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "GiftCardHistoryCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\nH\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "locale",
        "Ljava/util/Locale;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "errorsBar",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "(Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/ui/ErrorsBarPresenter;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "getDateString",
        "",
        "date",
        "Lcom/squareup/protos/common/time/DateTime;",
        "getMoneyString",
        "money",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormatter:Ljava/text/DateFormat;

.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final locale:Ljava/util/Locale;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .param p5    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/LongForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
            "Lcom/squareup/util/Res;",
            "Ljava/util/Locale;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->locale:Ljava/util/Locale;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->dateFormatter:Ljava/text/DateFormat;

    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method public static final synthetic access$getDateString(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->getDateString(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-object p0
.end method

.method public static final synthetic access$getMoneyFormatter$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getMoneyString(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;

    return-object p0
.end method

.method private final getDateString(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->dateFormatter:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->locale:Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "dateFormatter.format(asDate(date, locale))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getMoneyString(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 5

    .line 127
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 128
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 129
    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-lez p1, :cond_0

    .line 130
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x2b

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 9

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 45
    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->runner:Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;->giftCardName()Lrx/Observable;

    move-result-object v1

    .line 46
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$1;

    invoke-direct {v2, v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$1;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    const-string v2, "runner.giftCardName()\n  \u2026AndText(BACK_ARROW, it) }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {v1, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 48
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 50
    sget v0, Lcom/squareup/giftcard/activation/R$id;->gift_card_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 51
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gc_current_balance_value:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Landroid/widget/TextView;

    .line 52
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gift_card_history_contents:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/LinearLayout;

    .line 53
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gc_history_event_container:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Landroid/widget/LinearLayout;

    .line 55
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gc_add_value_button:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 57
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$3;

    invoke-direct {v2, p0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 58
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$4;

    invoke-direct {v2, p0, v1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$4;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Landroid/widget/Button;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 63
    new-instance v2, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$5;

    invoke-direct {v2, p0, v1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$5;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Landroid/widget/Button;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 68
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gift_card_clear_balance_button:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Landroid/widget/Button;

    .line 69
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$6;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$6;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$7;

    invoke-direct {v1, p0, v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$7;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Landroid/widget/ProgressBar;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 80
    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;

    move-object v2, v0

    move-object v3, p0

    move-object v7, p1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Landroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/Button;Landroid/view/View;Landroid/widget/LinearLayout;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
