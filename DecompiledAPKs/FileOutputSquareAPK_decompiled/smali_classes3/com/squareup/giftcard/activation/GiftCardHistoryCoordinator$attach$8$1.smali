.class final Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;
.super Ljava/lang/Object;
.source "GiftCardHistoryCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;

    invoke-direct {v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;-><init>()V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;->call(Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;)Z
    .locals 1

    .line 82
    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinatorKt;->getEMPTY_GIFT_CARD_HISTORY()Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method
