.class public abstract Lcom/squareup/giftcard/activation/GiftCardLoadingScopeModule;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0005H!\u00a2\u0006\u0002\u0008\u0006J\u0015\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000bJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000eJ\u0015\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u0011J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u0014J\u0015\u0010\u0015\u001a\u00020\u00162\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeModule;",
        "",
        "()V",
        "giftCardActivationFlow",
        "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
        "Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;",
        "giftCardActivationFlow$giftcard_activation_release",
        "giftCardBalanceScreen",
        "Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;",
        "scopeRunner",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;",
        "giftCardBalanceScreen$giftcard_activation_release",
        "giftCardChooseType",
        "Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;",
        "giftCardChooseType$giftcard_activation_release",
        "giftCardClearBalanceScreen",
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;",
        "giftCardClearBalanceScreen$giftcard_activation_release",
        "giftCardHistoryScreen",
        "Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;",
        "giftCardHistoryScreen$giftcard_activation_release",
        "giftCardLookupScreen",
        "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
        "giftCardLookupScreen$giftcard_activation_release",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract giftCardActivationFlow$giftcard_activation_release(Lcom/squareup/giftcard/activation/RealGiftCardActivationFlow;)Lcom/squareup/giftcardactivation/GiftCardActivationFlow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract giftCardBalanceScreen$giftcard_activation_release(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract giftCardChooseType$giftcard_activation_release(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardChooseTypeScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract giftCardClearBalanceScreen$giftcard_activation_release(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract giftCardHistoryScreen$giftcard_activation_release(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract giftCardLookupScreen$giftcard_activation_release(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
