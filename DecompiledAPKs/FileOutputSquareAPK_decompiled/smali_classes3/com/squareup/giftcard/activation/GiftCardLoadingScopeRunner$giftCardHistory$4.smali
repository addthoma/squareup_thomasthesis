.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->giftCardHistory()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "it",
        "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;

    invoke-direct {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;-><init>()V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)Lcom/squareup/protos/client/giftcards/GiftCard;
    .locals 0

    if-eqz p1, :cond_0

    .line 521
    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$giftCardHistory$4;->call(Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;)Lcom/squareup/protos/client/giftcards/GiftCard;

    move-result-object p1

    return-object p1
.end method
