.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1$1;
.super Ljava/lang/Object;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0003*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1$1;->call(Lkotlin/Unit;)V

    return-void
.end method

.method public final call(Lkotlin/Unit;)V
    .locals 3

    .line 160
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;->$amount:Lcom/squareup/protos/common/Money;

    const-string v1, "amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;

    iget-object v1, v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2$1;->$giftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    const-string v2, "giftCard"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0, v1}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$addGiftCardToTransaction(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/GiftCard;)V

    return-void
.end method
