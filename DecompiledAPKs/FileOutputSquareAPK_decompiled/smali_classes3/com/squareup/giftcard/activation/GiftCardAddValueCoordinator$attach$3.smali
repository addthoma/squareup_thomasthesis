.class final Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardAddValueCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field final synthetic $contents:Landroid/widget/LinearLayout;

.field final synthetic $customAmountButton:Lcom/squareup/marketfont/MarketButton;

.field final synthetic $customAmountField:Lcom/squareup/widgets/OnScreenRectangleEditText;

.field final synthetic $customLayoutContainer:Landroid/widget/FrameLayout;

.field final synthetic $invalidCardView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field final synthetic $presetAmountContainer:Landroid/widget/LinearLayout;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;Landroid/widget/LinearLayout;Landroid/view/View;Lcom/squareup/widgets/OnScreenRectangleEditText;Lcom/squareup/marketfont/MarketButton;Landroid/widget/FrameLayout;Landroid/widget/LinearLayout;Lcom/squareup/marin/widgets/MarinGlyphMessage;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$presetAmountContainer:Landroid/widget/LinearLayout;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$view:Landroid/view/View;

    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customAmountField:Lcom/squareup/widgets/OnScreenRectangleEditText;

    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customAmountButton:Lcom/squareup/marketfont/MarketButton;

    iput-object p7, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$customLayoutContainer:Landroid/widget/FrameLayout;

    iput-object p8, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$contents:Landroid/widget/LinearLayout;

    iput-object p9, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->$invalidCardView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 2

    .line 110
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;->this$0:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator;)Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/giftcard/activation/GiftCardAddValueScreen$Controller;->registerGiftCard()Lrx/Observable;

    move-result-object v0

    .line 111
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 112
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardAddValueCoordinator$attach$3;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "controller.registerGiftC\u2026            }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
