.class public final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final controllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->controllerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->errorsBarProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)",
            "Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ljavax/inject/Provider;)Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)",
            "Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;"
        }
    .end annotation

    .line 63
    new-instance v7, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ljavax/inject/Provider;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->controllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->errorsBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->giftCardsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/giftcard/GiftCards;

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v6, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v6}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->newInstance(Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ljavax/inject/Provider;)Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator_Factory;->get()Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    move-result-object v0

    return-object v0
.end method
