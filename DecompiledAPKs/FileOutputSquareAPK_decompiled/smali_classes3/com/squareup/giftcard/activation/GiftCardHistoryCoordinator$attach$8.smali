.class final Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;
.super Lkotlin/jvm/internal/Lambda;
.source "GiftCardHistoryCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lrx/Subscription;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Subscription;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $balanceValue:Landroid/widget/TextView;

.field final synthetic $clearBalanceButton:Landroid/widget/Button;

.field final synthetic $historyContainer:Landroid/widget/LinearLayout;

.field final synthetic $screenContent:Landroid/widget/LinearLayout;

.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;Landroid/widget/LinearLayout;Landroid/widget/TextView;Landroid/widget/Button;Landroid/view/View;Landroid/widget/LinearLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$historyContainer:Landroid/widget/LinearLayout;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$balanceValue:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$clearBalanceButton:Landroid/widget/Button;

    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$view:Landroid/view/View;

    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->$screenContent:Landroid/widget/LinearLayout;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->invoke()Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lrx/Subscription;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;->this$0:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;->access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator;)Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/giftcard/activation/GiftCardHistoryScreen$Runner;->giftCardHistory()Lrx/Observable;

    move-result-object v0

    .line 82
    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;

    invoke-direct {v1, p0}, Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardHistoryCoordinator$attach$8;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.giftCardHistory()\u2026eOrGone(true)\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
