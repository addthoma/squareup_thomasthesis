.class public Lcom/squareup/giftcard/GiftCardServiceHelper;
.super Ljava/lang/Object;
.source "GiftCardServiceHelper.java"


# static fields
.field private static final JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

.field private static final MAX_RESULTS:I = 0x64


# instance fields
.field private final cardConverter:Lcom/squareup/payment/CardConverter;

.field private final giftCardService:Lcom/squareup/server/payment/GiftCardService;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "image/jpeg"

    .line 51
    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/giftcard/GiftCardServiceHelper;->JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

    return-void
.end method

.method constructor <init>(Lcom/squareup/server/payment/GiftCardService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/CardConverter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    .line 62
    iput-object p2, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 63
    iput-object p3, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->cardConverter:Lcom/squareup/payment/CardConverter;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 168
    iget-object p0, p0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;->gift_card:Lcom/squareup/protos/client/giftcards/GiftCard;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$registerEGiftCard$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 168
    sget-object v0, Lcom/squareup/giftcard/-$$Lambda$GiftCardServiceHelper$Hj0P_BXU2Zzop5CvRbK57b1vF8Q;->INSTANCE:Lcom/squareup/giftcard/-$$Lambda$GiftCardServiceHelper$Hj0P_BXU2Zzop5CvRbK57b1vF8Q;

    invoke-static {p0, v0}, Lcom/squareup/receiving/StandardReceiver;->rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public checkBalanceByServerToken(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenResponse;",
            ">;>;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 109
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;

    move-result-object v0

    .line 110
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->gift_card_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;

    move-result-object p1

    .line 111
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;

    move-result-object p1

    .line 113
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    invoke-interface {v0, p1}, Lcom/squareup/server/payment/GiftCardService;->checkBalanceByServerToken(Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public clearBalance(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;>;"
        }
    .end annotation

    .line 83
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;-><init>()V

    .line 84
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 85
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    .line 86
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->gift_card_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->reason(Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;

    move-result-object p1

    .line 90
    iget-object p2, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    invoke-interface {p2, p1}, Lcom/squareup/server/payment/GiftCardService;->clearBalance(Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public clearBalanceWithReasonText(Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/GiftCard;",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;>;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;-><init>()V

    .line 96
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 97
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/GiftCard;->server_id:Ljava/lang/String;

    .line 98
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->gift_card_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->notes(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->reason(Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;

    move-result-object p1

    .line 103
    iget-object p2, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    invoke-interface {p2, p1}, Lcom/squareup/server/payment/GiftCardService;->clearBalance(Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getEGiftCardOrderConfiguration()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
            ">;>;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    new-instance v1, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest$Builder;-><init>()V

    .line 129
    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/payment/GiftCardService;->getEGiftCardOrderConfiguration(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getGiftCardHistory(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
            ">;>;"
        }
    .end annotation

    .line 118
    new-instance v0, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;-><init>()V

    .line 119
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->gift_card_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;

    move-result-object p1

    const/16 v0, 0x64

    .line 120
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->size(Ljava/lang/Integer;)Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;

    move-result-object p1

    .line 121
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;

    move-result-object p1

    .line 123
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    invoke-interface {v0, p1}, Lcom/squareup/server/payment/GiftCardService;->getGiftCardHistory(Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public registerEGiftCard(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;",
            ">;>;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    new-instance v1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;-><init>()V

    .line 157
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 159
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v2

    .line 160
    invoke-virtual {v2}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 158
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/util/DateTimeFactory;

    invoke-direct {v2}, Lcom/squareup/util/DateTimeFactory;-><init>()V

    .line 162
    invoke-virtual {v2}, Lcom/squareup/util/DateTimeFactory;->now()Lcom/squareup/protos/common/time/DateTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    move-result-object v1

    .line 163
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->egift_theme_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    move-result-object p1

    .line 164
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->recipient_email(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    move-result-object p1

    .line 165
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    move-result-object p1

    .line 156
    invoke-interface {v0, p1}, Lcom/squareup/server/payment/GiftCardService;->registerEGiftCard(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 166
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    sget-object p2, Lcom/squareup/giftcard/-$$Lambda$GiftCardServiceHelper$AgSUS1_vtQQGa3fbtLkcfLdhwSI;->INSTANCE:Lcom/squareup/giftcard/-$$Lambda$GiftCardServiceHelper$AgSUS1_vtQQGa3fbtLkcfLdhwSI;

    .line 167
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public registerGiftCard(Lcom/squareup/Card;Lcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/Card;",
            "Lcom/squareup/protos/client/IdPair;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            ">;>;"
        }
    .end annotation

    .line 72
    new-instance v0, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 73
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->cardConverter:Lcom/squareup/payment/CardConverter;

    .line 74
    invoke-virtual {v1, p1}, Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->gift_card_data(Lcom/squareup/protos/client/bills/CardData;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;

    move-result-object p1

    .line 75
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;

    move-result-object p1

    .line 78
    iget-object p2, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    invoke-interface {p2, p1}, Lcom/squareup/server/payment/GiftCardService;->registerGiftCard(Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setEGiftCardOrderConfig(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
            ">;>;"
        }
    .end annotation

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/squareup/giftcard/GiftCardServiceHelper;->setEGiftCardOrderConfig(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public setEGiftCardOrderConfig(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
            ">;>;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    new-instance v1, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;-><init>()V

    .line 142
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->order_configuration(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    move-result-object p1

    .line 143
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->new_custom_egift_themes(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;

    move-result-object p1

    .line 141
    invoke-interface {v0, p1}, Lcom/squareup/server/payment/GiftCardService;->setEGiftCardOrderConfiguration(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 145
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public uploadEGiftCardImage(Ljava/io/File;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
            ">;>;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/squareup/giftcard/GiftCardServiceHelper;->giftCardService:Lcom/squareup/server/payment/GiftCardService;

    .line 175
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/giftcard/GiftCardServiceHelper;->JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

    .line 176
    invoke-static {v2, p1}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/io/File;)Lokhttp3/RequestBody;

    move-result-object p1

    const-string v2, "file"

    .line 173
    invoke-static {v2, v1, p1}, Lokhttp3/MultipartBody$Part;->createFormData(Ljava/lang/String;Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/MultipartBody$Part;

    move-result-object p1

    .line 172
    invoke-interface {v0, p1}, Lcom/squareup/server/payment/GiftCardService;->uploadEGiftCardTheme(Lokhttp3/MultipartBody$Part;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
