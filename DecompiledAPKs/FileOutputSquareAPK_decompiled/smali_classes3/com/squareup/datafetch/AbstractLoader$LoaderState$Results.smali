.class public final Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;
.super Lcom/squareup/datafetch/AbstractLoader$LoaderState;
.source "AbstractLoader.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/AbstractLoader$LoaderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Results"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState<",
        "TTInput;TTItem;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractLoader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractLoader.kt\ncom/squareup/datafetch/AbstractLoader$LoaderState$Results\n*L\n1#1,321:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\r\n\u0002\u0010\u0000\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0004\u0008\u0004\u0010\u0001*\u0004\u0008\u0005\u0010\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00020\u0003B)\u0012\u0006\u0010\u0004\u001a\u00028\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00050\u00060\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000e\u0010\u0017\u001a\u00028\u0004H\u00c6\u0003\u00a2\u0006\u0002\u0010\rJ\u0015\u0010\u0018\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00050\u00060\u0006H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0008H\u00c6\u0003JD\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00028\u00050\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00028\u00042\u0014\u0008\u0002\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00050\u00060\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001bJ\u0013\u0010\u001c\u001a\u00020\u00082\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u00d6\u0003J\u0016\u0010\u001f\u001a\u00028\u00052\u0006\u0010 \u001a\u00020\u0010H\u0086\u0002\u00a2\u0006\u0002\u0010!J\u0006\u0010\"\u001a\u00020\u0008J\t\u0010#\u001a\u00020\u0010H\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0016\u0010\u0004\u001a\u00028\u0004X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u000e\u001a\u0004\u0008\u000c\u0010\rR\u001b\u0010\u000f\u001a\u00020\u00108FX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0013\u0010\u0014\u001a\u0004\u0008\u0011\u0010\u0012R\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00050\u00060\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;",
        "TInput",
        "TItem",
        "Lcom/squareup/datafetch/AbstractLoader$LoaderState;",
        "input",
        "items",
        "",
        "hasMore",
        "",
        "(Ljava/lang/Object;Ljava/util/List;Z)V",
        "getHasMore",
        "()Z",
        "getInput",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "itemCount",
        "",
        "getItemCount",
        "()I",
        "itemCount$delegate",
        "Lkotlin/Lazy;",
        "getItems",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "copy",
        "(Ljava/lang/Object;Ljava/util/List;Z)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;",
        "equals",
        "other",
        "",
        "get",
        "index",
        "(I)Ljava/lang/Object;",
        "hasItems",
        "hashCode",
        "toString",
        "",
        "data-fetch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hasMore:Z

.field private final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field private final itemCount$delegate:Lkotlin/Lazy;

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TTItem;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Ljava/util/List<",
            "+",
            "Ljava/util/List<",
            "+TTItem;>;>;Z)V"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 242
    invoke-direct {p0, v0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->input:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    .line 243
    new-instance p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2;

    invoke-direct {p1, p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results$itemCount$2;-><init>(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->itemCount$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;Ljava/lang/Object;Ljava/util/List;ZILjava/lang/Object;)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->copy(Ljava/lang/Object;Ljava/util/List;Z)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TTItem;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    return v0
.end method

.method public final copy(Ljava/lang/Object;Ljava/util/List;Z)Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Ljava/util/List<",
            "+",
            "Ljava/util/List<",
            "+TTItem;>;>;Z)",
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "TTInput;TTItem;>;"
        }
    .end annotation

    const-string v0, "items"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;-><init>(Ljava/lang/Object;Ljava/util/List;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    iget-boolean p1, p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTItem;"
        }
    .end annotation

    .line 249
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 250
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 251
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 253
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr p1, v1

    goto :goto_0

    .line 255
    :cond_1
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final getHasMore()Z
    .locals 1

    .line 241
    iget-boolean v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    return v0
.end method

.method public getInput()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTInput;"
        }
    .end annotation

    .line 239
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->input:Ljava/lang/Object;

    return-object v0
.end method

.method public final getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->itemCount$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    return v0
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TTItem;>;>;"
        }
    .end annotation

    .line 240
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    return-object v0
.end method

.method public final hasItems()Z
    .locals 2

    .line 245
    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Results(input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getInput()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hasMore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasMore:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
