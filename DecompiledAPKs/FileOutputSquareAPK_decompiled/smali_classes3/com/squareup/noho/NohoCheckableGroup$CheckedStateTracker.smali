.class Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "NohoCheckableGroup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoCheckableGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckedStateTracker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoCheckableGroup;


# direct methods
.method private constructor <init>(Lcom/squareup/noho/NohoCheckableGroup;)V
    .locals 0

    .line 535
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/noho/NohoCheckableGroup;Lcom/squareup/noho/NohoCheckableGroup$1;)V
    .locals 0

    .line 535
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;-><init>(Lcom/squareup/noho/NohoCheckableGroup;)V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 3

    .line 556
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    .line 557
    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$800(Lcom/squareup/noho/NohoCheckableGroup;)Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    .line 558
    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$800(Lcom/squareup/noho/NohoCheckableGroup;)Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedClickListener;->onCheckedClicked(Lcom/squareup/noho/NohoCheckableGroup;I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 562
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$500(Lcom/squareup/noho/NohoCheckableGroup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 563
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    goto :goto_0

    .line 565
    :cond_1
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->toggle(I)V

    :goto_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 540
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {p2}, Lcom/squareup/noho/NohoCheckableGroup;->access$300(Lcom/squareup/noho/NohoCheckableGroup;)Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 544
    :cond_0
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    const/4 v0, 0x1

    invoke-static {p2, v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$302(Lcom/squareup/noho/NohoCheckableGroup;Z)Z

    .line 545
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {p2}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {p2}, Lcom/squareup/noho/NohoCheckableGroup;->access$500(Lcom/squareup/noho/NohoCheckableGroup;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 546
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {p2}, Lcom/squareup/noho/NohoCheckableGroup;->access$400(Lcom/squareup/noho/NohoCheckableGroup;)Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    .line 547
    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {v1, p2, v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$600(Lcom/squareup/noho/NohoCheckableGroup;IZ)Z

    .line 549
    :cond_1
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {p2, v0}, Lcom/squareup/noho/NohoCheckableGroup;->access$302(Lcom/squareup/noho/NohoCheckableGroup;Z)Z

    .line 551
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result p1

    .line 552
    iget-object p2, p0, Lcom/squareup/noho/NohoCheckableGroup$CheckedStateTracker;->this$0:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-static {p2, p1}, Lcom/squareup/noho/NohoCheckableGroup;->access$700(Lcom/squareup/noho/NohoCheckableGroup;I)V

    return-void
.end method
