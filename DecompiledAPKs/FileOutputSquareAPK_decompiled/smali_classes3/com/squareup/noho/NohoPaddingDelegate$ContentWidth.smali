.class abstract enum Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;
.super Ljava/lang/Enum;
.source "NohoPaddingDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoPaddingDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x440a
    name = "ContentWidth"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

.field public static final enum AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

.field public static final enum SCREEN_MIN_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

.field public static final enum SCREEN_MIN_DIMEN_MINUS_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 35
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth$1;

    const/4 v1, 0x0

    const-string v2, "AS_IS"

    invoke-direct {v0, v2, v1}, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    .line 45
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth$2;

    const/4 v2, 0x1

    const-string v3, "SCREEN_MIN_DIMEN_DESIRED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    .line 56
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth$3;

    const/4 v3, 0x2

    const-string v4, "SCREEN_MIN_DIMEN_MINUS_DIMEN_DESIRED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_MINUS_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    .line 33
    sget-object v4, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_MINUS_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->$VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/squareup/noho/NohoPaddingDelegate$1;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(III)I
    .locals 0

    .line 33
    invoke-static {p0, p1, p2}, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->clampPadding(III)I

    move-result p0

    return p0
.end method

.method private static clampPadding(III)I
    .locals 1

    sub-int v0, p2, p0

    sub-int/2addr v0, p0

    if-le v0, p1, :cond_0

    sub-int/2addr p2, p1

    .line 77
    div-int/lit8 p2, p2, 0x2

    return p2

    :cond_0
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;
    .locals 1

    .line 33
    const-class v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;
    .locals 1

    .line 33
    sget-object v0, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->$VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    return-object v0
.end method


# virtual methods
.method abstract computeHorizontalPadding(Landroid/content/res/Resources;III)I
.end method
