.class public final Lcom/squareup/noho/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final balloonStyle:I = 0x7f040053

.field public static final baseStyle:I = 0x7f040058

.field public static final defaultStyle:I = 0x7f04011b

.field public static final filterOutStyle:I = 0x7f040184

.field public static final layout_edges:I = 0x7f040269

.field public static final layout_insetEdges:I = 0x7f040273

.field public static final nohoActionBarStyle:I = 0x7f0402cc

.field public static final nohoButtonStyle:I = 0x7f0402cd

.field public static final nohoCheckColorFill:I = 0x7f0402ce

.field public static final nohoCheckColorStyleDisabled:I = 0x7f0402cf

.field public static final nohoCheckColorStyleFocused:I = 0x7f0402d0

.field public static final nohoCheckColorStyleNormal:I = 0x7f0402d1

.field public static final nohoCheckColorStylePressed:I = 0x7f0402d2

.field public static final nohoCheckColorTick:I = 0x7f0402d3

.field public static final nohoCheckColors:I = 0x7f0402d4

.field public static final nohoConstraintLayoutStyle:I = 0x7f0402d5

.field public static final nohoDestructiveButtonStyle:I = 0x7f0402d6

.field public static final nohoEditStyle:I = 0x7f0402d7

.field public static final nohoEditTextStyle:I = 0x7f0402d8

.field public static final nohoIconButtonStyle:I = 0x7f0402d9

.field public static final nohoIconColor:I = 0x7f0402da

.field public static final nohoLabelBody2Style:I = 0x7f0402db

.field public static final nohoLabelBodyStyle:I = 0x7f0402dc

.field public static final nohoLabelDisplay2Style:I = 0x7f0402dd

.field public static final nohoLabelDisplayStyle:I = 0x7f0402de

.field public static final nohoLabelHeading2Style:I = 0x7f0402df

.field public static final nohoLabelHeadingStyle:I = 0x7f0402e0

.field public static final nohoLabelLabel2Style:I = 0x7f0402e1

.field public static final nohoLabelLabelStyle:I = 0x7f0402e2

.field public static final nohoLinearLayoutStyle:I = 0x7f0402e3

.field public static final nohoLinkButtonStyle:I = 0x7f0402e4

.field public static final nohoListViewStyle:I = 0x7f0402e5

.field public static final nohoMessageTextStyle:I = 0x7f0402e6

.field public static final nohoMessageViewStyle:I = 0x7f0402e7

.field public static final nohoNumberPickerStyle:I = 0x7f0402e8

.field public static final nohoPrimaryButtonStyle:I = 0x7f0402e9

.field public static final nohoRadioColorBackground:I = 0x7f0402ea

.field public static final nohoRadioColorCenter:I = 0x7f0402eb

.field public static final nohoRadioColorOuterCircle:I = 0x7f0402ec

.field public static final nohoRadioColorStyleChecked:I = 0x7f0402ed

.field public static final nohoRadioColorStyleCheckedPressed:I = 0x7f0402ee

.field public static final nohoRadioColorStyleDisabled:I = 0x7f0402ef

.field public static final nohoRadioColorStyleNormal:I = 0x7f0402f0

.field public static final nohoRadioColorStylePressed:I = 0x7f0402f1

.field public static final nohoRadioColors:I = 0x7f0402f2

.field public static final nohoRecyclerViewStyle:I = 0x7f0402f3

.field public static final nohoResponsiveLinearLayoutStyle:I = 0x7f0402f4

.field public static final nohoScrollViewStyle:I = 0x7f0402f5

.field public static final nohoSecondaryButtonStyle:I = 0x7f0402f6

.field public static final nohoSpinnerStyle:I = 0x7f0402f7

.field public static final nohoSwitchStyle:I = 0x7f0402f8

.field public static final nohoTertiaryButtonStyle:I = 0x7f0402f9

.field public static final nohoTitleValueRowStyle:I = 0x7f0402fa

.field public static final noho_state_error:I = 0x7f0402fb

.field public static final noho_state_open:I = 0x7f0402fc

.field public static final noho_state_validated:I = 0x7f0402fd

.field public static final sqAccessoryStyle:I = 0x7f040362

.field public static final sqAccessoryType:I = 0x7f040363

.field public static final sqActionIcon:I = 0x7f040364

.field public static final sqActionIconStyle:I = 0x7f040365

.field public static final sqActionLink:I = 0x7f040366

.field public static final sqActionLinkAppearance:I = 0x7f040367

.field public static final sqAlignToLabel:I = 0x7f040368

.field public static final sqBallBackgroundColor:I = 0x7f040369

.field public static final sqButtonType:I = 0x7f04036b

.field public static final sqCheckType:I = 0x7f04036e

.field public static final sqCheckableRowStyle:I = 0x7f04036f

.field public static final sqCheckedButton:I = 0x7f040370

.field public static final sqClearButton:I = 0x7f040371

.field public static final sqColor:I = 0x7f040372

.field public static final sqContentPaddingType:I = 0x7f040373

.field public static final sqDescription:I = 0x7f040374

.field public static final sqDescriptionAppearance:I = 0x7f040375

.field public static final sqDetailsAppearance:I = 0x7f040376

.field public static final sqDrawable:I = 0x7f040377

.field public static final sqDropdownStyle:I = 0x7f040378

.field public static final sqEdgeColor:I = 0x7f040379

.field public static final sqEdgeWidth:I = 0x7f04037a

.field public static final sqErrorAppearance:I = 0x7f04037b

.field public static final sqFillAvailableArea:I = 0x7f04037c

.field public static final sqFlexibleLabel:I = 0x7f04037d

.field public static final sqFocusColor:I = 0x7f04037e

.field public static final sqFocusHeight:I = 0x7f04037f

.field public static final sqHelpText:I = 0x7f040381

.field public static final sqHideBorder:I = 0x7f040382

.field public static final sqHideShadow:I = 0x7f040383

.field public static final sqIcon:I = 0x7f040384

.field public static final sqIconBackground:I = 0x7f040385

.field public static final sqIconStyle:I = 0x7f040386

.field public static final sqInnerShadowColor:I = 0x7f040388

.field public static final sqInnerShadowHeight:I = 0x7f040389

.field public static final sqInputBoxStyle:I = 0x7f04038a

.field public static final sqIsAlert:I = 0x7f04038b

.field public static final sqIsHorizontal:I = 0x7f04038c

.field public static final sqIsResponsive:I = 0x7f04038d

.field public static final sqIsTwoLines:I = 0x7f04038e

.field public static final sqLabel:I = 0x7f04038f

.field public static final sqLabelAppearance:I = 0x7f040390

.field public static final sqLabelColor:I = 0x7f040391

.field public static final sqLabelLayoutWeight:I = 0x7f040392

.field public static final sqLabelStyle:I = 0x7f040393

.field public static final sqLabelText:I = 0x7f040394

.field public static final sqLabelType:I = 0x7f040395

.field public static final sqLeadingButtonType:I = 0x7f040396

.field public static final sqLeadingText:I = 0x7f040397

.field public static final sqLinkText:I = 0x7f040398

.field public static final sqMenuViewLayoutId:I = 0x7f04039a

.field public static final sqMessageText:I = 0x7f04039b

.field public static final sqMinHeightTwoLines:I = 0x7f04039c

.field public static final sqNoteColor:I = 0x7f04039e

.field public static final sqPositionInList:I = 0x7f0403a3

.field public static final sqPrimaryButtonText:I = 0x7f0403a4

.field public static final sqRecyclerEdgesStyle:I = 0x7f0403a5

.field public static final sqRowStyle:I = 0x7f0403a6

.field public static final sqSearchIcon:I = 0x7f0403a7

.field public static final sqSecondaryButtonText:I = 0x7f0403a8

.field public static final sqSelectableStyle:I = 0x7f0403aa

.field public static final sqSelectedViewLayoutId:I = 0x7f0403ac

.field public static final sqSelectionDividersDistance:I = 0x7f0403ad

.field public static final sqSelectionItemGap:I = 0x7f0403ae

.field public static final sqSelectionTextColor:I = 0x7f0403af

.field public static final sqSingleChoice:I = 0x7f0403b1

.field public static final sqSubText:I = 0x7f0403b2

.field public static final sqSubTextAppearance:I = 0x7f0403b3

.field public static final sqSubTextColor:I = 0x7f0403b4

.field public static final sqSubValue:I = 0x7f0403b5

.field public static final sqSubValueAppearance:I = 0x7f0403b6

.field public static final sqSwitchColor:I = 0x7f0403b7

.field public static final sqTextAlignment:I = 0x7f0403b8

.field public static final sqTextGravity:I = 0x7f0403b9

.field public static final sqTextIcon:I = 0x7f0403ba

.field public static final sqTrailingButtonType:I = 0x7f0403bb

.field public static final sqTrailingText:I = 0x7f0403bc

.field public static final sqTwoColumns:I = 0x7f0403bd

.field public static final sqValue:I = 0x7f0403be

.field public static final sqValueAppearance:I = 0x7f0403bf

.field public static final sqValueText:I = 0x7f0403c0

.field public static final sqViewPasswordButton:I = 0x7f0403c1

.field public static final textStyle:I = 0x7f04044e

.field public static final vectorStyle:I = 0x7f040493


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
