.class public final Lcom/squareup/noho/NohoRow$ExtraArrangeable;
.super Ljava/lang/Object;
.source "NohoRow.kt"

# interfaces
.implements Lcom/squareup/noho/NohoRow$Arrangeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExtraArrangeable"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0014\u0010\n\u001a\u00020\u000bX\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000cR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$ExtraArrangeable;",
        "Lcom/squareup/noho/NohoRow$Arrangeable;",
        "view",
        "Landroid/view/View;",
        "margin",
        "",
        "(Landroid/view/View;I)V",
        "id",
        "getId",
        "()I",
        "isEnabled",
        "",
        "()Z",
        "getMargin",
        "getView",
        "()Landroid/view/View;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:I

.field private final isEnabled:Z

.field private final margin:I

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1000
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->view:Landroid/view/View;

    iput p2, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->margin:I

    .line 1001
    iget-object p1, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->id:I

    const/4 p1, 0x1

    .line 1002
    iput-boolean p1, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->isEnabled:Z

    return-void
.end method


# virtual methods
.method public get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1000
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoRow$Arrangeable$DefaultImpls;->get(Lcom/squareup/noho/NohoRow$Arrangeable;Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;

    move-result-object p1

    return-object p1
.end method

.method public getId()I
    .locals 1

    .line 1001
    iget v0, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->id:I

    return v0
.end method

.method public getMargin()I
    .locals 1

    .line 1000
    iget v0, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->margin:I

    return v0
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .line 1000
    iget-object v0, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->view:Landroid/view/View;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 1002
    iget-boolean v0, p0, Lcom/squareup/noho/NohoRow$ExtraArrangeable;->isEnabled:Z

    return v0
.end method
