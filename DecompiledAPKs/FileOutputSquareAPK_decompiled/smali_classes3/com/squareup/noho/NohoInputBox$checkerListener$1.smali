.class public final Lcom/squareup/noho/NohoInputBox$checkerListener$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "NohoInputBox.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoInputBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/noho/NohoInputBox$checkerListener$1",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "text",
        "Landroid/text/Editable;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoInputBox;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoInputBox;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 84
    iput-object p1, p0, Lcom/squareup/noho/NohoInputBox$checkerListener$1;->this$0:Lcom/squareup/noho/NohoInputBox;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 85
    iget-object p1, p0, Lcom/squareup/noho/NohoInputBox$checkerListener$1;->this$0:Lcom/squareup/noho/NohoInputBox;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/noho/NohoInputBox;->refreshState$default(Lcom/squareup/noho/NohoInputBox;ZILjava/lang/Object;)V

    return-void
.end method
