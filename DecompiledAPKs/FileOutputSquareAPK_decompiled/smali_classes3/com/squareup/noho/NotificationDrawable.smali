.class public final Lcom/squareup/noho/NotificationDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "NotificationDrawable.kt"

# interfaces
.implements Lcom/squareup/noho/NeedsApplicationContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NotificationDrawable$Item;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDrawable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDrawable.kt\ncom/squareup/noho/NotificationDrawable\n+ 2 XmlResourceParsing.kt\ncom/squareup/android/xml/XmlResourceParsingKt\n+ 3 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n+ 4 Delegates.kt\ncom/squareup/util/DelegatesKt\n+ 5 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,281:1\n38#2:282\n60#3,6:283\n60#3,6:289\n37#3,6:299\n45#4:295\n33#5,3:296\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationDrawable.kt\ncom/squareup/noho/NotificationDrawable\n*L\n101#1:282\n144#1,6:283\n166#1,6:289\n83#1,6:299\n74#1:295\n74#1,3:296\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0015\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0001VB-\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0003\u0010\u0008\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\tB\u0005\u00a2\u0006\u0002\u0010\nJ&\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020,2\n\u0010-\u001a\u00060.R\u00020/2\u0008\u0008\u0001\u00100\u001a\u00020\u0006H\u0002J\u0010\u00101\u001a\u00020*2\u0006\u00102\u001a\u000203H\u0016J\u0008\u00104\u001a\u00020%H\u0002J\u0008\u00105\u001a\u00020\u0006H\u0016J\u0008\u00106\u001a\u00020\u0006H\u0016J\u0008\u00107\u001a\u00020\u0006H\u0016J.\u00108\u001a\u00020*2\u0006\u00109\u001a\u00020/2\u0006\u0010:\u001a\u00020;2\u0006\u0010<\u001a\u00020=2\u000c\u0010>\u001a\u0008\u0018\u00010.R\u00020/H\u0016J\u0008\u0010?\u001a\u00020@H\u0016J0\u0010A\u001a\u00020\u00112\u0006\u00109\u001a\u00020/2\n\u0010-\u001a\u00060.R\u00020/2\u0008\u0008\u0001\u0010B\u001a\u00020\u00062\u0008\u0008\u0002\u0010C\u001a\u00020\u0006H\u0002J\u001a\u0010D\u001a\u00020*2\u0006\u00109\u001a\u00020/2\u0008\u0008\u0001\u0010E\u001a\u00020\u0006H\u0002J\u0010\u0010F\u001a\u00020*2\u0006\u0010G\u001a\u00020\u0006H\u0016J\u0012\u0010H\u001a\u00020*2\u0008\u0010I\u001a\u0004\u0018\u00010JH\u0016J\u0010\u0010K\u001a\u00020@2\u0006\u0010L\u001a\u00020MH\u0016J\u0010\u0010N\u001a\u00020*2\u0006\u0010O\u001a\u00020\u0006H\u0016J\u0012\u0010P\u001a\u00020*2\u0008\u0010Q\u001a\u0004\u0018\u00010RH\u0016J\u0012\u0010S\u001a\u00020*2\u0008\u0010T\u001a\u0004\u0018\u00010UH\u0016R\u001a\u0010\u000b\u001a\u00020\u0004X\u0096.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R/\u0010\u001a\u001a\u0004\u0018\u00010\u00192\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u00198F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001f\u0010 \u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u000e\u0010!\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010$\u001a\u0004\u0018\u00010%X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\'X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006W"
    }
    d2 = {
        "Lcom/squareup/noho/NotificationDrawable;",
        "Landroid/graphics/drawable/Drawable;",
        "Lcom/squareup/noho/NeedsApplicationContext;",
        "context",
        "Landroid/content/Context;",
        "drawableId",
        "",
        "defStyleAttr",
        "defStyleRes",
        "(Landroid/content/Context;III)V",
        "()V",
        "applicationContext",
        "getApplicationContext",
        "()Landroid/content/Context;",
        "setApplicationContext",
        "(Landroid/content/Context;)V",
        "balloon",
        "Lcom/squareup/noho/NotificationDrawable$Item;",
        "base",
        "basePaint",
        "Landroid/graphics/Paint;",
        "compositionHeight",
        "compositionWidth",
        "filterOut",
        "<set-?>",
        "",
        "text",
        "getText",
        "()Ljava/lang/String;",
        "setText",
        "(Ljava/lang/String;)V",
        "text$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "textAppearanceId",
        "textBounds",
        "Landroid/graphics/Rect;",
        "textPaint",
        "Landroid/text/TextPaint;",
        "textX",
        "",
        "textY",
        "configureFrom",
        "",
        "ta",
        "Landroid/content/res/TypedArray;",
        "theme",
        "Landroid/content/res/Resources$Theme;",
        "Landroid/content/res/Resources;",
        "baseDrawableId",
        "draw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "ensureTextAppearance",
        "getIntrinsicHeight",
        "getIntrinsicWidth",
        "getOpacity",
        "inflate",
        "resources",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "maybeTheme",
        "isStateful",
        "",
        "loadItem",
        "styleId",
        "defaultDrawableId",
        "loadText",
        "textStyle",
        "setAlpha",
        "alpha",
        "setColorFilter",
        "colorFilter",
        "Landroid/graphics/ColorFilter;",
        "setState",
        "stateSet",
        "",
        "setTint",
        "tintColor",
        "setTintList",
        "tint",
        "Landroid/content/res/ColorStateList;",
        "setTintMode",
        "tintMode",
        "Landroid/graphics/PorterDuff$Mode;",
        "Item",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field public applicationContext:Landroid/content/Context;

.field private balloon:Lcom/squareup/noho/NotificationDrawable$Item;

.field private base:Lcom/squareup/noho/NotificationDrawable$Item;

.field private final basePaint:Landroid/graphics/Paint;

.field private compositionHeight:I

.field private compositionWidth:I

.field private filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

.field private final text$delegate:Lkotlin/properties/ReadWriteProperty;

.field private textAppearanceId:I

.field private final textBounds:Landroid/graphics/Rect;

.field private textPaint:Landroid/text/TextPaint;

.field private textX:F

.field private textY:F


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/NotificationDrawable;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "text"

    const-string v4, "getText()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NotificationDrawable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 52
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->textBounds:Landroid/graphics/Rect;

    .line 71
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    check-cast v1, Landroid/graphics/Xfermode;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iput-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->basePaint:Landroid/graphics/Paint;

    .line 295
    sget-object v0, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 296
    new-instance v0, Lcom/squareup/noho/NotificationDrawable$$special$$inlined$observable$1;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, p0}, Lcom/squareup/noho/NotificationDrawable$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NotificationDrawable;)V

    check-cast v0, Lkotlin/properties/ReadWriteProperty;

    .line 295
    iput-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->text$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;III)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lcom/squareup/noho/NotificationDrawable;-><init>()V

    .line 85
    sget-object v0, Lcom/squareup/noho/R$styleable;->NotificationDrawable:[I

    const-string v1, "R.styleable.NotificationDrawable"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 299
    invoke-virtual {p1, v1, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p3

    :try_start_0
    const-string p4, "a"

    .line 301
    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p4

    const-string v0, "context.theme"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, p3, p4, p2}, Lcom/squareup/noho/NotificationDrawable;->access$configureFrom(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/TypedArray;Landroid/content/res/Resources$Theme;I)V

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "context.applicationContext"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NotificationDrawable;->setApplicationContext(Landroid/content/Context;)V

    .line 91
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method public synthetic constructor <init>(Landroid/content/Context;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_0

    const/4 p3, 0x0

    :cond_0
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_1

    .line 81
    sget p4, Lcom/squareup/noho/R$style;->Widget_Noho_Notification:I

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/NotificationDrawable;-><init>(Landroid/content/Context;III)V

    return-void
.end method

.method public static final synthetic access$configureFrom(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/TypedArray;Landroid/content/res/Resources$Theme;I)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NotificationDrawable;->configureFrom(Landroid/content/res/TypedArray;Landroid/content/res/Resources$Theme;I)V

    return-void
.end method

.method public static final synthetic access$getCompositionHeight$p(Lcom/squareup/noho/NotificationDrawable;)I
    .locals 0

    .line 52
    iget p0, p0, Lcom/squareup/noho/NotificationDrawable;->compositionHeight:I

    return p0
.end method

.method public static final synthetic access$getCompositionWidth$p(Lcom/squareup/noho/NotificationDrawable;)I
    .locals 0

    .line 52
    iget p0, p0, Lcom/squareup/noho/NotificationDrawable;->compositionWidth:I

    return p0
.end method

.method public static final synthetic access$getTextAppearanceId$p(Lcom/squareup/noho/NotificationDrawable;)I
    .locals 0

    .line 52
    iget p0, p0, Lcom/squareup/noho/NotificationDrawable;->textAppearanceId:I

    return p0
.end method

.method public static final synthetic access$getTextX$p(Lcom/squareup/noho/NotificationDrawable;)F
    .locals 0

    .line 52
    iget p0, p0, Lcom/squareup/noho/NotificationDrawable;->textX:F

    return p0
.end method

.method public static final synthetic access$getTextY$p(Lcom/squareup/noho/NotificationDrawable;)F
    .locals 0

    .line 52
    iget p0, p0, Lcom/squareup/noho/NotificationDrawable;->textY:F

    return p0
.end method

.method public static final synthetic access$setCompositionHeight$p(Lcom/squareup/noho/NotificationDrawable;I)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/squareup/noho/NotificationDrawable;->compositionHeight:I

    return-void
.end method

.method public static final synthetic access$setCompositionWidth$p(Lcom/squareup/noho/NotificationDrawable;I)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/squareup/noho/NotificationDrawable;->compositionWidth:I

    return-void
.end method

.method public static final synthetic access$setTextAppearanceId$p(Lcom/squareup/noho/NotificationDrawable;I)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/squareup/noho/NotificationDrawable;->textAppearanceId:I

    return-void
.end method

.method public static final synthetic access$setTextX$p(Lcom/squareup/noho/NotificationDrawable;F)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/squareup/noho/NotificationDrawable;->textX:F

    return-void
.end method

.method public static final synthetic access$setTextY$p(Lcom/squareup/noho/NotificationDrawable;F)V
    .locals 0

    .line 52
    iput p1, p0, Lcom/squareup/noho/NotificationDrawable;->textY:F

    return-void
.end method

.method private final configureFrom(Landroid/content/res/TypedArray;Landroid/content/res/Resources$Theme;I)V
    .locals 13

    move-object v7, p0

    move-object v8, p1

    .line 123
    sget v0, Lcom/squareup/noho/R$styleable;->NotificationDrawable_baseStyle:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 124
    sget v2, Lcom/squareup/noho/R$styleable;->NotificationDrawable_filterOutStyle:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 125
    sget v2, Lcom/squareup/noho/R$styleable;->NotificationDrawable_balloonStyle:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 126
    sget v2, Lcom/squareup/noho/R$styleable;->NotificationDrawable_textStyle:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v10

    .line 127
    sget v2, Lcom/squareup/noho/R$styleable;->NotificationDrawable_android_width:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, v7, Lcom/squareup/noho/NotificationDrawable;->compositionWidth:I

    .line 128
    sget v2, Lcom/squareup/noho/R$styleable;->NotificationDrawable_android_height:I

    invoke-virtual {p1, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, v7, Lcom/squareup/noho/NotificationDrawable;->compositionHeight:I

    .line 129
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v11, "resources"

    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v12, p2

    move/from16 v2, p3

    invoke-direct {p0, v1, p2, v0, v2}, Lcom/squareup/noho/NotificationDrawable;->loadItem(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;II)Lcom/squareup/noho/NotificationDrawable$Item;

    move-result-object v0

    iput-object v0, v7, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    .line 130
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/squareup/noho/NotificationDrawable;->loadItem$default(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;IIILjava/lang/Object;)Lcom/squareup/noho/NotificationDrawable$Item;

    move-result-object v0

    iput-object v0, v7, Lcom/squareup/noho/NotificationDrawable;->filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

    .line 131
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    move v3, v9

    invoke-static/range {v0 .. v6}, Lcom/squareup/noho/NotificationDrawable;->loadItem$default(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;IIILjava/lang/Object;)Lcom/squareup/noho/NotificationDrawable$Item;

    move-result-object v0

    iput-object v0, v7, Lcom/squareup/noho/NotificationDrawable;->balloon:Lcom/squareup/noho/NotificationDrawable$Item;

    .line 132
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v10}, Lcom/squareup/noho/NotificationDrawable;->loadText(Landroid/content/res/Resources;I)V

    return-void
.end method

.method private final ensureTextAppearance()Landroid/text/TextPaint;
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->textPaint:Landroid/text/TextPaint;

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/squareup/noho/NotificationDrawable;->textAppearanceId:I

    invoke-static {v0, v1}, Lcom/squareup/noho/CanvasExtensionsKt;->createTextPaintFromTextAppearance(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->textPaint:Landroid/text/TextPaint;

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->textPaint:Landroid/text/TextPaint;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    return-object v0
.end method

.method private final loadItem(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;II)Lcom/squareup/noho/NotificationDrawable$Item;
    .locals 3

    .line 143
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 144
    invoke-static {p1, p3}, Lcom/squareup/util/StyledAttributesKt;->styleAsTheme(Landroid/content/res/Resources;I)Landroid/content/res/Resources$Theme;

    move-result-object p3

    sget-object v1, Lcom/squareup/noho/R$styleable;->NotificationDrawableItem:[I

    const-string v2, "R.styleable.NotificationDrawableItem"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    invoke-virtual {p3, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p3

    :try_start_0
    const-string v1, "a"

    .line 285
    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    sget v1, Lcom/squareup/noho/R$styleable;->NotificationDrawableItem_android_drawable:I

    invoke-virtual {p3, v1, p4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p4

    .line 146
    sget v1, Lcom/squareup/noho/R$styleable;->NotificationDrawableItem_android_left:I

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 147
    sget v1, Lcom/squareup/noho/R$styleable;->NotificationDrawableItem_android_top:I

    invoke-virtual {p3, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 149
    sget v1, Lcom/squareup/noho/R$styleable;->NotificationDrawableItem_android_width:I

    invoke-static {p0}, Lcom/squareup/noho/NotificationDrawable;->access$getCompositionWidth$p(Lcom/squareup/noho/NotificationDrawable;)I

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 151
    sget v1, Lcom/squareup/noho/R$styleable;->NotificationDrawableItem_android_height:I

    invoke-static {p0}, Lcom/squareup/noho/NotificationDrawable;->access$getCompositionHeight$p(Lcom/squareup/noho/NotificationDrawable;)I

    move-result v2

    invoke-virtual {p3, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 152
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    .line 154
    iget p3, v0, Landroid/graphics/Rect;->right:I

    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr p3, v1

    iput p3, v0, Landroid/graphics/Rect;->right:I

    .line 155
    iget p3, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr p3, v1

    iput p3, v0, Landroid/graphics/Rect;->bottom:I

    .line 156
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p3

    if-nez p3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "javaClass.classLoader!!"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p3, p4, p2}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable(Landroid/content/res/Resources;Ljava/lang/ClassLoader;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 157
    new-instance p2, Lcom/squareup/noho/NotificationDrawable$Item;

    invoke-direct {p2, v0, p1}, Lcom/squareup/noho/NotificationDrawable$Item;-><init>(Landroid/graphics/Rect;Landroid/graphics/drawable/Drawable;)V

    return-object p2

    :catchall_0
    move-exception p1

    .line 287
    invoke-virtual {p3}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method static synthetic loadItem$default(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;IIILjava/lang/Object;)Lcom/squareup/noho/NotificationDrawable$Item;
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 140
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/noho/NotificationDrawable;->loadItem(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;II)Lcom/squareup/noho/NotificationDrawable$Item;

    move-result-object p0

    return-object p0
.end method

.method private final loadText(Landroid/content/res/Resources;I)V
    .locals 1

    .line 166
    invoke-static {p1, p2}, Lcom/squareup/util/StyledAttributesKt;->styleAsTheme(Landroid/content/res/Resources;I)Landroid/content/res/Resources$Theme;

    move-result-object p1

    sget-object p2, Lcom/squareup/noho/R$styleable;->NotificationDrawableText:[I

    const-string v0, "R.styleable.NotificationDrawableText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    invoke-virtual {p1, p2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 291
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 168
    sget p2, Lcom/squareup/noho/R$styleable;->NotificationDrawableText_android_textAppearance:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NotificationDrawable;->access$setTextAppearanceId$p(Lcom/squareup/noho/NotificationDrawable;I)V

    .line 169
    sget p2, Lcom/squareup/noho/R$styleable;->NotificationDrawableText_android_x:I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NotificationDrawable;->access$setTextX$p(Lcom/squareup/noho/NotificationDrawable;F)V

    .line 170
    sget p2, Lcom/squareup/noho/R$styleable;->NotificationDrawableText_android_y:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    invoke-static {p0, p2}, Lcom/squareup/noho/NotificationDrawable;->access$setTextY$p(Lcom/squareup/noho/NotificationDrawable;F)V

    .line 171
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 188
    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    neg-float v1, v1

    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    neg-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 191
    iget v1, p0, Lcom/squareup/noho/NotificationDrawable;->compositionWidth:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 192
    iget v2, p0, Lcom/squareup/noho/NotificationDrawable;->compositionHeight:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 190
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 196
    iget-object v1, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    const-string v2, "base"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/squareup/noho/NotificationDrawable$Item;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 198
    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getText()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 202
    iget-object v1, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 205
    :cond_3
    iget-object v3, p0, Lcom/squareup/noho/NotificationDrawable;->filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

    const-string v4, "filterOut"

    if-nez v3, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v3}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v5, p0, Lcom/squareup/noho/NotificationDrawable;->filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v5, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v5}, Lcom/squareup/noho/NotificationDrawable$Item;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 206
    iget-object v3, p0, Lcom/squareup/noho/NotificationDrawable;->balloon:Lcom/squareup/noho/NotificationDrawable$Item;

    const-string v5, "balloon"

    if-nez v3, :cond_6

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v3}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v6, p0, Lcom/squareup/noho/NotificationDrawable;->balloon:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v6, :cond_7

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v6}, Lcom/squareup/noho/NotificationDrawable$Item;->getRect()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 212
    new-instance v3, Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v6, :cond_8

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v6}, Lcom/squareup/noho/NotificationDrawable$Item;->getRect()Landroid/graphics/Rect;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    const/4 v6, 0x0

    invoke-virtual {p1, v3, v6}, Landroid/graphics/Canvas;->saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;)I

    .line 213
    iget-object v3, p0, Lcom/squareup/noho/NotificationDrawable;->filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v3, :cond_9

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v3}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 214
    new-instance v3, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v4, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v4}, Lcom/squareup/noho/NotificationDrawable$Item;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/squareup/noho/NotificationDrawable;->basePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;)I

    .line 215
    iget-object v3, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v3, :cond_b

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v3}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 216
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 217
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 220
    iget-object v2, p0, Lcom/squareup/noho/NotificationDrawable;->balloon:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v2, :cond_c

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {v2}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 221
    move-object v2, v1

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_d

    .line 222
    invoke-direct {p0}, Lcom/squareup/noho/NotificationDrawable;->ensureTextAppearance()Landroid/text/TextPaint;

    move-result-object v2

    .line 225
    invoke-virtual {p0}, Lcom/squareup/noho/NotificationDrawable;->getText()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/squareup/noho/NotificationDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 226
    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v3

    .line 229
    iget v4, p0, Lcom/squareup/noho/NotificationDrawable;->textX:F

    const/4 v5, 0x2

    int-to-float v5, v5

    div-float/2addr v3, v5

    sub-float/2addr v4, v3

    .line 230
    iget v3, p0, Lcom/squareup/noho/NotificationDrawable;->textY:F

    iget-object v6, p0, Lcom/squareup/noho/NotificationDrawable;->textBounds:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v5

    add-float/2addr v3, v6

    .line 231
    check-cast v2, Landroid/graphics/Paint;

    .line 227
    invoke-virtual {p1, v1, v4, v3, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 236
    :cond_d
    :goto_0
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->applicationContext:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v1, "applicationContext"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 241
    iget v0, p0, Lcom/squareup/noho/NotificationDrawable;->compositionHeight:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 239
    iget v0, p0, Lcom/squareup/noho/NotificationDrawable;->compositionWidth:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->text$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NotificationDrawable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 6

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p4, :cond_0

    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {p1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object p4

    :goto_0
    move-object v1, p4

    .line 282
    new-instance v0, Lcom/squareup/android/xml/TagVisitor;

    invoke-direct {v0, p2, p3}, Lcom/squareup/android/xml/TagVisitor;-><init>(Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V

    const-string/jumbo p1, "theme"

    .line 104
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    sget-object v2, Lcom/squareup/noho/R$styleable;->NotificationDrawable:[I

    const-string p1, "R.styleable.NotificationDrawable"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 107
    sget v4, Lcom/squareup/noho/R$style;->Widget_Noho_Notification:I

    .line 108
    new-instance p1, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;

    invoke-direct {p1, p0, v1}, Lcom/squareup/noho/NotificationDrawable$inflate$$inlined$visitXml$lambda$1;-><init>(Lcom/squareup/noho/NotificationDrawable;Landroid/content/res/Resources$Theme;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    .line 103
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/android/xml/TagVisitor;->withStyledAttributes(Landroid/content/res/Resources$Theme;[IIILkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    return-void
.end method

.method public isStateful()Z
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_0

    const-string v1, "base"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_1

    const-string v1, "filterOut"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->balloon:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_2

    const-string v1, "balloon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setApplicationContext(Landroid/content/Context;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/noho/NotificationDrawable;->applicationContext:Landroid/content/Context;

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method

.method public setState([I)Z
    .locals 4

    const-string v0, "stateSet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_0

    const-string v1, "base"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    .line 275
    iget-object v1, p0, Lcom/squareup/noho/NotificationDrawable;->filterOut:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v1, :cond_1

    const-string v2, "filterOut"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v1

    .line 276
    iget-object v2, p0, Lcom/squareup/noho/NotificationDrawable;->balloon:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v2, :cond_2

    const-string v3, "balloon"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result p1

    if-nez v0, :cond_4

    if-nez v1, :cond_4

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final setText(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->text$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NotificationDrawable;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public setTint(I)V
    .locals 2

    .line 256
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_0

    const-string v1, "base"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    return-void
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
    .locals 2

    .line 261
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_0

    const-string v1, "base"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 2

    .line 266
    iget-object v0, p0, Lcom/squareup/noho/NotificationDrawable;->base:Lcom/squareup/noho/NotificationDrawable$Item;

    if-nez v0, :cond_0

    const-string v1, "base"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NotificationDrawable$Item;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    return-void
.end method
