.class public final Lcom/squareup/noho/NohoActionButtonStyleKt;
.super Ljava/lang/Object;
.source "NohoActionButtonStyle.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0005\u001a\u00020\u0001*\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0002\u00a8\u0006\u0007"
    }
    d2 = {
        "applyFrom",
        "",
        "Lcom/squareup/noho/NohoButton;",
        "type",
        "Lcom/squareup/noho/NohoActionButtonStyle;",
        "applyTo",
        "view",
        "noho_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final applyFrom(Lcom/squareup/noho/NohoButton;Lcom/squareup/noho/NohoActionButtonStyle;)V
    .locals 1

    const-string v0, "$this$applyFrom"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-static {p1, p0}, Lcom/squareup/noho/NohoActionButtonStyleKt;->applyTo(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/noho/NohoButton;)V

    return-void
.end method

.method public static final applyTo(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/noho/NohoButton;)V
    .locals 2

    const-string v0, "$this$applyTo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionButtonStyle;->getButtonType()Lcom/squareup/noho/NohoButtonType;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionButtonStyle;->getMinimumWidth()Lcom/squareup/resources/DimenModel;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/noho/NohoButton;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Lcom/squareup/resources/DimenModel;->toSize(Landroid/content/Context;)I

    move-result p0

    invoke-virtual {p1, p0}, Lcom/squareup/noho/NohoButton;->setMinimumWidth(I)V

    return-void
.end method
