.class public final Lcom/squareup/noho/FadingDrawable;
.super Lcom/squareup/noho/DrawableWrapper;
.source "FadingDrawable.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFadingDrawable.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FadingDrawable.kt\ncom/squareup/noho/FadingDrawable\n*L\n1#1,137:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J$\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\n\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u00062\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0015J\u0010\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\u0008\u0010\u0017\u001a\u00020\u000fH\u0002J\u0012\u0010\u0018\u001a\u00020\u000f2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\tR\u0010\u0010\n\u001a\u0004\u0018\u00010\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/noho/FadingDrawable;",
        "Lcom/squareup/noho/DrawableWrapper;",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "(Landroid/graphics/drawable/Drawable;)V",
        "fadeDuration",
        "",
        "isAnimating",
        "",
        "()Z",
        "next",
        "nextNeedsBounds",
        "startTimeMillis",
        "",
        "draw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "fadeTo",
        "duration",
        "bounds",
        "Landroid/graphics/Rect;",
        "onBoundsChange",
        "resetNextBoundsIfNeeded",
        "setColorFilter",
        "cf",
        "Landroid/graphics/ColorFilter;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private fadeDuration:I

.field private next:Landroid/graphics/drawable/Drawable;

.field private nextNeedsBounds:Z

.field private startTimeMillis:J


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/noho/DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public static synthetic fadeTo$default(Lcom/squareup/noho/FadingDrawable;Landroid/graphics/drawable/Drawable;ILandroid/graphics/Rect;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/16 p2, 0xc8

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 26
    check-cast p3, Landroid/graphics/Rect;

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/noho/FadingDrawable;->fadeTo(Landroid/graphics/drawable/Drawable;ILandroid/graphics/Rect;)V

    return-void
.end method

.method private final resetNextBoundsIfNeeded()V
    .locals 2

    .line 106
    iget-boolean v0, p0, Lcom/squareup/noho/FadingDrawable;->nextNeedsBounds:Z

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/squareup/noho/FadingDrawable;->next:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->getDelegate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->getDelegate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 112
    invoke-virtual {p0, v0}, Lcom/squareup/noho/FadingDrawable;->setDelegate(Landroid/graphics/drawable/Drawable;)V

    .line 114
    invoke-virtual {p0, v1}, Lcom/squareup/noho/FadingDrawable;->setDelegate(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    .line 117
    iput-boolean v0, p0, Lcom/squareup/noho/FadingDrawable;->nextNeedsBounds:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1}, Lcom/squareup/noho/DrawableWrapper;->draw(Landroid/graphics/Canvas;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/noho/FadingDrawable;->next:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 47
    invoke-direct {p0}, Lcom/squareup/noho/FadingDrawable;->resetNextBoundsIfNeeded()V

    .line 48
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/squareup/noho/FadingDrawable;->startTimeMillis:J

    sub-long/2addr v1, v3

    long-to-float v1, v1

    iget v2, p0, Lcom/squareup/noho/FadingDrawable;->fadeDuration:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 49
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 51
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->getAlpha()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v5

    mul-float v3, v3, v5

    float-to-int v3, v3

    .line 52
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 75
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 77
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v6

    mul-float v6, v6, v2

    sub-float/2addr v5, v6

    .line 78
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v7

    mul-float v7, v7, v2

    sub-float/2addr v6, v7

    .line 76
    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 80
    invoke-virtual {p1, v2, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 81
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 82
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    cmpl-float p1, v1, v4

    if-ltz p1, :cond_0

    .line 86
    invoke-virtual {p0, v0}, Lcom/squareup/noho/FadingDrawable;->setDelegate(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x0

    .line 87
    check-cast p1, Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/squareup/noho/FadingDrawable;->next:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->invalidateSelf()V

    :cond_1
    :goto_0
    return-void
.end method

.method public final fadeTo(Landroid/graphics/drawable/Drawable;ILandroid/graphics/Rect;)V
    .locals 1

    const-string v0, "next"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p3, :cond_0

    const/4 p3, 0x1

    .line 28
    iput-boolean p3, p0, Lcom/squareup/noho/FadingDrawable;->nextNeedsBounds:Z

    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p1, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 32
    :goto_0
    iput-object p1, p0, Lcom/squareup/noho/FadingDrawable;->next:Landroid/graphics/drawable/Drawable;

    .line 33
    iput p2, p0, Lcom/squareup/noho/FadingDrawable;->fadeDuration:I

    .line 34
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/squareup/noho/FadingDrawable;->startTimeMillis:J

    .line 35
    invoke-virtual {p0}, Lcom/squareup/noho/FadingDrawable;->invalidateSelf()V

    return-void
.end method

.method public final isAnimating()Z
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/noho/FadingDrawable;->next:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "bounds"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-super {p0, p1}, Lcom/squareup/noho/DrawableWrapper;->onBoundsChange(Landroid/graphics/Rect;)V

    const/4 p1, 0x1

    .line 125
    iput-boolean p1, p0, Lcom/squareup/noho/FadingDrawable;->nextNeedsBounds:Z

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .line 131
    invoke-super {p0, p1}, Lcom/squareup/noho/DrawableWrapper;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/noho/FadingDrawable;->next:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :cond_0
    return-void
.end method
