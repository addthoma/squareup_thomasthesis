.class public Lcom/squareup/noho/NohoResponsiveLinearLayout;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "NohoResponsiveLinearLayout.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoResponsiveLinearLayout.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoResponsiveLinearLayout.kt\ncom/squareup/noho/NohoResponsiveLinearLayout\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,34:1\n37#2,6:35\n*E\n*S KotlinDebug\n*F\n+ 1 NohoResponsiveLinearLayout.kt\ncom/squareup/noho/NohoResponsiveLinearLayout\n*L\n16#1,6:35\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0014R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/noho/NohoResponsiveLinearLayout;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "controller",
        "Lcom/squareup/widgets/ResponsiveViewController;",
        "onMeasure",
        "",
        "widthMeasureSpec",
        "",
        "heightMeasureSpec",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/widgets/ResponsiveViewController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    sget v0, Lcom/squareup/noho/R$attr;->nohoResponsiveLinearLayoutStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 18
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoResponseLinearLayout:[I

    const-string v1, "R.styleable.NohoResponseLinearLayout"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget v1, Lcom/squareup/noho/R$attr;->nohoResponsiveLinearLayoutStyle:I

    .line 20
    sget v2, Lcom/squareup/noho/R$style;->Widget_Noho_ResponsiveLinearLayout:I

    .line 35
    invoke-virtual {p1, p2, v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string v0, "a"

    .line 37
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget v0, Lcom/squareup/noho/R$styleable;->NohoResponseLinearLayout_sqIsResponsive:I

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 23
    new-instance p2, Lcom/squareup/widgets/ResponsiveViewController;

    move-object v1, p0

    check-cast v1, Landroid/view/View;

    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/widgets/ResponsiveViewController;-><init>(Landroid/content/Context;ZLandroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/noho/NohoResponsiveLinearLayout;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    return-void

    :catchall_0
    move-exception p1

    .line 39
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/noho/NohoResponsiveLinearLayout;->controller:Lcom/squareup/widgets/ResponsiveViewController;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/ResponsiveViewController;->measure(II)V

    .line 31
    invoke-super {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;->onMeasure(II)V

    return-void
.end method
