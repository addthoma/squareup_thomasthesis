.class public final Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher;
.super Lcom/squareup/noho/ListenerAttacher;
.source "CheckableGroups.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/ListenerAttacher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckableViewAttacher"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ":",
        "Landroid/widget/Checkable;",
        ">",
        "Lcom/squareup/noho/ListenerAttacher<",
        "TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckableGroups.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckableGroups.kt\ncom/squareup/noho/ListenerAttacher$CheckableViewAttacher\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,285:1\n1103#2,7:286\n*E\n*S KotlinDebug\n*F\n+ 1 CheckableGroups.kt\ncom/squareup/noho/ListenerAttacher$CheckableViewAttacher\n*L\n263#1,7:286\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u000c\u0008\u0001\u0010\u0001*\u00020\u0002*\u00020\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J9\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00028\u00012\"\u0010\t\u001a\u001e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00070\nj\u0008\u0012\u0004\u0012\u00028\u0001`\u000cH\u0016\u00a2\u0006\u0002\u0010\rJ\u0015\u0010\u000e\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00028\u0001H\u0016\u00a2\u0006\u0002\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher;",
        "T",
        "Landroid/view/View;",
        "Landroid/widget/Checkable;",
        "Lcom/squareup/noho/ListenerAttacher;",
        "()V",
        "attach",
        "",
        "item",
        "onCheckedChange",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/noho/OnCheckedChange;",
        "(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V",
        "detach",
        "(Landroid/view/View;)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 260
    invoke-direct {p0, v0}, Lcom/squareup/noho/ListenerAttacher;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lkotlin/jvm/functions/Function2<",
            "-TT;-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCheckedChange"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 286
    new-instance v0, Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher$attach$$inlined$onClickDebounced$1;

    invoke-direct {v0, p2, p1}, Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher$attach$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function2;Landroid/view/View;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic attach(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V
    .locals 0

    .line 260
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher;->attach(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 267
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic detach(Ljava/lang/Object;)V
    .locals 0

    .line 260
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/ListenerAttacher$CheckableViewAttacher;->detach(Landroid/view/View;)V

    return-void
.end method
