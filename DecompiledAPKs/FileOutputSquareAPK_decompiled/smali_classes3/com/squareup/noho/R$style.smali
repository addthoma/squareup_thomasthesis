.class public final Lcom/squareup/noho/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final Base_Widget_Noho_Button:I = 0x7f1300e1

.field public static final FontSelection_Noho:I = 0x7f1300f9

.field public static final None:I = 0x7f130149

.field public static final Style_Noho_Dialog:I = 0x7f1301c6

.field public static final Style_Noho_Dialog_Message:I = 0x7f1301c7

.field public static final Style_Noho_Dialog_Title:I = 0x7f1301c8

.field public static final TextAppearance_Noho:I = 0x7f130287

.field public static final TextAppearance_Noho_Banner:I = 0x7f130288

.field public static final TextAppearance_Noho_Body:I = 0x7f130289

.field public static final TextAppearance_Noho_Body2:I = 0x7f13028a

.field public static final TextAppearance_Noho_Button:I = 0x7f13028b

.field public static final TextAppearance_Noho_Heading:I = 0x7f13028c

.field public static final TextAppearance_Noho_Heading2:I = 0x7f13028d

.field public static final TextAppearance_Noho_HelpText:I = 0x7f13028e

.field public static final TextAppearance_Noho_HelpTextLink:I = 0x7f13028f

.field public static final TextAppearance_Noho_Hint:I = 0x7f130290

.field public static final TextAppearance_Noho_Input:I = 0x7f130291

.field public static final TextAppearance_Noho_Label:I = 0x7f130292

.field public static final TextAppearance_Noho_Oversized:I = 0x7f130293

.field public static final TextAppearance_Noho_SecondaryLabel:I = 0x7f130294

.field public static final TextAppearance_Noho_Subheader:I = 0x7f130295

.field public static final TextAppearance_Noho_Subheader_AllCaps:I = 0x7f130296

.field public static final TextAppearance_Noho_TopBar:I = 0x7f130297

.field public static final TextAppearance_Noho_TopBarAction:I = 0x7f130298

.field public static final TextAppearance_Widget_Noho_Edit:I = 0x7f1302af

.field public static final TextAppearance_Widget_Noho_Edit_Label:I = 0x7f1302b0

.field public static final TextAppearance_Widget_Noho_Edit_Note:I = 0x7f1302b1

.field public static final TextAppearance_Widget_Noho_InputBox_Details:I = 0x7f1302b2

.field public static final TextAppearance_Widget_Noho_InputBox_Error:I = 0x7f1302b3

.field public static final TextAppearance_Widget_Noho_Label_AllCapsHeader:I = 0x7f1302b4

.field public static final TextAppearance_Widget_Noho_Label_Base:I = 0x7f1302b5

.field public static final TextAppearance_Widget_Noho_Label_Body:I = 0x7f1302b6

.field public static final TextAppearance_Widget_Noho_Label_Body2:I = 0x7f1302b7

.field public static final TextAppearance_Widget_Noho_Label_Display:I = 0x7f1302b8

.field public static final TextAppearance_Widget_Noho_Label_Display2:I = 0x7f1302b9

.field public static final TextAppearance_Widget_Noho_Label_Heading:I = 0x7f1302ba

.field public static final TextAppearance_Widget_Noho_Label_Heading2:I = 0x7f1302bb

.field public static final TextAppearance_Widget_Noho_Label_Label:I = 0x7f1302bc

.field public static final TextAppearance_Widget_Noho_Label_Label2:I = 0x7f1302bd

.field public static final TextAppearance_Widget_Noho_Label_SubHeader:I = 0x7f1302be

.field public static final TextAppearance_Widget_Noho_Notification:I = 0x7f1302bf

.field public static final TextAppearance_Widget_Noho_Row_ActionLink:I = 0x7f1302c0

.field public static final TextAppearance_Widget_Noho_Row_AllCapsSubHeader_Label:I = 0x7f1302c1

.field public static final TextAppearance_Widget_Noho_Row_AllCapsSubHeader_Value:I = 0x7f1302c2

.field public static final TextAppearance_Widget_Noho_Row_Description:I = 0x7f1302c3

.field public static final TextAppearance_Widget_Noho_Row_Label:I = 0x7f1302c4

.field public static final TextAppearance_Widget_Noho_Row_SubValue:I = 0x7f1302c8

.field public static final TextAppearance_Widget_Noho_Row_TextIcon:I = 0x7f1302c9

.field public static final TextAppearance_Widget_Noho_Row_Value:I = 0x7f1302ca

.field public static final TextAppearance_Widget_Noho_Selectable_Label:I = 0x7f1302d0

.field public static final TextAppearance_Widget_Noho_Selectable_Value:I = 0x7f1302d1

.field public static final Theme_Noho:I = 0x7f13033a

.field public static final Theme_Noho_Dialog:I = 0x7f13033c

.field public static final Theme_Noho_DialogBase:I = 0x7f13033f

.field public static final Theme_Noho_Dialog_NoBackground:I = 0x7f13033d

.field public static final Theme_Noho_Dialog_NoBackground_NoDim:I = 0x7f13033e

.field public static final Widget_Noho_Banner:I = 0x7f13053f

.field public static final Widget_Noho_Button_BuyerFacingLanguageSelection:I = 0x7f130540

.field public static final Widget_Noho_Button_Dialog:I = 0x7f130541

.field public static final Widget_Noho_Button_Dialog_TutorialPrimary:I = 0x7f130542

.field public static final Widget_Noho_Button_Dialog_TutorialSecondary:I = 0x7f130543

.field public static final Widget_Noho_CardEditor:I = 0x7f130544

.field public static final Widget_Noho_ChargeButton:I = 0x7f130545

.field public static final Widget_Noho_ChargeButtonPaymentFlow:I = 0x7f130546

.field public static final Widget_Noho_Check_Colors:I = 0x7f130547

.field public static final Widget_Noho_Check_Colors_Disabled:I = 0x7f130548

.field public static final Widget_Noho_Check_Colors_Focused:I = 0x7f130549

.field public static final Widget_Noho_Check_Colors_Normal:I = 0x7f13054a

.field public static final Widget_Noho_Check_Colors_Pressed:I = 0x7f13054b

.field public static final Widget_Noho_CheckableRow:I = 0x7f13054c

.field public static final Widget_Noho_ConstraintLayout:I = 0x7f13054d

.field public static final Widget_Noho_DatePicker:I = 0x7f13054e

.field public static final Widget_Noho_DestructiveButton:I = 0x7f13054f

.field public static final Widget_Noho_Dropdown:I = 0x7f130550

.field public static final Widget_Noho_Dropdown_MenuItem:I = 0x7f130551

.field public static final Widget_Noho_Dropdown_Popup:I = 0x7f130552

.field public static final Widget_Noho_Dropdown_Popup_ListView:I = 0x7f130553

.field public static final Widget_Noho_Dropdown_SelectedItem:I = 0x7f130554

.field public static final Widget_Noho_Edit:I = 0x7f130555

.field public static final Widget_Noho_EditText:I = 0x7f130557

.field public static final Widget_Noho_EditTextPaymentFlow:I = 0x7f13055b

.field public static final Widget_Noho_HorizontalDividers:I = 0x7f13055c

.field public static final Widget_Noho_IconButton:I = 0x7f13055d

.field public static final Widget_Noho_InputBox:I = 0x7f13055e

.field public static final Widget_Noho_Label_Base:I = 0x7f13055f

.field public static final Widget_Noho_Label_Body:I = 0x7f130560

.field public static final Widget_Noho_Label_Body2:I = 0x7f130561

.field public static final Widget_Noho_Label_Display:I = 0x7f130562

.field public static final Widget_Noho_Label_Display2:I = 0x7f130563

.field public static final Widget_Noho_Label_Heading:I = 0x7f130564

.field public static final Widget_Noho_Label_Heading2:I = 0x7f130565

.field public static final Widget_Noho_Label_Label:I = 0x7f130566

.field public static final Widget_Noho_Label_Label2:I = 0x7f130567

.field public static final Widget_Noho_LinearLayout:I = 0x7f130568

.field public static final Widget_Noho_LinkButton:I = 0x7f130569

.field public static final Widget_Noho_LinkText:I = 0x7f13056a

.field public static final Widget_Noho_ListView:I = 0x7f13056b

.field public static final Widget_Noho_MessageText:I = 0x7f13056c

.field public static final Widget_Noho_MessageView:I = 0x7f13056d

.field public static final Widget_Noho_Notification:I = 0x7f13056e

.field public static final Widget_Noho_Notification_Balloon:I = 0x7f13056f

.field public static final Widget_Noho_Notification_Base:I = 0x7f130570

.field public static final Widget_Noho_Notification_Fatal:I = 0x7f130571

.field public static final Widget_Noho_Notification_Fatal_Balloon:I = 0x7f130572

.field public static final Widget_Noho_Notification_Fatal_FilterOut:I = 0x7f130573

.field public static final Widget_Noho_Notification_FilterOut:I = 0x7f130574

.field public static final Widget_Noho_Notification_HighPri:I = 0x7f130575

.field public static final Widget_Noho_Notification_HighPri_Balloon:I = 0x7f130576

.field public static final Widget_Noho_Notification_Text:I = 0x7f130577

.field public static final Widget_Noho_NumberPicker:I = 0x7f130578

.field public static final Widget_Noho_PrimaryButton:I = 0x7f130579

.field public static final Widget_Noho_PrimaryButtonLarge:I = 0x7f13057a

.field public static final Widget_Noho_Radio_Colors:I = 0x7f13057b

.field public static final Widget_Noho_Radio_Colors_Checked:I = 0x7f13057c

.field public static final Widget_Noho_Radio_Colors_CheckedPressed:I = 0x7f13057d

.field public static final Widget_Noho_Radio_Colors_Disabled:I = 0x7f13057e

.field public static final Widget_Noho_Radio_Colors_Normal:I = 0x7f13057f

.field public static final Widget_Noho_Radio_Colors_Pressed:I = 0x7f130580

.field public static final Widget_Noho_RecyclerEdges:I = 0x7f130581

.field public static final Widget_Noho_RecyclerView:I = 0x7f130582

.field public static final Widget_Noho_ResponsiveLinearLayout:I = 0x7f130583

.field public static final Widget_Noho_Row:I = 0x7f130584

.field public static final Widget_Noho_Row_Accessory:I = 0x7f130585

.field public static final Widget_Noho_Row_ActionIcon:I = 0x7f130586

.field public static final Widget_Noho_Row_Icon:I = 0x7f130587

.field public static final Widget_Noho_Row_Legacy:I = 0x7f130588

.field public static final Widget_Noho_Row_Legacy_Standard:I = 0x7f130589

.field public static final Widget_Noho_ScrollView:I = 0x7f13058a

.field public static final Widget_Noho_SecondaryButton:I = 0x7f13058b

.field public static final Widget_Noho_SecondaryButtonLarge:I = 0x7f13058c

.field public static final Widget_Noho_Selectable:I = 0x7f13058d

.field public static final Widget_Noho_Spinner:I = 0x7f13058e

.field public static final Widget_Noho_SubHeader:I = 0x7f1305a2

.field public static final Widget_Noho_Switch:I = 0x7f1305a3

.field public static final Widget_Noho_TertiaryButton:I = 0x7f1305a4

.field public static final Widget_Noho_TertiaryButtonLarge:I = 0x7f1305a5

.field public static final Widget_Noho_TextView:I = 0x7f1305a6

.field public static final Widget_Noho_TextView_Help:I = 0x7f1305a7

.field public static final Widget_Noho_TopBar:I = 0x7f1305a8

.field public static final Widget_Noho_TopBarAction:I = 0x7f1305a9

.field public static final Widget_Noho_TopBarIcon:I = 0x7f1305aa

.field public static final Widget_Noho_TopBarTitle:I = 0x7f1305ab


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
