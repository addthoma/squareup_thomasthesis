.class public Lcom/squareup/noho/HideablePlugin;
.super Ljava/lang/Object;
.source "NohoEditRowPlugins.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEditRowPlugins.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEditRowPlugins.kt\ncom/squareup/noho/HideablePlugin\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,261:1\n33#2,3:262\n*E\n*S KotlinDebug\n*F\n+ 1 NohoEditRowPlugins.kt\ncom/squareup/noho/HideablePlugin\n*L\n92#1,3:262\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0016\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\tH\u0016J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\tH\u0016J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J\u0008\u0010#\u001a\u00020\u0004H\u0016J\u0018\u0010$\u001a\u00020\u00182\u0006\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\u0016R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u001a\u0010\u0008\u001a\u00020\tX\u0084.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u0014\u0010\u000e\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR+\u0010\u0011\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00048F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0015\u0010\u0016\u001a\u0004\u0008\u0012\u0010\u000f\"\u0004\u0008\u0013\u0010\u0014\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/noho/HideablePlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "delegate",
        "startsVisible",
        "",
        "(Lcom/squareup/noho/NohoEditRow$Plugin;Z)V",
        "getDelegate",
        "()Lcom/squareup/noho/NohoEditRow$Plugin;",
        "edit",
        "Lcom/squareup/noho/NohoEditRow;",
        "getEdit",
        "()Lcom/squareup/noho/NohoEditRow;",
        "setEdit",
        "(Lcom/squareup/noho/NohoEditRow;)V",
        "isClickable",
        "()Z",
        "<set-?>",
        "visible",
        "getVisible",
        "setVisible",
        "(Z)V",
        "visible$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "attach",
        "",
        "editText",
        "description",
        "",
        "editDescription",
        "",
        "detach",
        "measure",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "editRect",
        "Landroid/graphics/Rect;",
        "onClick",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

.field protected edit:Lcom/squareup/noho/NohoEditRow;

.field private final visible$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/HideablePlugin;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string/jumbo v3, "visible"

    const-string v4, "getVisible()Z"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/HideablePlugin;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/NohoEditRow$Plugin;Z)V
    .locals 1

    const-string v0, "delegate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 92
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .line 262
    new-instance p2, Lcom/squareup/noho/HideablePlugin$$special$$inlined$observable$1;

    invoke-direct {p2, p1, p1, p0}, Lcom/squareup/noho/HideablePlugin$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/HideablePlugin;)V

    check-cast p2, Lkotlin/properties/ReadWriteProperty;

    .line 264
    iput-object p2, p0, Lcom/squareup/noho/HideablePlugin;->visible$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/noho/NohoEditRow$Plugin;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    .line 87
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/HideablePlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Plugin;Z)V

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iput-object p1, p0, Lcom/squareup/noho/HideablePlugin;->edit:Lcom/squareup/noho/NohoEditRow;

    .line 100
    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin;->attach(Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin;->description(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin;->detach(Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 85
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public final getDelegate()Lcom/squareup/noho/NohoEditRow$Plugin;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    return-object v0
.end method

.method protected final getEdit()Lcom/squareup/noho/NohoEditRow;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->edit:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_0

    const-string v1, "edit"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getVisible()Z
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->visible$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/HideablePlugin;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isClickable()Z
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0}, Lcom/squareup/noho/NohoEditRow$Plugin;->isClickable()Z

    move-result v0

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 1

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/squareup/noho/HideablePlugin;->getVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin;->measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/squareup/noho/NohoEditRowPluginsKt;->access$getINVISIBLE_SIZE$p()Lcom/squareup/noho/NohoEditRow$PluginSize;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public onClick()Z
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/noho/HideablePlugin;->getVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0}, Lcom/squareup/noho/NohoEditRow$Plugin;->onClick()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0}, Lcom/squareup/noho/HideablePlugin;->getVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->delegate:Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-interface {v0, p1, p2}, Lcom/squareup/noho/NohoEditRow$Plugin;->onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V

    :cond_0
    return-void
.end method

.method protected final setEdit(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iput-object p1, p0, Lcom/squareup/noho/HideablePlugin;->edit:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method public final setVisible(Z)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/HideablePlugin;->visible$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/HideablePlugin;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
