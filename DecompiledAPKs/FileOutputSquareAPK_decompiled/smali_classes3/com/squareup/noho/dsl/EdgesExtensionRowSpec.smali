.class public final Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;
.super Ljava/lang/Object;
.source "RecyclerEdges.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerEdges.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/EdgesExtensionRowSpec\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,186:1\n37#2,6:187\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerEdges.kt\ncom/squareup/noho/dsl/EdgesExtensionRowSpec\n*L\n96#1,6:187\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J \u0010\r\u001a\u00020\u00152\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00050\u0012J\u001d\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0019\u001a\u00020\u0002H\u0000\u00a2\u0006\u0002\u0008\u001aJ\u001d\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0014H\u0000\u00a2\u0006\u0002\u0008\u001eR\u001e\u0010\u0004\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001e\u0010\n\u001a\u00020\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u0007\"\u0004\u0008\u000c\u0010\tR \u0010\r\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u000e\u0010\u0003\u001a\u0004\u0008\u000f\u0010\u0007\"\u0004\u0008\u0010\u0010\tR \u0010\u0011\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00050\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;",
        "S",
        "",
        "()V",
        "defStyleAttr",
        "",
        "getDefStyleAttr",
        "()I",
        "setDefStyleAttr",
        "(I)V",
        "defStyleRes",
        "getDefStyleRes",
        "setDefStyleRes",
        "edges",
        "edges$annotations",
        "getEdges",
        "setEdges",
        "edgesLambda",
        "Lkotlin/Function2;",
        "padding",
        "Landroid/graphics/Rect;",
        "",
        "block",
        "edgesFor",
        "position",
        "item",
        "edgesFor$public_release",
        "context",
        "Landroid/content/Context;",
        "defaultPadding",
        "padding$public_release",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private defStyleAttr:I

.field private defStyleRes:I

.field private edges:I

.field private edgesLambda:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private padding:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec$edgesLambda$1;

    invoke-direct {v0, p0}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec$edgesLambda$1;-><init>(Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    iput-object v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edgesLambda:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static final synthetic access$getPadding$p(Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;)Landroid/graphics/Rect;
    .locals 0

    .line 80
    iget-object p0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->padding:Landroid/graphics/Rect;

    return-object p0
.end method

.method public static final synthetic access$setPadding$p(Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;Landroid/graphics/Rect;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->padding:Landroid/graphics/Rect;

    return-void
.end method

.method public static synthetic edges$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final edges(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edgesLambda:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final edgesFor$public_release(ILjava/lang/Object;)I
    .locals 1

    const-string v0, "item"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edgesLambda:Lkotlin/jvm/functions/Function2;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    return p1
.end method

.method public final getDefStyleAttr()I
    .locals 1

    .line 85
    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleAttr:I

    return v0
.end method

.method public final getDefStyleRes()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleRes:I

    return v0
.end method

.method public final getEdges()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edges:I

    return v0
.end method

.method public final padding$public_release(Landroid/content/Context;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultPadding"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->padding:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleAttr:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleRes:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 98
    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdgesRow:[I

    const-string v2, "R.styleable.NohoRecyclerEdgesRow"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget v2, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleAttr:I

    .line 100
    iget v3, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleRes:I

    .line 187
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string v0, "a"

    .line 189
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Landroid/graphics/Rect;

    .line 103
    sget v1, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdgesRow_android_paddingLeft:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    .line 102
    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 106
    sget v2, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdgesRow_android_paddingTop:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    .line 105
    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 109
    sget v3, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdgesRow_android_paddingRight:I

    iget v4, p2, Landroid/graphics/Rect;->right:I

    .line 108
    invoke-virtual {p1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 112
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRecyclerEdgesRow_android_paddingBottom:I

    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    .line 111
    invoke-virtual {p1, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    .line 101
    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {p0, v0}, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->access$setPadding$p(Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;Landroid/graphics/Rect;)V

    .line 115
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2

    .line 117
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->padding:Landroid/graphics/Rect;

    if-eqz p1, :cond_2

    goto :goto_1

    :cond_2
    move-object p1, p2

    :goto_1
    return-object p1
.end method

.method public final setDefStyleAttr(I)V
    .locals 0

    .line 85
    iput p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleAttr:I

    return-void
.end method

.method public final setDefStyleRes(I)V
    .locals 0

    .line 86
    iput p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->defStyleRes:I

    return-void
.end method

.method public final setEdges(I)V
    .locals 0

    .line 83
    iput p1, p0, Lcom/squareup/noho/dsl/EdgesExtensionRowSpec;->edges:I

    return-void
.end method
