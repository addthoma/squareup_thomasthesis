.class public final Lcom/squareup/noho/NotePlugin;
.super Ljava/lang/Object;
.source "NotePlugin.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotePlugin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotePlugin.kt\ncom/squareup/noho/NotePlugin\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,53:1\n33#2,3:54\n*E\n*S KotlinDebug\n*F\n+ 1 NotePlugin.kt\ncom/squareup/noho/NotePlugin\n*L\n31#1,3:54\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0018\u0010\u001f\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0016R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\rR+\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u00078F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u0010\u0010\u0016\u001a\u00020\u00058\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/noho/NotePlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "context",
        "Landroid/content/Context;",
        "appearanceId",
        "",
        "initialNote",
        "",
        "(Landroid/content/Context;ILjava/lang/String;)V",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "isClickable",
        "",
        "()Z",
        "<set-?>",
        "note",
        "getNote",
        "()Ljava/lang/String;",
        "setNote",
        "(Ljava/lang/String;)V",
        "note$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "notePaddingLeft",
        "paint",
        "Landroid/text/TextPaint;",
        "attach",
        "",
        "measure",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "editRect",
        "Landroid/graphics/Rect;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private editText:Lcom/squareup/noho/NohoEditRow;

.field private final note$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final notePaddingLeft:I

.field private final paint:Landroid/text/TextPaint;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/NotePlugin;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "note"

    const-string v4, "getNote()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NotePlugin;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialNote"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$dimen;->noho_edit_note_margin_before:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/noho/NotePlugin;->notePaddingLeft:I

    .line 29
    invoke-static {p1, p2}, Lcom/squareup/noho/CanvasExtensionsKt;->createTextPaintFromTextAppearance(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/noho/NotePlugin;->paint:Landroid/text/TextPaint;

    .line 31
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 54
    new-instance p1, Lcom/squareup/noho/NotePlugin$$special$$inlined$observable$1;

    invoke-direct {p1, p3, p3, p0}, Lcom/squareup/noho/NotePlugin$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NotePlugin;)V

    check-cast p1, Lkotlin/properties/ReadWriteProperty;

    .line 56
    iput-object p1, p0, Lcom/squareup/noho/NotePlugin;->note$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method

.method public static final synthetic access$getEditText$p(Lcom/squareup/noho/NotePlugin;)Lcom/squareup/noho/NohoEditRow;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/noho/NotePlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-object p0
.end method

.method public static final synthetic access$setEditText$p(Lcom/squareup/noho/NotePlugin;Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/noho/NotePlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/squareup/noho/NotePlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 21
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public final getNote()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NotePlugin;->note$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NotePlugin;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 2

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance p1, Lcom/squareup/noho/NohoEditRow$PluginSize;

    sget-object v0, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1, v1}, Lcom/squareup/noho/NohoEditRow$PluginSize;-><init>(Lcom/squareup/noho/NohoEditRow$Side;II)V

    return-object p1
.end method

.method public onClick()Z
    .locals 1

    .line 21
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 5

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/noho/NotePlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-eqz v0, :cond_1

    .line 39
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    const-string v2, "editText.paint"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/noho/CanvasExtensionsKt;->textWidth(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v0

    .line 40
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getTextRect()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v0

    iget v0, p0, Lcom/squareup/noho/NotePlugin;->notePaddingLeft:I

    add-int/2addr v1, v0

    .line 41
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getTextRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    .line 42
    iget-object v2, p0, Lcom/squareup/noho/NotePlugin;->paint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/squareup/noho/NotePlugin;->getNote()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/noho/CanvasExtensionsKt;->textWidth(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v2

    if-gt v2, v0, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/noho/NotePlugin;->getNote()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/noho/NotePlugin;->getNote()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/squareup/noho/NotePlugin;->paint:Landroid/text/TextPaint;

    int-to-float v0, v0

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v3, v0, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    int-to-float v1, v1

    .line 49
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getTextRect()Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p2

    iget-object v2, p0, Lcom/squareup/noho/NotePlugin;->paint:Landroid/text/TextPaint;

    .line 48
    invoke-static {p1, v0, v1, p2, v2}, Lcom/squareup/noho/CanvasExtensionsKt;->drawTextCenteredAt(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/text/TextPaint;)V

    :cond_1
    return-void
.end method

.method public final setNote(Ljava/lang/String;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NotePlugin;->note$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NotePlugin;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
