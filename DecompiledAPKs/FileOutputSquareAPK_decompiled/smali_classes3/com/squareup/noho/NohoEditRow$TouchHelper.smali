.class final Lcom/squareup/noho/NohoEditRow$TouchHelper;
.super Landroidx/customview/widget/ExploreByTouchHelper;
.source "NohoEditRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEditRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TouchHelper"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEditRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEditRow.kt\ncom/squareup/noho/NohoEditRow$TouchHelper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,583:1\n310#2,7:584\n1651#2,3:591\n*E\n*S KotlinDebug\n*F\n+ 1 NohoEditRow.kt\ncom/squareup/noho/NohoEditRow$TouchHelper\n*L\n529#1,7:584\n534#1,3:591\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0014J\u0016\u0010\u000c\u001a\u00020\r2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u000fH\u0014J\"\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0013\u001a\u00020\u00082\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0014J\u0018\u0010\u0016\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u0018H\u0014J\u0018\u0010\u0019\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u001a\u001a\u00020\u001bH\u0014R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEditRow$TouchHelper;",
        "Landroidx/customview/widget/ExploreByTouchHelper;",
        "(Lcom/squareup/noho/NohoEditRow;)V",
        "accessibilityDescription",
        "",
        "getAccessibilityDescription",
        "()Ljava/lang/CharSequence;",
        "getVirtualViewAt",
        "",
        "x",
        "",
        "y",
        "getVisibleVirtualViews",
        "",
        "result",
        "",
        "onPerformActionForVirtualView",
        "",
        "viewId",
        "action",
        "arguments",
        "Landroid/os/Bundle;",
        "onPopulateEventForVirtualView",
        "event",
        "Landroid/view/accessibility/AccessibilityEvent;",
        "onPopulateNodeForVirtualView",
        "node",
        "Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoEditRow;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoEditRow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 519
    iput-object p1, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    check-cast p1, Landroid/view/View;

    invoke-direct {p0, p1}, Landroidx/customview/widget/ExploreByTouchHelper;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final getAccessibilityDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->valueOrEmpty(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    :goto_0
    return-object v0
.end method

.method protected getVirtualViewAt(FF)I
    .locals 3

    float-to-int p1, p1

    float-to-int p2, p2

    .line 529
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/noho/NohoEditRow;->access$ensureDrawingInfo(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;

    move-result-object v0

    .line 585
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 586
    check-cast v2, Lcom/squareup/noho/NohoEditRow$DrawingInfo;

    .line 529
    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    :goto_1
    if-gez v1, :cond_2

    const/high16 v1, -0x80000000

    :cond_2
    return v1
.end method

.method protected getVisibleVirtualViews(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 534
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/noho/NohoEditRow;->access$getPlugins$p(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 592
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    if-gez v1, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v2, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 535
    invoke-interface {v2}, Lcom/squareup/noho/NohoEditRow$Plugin;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 536
    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
    .locals 0

    const/16 p3, 0x10

    if-eq p2, p3, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 572
    :cond_0
    iget-object p2, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-static {p2, p1}, Lcom/squareup/noho/NohoEditRow;->access$clickPlugin(Lcom/squareup/noho/NohoEditRow;I)Z

    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method protected onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    const-string v0, "event"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 547
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/noho/NohoEditRow;->access$getPlugins$p(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 548
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object p2

    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow$TouchHelper;->getAccessibilityDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/noho/NohoEditRow$Plugin;->description(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected onPopulateNodeForVirtualView(ILandroidx/core/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2

    const-string v0, "node"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/noho/NohoEditRow;->access$getPlugins$p(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow$Plugin;

    .line 555
    invoke-virtual {p0}, Lcom/squareup/noho/NohoEditRow$TouchHelper;->getAccessibilityDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/noho/NohoEditRow$Plugin;->description(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->setText(Ljava/lang/CharSequence;)V

    .line 560
    iget-object v0, p0, Lcom/squareup/noho/NohoEditRow$TouchHelper;->this$0:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0}, Lcom/squareup/noho/NohoEditRow;->access$ensureDrawingInfo(Lcom/squareup/noho/NohoEditRow;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow$DrawingInfo;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    const/16 p1, 0x10

    .line 563
    invoke-virtual {p2, p1}, Landroidx/core/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    return-void
.end method
