.class public abstract Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.super Ljava/lang/Object;
.source "DeviceSettingOrLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final valueWhenDisabled:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "TT;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->features:Lcom/squareup/settings/server/Features;

    const-string/jumbo p1, "valueWhenDisabled"

    .line 18
    invoke-static {p2, p1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->valueWhenDisabled:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 28
    invoke-virtual {p0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->useDeviceProfile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->getValueFromDeviceProfile()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "getValueFromDeviceProfile()"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 32
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->isAllowedWhenUsingLocal()Z

    move-result v0

    if-nez v0, :cond_1

    .line 33
    iget-object v0, p0, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->valueWhenDisabled:Ljava/lang/Object;

    return-object v0

    .line 37
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->getValueFromLocalSettings()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getValueFromDeviceProfile()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract getValueFromLocalSettings()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract isAllowedWhenUsingLocal()Z
.end method

.method public setValueLocally(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 44
    invoke-virtual {p0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->useDeviceProfile()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "newValue"

    .line 49
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->setValueLocallyInternal(Ljava/lang/Object;)V

    return-void

    .line 45
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trying to set "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'s local status when it is being administered through device profiles"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected abstract setValueLocallyInternal(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public useDeviceProfile()Z
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
