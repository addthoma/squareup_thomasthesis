.class public final Lcom/squareup/onboarding/ActivationResourcesService;
.super Ljava/lang/Object;
.source "ActivationResourcesService.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/onboarding/OnboardingActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0001\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0012H\u0016J\u0006\u0010\u0017\u001a\u00020\u0012R6\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u00082\u0012\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u0008@BX\u0086.\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/onboarding/ActivationResourcesService;",
        "Lmortar/Scoped;",
        "activationService",
        "Lcom/squareup/server/activation/ActivationService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;)V",
        "<set-?>",
        "Lio/reactivex/observables/ConnectableObservable;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/activation/ActivationResources;",
        "activationResources",
        "getActivationResources",
        "()Lio/reactivex/observables/ConnectableObservable;",
        "disposable",
        "Lio/reactivex/disposables/Disposable;",
        "onRefresh",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "refresh",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private activationResources:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/activation/ActivationResources;",
            ">;>;"
        }
    .end annotation
.end field

.field private final activationService:Lcom/squareup/server/activation/ActivationService;

.field private disposable:Lio/reactivex/disposables/Disposable;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "activationService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/ActivationResourcesService;->activationService:Lcom/squareup/server/activation/ActivationService;

    iput-object p2, p0, Lcom/squareup/onboarding/ActivationResourcesService;->mainScheduler:Lio/reactivex/Scheduler;

    .line 27
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/onboarding/ActivationResourcesService;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getActivationService$p(Lcom/squareup/onboarding/ActivationResourcesService;)Lcom/squareup/server/activation/ActivationService;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/onboarding/ActivationResourcesService;->activationService:Lcom/squareup/server/activation/ActivationService;

    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/onboarding/ActivationResourcesService;)Lio/reactivex/Scheduler;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/onboarding/ActivationResourcesService;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method


# virtual methods
.method public final getActivationResources()Lio/reactivex/observables/ConnectableObservable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/activation/ActivationResources;",
            ">;>;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationResourcesService;->activationResources:Lio/reactivex/observables/ConnectableObservable;

    if-nez v0, :cond_0

    const-string v1, "activationResources"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/onboarding/ActivationResourcesService;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 39
    new-instance v0, Lcom/squareup/onboarding/ActivationResourcesService$onEnterScope$1;

    invoke-direct {v0, p0}, Lcom/squareup/onboarding/ActivationResourcesService$onEnterScope$1;-><init>(Lcom/squareup/onboarding/ActivationResourcesService;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 v0, 0x1

    .line 45
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    const-string v0, "onRefresh\n        .switc\u2026     }\n        .replay(1)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/onboarding/ActivationResourcesService;->activationResources:Lio/reactivex/observables/ConnectableObservable;

    .line 47
    iget-object p1, p0, Lcom/squareup/onboarding/ActivationResourcesService;->activationResources:Lio/reactivex/observables/ConnectableObservable;

    if-nez p1, :cond_0

    const-string v0, "activationResources"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "activationResources.connect()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/onboarding/ActivationResourcesService;->disposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationResourcesService;->disposable:Lio/reactivex/disposables/Disposable;

    if-nez v0, :cond_0

    const-string v1, "disposable"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method

.method public final refresh()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationResourcesService;->onRefresh:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
