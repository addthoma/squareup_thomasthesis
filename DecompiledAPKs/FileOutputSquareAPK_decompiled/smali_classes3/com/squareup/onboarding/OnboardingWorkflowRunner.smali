.class public interface abstract Lcom/squareup/onboarding/OnboardingWorkflowRunner;
.super Ljava/lang/Object;
.source "OnboardingWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/OnboardingWorkflowRunner$ParentComponent;,
        Lcom/squareup/onboarding/OnboardingWorkflowRunner$NoOp;,
        Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \u00072\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0007\u0008\tJ\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "startWorkflow",
        "",
        "flow",
        "Lcom/squareup/onboarding/OnboardingFlow;",
        "Companion",
        "NoOp",
        "ParentComponent",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/onboarding/OnboardingWorkflowRunner;->Companion:Lcom/squareup/onboarding/OnboardingWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startWorkflow(Lcom/squareup/onboarding/OnboardingFlow;)V
.end method
