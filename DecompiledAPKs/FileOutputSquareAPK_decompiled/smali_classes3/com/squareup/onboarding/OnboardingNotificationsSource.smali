.class public final Lcom/squareup/onboarding/OnboardingNotificationsSource;
.super Ljava/lang/Object;
.source "OnboardingNotificationsSource.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingNotificationsSource.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingNotificationsSource.kt\ncom/squareup/onboarding/OnboardingNotificationsSource\n*L\n1#1,220:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0016H\u0016J\u0014\u0010\u0018\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u00190\u0016H\u0002J\u0016\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00162\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u001a\u0010\u001b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00140\u001c0\u0016H\u0002J\u0014\u0010\u001d\u001a\u00020\u001e*\u00020\u00122\u0006\u0010\u0002\u001a\u00020\u0003H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingNotificationsSource;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "resources",
        "Landroid/content/res/Resources;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "localNotificationAnalytics",
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "bankLinkingStarter",
        "Lcom/squareup/banklinking/BankLinkingStarter;",
        "notificationStateStore",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "(Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V",
        "createOnboardingNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "notificationType",
        "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "notifications",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "onboardingNotifications",
        "",
        "readState",
        "readStatesByNotificationType",
        "",
        "destination",
        "Lcom/squareup/notificationcenterdata/Notification$Destination;",
        "NotificationType",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

.field private final notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

.field private final resources:Landroid/content/res/Resources;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localNotificationAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankLinkingStarter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStateStore"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->resources:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p3, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    iput-object p4, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p5, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    iput-object p6, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    return-void
.end method

.method public static final synthetic access$createOnboardingNotification(Lcom/squareup/onboarding/OnboardingNotificationsSource;Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->createOnboardingNotification(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/onboarding/OnboardingNotificationsSource;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method

.method private final createOnboardingNotification(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 13

    .line 129
    new-instance v12, Lcom/squareup/notificationcenterdata/Notification;

    .line 130
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->getId()Ljava/lang/String;

    move-result-object v1

    .line 132
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->resources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->getTitle()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "resources.getString(notificationType.title)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->resources:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->getContent()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v0, "resources.getString(notificationType.content)"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    move-object v6, v0

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification$Priority;

    .line 136
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->getDisplayType()Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    move-result-object v7

    .line 137
    sget-object v9, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    .line 138
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->resources:Landroid/content/res/Resources;

    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->destination(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;Landroid/content/res/Resources;)Lcom/squareup/notificationcenterdata/Notification$Destination;

    move-result-object v8

    .line 139
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v10

    .line 140
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->getMessageType()Lcom/squareup/communications/Message$Type;

    move-result-object v11

    const-string v2, ""

    move-object v0, v12

    move-object v5, p2

    .line 129
    invoke-direct/range {v0 .. v11}, Lcom/squareup/notificationcenterdata/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;)V

    .line 142
    iget-object p1, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    invoke-interface {p1, v12}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;->logLocalNotificationCreated(Lcom/squareup/notificationcenterdata/Notification;)V

    return-object v12
.end method

.method private final destination(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;Landroid/content/res/Resources;)Lcom/squareup/notificationcenterdata/Notification$Destination;
    .locals 2

    .line 185
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupInApp;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupInApp;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "ClientAction.Builder()\n \u2026ONE)\n            .build()"

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 186
    new-instance p2, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 188
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ActivateAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ActivateAccount$Builder;-><init>()V

    .line 189
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ActivateAccount$Builder;->build()Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    move-result-object v0

    .line 187
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/ClientAction$Builder;->activate_account(Lcom/squareup/protos/client/ClientAction$ActivateAccount;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p2

    .line 191
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p2

    .line 192
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    invoke-direct {p1, p2}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    check-cast p1, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto/16 :goto_0

    .line 194
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$CompleteSignupOnWeb;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 195
    new-instance p2, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 197
    new-instance v0, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;-><init>()V

    .line 198
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$FinishAccountSetup$Builder;->build()Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    move-result-object v0

    .line 196
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/ClientAction$Builder;->finish_account_setup(Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p2

    .line 200
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p2

    .line 201
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    invoke-direct {p1, p2}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    check-cast p1, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto :goto_0

    .line 203
    :cond_1
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 204
    new-instance p2, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 206
    new-instance v0, Lcom/squareup/protos/client/ClientAction$LinkBankAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$LinkBankAccount$Builder;-><init>()V

    .line 207
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$LinkBankAccount$Builder;->build()Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    move-result-object v0

    .line 205
    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/ClientAction$Builder;->link_bank_account(Lcom/squareup/protos/client/ClientAction$LinkBankAccount;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p2

    .line 209
    sget-object v0, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {p2, v0}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object p2

    .line 210
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-direct {p1, p2}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    check-cast p1, Lcom/squareup/notificationcenterdata/Notification$Destination;

    goto :goto_0

    .line 212
    :cond_2
    sget-object v0, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    .line 213
    sget v0, Lcom/squareup/banklinking/R$string;->link_bank_account_url:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "resources.getString(com.\u2026ng.link_bank_account_url)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    sget v1, Lcom/squareup/onboarding/common/R$string;->link_bank_account_notification_browser_dialog_message:I

    .line 214
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v1, "resources.getString(\n   \u2026_dialog_message\n        )"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-direct {p1, v0, p2}, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/notificationcenterdata/Notification$Destination;

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final onboardingNotifications()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;>;"
        }
    .end annotation

    .line 89
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 91
    iget-object v1, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    invoke-interface {v1}, Lcom/squareup/banklinking/BankLinkingStarter;->variant()Lrx/Observable;

    move-result-object v1

    .line 92
    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 93
    invoke-virtual {v1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "bankLinkingStarter.varia\u2026  .distinctUntilChanged()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v2, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "settings.settingsAvailable()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->readStatesByNotificationType()Lio/reactivex/Observable;

    move-result-object v3

    .line 90
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 97
    new-instance v1, Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/OnboardingNotificationsSource$onboardingNotifications$1;-><init>(Lcom/squareup/onboarding/OnboardingNotificationsSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final readState(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/onboarding/OnboardingNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    .line 72
    invoke-virtual {p1}, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;->getId()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-interface {v0, p1, v1}, Lcom/squareup/notificationcenterdata/NotificationStateStore;->getNotificationState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lio/reactivex/Observable;

    move-result-object p1

    .line 73
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "notificationStateStore\n \u2026       .startWith(UNREAD)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final readStatesByNotificationType()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Map<",
            "Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;>;"
        }
    .end annotation

    .line 77
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 78
    sget-object v1, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountInApp;

    check-cast v1, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;

    invoke-direct {p0, v1}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->readState(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType$LinkBankAccountOnWeb;

    check-cast v2, Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;

    invoke-direct {p0, v2}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->readState(Lcom/squareup/onboarding/OnboardingNotificationsSource$NotificationType;)Lio/reactivex/Observable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 79
    sget-object v1, Lcom/squareup/onboarding/OnboardingNotificationsSource$readStatesByNotificationType$1;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$readStatesByNotificationType$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observables\n        .com\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public notifications()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 64
    invoke-direct {p0}, Lcom/squareup/onboarding/OnboardingNotificationsSource;->onboardingNotifications()Lio/reactivex/Observable;

    move-result-object v0

    .line 65
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;->INSTANCE:Lcom/squareup/onboarding/OnboardingNotificationsSource$notifications$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "onboardingNotifications(\u2026     .map { Success(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
