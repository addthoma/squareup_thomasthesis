.class public interface abstract Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;
.super Ljava/lang/Object;
.source "OnboardingVerticalSelectionRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\tJ\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner;",
        "",
        "onActivationCompleted",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;",
        "businessCategoryKey",
        "",
        "onOnboardingCompleted",
        "Lio/reactivex/Completable;",
        "VerticalSelectionResult",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onActivationCompleted(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onOnboardingCompleted()Lio/reactivex/Completable;
.end method
