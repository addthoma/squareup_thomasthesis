.class public final Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;
.super Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;
.source "OnboardingEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Timeout"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003R\u0011\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;",
        "event",
        "(Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)V",
        "getEvent",
        "()Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final event:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->getActionName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;->getOutputs()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;-><init>(Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;->event:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    return-void
.end method


# virtual methods
.method public final getEvent()Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;->event:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    return-object v0
.end method
