.class final Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingRenderer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/analytics/Analytics;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "it",
        "Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;)Lcom/squareup/onboarding/flow/OnboardingEvent;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event$ExitFlowConfirmed;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event$ExitFlowConfirmed;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingEvent;

    goto :goto_0

    .line 115
    :cond_0
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event$ExitFlowCancelled;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event$ExitFlowCancelled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingEvent;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 90
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forDialog$1;->invoke(Lcom/squareup/onboarding/flow/OnboardingExitDialog$Event;)Lcom/squareup/onboarding/flow/OnboardingEvent;

    move-result-object p1

    return-object p1
.end method
