.class public final Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingAddressItem.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\nJ\u0006\u0010\r\u001a\u00020\nJ\u0006\u0010\u000e\u001a\u00020\nJ\u0006\u0010\u000f\u001a\u00020\nJ\u0006\u0010\u0010\u001a\u00020\nJ\u0006\u0010\u0011\u001a\u00020\nJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0014J\u0006\u0010\u0016\u001a\u00020\nJ\u000e\u0010\u0017\u001a\u00020\u00082\u0006\u0010\u0018\u001a\u00020\u0019J\u000e\u0010\u001a\u001a\u00020\u00082\u0006\u0010\u001b\u001a\u00020\nJ\u000e\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\nJ\u000e\u0010\u001e\u001a\u00020\u00082\u0006\u0010\u001f\u001a\u00020\n\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "componentData",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V",
        "apartmentOutput",
        "Lcom/squareup/protos/client/onboard/Output;",
        "apartment",
        "",
        "cityOutput",
        "city",
        "defaultApartment",
        "defaultCity",
        "defaultPostalCode",
        "defaultState",
        "defaultStreet",
        "getValidatorHelper",
        "Lcom/squareup/onboarding/flow/data/OnboardingValidator;",
        "outputs",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;",
        "label",
        "postalCodeIsValidOutput",
        "isValid",
        "",
        "postalCodeOutput",
        "postalCode",
        "stateOutput",
        "state",
        "streetOutput",
        "street",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    return-void
.end method


# virtual methods
.method public final apartmentOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "apartment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address_line_2"

    .line 31
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"address_line_2\", apartment)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final cityOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "city"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locality"

    .line 33
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"locality\", city)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final defaultApartment()Ljava/lang/String;
    .locals 4

    .line 18
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_address_line_2"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "address_line_2"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final defaultCity()Ljava/lang/String;
    .locals 4

    .line 21
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_locality"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "locality"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final defaultPostalCode()Ljava/lang/String;
    .locals 4

    .line 27
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_postal_code"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "postal_code"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final defaultState()Ljava/lang/String;
    .locals 4

    .line 24
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_admin_level_1"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "admin_level_1"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final defaultStreet()Ljava/lang/String;
    .locals 4

    .line 15
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "default_address_line_1"

    const/4 v3, 0x2

    invoke-static {p0, v2, v1, v3, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "address_line_1"

    invoke-interface {v0, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    .line 49
    new-instance v1, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem$getValidatorHelper$1;

    invoke-direct {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem$getValidatorHelper$1;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v3, v1

    check-cast v3, Lkotlin/jvm/functions/Function0;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    move-object v1, v0

    .line 48
    invoke-direct/range {v1 .. v7}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final label()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "label"

    const/4 v2, 0x2

    .line 12
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final postalCodeIsValidOutput(Z)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "_postal_code_is_valid"

    .line 45
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->createOutput$onboarding_release(Ljava/lang/String;Z)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"_postal_code_is_valid\", isValid)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final postalCodeOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "postalCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postal_code"

    .line 37
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"postal_code\", postalCode)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final stateOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "admin_level_1"

    .line 35
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"admin_level_1\", state)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final streetOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "street"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address_line_1"

    .line 29
    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingAddressItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"address_line_1\", street)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
