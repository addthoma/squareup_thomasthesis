.class public final Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;
.super Lcom/squareup/onboarding/flow/OnboardingState;
.source "OnboardingState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowingExitDialog"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u000cX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "context",
        "Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
        "exitDialog",
        "Lcom/squareup/protos/client/onboard/Dialog;",
        "(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)V",
        "getContext",
        "()Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;",
        "getExitDialog",
        "()Lcom/squareup/protos/client/onboard/Dialog;",
        "history",
        "Lcom/squareup/onboarding/flow/PanelHistory;",
        "getHistory",
        "()Lcom/squareup/onboarding/flow/PanelHistory;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

.field private final exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

.field private final history:Lcom/squareup/onboarding/flow/PanelHistory;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "exitDialog"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 100
    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    .line 101
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;->getHistory()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->history:Lcom/squareup/onboarding/flow/PanelHistory;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;ILjava/lang/Object;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->copy(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/onboard/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    return-object v0
.end method

.method public final copy(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "exitDialog"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-direct {v0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;-><init>(Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/protos/client/onboard/Dialog;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    iget-object v1, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    iget-object p1, p1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getContext()Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    return-object v0
.end method

.method public final getExitDialog()Lcom/squareup/protos/client/onboard/Dialog;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    return-object v0
.end method

.method public getHistory()Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->history:Lcom/squareup/onboarding/flow/PanelHistory;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowingExitDialog(context="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->context:Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", exitDialog="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->exitDialog:Lcom/squareup/protos/client/onboard/Dialog;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
