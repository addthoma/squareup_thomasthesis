.class public final Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingHardwareViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingHardwareViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingHardwareViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,97:1\n704#2:98\n777#2,2:99\n1642#2,2:101\n66#3:103\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingHardwareViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder\n*L\n41#1:98\n41#1,2:99\n42#1,2:101\n34#1:103\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0014J\u0008\u0010\u0019\u001a\u00020\u0017H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u00020\u000f8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "captionView",
        "Lcom/squareup/widgets/MessageView;",
        "imageView",
        "Landroid/widget/ImageView;",
        "items",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "getPicasso$onboarding_release",
        "()Lcom/squareup/picasso/Picasso;",
        "setPicasso$onboarding_release",
        "(Lcom/squareup/picasso/Picasso;)V",
        "subtitle",
        "title",
        "onBindComponent",
        "",
        "component",
        "onHighlightError",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final captionView:Lcom/squareup/widgets/MessageView;

.field private final imageView:Landroid/widget/ImageView;

.field private final items:Lcom/squareup/noho/NohoCheckableGroup;

.field public picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final subtitle:Lcom/squareup/widgets/MessageView;

.field private final title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_hardware_list:I

    .line 22
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 27
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_hardware_image:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->imageView:Landroid/widget/ImageView;

    .line 28
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_hardware_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->title:Lcom/squareup/widgets/MessageView;

    .line 29
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_hardware_subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 30
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_hardware_items:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    .line 31
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_hardware_disclaimer:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->captionView:Lcom/squareup/widgets/MessageView;

    .line 34
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 103
    const-class p2, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    .line 35
    invoke-interface {p1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;->inject(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)V

    return-void
.end method

.method public static final synthetic access$getCaptionView$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Lcom/squareup/widgets/MessageView;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->captionView:Lcom/squareup/widgets/MessageView;

    return-object p0
.end method

.method public static final synthetic access$getImageView$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Landroid/widget/ImageView;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->imageView:Landroid/widget/ImageView;

    return-object p0
.end method

.method public static final synthetic access$getItems$p(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)Lcom/squareup/noho/NohoCheckableGroup;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    return-object p0
.end method


# virtual methods
.method public final getPicasso$onboarding_release()Lcom/squareup/picasso/Picasso;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    if-nez v0, :cond_0

    const-string v1, "picasso"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;)V
    .locals 10

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;->products()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 99
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;

    .line 41
    invoke-virtual {v3}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 101
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;

    .line 43
    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    if-nez v2, :cond_3

    const-string v3, "picasso"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/squareup/picasso/RequestCreator;->fetch()V

    goto :goto_2

    .line 47
    :cond_4
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->title:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;->title()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;->subtitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setTextAndVisibility(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;->keySelectedByDefault()Ljava/lang/String;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    new-instance v2, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;

    invoke-direct {v2, p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder$onBindComponent$3;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;)V

    check-cast v2, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    const/4 v1, 0x0

    .line 68
    check-cast v1, Ljava/lang/Integer;

    .line 70
    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoCheckableGroup;->removeAllViews()V

    .line 71
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem;->products()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;

    .line 72
    new-instance v9, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v3, v9

    invoke-direct/range {v3 .. v8}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 73
    invoke-virtual {v9, v2}, Lcom/squareup/noho/NohoCheckableRow;->setTag(Ljava/lang/Object;)V

    .line 74
    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 75
    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getCost()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v9, v3}, Lcom/squareup/noho/NohoCheckableRow;->setDescription(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v3, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    .line 78
    move-object v4, v9

    check-cast v4, Landroid/view/View;

    new-instance v5, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    invoke-direct {v5, v6, v7}, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;-><init>(II)V

    check-cast v5, Landroid/view/ViewGroup$LayoutParams;

    .line 77
    invoke-virtual {v3, v4, v5}, Lcom/squareup/noho/NohoCheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/data/OnboardingHardwareItem$Product;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 83
    invoke-virtual {v9}, Lcom/squareup/noho/NohoCheckableRow;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_3

    :cond_6
    if-eqz v1, :cond_7

    .line 90
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result p1

    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    :cond_7
    return-void
.end method

.method public onHighlightError()V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->items:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->requestFocus()Z

    return-void
.end method

.method public final setPicasso$onboarding_release(Lcom/squareup/picasso/Picasso;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method
