.class public final Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingDropdownViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0014J\u0008\u0010\u000f\u001a\u00020\rH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "spinner",
        "Lcom/squareup/noho/NohoSpinner;",
        "onBindComponent",
        "",
        "component",
        "onHighlightError",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field private final spinner:Lcom/squareup/noho/NohoSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 1

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_dropdown:I

    .line 20
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 24
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_text_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 25
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget p2, Lcom/squareup/onboarding/flow/R$id;->onboarding_spinner:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoSpinner;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->spinner:Lcom/squareup/noho/NohoSpinner;

    return-void
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;)V
    .locals 5

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 30
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;->keys()Ljava/util/List;

    move-result-object v0

    .line 31
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->spinner:Lcom/squareup/noho/NohoSpinner;

    new-instance v2, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder$onBindComponent$1;-><init>(Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;Ljava/util/List;)V

    check-cast v2, Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 43
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/squareup/noho/R$layout;->noho_spinner_item:I

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;->values()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 44
    sget v2, Lcom/squareup/noho/R$layout;->noho_simple_list_item:I

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 46
    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->spinner:Lcom/squareup/noho/NohoSpinner;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;->hint()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/noho/NohoSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v2, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->spinner:Lcom/squareup/noho/NohoSpinner;

    check-cast v1, Landroid/widget/SpinnerAdapter;

    invoke-virtual {v2, v1}, Lcom/squareup/noho/NohoSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 48
    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->spinner:Lcom/squareup/noho/NohoSpinner;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingDropdownItem;->keySelectedByDefault()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {v1, p1}, Lcom/squareup/noho/NohoSpinner;->setSelection(I)V

    return-void
.end method

.method public onHighlightError()V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingDropdownViewHolder;->spinner:Lcom/squareup/noho/NohoSpinner;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoSpinner;->requestFocus()Z

    return-void
.end method
