.class public final Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "OnboardingPersonNameViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1",
        "Lcom/squareup/debounce/DebouncedTextWatcher;",
        "doAfterTextChanged",
        "",
        "s",
        "Landroid/text/Editable;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $component:Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;

.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;",
            ")V"
        }
    .end annotation

    .line 35
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 2

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingPersonNameViewHolder$onBindComponent$1;->$component:Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/onboarding/flow/data/OnboardingPersonNameItem;->firstNameOutput(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/flow/OnboardingInputHandler;->onOutput(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method
