.class public final Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "OnboardingComponentsAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "+",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingComponentsAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingComponentsAdapter.kt\ncom/squareup/onboarding/flow/OnboardingComponentsAdapter\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00192\u0010\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00020\u0001:\u0001\u0019B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nH\u0016J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\nH\u0016J \u0010\u000f\u001a\u00020\u00102\u000e\u0010\u0011\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\r\u001a\u00020\nH\u0016J(\u0010\u0012\u001a\u00020\u0010\"\u0008\u0008\u0000\u0010\u0013*\u00020\u00032\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u0002H\u00130\u00022\u0006\u0010\r\u001a\u00020\nH\u0002J \u0010\u0014\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\nH\u0016J\u0014\u0010\u0018\u001a\u00020\u00102\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0008R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "handler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V",
        "components",
        "",
        "getItemCount",
        "",
        "getItemId",
        "",
        "position",
        "getItemViewType",
        "onBindViewHolder",
        "",
        "holder",
        "onBindViewHolderInner",
        "T",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "update",
        "Companion",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter$Companion;

.field private static final UNSUPPORTED:I = -0x1


# instance fields
.field private components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
            ">;"
        }
    .end annotation
.end field

.field private final handler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->Companion:Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V
    .locals 1

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->handler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    .line 21
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->components:Ljava/util/List;

    const/4 p1, 0x1

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->setHasStableIds(Z)V

    return-void
.end method

.method private final onBindViewHolderInner(Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
            ">(",
            "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
            "TT;>;I)V"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    invoke-virtual {p1, p2}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;->bind(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V

    return-void

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type T"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->components:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->getType()Lcom/squareup/protos/client/onboard/ComponentType;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/ComponentType;->getValue()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
            "+",
            "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->onBindViewHolderInner(Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
            "+",
            "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 36
    invoke-static {p2}, Lcom/squareup/protos/client/onboard/ComponentType;->fromValue(I)Lcom/squareup/protos/client/onboard/ComponentType;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 37
    :goto_0
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->handler:Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    invoke-virtual {v0, p2, p1, v1}, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->createComponentViewHolder(Lcom/squareup/protos/client/onboard/ComponentType;Landroid/view/ViewGroup;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public final update(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "components"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->components:Ljava/util/List;

    .line 29
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->notifyDataSetChanged()V

    return-void
.end method
