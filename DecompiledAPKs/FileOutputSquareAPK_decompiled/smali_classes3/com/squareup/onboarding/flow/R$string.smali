.class public final Lcom/squareup/onboarding/flow/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept_credit_subtitle_no_free_reader:I = 0x7f120024

.field public static final accept_credit_subtitle_non_payment:I = 0x7f120025

.field public static final activation_declined_explanation:I = 0x7f120046

.field public static final activation_declined_explanation_contact_support:I = 0x7f120047

.field public static final activation_declined_title:I = 0x7f120048

.field public static final additional_info_subtitle:I = 0x7f1200a2

.field public static final additional_info_subtitle_vague:I = 0x7f1200a3

.field public static final awaiting_bank_account_verification:I = 0x7f120112

.field public static final awaiting_bank_account_verification_card_skip_message:I = 0x7f120113

.field public static final awaiting_bank_account_verification_message:I = 0x7f120114

.field public static final awaiting_bank_account_verification_schedule_failure_message:I = 0x7f120115

.field public static final awaiting_bank_account_verification_schedule_success_message:I = 0x7f120116

.field public static final awaiting_verification:I = 0x7f12011a

.field public static final back:I = 0x7f12011b

.field public static final bank_confirmation_approved_headline:I = 0x7f12015c

.field public static final bank_confirmation_approved_prompt:I = 0x7f12015d

.field public static final bank_confirmation_headline:I = 0x7f12015e

.field public static final bank_confirmation_prompt:I = 0x7f12015f

.field public static final bank_failed_prompt:I = 0x7f120160

.field public static final business_address_different_as_home:I = 0x7f1201b1

.field public static final business_address_heading:I = 0x7f1201b2

.field public static final business_address_mobile_business:I = 0x7f1201b3

.field public static final business_address_mobile_business_help:I = 0x7f1201b4

.field public static final business_address_same_as_home:I = 0x7f1201b5

.field public static final business_address_subheading:I = 0x7f1201b6

.field public static final business_address_warning_no_po_box_allowed_message:I = 0x7f1201b7

.field public static final business_address_warning_no_po_box_allowed_title:I = 0x7f1201b8

.field public static final business_address_warning_try_again:I = 0x7f1201b9

.field public static final business_address_warning_try_again_message:I = 0x7f1201ba

.field public static final business_info_heading:I = 0x7f1201bc

.field public static final business_info_subheading:I = 0x7f1201bd

.field public static final business_name_hint:I = 0x7f1201be

.field public static final card_not_supported:I = 0x7f120313

.field public static final card_not_supported_bank_pending_message:I = 0x7f120314

.field public static final card_not_supported_bank_success_message:I = 0x7f120315

.field public static final card_processing_not_activated_text:I = 0x7f120338

.field public static final card_processing_not_activated_title:I = 0x7f120339

.field public static final choose_a_category:I = 0x7f12041d

.field public static final confirm_identity_heading:I = 0x7f120481

.field public static final confirm_identity_subheading:I = 0x7f120482

.field public static final contact_support_reapply:I = 0x7f120498

.field public static final content_description_date_of_birth:I = 0x7f1204c0

.field public static final content_description_enter_first_name:I = 0x7f1204c4

.field public static final content_description_enter_full_ssn:I = 0x7f1204c5

.field public static final content_description_enter_last_four_ssn:I = 0x7f1204c6

.field public static final content_description_enter_last_name:I = 0x7f1204c7

.field public static final content_description_enter_name:I = 0x7f1204c8

.field public static final content_description_enter_phone_number:I = 0x7f1204c9

.field public static final could_not_enable_same_day_deposit:I = 0x7f1204da

.field public static final could_not_enable_same_day_deposit_message:I = 0x7f1204db

.field public static final date_hint:I = 0x7f1207ca

.field public static final deposit_speed_subtitle:I = 0x7f120805

.field public static final deposit_speed_title:I = 0x7f120806

.field public static final first_name:I = 0x7f120ad0

.field public static final help_url:I = 0x7f120b50

.field public static final intent_how_custom:I = 0x7f120c43

.field public static final intent_how_custom_description:I = 0x7f120c44

.field public static final intent_how_preset:I = 0x7f120c45

.field public static final intent_how_preset_description:I = 0x7f120c46

.field public static final intent_how_title:I = 0x7f120c47

.field public static final intent_where_computer:I = 0x7f120c48

.field public static final intent_where_invoice:I = 0x7f120c49

.field public static final intent_where_phone:I = 0x7f120c4a

.field public static final intent_where_subtitle:I = 0x7f120c4b

.field public static final intent_where_tablet:I = 0x7f120c4c

.field public static final intent_where_title:I = 0x7f120c4d

.field public static final intent_where_website:I = 0x7f120c4e

.field public static final invalid_ein:I = 0x7f120c57

.field public static final invalid_ein_length:I = 0x7f120c58

.field public static final invalid_last4_ssn_message:I = 0x7f120c5c

.field public static final invalid_last4_ssn_message_with_itin:I = 0x7f120c5d

.field public static final invalid_phone_number:I = 0x7f120c60

.field public static final invalid_phone_number_message:I = 0x7f120c61

.field public static final invalid_postal_code:I = 0x7f120c62

.field public static final invalid_ssn:I = 0x7f120c69

.field public static final invalid_ssn_length:I = 0x7f120c6a

.field public static final last_name:I = 0x7f120eb1

.field public static final later:I = 0x7f120eb2

.field public static final link_bank_account_failed:I = 0x7f120ecf

.field public static final link_bank_account_subtitle:I = 0x7f120ed4

.field public static final link_debit_card_subtitle:I = 0x7f120ed7

.field public static final merchant_category_heading:I = 0x7f120f96

.field public static final missing_address_fields:I = 0x7f120fed

.field public static final missing_date_of_birth:I = 0x7f120ff0

.field public static final missing_debit_card_info:I = 0x7f120ff1

.field public static final missing_debit_card_info_message:I = 0x7f120ff2

.field public static final missing_ein:I = 0x7f120ff3

.field public static final missing_ein_body:I = 0x7f120ff4

.field public static final missing_name_fields:I = 0x7f120ff5

.field public static final missing_personal_fields:I = 0x7f120ff6

.field public static final missing_personal_fields_with_itin:I = 0x7f120ff7

.field public static final missing_phone_number_field:I = 0x7f120ff8

.field public static final missing_required_field:I = 0x7f120ff9

.field public static final next_business_day_deposit:I = 0x7f121075

.field public static final next_business_day_deposit_description:I = 0x7f121076

.field public static final onboarding_accept_cash_only:I = 0x7f1210e5

.field public static final onboarding_actionbar_cancel:I = 0x7f1210e6

.field public static final onboarding_actionbar_continue:I = 0x7f1210e7

.field public static final onboarding_actionbar_later:I = 0x7f1210e8

.field public static final onboarding_actionbar_save:I = 0x7f1210e9

.field public static final onboarding_actionbar_save_address:I = 0x7f1210ea

.field public static final onboarding_actionbar_send:I = 0x7f1210eb

.field public static final onboarding_activate_on_web:I = 0x7f1210ee

.field public static final onboarding_alternative_activate_in_app:I = 0x7f1210ef

.field public static final onboarding_alternative_activate_message:I = 0x7f1210f0

.field public static final onboarding_answer_all:I = 0x7f1210f1

.field public static final onboarding_answers_fail:I = 0x7f1210f2

.field public static final onboarding_answers_progress:I = 0x7f1210f3

.field public static final onboarding_apply_failure_title:I = 0x7f1210f4

.field public static final onboarding_apply_progress_title:I = 0x7f1210f5

.field public static final onboarding_bank_account_confirm_later:I = 0x7f1210f6

.field public static final onboarding_bank_add_failure_title:I = 0x7f1210f7

.field public static final onboarding_bank_add_progress:I = 0x7f1210f8

.field public static final onboarding_card_reader_options_have:I = 0x7f1210f9

.field public static final onboarding_card_reader_options_send:I = 0x7f1210fa

.field public static final onboarding_confirm_address_title:I = 0x7f1210fb

.field public static final onboarding_confirm_magstripe_title:I = 0x7f1210fc

.field public static final onboarding_confirm_same_day_deposit_fee_message:I = 0x7f1210fd

.field public static final onboarding_confirm_same_day_deposit_fee_title:I = 0x7f1210fe

.field public static final onboarding_country:I = 0x7f1210ff

.field public static final onboarding_federal_has_ein:I = 0x7f121100

.field public static final onboarding_federal_no_ein:I = 0x7f121101

.field public static final onboarding_finalize_account_setup:I = 0x7f121102

.field public static final onboarding_finished_heading:I = 0x7f121103

.field public static final onboarding_finished_subheading:I = 0x7f121104

.field public static final onboarding_help_url:I = 0x7f121106

.field public static final onboarding_panel_error_button:I = 0x7f121111

.field public static final onboarding_panel_error_link:I = 0x7f121112

.field public static final onboarding_panel_error_support:I = 0x7f121113

.field public static final onboarding_panel_error_title:I = 0x7f121114

.field public static final onboarding_panel_processing:I = 0x7f121115

.field public static final onboarding_panel_unknown_component:I = 0x7f121116

.field public static final onboarding_quiz_timeout_explanation:I = 0x7f121117

.field public static final onboarding_quiz_timeout_title:I = 0x7f121118

.field public static final onboarding_reader_on_the_way:I = 0x7f121119

.field public static final onboarding_retry_message:I = 0x7f12111a

.field public static final onboarding_retry_title:I = 0x7f12111b

.field public static final onboarding_send_reader:I = 0x7f12111c

.field public static final onboarding_shipping_confirm_have_reader_button:I = 0x7f12111d

.field public static final onboarding_shipping_confirm_have_reader_cancel_button:I = 0x7f12111e

.field public static final onboarding_shipping_confirm_have_reader_message:I = 0x7f12111f

.field public static final onboarding_shipping_confirm_have_reader_title:I = 0x7f121120

.field public static final onboarding_shipping_confirm_later:I = 0x7f121121

.field public static final onboarding_status_failure_title:I = 0x7f121122

.field public static final onboarding_status_progress_title:I = 0x7f121123

.field public static final personal_info_heading:I = 0x7f121437

.field public static final personal_info_subheading:I = 0x7f121438

.field public static final phone_number_content_description:I = 0x7f121439

.field public static final same_day_deposit:I = 0x7f12176a

.field public static final same_day_deposit_description:I = 0x7f12176b

.field public static final select_an_option:I = 0x7f1217a0

.field public static final select_an_option_detail:I = 0x7f1217a1

.field public static final setup_failed:I = 0x7f1217d9

.field public static final skip_for_now:I = 0x7f121814

.field public static final underwriting_error_explanation:I = 0x7f121ae6

.field public static final underwriting_error_title:I = 0x7f121ae7

.field public static final uppercase_header_account_holder_name:I = 0x7f121b2e

.field public static final uppercase_header_bank_account_information:I = 0x7f121b31

.field public static final uppercase_header_business_name:I = 0x7f121b34

.field public static final uppercase_header_choose_account_type:I = 0x7f121b35

.field public static final uppercase_header_date_of_birth:I = 0x7f121b38

.field public static final uppercase_header_do_you_have_ein:I = 0x7f121b39

.field public static final uppercase_header_estimated_revenue:I = 0x7f121b3a

.field public static final uppercase_header_last_4_ssn:I = 0x7f121b3b

.field public static final uppercase_header_last_4_ssn_with_itin:I = 0x7f121b3c

.field public static final uppercase_header_ssn:I = 0x7f121b3e

.field public static final uppercase_header_what_is_your_ein:I = 0x7f121b3f

.field public static final uppercase_header_your_business_address:I = 0x7f121b40

.field public static final uppercase_header_your_home_address:I = 0x7f121b41

.field public static final uppercase_header_your_legal_name:I = 0x7f121b42

.field public static final uppercase_header_your_phone_number:I = 0x7f121b43

.field public static final what_is_ein_hint:I = 0x7f121be0

.field public static final you_are_set_up_for_same_day_deposits:I = 0x7f121bf2

.field public static final you_are_set_up_for_same_day_deposits_footer:I = 0x7f121bf3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
