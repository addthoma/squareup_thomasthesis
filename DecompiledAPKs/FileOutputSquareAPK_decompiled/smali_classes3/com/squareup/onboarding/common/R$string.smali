.class public final Lcom/squareup/onboarding/common/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/common/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activation_account_title:I = 0x7f120044

.field public static final activation_call_to_action_no_free_reader:I = 0x7f120045

.field public static final add_bank_account:I = 0x7f12007a

.field public static final bank_call_to_action:I = 0x7f12015a

.field public static final complete_signup_notification_content:I = 0x7f120471

.field public static final complete_signup_notification_title:I = 0x7f120472

.field public static final finalize_account_setup:I = 0x7f120abe

.field public static final link_bank_account_notification_browser_dialog_message:I = 0x7f120ed0

.field public static final link_bank_account_notification_content:I = 0x7f120ed1

.field public static final link_bank_account_notification_title:I = 0x7f120ed2

.field public static final onboarding_activate_in_app:I = 0x7f1210ec

.field public static final onboarding_activate_message:I = 0x7f1210ed

.field public static final onboarding_get_web_link_fail:I = 0x7f121105

.field public static final world_finish_account_subtitle:I = 0x7f121be9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
