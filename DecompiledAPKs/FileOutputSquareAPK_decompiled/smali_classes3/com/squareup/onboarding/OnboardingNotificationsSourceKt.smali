.class public final Lcom/squareup/onboarding/OnboardingNotificationsSourceKt;
.super Ljava/lang/Object;
.source "OnboardingNotificationsSource.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"
    }
    d2 = {
        "COMPLETE_SIGNUP_IN_APP_ID",
        "",
        "COMPLETE_SIGNUP_ON_WEB_ID",
        "LINK_BANK_ACCOUNT_IN_APP_ID",
        "LINK_BANK_ACCOUNT_ON_WEB_ID",
        "onboarding_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final COMPLETE_SIGNUP_IN_APP_ID:Ljava/lang/String; = "onboarding-complete-signup-in-app"

.field private static final COMPLETE_SIGNUP_ON_WEB_ID:Ljava/lang/String; = "onboarding-complete-signup-on-web"

.field private static final LINK_BANK_ACCOUNT_IN_APP_ID:Ljava/lang/String; = "onboarding-link-bank-account-in-app-signup"

.field private static final LINK_BANK_ACCOUNT_ON_WEB_ID:Ljava/lang/String; = "onboarding-link-bank-account-on-web-signup"
