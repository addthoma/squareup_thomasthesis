.class public interface abstract Lcom/squareup/onboarding/OnboardingDiverter;
.super Ljava/lang/Object;
.source "OnboardingDiverter.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/OnboardingDiverter$NoOp;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\tJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingDiverter;",
        "Lmortar/Scoped;",
        "divertToOnboarding",
        "",
        "launchMode",
        "Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;",
        "maybeDivertToOnboardingOrBank",
        "shouldDivertToOnboarding",
        "",
        "NoOp",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V
.end method

.method public abstract maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V
.end method

.method public abstract shouldDivertToOnboarding()Z
.end method
