.class public final Lcom/squareup/onboarding/ActivationUrlHelper_Factory;
.super Ljava/lang/Object;
.source "ActivationUrlHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onboarding/ActivationUrlHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final progressAndFailureRxFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->resProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->activationServiceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->progressAndFailureRxFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/ActivationUrlHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;",
            ">;)",
            "Lcom/squareup/onboarding/ActivationUrlHelper_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)Lcom/squareup/onboarding/ActivationUrlHelper;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/onboarding/ActivationUrlHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/onboarding/ActivationUrlHelper;-><init>(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onboarding/ActivationUrlHelper;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->activationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onboarding/ActivationServiceHelper;

    iget-object v2, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/BrowserLauncher;

    iget-object v3, p0, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->progressAndFailureRxFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationServiceHelper;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)Lcom/squareup/onboarding/ActivationUrlHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/onboarding/ActivationUrlHelper_Factory;->get()Lcom/squareup/onboarding/ActivationUrlHelper;

    move-result-object v0

    return-object v0
.end method
