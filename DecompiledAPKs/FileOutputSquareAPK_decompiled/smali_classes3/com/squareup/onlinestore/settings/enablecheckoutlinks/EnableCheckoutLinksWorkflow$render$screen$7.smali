.class final Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;
.super Lkotlin/jvm/internal/Lambda;
.source "EnableCheckoutLinksWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->render(Lkotlin/Unit;Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;",
        "it",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;->this$0:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;->this$0:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->logCheckoutLinksEnabled(Z)V

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;->this$0:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;->logCheckoutLinksEnabled(Z)V

    .line 145
    :goto_0
    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;-><init>(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$render$screen$7;->invoke(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow$Action$EnableCheckoutLinksApiResult;

    move-result-object p1

    return-object p1
.end method
