.class public final Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;
.super Ljava/lang/Object;
.source "EnableCheckoutLinksScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnableCheckoutLinksScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnableCheckoutLinksScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner\n*L\n1#1,184:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 *2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001*B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0016\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u001cH\u0002J\u0010\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020 H\u0002J\u0010\u0010!\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\"H\u0002J\u0018\u0010#\u001a\u00020\u00132\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020&H\u0016J\u0010\u0010\'\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020\u00132\u0006\u0010$\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "enableCheckoutLinksMessageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "enableCheckoutLinksResultMessageView",
        "enableScreenContainer",
        "Landroid/view/ViewGroup;",
        "enablingScreenContainer",
        "loadingScreenContainer",
        "resultContainer",
        "showBackArrow",
        "",
        "setUpButtonPressed",
        "",
        "onUpButtonPressed",
        "Lkotlin/Function0;",
        "showEnableScreen",
        "screen",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;",
        "showEnabledScreen",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;",
        "showEnablingScreen",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;",
        "showFeatureNotAvailableScreen",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;",
        "showLoadingScreen",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;",
        "showNetworkError",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showUnableToEnableScreen",
        "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;",
        "updateVisibility",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final enableCheckoutLinksMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final enableCheckoutLinksResultMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final enableScreenContainer:Landroid/view/ViewGroup;

.field private final enablingScreenContainer:Landroid/view/ViewGroup;

.field private final loadingScreenContainer:Landroid/view/ViewGroup;

.field private final resultContainer:Landroid/view/ViewGroup;

.field private final showBackArrow:Z

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->Companion:Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    .line 33
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showBackArrow:Z

    .line 34
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    .line 36
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 37
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 43
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->enable_checkout_links_loading_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    .line 45
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->enable_checkout_links_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableScreenContainer:Landroid/view/ViewGroup;

    .line 47
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->enabling_checkout_links_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enablingScreenContainer:Landroid/view/ViewGroup;

    .line 49
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->enable_checkout_links_result_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->resultContainer:Landroid/view/ViewGroup;

    .line 53
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->enable_checkout_links_message_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 57
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/settings/impl/R$id;->enable_checkout_links_result_message_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksResultMessageView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method private final setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$setUpButtonPressed$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$setUpButtonPressed$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 167
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 169
    iget-boolean v2, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showBackArrow:Z

    if-eqz v2, :cond_0

    .line 170
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$setUpButtonPressed$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$setUpButtonPressed$$inlined$apply$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;Lkotlin/jvm/functions/Function0;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 172
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 175
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final showEnableScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;)V
    .locals 2

    .line 113
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksMessageView:Lcom/squareup/noho/NohoMessageView;

    new-instance v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$showEnableScreen$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$showEnableScreen$1;-><init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounce {\n      screen.\u2026inksButtonPressed()\n    }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final showEnabledScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;)V
    .locals 1

    .line 124
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    .line 125
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksResultMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 126
    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_enabled:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 127
    sget v0, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_enabled_help_text:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    const/4 v0, 0x0

    .line 128
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final showEnablingScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;)V
    .locals 0

    .line 120
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final showFeatureNotAvailableScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;)V
    .locals 2

    .line 157
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    .line 158
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksResultMessageView:Lcom/squareup/noho/NohoMessageView;

    const/4 v0, 0x0

    .line 159
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 160
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_feature_not_available_msg:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 161
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final showLoadingScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;)V
    .locals 0

    .line 109
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final showNetworkError(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;)V
    .locals 2

    .line 145
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksResultMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 147
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_network_error_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 148
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_could_not_enable_help_text:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 149
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_retry:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 150
    new-instance v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$showNetworkError$$inlined$with$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$showNetworkError$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounce {\n        scree\u2026ryButtonPressed()\n      }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final showUnableToEnableScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;)V
    .locals 2

    .line 133
    invoke-virtual {p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;->getOnUpButtonPressed()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->setUpButtonPressed(Lkotlin/jvm/functions/Function0;)V

    .line 134
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableCheckoutLinksResultMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 135
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_could_not_enable:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 136
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_could_not_enable_help_text:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 137
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->online_checkout_settings_enable_checkout_links:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonText(I)V

    .line 138
    new-instance v1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$showUnableToEnableScreen$$inlined$with$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner$showUnableToEnableScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounce {\n        scree\u2026ksButtonPressed()\n      }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setSecondaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final updateVisibility(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;)V
    .locals 1

    .line 78
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;

    if-eqz v0, :cond_0

    .line 79
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enablingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->resultContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto/16 :goto_1

    .line 84
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;

    if-eqz v0, :cond_1

    .line 85
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enablingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->resultContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_1

    .line 90
    :cond_1
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;

    if-eqz v0, :cond_2

    .line 91
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->resultContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enablingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_1

    .line 96
    :cond_2
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;

    if-eqz v0, :cond_3

    goto :goto_0

    .line 97
    :cond_3
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;

    if-eqz v0, :cond_4

    goto :goto_0

    .line 98
    :cond_4
    instance-of v0, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;

    if-eqz v0, :cond_5

    goto :goto_0

    .line 99
    :cond_5
    instance-of p1, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;

    if-eqz p1, :cond_6

    .line 100
    :goto_0
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enableScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->enablingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->resultContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :cond_6
    :goto_1
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;

    if-eqz p2, :cond_0

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showLoadingScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$LoadingScreen;)V

    goto :goto_0

    .line 65
    :cond_0
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;

    if-eqz p2, :cond_1

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showEnableScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnableScreen;)V

    goto :goto_0

    .line 66
    :cond_1
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;

    if-eqz p2, :cond_2

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showEnablingScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnablingScreen;)V

    goto :goto_0

    .line 67
    :cond_2
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;

    if-eqz p2, :cond_3

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showEnabledScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$EnabledScreen;)V

    goto :goto_0

    .line 68
    :cond_3
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;

    if-eqz p2, :cond_4

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showUnableToEnableScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$UnableToEnableScreen;)V

    goto :goto_0

    .line 69
    :cond_4
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;

    if-eqz p2, :cond_5

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showNetworkError(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$NetworkErrorScreen;)V

    goto :goto_0

    .line 70
    :cond_5
    instance-of p2, p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;

    if-eqz p2, :cond_6

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showFeatureNotAvailableScreen(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen$FeatureNotAvailableScreen;)V

    .line 73
    :cond_6
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->updateVisibility(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
