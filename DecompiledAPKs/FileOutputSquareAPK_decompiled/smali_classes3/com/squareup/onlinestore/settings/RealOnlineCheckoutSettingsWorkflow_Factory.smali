.class public final Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutSettingsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;-><init>(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;

    invoke-static {v0, v1, v2}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->newInstance(Lcom/squareup/onlinestore/settings/enablecheckoutlinks/EnableCheckoutLinksWorkflow;Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;)Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow_Factory;->get()Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflow;

    move-result-object v0

    return-object v0
.end method
