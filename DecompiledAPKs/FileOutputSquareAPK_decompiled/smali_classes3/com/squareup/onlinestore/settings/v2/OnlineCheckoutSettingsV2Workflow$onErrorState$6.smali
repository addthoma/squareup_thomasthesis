.class final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;
.super Lkotlin/jvm/internal/Lambda;
.source "OnlineCheckoutSettingsV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->onErrorState(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionSink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;->$actionSink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    .line 503
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;->$actionSink:Lcom/squareup/workflow/Sink;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    sget-object v2, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6$1;->INSTANCE:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$onErrorState$6$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v1, v3, v2, v4, v3}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
