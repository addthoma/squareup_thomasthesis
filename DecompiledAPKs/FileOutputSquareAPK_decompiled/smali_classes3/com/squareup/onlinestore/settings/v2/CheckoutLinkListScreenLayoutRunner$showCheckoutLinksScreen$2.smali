.class final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "CheckoutLinkListScreenLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->showCheckoutLinksScreen(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutLinkListScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutLinkListScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,323:1\n1360#2:324\n1429#2,3:325\n*E\n*S KotlinDebug\n*F\n+ 1 CheckoutLinkListScreenLayoutRunner.kt\ncom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2\n*L\n193#1:324\n193#1,3:325\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 324
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 325
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 326
    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    .line 195
    instance-of v3, v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    if-eqz v3, :cond_0

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;

    .line 196
    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v4

    .line 197
    iget-object v5, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;

    invoke-static {v5}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;->access$getMoneyFormatter$p(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner;)Lcom/squareup/text/Formatter;

    move-result-object v5

    move-object v6, v2

    check-cast v6, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    invoke-virtual {v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 198
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 199
    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result v2

    iget-object v6, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-virtual {v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getOnClickCheckoutLink()Lkotlin/jvm/functions/Function1;

    move-result-object v6

    .line 195
    invoke-direct {v3, v4, v5, v2, v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$CustomAmountLinkData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;

    goto :goto_1

    .line 201
    :cond_0
    instance-of v3, v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    if-eqz v3, :cond_1

    new-instance v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$DonationLinkData;

    .line 202
    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;->getEnabled()Z

    move-result v2

    .line 203
    iget-object v5, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-virtual {v5}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getOnClickCheckoutLink()Lkotlin/jvm/functions/Function1;

    move-result-object v5

    .line 201
    invoke-direct {v3, v4, v2, v5}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$DonationLinkData;-><init>(Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V

    check-cast v3, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow;

    .line 205
    :goto_1
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 201
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 327
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    .line 207
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getHasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 209
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;

    const/4 v3, 0x0

    new-instance v4, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;

    invoke-direct {v4, p0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$CheckoutLinkRow$LoadingNextIndicator;-><init>(ZLkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_3
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method
