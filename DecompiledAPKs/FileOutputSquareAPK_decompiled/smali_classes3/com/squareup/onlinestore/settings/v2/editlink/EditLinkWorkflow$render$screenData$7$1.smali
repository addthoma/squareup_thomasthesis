.class final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;->invoke(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
        "-",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;->$it:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;->$it:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$Success;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult$Success;->getPayLink()Lcom/squareup/onlinestore/repository/PayLink;

    move-result-object v0

    .line 95
    instance-of v1, v0, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;

    iget-object v1, v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    invoke-static {v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v1

    new-instance v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v3, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->UPDATE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v2, v3}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v1, v2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 98
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Success;

    .line 99
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/PayLink;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/PayLink;->getName()Ljava/lang/String;

    move-result-object v4

    move-object v5, v0

    check-cast v5, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;

    invoke-virtual {v5}, Lcom/squareup/onlinestore/repository/PayLink$CustomAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/PayLink;->getEnabled()Z

    move-result v0

    .line 99
    invoke-direct {v2, v3, v4, v5, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$CustomAmountLink;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    .line 98
    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Success;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    .line 97
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 105
    :cond_0
    instance-of v1, v0, Lcom/squareup/onlinestore/repository/PayLink$Donation;

    if-eqz v1, :cond_1

    .line 106
    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;

    iget-object v1, v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;

    invoke-static {v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v1

    new-instance v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;

    sget-object v3, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->UPDATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-direct {v2, v3}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V

    check-cast v2, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v1, v2}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 108
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Success;

    .line 109
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/PayLink;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/PayLink;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/squareup/onlinestore/repository/PayLink;->getEnabled()Z

    move-result v0

    invoke-direct {v2, v3, v4, v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLink$DonationLink;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v2, Lcom/squareup/onlinestore/settings/v2/CheckoutLink;

    .line 108
    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult$Success;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)V

    .line 107
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
