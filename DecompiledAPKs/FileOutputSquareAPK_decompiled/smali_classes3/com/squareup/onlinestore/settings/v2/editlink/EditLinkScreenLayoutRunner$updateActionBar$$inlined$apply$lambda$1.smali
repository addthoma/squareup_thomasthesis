.class final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditLinkScreenLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->updateActionBar(Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $editLinkInfo$inlined:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

.field final synthetic $onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic $onClickUpButton$inlined:Lkotlin/jvm/functions/Function0;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;Lkotlin/jvm/functions/Function0;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickUpButton$inlined:Lkotlin/jvm/functions/Function0;

    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$editLinkInfo$inlined:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    iput-object p4, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 172
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$editLinkInfo$inlined:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    .line 173
    instance-of v1, v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;

    .line 174
    check-cast v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

    .line 173
    invoke-static {v1, v0, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->access$onSaveCustomAmountLinkInfo(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditCustomAmountLinkInfo;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 176
    :cond_0
    instance-of v1, v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner$updateActionBar$$inlined$apply$lambda$1;->$onClickSaveButton$inlined:Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v0, v2}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;->access$onSaveDonation(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkScreenLayoutRunner;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo$EditDonationLinkInfo;Lkotlin/jvm/functions/Function1;)V

    :cond_1
    :goto_0
    return-void
.end method
