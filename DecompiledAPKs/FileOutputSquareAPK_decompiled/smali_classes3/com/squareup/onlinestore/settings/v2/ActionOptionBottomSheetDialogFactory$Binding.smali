.class public final Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;
.super Ljava/lang/Object;
.source "ActionOptionBottomSheetDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Binding"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u0018\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007JI\u0010\u0010\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00120\u00112\u0006\u0010\u0013\u001a\u00020\u00142\u0012\u0010\u0015\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u00162\u001a\u0010\u0017\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0019j\u0002`\u001a0\u0018H\u0096\u0001R\u0012\u0010\u0008\u001a\u00020\tX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u001e\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\rX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;",
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;",
        "",
        "Lcom/squareup/workflow/V2DialogBinding;",
        "factory",
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;",
        "(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;)V",
        "hint",
        "Lcom/squareup/workflow/ScreenHint;",
        "getHint",
        "()Lcom/squareup/workflow/ScreenHint;",
        "key",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "getKey",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "promiseDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;)V
    .locals 3

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 87
    const-class v1, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetScreenData;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 88
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding$1;

    invoke-direct {v2, p1}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding$1;-><init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    return-void
.end method


# virtual methods
.method public getHint()Lcom/squareup/workflow/ScreenHint;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-interface {v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    return-object v0
.end method

.method public promiseDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "contextForNewDialog"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenKey"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;->$$delegate_0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->promiseDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
