.class public final Lcom/squareup/onlinestore/common/PayLinkUtil;
.super Ljava/lang/Object;
.source "PayLinkUtil.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPayLinkUtil.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PayLinkUtil.kt\ncom/squareup/onlinestore/common/PayLinkUtil\n*L\n1#1,30:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0012R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/onlinestore/common/PayLinkUtil;",
        "",
        "()V",
        "MAX_PAY_LINK_NAME_LEN",
        "",
        "MAX_PAY_LINK_PRICE",
        "",
        "MIN_PAY_LINK_PRICE",
        "isAmountInRange",
        "",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "maxPrice",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "minPrice",
        "nameExceedsCharCountLimit",
        "name",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

.field private static final MAX_PAY_LINK_NAME_LEN:I = 0xff

.field private static final MAX_PAY_LINK_PRICE:J = 0x7a120L

.field private static final MIN_PAY_LINK_PRICE:J = 0x64L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/onlinestore/common/PayLinkUtil;

    invoke-direct {v0}, Lcom/squareup/onlinestore/common/PayLinkUtil;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final isAmountInRange(Lcom/squareup/protos/common/Money;)Z
    .locals 4

    const-string v0, "amount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    iget-object v1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v2, "currency_code"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/onlinestore/common/PayLinkUtil;->minPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    sget-object v1, Lcom/squareup/onlinestore/common/PayLinkUtil;->INSTANCE:Lcom/squareup/onlinestore/common/PayLinkUtil;

    iget-object v3, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/squareup/onlinestore/common/PayLinkUtil;->maxPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/squareup/money/MoneyMath;->inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method

.method public final maxPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/32 v0, 0x7a120

    .line 28
    invoke-static {p1, v0, v1}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public final minPrice(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x64

    .line 26
    invoke-static {p1, v0, v1}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public final nameExceedsCharCountLimit(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v0, 0xff

    if-le p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
