.class public final Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;
.super Ljava/lang/Object;
.source "OnlineStoreServiceModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c1\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\u0012\u0010\r\u001a\u00020\u00082\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000fH\u0007\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;",
        "",
        "()V",
        "provideRealCheckoutLinksRepository",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "weeblySquareSyncService",
        "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
        "multiPassAuthService",
        "Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "provideWeeblySquareSyncService",
        "serviceCreator",
        "Lcom/squareup/api/ServiceCreator;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;

    invoke-direct {v0}, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;->INSTANCE:Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideRealCheckoutLinksRepository(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weeblySquareSyncService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "multiPassAuthService"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/onlinestore/repository/RealCheckoutLinksRepository;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)V

    check-cast v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    return-object v0
.end method

.method public final provideWeeblySquareSyncService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;
    .locals 1
    .param p1    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitWeeblyUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/api/RealService;
    .end annotation

    .annotation runtime Lcom/squareup/dagger/SingleIn;
        value = Lcom/squareup/dagger/LoggedInScope;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "serviceCreator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    const-class v0, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    invoke-interface {p1, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "serviceCreator.create(We\u2026eSyncService::class.java)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    return-object p1
.end method
