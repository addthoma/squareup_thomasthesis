.class public final Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;
.super Ljava/lang/Object;
.source "OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final multiPassAuthServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;",
            ">;"
        }
    .end annotation
.end field

.field private final weeblySquareSyncServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->weeblySquareSyncServiceProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->multiPassAuthServiceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRealCheckoutLinksRepository(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;->INSTANCE:Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule;->provideRealCheckoutLinksRepository(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;
    .locals 4

    .line 43
    iget-object v0, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->weeblySquareSyncServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;

    iget-object v2, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->multiPassAuthServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;

    iget-object v3, p0, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->provideRealCheckoutLinksRepository(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onlinestore/api/squaresync/WeeblySquareSyncService;Lcom/squareup/api/multipassauth/SquareSyncMultiPassAuthService;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/onlinestore/api/OnlineStoreServiceModule_ProvideRealCheckoutLinksRepositoryFactory;->get()Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object v0

    return-object v0
.end method
