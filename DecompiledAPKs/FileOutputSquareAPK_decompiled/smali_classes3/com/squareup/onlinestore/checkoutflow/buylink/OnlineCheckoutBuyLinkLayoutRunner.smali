.class public final Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;
.super Ljava/lang/Object;
.source "OnlineCheckoutBuyLinkLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnlineCheckoutBuyLinkLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnlineCheckoutBuyLinkLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,207:1\n1103#2,7:208\n1103#2,7:215\n*E\n*S KotlinDebug\n*F\n+ 1 OnlineCheckoutBuyLinkLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner\n*L\n126#1,7:208\n138#1,7:215\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 +2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001+B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J4\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0010\u0008\u0002\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u001b2\u0010\u0008\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u001bH\u0002J\u0010\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0010\u0010 \u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020!H\u0002J\u0010\u0010\"\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020#H\u0002J\u0010\u0010$\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020%H\u0002J\u0018\u0010&\u001a\u00020\u00172\u0006\u0010\'\u001a\u00020\u00022\u0006\u0010(\u001a\u00020)H\u0016J\u0010\u0010*\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "checkoutLinkUrl",
        "Lcom/squareup/noho/NohoRow;",
        "checkoutLinksScreenContainer",
        "Landroid/view/ViewGroup;",
        "errorMessageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "errorScreenContainer",
        "helpText",
        "Lcom/squareup/widgets/MessageView;",
        "loadingScreenContainer",
        "qrCodeView",
        "Landroid/widget/ImageView;",
        "shareLink",
        "Lcom/squareup/noho/NohoButton;",
        "setupActionBar",
        "",
        "title",
        "",
        "onClickCloseButton",
        "Lkotlin/Function0;",
        "onClickNewSaleButton",
        "showCheckoutLinkScreen",
        "screenData",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;",
        "showErrorScreen",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;",
        "showFeatureNotAvailableScreen",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;",
        "showLoadingScreen",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateScreenVisibility",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CHECKOUT_LINK_URL_MAX_LINES:I = 0x2

.field private static final CHECKOUT_LINK_URL_WIDTH_PERCENT:F = 0.8f

.field public static final Companion:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

.field private final checkoutLinksScreenContainer:Landroid/view/ViewGroup;

.field private final errorMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final errorScreenContainer:Landroid/view/ViewGroup;

.field private final helpText:Lcom/squareup/widgets/MessageView;

.field private final loadingScreenContainer:Landroid/view/ViewGroup;

.field private final qrCodeView:Landroid/widget/ImageView;

.field private final shareLink:Lcom/squareup/noho/NohoButton;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->Companion:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    .line 43
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 46
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_loading_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    .line 48
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlinks_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/ViewGroup;

    .line 50
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_error_screen_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    .line 52
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlink_qr_code:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    .line 53
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_share_checkoutlink:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->shareLink:Lcom/squareup/noho/NohoButton;

    .line 54
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlink_url:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

    .line 55
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_checkoutlink_help_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    .line 58
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->online_checkout_error_message_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    return-void
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;)Landroid/view/View;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    return-object p0
.end method

.method private final setupActionBar(Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 194
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 180
    new-instance v8, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 182
    new-instance v1, Lcom/squareup/resources/FixedText;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v1, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v8, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    if-eqz p2, :cond_0

    .line 184
    sget-object p1, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v8, p1, p2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    :cond_0
    if-eqz p3, :cond_1

    .line 188
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 189
    new-instance p1, Lcom/squareup/resources/ResourceString;

    sget p2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_new_sale:I

    invoke-direct {p1, p2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v3, p1

    check-cast v3, Lcom/squareup/resources/TextModel;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, v8

    move-object v5, p3

    .line 187
    invoke-static/range {v1 .. v7}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 194
    :cond_1
    invoke-virtual {v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method static synthetic setupActionBar$default(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x0

    if-eqz p5, :cond_0

    .line 177
    move-object p2, v0

    check-cast p2, Lkotlin/jvm/functions/Function0;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 178
    move-object p3, v0

    check-cast p3, Lkotlin/jvm/functions/Function0;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->setupActionBar(Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final showCheckoutLinkScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;)V
    .locals 6

    .line 109
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;->getActionBarTitle()Ljava/lang/String;

    move-result-object v1

    .line 110
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;->getOnClickNewSaleButton()Lkotlin/jvm/functions/Function0;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, p0

    .line 108
    invoke-static/range {v0 .. v5}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->setupActionBar$default(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    sget-object v1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showCheckoutLinkScreen$1;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showCheckoutLinkScreen$1;

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 116
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;->getQrCodeBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 118
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 119
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_checkout_link_help_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->qrCodeView:Landroid/widget/ImageView;

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->helpText:Lcom/squareup/widgets/MessageView;

    sget v2, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_checkout_link_help_text_no_qr_code:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :goto_0
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinkUrl:Lcom/squareup/noho/NohoRow;

    .line 127
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;->getCheckoutLinkUrl()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    const v1, 0x3f4ccccd    # 0.8f

    .line 128
    invoke-static {v0, v1}, Lcom/squareup/noho/NohoRowUtilsKt;->constrainValueSize(Lcom/squareup/noho/NohoRow;F)V

    const/4 v1, 0x2

    .line 129
    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, v2}, Lcom/squareup/noho/NohoRowUtilsKt;->setMaxLinesForValue(Lcom/squareup/noho/NohoRow;ILandroid/text/TextUtils$TruncateAt;)V

    const v1, 0x800005

    .line 130
    invoke-static {v0, v1}, Lcom/squareup/noho/NohoRowUtilsKt;->setHorizontalGravityForValue(Lcom/squareup/noho/NohoRow;I)V

    .line 131
    check-cast v0, Landroid/view/View;

    .line 208
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showCheckoutLinkScreen$$inlined$with$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showCheckoutLinkScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->shareLink:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    .line 215
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showCheckoutLinkScreen$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showCheckoutLinkScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final showErrorScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;)V
    .locals 8

    .line 147
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "view.resources.getString\u2026ng.online_checkout_title)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;->getOnClickBackButton()Lkotlin/jvm/functions/Function0;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    .line 146
    invoke-static/range {v2 .. v7}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->setupActionBar$default(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;->getOnClickBackButton()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 153
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_error_screen_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 154
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_error_screen_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 155
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_error_screen_retry_text:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(I)V

    .line 156
    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showErrorScreen$$inlined$with$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner$showErrorScreen$$inlined$with$lambda$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    const-string v1, "debounce {\n        scree\u2026kTryAgainButton()\n      }"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonOnClickListener(Lcom/squareup/debounce/DebouncedOnClickListener;)V

    return-void
.end method

.method private final showFeatureNotAvailableScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;)V
    .locals 8

    .line 164
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "view.resources.getString\u2026ng.online_checkout_title)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;->getOnClickCloseButton()Lkotlin/jvm/functions/Function0;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    .line 163
    invoke-static/range {v2 .. v7}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->setupActionBar$default(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;->getOnClickCloseButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 168
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorMessageView:Lcom/squareup/noho/NohoMessageView;

    const/4 v0, 0x0

    .line 169
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 170
    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_feature_not_available_msg:I

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    .line 171
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final showLoadingScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;)V
    .locals 8

    .line 101
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "view.resources.getString\u2026ng.online_checkout_title)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;->getOnClickBackButton()Lkotlin/jvm/functions/Function0;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v2, p0

    .line 100
    invoke-static/range {v2 .. v7}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->setupActionBar$default(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;Ljava/lang/String;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;->getOnClickBackButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final updateScreenVisibility(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;)V
    .locals 1

    .line 76
    instance-of v0, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;

    if-eqz v0, :cond_0

    .line 77
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 81
    :cond_0
    instance-of v0, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;

    if-eqz v0, :cond_1

    .line 82
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 86
    :cond_1
    instance-of v0, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;

    if-eqz v0, :cond_2

    .line 87
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 91
    :cond_2
    instance-of p1, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;

    if-eqz p1, :cond_3

    .line 92
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->loadingScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 93
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->checkoutLinksScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->errorScreenContainer:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;

    if-eqz p2, :cond_0

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->showLoadingScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$LoadingScreenData;)V

    goto :goto_0

    .line 66
    :cond_0
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;

    if-eqz p2, :cond_1

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->showCheckoutLinkScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$CheckoutLinkScreenData;)V

    goto :goto_0

    .line 67
    :cond_1
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;

    if-eqz p2, :cond_2

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->showErrorScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$ErrorScreenData;)V

    goto :goto_0

    .line 68
    :cond_2
    instance-of p2, p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;

    if-eqz p2, :cond_3

    move-object p2, p1

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->showFeatureNotAvailableScreen(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData$FeatureNotAvailableScreenData;)V

    .line 71
    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->updateScreenVisibility(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkLayoutRunner;->showRendering(Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
