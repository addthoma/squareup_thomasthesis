.class public abstract Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutBuyLinkWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$Close;,
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$NewSale;,
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$Retry;,
        Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$HandleEnableCheckoutLinksResult;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0007\u0008\t\nB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Close",
        "HandleEnableCheckoutLinksResult",
        "NewSale",
        "Retry",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$Close;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$NewSale;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$Retry;",
        "Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$HandleEnableCheckoutLinksResult;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 96
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
            ">;)",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$Close;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;

    move-object v1, p1

    check-cast v1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    goto :goto_1

    .line 105
    :cond_0
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$NewSale;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;

    move-object v1, p1

    check-cast v1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    goto :goto_1

    .line 106
    :cond_1
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$Retry;

    if-eqz v0, :cond_2

    .line 107
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$LoadingState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$LoadingState;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 110
    :cond_2
    instance-of v0, p0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$HandleEnableCheckoutLinksResult;

    if-eqz v0, :cond_6

    .line 111
    move-object v0, p0

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$HandleEnableCheckoutLinksResult;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action$HandleEnableCheckoutLinksResult;->getResult()Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult;

    move-result-object v0

    .line 112
    sget-object v2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult$Success;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult$Success;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$CheckoutLinkState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$CheckoutLinkState;

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    goto :goto_0

    .line 113
    :cond_3
    sget-object v2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult$NotAvailable;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult$NotAvailable;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$FeatureNotAvailableState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$FeatureNotAvailableState;

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    goto :goto_0

    .line 114
    :cond_4
    sget-object v2, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$EnableCheckoutLinksWorkerResult$OtherError;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$ErrorState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState$ErrorState;

    check-cast v0, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;

    .line 111
    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    :goto_1
    return-object v1

    .line 114
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 116
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 96
    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkState;",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
