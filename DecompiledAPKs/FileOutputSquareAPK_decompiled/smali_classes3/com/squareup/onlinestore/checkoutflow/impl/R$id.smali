.class public final Lcom/squareup/onlinestore/checkoutflow/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/checkoutflow/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final checkout_link_screen_container:I = 0x7f0a0320

.field public static final checkout_link_screen_name_label:I = 0x7f0a0321

.field public static final error_msg:I = 0x7f0a0721

.field public static final error_screen_container:I = 0x7f0a0728

.field public static final loading_screen_container:I = 0x7f0a0955

.field public static final online_checkout_checkoutlink_help_text:I = 0x7f0a0aa3

.field public static final online_checkout_checkoutlink_qr_code:I = 0x7f0a0aa4

.field public static final online_checkout_checkoutlink_url:I = 0x7f0a0aa5

.field public static final online_checkout_checkoutlinks_screen_container:I = 0x7f0a0aa6

.field public static final online_checkout_error_message_view:I = 0x7f0a0aa7

.field public static final online_checkout_error_screen_container:I = 0x7f0a0aa8

.field public static final online_checkout_loading_screen_container:I = 0x7f0a0aa9

.field public static final online_checkout_pay_link_name_container:I = 0x7f0a0aaa

.field public static final online_checkout_share_checkoutlink:I = 0x7f0a0aab

.field public static final pay_link_amount_label:I = 0x7f0a0bdb

.field public static final pay_link_get_link_button:I = 0x7f0a0bdc

.field public static final pay_link_name:I = 0x7f0a0bdd


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
