.class public final Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealOnlineCheckoutTenderOptionFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/onlinestore/restrictions/OnlineStoreRestrictions;Lcom/squareup/payment/Transaction;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOnlineCheckoutTenderOptionFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOnlineCheckoutTenderOptionFactory.kt\ncom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,185:1\n32#2,12:186\n*E\n*S KotlinDebug\n*F\n+ 1 RealOnlineCheckoutTenderOptionFactory.kt\ncom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1\n*L\n156#1,12:186\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000=\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001J\u001a\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u00022\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016JN\u0010\u000e\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u00032\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0003H\u0016\u00a8\u0006\u0013"
    }
    d2 = {
        "com/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
        "Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 147
    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 186
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 191
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 193
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 194
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 195
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 197
    :cond_3
    check-cast v1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;

    if-eqz v1, :cond_4

    goto :goto_3

    .line 156
    :cond_4
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$isPayLinkEnabled(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z

    move-result p1

    if-eqz p1, :cond_5

    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$PayLinkState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$PayLinkState;

    goto :goto_2

    :cond_5
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$BuyLinkState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$BuyLinkState;

    :goto_2
    move-object v1, p1

    check-cast v1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;

    :goto_3
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 147
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->initialState(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 147
    check-cast p1, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;

    check-cast p2, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;",
            "Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;",
            "-",
            "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$BuyLinkState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$BuyLinkState;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p2}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getOnlineCheckoutBuyLinkWorkflow$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/onlinestore/checkoutflow/buylink/RealOnlineCheckoutBuyLinkWorkflow;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 166
    new-instance v2, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;

    .line 167
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p2}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getTransaction$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/payment/Transaction;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object p2

    const-string/jumbo v0, "transaction.items"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/CartItem;

    if-eqz p2, :cond_0

    iget-object p2, p2, Lcom/squareup/checkout/CartItem;->merchantCatalogObjectToken:Ljava/lang/String;

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const-string p2, ""

    .line 168
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getCheckoutLinkItemId(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {v3}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$hasEcomAvailableItemInCart(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Z

    move-result v3

    .line 166
    invoke-direct {v2, p2, p1, v0, v3}, Lcom/squareup/onlinestore/checkoutflow/buylink/OnlineCheckoutBuyLinkInput;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Z)V

    const/4 v3, 0x0

    .line 170
    new-instance p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1$render$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    .line 164
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_1

    .line 171
    :cond_1
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$PayLinkState;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState$PayLinkState;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 172
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;

    invoke-static {p2}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;->access$getOnlineCheckoutPayLinkWorkflow$p(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory;)Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    const/4 v3, 0x0

    .line 173
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1$render$2;

    invoke-direct {p2, p0}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1$render$2;-><init>(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    move-object v2, p1

    .line 171
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 147
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$workflow$1;->snapshotState(Lcom/squareup/onlinestore/checkoutflow/RealOnlineCheckoutTenderOptionFactory$OnlineCheckoutTypeState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
