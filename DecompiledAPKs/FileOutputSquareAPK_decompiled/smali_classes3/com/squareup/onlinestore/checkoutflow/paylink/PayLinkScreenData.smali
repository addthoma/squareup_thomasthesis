.class public abstract Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;
.super Ljava/lang/Object;
.source "ScreenData.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;,
        Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000b2\u00020\u0001:\u0005\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J0\u0010\u0003\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\t\u001a\u00020\u0005\u0082\u0001\u0004\u000f\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "()V",
        "toPosLayer",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "posLayering",
        "CheckoutLinkScreenData",
        "Companion",
        "ErrorScreenData",
        "FeatureNotAvailableScreenData",
        "LoadingScreenData",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$LoadingScreenData;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$CheckoutLinkScreenData;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$FeatureNotAvailableScreenData;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$ErrorScreenData;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;->Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData$Companion;

    .line 46
    const-class v0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;-><init>()V

    return-void
.end method


# virtual methods
.method public final toPosLayer(Lcom/squareup/container/PosLayering;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/PosLayering;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "posLayering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/onlinestore/checkoutflow/paylink/PayLinkScreenData;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v0, p1}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
