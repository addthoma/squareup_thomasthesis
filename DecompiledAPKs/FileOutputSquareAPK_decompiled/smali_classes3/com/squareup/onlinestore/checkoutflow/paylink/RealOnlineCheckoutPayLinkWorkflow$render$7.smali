.class final Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;->PAY_LINK_NEW_SALE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkTapEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logTap(Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$7;->$sink:Lcom/squareup/workflow/Sink;

    sget-object v1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$NewSale;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$NewSale;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
