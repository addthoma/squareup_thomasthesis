.class public Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;
.super Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;
.source "OnlineCheckoutSettingsEvents.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent$SharePayLinkAppSelectedEvent;,
        Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent$ShareDonationLinkAppSelectedEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0008\u0016\u0018\u00002\u00020\u0001:\u0002\t\nB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;",
        "name",
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;",
        "(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V",
        "description",
        "",
        "getDescription",
        "()Ljava/lang/String;",
        "ShareDonationLinkAppSelectedEvent",
        "SharePayLinkAppSelectedEvent",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final description:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Lcom/squareup/onlinestore/analytics/OnlineStoreTapEvent;-><init>()V

    .line 47
    invoke-virtual {p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->getValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;->description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEvent;->description:Ljava/lang/String;

    return-object v0
.end method
