.class public final Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;
.super Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;
.source "OnlineCheckoutBuyLinkEvents.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\t\u001a\u00020\u0003H\u00c2\u0003J\u0013\u0010\n\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0006H\u00d6\u0001R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;",
        "Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;",
        "name",
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;",
        "(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;)V",
        "description",
        "",
        "getDescription",
        "()Ljava/lang/String;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final description:Ljava/lang/String;

.field private final name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;)V
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    .line 36
    iget-object p1, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;->getValue()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->description:Ljava/lang/String;

    return-void
.end method

.method private final component1()Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;ILjava/lang/Object;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->copy(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;
    .locals 1

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;

    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    iget-object p1, p1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->description:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnlineCheckoutBuyLinkViewEvent(name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEvent;->name:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutBuyLinkViewEventName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
