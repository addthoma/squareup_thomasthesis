.class Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;
.super Landroid/graphics/drawable/Drawable;
.source "ReaderMessageBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderMessageBarView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BatteryPositionAdjustment"
.end annotation


# instance fields
.field private child:Landroid/graphics/drawable/Drawable;

.field private verticalOffset:I


# direct methods
.method constructor <init>(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .line 252
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 253
    iput-object p1, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->child:Landroid/graphics/drawable/Drawable;

    .line 254
    iput p2, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->verticalOffset:I

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 266
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 267
    iget v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->verticalOffset:I

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 268
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->child:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 269
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 2

    .line 262
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->child:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget v1, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->verticalOffset:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/squareup/messagebar/ReaderMessageBarView$BatteryPositionAdjustment;->child:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
