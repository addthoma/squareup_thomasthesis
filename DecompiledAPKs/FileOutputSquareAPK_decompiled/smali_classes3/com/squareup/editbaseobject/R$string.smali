.class public final Lcom/squareup/editbaseobject/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/editbaseobject/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final delete_item:I = 0x7f1207e9

.field public static final edit_item:I = 0x7f120908

.field public static final edit_restaurant_item:I = 0x7f12094e

.field public static final edit_retail_item:I = 0x7f12094f

.field public static final edit_service:I = 0x7f120950

.field public static final item_editing_banner_category:I = 0x7f120dd2

.field public static final item_editing_banner_discount:I = 0x7f120dd3

.field public static final item_editing_banner_inactive_category:I = 0x7f120dd4

.field public static final item_editing_banner_inactive_discount:I = 0x7f120dd5

.field public static final item_editing_banner_inactive_item:I = 0x7f120dd6

.field public static final item_editing_banner_inactive_menu_item:I = 0x7f120dd7

.field public static final item_editing_banner_inactive_mod_set:I = 0x7f120dd8

.field public static final item_editing_banner_inactive_product:I = 0x7f120dd9

.field public static final item_editing_banner_inactive_service:I = 0x7f120dda

.field public static final item_editing_banner_inactive_tax:I = 0x7f120ddb

.field public static final item_editing_banner_item:I = 0x7f120ddc

.field public static final item_editing_banner_menu_item:I = 0x7f120ddd

.field public static final item_editing_banner_mod_set:I = 0x7f120dde

.field public static final item_editing_banner_not_shared_category:I = 0x7f120ddf

.field public static final item_editing_banner_not_shared_discount:I = 0x7f120de0

.field public static final item_editing_banner_not_shared_item:I = 0x7f120de1

.field public static final item_editing_banner_not_shared_menu_item:I = 0x7f120de2

.field public static final item_editing_banner_not_shared_mod_set:I = 0x7f120de3

.field public static final item_editing_banner_not_shared_product:I = 0x7f120de4

.field public static final item_editing_banner_not_shared_service:I = 0x7f120de5

.field public static final item_editing_banner_not_shared_tax:I = 0x7f120de6

.field public static final item_editing_banner_one_other_location_category:I = 0x7f120de7

.field public static final item_editing_banner_one_other_location_discount:I = 0x7f120de8

.field public static final item_editing_banner_one_other_location_item:I = 0x7f120de9

.field public static final item_editing_banner_one_other_location_menu_item:I = 0x7f120dea

.field public static final item_editing_banner_one_other_location_mod_set:I = 0x7f120deb

.field public static final item_editing_banner_one_other_location_product:I = 0x7f120dec

.field public static final item_editing_banner_one_other_location_service:I = 0x7f120ded

.field public static final item_editing_banner_one_other_location_tax:I = 0x7f120dee

.field public static final item_editing_banner_product:I = 0x7f120def

.field public static final item_editing_banner_service:I = 0x7f120df0

.field public static final item_editing_banner_tax:I = 0x7f120df1

.field public static final item_editing_category_delete_confirmation_cancel:I = 0x7f120df2

.field public static final item_editing_category_delete_confirmation_delete:I = 0x7f120df3

.field public static final item_editing_category_delete_confirmation_maybe_empty:I = 0x7f120df4

.field public static final item_editing_category_delete_confirmation_title:I = 0x7f120df5

.field public static final item_editing_category_delete_confirmation_with_items:I = 0x7f120df6

.field public static final item_editing_changes_not_available_category:I = 0x7f120df7

.field public static final item_editing_changes_not_available_discount:I = 0x7f120df8

.field public static final item_editing_changes_not_available_item:I = 0x7f120df9

.field public static final item_editing_changes_not_available_menu_item:I = 0x7f120dfa

.field public static final item_editing_changes_not_available_mod_set:I = 0x7f120dfb

.field public static final item_editing_changes_not_available_product:I = 0x7f120dfc

.field public static final item_editing_changes_not_available_service:I = 0x7f120dfd

.field public static final item_editing_changes_not_available_tax:I = 0x7f120dfe

.field public static final item_editing_changes_saved_restored_connection:I = 0x7f120dff

.field public static final item_editing_delete_from_location_item:I = 0x7f120e02

.field public static final item_editing_delete_from_location_menu_item:I = 0x7f120e03

.field public static final item_editing_delete_from_location_mod_set:I = 0x7f120e04

.field public static final item_editing_delete_from_location_product:I = 0x7f120e05

.field public static final item_editing_delete_from_location_service:I = 0x7f120e06

.field public static final item_editing_delete_menu_item:I = 0x7f120e08

.field public static final item_editing_delete_product:I = 0x7f120e09

.field public static final item_editing_delete_service:I = 0x7f120e0a

.field public static final item_editing_read_only_details_menu_item:I = 0x7f120e0d

.field public static final item_editing_read_only_details_product:I = 0x7f120e0e

.field public static final item_editing_read_only_details_service:I = 0x7f120e0f

.field public static final item_editing_saving:I = 0x7f120e15


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
