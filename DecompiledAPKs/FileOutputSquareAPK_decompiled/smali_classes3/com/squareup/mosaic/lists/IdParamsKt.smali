.class public final Lcom/squareup/mosaic/lists/IdParamsKt;
.super Ljava/lang/Object;
.source "IdParams.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a*\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u00a8\u0006\u0006"
    }
    d2 = {
        "sameIdentityAs",
        "",
        "P",
        "Lcom/squareup/mosaic/lists/IdParams;",
        "Lcom/squareup/mosaic/core/UiModel;",
        "other",
        "mosaic-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final sameIdentityAs(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P::",
            "Lcom/squareup/mosaic/lists/IdParams;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "+TP;>;",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)Z"
        }
    .end annotation

    const-string v0, "$this$sameIdentityAs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/lists/IdParams;

    invoke-interface {v0}, Lcom/squareup/mosaic/lists/IdParams;->getItemId()I

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mosaic/lists/IdParams;

    invoke-interface {v1}, Lcom/squareup/mosaic/lists/IdParams;->getItemId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
