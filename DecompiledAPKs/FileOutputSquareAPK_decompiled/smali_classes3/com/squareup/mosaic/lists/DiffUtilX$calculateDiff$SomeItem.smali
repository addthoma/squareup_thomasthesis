.class public final Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;
.super Ljava/lang/Object;
.source "DiffUtilX.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/lists/DiffUtilX;->calculateDiff(Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;Lcom/squareup/mosaic/lists/DiffUtilX$Results;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SomeItem"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000*\u0001\u0000\u0008\u008a\u0008\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0008\u001a\u00020\u0003H\u00c6\u0003J\u001e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\nJ\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0004\u00a8\u0006\u0011"
    }
    d2 = {
        "com/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem",
        "",
        "finalPos",
        "",
        "(I)V",
        "getFinalPos",
        "()I",
        "setFinalPos",
        "component1",
        "copy",
        "(I)Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private finalPos:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    return-void
.end method

.method public synthetic constructor <init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, -0x1

    .line 62
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;-><init>(I)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;IILjava/lang/Object;)Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->copy(I)Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;
    .locals 1

    new-instance v0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;

    iget v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    iget p1, p1, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFinalPos()I
    .locals 1

    .line 62
    iget v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    return v0
.end method

.method public final setFinalPos(I)V
    .locals 0

    .line 62
    iput p1, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SomeItem(finalPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->finalPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
