.class public final Lcom/squareup/mosaic/basic/ResponsiveViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "Responsive.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/basic/ResponsiveModel<",
        "*>;",
        "Landroid/widget/FrameLayout;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nResponsive.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Responsive.kt\ncom/squareup/mosaic/basic/ResponsiveViewRef\n+ 2 ViewRef.kt\ncom/squareup/mosaic/core/ViewRefKt\n*L\n1#1,101:1\n133#2,8:102\n*E\n*S KotlinDebug\n*F\n+ 1 Responsive.kt\ncom/squareup/mosaic/basic/ResponsiveViewRef\n*L\n82#1,8:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u000f\u001a\u00020\u00102\u000c\u0010\u0011\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u0012\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\u001c\u0010\u0013\u001a\u00020\u00102\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u00022\u0006\u0010\u0014\u001a\u00020\u0008H\u0002R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0018\u0010\t\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/mosaic/basic/ResponsiveViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/basic/ResponsiveModel;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "generatedForState",
        "Lcom/squareup/mosaic/basic/ResponsiveState;",
        "subView",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "updatingContent",
        "",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "maybeUpdateContent",
        "newState",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private generatedForState:Lcom/squareup/mosaic/basic/ResponsiveState;

.field private subView:Lcom/squareup/mosaic/core/ViewRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation
.end field

.field private updatingContent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$getUpdatingContent$p(Lcom/squareup/mosaic/basic/ResponsiveViewRef;)Z
    .locals 0

    .line 33
    iget-boolean p0, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->updatingContent:Z

    return p0
.end method

.method public static final synthetic access$maybeUpdateContent(Lcom/squareup/mosaic/basic/ResponsiveViewRef;Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveState;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->maybeUpdateContent(Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveState;)V

    return-void
.end method

.method public static final synthetic access$setUpdatingContent$p(Lcom/squareup/mosaic/basic/ResponsiveViewRef;Z)V
    .locals 0

    .line 33
    iput-boolean p1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->updatingContent:Z

    return-void
.end method

.method private final maybeUpdateContent(Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveState;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/basic/ResponsiveModel<",
            "*>;",
            "Lcom/squareup/mosaic/basic/ResponsiveState;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x1

    .line 77
    iput-boolean v0, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->updatingContent:Z

    .line 78
    iget-object v1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->generatedForState:Lcom/squareup/mosaic/basic/ResponsiveState;

    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_3

    .line 79
    iput-object p2, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->generatedForState:Lcom/squareup/mosaic/basic/ResponsiveState;

    .line 80
    invoke-virtual {p1}, Lcom/squareup/mosaic/basic/ResponsiveModel;->getModelProvider$mosaic_core_release()Lkotlin/jvm/functions/Function2;

    move-result-object p1

    .line 81
    new-instance p2, Lcom/squareup/mosaic/core/OneUiModelContext;

    sget-object v0, Lcom/squareup/mosaic/basic/ResponsiveViewRef$maybeUpdateContent$newModel$1;->INSTANCE:Lcom/squareup/mosaic/basic/ResponsiveViewRef$maybeUpdateContent$newModel$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-direct {p2, v0}, Lcom/squareup/mosaic/core/OneUiModelContext;-><init>(Lkotlin/jvm/functions/Function0;)V

    iget-object v0, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->generatedForState:Lcom/squareup/mosaic/basic/ResponsiveState;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {p1, p2, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/squareup/mosaic/core/OneUiModelContext;->getModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    .line 82
    iget-object p2, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->subView:Lcom/squareup/mosaic/core/ViewRef;

    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p2, :cond_1

    .line 102
    invoke-virtual {p2, p1}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    invoke-static {p2, p1}, Lcom/squareup/mosaic/core/ViewRefKt;->castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V

    move-object p1, p2

    goto :goto_0

    .line 107
    :cond_1
    invoke-static {p1, v0}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object p1

    if-eqz p2, :cond_2

    .line 108
    invoke-virtual {p2}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p2

    .line 83
    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 84
    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 109
    :goto_0
    iput-object p1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->subView:Lcom/squareup/mosaic/core/ViewRef;

    :cond_3
    const/4 p1, 0x0

    .line 87
    iput-boolean p1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->updatingContent:Z

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 33
    check-cast p2, Lcom/squareup/mosaic/basic/ResponsiveModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/basic/ResponsiveModel;)Landroid/widget/FrameLayout;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/basic/ResponsiveModel;)Landroid/widget/FrameLayout;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/basic/ResponsiveModel<",
            "*>;)",
            "Landroid/widget/FrameLayout;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance p2, Landroid/widget/FrameLayout;

    invoke-direct {p2, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 46
    new-instance p1, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;

    invoke-direct {p1, p2, p0}, Lcom/squareup/mosaic/basic/ResponsiveViewRef$createView$$inlined$apply$lambda$1;-><init>(Landroid/widget/FrameLayout;Lcom/squareup/mosaic/basic/ResponsiveViewRef;)V

    check-cast p1, Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {p2, p1}, Landroid/widget/FrameLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/basic/ResponsiveModel<",
            "*>;",
            "Lcom/squareup/mosaic/basic/ResponsiveModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    check-cast p2, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, p2}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    const/4 p1, 0x0

    .line 98
    check-cast p1, Lcom/squareup/mosaic/basic/ResponsiveState;

    iput-object p1, p0, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->generatedForState:Lcom/squareup/mosaic/basic/ResponsiveState;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->requestLayout()V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/mosaic/basic/ResponsiveModel;

    check-cast p2, Lcom/squareup/mosaic/basic/ResponsiveModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->doBind(Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/mosaic/basic/ResponsiveModel;

    check-cast p2, Lcom/squareup/mosaic/basic/ResponsiveModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/basic/ResponsiveViewRef;->doBind(Lcom/squareup/mosaic/basic/ResponsiveModel;Lcom/squareup/mosaic/basic/ResponsiveModel;)V

    return-void
.end method
