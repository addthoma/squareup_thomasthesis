.class public abstract Lcom/squareup/mosaic/core/StandardUiModel;
.super Lcom/squareup/mosaic/core/UiModel;
.source "StandardUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/UiModel<",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u0008\u0012\u0004\u0012\u0002H\u00030\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\u001f\u0010\u0016\u001a\u00020\t2\u0017\u0010\u0017\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0008\nJ\u001f\u0010\u0018\u001a\u00020\t2\u0017\u0010\u0017\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0008\nR-\u0010\u0007\u001a\u0015\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008\u00a2\u0006\u0002\u0008\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000eR-\u0010\u000f\u001a\u0015\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t\u0018\u00010\u0008\u00a2\u0006\u0002\u0008\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u000c\"\u0004\u0008\u0011\u0010\u000eR\u0016\u0010\u0012\u001a\u00020\u00138\u0016X\u0097D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "V",
        "Landroid/view/View;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModel;",
        "()V",
        "customBlock",
        "Lkotlin/Function1;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "getCustomBlock$mosaic_core_release",
        "()Lkotlin/jvm/functions/Function1;",
        "setCustomBlock$mosaic_core_release",
        "(Lkotlin/jvm/functions/Function1;)V",
        "customOnceBlock",
        "getCustomOnceBlock$mosaic_core_release",
        "setCustomOnceBlock$mosaic_core_release",
        "styleRes",
        "",
        "getStyleRes",
        "()I",
        "custom",
        "block",
        "customOnce",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private customBlock:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TV;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private customOnceBlock:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TV;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final styleRes:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/mosaic/core/UiModel;-><init>()V

    return-void
.end method


# virtual methods
.method public final custom(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/squareup/mosaic/core/StandardUiModel;->customBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final customOnce(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lcom/squareup/mosaic/core/StandardUiModel;->customOnceBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final getCustomBlock$mosaic_core_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TV;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/mosaic/core/StandardUiModel;->customBlock:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getCustomOnceBlock$mosaic_core_release()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TV;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 20
    iget-object v0, p0, Lcom/squareup/mosaic/core/StandardUiModel;->customOnceBlock:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public getStyleRes()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/squareup/mosaic/core/StandardUiModel;->styleRes:I

    return v0
.end method

.method public final setCustomBlock$mosaic_core_release(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 17
    iput-object p1, p0, Lcom/squareup/mosaic/core/StandardUiModel;->customBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setCustomOnceBlock$mosaic_core_release(Lkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TV;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 20
    iput-object p1, p0, Lcom/squareup/mosaic/core/StandardUiModel;->customOnceBlock:Lkotlin/jvm/functions/Function1;

    return-void
.end method
