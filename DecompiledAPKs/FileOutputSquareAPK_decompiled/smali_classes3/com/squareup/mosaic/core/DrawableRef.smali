.class public abstract Lcom/squareup/mosaic/core/DrawableRef;
.super Ljava/lang/Object;
.source "DrawableRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M:",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "D:",
        "Landroid/graphics/drawable/Drawable;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDrawableRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DrawableRef.kt\ncom/squareup/mosaic/core/DrawableRef\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\u0013\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u000fJ\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u0002J\u001d\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00028\u00002\u0006\u0010\u000e\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\u0014J\u0015\u0010\u0015\u001a\u00020\r2\u0006\u0010\n\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u000fR\u0012\u0010\u0007\u001a\u00028\u0001X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u0004\u0018\u00018\u0000X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "M",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "D",
        "Landroid/graphics/drawable/Drawable;",
        "",
        "()V",
        "drawable",
        "getDrawable",
        "()Landroid/graphics/drawable/Drawable;",
        "model",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "bind",
        "",
        "newModel",
        "(Lcom/squareup/mosaic/core/DrawableModel;)V",
        "canAccept",
        "",
        "canUpdateTo",
        "currentModel",
        "(Lcom/squareup/mosaic/core/DrawableModel;Lcom/squareup/mosaic/core/DrawableModel;)Z",
        "doBind",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private model:Lcom/squareup/mosaic/core/DrawableModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TM;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iput-object p1, p0, Lcom/squareup/mosaic/core/DrawableRef;->model:Lcom/squareup/mosaic/core/DrawableModel;

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/mosaic/core/DrawableRef;->doBind(Lcom/squareup/mosaic/core/DrawableModel;)V

    return-void
.end method

.method public final canAccept(Lcom/squareup/mosaic/core/DrawableModel;)Z
    .locals 4

    const-string v0, "newModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/mosaic/core/DrawableRef;->model:Lcom/squareup/mosaic/core/DrawableModel;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/mosaic/core/DrawableRef;->canUpdateTo(Lcom/squareup/mosaic/core/DrawableModel;Lcom/squareup/mosaic/core/DrawableModel;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method protected canUpdateTo(Lcom/squareup/mosaic/core/DrawableModel;Lcom/squareup/mosaic/core/DrawableModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;TM;)Z"
        }
    .end annotation

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newModel"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x1

    return p1
.end method

.method public abstract doBind(Lcom/squareup/mosaic/core/DrawableModel;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)V"
        }
    .end annotation
.end method

.method public abstract getDrawable()Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation
.end method
