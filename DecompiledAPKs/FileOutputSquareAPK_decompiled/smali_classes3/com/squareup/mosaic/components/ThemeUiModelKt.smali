.class public final Lcom/squareup/mosaic/components/ThemeUiModelKt;
.super Ljava/lang/Object;
.source "ThemeUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nThemeUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ThemeUiModel.kt\ncom/squareup/mosaic/components/ThemeUiModelKt\n*L\n1#1,49:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aF\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u001d\u0010\u0007\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\u0004\u0012\u0004\u0012\u00020\u00010\u0008\u00a2\u0006\u0002\u0008\tH\u0086\u0008\u00a8\u0006\n"
    }
    d2 = {
        "theme",
        "",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "themeId",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final theme(Lcom/squareup/mosaic/core/UiModelContext;ILkotlin/jvm/functions/Function1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "TP;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$theme"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/mosaic/components/ThemeUiModel;

    invoke-interface {p0}, Lcom/squareup/mosaic/core/UiModelContext;->createParams()Ljava/lang/Object;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, v0

    move v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/mosaic/components/ThemeUiModel;-><init>(Ljava/lang/Object;ILcom/squareup/mosaic/core/UiModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 19
    invoke-interface {p2, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    invoke-virtual {v0}, Lcom/squareup/mosaic/components/ThemeUiModel;->checkInitialized()V

    .line 21
    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/UiModelContext;->add(Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
