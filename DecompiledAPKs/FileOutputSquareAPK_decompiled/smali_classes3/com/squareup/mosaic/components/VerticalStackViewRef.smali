.class public final Lcom/squareup/mosaic/components/VerticalStackViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "VerticalStackViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel<",
        "*>;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerticalStackViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerticalStackViewRef.kt\ncom/squareup/mosaic/components/VerticalStackViewRef\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000U\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0011\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0014\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u0017\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u0018\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u001dH\u0016J\u0008\u0010\u001e\u001a\u00020\u001fH\u0002R\"\u0010\u0007\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\t0\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR \u0010\u000c\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000f0\u000e\u0012\u0004\u0012\u00020\u000f0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0012\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalStackViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "children",
        "Lkotlin/sequences/Sequence;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "getChildren",
        "()Lkotlin/sequences/Sequence;",
        "viewList",
        "Lcom/squareup/mosaic/lists/ModelsList;",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
        "viewListChanges",
        "com/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1",
        "Lcom/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1;",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "Lcom/squareup/mosaic/lists/ModelsList$State;",
        "verticalItemLayout",
        "Landroid/widget/LinearLayout$LayoutParams;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final viewList:Lcom/squareup/mosaic/lists/ModelsList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/lists/ModelsList<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
            ">;",
            "Lcom/squareup/mosaic/components/VerticalStackUiModel$Params;",
            ">;"
        }
    .end annotation
.end field

.field private final viewListChanges:Lcom/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Lcom/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1;-><init>(Lcom/squareup/mosaic/components/VerticalStackViewRef;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewListChanges:Lcom/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1;

    .line 99
    new-instance v0, Lcom/squareup/mosaic/lists/ModelsList;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewListChanges:Lcom/squareup/mosaic/components/VerticalStackViewRef$viewListChanges$1;

    check-cast v1, Lcom/squareup/mosaic/lists/ModelsList$Changes;

    invoke-direct {v0, p1, v1}, Lcom/squareup/mosaic/lists/ModelsList;-><init>(Landroid/content/Context;Lcom/squareup/mosaic/lists/ModelsList$Changes;)V

    iput-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    return-void
.end method

.method public static final synthetic access$verticalItemLayout(Lcom/squareup/mosaic/components/VerticalStackViewRef;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/mosaic/components/VerticalStackViewRef;->verticalItemLayout()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object p0

    return-object p0
.end method

.method private final verticalItemLayout()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .line 101
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 14
    check-cast p2, Lcom/squareup/mosaic/components/VerticalStackUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalStackViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/VerticalStackUiModel;)Landroid/widget/LinearLayout;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/VerticalStackUiModel;)Landroid/widget/LinearLayout;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/VerticalStackUiModel<",
            "*>;)",
            "Landroid/widget/LinearLayout;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance p2, Landroid/widget/LinearLayout;

    invoke-direct {p2, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 23
    invoke-virtual {p2, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/VerticalStackUiModel;Lcom/squareup/mosaic/components/VerticalStackUiModel;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/VerticalStackUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/VerticalStackUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    check-cast p1, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v0, p2

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, p1, v0}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 34
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getHorizontalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v0

    .line 35
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getVerticalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v1

    .line 36
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getHorizontalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v3

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v3

    .line 37
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getVerticalPadding()Lcom/squareup/resources/DimenModel;

    move-result-object v4

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/squareup/resources/DimenModel;->toOffset(Landroid/content/Context;)I

    move-result v2

    .line 33
    invoke-virtual {p1, v0, v1, v3, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 41
    iget-object p1, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/VerticalStackUiModel;->getSubModels()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/mosaic/lists/ModelsList;->bind(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/mosaic/components/VerticalStackUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/VerticalStackUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalStackViewRef;->doBind(Lcom/squareup/mosaic/components/VerticalStackUiModel;Lcom/squareup/mosaic/components/VerticalStackUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/mosaic/components/VerticalStackUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/VerticalStackUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/VerticalStackViewRef;->doBind(Lcom/squareup/mosaic/components/VerticalStackUiModel;Lcom/squareup/mosaic/components/VerticalStackUiModel;)V

    return-void
.end method

.method public getChildren()Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/sequences/Sequence<",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->getSubViews()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v0

    return-object v0
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0, p1}, Lcom/squareup/mosaic/lists/ModelsList;->restoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public bridge synthetic saveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalStackViewRef;->saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackViewRef;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;

    move-result-object v0

    return-object v0
.end method
