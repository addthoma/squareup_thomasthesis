.class public final Lcom/squareup/mosaic/components/LabelViewRef;
.super Lcom/squareup/mosaic/core/StandardViewRef;
.source "LabelViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/StandardViewRef<",
        "Lcom/squareup/mosaic/components/LabelUiModel<",
        "*>;",
        "Lcom/squareup/noho/NohoLabel;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLabelViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LabelViewRef.kt\ncom/squareup/mosaic/components/LabelViewRef\n+ 2 ViewRefUtils.kt\ncom/squareup/mosaic/components/ViewRefUtilsKt\n*L\n1#1,29:1\n15#2,2:30\n15#2,2:32\n*E\n*S KotlinDebug\n*F\n+ 1 LabelViewRef.kt\ncom/squareup/mosaic/components/LabelViewRef\n*L\n20#1,2:30\n24#1,2:32\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\n\u0010\u0008\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\"\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000c\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/LabelViewRef;",
        "Lcom/squareup/mosaic/core/StandardViewRef;",
        "Lcom/squareup/mosaic/components/LabelUiModel;",
        "Lcom/squareup/noho/NohoLabel;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "createView",
        "model",
        "doBind",
        "",
        "oldModel",
        "newModel",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/core/StandardViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic createView(Landroid/content/Context;Lcom/squareup/mosaic/core/StandardUiModel;)Landroid/view/View;
    .locals 0

    .line 7
    check-cast p2, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/LabelViewRef;->createView(Landroid/content/Context;Lcom/squareup/mosaic/components/LabelUiModel;)Lcom/squareup/noho/NohoLabel;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method public createView(Landroid/content/Context;Lcom/squareup/mosaic/components/LabelUiModel;)Lcom/squareup/noho/NohoLabel;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/mosaic/components/LabelUiModel<",
            "*>;)",
            "Lcom/squareup/noho/NohoLabel;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v0

    if-nez v0, :cond_0

    .line 11
    new-instance p2, Lcom/squareup/noho/NohoLabel;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xe

    const/4 v7, 0x0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 13
    :cond_0
    new-instance v0, Lcom/squareup/noho/NohoLabel;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p2}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result p2

    invoke-direct {v0, p1, v1, v2, p2}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    move-object p2, v0

    :goto_0
    return-object p2
.end method

.method public doBind(Lcom/squareup/mosaic/components/LabelUiModel;Lcom/squareup/mosaic/components/LabelUiModel;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/LabelUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/LabelUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    move-object v0, p1

    check-cast v0, Lcom/squareup/mosaic/core/StandardUiModel;

    move-object v1, p2

    check-cast v1, Lcom/squareup/mosaic/core/StandardUiModel;

    invoke-super {p0, v0, v1}, Lcom/squareup/mosaic/core/StandardViewRef;->doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V

    .line 18
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/LabelUiModel;->getStyleRes()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 20
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/LabelUiModel;->getType()Lcom/squareup/noho/NohoLabel$Type;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/LabelUiModel;->getType()Lcom/squareup/noho/NohoLabel$Type;

    move-result-object v2

    .line 30
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    .line 21
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/LabelUiModel;->getType()Lcom/squareup/noho/NohoLabel$Type;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoLabel;->apply(Lcom/squareup/noho/NohoLabel$Type;)V

    :cond_1
    if-eqz p1, :cond_2

    .line 24
    invoke-virtual {p1}, Lcom/squareup/mosaic/components/LabelUiModel;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v1

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/LabelUiModel;->getText()Lcom/squareup/resources/TextModel;

    move-result-object p1

    .line 32
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_3

    .line 25
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelViewRef;->getAndroidView()Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/LabelViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "androidView.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/StandardUiModel;Lcom/squareup/mosaic/core/StandardUiModel;)V
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/mosaic/components/LabelUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/LabelViewRef;->doBind(Lcom/squareup/mosaic/components/LabelUiModel;Lcom/squareup/mosaic/components/LabelUiModel;)V

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 7
    check-cast p1, Lcom/squareup/mosaic/components/LabelUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/LabelUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/LabelViewRef;->doBind(Lcom/squareup/mosaic/components/LabelUiModel;Lcom/squareup/mosaic/components/LabelUiModel;)V

    return-void
.end method
