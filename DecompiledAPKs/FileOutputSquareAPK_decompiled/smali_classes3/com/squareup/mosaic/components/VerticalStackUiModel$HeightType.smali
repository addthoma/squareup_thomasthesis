.class public final enum Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;
.super Ljava/lang/Enum;
.source "VerticalStackUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mosaic/components/VerticalStackUiModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HeightType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0008\u0087\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0008\u0002\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\u0006R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;",
        "",
        "configure",
        "Lkotlin/Function1;",
        "Landroid/widget/LinearLayout$LayoutParams;",
        "",
        "(Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V",
        "getConfigure",
        "()Lkotlin/jvm/functions/Function1;",
        "NORMAL",
        "EXTEND",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

.field public static final enum EXTEND:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

.field public static final enum NORMAL:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;


# instance fields
.field private final configure:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/widget/LinearLayout$LayoutParams;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    new-instance v1, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    .line 119
    sget-object v2, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType$1;->INSTANCE:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const-string v4, "NORMAL"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;-><init>(Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V

    sput-object v1, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->NORMAL:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    .line 123
    sget-object v2, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType$2;->INSTANCE:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x1

    const-string v4, "EXTEND"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;-><init>(Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V

    sput-object v1, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->EXTEND:Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->$VALUES:[Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/widget/LinearLayout$LayoutParams;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->configure:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;
    .locals 1

    const-class v0, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;
    .locals 1

    sget-object v0, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->$VALUES:[Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    invoke-virtual {v0}, [Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;

    return-object v0
.end method


# virtual methods
.method public final getConfigure()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/widget/LinearLayout$LayoutParams;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalStackUiModel$HeightType;->configure:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
