.class public final Lcom/squareup/mosaic/drawables/NotificationDrawableModelKt;
.super Ljava/lang/Object;
.source "NotificationDrawableModel.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationDrawableModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationDrawableModel.kt\ncom/squareup/mosaic/drawables/NotificationDrawableModelKt\n*L\n1#1,37:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a#\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0017\u0010\u0003\u001a\u0013\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00010\u0004\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "notification",
        "",
        "Lcom/squareup/mosaic/core/DrawableModelContext;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/mosaic/drawables/NotificationDrawableModel;",
        "Lkotlin/ExtensionFunctionType;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final notification(Lcom/squareup/mosaic/core/DrawableModelContext;Lkotlin/jvm/functions/Function1;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/DrawableModelContext;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/mosaic/drawables/NotificationDrawableModel;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$notification"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/mosaic/drawables/NotificationDrawableModel;-><init>(IIILjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/mosaic/core/DrawableModel;

    invoke-interface {p0, v0}, Lcom/squareup/mosaic/core/DrawableModelContext;->add(Lcom/squareup/mosaic/core/DrawableModel;)V

    return-void
.end method
