.class public abstract Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;
.super Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;
.source "LoyaltyServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DecrementPointsReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$PointsAccidentallyAwarded;,
        Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;,
        Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$SuspiciousActivity;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\t\n\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;",
        "proto",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;",
        "(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;)V",
        "getProto",
        "()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;",
        "toProtoId",
        "",
        "PointsAccidentallyAwarded",
        "SuspiciousActivity",
        "TransactionRefunded",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$PointsAccidentallyAwarded;",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$TransactionRefunded;",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason$SuspiciousActivity;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;


# direct methods
.method private constructor <init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;)V
    .locals 1

    const/4 v0, 0x0

    .line 125
    invoke-direct {p0, v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;->proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 123
    invoke-direct {p0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;-><init>(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;)V

    return-void
.end method


# virtual methods
.method public final getProto()Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;->proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    return-object v0
.end method

.method public toProtoId()I
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyServiceHelper$AdjustPointsReason$DecrementPointsReason;->proto:Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;

    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest$ManualDecrementReason;->getValue()I

    move-result v0

    return v0
.end method
