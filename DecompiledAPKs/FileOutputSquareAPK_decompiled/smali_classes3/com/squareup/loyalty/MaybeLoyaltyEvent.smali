.class public abstract Lcom/squareup/loyalty/MaybeLoyaltyEvent;
.super Ljava/lang/Object;
.source "MaybeLoyaltyEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;,
        Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;,
        Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000c2\u00020\u0001:\u0003\u000c\r\u000eB\u001b\u0008\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0003R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u0082\u0001\u0002\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
        "",
        "tenderIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "billIdPair",
        "(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V",
        "getBillIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "getTenderIdPair",
        "matchesOtherTenderIdPair",
        "",
        "other",
        "Companion",
        "LoyaltyEvent",
        "NonLoyaltyEvent",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;


# instance fields
.field private final billIdPair:Lcom/squareup/protos/client/IdPair;

.field private final tenderIdPair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->Companion:Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    iput-object p2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyalty/MaybeLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;)V

    return-void
.end method

.method public static final createLoyaltyEventFromLoyaltyStatus(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;Ljava/lang/String;ZZ)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
    .locals 11
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->Companion:Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-virtual/range {v0 .. v10}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$Companion;->createLoyaltyEventFromLoyaltyStatus(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/loyalty/LoyaltyStatus;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;Ljava/lang/String;ZZ)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBillIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getTenderIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final matchesOtherTenderIdPair(Lcom/squareup/protos/client/IdPair;)Z
    .locals 2

    .line 30
    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_1
    if-eqz p1, :cond_2

    iget-object v1, p1, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    :cond_2
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    const/4 p1, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    :goto_2
    return p1
.end method
