.class public final Lcom/squareup/loyalty/ui/RewardAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "RewardAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/loyalty/ui/ViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRewardAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RewardAdapter.kt\ncom/squareup/loyalty/ui/RewardAdapter\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,113:1\n1103#2,7:114\n1103#2,7:121\n*E\n*S KotlinDebug\n*F\n+ 1 RewardAdapter.kt\ncom/squareup/loyalty/ui/RewardAdapter\n*L\n85#1,7:114\n85#1,7:121\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "Use RewardRecyclerViewHelper when feature flag [DISCOUNT_APPLY_MULTIPLE_COUPONS] is on"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00022\u0006\u0010\u0011\u001a\u00020\rH\u0016J\u0018\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\rH\u0016R0\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/loyalty/ui/RewardAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/loyalty/ui/ViewHolder;",
        "()V",
        "value",
        "",
        "Lcom/squareup/loyalty/ui/RewardWrapper;",
        "items",
        "getItems",
        "()Ljava/util/List;",
        "setItems",
        "(Ljava/util/List;)V",
        "getItemCount",
        "",
        "onBindViewHolder",
        "",
        "holder",
        "position",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 62
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 64
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapter;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapter;->items:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/loyalty/ui/ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyalty/ui/RewardAdapter;->onBindViewHolder(Lcom/squareup/loyalty/ui/ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/loyalty/ui/ViewHolder;I)V
    .locals 4

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/loyalty/ui/RewardAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/loyalty/ui/RewardWrapper;

    .line 85
    iget-object p1, p1, Lcom/squareup/loyalty/ui/ViewHolder;->itemView:Landroid/view/View;

    if-eqz p1, :cond_1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v0, 0x1

    .line 86
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    .line 87
    invoke-virtual {p2}, Lcom/squareup/loyalty/ui/RewardWrapper;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 89
    invoke-virtual {p2}, Lcom/squareup/loyalty/ui/RewardWrapper;->getValue()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 93
    invoke-virtual {p2}, Lcom/squareup/loyalty/ui/RewardWrapper;->getType()Lcom/squareup/loyalty/ui/RewardWrapper$Type;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/loyalty/ui/RewardWrapper$Type$Clickable;

    const-string v2, "endGlyph"

    if-eqz v1, :cond_0

    .line 94
    move-object v1, p1

    check-cast v1, Landroid/view/View;

    .line 114
    new-instance v3, Lcom/squareup/loyalty/ui/RewardAdapter$onBindViewHolder$$inlined$apply$lambda$1;

    invoke-direct {v3, p2}, Lcom/squareup/loyalty/ui/RewardAdapter$onBindViewHolder$$inlined$apply$lambda$1;-><init>(Lcom/squareup/loyalty/ui/RewardWrapper;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setEnabled(Z)V

    .line 96
    sget p2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    .line 97
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 98
    sget p2, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p2

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    goto :goto_0

    .line 101
    :cond_0
    move-object p2, p1

    check-cast p2, Landroid/view/View;

    .line 121
    new-instance v0, Lcom/squareup/loyalty/ui/RewardAdapter$$special$$inlined$onClickDebounced$2;

    invoke-direct {v0}, Lcom/squareup/loyalty/ui/RewardAdapter$$special$$inlined$onClickDebounced$2;-><init>()V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 p2, 0x0

    .line 102
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setEnabled(Z)V

    .line 103
    sget p2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    .line 104
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 105
    sget p2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 106
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getEndGlyph()Lcom/squareup/glyph/SquareGlyphView;

    move-result-object p2

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyphColor(I)V

    :goto_0
    return-void

    .line 85
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.account.view.SmartLineRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 62
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyalty/ui/RewardAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/loyalty/ui/ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/loyalty/ui/ViewHolder;
    .locals 3

    const-string p2, "parent"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    new-instance p2, Lcom/squareup/loyalty/ui/ViewHolder;

    .line 75
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 77
    sget v1, Lcom/squareup/loyalty/R$layout;->crm_reward_row:I

    const/4 v2, 0x0

    .line 76
    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 74
    invoke-direct {p2, p1}, Lcom/squareup/loyalty/ui/ViewHolder;-><init>(Lcom/squareup/ui/account/view/SmartLineRow;)V

    return-object p2

    .line 76
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.account.view.SmartLineRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/loyalty/ui/RewardWrapper;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/loyalty/ui/RewardAdapter;->items:Ljava/util/List;

    .line 67
    invoke-virtual {p0}, Lcom/squareup/loyalty/ui/RewardAdapter;->notifyDataSetChanged()V

    return-void
.end method
