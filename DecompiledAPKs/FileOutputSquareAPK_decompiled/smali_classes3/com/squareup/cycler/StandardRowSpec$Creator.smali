.class public final Lcom/squareup/cycler/StandardRowSpec$Creator;
.super Ljava/lang/Object;
.source "StandardRowSpec.kt"


# annotations
.annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cycler/StandardRowSpec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Creator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "S::TI;V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0010\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u0000*\u0008\u0008\u0003\u0010\u0001*\u00020\u0002*\u0008\u0008\u0004\u0010\u0003*\u0002H\u0001*\u0008\u0008\u0005\u0010\u0004*\u00020\u00052\u00020\u0002B\r\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u001f\u0010\u0018\u001a\u00020\u000c2\u0014\u0008\u0004\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00020\u000c0\u001aH\u0086\u0008J \u0010\u0018\u001a\u00020\u000c2\u0018\u0010\u0019\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00020\u000c0\nRB\u0010\r\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00020\u000c0\n2\u0018\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00028\u0004\u0012\u0004\u0012\u00020\u000c0\n@BX\u0080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u00028\u0005X\u0086.\u00a2\u0006\u0010\n\u0002\u0010\u0017\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "(Lcom/squareup/cycler/Recycler$CreatorContext;)V",
        "<set-?>",
        "Lkotlin/Function2;",
        "",
        "",
        "bindBlock",
        "getBindBlock$lib_release",
        "()Lkotlin/jvm/functions/Function2;",
        "getCreatorContext",
        "()Lcom/squareup/cycler/Recycler$CreatorContext;",
        "view",
        "getView",
        "()Landroid/view/View;",
        "setView",
        "(Landroid/view/View;)V",
        "Landroid/view/View;",
        "bind",
        "block",
        "Lkotlin/Function1;",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private bindBlock:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

.field public view:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/Recycler$CreatorContext;)V
    .locals 1

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

    .line 59
    sget-object p1, Lcom/squareup/cycler/StandardRowSpec$Creator$bindBlock$1;->INSTANCE:Lcom/squareup/cycler/StandardRowSpec$Creator$bindBlock$1;

    check-cast p1, Lkotlin/jvm/functions/Function2;

    iput-object p1, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->bindBlock:Lkotlin/jvm/functions/Function2;

    return-void
.end method


# virtual methods
.method public final bind(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec$Creator$bind$1;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/StandardRowSpec$Creator$bind$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final bind(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->bindBlock:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final getBindBlock$lib_release()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Ljava/lang/Integer;",
            "TS;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->bindBlock:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getCreatorContext()Lcom/squareup/cycler/Recycler$CreatorContext;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->creatorContext:Lcom/squareup/cycler/Recycler$CreatorContext;

    return-object v0
.end method

.method public final getView()Landroid/view/View;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->view:Landroid/view/View;

    if-nez v0, :cond_0

    const-string/jumbo v1, "view"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final setView(Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iput-object p1, p0, Lcom/squareup/cycler/StandardRowSpec$Creator;->view:Landroid/view/View;

    return-void
.end method
