.class public final Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;
.super Lkotlin/jvm/internal/Lambda;
.source "MosaicRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cycler/mosaic/MosaicRowSpecKt;->mosaicRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function3;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TI;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$3\n*L\n1#1,198:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "it",
        "invoke",
        "(Ljava/lang/Object;)Z",
        "com/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;

    invoke-direct {v0}, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;-><init>()V

    sput-object v0, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;->INSTANCE:Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/cycler/mosaic/MosaicRowSpecKt$mosaicRow$$inlined$mosaicRow$2;->invoke(Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    const-string v1, "S"

    .line 88
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    instance-of p1, p1, Ljava/lang/Object;

    return p1
.end method
