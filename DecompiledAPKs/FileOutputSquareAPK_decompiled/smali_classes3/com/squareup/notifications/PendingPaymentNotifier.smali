.class public interface abstract Lcom/squareup/notifications/PendingPaymentNotifier;
.super Ljava/lang/Object;
.source "PendingPaymentNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notifications/PendingPaymentNotifier$NoPendingPaymentNotifier;
    }
.end annotation


# virtual methods
.method public abstract hideNotification()V
.end method

.method public abstract showNotification(Ljava/lang/Integer;)V
.end method
