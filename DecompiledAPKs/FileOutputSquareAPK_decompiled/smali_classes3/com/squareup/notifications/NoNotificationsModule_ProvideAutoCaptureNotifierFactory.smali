.class public final Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory;
.super Ljava/lang/Object;
.source "NoNotificationsModule_ProvideAutoCaptureNotifierFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notifications/AutoCaptureNotifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory$InstanceHolder;->access$000()Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideAutoCaptureNotifier()Lcom/squareup/notifications/AutoCaptureNotifier;
    .locals 2

    .line 22
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule;->provideAutoCaptureNotifier()Lcom/squareup/notifications/AutoCaptureNotifier;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/notifications/AutoCaptureNotifier;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notifications/AutoCaptureNotifier;
    .locals 1

    .line 14
    invoke-static {}, Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory;->provideAutoCaptureNotifier()Lcom/squareup/notifications/AutoCaptureNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/notifications/NoNotificationsModule_ProvideAutoCaptureNotifierFactory;->get()Lcom/squareup/notifications/AutoCaptureNotifier;

    move-result-object v0

    return-object v0
.end method
