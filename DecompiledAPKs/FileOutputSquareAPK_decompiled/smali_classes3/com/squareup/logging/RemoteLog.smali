.class public final Lcom/squareup/logging/RemoteLog;
.super Ljava/lang/Object;
.source "RemoteLog.java"


# static fields
.field static logger:Lcom/squareup/logging/RemoteLogger;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static w(Ljava/lang/Throwable;)V
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/logging/RemoteLog;->logger:Lcom/squareup/logging/RemoteLogger;

    if-nez v0, :cond_0

    return-void

    .line 12
    :cond_0
    invoke-interface {v0, p0}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method public static w(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/logging/RemoteLog;->logger:Lcom/squareup/logging/RemoteLogger;

    if-nez v0, :cond_0

    return-void

    .line 20
    :cond_0
    invoke-interface {v0, p0, p1}, Lcom/squareup/logging/RemoteLogger;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method
