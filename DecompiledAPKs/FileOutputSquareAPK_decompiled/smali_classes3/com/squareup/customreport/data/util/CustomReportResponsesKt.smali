.class public final Lcom/squareup/customreport/data/util/CustomReportResponsesKt;
.super Ljava/lang/Object;
.source "CustomReportResponses.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomReportResponses.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomReportResponses.kt\ncom/squareup/customreport/data/util/CustomReportResponsesKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,74:1\n704#2:75\n777#2,2:76\n1360#2:78\n1429#2,3:79\n*E\n*S KotlinDebug\n*F\n+ 1 CustomReportResponses.kt\ncom/squareup/customreport/data/util/CustomReportResponsesKt\n*L\n23#1:75\n23#1,2:76\n72#1:78\n72#1,3:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\"\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0002\u001a\u0018\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0001*\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b\u001a$\u0010\u000c\u001a\u00020\r*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u00a8\u0006\u000f"
    }
    d2 = {
        "getChartedSales",
        "",
        "Lcom/squareup/customreport/data/ChartedSale;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "zoneId",
        "Lorg/threeten/bp/ZoneId;",
        "chartedSalesReportType",
        "Lcom/squareup/customreport/data/SalesReportType$Charted;",
        "getReportsByGroupType",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "groupByType",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
        "toSalesChartReport",
        "Lcom/squareup/customreport/data/SalesChartReport;",
        "comparisonResponse",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final getChartedSales(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lorg/threeten/bp/ZoneId;Lcom/squareup/customreport/data/SalesReportType$Charted;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Lorg/threeten/bp/ZoneId;",
            "Lcom/squareup/customreport/data/SalesReportType$Charted;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/customreport/data/ChartedSale;",
            ">;"
        }
    .end annotation

    .line 71
    invoke-virtual {p2}, Lcom/squareup/customreport/data/SalesReportType$Charted;->getGroupByType()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    move-result-object p2

    invoke-static {p0, p2}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getReportsByGroupType(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 78
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p0, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 79
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 80
    check-cast v0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    .line 72
    invoke-static {v0, p1}, Lcom/squareup/customreport/data/util/CustomReportsKt;->toChartedSale(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;Lorg/threeten/bp/ZoneId;)Lcom/squareup/customreport/data/ChartedSale;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_0
    check-cast p2, Ljava/util/List;

    return-object p2
.end method

.method public static final getReportsByGroupType(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$getReportsByGroupType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "groupByType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    iget-object p0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;->custom_report:Ljava/util/List;

    const-string v0, "custom_report"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Iterable;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 76
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    if-eqz v2, :cond_1

    .line 23
    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-ne v2, p1, :cond_2

    const/4 v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 77
    :cond_3
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final toSalesChartReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lorg/threeten/bp/ZoneId;Lcom/squareup/customreport/data/SalesReportType$Charted;Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;)Lcom/squareup/customreport/data/SalesChartReport;
    .locals 1

    const-string v0, "$this$toSalesChartReport"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "zoneId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartedSalesReportType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {p0, p1, p2}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getChartedSales(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lorg/threeten/bp/ZoneId;Lcom/squareup/customreport/data/SalesReportType$Charted;)Ljava/util/List;

    move-result-object p0

    .line 46
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    sget-object p0, Lcom/squareup/customreport/data/NoSalesChartReport;->INSTANCE:Lcom/squareup/customreport/data/NoSalesChartReport;

    check-cast p0, Lcom/squareup/customreport/data/SalesChartReport;

    goto :goto_1

    .line 49
    :cond_0
    new-instance v0, Lcom/squareup/customreport/data/WithSalesChartReport;

    if-eqz p3, :cond_1

    .line 54
    invoke-static {p3, p1, p2}, Lcom/squareup/customreport/data/util/CustomReportResponsesKt;->getChartedSales(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;Lorg/threeten/bp/ZoneId;Lcom/squareup/customreport/data/SalesReportType$Charted;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 49
    :goto_0
    invoke-direct {v0, p2, p0, p1}, Lcom/squareup/customreport/data/WithSalesChartReport;-><init>(Lcom/squareup/customreport/data/SalesReportType$Charted;Ljava/util/List;Ljava/util/List;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/customreport/data/SalesChartReport;

    :goto_1
    return-object p0
.end method
