.class public final synthetic Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->values()[Lcom/squareup/protos/beemo/translation_types/TranslationType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNITEMIZED_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SALES_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_TIP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CHANGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CASH_REFUND:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->INCLUSIVE_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CREDIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SWEDISH_ROUNDING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CATEGORY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DISCOUNT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SUBUNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MOBILE_STAFF:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->CUSTOM_AMOUNT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ITEM_VARIATION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MODIFIER_SET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TAX:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DINING_OPTION:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_GIFT_CARD:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_VOID_REASON:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_COMP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_GROUP_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->TICKET_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_DEVICE_CREDENTIAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_ACCOUNT_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_OWNER_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_DELETED_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->EMPLOYEE_ROLE_MOBILE_STAFF_ROLE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x2a

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONVERSATIONAL_MODE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x2b

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x2c

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_TICKET_TEMPLATE_CUSTOM_CHECK:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x2d

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x2e

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_CONTACT_TOKEN:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x2f

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_PAYMENT_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x30

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_REPORTING_GROUP:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x31

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x32

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE_AUTO_GRATUITY:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x33

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_SURCHARGE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x34

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_ORDER_SOURCE_NAME:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x35

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x36

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_MARKET:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x37

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ORDER:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x38

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EXTERNAL_API:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x39

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_APPOINTMENTS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x3a

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_INVOICES:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x3b

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_BILLING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x3c

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x3d

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_ONLINE_STORE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x3e

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_STARBUCKS:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x3f

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_WHITE_LABEL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x40

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_PAYROLL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x41

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CAPITAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x42

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_TERMINAL:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x43

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_EGIFTING:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x44

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_CASH:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x45

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ORDER_SOURCE_REGISTER_HARDWARE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x46

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->DEFAULT_MEASUREMENT_UNIT:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x47

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->SURCHARGE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x48

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_MENU:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x49

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/beemo/translation_types/TranslationType;->UNKNOWN_VENDOR:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/TranslationType;->ordinal()I

    move-result v1

    const/16 v2, 0x4a

    aput v2, v0, v1

    return-void
.end method
