.class public final Lcom/squareup/customreport/data/RealRemoteCustomReportStore;
.super Ljava/lang/Object;
.source "RealRemoteCustomReportStore.kt"

# interfaces
.implements Lcom/squareup/customreport/data/service/RemoteCustomReportStore;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealRemoteCustomReportStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealRemoteCustomReportStore.kt\ncom/squareup/customreport/data/RealRemoteCustomReportStore\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,144:1\n1360#2:145\n1429#2,3:146\n*E\n*S KotlinDebug\n*F\n+ 1 RealRemoteCustomReportStore.kt\ncom/squareup/customreport/data/RealRemoteCustomReportStore\n*L\n112#1:145\n112#1,3:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B3\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ0\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J<\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001a0\u00190\u00182\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u001e\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u001c2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/customreport/data/RealRemoteCustomReportStore;",
        "Lcom/squareup/customreport/data/service/RemoteCustomReportStore;",
        "userToken",
        "",
        "merchantToken",
        "deviceIdProvider",
        "Lcom/squareup/settings/DeviceIdProvider;",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "customReportService",
        "Lcom/squareup/customreport/data/service/CustomReportService;",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/CustomReportService;)V",
        "createRequest",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;",
        "salesReportType",
        "Lcom/squareup/customreport/data/SalesReportType;",
        "startDateTime",
        "Lorg/threeten/bp/ZonedDateTime;",
        "endDateTime",
        "thisDeviceOnly",
        "",
        "employeeFiltersSelection",
        "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
        "customReport",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "filters",
        "",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

.field private final customReportService:Lcom/squareup/customreport/data/service/CustomReportService;

.field private final deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

.field private final merchantToken:Ljava/lang/String;

.field private final userToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/customreport/data/service/CustomReportService;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/UserToken;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/squareup/user/MerchantToken;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "userToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deviceIdProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTimeZone"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customReportService"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->userToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->merchantToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

    iput-object p4, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    iput-object p5, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->customReportService:Lcom/squareup/customreport/data/service/CustomReportService;

    return-void
.end method

.method private final createRequest(Lcom/squareup/customreport/data/SalesReportType;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;
    .locals 2

    .line 75
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;-><init>()V

    .line 76
    iget-object v1, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->merchantToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->userToken:Ljava/lang/String;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->subunit_merchant_token(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object v0

    .line 78
    invoke-static {p2}, Lcom/squareup/util/threeten/ZonedDateTimesKt;->toProtoDateTime(Lorg/threeten/bp/ZonedDateTime;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p2

    .line 79
    invoke-static {p3}, Lcom/squareup/util/threeten/ZonedDateTimesKt;->toProtoDateTime(Lorg/threeten/bp/ZonedDateTime;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p2

    .line 80
    iget-object p3, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {p3}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object p3

    invoke-virtual {p3}, Lorg/threeten/bp/ZoneId;->getId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->tz_name(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object p2

    .line 81
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->build()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    move-result-object p2

    .line 82
    new-instance p3, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    invoke-direct {p3}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;-><init>()V

    .line 83
    invoke-virtual {p3, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->request_params(Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p2

    .line 84
    invoke-virtual {p1}, Lcom/squareup/customreport/data/SalesReportType;->getGroupByTypes()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->group_by_type(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p1

    .line 85
    invoke-direct {p0, p4, p5}, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->filters(ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->filter(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    const-string p2, "CustomReportRequest.Buil\u2026ection))\n        .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final filters(ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
            ">;"
        }
    .end annotation

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    if-eqz p1, :cond_0

    .line 96
    new-instance p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;-><init>()V

    .line 97
    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->DEVICE_TOKEN:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 100
    new-instance v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 102
    new-instance v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;-><init>()V

    .line 103
    iget-object v3, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->deviceIdProvider:Lcom/squareup/settings/DeviceIdProvider;

    invoke-interface {v3}, Lcom/squareup/settings/DeviceIdProvider;->getDeviceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;->device_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;

    move-result-object v2

    .line 104
    invoke-virtual {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;

    move-result-object v2

    .line 101
    invoke-virtual {v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->device(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Device;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v1

    .line 99
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 98
    invoke-virtual {p1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    const-string v1, "Filter.Builder()\n       \u2026)\n              ).build()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    :cond_0
    instance-of p1, p2, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    if-eqz p1, :cond_4

    .line 114
    check-cast p2, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    invoke-virtual {p2}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->getEmployeeFilters()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 145
    new-instance p2, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {p2, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 146
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 147
    check-cast v1, Lcom/squareup/customreport/data/EmployeeFilter;

    .line 116
    new-instance v2, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;-><init>()V

    .line 118
    new-instance v3, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;-><init>()V

    .line 120
    instance-of v4, v1, Lcom/squareup/customreport/data/EmployeeFilter$UnattributedEmployeeFilter;

    if-eqz v4, :cond_1

    .line 121
    new-instance v1, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;-><init>()V

    .line 122
    sget-object v4, Lcom/squareup/protos/beemo/translation_types/TranslationType;->NO_EMPLOYEE:Lcom/squareup/protos/beemo/translation_types/TranslationType;

    invoke-virtual {v1, v4}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->translation_type(Lcom/squareup/protos/beemo/translation_types/TranslationType;)Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;

    move-result-object v1

    .line 123
    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType$Builder;->build()Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    move-result-object v1

    .line 120
    invoke-virtual {v3, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;->name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;

    move-result-object v1

    goto :goto_1

    .line 125
    :cond_1
    instance-of v4, v1, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;

    if-eqz v4, :cond_2

    check-cast v1, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;

    invoke-virtual {v1}, Lcom/squareup/customreport/data/EmployeeFilter$SingleEmployeeFilter;->getEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;

    move-result-object v1

    .line 127
    :goto_1
    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;

    move-result-object v1

    .line 117
    invoke-virtual {v2, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->employee(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Employee;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 148
    :cond_3
    check-cast p2, Ljava/util/List;

    .line 133
    new-instance p1, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;-><init>()V

    .line 134
    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;->EMPLOYEE:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    invoke-virtual {p1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 135
    invoke-virtual {p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;

    move-result-object p1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object p1

    const-string p2, "Filter.Builder()\n       \u2026\n                .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    :cond_4
    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public customReport(Lcom/squareup/customreport/data/SalesReportType;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/SalesReportType;",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Z",
            "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;>;"
        }
    .end annotation

    const-string v0, "salesReportType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startDateTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endDateTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeFiltersSelection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct/range {p0 .. p5}, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->createRequest(Lcom/squareup/customreport/data/SalesReportType;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;

    move-result-object p1

    .line 63
    iget-object p2, p0, Lcom/squareup/customreport/data/RealRemoteCustomReportStore;->customReportService:Lcom/squareup/customreport/data/service/CustomReportService;

    .line 64
    invoke-interface {p2, p1}, Lcom/squareup/customreport/data/service/CustomReportService;->getCustomReport(Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
