.class public interface abstract Lcom/squareup/customreport/data/service/RemoteCustomReportStore;
.super Ljava/lang/Object;
.source "RemoteCustomReportStore.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J<\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/customreport/data/service/RemoteCustomReportStore;",
        "",
        "customReport",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
        "salesReportType",
        "Lcom/squareup/customreport/data/SalesReportType;",
        "startDateTime",
        "Lorg/threeten/bp/ZonedDateTime;",
        "endDateTime",
        "thisDeviceOnly",
        "",
        "employeeFiltersSelection",
        "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract customReport(Lcom/squareup/customreport/data/SalesReportType;Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;ZLcom/squareup/customreport/data/EmployeeFiltersSelection;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/SalesReportType;",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Lorg/threeten/bp/ZonedDateTime;",
            "Z",
            "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReportResponse;",
            ">;>;"
        }
    .end annotation
.end method
