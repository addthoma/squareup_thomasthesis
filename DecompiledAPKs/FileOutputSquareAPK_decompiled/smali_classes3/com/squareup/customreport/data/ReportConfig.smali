.class public final Lcom/squareup/customreport/data/ReportConfig;
.super Ljava/lang/Object;
.source "ReportConfig.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/ReportConfig$Creator;,
        Lcom/squareup/customreport/data/ReportConfig$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportConfig.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportConfig.kt\ncom/squareup/customreport/data/ReportConfig\n*L\n1#1,35:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u001b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u0000 82\u00020\u0001:\u00018BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\t\u0010!\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0003H\u00c6\u0003J\t\u0010#\u001a\u00020\u0006H\u00c6\u0003J\t\u0010$\u001a\u00020\u0006H\u00c6\u0003J\t\u0010%\u001a\u00020\tH\u00c6\u0003J\t\u0010&\u001a\u00020\tH\u00c6\u0003J\t\u0010\'\u001a\u00020\u000cH\u00c6\u0003J\t\u0010(\u001a\u00020\u000eH\u00c6\u0003J\t\u0010)\u001a\u00020\u0010H\u00c6\u0003Jc\u0010*\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\t2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e2\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010H\u00c6\u0001J\t\u0010+\u001a\u00020,H\u00d6\u0001J\u0013\u0010-\u001a\u00020\t2\u0008\u0010.\u001a\u0004\u0018\u00010/H\u00d6\u0003J\t\u00100\u001a\u00020,H\u00d6\u0001J\t\u00101\u001a\u000202H\u00d6\u0001J\u0019\u00103\u001a\u0002042\u0006\u00105\u001a\u0002062\u0006\u00107\u001a\u00020,H\u00d6\u0001R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001bR\u0011\u0010\n\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u0013\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/customreport/data/ReportConfig;",
        "Landroid/os/Parcelable;",
        "startDate",
        "Lorg/threeten/bp/LocalDate;",
        "endDate",
        "startTime",
        "Lorg/threeten/bp/LocalTime;",
        "endTime",
        "allDay",
        "",
        "thisDeviceOnly",
        "employeeFiltersSelection",
        "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
        "rangeSelection",
        "Lcom/squareup/customreport/data/RangeSelection;",
        "comparisonRange",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;)V",
        "getAllDay",
        "()Z",
        "getComparisonRange",
        "()Lcom/squareup/customreport/data/ComparisonRange;",
        "getEmployeeFiltersSelection",
        "()Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
        "getEndDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getEndTime",
        "()Lorg/threeten/bp/LocalTime;",
        "getRangeSelection",
        "()Lcom/squareup/customreport/data/RangeSelection;",
        "getStartDate",
        "getStartTime",
        "getThisDeviceOnly",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final Companion:Lcom/squareup/customreport/data/ReportConfig$Companion;

.field private static final DEFAULT_END_TIME:Lorg/threeten/bp/LocalTime;

.field private static final DEFAULT_START_TIME:Lorg/threeten/bp/LocalTime;


# instance fields
.field private final allDay:Z

.field private final comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

.field private final employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

.field private final endDate:Lorg/threeten/bp/LocalDate;

.field private final endTime:Lorg/threeten/bp/LocalTime;

.field private final rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

.field private final startDate:Lorg/threeten/bp/LocalDate;

.field private final startTime:Lorg/threeten/bp/LocalTime;

.field private final thisDeviceOnly:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/customreport/data/ReportConfig$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/customreport/data/ReportConfig$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/customreport/data/ReportConfig;->Companion:Lcom/squareup/customreport/data/ReportConfig$Companion;

    const/4 v0, 0x0

    const/16 v1, 0x8

    .line 31
    invoke-static {v1, v0}, Lorg/threeten/bp/LocalTime;->of(II)Lorg/threeten/bp/LocalTime;

    move-result-object v1

    const-string v2, "LocalTime.of(8, 0)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v1, Lcom/squareup/customreport/data/ReportConfig;->DEFAULT_START_TIME:Lorg/threeten/bp/LocalTime;

    const/16 v1, 0x14

    .line 32
    invoke-static {v1, v0}, Lorg/threeten/bp/LocalTime;->of(II)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    const-string v1, "LocalTime.of(20, 0)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/customreport/data/ReportConfig;->DEFAULT_END_TIME:Lorg/threeten/bp/LocalTime;

    new-instance v0, Lcom/squareup/customreport/data/ReportConfig$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/ReportConfig$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/ReportConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;)V
    .locals 1

    const-string v0, "startDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endDate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endTime"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeFiltersSelection"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rangeSelection"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "comparisonRange"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    iput-object p2, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    iput-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    iput-object p4, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    iput-boolean p5, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    iput-boolean p6, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    iput-object p7, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    iput-object p8, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    iput-object p9, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    .line 21
    iget-object p1, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    iget-object p2, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    check-cast p2, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/LocalDate;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result p1

    const/16 p2, 0x29

    if-ltz p1, :cond_2

    .line 25
    iget-object p1, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {p1, p3}, Lorg/threeten/bp/LocalTime;->compareTo(Lorg/threeten/bp/LocalTime;)I

    move-result p1

    if-gtz p1, :cond_0

    goto :goto_0

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "endTime("

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ") should be after startTime("

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ") on the same date("

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    :cond_1
    :goto_0
    return-void

    .line 22
    :cond_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "endDate("

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p3, ") should be after startDate("

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public static final synthetic access$getDEFAULT_END_TIME$cp()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/customreport/data/ReportConfig;->DEFAULT_END_TIME:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public static final synthetic access$getDEFAULT_START_TIME$cp()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/customreport/data/ReportConfig;->DEFAULT_START_TIME:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    goto :goto_4

    :cond_4
    move v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move p5, v6

    move/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/customreport/data/ReportConfig;->copy(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component2()Lorg/threeten/bp/LocalDate;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final component3()Lorg/threeten/bp/LocalTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final component4()Lorg/threeten/bp/LocalTime;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    return v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    return v0
.end method

.method public final component7()Lcom/squareup/customreport/data/EmployeeFiltersSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    return-object v0
.end method

.method public final component8()Lcom/squareup/customreport/data/RangeSelection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    return-object v0
.end method

.method public final component9()Lcom/squareup/customreport/data/ComparisonRange;
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    return-object v0
.end method

.method public final copy(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;)Lcom/squareup/customreport/data/ReportConfig;
    .locals 11

    const-string v0, "startDate"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endDate"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "startTime"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endTime"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeFiltersSelection"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rangeSelection"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "comparisonRange"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/customreport/data/ReportConfig;

    move-object v1, v0

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-direct/range {v1 .. v10}, Lcom/squareup/customreport/data/ReportConfig;-><init>(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/customreport/data/ReportConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/customreport/data/ReportConfig;

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    iget-object v1, p1, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    iget-object v1, p1, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    iget-object v1, p1, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    iget-boolean v1, p1, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    iget-boolean v1, p1, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    iget-object v1, p1, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    iget-object v1, p1, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    iget-object p1, p1, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllDay()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    return v0
.end method

.method public final getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    return-object v0
.end method

.method public final getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    return-object v0
.end method

.method public final getEndDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getEndTime()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    return-object v0
.end method

.method public final getStartDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getStartTime()Lorg/threeten/bp/LocalTime;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    return-object v0
.end method

.method public final getThisDeviceOnly()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReportConfig(startDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", endDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", startTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", endTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allDay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", thisDeviceOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", employeeFiltersSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", rangeSelection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", comparisonRange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endDate:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->startTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->endTime:Lorg/threeten/bp/LocalTime;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->allDay:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/squareup/customreport/data/ReportConfig;->thisDeviceOnly:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->employeeFiltersSelection:Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->rangeSelection:Lcom/squareup/customreport/data/RangeSelection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/squareup/customreport/data/ReportConfig;->comparisonRange:Lcom/squareup/customreport/data/ComparisonRange;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
