.class public abstract Lcom/squareup/customreport/data/ComparisonRange;
.super Ljava/lang/Object;
.source "ComparisonRange.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;,
        Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;,
        Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;,
        Lcom/squareup/customreport/data/ComparisonRange$Yesterday;,
        Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;,
        Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;,
        Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;,
        Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;,
        Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;,
        Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;,
        Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;,
        Lcom/squareup/customreport/data/ComparisonRange$NoComparison;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000c\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000c\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "Landroid/os/Parcelable;",
        "()V",
        "NoComparison",
        "PreviousDay",
        "PreviousMonth",
        "PreviousThreeMonths",
        "PreviousWeek",
        "PreviousYear",
        "SameDayPreviousWeek",
        "SameDayPreviousYear",
        "SameMonthPreviousYear",
        "SameThreeMonthsPreviousYear",
        "SameWeekPreviousYear",
        "Yesterday",
        "Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;",
        "Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;",
        "Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;",
        "Lcom/squareup/customreport/data/ComparisonRange$Yesterday;",
        "Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;",
        "Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;",
        "Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;",
        "Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;",
        "Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;",
        "Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;",
        "Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;",
        "Lcom/squareup/customreport/data/ComparisonRange$NoComparison;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/customreport/data/ComparisonRange;-><init>()V

    return-void
.end method
