.class public Lcom/squareup/dialog/GlassDialogUtil;
.super Ljava/lang/Object;
.source "GlassDialogUtil.java"


# instance fields
.field private volatile inputEnabled:Z

.field private final owner:Lcom/squareup/dialog/CanDisableClicks;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/squareup/dialog/GlassDialogUtil;->owner(Landroid/content/Context;)Lcom/squareup/dialog/CanDisableClicks;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/dialog/GlassDialogUtil;->owner:Lcom/squareup/dialog/CanDisableClicks;

    return-void
.end method

.method public static createCancelEvent()Landroid/view/MotionEvent;
    .locals 8

    .line 18
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-wide v0, v2

    .line 19
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    return-object v0
.end method

.method private static owner(Landroid/content/Context;)Lcom/squareup/dialog/CanDisableClicks;
    .locals 1

    .line 34
    :goto_0
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_1

    .line 35
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    instance-of v0, p0, Lcom/squareup/dialog/CanDisableClicks;

    if-eqz v0, :cond_0

    .line 36
    check-cast p0, Lcom/squareup/dialog/CanDisableClicks;

    return-object p0

    .line 38
    :cond_0
    check-cast p0, Landroid/content/ContextWrapper;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method dismiss()V
    .locals 1

    const/4 v0, 0x0

    .line 49
    iput-boolean v0, p0, Lcom/squareup/dialog/GlassDialogUtil;->inputEnabled:Z

    .line 50
    iget-object v0, p0, Lcom/squareup/dialog/GlassDialogUtil;->owner:Lcom/squareup/dialog/CanDisableClicks;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/dialog/CanDisableClicks;->enableClicks()V

    :cond_0
    return-void
.end method

.method isInputEnabled()Z
    .locals 1

    .line 54
    iget-boolean v0, p0, Lcom/squareup/dialog/GlassDialogUtil;->inputEnabled:Z

    return v0
.end method

.method show()V
    .locals 1

    const/4 v0, 0x1

    .line 44
    iput-boolean v0, p0, Lcom/squareup/dialog/GlassDialogUtil;->inputEnabled:Z

    .line 45
    iget-object v0, p0, Lcom/squareup/dialog/GlassDialogUtil;->owner:Lcom/squareup/dialog/CanDisableClicks;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/dialog/CanDisableClicks;->disableClicks()V

    :cond_0
    return-void
.end method
