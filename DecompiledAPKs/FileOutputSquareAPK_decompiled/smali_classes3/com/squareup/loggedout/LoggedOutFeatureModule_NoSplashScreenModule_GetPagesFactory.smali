.class public final Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/List<",
        "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
        ">;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory;
    .locals 1

    .line 24
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory$InstanceHolder;->access$000()Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory;

    move-result-object v0

    return-object v0
.end method

.method public static getPages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;"
        }
    .end annotation

    .line 28
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule$NoSplashScreenModule;->getPages()Ljava/util/List;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;"
        }
    .end annotation

    .line 20
    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_NoSplashScreenModule_GetPagesFactory;->getPages()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
