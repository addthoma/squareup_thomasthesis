.class public abstract Lcom/squareup/loggedout/LoggedOutFeatureModule$LoggedOutStarterModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/LoggedOutFeatureModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "LoggedOutStarterModule"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loggedout/LoggedOutFeatureModule;


# direct methods
.method public constructor <init>(Lcom/squareup/loggedout/LoggedOutFeatureModule;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutFeatureModule$LoggedOutStarterModule;->this$0:Lcom/squareup/loggedout/LoggedOutFeatureModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideLoggedOutStarter(Lcom/squareup/loggedout/LoggedOutFeatureStarter;)Lcom/squareup/loggedout/LoggedOutStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
