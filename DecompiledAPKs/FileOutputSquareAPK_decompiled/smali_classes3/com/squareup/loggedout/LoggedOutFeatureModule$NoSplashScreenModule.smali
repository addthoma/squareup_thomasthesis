.class public abstract Lcom/squareup/loggedout/LoggedOutFeatureModule$NoSplashScreenModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/LoggedOutFeatureModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NoSplashScreenModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getPages()Ljava/util/List;
    .locals 2
    .annotation runtime Lcom/squareup/ui/loggedout/SplashCoordinator$Pages;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;"
        }
    .end annotation

    .line 144
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No Splash Screen pages have been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static provideTextAboveImageSplashScreenConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 148
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No TextAboveImageSplashScreenConfig has been set. Did you mean to use SplashScreen?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
