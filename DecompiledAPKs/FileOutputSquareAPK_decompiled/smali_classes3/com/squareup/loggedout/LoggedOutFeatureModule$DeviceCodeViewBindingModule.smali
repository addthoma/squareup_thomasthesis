.class public abstract Lcom/squareup/loggedout/LoggedOutFeatureModule$DeviceCodeViewBindingModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/LoggedOutFeatureModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "DeviceCodeViewBindingModule"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loggedout/LoggedOutFeatureModule;


# direct methods
.method public constructor <init>(Lcom/squareup/loggedout/LoggedOutFeatureModule;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutFeatureModule$DeviceCodeViewBindingModule;->this$0:Lcom/squareup/loggedout/LoggedOutFeatureModule;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideDeviceCodeViewBinding(Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding;)Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
