.class public interface abstract Lcom/squareup/loggedout/CommonLoggedOutActivityComponent;
.super Ljava/lang/Object;
.source "CommonLoggedOutActivityComponent.java"

# interfaces
.implements Lcom/squareup/caller/ProgressPopup$Component;
.implements Lcom/squareup/ui/login/AuthenticatorScope$ParentComponent;
.implements Lcom/squareup/ui/LinkSpan$Component;
.implements Lcom/squareup/ui/loggedout/SplashScreen$ParentComponent;
.implements Lcom/squareup/ui/loggedout/TextAboveImageSplashScreen$ParentComponent;


# virtual methods
.method public abstract createAccount()Lcom/squareup/ui/login/CreateAccountScreen$Component;
.end method

.method public abstract inject(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V
.end method

.method public abstract inject(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)V
.end method

.method public abstract loggedOutScopeRunner()Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
.end method

.method public abstract tour()Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;
.end method
