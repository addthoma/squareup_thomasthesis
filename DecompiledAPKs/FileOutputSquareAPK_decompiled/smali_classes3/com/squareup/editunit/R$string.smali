.class public final Lcom/squareup/editunit/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/editunit/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abbreviation_label:I = 0x7f120000

.field public static final count_of_affected_variations_alert_confirm_button_text_for_deletion:I = 0x7f1204dc

.field public static final count_of_affected_variations_alert_confirm_button_text_for_editing:I = 0x7f1204dd

.field public static final count_of_affected_variations_alert_message_for_deletion_plural:I = 0x7f1204de

.field public static final count_of_affected_variations_alert_message_for_deletion_singular:I = 0x7f1204df

.field public static final count_of_affected_variations_alert_message_for_editing_plural:I = 0x7f1204e0

.field public static final count_of_affected_variations_alert_message_for_editing_singular:I = 0x7f1204e1

.field public static final count_of_affected_variations_alert_title_for_deletion:I = 0x7f1204e2

.field public static final count_of_affected_variations_alert_title_for_editing:I = 0x7f1204e3

.field public static final create_unit_failed_alert_message_due_to_network:I = 0x7f1205ed

.field public static final create_unit_failed_alert_message_not_due_to_network:I = 0x7f1205ee

.field public static final create_unit_failed_alert_title:I = 0x7f1205ef

.field public static final delete_unit_failed_alert_message_due_to_network:I = 0x7f1207eb

.field public static final delete_unit_failed_alert_message_not_due_to_network:I = 0x7f1207ec

.field public static final delete_unit_failed_alert_title:I = 0x7f1207ed

.field public static final edit_item_assign_unit_internet_not_available_alert_message:I = 0x7f12090a

.field public static final edit_item_assign_unit_internet_not_available_alert_title:I = 0x7f12090b

.field public static final edit_unit_abbreviation_hint:I = 0x7f12096e

.field public static final edit_unit_duplicate_custom_unit_name_message:I = 0x7f12096f

.field public static final edit_unit_duplicate_custom_unit_name_title:I = 0x7f120970

.field public static final edit_unit_failed_alert_message_due_to_network:I = 0x7f120971

.field public static final edit_unit_failed_alert_message_not_due_to_network:I = 0x7f120972

.field public static final edit_unit_failed_alert_title:I = 0x7f120973

.field public static final edit_unit_name_hint:I = 0x7f120974

.field public static final edit_unit_precision_hint:I = 0x7f120975

.field public static final edit_unit_screen_delete_button_initial_stage_text:I = 0x7f120976

.field public static final edit_unit_screen_title_create_custom:I = 0x7f120977

.field public static final edit_unit_screen_title_create_standard:I = 0x7f120978

.field public static final edit_unit_screen_title_edit_custom:I = 0x7f120979

.field public static final edit_unit_screen_title_edit_standard:I = 0x7f12097a

.field public static final edit_unit_screen_title_edit_unknown_or_generic:I = 0x7f12097b

.field public static final precision_label:I = 0x7f121466

.field public static final standard_units_list_create_custom_unit:I = 0x7f1218ad

.field public static final standard_units_list_create_custom_unit_with_name:I = 0x7f1218ae

.field public static final standard_units_list_create_unit:I = 0x7f1218af

.field public static final standard_units_list_family_label_area:I = 0x7f1218b0

.field public static final standard_units_list_family_label_length:I = 0x7f1218b1

.field public static final standard_units_list_family_label_time:I = 0x7f1218b2

.field public static final standard_units_list_family_label_volume:I = 0x7f1218b3

.field public static final standard_units_list_family_label_weight:I = 0x7f1218b4

.field public static final standard_units_list_no_search_results:I = 0x7f1218b5

.field public static final standard_units_list_search_bar_hint_text:I = 0x7f1218b6

.field public static final standard_units_list_unit_label:I = 0x7f1218b7

.field public static final unit_label:I = 0x7f121aea

.field public static final unit_number_limit_help_text:I = 0x7f121aeb


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
