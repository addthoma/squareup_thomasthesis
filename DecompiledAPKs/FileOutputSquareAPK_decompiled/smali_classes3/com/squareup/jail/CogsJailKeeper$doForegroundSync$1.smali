.class final Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;
.super Ljava/lang/Object;
.source "CogsJailKeeper.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jail/CogsJailKeeper;->doForegroundSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/shared/catalog/CatalogResult<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/shared/catalog/CatalogResult;",
        "",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jail/CogsJailKeeper;


# direct methods
.method constructor <init>(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/jail/CogsJailKeeper;->access$setRequireCogsSync$p(Lcom/squareup/jail/CogsJailKeeper;Z)V

    .line 188
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-static {p1}, Lcom/squareup/jail/CogsJailKeeper;->access$getRequireCogsSync$p(Lcom/squareup/jail/CogsJailKeeper;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 189
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-static {p1}, Lcom/squareup/jail/CogsJailKeeper;->access$doCogsForegroundSync(Lcom/squareup/jail/CogsJailKeeper;)V

    goto :goto_0

    .line 191
    :cond_1
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/jail/CogsJailKeeper;->access$preloadData(Lcom/squareup/jail/CogsJailKeeper;Z)V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/shared/catalog/CatalogResult;

    invoke-virtual {p0, p1}, Lcom/squareup/jail/CogsJailKeeper$doForegroundSync$1;->accept(Lcom/squareup/shared/catalog/CatalogResult;)V

    return-void
.end method
