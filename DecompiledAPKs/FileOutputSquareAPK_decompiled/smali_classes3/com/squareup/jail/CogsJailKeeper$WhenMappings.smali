.class public final synthetic Lcom/squareup/jail/CogsJailKeeper$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->values()[Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_TOO_MANY_REQUESTS:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/jail/CogsJailKeeper$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    return-void
.end method
