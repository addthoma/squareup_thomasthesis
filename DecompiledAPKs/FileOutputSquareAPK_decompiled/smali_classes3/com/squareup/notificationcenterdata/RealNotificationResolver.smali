.class public final Lcom/squareup/notificationcenterdata/RealNotificationResolver;
.super Ljava/lang/Object;
.source "RealNotificationResolver.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationResolver;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0014\u0010\u000b\u001a\u00020\u000c*\u00020\n2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0014\u0010\u0007\u001a\u00020\u0008*\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/RealNotificationResolver;",
        "Lcom/squareup/notificationcenterdata/NotificationResolver;",
        "messageResolver",
        "Lcom/squareup/communications/MessageResolver;",
        "notificationStateStore",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "(Lcom/squareup/communications/MessageResolver;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V",
        "resolve",
        "Lio/reactivex/Completable;",
        "notification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "addToNotificationStateStore",
        "",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "resolution",
        "Lcom/squareup/communications/MessageResolver$Resolution;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final messageResolver:Lcom/squareup/communications/MessageResolver;

.field private final notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;


# direct methods
.method public constructor <init>(Lcom/squareup/communications/MessageResolver;Lcom/squareup/notificationcenterdata/NotificationStateStore;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "messageResolver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStateStore"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->messageResolver:Lcom/squareup/communications/MessageResolver;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    return-void
.end method

.method private final addToNotificationStateStore(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenterdata/Notification$State;)V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object p1

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/notificationcenterdata/NotificationStateStore;->addNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V

    return-void
.end method

.method private final resolve(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Completable;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->messageResolver:Lcom/squareup/communications/MessageResolver;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getRequestToken()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/communications/MessageResolver;->resolve(Ljava/lang/String;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Single;

    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lio/reactivex/Single;->ignoreElement()Lio/reactivex/Completable;

    move-result-object p1

    const-string p2, "messageResolver\n        \u2026\n        .ignoreElement()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public resolve(Lcom/squareup/notificationcenterdata/Notification;)Lio/reactivex/Completable;
    .locals 4

    const-string v0, "notification"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-direct {p0, p1, v0}, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->addToNotificationStateStore(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenterdata/Notification$State;)V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getSource()Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v0

    sget-object v1, Lcom/squareup/notificationcenterdata/RealNotificationResolver$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/Notification$Source;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    .line 42
    invoke-static {}, Lio/reactivex/Completable;->complete()Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "Completable.complete()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 35
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification;->getDisplayType()Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    move-result-object v0

    .line 36
    instance-of v3, v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    if-eqz v3, :cond_2

    sget-object v0, Lcom/squareup/communications/MessageResolver$Resolution$Viewed;->INSTANCE:Lcom/squareup/communications/MessageResolver$Resolution$Viewed;

    check-cast v0, Lcom/squareup/communications/MessageResolver$Resolution;

    invoke-direct {p0, p1, v0}, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->resolve(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Completable;

    move-result-object p1

    goto :goto_0

    .line 37
    :cond_2
    instance-of v0, v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    if-eqz v0, :cond_3

    new-array v0, v1, [Lio/reactivex/CompletableSource;

    const/4 v1, 0x0

    .line 38
    sget-object v3, Lcom/squareup/communications/MessageResolver$Resolution$Viewed;->INSTANCE:Lcom/squareup/communications/MessageResolver$Resolution$Viewed;

    check-cast v3, Lcom/squareup/communications/MessageResolver$Resolution;

    invoke-direct {p0, p1, v3}, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->resolve(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Completable;

    move-result-object v3

    check-cast v3, Lio/reactivex/CompletableSource;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/communications/MessageResolver$Resolution$Clicked;->INSTANCE:Lcom/squareup/communications/MessageResolver$Resolution$Clicked;

    check-cast v1, Lcom/squareup/communications/MessageResolver$Resolution;

    invoke-direct {p0, p1, v1}, Lcom/squareup/notificationcenterdata/RealNotificationResolver;->resolve(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/communications/MessageResolver$Resolution;)Lio/reactivex/Completable;

    move-result-object p1

    check-cast p1, Lio/reactivex/CompletableSource;

    aput-object p1, v0, v2

    .line 37
    invoke-static {v0}, Lio/reactivex/Completable;->mergeArray([Lio/reactivex/CompletableSource;)Lio/reactivex/Completable;

    move-result-object p1

    const-string v0, "Completable.mergeArray(\n\u2026olve(Clicked)\n          )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
