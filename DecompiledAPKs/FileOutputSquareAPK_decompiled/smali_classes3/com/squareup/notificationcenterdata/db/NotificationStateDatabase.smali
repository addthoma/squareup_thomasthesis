.class public final Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;
.super Ljava/lang/Object;
.source "NotificationStateDatabase.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/impl/Database;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationStateDatabase.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationStateDatabase.kt\ncom/squareup/notificationcenterdata/db/NotificationStateDatabase\n+ 2 EnumColumnAdapter.kt\ncom/squareup/sqldelight/EnumColumnAdapterKt\n*L\n1#1,20:1\n30#2:21\n30#2:22\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationStateDatabase.kt\ncom/squareup/notificationcenterdata/db/NotificationStateDatabase\n*L\n16#1:21\n17#1:22\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J*\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0017\u0010\r\u001a\u0013\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\n0\u000e\u00a2\u0006\u0002\u0008\u0010H\u0096\u0001R\u0012\u0010\u0005\u001a\u00020\u0006X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;",
        "Lcom/squareup/notificationcenterdata/impl/Database;",
        "sqlDriver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "(Lcom/squareup/sqldelight/db/SqlDriver;)V",
        "notificationStateQueries",
        "Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;",
        "getNotificationStateQueries",
        "()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;",
        "transaction",
        "",
        "noEnclosing",
        "",
        "body",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqldelight/Transacter$Transaction;",
        "Lkotlin/ExtensionFunctionType;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/notificationcenterdata/impl/Database;

.field private final sqlDriver:Lcom/squareup/sqldelight/db/SqlDriver;


# direct methods
.method public constructor <init>(Lcom/squareup/sqldelight/db/SqlDriver;)V
    .locals 4
    .param p1    # Lcom/squareup/sqldelight/db/SqlDriver;
        .annotation runtime Lcom/squareup/notificationcenterdata/db/ForNotificationStateDatabase;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "sqlDriver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    sget-object v0, Lcom/squareup/notificationcenterdata/impl/Database;->Companion:Lcom/squareup/notificationcenterdata/impl/Database$Companion;

    .line 21
    invoke-static {}, Lcom/squareup/notificationcenterdata/Notification$Source;->values()[Lcom/squareup/notificationcenterdata/Notification$Source;

    move-result-object v1

    new-instance v2, Lcom/squareup/sqldelight/EnumColumnAdapter;

    invoke-direct {v2, v1}, Lcom/squareup/sqldelight/EnumColumnAdapter;-><init>([Ljava/lang/Enum;)V

    check-cast v2, Lcom/squareup/sqldelight/ColumnAdapter;

    .line 22
    invoke-static {}, Lcom/squareup/notificationcenterdata/Notification$State;->values()[Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v1

    new-instance v3, Lcom/squareup/sqldelight/EnumColumnAdapter;

    invoke-direct {v3, v1}, Lcom/squareup/sqldelight/EnumColumnAdapter;-><init>([Ljava/lang/Enum;)V

    check-cast v3, Lcom/squareup/sqldelight/ColumnAdapter;

    .line 15
    new-instance v1, Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    invoke-direct {v1, v2, v3}, Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;-><init>(Lcom/squareup/sqldelight/ColumnAdapter;Lcom/squareup/sqldelight/ColumnAdapter;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/notificationcenterdata/impl/Database$Companion;->invoke(Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)Lcom/squareup/notificationcenterdata/impl/Database;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;->$$delegate_0:Lcom/squareup/notificationcenterdata/impl/Database;

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;->sqlDriver:Lcom/squareup/sqldelight/db/SqlDriver;

    return-void
.end method


# virtual methods
.method public getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;->$$delegate_0:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0}, Lcom/squareup/notificationcenterdata/impl/Database;->getNotificationStateQueries()Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;

    move-result-object v0

    return-object v0
.end method

.method public transaction(ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqldelight/Transacter$Transaction;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "body"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;->$$delegate_0:Lcom/squareup/notificationcenterdata/impl/Database;

    invoke-interface {v0, p1, p2}, Lcom/squareup/notificationcenterdata/impl/Database;->transaction(ZLkotlin/jvm/functions/Function1;)V

    return-void
.end method
