.class public final Lcom/squareup/egiftcard/activation/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final egiftcard_choose_amount:I = 0x7f12097d

.field public static final egiftcard_choose_design:I = 0x7f12097e

.field public static final egiftcard_choose_email:I = 0x7f12097f

.field public static final egiftcard_choose_email_hint:I = 0x7f120980

.field public static final egiftcard_custom_amount:I = 0x7f120981

.field public static final egiftcard_custom_amount_hint:I = 0x7f120982

.field public static final egiftcard_custom_amount_title:I = 0x7f120983

.field public static final egiftcard_done:I = 0x7f120984

.field public static final egiftcard_registered_body:I = 0x7f120985

.field public static final egiftcard_registered_error:I = 0x7f120986

.field public static final egiftcard_registered_error_body:I = 0x7f120987

.field public static final egiftcard_registered_error_try_again:I = 0x7f120988

.field public static final egiftcard_registered_title:I = 0x7f120989

.field public static final egiftcard_return_to_cart_actionbar:I = 0x7f12098a

.field public static final egiftcard_x2_seller_amount:I = 0x7f12098b

.field public static final egiftcard_x2_seller_design:I = 0x7f12098c

.field public static final egiftcard_x2_seller_email:I = 0x7f12098d

.field public static final egiftcard_x2_seller_title:I = 0x7f12098e


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
