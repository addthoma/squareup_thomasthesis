.class public abstract Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.super Ljava/lang/Object;
.source "ActivateEGiftCardState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;,
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \r2\u00020\u0001:\t\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\u0008R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0008\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Landroid/os/Parcelable;",
        "()V",
        "config",
        "Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "getConfig",
        "()Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "ChoosingAmount",
        "ChoosingCustomAmount",
        "ChoosingDesign",
        "ChoosingEmail",
        "Companion",
        "ErrorRegistering",
        "InitState",
        "RegisteringEGiftCard",
        "SuccessRegistering",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;->Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;-><init>()V

    return-void
.end method

.method public static final fromByteString(Lokio/ByteString;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;->Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
.end method

.method public final takeSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 70
    move-object v0, p0

    check-cast v0, Landroid/os/Parcelable;

    invoke-static {v0}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
