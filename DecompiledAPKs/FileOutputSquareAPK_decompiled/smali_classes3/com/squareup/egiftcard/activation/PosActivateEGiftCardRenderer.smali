.class public final Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;
.super Ljava/lang/Object;
.source "PosActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u0005\u00a2\u0006\u0002\u0010\tJH\u0010\n\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000b\u001a\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;->render(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    sget-object p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_7

    .line 58
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;

    if-eqz p3, :cond_0

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;

    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/ChooseDesignScreenKt;->ChooseEGiftCardDesignScreen(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_1

    .line 59
    :cond_0
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;

    if-eqz p3, :cond_1

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;

    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/ChooseAmountScreenKt;->ChooseAmountScreen(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_1

    .line 60
    :cond_1
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    if-eqz p3, :cond_2

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountScreenKt;->ChooseCustomAmountScreen(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_1

    .line 61
    :cond_2
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    if-eqz p3, :cond_3

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;

    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/ChooseEmailScreenKt;->ChooseEmailScreen(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingEmail;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_1

    .line 62
    :cond_3
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$RegisteringEGiftCard;

    if-eqz p3, :cond_4

    goto :goto_0

    .line 63
    :cond_4
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    if-eqz p3, :cond_5

    goto :goto_0

    .line 64
    :cond_5
    instance-of p3, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ErrorRegistering;

    if-eqz p3, :cond_6

    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredScreenKt;->EGiftCardRegisteredScreen(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 66
    :goto_1
    sget-object p2, Lcom/squareup/container/PosLayering;->SHEET:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 64
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 57
    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
