.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "ActivateEGiftCardViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "chooseDesignCoordinator",
        "Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;",
        "chooseAmountCoordinator",
        "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;",
        "chooseCustomAmountCoordinator",
        "Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;",
        "chooseEmailCoordinator",
        "Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;",
        "(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;)V",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chooseDesignCoordinator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseAmountCoordinator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseCustomAmountCoordinator"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseEmailCoordinator"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 14
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 15
    sget-object v2, Lcom/squareup/egiftcard/activation/ChooseEGiftCardDesign;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v3, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_choose_design_view:I

    .line 16
    new-instance v4, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$1;-><init>(Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 14
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 18
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 19
    sget-object v3, Lcom/squareup/egiftcard/activation/ChooseAmount;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v4, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_choose_amount_view:I

    .line 20
    new-instance p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$2;

    invoke-direct {p1, p2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$2;-><init>(Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 18
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 22
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 23
    sget-object v2, Lcom/squareup/egiftcard/activation/ChooseCustomAmount;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v3, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_choose_custom_amount_view:I

    .line 24
    new-instance p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$3;

    invoke-direct {p1, p3}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$3;-><init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$Factory;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 22
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x2

    aput-object p1, v0, p2

    .line 26
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 27
    sget-object v2, Lcom/squareup/egiftcard/activation/ChooseEmail;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v3, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_choose_email_view:I

    .line 28
    new-instance p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$4;

    invoke-direct {p1, p4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$4;-><init>(Lcom/squareup/egiftcard/activation/ChooseEmailCoordinator$Factory;)V

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 26
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x3

    aput-object p1, v0, p2

    .line 30
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 31
    sget-object v2, Lcom/squareup/egiftcard/activation/EGiftCardRegistered;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v3, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_registering_view:I

    .line 32
    sget-object p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$5;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$5;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 30
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x4

    aput-object p1, v0, p2

    .line 34
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 35
    sget-object v2, Lcom/squareup/egiftcard/activation/BuyerActivatingEGiftCard;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget v3, Lcom/squareup/egiftcard/activation/R$layout;->egiftcard_x2_seller_buyer_activating_view:I

    .line 36
    sget-object p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$6;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$6;

    move-object v6, p1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 34
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x5

    aput-object p1, v0, p2

    .line 38
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    sget-object p2, Lcom/squareup/egiftcard/activation/ActivatingErrorSellerDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object p3, Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$7;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardViewFactory$7;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2, p3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x6

    aput-object p1, v0, p2

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
