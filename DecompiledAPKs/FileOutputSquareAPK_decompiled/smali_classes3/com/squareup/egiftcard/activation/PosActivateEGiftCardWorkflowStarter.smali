.class public final Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;
.super Ljava/lang/Object;
.source "PosActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J`\u0010\t\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\r\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000ej\u0002`\u000f0\u000cj\n\u0012\u0006\u0008\u0001\u0012\u00020\r`\u00100\u000b\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00120\nj\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u0012`\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardWorkflowStarter;",
        "reactor",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;",
        "statusBarEventManager",
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        "(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;Lcom/squareup/statusbar/event/StatusBarEventManager;)V",
        "renderer",
        "Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reactor:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;

.field private final renderer:Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;

.field private final statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;


# direct methods
.method public constructor <init>(Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;Lcom/squareup/statusbar/event/StatusBarEventManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statusBarEventManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;->reactor:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    .line 30
    new-instance p1, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;

    invoke-direct {p1}, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;->renderer:Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;

    return-void
.end method

.method public static final synthetic access$getRenderer$p(Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;)Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;->renderer:Lcom/squareup/egiftcard/activation/PosActivateEGiftCardRenderer;

    return-object p0
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardResult;",
            ">;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 35
    iget-object v1, p0, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;->reactor:Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;

    .line 36
    sget-object v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;->Companion:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;

    invoke-virtual {v2, p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$Companion;->fromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$InitState;

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    .line 35
    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardReactor;->launch(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 39
    iget-object v1, p0, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;->statusBarEventManager:Lcom/squareup/statusbar/event/StatusBarEventManager;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->toCompletable(Lcom/squareup/workflow/legacy/Workflow;)Lio/reactivex/Completable;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/statusbar/event/StatusBarEventManager;->enterFullscreenMode(Lio/reactivex/Completable;)V

    .line 42
    new-instance v1, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter$start$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v0, v2}, Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter$start$1;-><init>(Lcom/squareup/egiftcard/activation/PosActivateEGiftCardWorkflowStarter;Lcom/squareup/workflow/legacy/Workflow;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
