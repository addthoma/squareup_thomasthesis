.class final Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;
.super Ljava/lang/Object;
.source "X2ActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;->INSTANCE:Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 30
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$2;->test(Lkotlin/Pair;)Z

    move-result p1

    return p1
.end method

.method public final test(Lkotlin/Pair;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
            "+",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    .line 58
    invoke-virtual {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;->getState()Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
