.class public final Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "ChooseCustomAmountCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "v",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

.field final synthetic this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;",
            ")V"
        }
    .end annotation

    .line 93
    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;->this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    const-string/jumbo p3, "v"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p3, 0x5

    if-ne p2, p3, :cond_0

    .line 99
    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;->this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;

    iget-object p3, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;->$state:Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    const-string/jumbo v0, "v.text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2, p3, p1}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->access$isInputValidMoney(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingCustomAmount;Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 100
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator$update$4;->this$0:Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;

    invoke-static {p1}, Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;->access$getDoneClicked$p(Lcom/squareup/egiftcard/activation/ChooseCustomAmountCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
