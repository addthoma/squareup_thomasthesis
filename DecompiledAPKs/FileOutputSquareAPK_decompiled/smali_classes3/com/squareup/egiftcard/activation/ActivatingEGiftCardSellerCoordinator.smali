.class public final Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ActivatingEGiftCardSellerCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J&\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00052\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0017H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ScreenData;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "bodyText",
        "Lcom/squareup/marketfont/MarketTextView;",
        "spinner",
        "Lcom/squareup/marin/widgets/MarinSpinnerGlyph;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "state",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "egiftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private bodyText:Lcom/squareup/marketfont/MarketTextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ScreenData;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private spinner:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ScreenData;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 60
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 61
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_x2_seller_body:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    .line 62
    sget v0, Lcom/squareup/egiftcard/activation/R$id;->egiftcard_spinner_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->spinner:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ScreenData;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/egiftcard/activation/ScreenData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/egiftcard/activation/R$string;->egiftcard_x2_seller_title:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 40
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getSpinnerVisible()Z

    move-result v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getSpinnerIsCheckmark()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 42
    :goto_0
    iget-object v4, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v4, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    xor-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    if-eqz v0, :cond_3

    .line 44
    sget-object p3, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$update$1;->INSTANCE:Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$update$1;

    check-cast p3, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, p3}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 46
    :cond_3
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$update$2;

    invoke-direct {v1, p3}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$update$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 47
    new-instance v0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$update$3;

    invoke-direct {v0, p3}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$update$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 50
    :goto_1
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    const-string p3, "bodyText"

    if-nez p1, :cond_5

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getBody()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    :cond_6
    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 51
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getBody()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_8

    .line 52
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->bodyText:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_7

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getBody()Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    :cond_8
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->spinner:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    const-string p3, "spinner"

    if-nez p1, :cond_9

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getSpinnerVisible()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 56
    invoke-virtual {p2}, Lcom/squareup/egiftcard/activation/ScreenData;->getSpinnerIsCheckmark()Z

    move-result p1

    if-eqz p1, :cond_b

    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->spinner:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    if-nez p1, :cond_a

    invoke-static {p3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    :cond_b
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->bindViews(Landroid/view/View;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator$attach$1;-><init>(Lcom/squareup/egiftcard/activation/ActivatingEGiftCardSellerCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
