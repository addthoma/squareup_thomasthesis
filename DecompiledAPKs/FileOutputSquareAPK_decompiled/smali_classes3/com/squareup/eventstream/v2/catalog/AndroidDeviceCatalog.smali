.class public Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;
.super Ljava/lang/Object;
.source "AndroidDeviceCatalog.java"


# instance fields
.field private android_device_id:Ljava/lang/String;

.field private android_device_security_patch:Ljava/lang/String;

.field private android_device_version_incremental:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setId(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;->android_device_id:Ljava/lang/String;

    return-object p0
.end method

.method public setSecurityPatch(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;->android_device_security_patch:Ljava/lang/String;

    return-object p0
.end method

.method public setVersionIncremental(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/AndroidDeviceCatalog;->android_device_version_incremental:Ljava/lang/String;

    return-object p0
.end method
