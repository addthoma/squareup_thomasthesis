.class public Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
.super Ljava/lang/Object;
.source "MobileAppCatalog.java"


# instance fields
.field private mobile_app_build_sha:Ljava/lang/String;

.field private mobile_app_build_type:Ljava/lang/String;

.field private mobile_app_installation_id:Ljava/lang/String;

.field private mobile_app_package_name:Ljava/lang/String;

.field private mobile_app_version_code:Ljava/lang/String;

.field private mobile_app_version_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setBuildSha(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->mobile_app_build_sha:Ljava/lang/String;

    return-object p0
.end method

.method public setBuildType(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
    .locals 0

    .line 30
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->mobile_app_build_type:Ljava/lang/String;

    return-object p0
.end method

.method public setInstallationID(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->mobile_app_installation_id:Ljava/lang/String;

    return-object p0
.end method

.method public setPackageName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->mobile_app_package_name:Ljava/lang/String;

    return-object p0
.end method

.method public setVersionCode(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->mobile_app_version_code:Ljava/lang/String;

    return-object p0
.end method

.method public setVersionName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;
    .locals 0

    .line 15
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/MobileAppCatalog;->mobile_app_version_name:Ljava/lang/String;

    return-object p0
.end method
