.class public interface abstract Lcom/squareup/eventstream/DroppedEventsFactory;
.super Ljava/lang/Object;
.source "DroppedEventsFactory.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008f\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002J+\u0010\u0003\u001a\u00028\u00002\u0014\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00052\u0006\u0010\u0008\u001a\u00020\u0007H&\u00a2\u0006\u0002\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/eventstream/DroppedEventsFactory;",
        "T",
        "",
        "createDroppedEventsEvent",
        "counts",
        "",
        "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
        "",
        "batchSize",
        "(Ljava/util/Map;I)Ljava/lang/Object;",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createDroppedEventsEvent(Ljava/util/Map;I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lcom/squareup/eventstream/DroppedEventCounter$DropType;",
            "Ljava/lang/Integer;",
            ">;I)TT;"
        }
    .end annotation
.end method
