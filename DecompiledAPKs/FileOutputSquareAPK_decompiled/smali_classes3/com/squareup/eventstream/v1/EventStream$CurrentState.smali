.class public final Lcom/squareup/eventstream/v1/EventStream$CurrentState;
.super Ljava/lang/Object;
.source "EventStream.java"

# interfaces
.implements Lcom/squareup/eventstream/CommonProperties;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/v1/EventStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CurrentState"
.end annotation


# instance fields
.field volatile advertisingId:Ljava/lang/String;

.field final commonProperties:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field volatile locale:Ljava/util/Locale;

.field volatile location:Landroid/location/Location;

.field volatile subject:Lcom/squareup/protos/eventstream/v1/Subject;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public clearCommonProperty(Ljava/lang/String;)V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    monitor-enter v0

    .line 202
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method getCommonProperties()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 208
    iget-object v0, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    monitor-enter v0

    .line 209
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    invoke-direct {v1, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    .line 211
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setAdvertisingId(Ljava/lang/String;)V
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->advertisingId:Ljava/lang/String;

    return-void
.end method

.method public setCommonProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 194
    iget-object v0, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    monitor-enter v0

    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->commonProperties:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->locale:Ljava/util/Locale;

    return-void
.end method

.method public setLocation(Landroid/location/Location;)V
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->location:Landroid/location/Location;

    return-void
.end method

.method public setSubject(Lcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/eventstream/v1/EventStream$CurrentState;->subject:Lcom/squareup/protos/eventstream/v1/Subject;

    return-void
.end method
