.class public abstract Lcom/squareup/eventstream/v1/EventStreamEvent;
.super Ljava/lang/Object;
.source "EventStreamEvent.java"


# instance fields
.field public final transient name:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public final transient overrideSubject:Lcom/squareup/protos/eventstream/v1/Subject;

.field public transient overrideTimestamp:Lcom/squareup/protos/common/time/DateTime;

.field public final transient rawBytes:Lokio/ByteString;

.field public final transient value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/eventstream/v1/Subject;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/eventstream/v1/Subject;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/eventstream/v1/EventStreamEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 63
    iput-object p2, p0, Lcom/squareup/eventstream/v1/EventStreamEvent;->value:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/squareup/eventstream/v1/EventStreamEvent;->rawBytes:Lokio/ByteString;

    .line 65
    iput-object p4, p0, Lcom/squareup/eventstream/v1/EventStreamEvent;->overrideSubject:Lcom/squareup/protos/eventstream/v1/Subject;

    return-void
.end method
