.class public final enum Lcom/squareup/eventstream/v1/EventStream$Name;
.super Ljava/lang/Enum;
.source "EventStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/eventstream/v1/EventStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Name"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum BANDIT_ASSIGNMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum CRASH:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum FINISHED_PAYMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum IMPRESSION:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum LOADED:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum READER:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum SCALE:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum SELECT:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum STATUS:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum TUTORIAL_STEP:Lcom/squareup/eventstream/v1/EventStream$Name;

.field public static final enum VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;


# instance fields
.field public final loggingName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 40
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v1, 0x0

    const-string v2, "ACTION"

    const-string v3, "Action"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 42
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v2, 0x1

    const-string v3, "ERROR"

    const-string v4, "Error"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 44
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v3, 0x2

    const-string v4, "FINISHED_PAYMENT"

    const-string v5, "Finished Payment"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->FINISHED_PAYMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 46
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v4, 0x3

    const-string v5, "READER"

    const-string v6, "Reader"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 51
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v5, 0x4

    const-string v6, "SELECT"

    const-string v7, "Select"

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->SELECT:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 53
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v6, 0x5

    const-string v7, "STATUS"

    const-string v8, "Status"

    invoke-direct {v0, v7, v6, v8}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->STATUS:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 60
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v7, 0x6

    const-string v8, "TAP"

    const-string v9, "Tap"

    invoke-direct {v0, v8, v7, v9}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 62
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v8, 0x7

    const-string v9, "TIMING"

    const-string v10, "Timing"

    invoke-direct {v0, v9, v8, v10}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 64
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v9, 0x8

    const-string v10, "TUTORIAL_STEP"

    const-string v11, "Tutorial Step"

    invoke-direct {v0, v10, v9, v11}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TUTORIAL_STEP:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 66
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v10, 0x9

    const-string v11, "VIEW"

    const-string v12, "View"

    invoke-direct {v0, v11, v10, v12}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 68
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v11, 0xa

    const-string v12, "LOADED"

    const-string v13, "Loaded"

    invoke-direct {v0, v12, v11, v13}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->LOADED:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 70
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v12, 0xb

    const-string v13, "BANDIT_ASSIGNMENT"

    const-string v14, "BanditAssignment"

    invoke-direct {v0, v13, v12, v14}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->BANDIT_ASSIGNMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 75
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v13, 0xc

    const-string v14, "CRASH"

    const-string v15, "Crash"

    invoke-direct {v0, v14, v13, v15}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->CRASH:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 80
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v14, 0xd

    const-string v15, "IMPRESSION"

    const-string v13, "Impression"

    invoke-direct {v0, v15, v14, v13}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->IMPRESSION:Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 82
    new-instance v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v13, 0xe

    const-string v15, "SCALE"

    const-string v14, "Scale"

    invoke-direct {v0, v15, v13, v14}, Lcom/squareup/eventstream/v1/EventStream$Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->SCALE:Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/eventstream/v1/EventStream$Name;

    .line 35
    sget-object v14, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ERROR:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->FINISHED_PAYMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->SELECT:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->STATUS:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->TIMING:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->TUTORIAL_STEP:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->LOADED:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->BANDIT_ASSIGNMENT:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->CRASH:Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->IMPRESSION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->SCALE:Lcom/squareup/eventstream/v1/EventStream$Name;

    aput-object v1, v0, v13

    sput-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->$VALUES:[Lcom/squareup/eventstream/v1/EventStream$Name;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 88
    iput-object p3, p0, Lcom/squareup/eventstream/v1/EventStream$Name;->loggingName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/eventstream/v1/EventStream$Name;
    .locals 1

    .line 35
    const-class v0, Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/eventstream/v1/EventStream$Name;

    return-object p0
.end method

.method public static values()[Lcom/squareup/eventstream/v1/EventStream$Name;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->$VALUES:[Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-virtual {v0}, [Lcom/squareup/eventstream/v1/EventStream$Name;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/eventstream/v1/EventStream$Name;

    return-object v0
.end method
