.class final Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromTelephony$1;
.super Ljava/lang/Object;
.source "AndroidGeoCountryCodeGuesser.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/location/AndroidGeoCountryCodeGuesser;-><init>(Lcom/squareup/core/location/providers/AddressProvider;Lcom/squareup/util/Clock;Ljavax/inject/Provider;Landroid/telephony/TelephonyManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Pair;",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "Lcom/squareup/CountryCode;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/location/AndroidGeoCountryCodeGuesser;


# direct methods
.method constructor <init>(Lcom/squareup/location/AndroidGeoCountryCodeGuesser;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromTelephony$1;->this$0:Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromTelephony$1;->call()Lkotlin/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final call()Lkotlin/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/Pair<",
            "Lcom/squareup/location/CountryGuesser$Result;",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/location/AndroidGeoCountryCodeGuesser$guessFromTelephony$1;->this$0:Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    invoke-virtual {v0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesser;->tryTelephony()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 41
    new-instance v1, Lkotlin/Pair;

    invoke-static {v0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesserKt;->access$toResult(Lcom/squareup/CountryCode;)Lcom/squareup/location/CountryGuesser$Result;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method
