.class public final Lcom/squareup/comms/protos/seller/TipRequirements$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipRequirements.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/TipRequirements;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/TipRequirements;",
        "Lcom/squareup/comms/protos/seller/TipRequirements$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0015\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u000f\u001a\u00020\u0002H\u0016J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\u0010J\u0015\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u000eJ\u0014\u0010\u000b\u001a\u00020\u00002\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/TipRequirements$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/TipRequirements;",
        "()V",
        "allow_custom_tip",
        "",
        "Ljava/lang/Boolean;",
        "max_custom_tip",
        "",
        "Ljava/lang/Long;",
        "sign_on_printed_receipt",
        "tip_options",
        "",
        "Lcom/squareup/comms/protos/seller/TipOption;",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/TipRequirements$Builder;",
        "build",
        "(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/TipRequirements$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public allow_custom_tip:Ljava/lang/Boolean;

.field public max_custom_tip:Ljava/lang/Long;

.field public sign_on_printed_receipt:Ljava/lang/Boolean;

.field public tip_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/TipOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 124
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->tip_options:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final allow_custom_tip(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/TipRequirements$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->allow_custom_tip:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/TipRequirements;
    .locals 7

    .line 170
    new-instance v6, Lcom/squareup/comms/protos/seller/TipRequirements;

    .line 171
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->tip_options:Ljava/util/List;

    .line 172
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->allow_custom_tip:Ljava/lang/Boolean;

    .line 173
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->max_custom_tip:Ljava/lang/Long;

    .line 174
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->sign_on_printed_receipt:Ljava/lang/Boolean;

    .line 175
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 170
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/seller/TipRequirements;-><init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 122
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->build()Lcom/squareup/comms/protos/seller/TipRequirements;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final max_custom_tip(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/TipRequirements$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->max_custom_tip:Ljava/lang/Long;

    return-object p0
.end method

.method public final sign_on_printed_receipt(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/TipRequirements$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->sign_on_printed_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final tip_options(Ljava/util/List;)Lcom/squareup/comms/protos/seller/TipRequirements$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/seller/TipOption;",
            ">;)",
            "Lcom/squareup/comms/protos/seller/TipRequirements$Builder;"
        }
    .end annotation

    const-string/jumbo v0, "tip_options"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 141
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TipRequirements$Builder;->tip_options:Ljava/util/List;

    return-object p0
.end method
