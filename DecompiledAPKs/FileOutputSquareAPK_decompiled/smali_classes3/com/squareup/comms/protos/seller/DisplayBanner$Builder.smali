.class public final Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayBanner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/DisplayBanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/DisplayBanner;",
        "Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0005J\u0015\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000cR\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/DisplayBanner;",
        "()V",
        "item_client_id",
        "",
        "name",
        "price",
        "quantity",
        "",
        "Ljava/lang/Integer;",
        "build",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public item_client_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public quantity:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 98
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/DisplayBanner;
    .locals 7

    .line 131
    new-instance v6, Lcom/squareup/comms/protos/seller/DisplayBanner;

    .line 132
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->name:Ljava/lang/String;

    .line 133
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->price:Ljava/lang/String;

    .line 134
    iget-object v3, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->item_client_id:Ljava/lang/String;

    .line 135
    iget-object v4, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->quantity:Ljava/lang/Integer;

    .line 136
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    .line 131
    invoke-direct/range {v0 .. v5}, Lcom/squareup/comms/protos/seller/DisplayBanner;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 98
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->build()Lcom/squareup/comms/protos/seller/DisplayBanner;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final item_client_id(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->item_client_id:Ljava/lang/String;

    return-object p0
.end method

.method public final name(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public final price(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->price:Ljava/lang/String;

    return-object p0
.end method

.method public final quantity(Ljava/lang/Integer;)Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/DisplayBanner$Builder;->quantity:Ljava/lang/Integer;

    return-object p0
.end method
