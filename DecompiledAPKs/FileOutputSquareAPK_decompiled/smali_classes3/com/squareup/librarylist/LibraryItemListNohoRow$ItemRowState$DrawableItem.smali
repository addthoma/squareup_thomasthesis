.class public final Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;
.super Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;
.source "LibraryItemListNohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DrawableItem"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\nH\u00c6\u0003JE\u0010\u0019\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u00072\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0003H\u00d6\u0001R\u0014\u0010\u0005\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0010R\u0014\u0010\u0008\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0010R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\rR\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\r\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        "itemName",
        "",
        "optionNote",
        "amount",
        "isChevronVisible",
        "",
        "isTextTile",
        "drawable",
        "Landroid/graphics/drawable/Drawable;",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;)V",
        "getAmount",
        "()Ljava/lang/String;",
        "getDrawable",
        "()Landroid/graphics/drawable/Drawable;",
        "()Z",
        "getItemName",
        "getOptionNote",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Ljava/lang/String;

.field private final drawable:Landroid/graphics/drawable/Drawable;

.field private final isChevronVisible:Z

.field private final isTextTile:Z

.field private final itemName:Ljava/lang/String;

.field private final optionNote:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;)V
    .locals 1

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionNote"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawable"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->itemName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->optionNote:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->amount:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible:Z

    iput-boolean p5, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile:Z

    iput-object p6, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;ILjava/lang/Object;)Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getItemName()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getOptionNote()Ljava/lang/String;

    move-result-object p2

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getAmount()Ljava/lang/String;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible()Z

    move-result p4

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile()Z

    move-result p5

    :cond_4
    move v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move-object p5, v0

    move p6, v1

    move p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;)Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getOptionNote()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getAmount()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible()Z

    move-result v0

    return v0
.end method

.method public final component5()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile()Z

    move-result v0

    return v0
.end method

.method public final component6()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;)Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;
    .locals 8

    const-string v0, "itemName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "optionNote"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawable"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getItemName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getOptionNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getOptionNote()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getAmount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getAmount()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    iget-object p1, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAmount()Ljava/lang/String;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getItemName()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->itemName:Ljava/lang/String;

    return-object v0
.end method

.method public getOptionNote()Ljava/lang/String;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->optionNote:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getItemName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getOptionNote()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getAmount()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :cond_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public isChevronVisible()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible:Z

    return v0
.end method

.method public isTextTile()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DrawableItem(itemName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getItemName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", optionNote="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getOptionNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getAmount()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", isChevronVisible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isChevronVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isTextTile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->isTextTile()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", drawable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
