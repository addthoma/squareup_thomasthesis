.class final Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;
.super Ljava/lang/Object;
.source "LibraryItemListNohoRow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;)V
    .locals 2

    .line 157
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;

    iget-object v0, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-static {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->access$getThumbnailLoader$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)Lcom/squareup/librarylist/ThumbnailLoader;

    move-result-object v0

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableItem;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/ThumbnailLoader;->loadThumbnail(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 158
    :cond_0
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;

    iget-object v0, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-static {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->access$getThumbnailLoader$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)Lcom/squareup/librarylist/ThumbnailLoader;

    move-result-object v0

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$TextIconItem;->getIconText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/ThumbnailLoader;->loadThumbnail(Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_1
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableActionItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;

    iget-object v0, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-static {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->access$getThumbnailLoader$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)Lcom/squareup/librarylist/ThumbnailLoader;

    move-result-object v0

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableActionItem;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$DrawableActionItem;->getDrawableRes()I

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/librarylist/ThumbnailLoader;->loadThumbnail(I)V

    goto :goto_0

    .line 160
    :cond_2
    instance-of v0, p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;

    iget-object v0, v0, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2;->this$0:Lcom/squareup/librarylist/LibraryItemListNohoRow;

    invoke-static {v0}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->access$getThumbnailLoader$p(Lcom/squareup/librarylist/LibraryItemListNohoRow;)Lcom/squareup/librarylist/ThumbnailLoader;

    move-result-object v0

    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;->getEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState$ItemPhotoItem;->getItemPhotos()Lcom/squareup/ui/photo/ItemPhoto$Factory;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/librarylist/ThumbnailLoader;->loadThumbnail(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;)V

    :cond_3
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/LibraryItemListNohoRow$onAttachedToWindow$2$1;->accept(Lcom/squareup/librarylist/LibraryItemListNohoRow$ItemRowState;)V

    return-void
.end method
