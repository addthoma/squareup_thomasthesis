.class public final Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;
.super Ljava/lang/Object;
.source "ItemSuggestionsManager_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/librarylist/ItemSuggestionsManager;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final savedItemSuggestionsPrefProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final userSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->userSettingsProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->savedItemSuggestionsPrefProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->busProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;)",
            "Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;)Lcom/squareup/librarylist/ItemSuggestionsManager;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/UserSettingsProvider;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/badbus/BadBus;",
            ")",
            "Lcom/squareup/librarylist/ItemSuggestionsManager;"
        }
    .end annotation

    .line 58
    new-instance v6, Lcom/squareup/librarylist/ItemSuggestionsManager;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/librarylist/ItemSuggestionsManager;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/librarylist/ItemSuggestionsManager;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->userSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/UserSettingsProvider;

    iget-object v2, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->savedItemSuggestionsPrefProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v3, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/badbus/BadBus;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/UserSettingsProvider;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;Lcom/squareup/badbus/BadBus;)Lcom/squareup/librarylist/ItemSuggestionsManager;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/librarylist/ItemSuggestionsManager_Factory;->get()Lcom/squareup/librarylist/ItemSuggestionsManager;

    move-result-object v0

    return-object v0
.end method
