.class final Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLibraryListPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/librarylist/LibraryListView$Action;",
        "+",
        "Lcom/squareup/librarylist/LibraryListResults;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012F\u0010\u0002\u001aB\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006 \u0005* \u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/librarylist/LibraryListView$Action;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/librarylist/LibraryListResults;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;->this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/librarylist/LibraryListView$Action;",
            "Lcom/squareup/librarylist/LibraryListResults;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListView$Action;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/librarylist/LibraryListResults;

    .line 67
    instance-of v1, v0, Lcom/squareup/librarylist/LibraryListView$Action$OpenItemsApplet;

    if-eqz v1, :cond_0

    .line 68
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;->this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;

    invoke-static {p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->access$getLibraryListAssistant$p(Lcom/squareup/librarylist/RealLibraryListPresenter;)Lcom/squareup/librarylist/LibraryListAssistant;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListAssistant;->openItemsApplet()V

    goto :goto_0

    .line 70
    :cond_0
    instance-of v1, v0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    if-eqz v1, :cond_1

    .line 71
    check-cast v0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->component1()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemClicked;->component2()I

    move-result v0

    .line 72
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;->this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->getCurrentState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getSearchQuery()Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState$SearchQuery;->getSearchText()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, v1, v0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->access$libraryListItemClicked(Lcom/squareup/librarylist/RealLibraryListPresenter;Landroid/view/View;ILjava/lang/String;)V

    goto :goto_0

    .line 74
    :cond_1
    instance-of v1, v0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;

    if-eqz v1, :cond_2

    .line 75
    check-cast v0, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;->component1()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListView$Action$LibraryItemLongClicked;->component2()I

    move-result v0

    .line 76
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListPresenter$onLoad$1;->this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListResults;->getCurrentState()Lcom/squareup/librarylist/LibraryListState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState;->getSearchQuery()Lcom/squareup/librarylist/LibraryListState$SearchQuery;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListState$SearchQuery;->getSearchText()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, v1, v0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->access$libraryListItemLongClicked(Lcom/squareup/librarylist/RealLibraryListPresenter;Landroid/view/View;ILjava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method
