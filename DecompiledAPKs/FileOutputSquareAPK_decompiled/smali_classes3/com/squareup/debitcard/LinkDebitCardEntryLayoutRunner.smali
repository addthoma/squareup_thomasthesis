.class public final Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;
.super Ljava/lang/Object;
.source "LinkDebitCardEntryLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Factory;,
        Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/debitcard/LinkDebitCardEntryScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002!\"B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0008\u0010\u0018\u001a\u00020\u0013H\u0002J\u001e\u0010\u0019\u001a\u00020\u00132\u0014\u0008\u0002\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00130\u001bH\u0002J\u0018\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020\u0013H\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/debitcard/LinkDebitCardEntryScreen;",
        "view",
        "Landroid/view/View;",
        "analytics",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "(Landroid/view/View;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "cardEditor",
        "Lcom/squareup/register/widgets/card/CardEditor;",
        "instructions",
        "Lcom/squareup/widgets/MessageView;",
        "getCardData",
        "Lcom/squareup/protos/client/bills/CardData;",
        "card",
        "Lcom/squareup/Card;",
        "onCancel",
        "",
        "screen",
        "onLink",
        "onScreen",
        "setUpActionBar",
        "setUpCardEditor",
        "setUpCardEditorListener",
        "onChargeCard",
        "Lkotlin/Function1;",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateLinkButton",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

.field private final instructions:Lcom/squareup/widgets/MessageView;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    .line 30
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    const-string p2, "ActionBarView.findIn(view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 31
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/debitcard/impl/R$id;->debit_card_input_editor:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.debit_card_input_editor)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/register/widgets/card/CardEditor;

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    .line 32
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/debitcard/impl/R$id;->instructions:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.instructions)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->instructions:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$onCancel(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->onCancel(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    return-void
.end method

.method public static final synthetic access$onLink(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->onLink(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    return-void
.end method

.method public static final synthetic access$updateLinkButton(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->updateLinkButton()V

    return-void
.end method

.method private final getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;
    .locals 3

    .line 84
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;-><init>()V

    .line 85
    invoke-virtual {p1}, Lcom/squareup/Card;->getExpirationMonth()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->month(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object v0

    .line 86
    invoke-virtual {p1}, Lcom/squareup/Card;->getExpirationYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->year(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;-><init>()V

    .line 89
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->card_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v1

    .line 90
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->expiry(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {p1}, Lcom/squareup/Card;->getVerification()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->cvv(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {p1}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$KeyedCard$Builder;->build()Lcom/squareup/protos/client/bills/CardData$KeyedCard;

    move-result-object p1

    .line 94
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$Builder;-><init>()V

    .line 95
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->keyed_card(Lcom/squareup/protos/client/bills/CardData$KeyedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$Builder;->build()Lcom/squareup/protos/client/bills/CardData;

    move-result-object p1

    const-string v0, "CardData.Builder()\n     \u2026yedCard)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onCancel(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;->getHasLinkedCard()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->cancelCardLinking(Z)V

    .line 75
    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;->getOnCancelCardEntryClicked()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method private final onLink(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;->getOnLinkCardClicked()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final onScreen(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 3

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->setUpActionBar(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    .line 54
    new-instance v0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$onScreen$1;-><init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->setUpCardEditorListener(Lkotlin/jvm/functions/Function1;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object p1

    new-instance v1, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    check-cast v1, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/register/widgets/card/CardEditor;->init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    sget v0, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card_editor_hint:I

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(I)V

    .line 58
    iget-object p1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->instructions:Lcom/squareup/widgets/MessageView;

    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card_instructions:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V
    .locals 5

    .line 62
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 64
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 65
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 66
    new-instance v3, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpActionBar$1;-><init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 67
    sget v3, Lcom/squareup/debitcard/R$string;->link:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 68
    new-instance v2, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpActionBar$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpActionBar$2;-><init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 69
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setUpCardEditor()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 43
    invoke-static {p0, v0, v1, v0}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->setUpCardEditorListener$default(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CardEditor;->requestFocus()Z

    .line 45
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    new-instance v1, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    check-cast v1, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    return-void
.end method

.method private final setUpCardEditorListener(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/Card;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    new-instance v1, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpCardEditorListener$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpCardEditorListener$2;-><init>(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/register/widgets/card/OnCardListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setOnCardListener(Lcom/squareup/register/widgets/card/OnCardListener;)V

    return-void
.end method

.method static synthetic setUpCardEditorListener$default(Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 100
    sget-object p1, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpCardEditorListener$1;->INSTANCE:Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner$setUpCardEditorListener$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->setUpCardEditorListener(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateLinkButton()V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->cardEditor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/CardEditor;->getCard()Lcom/squareup/Card;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->setUpCardEditor()V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->onScreen(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/debitcard/LinkDebitCardEntryScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/LinkDebitCardEntryLayoutRunner;->showRendering(Lcom/squareup/debitcard/LinkDebitCardEntryScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
