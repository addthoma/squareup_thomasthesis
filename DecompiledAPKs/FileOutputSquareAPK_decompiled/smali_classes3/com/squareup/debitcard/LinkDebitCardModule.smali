.class public abstract Lcom/squareup/debitcard/LinkDebitCardModule;
.super Ljava/lang/Object;
.source "LinkDebitCardModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/debitcard/LinkDebitCardModule;",
        "",
        "()V",
        "bindDebitCardSettings",
        "Lcom/squareup/debitcard/DebitCardSettings;",
        "realDebitCardSettings",
        "Lcom/squareup/debitcard/RealDebitCardSettings;",
        "bindLinkDebitCardViewFactory",
        "Lcom/squareup/debitcard/LinkDebitCardViewFactory;",
        "viewFactory",
        "Lcom/squareup/debitcard/RealLinkDebitCardViewFactory;",
        "bindLinkDebitCardWorkflow",
        "Lcom/squareup/debitcard/LinkDebitCardWorkflow;",
        "workflow",
        "Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindDebitCardSettings(Lcom/squareup/debitcard/RealDebitCardSettings;)Lcom/squareup/debitcard/DebitCardSettings;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindLinkDebitCardViewFactory(Lcom/squareup/debitcard/RealLinkDebitCardViewFactory;)Lcom/squareup/debitcard/LinkDebitCardViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindLinkDebitCardWorkflow(Lcom/squareup/debitcard/RealLinkDebitCardWorkflow;)Lcom/squareup/debitcard/LinkDebitCardWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
