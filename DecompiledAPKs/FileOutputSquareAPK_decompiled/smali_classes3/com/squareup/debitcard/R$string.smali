.class public final Lcom/squareup/debitcard/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/debitcard/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final card_linking_failed:I = 0x7f120310

.field public static final instant_deposits_app_update_message:I = 0x7f120c10

.field public static final instant_deposits_app_update_title:I = 0x7f120c11

.field public static final instant_deposits_app_update_update:I = 0x7f120c12

.field public static final instant_deposits_card_linked:I = 0x7f120c17

.field public static final instant_deposits_card_linked_message:I = 0x7f120c18

.field public static final instant_deposits_check_your_inbox:I = 0x7f120c19

.field public static final instant_deposits_link_card_url:I = 0x7f120c21

.field public static final instant_deposits_link_debit_card:I = 0x7f120c22

.field public static final instant_deposits_link_debit_card_editor_hint:I = 0x7f120c23

.field public static final instant_deposits_link_debit_card_helper_text:I = 0x7f120c24

.field public static final instant_deposits_link_debit_card_instructions:I = 0x7f120c25

.field public static final instant_deposits_linking_debit_card:I = 0x7f120c28

.field public static final instant_deposits_resend_email:I = 0x7f120c2d

.field public static final instant_deposits_try_another_debit_card:I = 0x7f120c34

.field public static final instant_deposits_verification_email_sent:I = 0x7f120c3a

.field public static final instant_deposits_verification_email_sent_message:I = 0x7f120c3b

.field public static final link:I = 0x7f120ecb

.field public static final play_store_intent_uri:I = 0x7f12143c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
