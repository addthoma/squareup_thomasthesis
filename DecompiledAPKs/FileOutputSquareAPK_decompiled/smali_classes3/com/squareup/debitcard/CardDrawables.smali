.class public final Lcom/squareup/debitcard/CardDrawables;
.super Ljava/lang/Object;
.source "CardDrawables.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a \u0010\u0000\u001a\u00020\u00012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0007\u001a\u0012\u0010\u0006\u001a\u00020\u00012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0003\u00a8\u0006\u0007"
    }
    d2 = {
        "cardDrawable",
        "",
        "brand",
        "Lcom/squareup/Card$Brand;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "unbrandedCardDrawable",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final cardDrawable(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 1

    if-nez p0, :cond_0

    goto :goto_0

    .line 25
    :cond_0
    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    goto :goto_0

    .line 34
    :pswitch_0
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_gift_card_24:I

    goto :goto_1

    .line 33
    :pswitch_1
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_union_pay_24:I

    goto :goto_1

    .line 32
    :pswitch_2
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_jcb_24:I

    goto :goto_1

    .line 31
    :pswitch_3
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_interac_24:I

    goto :goto_1

    .line 30
    :pswitch_4
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_diners_club_24:I

    goto :goto_1

    .line 29
    :pswitch_5
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_discover_24:I

    goto :goto_1

    .line 28
    :pswitch_6
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_amex_24:I

    goto :goto_1

    .line 27
    :pswitch_7
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_mastercard_24:I

    goto :goto_1

    .line 26
    :pswitch_8
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_visa_24:I

    goto :goto_1

    .line 35
    :goto_0
    invoke-static {p1}, Lcom/squareup/debitcard/CardDrawables;->unbrandedCardDrawable(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    :goto_1
    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static synthetic cardDrawable$default(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;ILjava/lang/Object;)I
    .locals 1

    and-int/lit8 p3, p2, 0x1

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    .line 23
    move-object p0, v0

    check-cast p0, Lcom/squareup/Card$Brand;

    :cond_0
    and-int/lit8 p2, p2, 0x2

    if-eqz p2, :cond_1

    .line 24
    move-object p1, v0

    check-cast p1, Lcom/squareup/protos/common/CurrencyCode;

    :cond_1
    invoke-static {p0, p1}, Lcom/squareup/debitcard/CardDrawables;->cardDrawable(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0
.end method

.method private static final unbrandedCardDrawable(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 1

    if-nez p0, :cond_0

    goto :goto_0

    .line 39
    :cond_0
    sget-object v0, Lcom/squareup/debitcard/CardDrawables$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    .line 42
    :goto_0
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_swipe_24:I

    goto :goto_1

    .line 41
    :cond_1
    sget p0, Lcom/squareup/vectoricons/R$drawable;->payment_card_chip_24:I

    :goto_1
    return p0
.end method
