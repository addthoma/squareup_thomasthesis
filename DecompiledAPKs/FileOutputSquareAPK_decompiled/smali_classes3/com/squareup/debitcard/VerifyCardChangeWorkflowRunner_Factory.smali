.class public final Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "VerifyCardChangeWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/VerifyCardChangeViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/VerifyCardChangeWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/VerifyCardChangeViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/VerifyCardChangeWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p3, p0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/VerifyCardChangeViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/debitcard/VerifyCardChangeWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/debitcard/VerifyCardChangeViewFactory;Lcom/squareup/debitcard/VerifyCardChangeWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;-><init>(Lcom/squareup/debitcard/VerifyCardChangeViewFactory;Lcom/squareup/debitcard/VerifyCardChangeWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;
    .locals 3

    .line 28
    iget-object v0, p0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/debitcard/VerifyCardChangeViewFactory;

    iget-object v1, p0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/debitcard/VerifyCardChangeWorkflow;

    iget-object v2, p0, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0, v1, v2}, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->newInstance(Lcom/squareup/debitcard/VerifyCardChangeViewFactory;Lcom/squareup/debitcard/VerifyCardChangeWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner_Factory;->get()Lcom/squareup/debitcard/VerifyCardChangeWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
