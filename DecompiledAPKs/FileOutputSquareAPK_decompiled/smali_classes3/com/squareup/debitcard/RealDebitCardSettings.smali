.class public final Lcom/squareup/debitcard/RealDebitCardSettings;
.super Ljava/lang/Object;
.source "RealDebitCardSettings.kt"

# interfaces
.implements Lcom/squareup/debitcard/DebitCardSettings;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0002J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0016\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0016\u0010\u0013\u001a\u00020\u000f2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015H\u0002J\u0010\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002J\u0016\u0010\u001b\u001a\u00020\u000f2\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u0015H\u0002J\u0010\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u001eH\u0002J\u000e\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/debitcard/RealDebitCardSettings;",
        "Lcom/squareup/debitcard/DebitCardSettings;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "linkDebitCardService",
        "Lcom/squareup/debitcard/LinkDebitCardService;",
        "messageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "analytics",
        "Lcom/squareup/instantdeposit/InstantDepositAnalytics;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)V",
        "hasLinkedCard",
        "",
        "linkCard",
        "Lio/reactivex/Single;",
        "Lcom/squareup/debitcard/DebitCardSettings$State;",
        "linkCardRequest",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
        "onLinkCard",
        "onLinkCardFailure",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;",
        "onLinkCardSuccess",
        "state",
        "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;",
        "onResendEmail",
        "onResendEmailFailure",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse;",
        "onResendEmailSuccess",
        "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;",
        "resendEmail",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

.field private final linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/instantdeposit/InstantDepositAnalytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "linkDebitCardService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

    iput-object p3, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p4, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    return-void
.end method

.method public static final synthetic access$onLinkCardFailure(Lcom/squareup/debitcard/RealDebitCardSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealDebitCardSettings;->onLinkCardFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/DebitCardSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLinkCardSuccess(Lcom/squareup/debitcard/RealDebitCardSettings;Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealDebitCardSettings;->onLinkCardSuccess(Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;)Lcom/squareup/debitcard/DebitCardSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onResendEmailFailure(Lcom/squareup/debitcard/RealDebitCardSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealDebitCardSettings;->onResendEmailFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/DebitCardSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onResendEmailSuccess(Lcom/squareup/debitcard/RealDebitCardSettings;Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealDebitCardSettings;->onResendEmailSuccess(Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;)Lcom/squareup/debitcard/DebitCardSettings$State;

    move-result-object p0

    return-object p0
.end method

.method private final hasLinkedCard()Z
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->hasLinkedCard()Z

    move-result v0

    return v0
.end method

.method private final onLinkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/DebitCardSettings$State;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

    invoke-interface {v0, p1}, Lcom/squareup/debitcard/LinkDebitCardService;->linkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 42
    new-instance v0, Lcom/squareup/debitcard/RealDebitCardSettings$onLinkCard$1;

    invoke-direct {v0, p0}, Lcom/squareup/debitcard/RealDebitCardSettings$onLinkCard$1;-><init>(Lcom/squareup/debitcard/RealDebitCardSettings;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "linkDebitCardService.lin\u2026it)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onLinkCardFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;",
            ">;)",
            "Lcom/squareup/debitcard/DebitCardSettings$State;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/debitcard/R$string;->card_linking_failed:I

    sget-object v2, Lcom/squareup/debitcard/RealDebitCardSettings$onLinkCardFailure$failureMessage$1;->INSTANCE:Lcom/squareup/debitcard/RealDebitCardSettings$onLinkCardFailure$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v6

    .line 74
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 75
    instance-of v9, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz v9, :cond_0

    .line 77
    move-object v2, p1

    check-cast v2, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {v2}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;

    iget-object v2, v2, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;->state:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    sget-object v3, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;->UNSUPPORTED:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    if-ne v2, v3, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    :goto_0
    if-eqz v9, :cond_1

    .line 82
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse;->state:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    sget-object v2, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;->CLIENT_UPGRADE_REQUIRED:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    if-ne p1, v2, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    .line 86
    :goto_1
    iget-object p1, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-direct {p0}, Lcom/squareup/debitcard/RealDebitCardSettings;->hasLinkedCard()Z

    move-result v0

    invoke-interface {p1, v0, v9}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tryToLinkCard(ZZ)V

    .line 87
    iget-object p1, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-direct {p0}, Lcom/squareup/debitcard/RealDebitCardSettings;->hasLinkedCard()Z

    move-result v0

    invoke-virtual {v6}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->cardLinkFailure(ZLjava/lang/String;Ljava/lang/String;)V

    .line 88
    new-instance p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    .line 89
    sget-object v4, Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;->FAILURE:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    const/4 v5, 0x0

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v3, p1

    .line 88
    invoke-direct/range {v3 .. v11}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final onLinkCardSuccess(Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 11

    .line 62
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-direct {p0}, Lcom/squareup/debitcard/RealDebitCardSettings;->hasLinkedCard()Z

    move-result v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->tryToLinkCard(ZZ)V

    .line 63
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-direct {p0}, Lcom/squareup/debitcard/RealDebitCardSettings;->hasLinkedCard()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->cardLinkSuccess(Z)V

    .line 64
    new-instance v0, Lcom/squareup/debitcard/DebitCardSettings$State;

    sget-object v1, Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;->VERIFICATION_PENDING:Lcom/squareup/protos/client/instantdeposits/LinkCardResponse$State;

    if-ne p1, v1, :cond_0

    sget-object p1, Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;->PENDING:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;->SUCCESS:Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;

    :goto_0
    move-object v3, p1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v10}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final onResendEmail()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/DebitCardSettings$State;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

    new-instance v1, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;

    invoke-direct {v1}, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/debitcard/LinkDebitCardService;->resendVerificationEmail(Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/debitcard/RealDebitCardSettings$onResendEmail$1;

    invoke-direct {v1, p0}, Lcom/squareup/debitcard/RealDebitCardSettings$onResendEmail$1;-><init>(Lcom/squareup/debitcard/RealDebitCardSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "linkDebitCardService.res\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onResendEmailFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse;",
            ">;)",
            "Lcom/squareup/debitcard/DebitCardSettings$State;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/common/strings/R$string;->email_resending_failed:I

    sget-object v2, Lcom/squareup/debitcard/RealDebitCardSettings$onResendEmailFailure$failureMessage$1;->INSTANCE:Lcom/squareup/debitcard/RealDebitCardSettings$onResendEmailFailure$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v6

    .line 109
    iget-object p1, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-virtual {v6}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->resendEmailFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    new-instance p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    sget-object v5, Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;->FAILURE:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x39

    const/4 v11, 0x0

    move-object v3, p1

    invoke-direct/range {v3 .. v11}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final onResendEmailSuccess(Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;)Lcom/squareup/debitcard/DebitCardSettings$State;
    .locals 9

    .line 98
    iget-object v0, p0, Lcom/squareup/debitcard/RealDebitCardSettings;->analytics:Lcom/squareup/instantdeposit/InstantDepositAnalytics;

    invoke-interface {v0}, Lcom/squareup/instantdeposit/InstantDepositAnalytics;->resendEmailSuccess()V

    .line 99
    sget-object v0, Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;->SENT:Lcom/squareup/protos/client/instantdeposits/ResendVerificationEmailResponse$VerificationEmailState;

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;->SENT:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;->ALREADY_VERIFIED:Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;

    :goto_0
    move-object v2, p1

    .line 100
    new-instance p1, Lcom/squareup/debitcard/DebitCardSettings$State;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x3d

    const/4 v8, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/debitcard/DebitCardSettings$State;-><init>(Lcom/squareup/debitcard/DebitCardSettings$LinkCardState;Lcom/squareup/debitcard/DebitCardSettings$ResendEmailState;Lcom/squareup/receiving/FailureMessage;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method


# virtual methods
.method public linkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/DebitCardSettings$State;",
            ">;"
        }
    .end annotation

    const-string v0, "linkCardRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/RealDebitCardSettings;->onLinkCard(Lcom/squareup/protos/client/instantdeposits/LinkCardRequest;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public resendEmail()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/debitcard/DebitCardSettings$State;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/debitcard/RealDebitCardSettings;->onResendEmail()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
