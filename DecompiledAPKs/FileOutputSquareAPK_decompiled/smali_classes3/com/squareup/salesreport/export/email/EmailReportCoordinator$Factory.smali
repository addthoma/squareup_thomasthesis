.class public final Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;
.super Ljava/lang/Object;
.source "EmailReportCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/export/email/EmailReportCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0002\u0010\rJ*\u0010\u000e\u001a\u00020\u000f2\"\u0010\u0010\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0012j\u0008\u0012\u0004\u0012\u00020\u0013`\u00150\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;",
        "",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "res",
        "Lcom/squareup/util/Res;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V",
        "create",
        "Lcom/squareup/salesreport/export/email/EmailReportCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/salesreport/export/email/EmailReportScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

.field private final localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "connectivityMonitor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    iput-object p2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    iput-object p3, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/salesreport/export/email/EmailReportCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/salesreport/export/email/EmailReportCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;

    .line 65
    iget-object v2, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    .line 66
    iget-object v3, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->localTimeFormatter:Lcom/squareup/salesreport/util/LocalTimeFormatter;

    .line 67
    iget-object v4, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    .line 68
    iget-object v5, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->res:Lcom/squareup/util/Res;

    .line 69
    iget-object v6, p0, Lcom/squareup/salesreport/export/email/EmailReportCoordinator$Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, v0

    move-object v7, p1

    .line 64
    invoke-direct/range {v1 .. v7}, Lcom/squareup/salesreport/export/email/EmailReportCoordinator;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lio/reactivex/Observable;)V

    return-object v0
.end method
