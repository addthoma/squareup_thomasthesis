.class public final Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "EmployeeSelectionCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/Scheduler;

    iget-object v1, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/recycler/RecyclerFactory;

    iget-object v2, p0, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->newInstance(Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator_Factory_Factory;->get()Lcom/squareup/salesreport/customize/employees/EmployeeSelectionCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
