.class public final Lcom/squareup/salesreport/chart/SalesChartRowsKt;
.super Ljava/lang/Object;
.source "SalesChartRows.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesChartRows.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesChartRows.kt\ncom/squareup/salesreport/chart/SalesChartRowsKt\n*L\n1#1,144:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u001a2\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002\u001a\u001c\u0010\u0013\u001a\u00020\u0014*\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002\"\u001e\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\"\u001e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005\"\u001e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0005\"\u001e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0005\u00a8\u0006\u0016"
    }
    d2 = {
        "dataPoints",
        "",
        "Lcom/squareup/salesreport/chart/SalesDataPoint;",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
        "getDataPoints",
        "(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;",
        "dataPointsByHour",
        "getDataPointsByHour",
        "dataPointsByMonth",
        "getDataPointsByMonth",
        "datePointsByDay",
        "getDatePointsByDay",
        "dataPointsByTimePeriod",
        "startDateTime",
        "Lorg/threeten/bp/LocalDateTime;",
        "endDateTime",
        "comparisonStartDateTime",
        "timeUnit",
        "Lorg/threeten/bp/temporal/TemporalUnit;",
        "isEqual",
        "",
        "dateTime",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final dataPointsByTimePeriod(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Ljava/util/List;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            "Lorg/threeten/bp/LocalDateTime;",
            "Lorg/threeten/bp/LocalDateTime;",
            "Lorg/threeten/bp/LocalDateTime;",
            "Lorg/threeten/bp/temporal/TemporalUnit;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/chart/SalesDataPoint;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    .line 75
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    .line 82
    :goto_0
    invoke-virtual {v0, v4, v5, v1}, Lorg/threeten/bp/LocalDateTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v7

    move-object/from16 v8, p2

    check-cast v8, Lorg/threeten/bp/chrono/ChronoLocalDateTime;

    invoke-virtual {v7, v8}, Lorg/threeten/bp/LocalDateTime;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDateTime;)I

    move-result v7

    if-gtz v7, :cond_2

    .line 83
    invoke-virtual {v0, v4, v5, v1}, Lorg/threeten/bp/LocalDateTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v7

    .line 84
    new-instance v14, Lcom/squareup/salesreport/chart/SalesDataPoint;

    const-string v8, "pointDateTime"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    move-object v8, v14

    move-object v9, v7

    invoke-direct/range {v8 .. v13}, Lcom/squareup/salesreport/chart/SalesDataPoint;-><init>(Lorg/threeten/bp/LocalDateTime;JJ)V

    .line 85
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/customreport/data/WithSalesChartReport;->getChartedSales()Ljava/util/List;

    move-result-object v8

    .line 86
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-ge v3, v9, :cond_0

    .line 87
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/customreport/data/ChartedSale;

    .line 88
    invoke-virtual {v8}, Lcom/squareup/customreport/data/ChartedSale;->getSalesSummaryReport()Lcom/squareup/customreport/data/SalesSummaryReport;

    move-result-object v9

    .line 89
    instance-of v10, v9, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    if-eqz v10, :cond_0

    .line 90
    invoke-virtual {v8}, Lcom/squareup/customreport/data/ChartedSale;->getDateTime()Lorg/threeten/bp/LocalDateTime;

    move-result-object v8

    invoke-static {v8, v7, v1}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->isEqual(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v3, v3, 0x1

    const/4 v7, 0x0

    .line 94
    check-cast v9, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-virtual {v9}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v8

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v9

    .line 94
    invoke-static {v8, v9}, Lcom/squareup/salesreport/util/SalesSummariesKt;->amountFromChartSalesSelection(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    const/4 v15, 0x5

    const/16 v16, 0x0

    move-object v8, v14

    move-object v9, v7

    move v14, v15

    move-object/from16 v15, v16

    .line 93
    invoke-static/range {v8 .. v15}, Lcom/squareup/salesreport/chart/SalesDataPoint;->copy$default(Lcom/squareup/salesreport/chart/SalesDataPoint;Lorg/threeten/bp/LocalDateTime;JJILjava/lang/Object;)Lcom/squareup/salesreport/chart/SalesDataPoint;

    move-result-object v14

    :cond_0
    move-object v15, v14

    .line 102
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/customreport/data/WithSalesChartReport;->getComparisonChartedSales()Ljava/util/List;

    move-result-object v7

    move-object/from16 v8, p3

    if-eqz v7, :cond_1

    .line 103
    invoke-virtual {v8, v4, v5, v1}, Lorg/threeten/bp/LocalDateTime;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v9

    .line 104
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    if-ge v6, v10, :cond_1

    .line 105
    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/squareup/customreport/data/ChartedSale;

    .line 106
    invoke-virtual {v7}, Lcom/squareup/customreport/data/ChartedSale;->getSalesSummaryReport()Lcom/squareup/customreport/data/SalesSummaryReport;

    move-result-object v10

    .line 107
    instance-of v11, v10, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    if-eqz v11, :cond_1

    .line 108
    invoke-virtual {v7}, Lcom/squareup/customreport/data/ChartedSale;->getDateTime()Lorg/threeten/bp/LocalDateTime;

    move-result-object v7

    const-string v11, "comparisonPointDateTime"

    invoke-static {v9, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v7, v9, v1}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->isEqual(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Z

    move-result v7

    if-eqz v7, :cond_1

    add-int/lit8 v6, v6, 0x1

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    .line 112
    check-cast v10, Lcom/squareup/customreport/data/WithSalesSummaryReport;

    invoke-virtual {v10}, Lcom/squareup/customreport/data/WithSalesSummaryReport;->getSalesSummary()Lcom/squareup/customreport/data/SalesSummary;

    move-result-object v7

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object v9

    .line 112
    invoke-static {v7, v9}, Lcom/squareup/salesreport/util/SalesSummariesKt;->amountFromChartSalesSelection(Lcom/squareup/customreport/data/SalesSummary;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)J

    move-result-wide v19

    const/16 v21, 0x3

    const/16 v22, 0x0

    .line 111
    invoke-static/range {v15 .. v22}, Lcom/squareup/salesreport/chart/SalesDataPoint;->copy$default(Lcom/squareup/salesreport/chart/SalesDataPoint;Lorg/threeten/bp/LocalDateTime;JJILjava/lang/Object;)Lcom/squareup/salesreport/chart/SalesDataPoint;

    move-result-object v7

    move-object v15, v7

    .line 119
    :cond_1
    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-wide/16 v9, 0x1

    add-long/2addr v4, v9

    goto/16 :goto_0

    :cond_2
    return-object v2
.end method

.method public static final getDataPoints(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/chart/SalesDataPoint;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$dataPoints"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesChartReport;->getType()Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object v0

    .line 21
    sget-object v1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->getDataPointsByHour(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 22
    :cond_0
    sget-object v1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->getDataPointsByMonth(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 23
    :cond_1
    invoke-static {p0}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->getDatePointsByDay(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method private static final getDataPointsByHour(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/chart/SalesDataPoint;",
            ">;"
        }
    .end annotation

    .line 33
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getStartDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 35
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    sget-object v2, Lorg/threeten/bp/LocalTime;->MAX:Lorg/threeten/bp/LocalTime;

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    .line 34
    :goto_0
    invoke-static {v1, v2}, Lorg/threeten/bp/LocalDateTime;->of(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    const-string v2, "LocalDateTime.of(\n      \u2026e\n            }\n        )"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getComparisonReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getStartDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    .line 43
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->HOURS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v3, Lorg/threeten/bp/temporal/TemporalUnit;

    .line 32
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->dataPointsByTimePeriod(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static final getDataPointsByMonth(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/chart/SalesDataPoint;",
            ">;"
        }
    .end annotation

    .line 63
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    const-string v1, "reportConfig.startDate.atStartOfDay()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    const-string v2, "reportConfig.endDate.atStartOfDay()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getComparisonReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    const-string v3, "comparisonReportConfig.startDate.atStartOfDay()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->MONTHS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v3, Lorg/threeten/bp/temporal/TemporalUnit;

    .line 62
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->dataPointsByTimePeriod(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static final getDatePointsByDay(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/salesreport/chart/SalesDataPoint;",
            ">;"
        }
    .end annotation

    .line 52
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    const-string v1, "reportConfig.startDate.atStartOfDay()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v1

    const-string v2, "reportConfig.endDate.atStartOfDay()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getComparisonReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v2

    const-string v3, "comparisonReportConfig.startDate.atStartOfDay()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    sget-object v3, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    check-cast v3, Lorg/threeten/bp/temporal/TemporalUnit;

    .line 51
    invoke-static {p0, v0, v1, v2, v3}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->dataPointsByTimePeriod(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private static final isEqual(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/temporal/TemporalUnit;)Z
    .locals 3

    .line 139
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->HOURS:Lorg/threeten/bp/temporal/ChronoUnit;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p2, v0, :cond_1

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->getHour()I

    move-result p0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->getHour()I

    move-result p1

    if-ne p0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 140
    :cond_1
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne p2, v0, :cond_2

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->toLocalDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0

    .line 141
    :cond_2
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->MONTHS:Lorg/threeten/bp/temporal/ChronoUnit;

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->getYear()I

    move-result p2

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->getYear()I

    move-result v0

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDateTime;->getMonth()Lorg/threeten/bp/Month;

    move-result-object p0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->getMonth()Lorg/threeten/bp/Month;

    move-result-object p1

    if-ne p0, p1, :cond_0

    :goto_0
    return v1
.end method
