.class public final Lcom/squareup/salesreport/chart/ChartHourFormatter;
.super Ljava/lang/Object;
.source "ChartHourFormatter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u001e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/salesreport/chart/ChartHourFormatter;",
        "",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "is24HourClock",
        "",
        "(Ljavax/inject/Provider;Z)V",
        "hour24Formatter",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "kotlin.jvm.PlatformType",
        "hourAmPmFormatter",
        "hourFormatter",
        "format",
        "",
        "dateTime",
        "Lorg/threeten/bp/LocalDateTime;",
        "startHour",
        "",
        "endHour",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hour24Formatter:Lorg/threeten/bp/format/DateTimeFormatter;

.field private final hourAmPmFormatter:Lorg/threeten/bp/format/DateTimeFormatter;

.field private final hourFormatter:Lorg/threeten/bp/format/DateTimeFormatter;

.field private final is24HourClock:Z

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->localeProvider:Ljavax/inject/Provider;

    iput-boolean p2, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->is24HourClock:Z

    const-string p1, "H"

    .line 15
    invoke-static {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->hour24Formatter:Lorg/threeten/bp/format/DateTimeFormatter;

    const-string p1, "h"

    .line 16
    invoke-static {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->hourFormatter:Lorg/threeten/bp/format/DateTimeFormatter;

    const-string p1, "h a"

    .line 17
    invoke-static {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->hourAmPmFormatter:Lorg/threeten/bp/format/DateTimeFormatter;

    return-void
.end method


# virtual methods
.method public final format(Lorg/threeten/bp/LocalDateTime;II)Ljava/lang/String;
    .locals 2

    const-string v0, "dateTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDateTime;->getHour()I

    move-result v0

    .line 34
    iget-boolean v1, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->is24HourClock:Z

    if-eqz v1, :cond_0

    iget-object p2, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->hour24Formatter:Lorg/threeten/bp/format/DateTimeFormatter;

    goto :goto_1

    :cond_0
    if-eq v0, p2, :cond_2

    if-eq v0, p3, :cond_2

    .line 35
    rem-int/lit8 v0, v0, 0xc

    if-nez v0, :cond_1

    goto :goto_0

    .line 36
    :cond_1
    iget-object p2, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->hourFormatter:Lorg/threeten/bp/format/DateTimeFormatter;

    goto :goto_1

    .line 35
    :cond_2
    :goto_0
    iget-object p2, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->hourAmPmFormatter:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 39
    :goto_1
    iget-object p3, p0, Lcom/squareup/salesreport/chart/ChartHourFormatter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Locale;

    invoke-virtual {p2, p3}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p2

    invoke-virtual {p1, p2}, Lorg/threeten/bp/LocalDateTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "dateTime.format(formatte\u2026le(localeProvider.get()))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
