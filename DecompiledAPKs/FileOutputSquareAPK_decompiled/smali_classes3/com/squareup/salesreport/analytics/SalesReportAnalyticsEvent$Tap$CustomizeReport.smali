.class public final Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;
.super Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomizeReport"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0012\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\u0008\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J;\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00062\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0008\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;",
        "start_date",
        "",
        "end_date",
        "all_day",
        "",
        "filter_by_employee",
        "filter_by_device",
        "(Ljava/lang/String;Ljava/lang/String;ZZZ)V",
        "getAll_day",
        "()Z",
        "getEnd_date",
        "()Ljava/lang/String;",
        "getFilter_by_device",
        "getFilter_by_employee",
        "getStart_date",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final all_day:Z

.field private final end_date:Ljava/lang/String;

.field private final filter_by_device:Z

.field private final filter_by_employee:Z

.field private final start_date:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 2

    const-string v0, "start_date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "end_date"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SALES_REPORT_CUSTOMIZE_REPORT:Lcom/squareup/analytics/RegisterTapName;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;-><init>(Lcom/squareup/analytics/RegisterTapName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    iput-boolean p4, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    iput-boolean p5, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Object;)Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->copy(Ljava/lang/String;Ljava/lang/String;ZZZ)Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;ZZZ)Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;
    .locals 7

    const-string v0, "start_date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "end_date"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;

    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    iget-boolean v1, p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    iget-boolean v1, p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    iget-boolean p1, p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAll_day()Z
    .locals 1

    .line 370
    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    return v0
.end method

.method public final getEnd_date()Ljava/lang/String;
    .locals 1

    .line 369
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    return-object v0
.end method

.method public final getFilter_by_device()Z
    .locals 1

    .line 372
    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    return v0
.end method

.method public final getFilter_by_employee()Z
    .locals 1

    .line 371
    iget-boolean v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    return v0
.end method

.method public final getStart_date()Ljava/lang/String;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomizeReport(start_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->start_date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", end_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->end_date:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", all_day="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->all_day:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", filter_by_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_employee:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", filter_by_device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;->filter_by_device:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
