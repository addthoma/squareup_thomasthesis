.class public interface abstract Lcom/squareup/salesreport/analytics/SalesReportAnalytics;
.super Ljava/lang/Object;
.source "SalesReportAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H&J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0010\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0019H&J\u0010\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0019H&J\u0010\u0010\u001b\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&J\u0008\u0010\u001c\u001a\u00020\u0003H&J\u0008\u0010\u001d\u001a\u00020\u0003H&J\u0010\u0010\u001e\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u001f\u001a\u00020\u00032\u0006\u0010 \u001a\u00020\u00082\u0006\u0010!\u001a\u00020\nH&J\u0010\u0010\"\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&J\u0010\u0010#\u001a\u00020\u00032\u0006\u0010$\u001a\u00020%H&J\u0010\u0010&\u001a\u00020\u00032\u0006\u0010\'\u001a\u00020(H&J\u0010\u0010)\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u0019H&J\u0018\u0010*\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010+\u001a\u00020\nH&J\u0018\u0010,\u001a\u00020\u00032\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010-\u001a\u00020\nH&J\u0008\u0010.\u001a\u00020\u0003H&J\u0010\u0010/\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&\u00a8\u00060"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
        "",
        "logCategoryGrossCountToggled",
        "",
        "amountCountSelection",
        "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
        "logCategoryItemsShownToggled",
        "categoryIndex",
        "",
        "showItems",
        "",
        "logCategoryViewCountChanged",
        "viewCount",
        "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
        "logChartSalesSelectionToggled",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "logCollapseAllDetails",
        "itemOrCategory",
        "Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;",
        "logComparisonRangeChanged",
        "comparisonRange",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "logCustomizeReport",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "logEmailReport",
        "logExpandAllDetails",
        "logFeesLearnMore",
        "logInitialReportLoad",
        "logItemGrossCountToggled",
        "logItemVariationsShownToggled",
        "itemIndex",
        "showVariations",
        "logItemViewCountChanged",
        "logOverviewDetailsToggled",
        "topSectionState",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "logPresetTimeSelection",
        "presetRangeSelection",
        "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
        "logPrintReport",
        "logReportEmailed",
        "emailChanged",
        "logReportPrinted",
        "itemsIncluded",
        "logViewFeesNote",
        "logViewedInDashboard",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logCategoryGrossCountToggled(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V
.end method

.method public abstract logCategoryItemsShownToggled(IZ)V
.end method

.method public abstract logCategoryViewCountChanged(Lcom/squareup/salesreport/SalesReportState$ViewCount;)V
.end method

.method public abstract logChartSalesSelectionToggled(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V
.end method

.method public abstract logCollapseAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
.end method

.method public abstract logComparisonRangeChanged(Lcom/squareup/customreport/data/ComparisonRange;)V
.end method

.method public abstract logCustomizeReport(Lcom/squareup/customreport/data/ReportConfig;)V
.end method

.method public abstract logEmailReport(Lcom/squareup/customreport/data/ReportConfig;)V
.end method

.method public abstract logExpandAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
.end method

.method public abstract logFeesLearnMore()V
.end method

.method public abstract logInitialReportLoad()V
.end method

.method public abstract logItemGrossCountToggled(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V
.end method

.method public abstract logItemVariationsShownToggled(IZ)V
.end method

.method public abstract logItemViewCountChanged(Lcom/squareup/salesreport/SalesReportState$ViewCount;)V
.end method

.method public abstract logOverviewDetailsToggled(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)V
.end method

.method public abstract logPresetTimeSelection(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;)V
.end method

.method public abstract logPrintReport(Lcom/squareup/customreport/data/ReportConfig;)V
.end method

.method public abstract logReportEmailed(Lcom/squareup/customreport/data/ReportConfig;Z)V
.end method

.method public abstract logReportPrinted(Lcom/squareup/customreport/data/ReportConfig;Z)V
.end method

.method public abstract logViewFeesNote()V
.end method

.method public abstract logViewedInDashboard(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
.end method
