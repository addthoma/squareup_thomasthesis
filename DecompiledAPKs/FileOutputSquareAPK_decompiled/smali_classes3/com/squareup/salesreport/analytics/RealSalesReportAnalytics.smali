.class public final Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;
.super Ljava/lang/Object;
.source "RealSalesReportAnalytics.kt"

# interfaces
.implements Lcom/squareup/salesreport/analytics/SalesReportAnalytics;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSalesReportAnalytics.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSalesReportAnalytics.kt\ncom/squareup/salesreport/analytics/RealSalesReportAnalytics\n*L\n1#1,449:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u00082\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00082\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010 \u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010!\u001a\u00020\u0008H\u0016J\u0008\u0010\"\u001a\u00020\u0008H\u0016J\u0010\u0010#\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010$\u001a\u00020\u00082\u0006\u0010%\u001a\u00020\r2\u0006\u0010&\u001a\u00020\u000fH\u0016J\u0010\u0010\'\u001a\u00020\u00082\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010(\u001a\u00020\u00082\u0006\u0010)\u001a\u00020*H\u0016J\u0010\u0010+\u001a\u00020\u00082\u0006\u0010,\u001a\u00020-H\u0016J\u0010\u0010.\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0018\u0010/\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u00100\u001a\u00020\u000fH\u0016J\u0018\u00101\u001a\u00020\u00082\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u00102\u001a\u00020\u000fH\u0016J\u0008\u00103\u001a\u00020\u0008H\u0016J\u0010\u00104\u001a\u00020\u00082\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;",
        "Lcom/squareup/salesreport/analytics/SalesReportAnalytics;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/time/CurrentTimeZone;)V",
        "logCategoryGrossCountToggled",
        "",
        "amountCountSelection",
        "Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;",
        "logCategoryItemsShownToggled",
        "categoryIndex",
        "",
        "showItems",
        "",
        "logCategoryViewCountChanged",
        "viewCount",
        "Lcom/squareup/salesreport/SalesReportState$ViewCount;",
        "logChartSalesSelectionToggled",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "logCollapseAllDetails",
        "itemOrCategory",
        "Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;",
        "logComparisonRangeChanged",
        "comparisonRange",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "logCustomizeReport",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "logEmailReport",
        "logExpandAllDetails",
        "logFeesLearnMore",
        "logInitialReportLoad",
        "logItemGrossCountToggled",
        "logItemVariationsShownToggled",
        "itemIndex",
        "showVariations",
        "logItemViewCountChanged",
        "logOverviewDetailsToggled",
        "topSectionState",
        "Lcom/squareup/salesreport/SalesReportState$TopSectionState;",
        "logPresetTimeSelection",
        "presetRangeSelection",
        "Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;",
        "logPrintReport",
        "logReportEmailed",
        "emailChanged",
        "logReportPrinted",
        "itemsIncluded",
        "logViewFeesNote",
        "logViewedInDashboard",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/time/CurrentTimeZone;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTimeZone"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    return-void
.end method


# virtual methods
.method public logCategoryGrossCountToggled(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V
    .locals 2

    const-string v0, "amountCountSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 156
    sget-object v1, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 158
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToCount$SwitchToCategoryCount;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToCount$SwitchToCategoryCount;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 157
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToCategoryAmount;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToCategoryAmount;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    .line 156
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 155
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logCategoryItemsShownToggled(IZ)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p2, :cond_0

    .line 270
    new-instance p2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowItemsForCategory;

    invoke-direct {p2, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowItemsForCategory;-><init>(I)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideItemsForCategory;

    invoke-direct {p2, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideItemsForCategory;-><init>(I)V

    :goto_0
    check-cast p2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    check-cast p2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 269
    invoke-interface {v0, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logCategoryViewCountChanged(Lcom/squareup/salesreport/SalesReportState$ViewCount;)V
    .locals 2

    const-string/jumbo v0, "viewCount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 185
    sget-object v1, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 188
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllCategories;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllCategories;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 187
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopTen$ShowTopTenCategories;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopTen$ShowTopTenCategories;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    .line 186
    :cond_2
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopFive$ShowTopFiveCategories;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopFive$ShowTopFiveCategories;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    .line 185
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 184
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logChartSalesSelectionToggled(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)V
    .locals 2

    const-string v0, "chartSalesSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 165
    sget-object v1, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 168
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToSalesCountChart;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToSalesCountChart;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 167
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToNetSalesChart;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToNetSalesChart;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    .line 166
    :cond_2
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToGrossSalesChart;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToGrossSalesChart;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    .line 165
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 164
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logCollapseAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
    .locals 2

    const-string v0, "itemOrCategory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 295
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails$CollapseAllItemDetails;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails$CollapseAllItemDetails;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails;

    goto :goto_0

    .line 296
    :cond_0
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails$CollapseAllCategoryDetails;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails$CollapseAllCategoryDetails;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CollapseAllDetails;

    .line 294
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 293
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void

    .line 296
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public logComparisonRangeChanged(Lcom/squareup/customreport/data/ComparisonRange;)V
    .locals 2

    const-string v0, "comparisonRange"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ChangeComparison;

    invoke-static {p1}, Lcom/squareup/salesreport/analytics/RealSalesReportAnalyticsKt;->access$getEventValue$p(Lcom/squareup/customreport/data/ComparisonRange;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ChangeComparison;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logCustomizeReport(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 9

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 197
    new-instance v8, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;

    .line 198
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v3

    .line 199
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v4

    .line 200
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v5

    .line 201
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    move-result-object v0

    instance-of v6, v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    .line 202
    invoke-virtual {p1}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result v7

    move-object v2, v8

    .line 197
    invoke-direct/range {v2 .. v7}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$CustomizeReport;-><init>(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    check-cast v8, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 196
    invoke-interface {v1, v8}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logEmailReport(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 4

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 238
    iget-object v1, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 239
    new-instance v2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$EmailReport;

    .line 240
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v3

    .line 241
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object p1

    .line 239
    invoke-direct {v2, v3, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$EmailReport;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 238
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logExpandAllDetails(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
    .locals 2

    const-string v0, "itemOrCategory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 286
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails$ExpandAllItemDetails;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails$ExpandAllItemDetails;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails;

    goto :goto_0

    .line 287
    :cond_0
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails$ExpandAllCategoryDetails;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails$ExpandAllCategoryDetails;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ExpandAllDetails;

    .line 285
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 284
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void

    .line 287
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public logFeesLearnMore()V
    .locals 3

    .line 306
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Reports: Learn More From Sales Fees Info"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logInitialReportLoad()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$View$ViewSalesReport;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$View$ViewSalesReport;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logItemGrossCountToggled(Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;)V
    .locals 2

    const-string v0, "amountCountSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 147
    sget-object v1, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 149
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToCount$SwitchToItemCount;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToCount$SwitchToItemCount;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 148
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToItemAmount;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToAmount$SwitchToItemAmount;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    .line 147
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 146
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logItemVariationsShownToggled(IZ)V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p2, :cond_0

    .line 279
    new-instance p2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowVariationsForItem;

    invoke-direct {p2, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowVariationsForItem;-><init>(I)V

    goto :goto_0

    :cond_0
    new-instance p2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideVariationsForItem;

    invoke-direct {p2, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$HideVariationsForItem;-><init>(I)V

    :goto_0
    check-cast p2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    check-cast p2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 278
    invoke-interface {v0, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logItemViewCountChanged(Lcom/squareup/salesreport/SalesReportState$ViewCount;)V
    .locals 2

    const-string/jumbo v0, "viewCount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 175
    sget-object v1, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 178
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowAll$ShowAllItems;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 177
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopTen$ShowTopTenItems;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopTen$ShowTopTenItems;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    .line 176
    :cond_2
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopFive$ShowTopFiveItems;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ShowTopFive$ShowTopFiveItems;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    .line 175
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 174
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logOverviewDetailsToggled(Lcom/squareup/salesreport/SalesReportState$TopSectionState;)V
    .locals 2

    const-string/jumbo v0, "topSectionState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 138
    sget-object v1, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 140
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToDetails;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToDetails;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 139
    :cond_1
    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToOverview;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SwitchToOverview;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap;

    .line 138
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 137
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logPresetTimeSelection(Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;)V
    .locals 2

    const-string v0, "presetRangeSelection"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SelectQuickTimerange;

    check-cast p1, Lcom/squareup/customreport/data/RangeSelection;

    invoke-static {p1}, Lcom/squareup/salesreport/analytics/RealSalesReportAnalyticsKt;->access$getEventValue$p(Lcom/squareup/customreport/data/RangeSelection;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$SelectQuickTimerange;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logPrintReport(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 4

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 248
    iget-object v1, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 249
    new-instance v2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$PrintReport;

    .line 250
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v3

    .line 251
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object p1

    .line 249
    invoke-direct {v2, v3, p1}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$PrintReport;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 248
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logReportEmailed(Lcom/squareup/customreport/data/ReportConfig;Z)V
    .locals 4

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 213
    iget-object v1, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 214
    new-instance v2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportEmailed;

    .line 215
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v3

    .line 216
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object p1

    .line 214
    invoke-direct {v2, v3, p1, p2}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportEmailed;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 213
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logReportPrinted(Lcom/squareup/customreport/data/ReportConfig;Z)V
    .locals 4

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v0}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    .line 227
    iget-object v1, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 228
    new-instance v2, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportPrinted;

    .line 229
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object v3

    .line 230
    invoke-static {p1, v0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;

    move-result-object p1

    .line 228
    invoke-direct {v2, v3, p1, p2}, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Action$SalesReportPrinted;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 227
    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logViewFeesNote()V
    .locals 3

    .line 302
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Reports: Fees Info From Sales"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method public logViewedInDashboard(Lcom/squareup/salesreport/SalesReportState$ItemOrCategory;)V
    .locals 2

    const-string v0, "itemOrCategory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/salesreport/analytics/RealSalesReportAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    .line 259
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Item;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard$ViewItemsInDashboard;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard$ViewItemsInDashboard;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard;

    goto :goto_0

    .line 260
    :cond_0
    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;->INSTANCE:Lcom/squareup/salesreport/SalesReportState$ItemOrCategory$Category;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard$ViewCategoriesInDashboard;->INSTANCE:Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard$ViewCategoriesInDashboard;

    check-cast p1, Lcom/squareup/salesreport/analytics/SalesReportAnalyticsEvent$Tap$ViewInDashboard;

    .line 258
    :goto_0
    check-cast p1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 257
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void

    .line 260
    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
