.class public final Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;
.super Ljava/lang/Object;
.source "SalesReportPrintPayloadFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 57
    iput-object p9, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p10, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg9Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;"
        }
    .end annotation

    .line 73
    new-instance v11, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/salesreport/print/SalesPrintFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;)Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/salesreport/print/SalesPrintFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/salesreport/util/PercentageChangeFormatter;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;"
        }
    .end annotation

    .line 80
    new-instance v11, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;-><init>(Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/salesreport/print/SalesPrintFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;
    .locals 11

    .line 63
    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/salesreport/print/SalesPrintFormatter;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/salesreport/util/PercentageChangeFormatter;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v10, p0, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v10}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->newInstance(Lcom/squareup/util/Res;Landroid/content/res/Resources;Lcom/squareup/salesreport/print/SalesPrintFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/salesreport/util/PercentageChangeFormatter;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;)Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory_Factory;->get()Lcom/squareup/salesreport/printing/SalesReportPrintPayloadFactory;

    move-result-object v0

    return-object v0
.end method
