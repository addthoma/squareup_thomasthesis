.class public final Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;
.super Ljava/lang/Object;
.source "DateRangeSelectorLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00142\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u000c\u0010\u0010\u001a\u00020\u0011*\u00020\u0012H\u0002J\u000c\u0010\u0013\u001a\u00020\u0012*\u00020\u0011H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "checkableGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "checkableRowId",
        "",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "dateRangeForId",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final checkableGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->Companion:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->view:Landroid/view/View;

    .line 26
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 28
    iget-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_checkable_group:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->checkableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 33
    iget-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 31
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 32
    new-instance v1, Lcom/squareup/resources/ResourceString;

    sget v2, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_customize_report:I

    invoke-direct {v1, v2}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method public static final synthetic access$dateRangeForId(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;I)Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->dateRangeForId(I)Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCheckableGroup$p(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;)Lcom/squareup/noho/NohoCheckableGroup;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->checkableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    return-object p0
.end method

.method private final checkableRowId(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)I
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 61
    sget p1, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_last_year:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 60
    :cond_1
    sget p1, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_this_year:I

    goto :goto_0

    .line 59
    :cond_2
    sget p1, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_last_month:I

    goto :goto_0

    .line 58
    :cond_3
    sget p1, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_this_month:I

    :goto_0
    return p1
.end method

.method private final dateRangeForId(I)Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;
    .locals 3

    .line 66
    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_this_month:I

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->THIS_MONTH:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    goto :goto_0

    .line 67
    :cond_0
    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_last_month:I

    if-ne p1, v0, :cond_1

    sget-object p1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->LAST_MONTH:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    goto :goto_0

    .line 68
    :cond_1
    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_this_year:I

    if-ne p1, v0, :cond_2

    sget-object p1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->THIS_YEAR:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    goto :goto_0

    .line 69
    :cond_2
    sget v0, Lcom/squareup/loyaltyreport/impl/R$id;->date_range_row_last_year:I

    if-ne p1, v0, :cond_3

    sget-object p1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->LAST_YEAR:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    :goto_0
    return-object p1

    .line 70
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported xml id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " used in dateRangeForId()"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public showRendering(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 10

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object p2, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$1;-><init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 51
    iget-object p2, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 41
    invoke-virtual {p2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$2;

    invoke-direct {v2, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$2;-><init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v3

    .line 46
    sget-object v4, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 47
    new-instance v0, Lcom/squareup/resources/ResourceString;

    sget v1, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_save:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    move-object v5, v0

    check-cast v5, Lcom/squareup/resources/TextModel;

    .line 48
    new-instance v0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner$showRendering$3;-><init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function0;

    const/4 v6, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    .line 45
    invoke-static/range {v3 .. v9}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 53
    iget-object p2, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->checkableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->checkableRowId(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorLayoutRunner;->showRendering(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
