.class final Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter$dateTimeFormatter$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LoyaltyReportDateTimeFormatter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;-><init>(Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lorg/threeten/bp/format/DateTimeFormatter;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter$dateTimeFormatter$2;->$res:Lcom/squareup/util/Res;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter$dateTimeFormatter$2;->invoke()Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Lorg/threeten/bp/format/DateTimeFormatter;
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter$dateTimeFormatter$2;->$res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_date_pattern:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    return-object v0
.end method
