.class public final Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;
.super Ljava/lang/Object;
.source "LoyaltyReportTransformer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/util/CustomerInitialsHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/util/CustomerInitialsHelper;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p7, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTimeZone;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/util/CustomerInitialsHelper;",
            ">;)",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;"
        }
    .end annotation

    .line 55
    new-instance v8, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/crm/util/CustomerInitialsHelper;)Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/time/CurrentTimeZone;",
            "Lcom/squareup/crm/util/CustomerInitialsHelper;",
            ")",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/crm/util/CustomerInitialsHelper;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;
    .locals 8

    .line 47
    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/time/CurrentTimeZone;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/crm/util/CustomerInitialsHelper;

    invoke-static/range {v1 .. v7}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->newInstance(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateTimeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/crm/util/CustomerInitialsHelper;)Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer_Factory;->get()Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    move-result-object v0

    return-object v0
.end method
