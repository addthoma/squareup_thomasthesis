.class final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLoyaltyReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->toLayeredScreen(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $loyaltyReport:Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

.field final synthetic this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p3, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->$state:Lcom/squareup/loyaltyreport/LoyaltyReportState;

    iput-object p4, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->$loyaltyReport:Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    .line 179
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->$sink:Lcom/squareup/workflow/Sink;

    .line 180
    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    new-instance v2, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2$1;

    invoke-direct {v2, p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2$1;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v1, v3, v2, v4, v3}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    .line 179
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
