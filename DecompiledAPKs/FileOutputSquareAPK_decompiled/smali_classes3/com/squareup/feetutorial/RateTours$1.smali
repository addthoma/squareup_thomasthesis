.class synthetic Lcom/squareup/feetutorial/RateTours$1;
.super Ljava/lang/Object;
.source "RateTours.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/feetutorial/RateTours;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$feetutorial$RateCategory:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 50
    invoke-static {}, Lcom/squareup/feetutorial/RateCategory;->values()[Lcom/squareup/feetutorial/RateCategory;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    :try_start_0
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R6_JCB:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->DIP_R12:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->TAP:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->TAP_INTERAC:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->MANUAL:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/feetutorial/RateTours$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    sget-object v1, Lcom/squareup/feetutorial/RateCategory;->SWIPE:Lcom/squareup/feetutorial/RateCategory;

    invoke-virtual {v1}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    return-void
.end method
