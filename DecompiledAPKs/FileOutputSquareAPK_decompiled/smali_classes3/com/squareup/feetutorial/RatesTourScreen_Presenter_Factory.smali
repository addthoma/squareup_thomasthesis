.class public final Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "RatesTourScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/feetutorial/RatesTourScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final rateToursProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/RateTours;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/RateTours;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->rateToursProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p5, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/RateTours;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)",
            "Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/analytics/Analytics;Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/feetutorial/RatesTourScreen$Presenter;
    .locals 7

    .line 51
    new-instance v6, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    move-object v3, p2

    check-cast v3, Lcom/squareup/feetutorial/RateTours;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/feetutorial/RateTours;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/util/BrowserLauncher;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/feetutorial/RatesTourScreen$Presenter;
    .locals 5

    .line 39
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->rateToursProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v4, p0, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/util/BrowserLauncher;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/analytics/Analytics;Ljava/lang/Object;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/feetutorial/RatesTourScreen_Presenter_Factory;->get()Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
