.class public final Lcom/squareup/hardware/UsbAttachedActivityEnabler;
.super Ljava/lang/Object;
.source "UsbAttachedActivityEnabler.kt"

# interfaces
.implements Lmortar/Scoped;
.implements Lcom/squareup/ActivityListener$ResumedPausedListener;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u001f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0012\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u000bH\u0016J\u0008\u0010\u000f\u001a\u00020\u000bH\u0016J\u0008\u0010\u0010\u001a\u00020\u000bH\u0016J\u0010\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/hardware/UsbAttachedActivityEnabler;",
        "Lmortar/Scoped;",
        "Lcom/squareup/ActivityListener$ResumedPausedListener;",
        "application",
        "Landroid/app/Application;",
        "packageManager",
        "Landroid/content/pm/PackageManager;",
        "activityListener",
        "Lcom/squareup/ActivityListener;",
        "(Landroid/app/Application;Landroid/content/pm/PackageManager;Lcom/squareup/ActivityListener;)V",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onPause",
        "onResume",
        "toggleUsbAttachedActivity",
        "isEnabled",
        "",
        "hardware_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activityListener:Lcom/squareup/ActivityListener;

.field private final application:Landroid/app/Application;

.field private final packageManager:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/content/pm/PackageManager;Lcom/squareup/ActivityListener;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "packageManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityListener"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->packageManager:Landroid/content/pm/PackageManager;

    iput-object p3, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->activityListener:Lcom/squareup/ActivityListener;

    return-void
.end method

.method private final toggleUsbAttachedActivity(Z)V
    .locals 4

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    .line 55
    :goto_0
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->application:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/squareup/hardware/UsbAttachedActivity;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v2, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1, p1, v0}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 60
    iget-object p1, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->activityListener:Lcom/squareup/ActivityListener;

    move-object v0, p0

    check-cast v0, Lcom/squareup/ActivityListener$ResumedPausedListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ActivityListener;->registerResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->activityListener:Lcom/squareup/ActivityListener;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ActivityListener$ResumedPausedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ActivityListener;->unregisterResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0, v0}, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->toggleUsbAttachedActivity(Z)V

    return-void
.end method

.method public onResume()V
    .locals 1

    const/4 v0, 0x1

    .line 45
    invoke-direct {p0, v0}, Lcom/squareup/hardware/UsbAttachedActivityEnabler;->toggleUsbAttachedActivity(Z)V

    return-void
.end method
