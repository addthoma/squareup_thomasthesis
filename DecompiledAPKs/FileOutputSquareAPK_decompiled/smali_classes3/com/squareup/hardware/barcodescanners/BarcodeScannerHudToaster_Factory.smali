.class public final Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;
.super Ljava/lang/Object;
.source "BarcodeScannerHudToaster_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannerTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->barcodeScannerTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster;

    iget-object v2, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->newInstance(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster_Factory;->get()Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

    move-result-object v0

    return-object v0
.end method
