.class public Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;
.super Ljava/lang/Object;
.source "BarcodeScannerHudToaster.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    .line 34
    iput-object p2, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 35
    iput-object p3, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->res:Lcom/squareup/util/Res;

    .line 36
    iput-object p5, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->features:Lcom/squareup/settings/server/Features;

    .line 38
    new-instance p1, Lcom/squareup/hardware/barcodescanners/-$$Lambda$BarcodeScannerHudToaster$JrDxt7NHN4zlloQZIG09bufp3IM;

    invoke-direct {p1, p4}, Lcom/squareup/hardware/barcodescanners/-$$Lambda$BarcodeScannerHudToaster$JrDxt7NHN4zlloQZIG09bufp3IM;-><init>(Lcom/squareup/analytics/Analytics;)V

    iput-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    .line 40
    new-instance p1, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;

    invoke-direct {p1, p0, p4}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;-><init>(Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;Lcom/squareup/analytics/Analytics;)V

    iput-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    return-void
.end method

.method static synthetic lambda$new$0(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 0

    .line 38
    sget-object p1, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p0, p1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 68
    iget-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {p1, v0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    invoke-virtual {p1, v0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addConnectionListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannedListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeConnectionListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;)V

    return-void
.end method

.method public showBarcodeScannerConnected()V
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/hardware/R$string;->barcode_scanner_connected:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_BARCODE_SCANNER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    :cond_0
    return-void
.end method

.method public showBarcodeScannerDisconnected()V
    .locals 4

    .line 61
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/hardware/R$string;->barcode_scanner_disconnected:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_BARCODE_SCANNER_DISCONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    :cond_0
    return-void
.end method
