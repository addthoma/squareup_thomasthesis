.class public Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "BarcodeScannerEvent.java"


# instance fields
.field public final connectionType:Ljava/lang/String;

.field public final deviceName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 20
    iput-object p3, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;->deviceName:Ljava/lang/String;

    .line 21
    iput-object p4, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;->connectionType:Ljava/lang/String;

    return-void
.end method

.method public static forBarcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;
    .locals 4

    .line 25
    invoke-interface {p0}, Lcom/squareup/barcodescanners/BarcodeScanner;->getName()Ljava/lang/String;

    move-result-object v0

    .line 26
    invoke-interface {p0}, Lcom/squareup/barcodescanners/BarcodeScanner;->getConnectionType()Ljava/lang/String;

    move-result-object p0

    .line 27
    new-instance v1, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNER_CONNECT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v3, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, p0}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public static forBarcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;
    .locals 4

    .line 31
    invoke-interface {p0}, Lcom/squareup/barcodescanners/BarcodeScanner;->getName()Ljava/lang/String;

    move-result-object v0

    .line 32
    invoke-interface {p0}, Lcom/squareup/barcodescanners/BarcodeScanner;->getConnectionType()Ljava/lang/String;

    move-result-object p0

    .line 33
    new-instance v1, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;

    sget-object v2, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->BARCODE_SCANNER_DISCONNECT:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, v3, Lcom/squareup/analytics/RegisterActionName;->value:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, p0}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method
