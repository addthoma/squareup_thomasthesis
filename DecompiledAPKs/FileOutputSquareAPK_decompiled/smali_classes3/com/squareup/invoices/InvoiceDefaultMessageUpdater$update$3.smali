.class final Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3;
.super Ljava/lang/Object;
.source "InvoiceDefaultMessageUpdater.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->update(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/DefaultMessageUpdatedResult;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3;->this$0:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoices/DefaultMessageUpdatedResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
            ">;)",
            "Lcom/squareup/invoices/DefaultMessageUpdatedResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Success;->INSTANCE:Lcom/squareup/invoices/DefaultMessageUpdatedResult$Success;

    check-cast p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult;

    goto :goto_0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3;->this$0:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;->access$getFailureMessageFactory$p(Lcom/squareup/invoices/InvoiceDefaultMessageUpdater;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_set_default_message_error:I

    sget-object v2, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3$failureMessage$1;->INSTANCE:Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 47
    new-instance v0, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Failed;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/DefaultMessageUpdatedResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/invoices/DefaultMessageUpdatedResult;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/InvoiceDefaultMessageUpdater$update$3;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/invoices/DefaultMessageUpdatedResult;

    move-result-object p1

    return-object p1
.end method
