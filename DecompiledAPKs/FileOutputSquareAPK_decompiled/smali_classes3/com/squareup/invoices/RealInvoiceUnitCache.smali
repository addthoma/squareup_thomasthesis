.class public final Lcom/squareup/invoices/RealInvoiceUnitCache;
.super Ljava/lang/Object;
.source "RealInvoiceUnitCache.kt"

# interfaces
.implements Lcom/squareup/invoicesappletapi/InvoiceUnitCache;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/LoggedInScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealInvoiceUnitCache.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealInvoiceUnitCache.kt\ncom/squareup/invoices/RealInvoiceUnitCache\n+ 2 PushMessageDelegate.kt\ncom/squareup/pushmessages/PushMessageDelegateKt\n*L\n1#1,350:1\n16#2:351\n16#2:352\n*E\n*S KotlinDebug\n*F\n+ 1 RealInvoiceUnitCache.kt\ncom/squareup/invoices/RealInvoiceUnitCache\n*L\n102#1:351\n118#1:352\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00ce\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 Q2\u00020\u0001:\u0001QB\u007f\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0003\u0012\u0012\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00060\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0008\u0008\u0001\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u0014\u0010$\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\'0&0%H\u0002J\u0014\u0010(\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020)0&0%H\u0002J\u0014\u0010*\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020+0&0%H\u0002J\u000e\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0%H\u0016J\u0008\u0010.\u001a\u00020/H\u0016J\u0008\u00100\u001a\u000201H\u0002J\u0008\u00102\u001a\u000203H\u0016J\u000e\u00104\u001a\u0008\u0012\u0004\u0012\u0002050\u0006H\u0016J\u0008\u00106\u001a\u000207H\u0016J\u0008\u00108\u001a\u00020\tH\u0016J\u0008\u00109\u001a\u00020/H\u0016J\u0008\u0010:\u001a\u00020;H\u0016J\u0014\u0010<\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00060=H\u0016J\u0008\u0010>\u001a\u00020\u001fH\u0002J\u0008\u0010?\u001a\u00020\u001fH\u0016J\u0014\u0010@\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060=H\u0016J\u0010\u0010A\u001a\u00020\u001f2\u0006\u0010B\u001a\u00020CH\u0016J\u0008\u0010D\u001a\u00020\u001fH\u0016J\u0008\u0010E\u001a\u00020\u001fH\u0016J\u0008\u0010F\u001a\u00020\u001fH\u0002J\u000e\u0010G\u001a\u0008\u0012\u0004\u0012\u00020\u00040=H\u0016J\u0010\u0010H\u001a\u00020\u001f2\u0006\u0010I\u001a\u000203H\u0016J\u0010\u0010J\u001a\u00020\u001f2\u0006\u0010K\u001a\u00020\tH\u0002J\u0010\u0010L\u001a\u00020\u001f2\u0006\u0010M\u001a\u00020NH\u0002J\u001e\u0010O\u001a\u00020\"\"\u0004\u0008\u0000\u0010P*\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002HP0&0%H\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001d\u001a\u0010\u0012\u000c\u0012\n  *\u0004\u0018\u00010\u001f0\u001f0\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006R"
    }
    d2 = {
        "Lcom/squareup/invoices/RealInvoiceUnitCache;",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "invoiceUnitMetadataPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "invoiceMetricsPreference",
        "",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "invoiceDefaultListPreference",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "invoiceService",
        "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "receiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "pushMessages",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)V",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "initialFetch",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "initialMetricsDisposable",
        "Lio/reactivex/disposables/Disposable;",
        "initialUnitSettingsDisposable",
        "fetchMetrics",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
        "fetchUnitMetadata",
        "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;",
        "fetchUnitSettings",
        "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
        "forceGetSettings",
        "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
        "getAutomaticReminderCountLimit",
        "",
        "getAutomaticReminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "getCurrentDefaultMessage",
        "",
        "getDefaultReminderConfigs",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "getFileAttachmentLimits",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;",
        "getInvoiceDefaults",
        "getReminderCharacterLimit",
        "hasInvoices",
        "",
        "invoiceDefaultsList",
        "Lio/reactivex/Observable;",
        "listenForPushes",
        "maybeRefreshFromServer",
        "metrics",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "setHasInvoicesTrue",
        "setupInitialFetchDisposables",
        "unitMetadataDisplayDetails",
        "updateDefaultMessage",
        "defaultMessage",
        "updateInvoiceDefaults",
        "invoiceDefaults",
        "updateUnitMetadata",
        "unitMetadata",
        "Lcom/squareup/protos/client/invoice/UnitMetadata;",
        "subscribeInitialFetch",
        "T",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BACKGROUND_RETRIES:I = 0x3

.field private static final BACKGROUND_RETRY_DELAY_SECONDS:J = 0x2L

.field public static final Companion:Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final METRIC_RANGE_RELATIVE_DAY_START:I = -0x1e

.field private static final REFRESH_MIN_SECONDS:J = 0x5L


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final initialFetch:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private initialMetricsDisposable:Lio/reactivex/disposables/Disposable;

.field private initialUnitSettingsDisposable:Lio/reactivex/disposables/Disposable;

.field private final invoiceDefaultListPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;"
        }
    .end annotation
.end field

.field private final invoiceMetricsPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;"
        }
    .end annotation
.end field

.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final pushMessages:Lcom/squareup/pushmessages/PushMessageDelegate;

.field private final receiver:Lcom/squareup/receiving/StandardReceiver;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/RealInvoiceUnitCache;->Companion:Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/pushmessages/PushMessageDelegate;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p11    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceUnitMetadataPreference"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceMetricsPreference"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceDefaultListPreference"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceService"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiver"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushMessages"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p2, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceMetricsPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p3, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceDefaultListPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iput-object p5, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->clock:Lcom/squareup/util/Clock;

    iput-object p6, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->res:Lcom/squareup/util/Res;

    iput-object p7, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->features:Lcom/squareup/settings/server/Features;

    iput-object p8, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->receiver:Lcom/squareup/receiving/StandardReceiver;

    iput-object p9, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p10, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->pushMessages:Lcom/squareup/pushmessages/PushMessageDelegate;

    iput-object p11, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->mainScheduler:Lio/reactivex/Scheduler;

    .line 76
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 77
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string p2, "Disposables.disposed()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialUnitSettingsDisposable:Lio/reactivex/disposables/Disposable;

    .line 78
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialMetricsDisposable:Lio/reactivex/disposables/Disposable;

    .line 80
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialFetch:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/invoices/RealInvoiceUnitCache;->Companion:Lcom/squareup/invoices/RealInvoiceUnitCache$Companion;

    return-object v0
.end method

.method public static final synthetic access$fetchMetrics(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/Single;
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->fetchMetrics()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$fetchUnitSettings(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/Single;
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->fetchUnitSettings()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFailureMessageFactory$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lcom/squareup/receiving/FailureMessageFactory;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-object p0
.end method

.method public static final synthetic access$getInitialMetricsDisposable$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/disposables/Disposable;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialMetricsDisposable:Lio/reactivex/disposables/Disposable;

    return-object p0
.end method

.method public static final synthetic access$getInitialUnitSettingsDisposable$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/disposables/Disposable;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialUnitSettingsDisposable:Lio/reactivex/disposables/Disposable;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceDefaultListPreference$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceDefaultListPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceMetricsPreference$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceMetricsPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceUnitMetadataPreference$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/invoices/RealInvoiceUnitCache;)Lio/reactivex/Scheduler;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$setInitialMetricsDisposable$p(Lcom/squareup/invoices/RealInvoiceUnitCache;Lio/reactivex/disposables/Disposable;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialMetricsDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public static final synthetic access$setInitialUnitSettingsDisposable$p(Lcom/squareup/invoices/RealInvoiceUnitCache;Lio/reactivex/disposables/Disposable;)V
    .locals 0

    .line 61
    iput-object p1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialUnitSettingsDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private final fetchMetrics()Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
            ">;>;"
        }
    .end annotation

    .line 287
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Times;->stripTime(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    .line 289
    new-instance v1, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;-><init>()V

    const-string/jumbo v2, "today"

    .line 290
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/Calendars;->copy(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v2

    const/4 v3, 0x5

    const/16 v4, -0x1e

    .line 291
    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 292
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 290
    invoke-static {v2}, Lcom/squareup/util/ProtoTimes;->asDateTime(Ljava/util/Date;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->inclusive_start(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/common/time/DateTimeInterval$Builder;

    move-result-object v1

    .line 294
    invoke-static {v0}, Lcom/squareup/util/Calendars;->copy(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Times;->setToEndOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    const-string v2, "setToEndOfDay(today.copy())"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/ProtoTimes;->asDateTime(Ljava/util/Date;)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->exclusive_end(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/common/time/DateTimeInterval$Builder;

    move-result-object v0

    .line 295
    invoke-virtual {v0}, Lcom/squareup/protos/common/time/DateTimeInterval$Builder;->build()Lcom/squareup/protos/common/time/DateTimeInterval;

    move-result-object v0

    .line 297
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 299
    new-instance v2, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    .line 300
    sget-object v3, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v2

    .line 301
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v2

    .line 302
    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v2

    .line 298
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    new-instance v2, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    sget-object v3, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    new-instance v2, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    sget-object v3, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    new-instance v2, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    sget-object v3, Lcom/squareup/protos/client/invoice/MetricType;->PENDING_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    new-instance v2, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    .line 309
    sget-object v3, Lcom/squareup/protos/client/invoice/MetricType;->ACCEPTED_ESTIMATE_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v2

    .line 310
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v0

    .line 307
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getMetrics(Ljava/util/List;)Lrx/Observable;

    move-result-object v0

    .line 315
    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->receiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchMetrics$1;->INSTANCE:Lcom/squareup/invoices/RealInvoiceUnitCache$fetchMetrics$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 316
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchMetrics$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchMetrics$2;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "invoiceService.getMetric\u2026ic)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 321
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "invoiceService.getMetric\u2026)\n        .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final fetchUnitMetadata()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;",
            ">;>;"
        }
    .end annotation

    .line 260
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getUnitMetadata()Lrx/Observable;

    move-result-object v0

    .line 262
    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->receiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitMetadata$1;->INSTANCE:Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitMetadata$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    .line 261
    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 263
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitMetadata$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitMetadata$2;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "invoiceService.getUnitMe\u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 271
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "invoiceService.getUnitMe\u2026)\n        .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final fetchUnitSettings()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
            ">;>;"
        }
    .end annotation

    .line 275
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getUnitSettings()Lrx/Observable;

    move-result-object v0

    .line 276
    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->receiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitSettings$1;->INSTANCE:Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitSettings$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 277
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitSettings$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$fetchUnitSettings$2;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "invoiceService.getUnitSe\u2026ts)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 282
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "invoiceService.getUnitSe\u2026)\n        .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final getAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    if-nez v0, :cond_0

    .line 254
    invoke-static {}, Lcom/squareup/invoices/InvoiceUnitCacheDefaultsKt;->defaultAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private final listenForPushes()V
    .locals 3

    .line 102
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->pushMessages:Lcom/squareup/pushmessages/PushMessageDelegate;

    .line 351
    const-class v1, Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;

    invoke-interface {v0, v1}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 103
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$1;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 109
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$2;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "pushMessages.observe<Inv\u2026   }\n        .subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v2}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->pushMessages:Lcom/squareup/pushmessages/PushMessageDelegate;

    .line 352
    const-class v2, Lcom/squareup/pushmessages/PushMessage$InvoicesMetricsSync;

    invoke-interface {v0, v2}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    .line 119
    new-instance v2, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$3;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$3;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 125
    new-instance v2, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$4;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$listenForPushes$4;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final setupInitialFetchDisposables()V
    .locals 2

    .line 92
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->fetchUnitSettings()Lio/reactivex/Single;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->subscribeInitialFetch(Lio/reactivex/Single;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialUnitSettingsDisposable:Lio/reactivex/disposables/Disposable;

    .line 93
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->fetchMetrics()Lio/reactivex/Single;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->subscribeInitialFetch(Lio/reactivex/Single;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialMetricsDisposable:Lio/reactivex/disposables/Disposable;

    .line 94
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->fetchUnitMetadata()Lio/reactivex/Single;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->subscribeInitialFetch(Lio/reactivex/Single;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialUnitSettingsDisposable:Lio/reactivex/disposables/Disposable;

    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialMetricsDisposable:Lio/reactivex/disposables/Disposable;

    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-static {v0, v1}, Lcom/squareup/util/DisposablesKt;->addTo(Lio/reactivex/disposables/Disposable;Lio/reactivex/disposables/CompositeDisposable;)V

    return-void
.end method

.method private final subscribeInitialFetch(Lio/reactivex/Single;)Lio/reactivex/disposables/Disposable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;)",
            "Lio/reactivex/disposables/Disposable;"
        }
    .end annotation

    .line 331
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialFetch:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 332
    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->throttleFirst(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    .line 333
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$subscribeInitialFetch$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/RealInvoiceUnitCache$subscribeInitialFetch$1;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;Lio/reactivex/Single;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 338
    sget-object v0, Lcom/squareup/invoices/RealInvoiceUnitCache$subscribeInitialFetch$2;->INSTANCE:Lcom/squareup/invoices/RealInvoiceUnitCache$subscribeInitialFetch$2;

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v0, 0x1

    .line 339
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    .line 340
    invoke-virtual {p1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "initialFetch\n        .th\u2026e(1)\n        .subscribe()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final updateInvoiceDefaults(Lcom/squareup/protos/client/invoice/InvoiceDefaults;)V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceDefaultListPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final updateUnitMetadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)V
    .locals 2

    .line 241
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    .line 242
    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    .line 243
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object v1

    .line 244
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p1

    .line 245
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p1

    .line 241
    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public forceGetSettings()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
            ">;"
        }
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getUnitSettings()Lrx/Observable;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->receiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$1;->INSTANCE:Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 150
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$2;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 155
    new-instance v1, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/RealInvoiceUnitCache$forceGetSettings$3;-><init>(Lcom/squareup/invoices/RealInvoiceUnitCache;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lrx/Observable;->toSingle()Lrx/Single;

    move-result-object v0

    const-string v1, "invoiceService.getUnitSe\u2026    }\n        .toSingle()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public getAutomaticReminderCountLimit()I
    .locals 2

    .line 226
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->getAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->automatic_reminder_count_limit:Ljava/lang/Integer;

    const-string v1, "getAutomaticReminderSett\u2026atic_reminder_count_limit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getCurrentDefaultMessage()Ljava/lang/String;
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    invoke-virtual {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->getInvoiceDefaults()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object v0

    .line 203
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 204
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_default_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->message:Ljava/lang/String;

    :goto_0
    const-string v1, "if (invoiceDefaults.mess\u2026  invoiceDefaults.message"

    .line 203
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    if-nez v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_default_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 213
    :cond_2
    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadata;->default_message:Ljava/lang/String;

    :goto_1
    const-string v1, "if (unitMetadata == null\u2026tMetadata.default_message"

    .line 210
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object v0
.end method

.method public getDefaultReminderConfigs()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation

    .line 218
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->getAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->default_reminder:Ljava/util/List;

    const-string v1, "getAutomaticReminderSettings().default_reminder"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getFileAttachmentLimits()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
    .locals 2

    .line 232
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    const-string v1, "invoiceUnitMetadataPrefe\u2026nce.get()\n        .limits"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getInvoiceDefaults()Lcom/squareup/protos/client/invoice/InvoiceDefaults;
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceDefaultListPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    return-object v0
.end method

.method public getReminderCharacterLimit()I
    .locals 2

    .line 222
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->getAutomaticReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->custom_message_char_limit:Ljava/lang/Integer;

    const-string v1, "getAutomaticReminderSett\u2026custom_message_char_limit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public hasInvoices()Z
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    const-string v1, "invoiceUnitMetadataPreference.get().has_invoices"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public invoiceDefaultsList()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceDefaultListPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "invoiceDefaultListPreference.asObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public maybeRefreshFromServer()V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->initialFetch:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public metrics()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceMetricsPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "invoiceMetricsPreference.asObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->setupInitialFetchDisposables()V

    .line 84
    invoke-direct {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->listenForPushes()V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public setHasInvoicesTrue()V
    .locals 3

    .line 187
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    .line 188
    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    .line 189
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 190
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_invoices(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object v1

    .line 191
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object v1

    .line 187
    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public unitMetadataDisplayDetails()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->invoiceUnitMetadataPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "invoiceUnitMetadataPreference.asObservable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public updateDefaultMessage(Ljava/lang/String;)V
    .locals 2

    const-string v0, "defaultMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/invoices/RealInvoiceUnitCache;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_INVOICE_DEFAULTS_MOBILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/squareup/invoices/RealInvoiceUnitCache;->getInvoiceDefaults()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object v0

    .line 173
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;

    move-result-object p1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDefaults$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object p1

    const-string v0, "newInvoiceDefaults"

    .line 176
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/invoices/RealInvoiceUnitCache;->updateInvoiceDefaults(Lcom/squareup/protos/client/invoice/InvoiceDefaults;)V

    goto :goto_0

    .line 178
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;-><init>()V

    .line 179
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->default_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;

    move-result-object p1

    .line 180
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadata;

    move-result-object p1

    const-string/jumbo v0, "unitMetadata"

    .line 182
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/invoices/RealInvoiceUnitCache;->updateUnitMetadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)V

    :goto_0
    return-void
.end method
