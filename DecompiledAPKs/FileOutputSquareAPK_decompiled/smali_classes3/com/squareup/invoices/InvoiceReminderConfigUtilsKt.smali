.class public final Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;
.super Ljava/lang/Object;
.source "InvoiceReminderConfigUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceReminderConfigUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceReminderConfigUtils.kt\ncom/squareup/invoices/InvoiceReminderConfigUtilsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,27:1\n1360#2:28\n1429#2,3:29\n1360#2:32\n1429#2,3:33\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceReminderConfigUtils.kt\ncom/squareup/invoices/InvoiceReminderConfigUtilsKt\n*L\n7#1:28\n7#1,3:29\n11#1:32\n11#1,3:33\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0003\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0004*\u0008\u0012\u0004\u0012\u00020\u00020\u0004\u001a\n\u0010\u0005\u001a\u00020\u0002*\u00020\u0001\u001a\u0016\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004*\u0008\u0012\u0004\u0012\u00020\u00010\u0004\u00a8\u0006\u0007"
    }
    d2 = {
        "toConfig",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "toConfigs",
        "",
        "toInstance",
        "toInstances",
        "invoices_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toConfig(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;
    .locals 2

    const-string v0, "$this$toConfig"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;-><init>()V

    .line 23
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object v0

    .line 24
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;

    move-result-object p0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object p0

    const-string v0, "InvoiceReminderConfig.Bu\u2026m_message)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toConfigs(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toConfigs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    check-cast p0, Ljava/lang/Iterable;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 29
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 30
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    .line 7
    invoke-static {v1}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toConfig(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 31
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
    .locals 2

    const-string v0, "$this$toInstance"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;-><init>()V

    .line 16
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;->relative_days:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days(Ljava/lang/Integer;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    .line 17
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;->custom_message:Ljava/lang/String;

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object p0

    .line 18
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object p0

    const-string v0, "InvoiceReminderInstance.\u2026m_message)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toInstances(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toInstances"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    check-cast p0, Ljava/lang/Iterable;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 33
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 34
    check-cast v1, Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    .line 11
    invoke-static {v1}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstance(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 35
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
