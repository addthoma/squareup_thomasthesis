.class public final Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory;
.super Ljava/lang/Object;
.source "ShowCreateNewButtonConfiguration_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory$InstanceHolder;->access$000()Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;

    invoke-direct {v0}, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory;->newInstance()Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration_Factory;->get()Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;

    move-result-object v0

    return-object v0
.end method
