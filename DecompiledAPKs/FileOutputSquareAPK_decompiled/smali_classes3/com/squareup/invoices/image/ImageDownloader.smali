.class public final Lcom/squareup/invoices/image/ImageDownloader;
.super Ljava/lang/Object;
.source "ImageDownloader.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011J\u0018\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c*\u0008\u0012\u0004\u0012\u00020\u00140\u0013H\u0002J\u0012\u0010\u0015\u001a\u00020\u0016*\u0008\u0012\u0004\u0012\u00020\u00140\u0017H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/invoices/image/ImageDownloader;",
        "",
        "invoiceFileHelper",
        "Lcom/squareup/invoices/image/InvoiceFileHelper;",
        "fileAttachmentServiceHelper",
        "Lcom/squareup/invoices/image/FileAttachmentServiceHelper;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;)V",
        "download",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/FileDownloadResult;",
        "invoiceTokenType",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "attachmentToken",
        "",
        "toDownloadResult",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;",
        "Lokhttp3/ResponseBody;",
        "toFailureResult",
        "Lcom/squareup/invoices/image/FileDownloadResult$Failed;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final fileAttachmentServiceHelper:Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/image/FileAttachmentServiceHelper;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceFileHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "fileAttachmentServiceHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageDownloader;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageDownloader;->fileAttachmentServiceHelper:Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

    iput-object p3, p0, Lcom/squareup/invoices/image/ImageDownloader;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p4, p0, Lcom/squareup/invoices/image/ImageDownloader;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/invoices/image/ImageDownloader;)Lcom/squareup/util/Res;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/invoices/image/ImageDownloader;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$toDownloadResult(Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lio/reactivex/Single;
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/invoices/image/ImageDownloader;->toDownloadResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toFailureResult(Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileDownloadResult$Failed;
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/invoices/image/ImageDownloader;->toFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    move-result-object p0

    return-object p0
.end method

.method private final toDownloadResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "+",
            "Lokhttp3/ResponseBody;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileDownloadResult;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageDownloader;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lokhttp3/ResponseBody;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper;->writeResponseBodyToFile(Lokhttp3/ResponseBody;)Lio/reactivex/Single;

    move-result-object p1

    .line 57
    sget-object v0, Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$1;->INSTANCE:Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 58
    new-instance v0, Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/image/ImageDownloader$toDownloadResult$2;-><init>(Lcom/squareup/invoices/image/ImageDownloader;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "invoiceFileHelper.writeR\u2026 \"\"\n          )\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toFailureResult(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/image/FileDownloadResult$Failed;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lokhttp3/ResponseBody;",
            ">;)",
            "Lcom/squareup/invoices/image/FileDownloadResult$Failed;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageDownloader;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 42
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_image_download_failed:I

    .line 43
    new-instance v2, Lcom/squareup/invoices/image/ImageDownloader$toFailureResult$failureMessage$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/image/ImageDownloader$toFailureResult$failureMessage$1;-><init>(Lcom/squareup/invoices/image/ImageDownloader;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 41
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 48
    new-instance v0, Lcom/squareup/invoices/image/FileDownloadResult$Failed;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/image/FileDownloadResult$Failed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final download(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileDownloadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceTokenType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageDownloader;->fileAttachmentServiceHelper:Lcom/squareup/invoices/image/FileAttachmentServiceHelper;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->downloadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 32
    new-instance p2, Lcom/squareup/invoices/image/ImageDownloader$download$1;

    invoke-direct {p2, p0}, Lcom/squareup/invoices/image/ImageDownloader$download$1;-><init>(Lcom/squareup/invoices/image/ImageDownloader;)V

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "fileAttachmentServiceHel\u2026t()\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
