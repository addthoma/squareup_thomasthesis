.class final Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;
.super Ljava/lang/Object;
.source "ImageUploader.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/image/ImageUploader;->uploadFileToInvoice(Ljava/io/File;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/image/FileUploadResult;",
        "it",
        "Lcom/squareup/util/Optional;",
        "Ljava/io/File;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

.field final synthetic this$0:Lcom/squareup/invoices/image/ImageUploader;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iput-object p2, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/Optional;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Ljava/io/File;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/FileUploadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;->this$0:Lcom/squareup/invoices/image/ImageUploader;

    iget-object v1, p0, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;->$invoiceTokenType:Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;

    invoke-static {v0, p1, v1}, Lcom/squareup/invoices/image/ImageUploader;->access$uploadFileToInvoice(Lcom/squareup/invoices/image/ImageUploader;Lcom/squareup/util/Optional;Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/image/ImageUploader$uploadFileToInvoice$1;->apply(Lcom/squareup/util/Optional;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
