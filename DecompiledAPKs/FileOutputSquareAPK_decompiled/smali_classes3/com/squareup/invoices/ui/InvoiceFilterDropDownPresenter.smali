.class public Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;
.super Lmortar/ViewPresenter;
.source "InvoiceFilterDropDownPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;",
        ">;"
    }
.end annotation


# instance fields
.field private dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->isDropDownVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->closeDropDown()V

    :cond_1
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 18
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    if-eqz p1, :cond_0

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    :cond_0
    return-void
.end method

.method public setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    goto :goto_0

    .line 27
    :cond_0
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->dropDownListener:Lcom/squareup/ui/DropDownContainer$DropDownListener;

    :goto_0
    return-void
.end method

.method public toggle()V
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->hasView()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->toggleDropDown()V

    return-void
.end method
