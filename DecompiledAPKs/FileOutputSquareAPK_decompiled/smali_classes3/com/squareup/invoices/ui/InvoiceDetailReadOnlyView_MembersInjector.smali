.class public final Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;
.super Ljava/lang/Object;
.source "InvoiceDetailReadOnlyView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;->injectPresenter(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView_MembersInjector;->injectMembers(Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyView;)V

    return-void
.end method
