.class public final Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator;
.super Ljava/lang/Object;
.source "InvoicesClientActionLinkTranslator.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionTranslator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator;",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "()V",
        "createViewSingleInvoiceUrlForId",
        "",
        "id",
        "translate",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ROOT_INVOICE_URL:Ljava/lang/String; = "square-register://invoice"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator;->Companion:Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final createViewSingleInvoiceUrlForId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "square-register://invoice/view/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public translate(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;
    .locals 2

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    const-string v1, "square-register://invoice"

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    invoke-direct {p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_1

    .line 14
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    invoke-direct {p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_1

    .line 15
    :cond_1
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;->access$hasInvoiceId(Lcom/squareup/protos/client/ClientAction;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 16
    new-instance v0, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->invoice_id:Ljava/lang/String;

    const-string v1, "clientAction.view_invoice.invoice_id"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator;->createViewSingleInvoiceUrlForId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_1

    .line 17
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    if-eqz v0, :cond_3

    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    invoke-direct {p1, v1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_1

    .line 18
    :cond_3
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;->access$hasOverdueInvoices(Lcom/squareup/protos/client/ClientAction;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 19
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;->access$getOverdueInvoiceIds(Lcom/squareup/protos/client/ClientAction;)Ljava/util/List;

    move-result-object p1

    .line 20
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    .line 22
    new-instance p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const-string v0, "square-register://invoice/outstanding"

    invoke-direct {p1, v0}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_0

    .line 21
    :cond_4
    new-instance v0, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "overdueInvoiceIds[0]"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslator;->createViewSingleInvoiceUrlForId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    :goto_0
    return-object p1

    .line 25
    :cond_5
    sget-object p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;->INSTANCE:Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    :goto_1
    return-object p1
.end method
