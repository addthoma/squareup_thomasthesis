.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->bind(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$2",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "v",
        "Landroid/view/View;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 760
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$2;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "v"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 762
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$2;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;

    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getPresenter$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->DRAFT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->setState(Lcom/squareup/invoices/ui/InvoiceStateFilter;)V

    return-void
.end method
