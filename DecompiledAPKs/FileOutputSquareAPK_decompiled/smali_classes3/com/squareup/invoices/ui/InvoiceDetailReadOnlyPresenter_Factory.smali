.class public final Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;
.super Ljava/lang/Object;
.source "InvoiceDetailReadOnlyPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final cartEntryViewsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CrmInvoiceListRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final withYearFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final withoutYearFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CrmInvoiceListRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;)V"
        }
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->cartEntryViewsFactoryProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p5, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->withYearFormatProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p6, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->withoutYearFormatProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p7, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p8, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p9, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p10, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->appletRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CrmInvoiceListRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;"
        }
    .end annotation

    .line 76
    new-instance v11, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/CrmInvoiceListRunner;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/cart/CartEntryViewsFactory;",
            "Lcom/squareup/invoices/ui/CrmInvoiceListRunner;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ")",
            "Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;"
        }
    .end annotation

    .line 84
    new-instance v11, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;-><init>(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/CrmInvoiceListRunner;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;
    .locals 11

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->cartEntryViewsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/cart/CartEntryViewsFactory;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/ui/CrmInvoiceListRunner;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->withYearFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->withoutYearFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Clock;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->appletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-static/range {v1 .. v10}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->newInstance(Lcom/squareup/ui/cart/CartEntryViewsFactory;Lcom/squareup/invoices/ui/CrmInvoiceListRunner;Lcom/squareup/util/Res;Lflow/Flow;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Clock;Lcom/squareup/settings/server/Features;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;)Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter_Factory;->get()Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyPresenter;

    move-result-object v0

    return-object v0
.end method
