.class Lcom/squareup/invoices/ui/InvoiceDetailView$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "InvoiceDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceDetailView;->setVisibilityAddPaymentButton(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceDetailView;)V
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$3;->this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 236
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$3;->this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoiceDetailView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->onAddPaymentClicked()V

    return-void
.end method
