.class public final Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;
.super Ljava/lang/Object;
.source "AddPaymentScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SectionData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J=\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0010\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;",
        "",
        "title",
        "",
        "subtitle",
        "drawableId",
        "",
        "buttonData",
        "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;",
        "helperText",
        "(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)V",
        "getButtonData",
        "()Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;",
        "getDrawableId",
        "()I",
        "getHelperText",
        "()Ljava/lang/String;",
        "getSubtitle",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

.field private final drawableId:I

.field private final helperText:Ljava/lang/String;

.field private final subtitle:Ljava/lang/String;

.field private final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonData"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    iput-object p4, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    iput-object p5, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    .line 112
    check-cast p5, Ljava/lang/String;

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->copy(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    return v0
.end method

.method public final component4()Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    return-object v0
.end method

.method public final component5()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;
    .locals 7

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "subtitle"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buttonData"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    iget v1, p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    iget-object v1, p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getButtonData()Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    return-object v0
.end method

.method public final getDrawableId()I
    .locals 1

    .line 110
    iget v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    return v0
.end method

.method public final getHelperText()Ljava/lang/String;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    return-object v0
.end method

.method public final getSubtitle()Ljava/lang/String;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SectionData(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", subtitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", drawableId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->drawableId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", buttonData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->buttonData:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$ButtonData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", helperText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$SectionData;->helperText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
