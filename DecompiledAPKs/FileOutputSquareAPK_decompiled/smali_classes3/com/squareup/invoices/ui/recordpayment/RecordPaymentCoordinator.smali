.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecordPaymentCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecordPaymentCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecordPaymentCoordinator.kt\ncom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,142:1\n1103#2,7:143\n*E\n*S KotlinDebug\n*F\n+ 1 RecordPaymentCoordinator.kt\ncom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator\n*L\n92#1,7:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 *2\u00020\u0001:\u0001*B=\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0008\u0010!\u001a\u00020\u001dH\u0002J\u0008\u0010\"\u001a\u00020#H\u0002J\u0010\u0010$\u001a\u00020\u001d2\u0006\u0010%\u001a\u00020&H\u0002J\u0008\u0010\'\u001a\u00020\u001dH\u0002J\u0010\u0010(\u001a\u00020)2\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "runner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "moneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "noteEditText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "paymentAmountEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "paymentMethodRow",
        "Lcom/squareup/ui/account/view/LineRow;",
        "sendReceiptToggle",
        "Lcom/squareup/widgets/list/ToggleButtonRow;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "getAllowedFixedAmountChars",
        "",
        "populateFromScreenData",
        "screenData",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        "setListeners",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ALLOWED_FIXED_AMOUNT_CHARS:Ljava/lang/String; = "0123456789.,"

.field public static final Companion:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field private noteEditText:Lcom/squareup/widgets/SelectableEditText;

.field private paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

.field private paymentMethodRow:Lcom/squareup/ui/account/view/LineRow;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;

.field private sendReceiptToggle:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->Companion:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;

    iput-object p3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p5, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 43
    new-instance p1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast p2, Lcom/squareup/money/MoneyExtractor;

    iget-object p3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 p4, 0x0

    invoke-static {p4, p5, p3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-direct {p1, p2, p6, p3}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    return-void
.end method

.method public static final synthetic access$getPriceLocaleHelper$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)Lcom/squareup/money/PriceLocaleHelper;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)V

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->spinnerData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 123
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 124
    sget v0, Lcom/squareup/features/invoices/R$id;->record_payment_amount:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

    .line 125
    sget v0, Lcom/squareup/features/invoices/R$id;->record_payment_method:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentMethodRow:Lcom/squareup/ui/account/view/LineRow;

    .line 126
    sget v0, Lcom/squareup/features/invoices/R$id;->record_payment_send_receipt:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->sendReceiptToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 127
    sget v0, Lcom/squareup/features/invoices/R$id;->record_payment_note:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->noteEditText:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method private final configureActionBar()V
    .locals 5

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 111
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->record_payment:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v3, 0x1

    .line 112
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 113
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$configureActionBar$configBuilder$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$configureActionBar$configBuilder$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 114
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->record_payment_save:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 115
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$configureActionBar$configBuilder$2;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$configureActionBar$configBuilder$2;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 119
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final getAllowedFixedAmountChars()Ljava/lang/String;
    .locals 2

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0123456789.,"

    .line 132
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1}, Lcom/squareup/currency_db/Currencies;->getCurrencySubunitSymbol(Lcom/squareup/protos/common/CurrencyCode;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder()\n        \u2026ode))\n        .toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getMaxRecordPaymentAmount()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 99
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getRecordPaymentAmount()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

    const-string v2, "paymentAmountEditText"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 100
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getRecordPaymentAmount()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getNote()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->noteEditText:Lcom/squareup/widgets/SelectableEditText;

    const-string v2, "noteEditText"

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 103
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->noteEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getNote()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_5
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentMethodRow:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_6

    const-string v1, "paymentMethodRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->getDisplayString()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->sendReceiptToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_7

    const-string v1, "sendReceiptToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getSendReceipt()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method private final setListeners()V
    .locals 4

    .line 72
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

    const-string v2, "paymentAmountEditText"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Lcom/squareup/text/HasSelectableText;

    sget-object v3, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v1, v3}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->moneyScrubber:Lcom/squareup/money/MaxMoneyScrubber;

    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->getAllowedFixedAmountChars()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v1

    check-cast v1, Landroid/text/method/KeyListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentAmountEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->noteEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_3

    const-string v1, "noteEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$2;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->sendReceiptToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_4

    const-string v1, "sendReceiptToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$3;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v1, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->paymentMethodRow:Lcom/squareup/ui/account/view/LineRow;

    if-nez v0, :cond_5

    const-string v1, "paymentMethodRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    .line 143
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$setListeners$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 68
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;->getShowSpinner()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    invoke-direct {p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->configureActionBar()V

    .line 54
    invoke-direct {p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->setListeners()V

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$Runner;->recordPaymentScreenData()Lrx/Observable;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$2;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.recordPaymentScre\u2026mScreenData(it)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
