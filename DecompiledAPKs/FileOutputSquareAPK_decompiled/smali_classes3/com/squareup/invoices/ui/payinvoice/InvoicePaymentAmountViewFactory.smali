.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "InvoicePaymentAmountViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "otherAmountDialogFactory",
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;",
        "(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "otherAmountDialogFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->Companion:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 12
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 13
    const-class v2, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 14
    new-instance v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory$1;

    invoke-direct {v3, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory$1;-><init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 12
    invoke-virtual {v1, v2, v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 17
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 18
    const-class v1, Lcom/squareup/invoices/ui/payinvoice/InvoiceAmountErrorDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 19
    sget-object v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory$2;->INSTANCE:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountViewFactory$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 17
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
