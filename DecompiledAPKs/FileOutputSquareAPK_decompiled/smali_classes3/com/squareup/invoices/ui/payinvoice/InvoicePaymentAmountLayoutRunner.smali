.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePaymentAmountLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePaymentAmountLayoutRunner.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,76:1\n1651#2,2:77\n1653#2:86\n1103#3,7:79\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePaymentAmountLayoutRunner.kt\ncom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner\n*L\n43#1,2:77\n43#1:86\n43#1,7:79\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u0000 \u00142\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u0002H\u0002J\u0018\u0010\u000e\u001a\u00020\u000c2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\r\u001a\u00020\u0002H\u0002J\u0018\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "kotlin.jvm.PlatformType",
        "optionsContainer",
        "Landroid/widget/LinearLayout;",
        "setOptions",
        "",
        "rendering",
        "setupActionBar",
        "resources",
        "Landroid/content/res/Resources;",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final optionsContainer:Landroid/widget/LinearLayout;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->Companion:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->view:Landroid/view/View;

    .line 28
    iget-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById<Action\u2026s.R.id.stable_action_bar)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 30
    iget-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/features/invoices/R$id;->payment_amount_options_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->optionsContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final setOptions(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V
    .locals 12

    .line 42
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->optionsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 43
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;->getOptionRows()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 78
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;

    .line 44
    new-instance v11, Lcom/squareup/noho/NohoCheckableRow;

    iget-object v5, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v5, "view.context"

    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x6

    const/4 v10, 0x0

    move-object v5, v11

    invoke-direct/range {v5 .. v10}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 45
    sget-object v5, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast v5, Lcom/squareup/noho/CheckType;

    invoke-virtual {v11, v5}, Lcom/squareup/noho/NohoCheckableRow;->setType(Lcom/squareup/noho/CheckType;)V

    .line 46
    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;->getTitle()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v11, v5}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 47
    invoke-virtual {v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;->getValue()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v11, v5}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;->getSelectedOption()I

    move-result v5

    if-ne v2, v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    invoke-virtual {v11, v5}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 51
    check-cast v11, Landroid/view/View;

    .line 79
    new-instance v5, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$setOptions$$inlined$forEachIndexed$lambda$1;

    invoke-direct {v5, v3, v2, p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$setOptions$$inlined$forEachIndexed$lambda$1;-><init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen$PaymentAmountOptionRow;ILcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V

    check-cast v5, Landroid/view/View$OnClickListener;

    invoke-virtual {v11, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->optionsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move v2, v4

    goto :goto_0

    :cond_2
    return-void
.end method

.method private final setupActionBar(Landroid/content/res/Resources;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 64
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;->getActionBarTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 65
    new-instance v2, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$setupActionBar$1;

    invoke-direct {v2, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$setupActionBar$1;-><init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 66
    sget v2, Lcom/squareup/common/strings/R$string;->next:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 67
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$setupActionBar$2;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$setupActionBar$2;-><init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner$showRendering$1;-><init>(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const-string/jumbo v0, "view.resources"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->setupActionBar(Landroid/content/res/Resources;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->setOptions(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountLayoutRunner;->showRendering(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
