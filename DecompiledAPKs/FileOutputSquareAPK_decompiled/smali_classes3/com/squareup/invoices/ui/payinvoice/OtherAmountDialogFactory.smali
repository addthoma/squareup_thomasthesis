.class public final Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;
.super Ljava/lang/Object;
.source "OtherAmountDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOtherAmountDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OtherAmountDialogFactory.kt\ncom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory\n*L\n1#1,115:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001!BI\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\"\u0010\t\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\u000e0\n\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0008H\u0002J\u0018\u0010\u0019\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0018\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u000cH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\t\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0008\u0012\u0004\u0012\u00020\u000c`\u000e0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lio/reactivex/Observable;)V",
        "moneyScrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "getMoneyScrubber",
        "()Lcom/squareup/money/MaxMoneyScrubber;",
        "configureScrubber",
        "",
        "editor",
        "Lcom/squareup/widgets/SelectableEditText;",
        "maxMoney",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/text/Formatter;Lio/reactivex/Observable;)V

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;Landroid/content/Context;Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getPriceLocaleHelper$p(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;)Lcom/squareup/money/PriceLocaleHelper;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    return-object p0
.end method

.method private final configureScrubber(Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 107
    invoke-direct {p0}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->getMoneyScrubber()Lcom/squareup/money/MaxMoneyScrubber;

    move-result-object v1

    .line 108
    invoke-virtual {v1, p2}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 111
    check-cast p1, Lcom/squareup/text/HasSelectableText;

    sget-object p2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p1

    .line 112
    check-cast v1, Lcom/squareup/text/Scrubber;

    invoke-virtual {p1, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    return-void
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)Landroid/app/AlertDialog;
    .locals 4

    .line 75
    sget v0, Lcom/squareup/flowlegacy/R$layout;->editor_dialog:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string v1, "root"

    .line 76
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/flowlegacy/R$id;->editor:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 77
    check-cast v1, Lcom/squareup/widgets/SelectableEditText;

    .line 78
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;->getAmount()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;->getMaxAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->configureScrubber(Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/protos/common/Money;)V

    .line 83
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    sget p1, Lcom/squareup/features/invoices/R$string;->payment_amount_option_other_amount:I

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 85
    sget v2, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v3, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$1;

    invoke-direct {v3, p2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$1;-><init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 88
    sget v2, Lcom/squareup/common/strings/R$string;->done:I

    new-instance v3, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;

    invoke-direct {v3, p0, v1, p2}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$createDialog$dialog$2;-><init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;Lcom/squareup/widgets/SelectableEditText;Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogScreen;)V

    check-cast v3, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 92
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 94
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 96
    invoke-virtual {v1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    const-string p2, "dialog"

    .line 97
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object p2

    if-eqz p2, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/Window;->setSoftInputMode(I)V

    :cond_0
    return-object p1
.end method

.method private final getMoneyScrubber()Lcom/squareup/money/MaxMoneyScrubber;
    .locals 6

    .line 62
    new-instance v0, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast v1, Lcom/squareup/money/MoneyExtractor;

    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    return-object v0
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;->screens:Lio/reactivex/Observable;

    .line 66
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 67
    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory$create$1;-><init>(Lcom/squareup/invoices/ui/payinvoice/OtherAmountDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .firstOr\u2026g(context, screen.data) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
