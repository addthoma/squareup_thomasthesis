.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;
.super Ljava/lang/Object;
.source "InvoicePaymentStarter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ \u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012J\u0018\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0014\u0010\u0017\u001a\u00020\u0014*\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J&\u0010\u001a\u001a\u00020\u000c*\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u0014\u0010\u0015\u001a\u00020\u0016*\u00020\u00122\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
        "",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V",
        "takePayment",
        "",
        "collectionAmount",
        "Lcom/squareup/protos/common/Money;",
        "contact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "tipSettings",
        "Lcom/squareup/settings/server/TipSettings;",
        "paymentConfig",
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "copyWithEnabled",
        "enabled",
        "",
        "initInvoiceTransaction",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "tenderStarter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method private final copyWithEnabled(Lcom/squareup/settings/server/TipSettings;Z)Lcom/squareup/settings/server/TipSettings;
    .locals 10

    .line 79
    new-instance v9, Lcom/squareup/settings/server/TipSettings;

    .line 81
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomPercentages()Z

    move-result v2

    .line 82
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result v3

    .line 83
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->isUsingSeparateTippingScreen()Z

    move-result v4

    .line 84
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->getTippingCalculationPhase()Lcom/squareup/settings/server/TipSettings$TippingCalculationPhase;

    move-result-object v5

    .line 85
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->getCustomPercentages()Ljava/util/List;

    move-result-object v6

    .line 86
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->getTipping()Lcom/squareup/server/account/protos/Tipping;

    move-result-object v7

    .line 87
    invoke-virtual {p1}, Lcom/squareup/settings/server/TipSettings;->isPreAuthTippingRequired()Z

    move-result v8

    move-object v0, v9

    move v1, p2

    .line 79
    invoke-direct/range {v0 .. v8}, Lcom/squareup/settings/server/TipSettings;-><init>(ZZZZLcom/squareup/settings/server/TipSettings$TippingCalculationPhase;Ljava/util/List;Lcom/squareup/server/account/protos/Tipping;Z)V

    return-object v9
.end method

.method private final initInvoiceTransaction(Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 52
    invoke-virtual {p3}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->isLastPaymentOnBill()Z

    move-result v0

    invoke-virtual {p1, p2, p4, v0}, Lcom/squareup/payment/Transaction;->startInvoiceTransaction(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/client/rolodex/Contact;Z)V

    .line 53
    iget-object p4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-direct {p0, p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->tipSettings(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/settings/server/TipSettings;

    move-result-object p2

    invoke-virtual {p4, p2}, Lcom/squareup/payment/Transaction;->setTipSettings(Lcom/squareup/settings/server/TipSettings;)V

    .line 54
    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->attributeChargeIfPossible()V

    return-void
.end method

.method private final paymentConfig(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;
    .locals 4

    .line 59
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    .line 60
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->totalPaid(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 62
    new-instance v2, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    .line 63
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const-string/jumbo v3, "totalAmount"

    .line 66
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {v2, p1, p2, v1, v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v2
.end method

.method private final tipSettings(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/settings/server/TipSettings;
    .locals 1

    .line 74
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v0, "invoice.invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->isTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->isLastPaymentOnBill()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 75
    :goto_0
    iget-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p2}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object p2

    const-string v0, "settings.tipSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->copyWithEnabled(Lcom/squareup/settings/server/TipSettings;Z)Lcom/squareup/settings/server/TipSettings;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final takePayment(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 1

    const-string v0, "collectionAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoice"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0, p3, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->paymentConfig(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object p1

    .line 38
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-direct {p0, v0, p3, p1, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->initInvoiceTransaction(Lcom/squareup/payment/Transaction;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 40
    iget-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 41
    new-instance p3, Lcom/squareup/invoices/analytics/InvoiceTakePaymentEvent;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->collectingTotalAmountOfBill()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-direct {p3, v0}, Lcom/squareup/invoices/analytics/InvoiceTakePaymentEvent;-><init>(Z)V

    check-cast p3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    .line 40
    invoke-interface {p2, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 43
    iget-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {p2, p1}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlowForPaymentConfig(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)V

    return-void
.end method
