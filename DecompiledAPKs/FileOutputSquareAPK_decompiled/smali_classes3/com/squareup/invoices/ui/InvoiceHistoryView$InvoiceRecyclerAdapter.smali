.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "InvoiceHistoryView.kt"

# interfaces
.implements Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InvoiceRecyclerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ButtonViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NoSearchResultViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$LoadMoreViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        ">;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter<",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceHistoryView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter\n*L\n1#1,900:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\t\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u000f\u0008\u0000\u0018\u0000 42\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00040\u0003:\n23456789:;B\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0014\u0010\u0019\u001a\u00020\u001a2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u0013J\u0008\u0010\u001d\u001a\u00020\u0008H\u0002J\u000e\u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u0010\u001a\u00020\u0011J\u0006\u0010\u001f\u001a\u00020\u001aJ\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0008\u0010#\u001a\u00020\u0016H\u0016J\u0010\u0010$\u001a\u00020!2\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0010\u0010%\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0012\u0010&\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\"\u001a\u00020\u0016H\u0002J\u0018\u0010\'\u001a\u00020\u001a2\u0006\u0010(\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0018\u0010)\u001a\u00020\u001a2\u0006\u0010*\u001a\u00020\u00022\u0006\u0010\"\u001a\u00020\u0016H\u0016J\u0010\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020-H\u0016J\u0018\u0010.\u001a\u00020\u00022\u0006\u0010,\u001a\u00020-2\u0006\u0010/\u001a\u00020\u0016H\u0016J\u0008\u00100\u001a\u00020\u0008H\u0002J\u0014\u00101\u001a\u00020\u001a2\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u000eR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u00020\u00168BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0017\u0010\u0018\u00a8\u0006<"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;",
        "presenter",
        "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
        "shouldShowCreateButton",
        "",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Z)V",
        "invoiceList",
        "Ljava/util/ArrayList;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;",
        "isSearching",
        "()Z",
        "isViewingArchive",
        "loadMoreState",
        "Lcom/squareup/ui/LoadMoreState;",
        "metrics",
        "",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "topRowCount",
        "",
        "getTopRowCount",
        "()I",
        "addItems",
        "",
        "invoices",
        "Lcom/squareup/invoices/DisplayDetails;",
        "canShowAnalyticsRow",
        "changeLoadingState",
        "clearItems",
        "getHeaderId",
        "",
        "position",
        "getItemCount",
        "getItemId",
        "getItemViewType",
        "getRow",
        "onBindHeaderViewHolder",
        "viewholder",
        "onBindViewHolder",
        "holder",
        "onCreateHeaderViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "onCreateViewHolder",
        "viewType",
        "showLoading",
        "updateAnalyticsRow",
        "AnalyticsViewHolder",
        "ButtonViewHolder",
        "Companion",
        "EstimatesBannerViewHolder",
        "HeaderViewHolder",
        "InvoiceViewHolder",
        "LoadMoreViewHolder",
        "NoSearchResultViewHolder",
        "NullViewHolder",
        "ViewHolder",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ANALYTICS_ROW:I = 0x5

.field private static final ANALYTICS_ROW_COUNT:I = 0x1

.field private static final CREATE_BUTTON_ROW:I = 0x0

.field private static final CREATE_BUTTON_ROW_COUNT:I = 0x1

.field public static final Companion:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$Companion;

.field private static final ESTIMATES_BANNER_ROW:I = 0x6

.field private static final ESTIMATES_BANNER_ROW_COUNT:I = 0x1

.field private static final INVOICE_ROW:I = 0x1

.field private static final LOAD_MORE_ROW:I = 0x2

.field private static final LOAD_MORE_ROW_COUNT:I = 0x1

.field private static final NO_SEARCH_RESULTS:I = 0x4

.field private static final NO_SEARCH_RESULTS_ROW_COUNT:I = 0x1

.field private static final NULL_STATE_ROW:I = 0x3

.field private static final NULL_STATE_ROW_COUNT:I = 0x1


# instance fields
.field private final invoiceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;",
            ">;"
        }
    .end annotation
.end field

.field private loadMoreState:Lcom/squareup/ui/LoadMoreState;

.field private metrics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

.field private final shouldShowCreateButton:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->Companion:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Z)V
    .locals 1

    const-string v0, "presenter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    iput-boolean p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    .line 387
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    .line 389
    sget-object p1, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    .line 390
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->metrics:Ljava/util/List;

    const/4 p1, 0x1

    .line 441
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->setHasStableIds(Z)V

    return-void
.end method

.method public static final synthetic access$getLoadMoreState$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/ui/LoadMoreState;
    .locals 0

    .line 382
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    return-object p0
.end method

.method public static final synthetic access$getPresenter$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
    .locals 0

    .line 382
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    return-object p0
.end method

.method public static final synthetic access$getRow(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;
    .locals 0

    .line 382
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getRow(I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setLoadMoreState$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Lcom/squareup/ui/LoadMoreState;)V
    .locals 0

    .line 382
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    return-void
.end method

.method private final canShowAnalyticsRow()Z
    .locals 1

    .line 445
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canShowAnalytics()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isSearching()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final getRow(I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;
    .locals 2

    .line 449
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 451
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v1

    sub-int/2addr p1, v1

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final getTopRowCount()I
    .locals 4

    .line 411
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v0, :cond_2

    .line 414
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isSearching()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isViewingArchive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 417
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_6

    :cond_1
    :goto_0
    const/4 v1, 0x1

    goto :goto_2

    .line 421
    :cond_2
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_2

    .line 423
    :cond_3
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canShowEstimatesBanner()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 424
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_4

    goto :goto_1

    :cond_4
    const/4 v1, 0x3

    goto :goto_2

    .line 427
    :cond_5
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 428
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_6

    goto :goto_0

    :cond_6
    :goto_1
    const/4 v1, 0x2

    goto :goto_2

    .line 431
    :cond_7
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canShowEstimatesBanner()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 432
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_6

    goto :goto_0

    .line 436
    :cond_8
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_1

    :goto_2
    return v1
.end method

.method private final isSearching()Z
    .locals 2

    .line 393
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getSearchTermString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final isViewingArchive()Z
    .locals 2

    .line 395
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getState()Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ARCHIVED:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final showLoading()Z
    .locals 2

    .line 653
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    sget-object v1, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public final addItems(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;)V"
        }
    .end annotation

    const-string v0, "invoices"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 456
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getItemCount()I

    move-result v0

    .line 457
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/DisplayDetails;

    .line 458
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    new-instance v4, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    invoke-direct {v4, v2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;-><init>(Lcom/squareup/invoices/DisplayDetails;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 460
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->notifyItemRangeInserted(II)V

    return-void
.end method

.method public final changeLoadingState(Lcom/squareup/ui/LoadMoreState;)V
    .locals 3

    const-string v0, "loadMoreState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 640
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    if-ne v1, p1, :cond_0

    return-void

    .line 644
    :cond_0
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->loadMoreState:Lcom/squareup/ui/LoadMoreState;

    .line 646
    sget-object v2, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    if-ne p1, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->notifyItemRemoved(I)V

    goto :goto_0

    .line 647
    :cond_1
    sget-object p1, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    if-ne v1, p1, :cond_2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->notifyItemInserted(I)V

    goto :goto_0

    .line 648
    :cond_2
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->notifyItemChanged(I)V

    :goto_0
    return-void
.end method

.method public final clearItems()V
    .locals 1

    .line 464
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 465
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getHeaderId(I)J
    .locals 2

    .line 657
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    add-int/lit8 p1, p1, -0x1

    .line 660
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getRow(I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->getHeaderDate$invoices_hairball_release()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->hashCode()I

    move-result p1

    :goto_0
    int-to-long v0, p1

    return-wide v0

    :cond_1
    if-eqz v0, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    goto :goto_1

    .line 669
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getRow(I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    move-result-object p1

    if-nez p1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->getHeaderDate$invoices_hairball_release()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Date;->hashCode()I

    move-result p1

    goto :goto_0

    :cond_4
    :goto_1
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getItemCount()I
    .locals 3

    .line 543
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->showLoading()Z

    move-result v0

    .line 544
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v1, v0

    return v1
.end method

.method public getItemId(I)J
    .locals 2

    .line 534
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 536
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getRow(I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->hashCode()I

    move-result p1

    :cond_0
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 7

    .line 549
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x6

    const/4 v2, 0x5

    const/4 v3, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eqz v0, :cond_9

    .line 551
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isSearching()Z

    move-result v0

    const-string v1, "Got position "

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isViewingArchive()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    .line 558
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_3

    if-nez p1, :cond_2

    :cond_1
    :goto_0
    const/4 v1, 0x3

    goto/16 :goto_6

    .line 561
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " when hiding create button"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 566
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasNeverCreatedInvoice()Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p1, :cond_1

    if-ne p1, v6, :cond_4

    goto/16 :goto_5

    .line 570
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " when creating invoice for the first time"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_5
    if-eqz p1, :cond_15

    if-ne p1, v6, :cond_6

    goto :goto_0

    .line 577
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " when not searching with no results"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_7
    :goto_1
    if-nez p1, :cond_8

    const/4 v1, 0x4

    goto/16 :goto_6

    .line 554
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " when searching with no results"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 582
    :cond_9
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 584
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne p1, v0, :cond_a

    :goto_2
    const/4 v1, 0x2

    goto/16 :goto_6

    :cond_a
    const/4 v1, 0x1

    goto/16 :goto_6

    .line 588
    :cond_b
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canShowEstimatesBanner()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 590
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_e

    if-nez p1, :cond_c

    .line 592
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v6, :cond_c

    goto/16 :goto_6

    :cond_c
    if-ne p1, v6, :cond_d

    .line 593
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v5, :cond_d

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_3
    goto :goto_4

    .line 595
    :cond_d
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_a

    goto :goto_2

    :cond_e
    if-nez p1, :cond_f

    .line 600
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v6, :cond_f

    goto/16 :goto_6

    :cond_f
    if-ne p1, v6, :cond_10

    .line 602
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v5, :cond_10

    goto :goto_5

    :cond_10
    if-ne p1, v5, :cond_11

    .line 603
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v4, :cond_11

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result v0

    if-eqz v0, :cond_11

    goto :goto_3

    .line 605
    :cond_11
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_a

    goto :goto_2

    .line 613
    :cond_12
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->shouldShowCreateButton:Z

    if-nez v0, :cond_14

    if-nez p1, :cond_13

    .line 614
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v6, :cond_13

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result v0

    if-eqz v0, :cond_13

    :goto_4
    const/4 v1, 0x5

    goto :goto_6

    .line 616
    :cond_13
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_a

    goto/16 :goto_2

    :cond_14
    if-nez p1, :cond_16

    .line 620
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v6, :cond_16

    :cond_15
    :goto_5
    const/4 v1, 0x0

    goto :goto_6

    :cond_16
    if-ne p1, v6, :cond_17

    .line 621
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    if-lt v0, v5, :cond_17

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_4

    .line 623
    :cond_17
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->invoiceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_a

    goto/16 :goto_2

    :goto_6
    return v1
.end method

.method public bridge synthetic onBindHeaderViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 382
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->onBindHeaderViewHolder(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;I)V

    return-void
.end method

.method public onBindHeaderViewHolder(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;I)V
    .locals 1

    const-string/jumbo v0, "viewholder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 683
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;->getTextView()Landroid/widget/TextView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getRow(I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;->getHeaderDate$invoices_hairball_release()Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->formatHeaderDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 382
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->onBindViewHolder(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 523
    instance-of v0, p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;

    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->metrics:Ljava/util/List;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->bind(Ljava/util/List;)V

    goto :goto_0

    .line 524
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ButtonViewHolder;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ButtonViewHolder;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ButtonViewHolder;->bind()V

    goto :goto_0

    .line 525
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;->bind(I)V

    goto :goto_0

    .line 526
    :cond_2
    instance-of p2, p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;

    if-eqz p2, :cond_3

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;->bind()V

    goto :goto_0

    .line 527
    :cond_3
    instance-of p2, p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;

    if-eqz p2, :cond_4

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;

    .line 528
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {p2}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasNeverCreatedEstimate()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasInvoicesAppInstalled()Z

    move-result v0

    .line 527
    invoke-virtual {p1, p2, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->bind(ZZ)V

    :cond_4
    :goto_0
    return-void
.end method

.method public bridge synthetic onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 382
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;
    .locals 3

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 674
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 675
    sget v1, Lcom/squareup/features/invoices/R$layout;->invoices_list_header_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 676
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;

    const-string v1, "inflate"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 382
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
    .locals 3

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "null cannot be cast to non-null type android.view.ViewGroup"

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    .line 514
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot understand viewtype "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 508
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 509
    sget v2, Lcom/squareup/features/invoices/R$layout;->estimates_banner:I

    .line 508
    invoke-virtual {p2, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    .line 511
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/ViewGroup;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 508
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 503
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 504
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_mobile_analytics_row:I

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 505
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;

    check-cast p1, Landroid/view/View;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 504
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.widget.LinearLayout"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 498
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 499
    sget v0, Lcom/squareup/features/invoices/R$layout;->no_invoice_search_results:I

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_2

    check-cast p1, Landroid/widget/TextView;

    .line 500
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NoSearchResultViewHolder;

    check-cast p1, Landroid/view/View;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NoSearchResultViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 499
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.widget.TextView"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 493
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 494
    sget v2, Lcom/squareup/features/invoices/R$layout;->invoice_null_state:I

    invoke-virtual {p2, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Landroid/view/ViewGroup;

    .line 495
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;

    check-cast p1, Landroid/view/View;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$NullViewHolder;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 494
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 513
    :pswitch_4
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$LoadMoreViewHolder;

    new-instance v0, Lcom/squareup/ui/LoadMoreRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/LoadMoreRow;-><init>(Landroid/content/Context;)V

    invoke-direct {p2, p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$LoadMoreViewHolder;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Lcom/squareup/ui/LoadMoreRow;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 484
    :pswitch_5
    invoke-static {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 485
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$onCreateViewHolder$2;

    invoke-direct {p2, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$onCreateViewHolder$2;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 490
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;

    const-string v0, "smartLineRow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$InvoiceViewHolder;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 474
    :pswitch_6
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 475
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoices_create_invoice_button:I

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_4

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    .line 476
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$onCreateViewHolder$1;

    invoke-direct {p2, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$onCreateViewHolder$1;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ButtonViewHolder;

    check-cast p1, Landroid/view/View;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ButtonViewHolder;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;

    return-object p2

    .line 475
    :cond_4
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.marketfont.MarketButton"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final updateAnalyticsRow(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;)V"
        }
    .end annotation

    const-string v0, "metrics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 632
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->metrics:Ljava/util/List;

    .line 633
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->canShowAnalyticsRow()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x0

    .line 634
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->getTopRowCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->notifyItemRangeChanged(II)V

    :cond_0
    return-void
.end method
