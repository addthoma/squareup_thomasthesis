.class public final Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;
.super Ljava/lang/Object;
.source "InvoicesAppletScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final addPaymentScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultInvoicesStateFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final downloadManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/SquareDownloadManager;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoiceGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final estimatesGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/timeline/EstimatesGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceBillLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceDetailTimelineDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFileHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceIssueRefundStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicePaymentStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceToDeepLinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final loaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionDiscountAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/timeline/EstimatesGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/SquareDownloadManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 130
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoicesAppletProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 131
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->loaderProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 132
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 133
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 134
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 135
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->cacheProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 136
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 137
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 138
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 139
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 140
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 141
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 142
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 143
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 144
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 145
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 146
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceBillLoaderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 147
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceIssueRefundStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 148
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 149
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->addPaymentScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 150
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 151
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 152
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceDetailTimelineDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 153
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->cancelScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 154
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->estimatesGatewayProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 155
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->defaultInvoicesStateFilterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 156
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 157
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->editInvoiceGatewayProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 158
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->downloadManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 159
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoicePaymentStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 160
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 161
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceToDeepLinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/timeline/EstimatesGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/SquareDownloadManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    .line 197
    new-instance v33, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;

    move-object/from16 v0, v33

    invoke-direct/range {v0 .. v32}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v33
.end method

.method public static newInstance(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/InvoiceLoader;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;Lcom/squareup/invoices/timeline/EstimatesGateway;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/http/SquareDownloadManager;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            "Lcom/squareup/analytics/Analytics;",
            "Lflow/Flow;",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;",
            "Lcom/squareup/invoices/timeline/EstimatesGateway;",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            "Lcom/squareup/http/SquareDownloadManager;",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ")",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    .line 218
    new-instance v33, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-object/from16 v0, v33

    invoke-direct/range {v0 .. v32}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;-><init>(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/InvoiceLoader;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;Lcom/squareup/invoices/timeline/EstimatesGateway;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/http/SquareDownloadManager;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V

    return-object v33
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
    .locals 34

    move-object/from16 v0, p0

    .line 166
    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoicesAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/invoicesappletapi/InvoicesApplet;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->loaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/ui/InvoiceLoader;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->cacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->transactionDiscountAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/payment/TransactionDiscountAdapter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/receiving/StandardReceiver;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceBillLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/invoices/ui/InvoiceBillLoader;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceIssueRefundStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->addPaymentScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/url/InvoiceShareUrlLauncher;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceDetailTimelineDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->cancelScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->estimatesGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/invoices/timeline/EstimatesGateway;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->defaultInvoicesStateFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->editInvoiceGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/invoices/edit/EditInvoiceGateway;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->downloadManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/http/SquareDownloadManager;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoicePaymentStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/invoices/image/InvoiceFileHelper;

    iget-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->invoiceToDeepLinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    invoke-static/range {v2 .. v33}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->newInstance(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/InvoiceLoader;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;Lcom/squareup/invoices/timeline/EstimatesGateway;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/http/SquareDownloadManager;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner_Factory;->get()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-result-object v0

    return-object v0
.end method
