.class public final Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;
.super Ljava/lang/Object;
.source "InvoicesDeepLinkHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final defaultInvoicesStateFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceToDeepLinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->invoicesAppletProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->defaultInvoicesStateFilterProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->invoiceToDeepLinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;-><init>(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->invoicesAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoicesappletapi/InvoicesApplet;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->defaultInvoicesStateFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->invoiceToDeepLinkProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->newInstance(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler_Factory;->get()Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method
