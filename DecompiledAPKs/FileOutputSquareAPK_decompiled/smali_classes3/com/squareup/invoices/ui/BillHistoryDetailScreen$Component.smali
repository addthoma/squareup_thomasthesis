.class public interface abstract Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.java"

# interfaces
.implements Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/invoices/ui/BillHistoryDetailView;)V
.end method
