.class Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
.super Lmortar/ViewPresenter;
.source "InvoiceHistoryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private canLinkToInvoicesApp:Z

.field private final canUseAnalytics:Z

.field private final dateFormat:Ljava/text/DateFormat;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final filterDropDownPresenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

.field private final flow:Lflow/Flow;

.field private hasEstimates:Z

.field private final invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

.field private final invoicesAppIntent:Landroid/content/Intent;

.field private isEnteringHistoryScreen:Z

.field isLeavingHistoryScreen:Z

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final onNearEndOfLists:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

.field private final showEstimatesBannerPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/invoices/ui/InvoiceLoader;Ljavax/inject/Provider;Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/applet/AppletSelection;Lcom/f2prateek/rx/preferences2/Preference;)V
    .locals 4
    .param p15    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/invoices/qualifier/ShowEstimatesBanner;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/analytics/Analytics;",
            "Lflow/Flow;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p3

    move-object v2, p10

    .line 98
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 82
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v3

    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->onNearEndOfLists:Lrx/subjects/PublishSubject;

    const/4 v3, 0x0

    .line 86
    iput-boolean v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isEnteringHistoryScreen:Z

    .line 87
    iput-boolean v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isLeavingHistoryScreen:Z

    .line 89
    iput-boolean v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasEstimates:Z

    .line 90
    iput-boolean v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canLinkToInvoicesApp:Z

    move-object v3, p1

    .line 99
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-object v3, p2

    .line 100
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 101
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    move-object v3, p4

    .line 102
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object v3, p5

    .line 103
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->dateFormat:Ljava/text/DateFormat;

    move-object v3, p6

    .line 104
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    move-object v3, p7

    .line 105
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->localeProvider:Ljavax/inject/Provider;

    move-object v3, p8

    .line 106
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->filterDropDownPresenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    move-object v3, p9

    .line 107
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->device:Lcom/squareup/util/Device;

    move-object v3, p11

    .line 108
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v3, p12

    .line 109
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->flow:Lflow/Flow;

    move-object/from16 v3, p13

    .line 110
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 111
    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v3, p14

    .line 112
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    move-object/from16 v3, p15

    .line 113
    iput-object v3, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->showEstimatesBannerPreference:Lcom/f2prateek/rx/preferences2/Preference;

    .line 114
    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ANALYTICS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p10, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    iput-boolean v2, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canUseAnalytics:Z

    .line 116
    sget v2, Lcom/squareup/features/invoices/R$string;->invoices_app_landing_page_url:I

    .line 117
    invoke-interface {p3, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 118
    new-instance v2, Landroid/content/Intent;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoicesAppIntent:Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)Lrx/subjects/PublishSubject;
    .locals 0

    .line 62
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->onNearEndOfLists:Lrx/subjects/PublishSubject;

    return-object p0
.end method

.method static synthetic lambda$null$10(Lcom/squareup/invoices/ui/InvoiceHistoryView;Ljava/util/concurrent/atomic/AtomicBoolean;Ljava/lang/Boolean;)V
    .locals 0

    .line 203
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    invoke-virtual {p0, p2, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->setSearch(ZZ)V

    return-void
.end method

.method static synthetic lambda$null$14(Lcom/squareup/util/Optional;Ljava/lang/Boolean;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Lcom/squareup/invoices/ListState;
    .locals 0

    .line 230
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 231
    sget-object p0, Lcom/squareup/invoices/ListState;->SHOW_LOADING:Lcom/squareup/invoices/ListState;

    return-object p0

    .line 232
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_1

    if-nez p2, :cond_1

    .line 233
    sget-object p0, Lcom/squareup/invoices/ListState;->SHOW_ERROR:Lcom/squareup/invoices/ListState;

    return-object p0

    .line 235
    :cond_1
    sget-object p0, Lcom/squareup/invoices/ListState;->SHOW_INVOICES:Lcom/squareup/invoices/ListState;

    return-object p0
.end method

.method static synthetic lambda$null$16(Lcom/squareup/util/Optional;Ljava/lang/Boolean;)Lcom/squareup/ui/LoadMoreState;
    .locals 0

    .line 246
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 247
    sget-object p0, Lcom/squareup/ui/LoadMoreState;->NO_CONNECTION_RETRY:Lcom/squareup/ui/LoadMoreState;

    return-object p0

    .line 248
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 249
    sget-object p0, Lcom/squareup/ui/LoadMoreState;->LOADING:Lcom/squareup/ui/LoadMoreState;

    return-object p0

    .line 251
    :cond_1
    sget-object p0, Lcom/squareup/ui/LoadMoreState;->NO_MORE:Lcom/squareup/ui/LoadMoreState;

    return-object p0
.end method

.method static synthetic lambda$null$20(Lcom/squareup/invoices/ui/InvoiceHistoryView;Ljava/lang/Boolean;)V
    .locals 0

    .line 266
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->clearInvoices()V

    :cond_0
    return-void
.end method

.method static synthetic lambda$null$22(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 274
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onLoad$2(Lcom/squareup/util/Optional;)Ljava/lang/Boolean;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;

    iget-boolean p0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onLoad$21(Lrx/Observable;Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 1

    .line 265
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$_U7-aEkBMp2ELqUVDm34l6ITUmo;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$_U7-aEkBMp2ELqUVDm34l6ITUmo;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-virtual {p0, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onLoad$3(Lcom/squareup/util/Optional;)Ljava/lang/Boolean;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;

    iget-boolean p0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private setTabletDetailTitle(Ljava/lang/String;)V
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getActionBar()Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setTitleNoUpButton(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private setUpFilters()V
    .locals 4

    .line 414
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    .line 416
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_ARCHIVE:Lcom/squareup/settings/server/Features$Feature;

    .line 417
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->getFilterValues(Z)Ljava/util/List;

    move-result-object v1

    .line 418
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    .line 419
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->addHeaderRow(Z)V

    .line 421
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/ui/GenericListFilter;

    .line 422
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->addFilterRow(Lcom/squareup/invoices/ui/GenericListFilter;)V

    goto :goto_0

    .line 426
    :cond_1
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_RECURRING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 427
    invoke-static {}, Lcom/squareup/invoices/ui/SeriesStateFilter;->getVisibleFilters()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    .line 428
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->addHeaderRow(Z)V

    .line 429
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/ui/GenericListFilter;

    .line 430
    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->addFilterRow(Lcom/squareup/invoices/ui/GenericListFilter;)V

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method canShowAnalytics()Z
    .locals 2

    .line 379
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentListFilter()Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_SENT:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canUseAnalytics:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public canShowEstimatesBanner()Z
    .locals 2

    .line 392
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->showEstimatesBannerPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$bool;->prompt_invoices_app_download:I

    .line 393
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICE_APP_BANNER:Lcom/squareup/settings/server/Features$Feature;

    .line 394
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canLinkToInvoicesApp:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method filterDropDownClicked()V
    .locals 1

    .line 305
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->filterDropDownPresenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->toggle()V

    return-void
.end method

.method formatHeaderDate(Ljava/util/Date;)Ljava/lang/String;
    .locals 3

    .line 316
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Times;->getRelativeDate(Ljava/util/Date;J)Lcom/squareup/util/Times$RelativeDate;

    move-result-object v0

    .line 318
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    .line 319
    sget-object v2, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$2;->$SwitchMap$com$squareup$util$Times$RelativeDate:[I

    invoke-virtual {v0}, Lcom/squareup/util/Times$RelativeDate;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 325
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 323
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/utilities/R$string;->yesterday:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 321
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/utilities/R$string;->today:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getCreateButtonText()Ljava/lang/String;
    .locals 2

    .line 367
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getState()Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/invoices/ui/GenericListFilter;->isRecurringListFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->create_recurring_series:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->create_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCustomerTitle(Lcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/String;
    .locals 1

    .line 330
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 331
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    return-object p1

    .line 333
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/strings/R$string;->no_customer:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getSearchTermString()Ljava/lang/String;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getSearchTerm()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/squareup/invoices/ui/GenericListFilter;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentListFilter()Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object v0

    return-object v0
.end method

.method public hasInvoicesAppInstalled()Z
    .locals 3

    .line 399
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 400
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoices_app_package_name:I

    .line 401
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 400
    invoke-static {v0, v1}, Lcom/squareup/util/PackageManagerUtils;->isPackageInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasNeverCreatedEstimate()Z
    .locals 1

    .line 388
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasEstimates:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasNeverCreatedInvoice()Z
    .locals 1

    .line 384
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->hasInvoices()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public synthetic lambda$null$0$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/applet/Applet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 133
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 134
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getActionBar()Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_history_title:I

    .line 135
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    return-void
.end method

.method public synthetic lambda$null$12$InvoiceHistoryPresenter(Ljava/lang/String;)V
    .locals 3

    const-string v0, ""

    .line 210
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->hasListFilter()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 212
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentListFilter()Lcom/squareup/invoices/ui/GenericListFilter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, v0}, Lcom/squareup/invoices/ui/GenericListFilter;->formatTitle(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->setTabletDetailTitle(Ljava/lang/String;)V

    .line 214
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceLoader;->setQuery(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmupdatecustomer/R$string;->search_hint:I

    .line 217
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->setTabletDetailTitle(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceLoader;->setQuery(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEARCH:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v1, v2, p1}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$18$InvoiceHistoryPresenter(Lkotlin/Unit;)V
    .locals 1

    .line 260
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 261
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method public synthetic lambda$null$4$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 2

    .line 150
    invoke-interface {p2}, Lcom/squareup/invoices/ui/GenericListFilter;->isRecurringListFilter()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    move-object v1, p2

    check-cast v1, Lcom/squareup/invoices/ui/SeriesStateFilter;

    .line 152
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/SeriesStateFilter;->getStateFilter()Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;

    move-result-object v1

    .line 151
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->setSeriesStateFilter(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest$StateFilter;)V

    goto :goto_0

    .line 154
    :cond_0
    invoke-interface {p2}, Lcom/squareup/invoices/ui/GenericListFilter;->isStateFilter()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    move-object v1, p2

    check-cast v1, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->setParentRecurringListFilter(Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;)V

    goto :goto_0

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    move-object v1, p2

    check-cast v1, Lcom/squareup/invoices/ui/InvoiceStateFilter;

    .line 159
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->getStateFilter()Lcom/squareup/protos/client/invoice/StateFilter;

    move-result-object v1

    .line 158
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->setInvoiceStateFilter(Lcom/squareup/protos/client/invoice/StateFilter;)V

    .line 162
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    invoke-interface {p2, v0}, Lcom/squareup/invoices/ui/GenericListFilter;->formatTitle(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->setTabletDetailTitle(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->setFilterTitle(Ljava/lang/String;)V

    .line 165
    iget-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isEnteringHistoryScreen:Z

    if-nez v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setIsSearching(Z)V

    .line 168
    :cond_2
    invoke-interface {p2}, Lcom/squareup/invoices/ui/GenericListFilter;->getSearchBarHint()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->setSearchBarHint(I)V

    return-void
.end method

.method public synthetic lambda$null$7$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 181
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasEstimates:Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 184
    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasEstimates:Z

    .line 186
    :goto_0
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 187
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->showFilterDropDownAndSearchIfPossible()V

    goto :goto_1

    .line 189
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->hideFilterDropDownAndSearch()V

    :goto_1
    return-void
.end method

.method public synthetic lambda$onLoad$1$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$yYXWAWYzhyj3nWDoz6F_5VD25hc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$yYXWAWYzhyj3nWDoz6F_5VD25hc;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$11$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 3

    .line 201
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 202
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getIsSearchingObservable()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$RxiBXrRNfJAza17VdaMwLJKAxMw;

    invoke-direct {v2, p1, v0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$RxiBXrRNfJAza17VdaMwLJKAxMw;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 203
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    const/4 v1, 0x1

    .line 204
    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-object p1
.end method

.method public synthetic lambda$onLoad$13$InvoiceHistoryPresenter()Lrx/Subscription;
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getSearchTerm()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$PtWqX07dbNNjSj-JZS8a1E13fvY;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$PtWqX07dbNNjSj-JZS8a1E13fvY;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$15$InvoiceHistoryPresenter(Lrx/Observable;Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 3

    .line 225
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    .line 226
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceLoader;->failure()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    .line 228
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->results()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$sBRCulP30rMGcMXKOBsxSWfHdOI;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$sBRCulP30rMGcMXKOBsxSWfHdOI;

    .line 225
    invoke-static {v0, p1, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p1

    .line 238
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$jaaYAKMJprrPPi09jaOIq-52juE;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/ui/-$$Lambda$jaaYAKMJprrPPi09jaOIq-52juE;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 239
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$17$InvoiceHistoryPresenter(Lrx/Observable;Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    .line 243
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceLoader;->failure()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$be1jbQjWmNdWgdroEms3666qnk8;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$be1jbQjWmNdWgdroEms3666qnk8;

    .line 242
    invoke-static {v0, p1, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    .line 254
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$NoWPUiomAQNrdy8iLMa8cKOVuaU;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/ui/-$$Lambda$NoWPUiomAQNrdy8iLMa8cKOVuaU;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 255
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$19$InvoiceHistoryPresenter()Lrx/Subscription;
    .locals 2

    .line 258
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->onNearEndOfLists:Lrx/subjects/PublishSubject;

    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$DjNZEVzh7QSUGuEBIeJqPyu5zpY;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$DjNZEVzh7QSUGuEBIeJqPyu5zpY;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)V

    .line 259
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$23$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    .line 273
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceLoader;->results()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$5JiRyu9KgtPSGkk_qQ1TXOZDgcg;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$5JiRyu9KgtPSGkk_qQ1TXOZDgcg;

    .line 274
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 275
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$cw58i8OFXSKgfArEsCEb0EDYrUQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$cw58i8OFXSKgfArEsCEb0EDYrUQ;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy()Lrx/Observable;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$T1lq8a4Q1roP_-FXPzNxASar9pY;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$T1lq8a4Q1roP_-FXPzNxASar9pY;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 174
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 178
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$O7QEfms6Vp03hcPkecHOFfL4M5I;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$O7QEfms6Vp03hcPkecHOFfL4M5I;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 180
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$InvoiceHistoryPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->metrics()Lio/reactivex/Observable;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$0aPC25LEZIIzEJXsPcs_t3ElhhE;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$0aPC25LEZIIzEJXsPcs_t3ElhhE;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 197
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method onCreateClicked()V
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->createNewInvoice()V

    return-void
.end method

.method onDisplayStateSelected(Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 4

    .line 309
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setCurrentListFilter(Lcom/squareup/invoices/ui/GenericListFilter;)V

    .line 310
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_FILTER:Lcom/squareup/analytics/RegisterActionName;

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->res:Lcom/squareup/util/Res;

    .line 311
    invoke-interface {p1}, Lcom/squareup/invoices/ui/GenericListFilter;->getTitle()I

    move-result p1

    invoke-interface {v3, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    .line 310
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 312
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->filterDropDownPresenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->close()V

    return-void
.end method

.method public onEstimatesBannerClicked()V
    .locals 2

    .line 409
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 410
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoicesAppIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onEstimatesBannerClosed()V
    .locals 2

    .line 405
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->showEstimatesBannerPreference:Lcom/f2prateek/rx/preferences2/Preference;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method onInvoiceClicked(Lcom/squareup/invoices/DisplayDetails;)V
    .locals 2

    const/4 v0, 0x1

    .line 338
    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isLeavingHistoryScreen:Z

    .line 341
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->clearSearchTextFocus()V

    .line 342
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->INVOICES_APPLET_INVOICE_DETAILS:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 344
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    .line 345
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 346
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result p1

    .line 344
    invoke-virtual {v0, v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->showInvoiceDetail(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 122
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setDisplayDetailsInput(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->clearInvoiceError()V

    .line 126
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoicesAppIntent:Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->canLinkToInvoicesApp:Z

    const/4 v0, 0x1

    .line 128
    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isEnteringHistoryScreen:Z

    const/4 v0, 0x0

    .line 129
    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isLeavingHistoryScreen:Z

    .line 130
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$S0_GbuXEbVbXdf4vUpSQTpui-GM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$S0_GbuXEbVbXdf4vUpSQTpui-GM;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 137
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->setUpFilters()V

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    .line 141
    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->progress()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$d6zmnEU7LX1O96BiVP-6tq2d52g;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$d6zmnEU7LX1O96BiVP-6tq2d52g;

    .line 142
    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 144
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->invoiceLoader:Lcom/squareup/invoices/ui/InvoiceLoader;

    .line 145
    invoke-virtual {v2}, Lcom/squareup/invoices/ui/InvoiceLoader;->progress()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$HkMhiwBkAqnGJajUE4TMF5yfLKo;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$HkMhiwBkAqnGJajUE4TMF5yfLKo;

    .line 146
    invoke-virtual {v2, v3}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v2

    .line 148
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$xyaN28Y__ofq5bsOE4v18ao5vUs;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$xyaN28Y__ofq5bsOE4v18ao5vUs;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 171
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$jDDeEAQMxt-rMwfBY8DiTEQpnqo;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$jDDeEAQMxt-rMwfBY8DiTEQpnqo;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 177
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$wa1NO65HBfCVCqtMGBTlH5aXHAs;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$wa1NO65HBfCVCqtMGBTlH5aXHAs;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 194
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$5Wvaw2-4kQze3iN4Q9leVImUGq0;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$5Wvaw2-4kQze3iN4Q9leVImUGq0;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 199
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$_039QYgDcG08cQqflWV6kT8Z1fw;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$_039QYgDcG08cQqflWV6kT8Z1fw;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 208
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$Esdz50_UC3OtOKmVLTznzBuG4t8;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$Esdz50_UC3OtOKmVLTznzBuG4t8;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 224
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$nCEDqkraeFEqx2bctxNmP11N0_E;

    invoke-direct {v3, p0, v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$nCEDqkraeFEqx2bctxNmP11N0_E;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lrx/Observable;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 241
    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$pbpZ5Mi-yi5iLU9bcvpV8AK9suI;

    invoke-direct {v3, p0, v2, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$pbpZ5Mi-yi5iLU9bcvpV8AK9suI;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lrx/Observable;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 257
    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$VfoHy8emvWLcbDIqnHGf326Ni8M;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$VfoHy8emvWLcbDIqnHGf326Ni8M;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 264
    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$pQY-7FyWWsRyFXAR-w847mBJiR4;

    invoke-direct {v2, v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$pQY-7FyWWsRyFXAR-w847mBJiR4;-><init>(Lrx/Observable;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 271
    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$eLKGMLc9DtUYpdWcAsljs2Zd8V4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoiceHistoryPresenter$eLKGMLc9DtUYpdWcAsljs2Zd8V4;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 297
    iput-boolean v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isEnteringHistoryScreen:Z

    return-void
.end method

.method public searchClicked()V
    .locals 2

    .line 358
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->filterDropDownPresenter:Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownPresenter;->close()V

    .line 359
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setIsSearching(Z)V

    return-void
.end method

.method searchExitClicked()V
    .locals 2

    .line 354
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setIsSearching(Z)V

    return-void
.end method

.method setState(Lcom/squareup/invoices/ui/InvoiceStateFilter;)V
    .locals 1

    .line 375
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setCurrentListFilter(Lcom/squareup/invoices/ui/GenericListFilter;)V

    return-void
.end method
