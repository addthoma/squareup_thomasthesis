.class public final Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;
.super Ljava/lang/Object;
.source "RealInvoicesApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/RealInvoicesApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ">;)",
            "Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;)Lcom/squareup/invoices/ui/RealInvoicesApplet;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/accountstatus/AccountStatusProvider;",
            ")",
            "Lcom/squareup/invoices/ui/RealInvoicesApplet;"
        }
    .end annotation

    .line 57
    new-instance v6, Lcom/squareup/invoices/ui/RealInvoicesApplet;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/RealInvoicesApplet;-><init>(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/RealInvoicesApplet;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v2, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v3, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v4, p0, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/accountstatus/AccountStatusProvider;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/Features;Lcom/squareup/accountstatus/AccountStatusProvider;)Lcom/squareup/invoices/ui/RealInvoicesApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/RealInvoicesApplet_Factory;->get()Lcom/squareup/invoices/ui/RealInvoicesApplet;

    move-result-object v0

    return-object v0
.end method
