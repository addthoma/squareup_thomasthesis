.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;
.super Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "AnalyticsViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceHistoryView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder\n+ 2 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,900:1\n17#2,2:901\n215#3,2:903\n215#3,2:905\n215#3,2:907\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder\n*L\n727#1,2:901\n740#1,2:903\n744#1,2:905\n748#1,2:907\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tR\u000e\u0010\u0002\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "linearLayout",
        "Landroid/view/View;",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V",
        "Landroid/widget/LinearLayout;",
        "bind",
        "",
        "metrics",
        "",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final linearLayout:Landroid/widget/LinearLayout;

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "linearLayout"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 719
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 720
    check-cast p2, Landroid/widget/LinearLayout;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    return-void
.end method


# virtual methods
.method public final bind(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;)V"
        }
    .end annotation

    const-string v0, "metrics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 723
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 724
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->access$getPresenter$p(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;)Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    const/16 v1, 0x1e

    .line 727
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->itemView:Landroid/view/View;

    const-string v3, "itemView"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "itemView.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 902
    new-instance v4, Lcom/squareup/fonts/FontSpan;

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v3

    invoke-direct {v4, v2, v3}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    .line 728
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast v2, Landroid/view/View;

    sget v3, Lcom/squareup/features/invoices/R$id;->paid_header:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 729
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 730
    sget v6, Lcom/squareup/features/invoices/R$string;->last_days:I

    invoke-static {v3, v6}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v6

    .line 731
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v7, "number"

    invoke-virtual {v6, v7, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 732
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 733
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 735
    sget v6, Lcom/squareup/common/invoices/R$string;->invoice_display_state_paid:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v6, "resources.getString(com.\u2026voice_display_state_paid)"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 736
    new-instance v6, Landroid/text/SpannableStringBuilder;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v6, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 737
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v6, v4, v5, v1, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 738
    check-cast v6, Ljava/lang/CharSequence;

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v2, v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 740
    check-cast p1, Ljava/lang/Iterable;

    .line 903
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "Collection contains no element matching the predicate."

    if-eqz v2, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    .line 740
    iget-object v4, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    sget-object v6, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    const/4 v7, 0x1

    if-ne v4, v6, :cond_2

    const/4 v4, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_1

    .line 741
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast v1, Landroid/view/View;

    sget v4, Lcom/squareup/features/invoices/R$id;->paid_amount:I

    invoke-static {v1, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 742
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 905
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    .line 744
    iget-object v4, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    sget-object v6, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    if-ne v4, v6, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_3

    .line 745
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast v1, Landroid/view/View;

    sget v4, Lcom/squareup/features/invoices/R$id;->outstanding_amount:I

    invoke-static {v1, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 746
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 907
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    .line 748
    iget-object v2, v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    sget-object v4, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    if-ne v2, v4, :cond_6

    const/4 v2, 0x1

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    :goto_2
    if-eqz v2, :cond_5

    .line 749
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    sget v2, Lcom/squareup/features/invoices/R$id;->draft_amount:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 750
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;->money_value:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 752
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/features/invoices/R$id;->paid_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 753
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$1;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 759
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/features/invoices/R$id;->draft_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 760
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$2;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 766
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;->linearLayout:Landroid/widget/LinearLayout;

    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/features/invoices/R$id;->outstanding_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    .line 767
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder$bind$3;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$AnalyticsViewHolder;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 908
    :cond_7
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 906
    :cond_8
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 904
    :cond_9
    new-instance p1, Ljava/util/NoSuchElementException;

    invoke-direct {p1, v3}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
