.class Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;
.super Ljava/lang/Object;
.source "InvoiceHistoryPresenter.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->lambda$onLoad$23(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
        "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
        "Lcom/squareup/invoices/DisplayDetails;",
        ">;>;"
    }
.end annotation


# instance fields
.field private previousSize:I

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

.field final synthetic val$view:Lcom/squareup/invoices/ui/InvoiceHistoryView;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Lcom/squareup/invoices/ui/InvoiceHistoryView;)V
    .locals 0

    .line 275
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->val$view:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 276
    iput p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Lcom/squareup/invoices/ui/InvoiceLoader$Input;",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;)V"
        }
    .end annotation

    .line 279
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 282
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v0, v1, :cond_0

    iget-object v0, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 283
    iput v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    .line 286
    :cond_1
    iget-boolean v0, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->hasMore:Z

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->access$000(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)Lrx/subjects/PublishSubject;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->val$view:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->onNearEndOfList()Lrx/Observable;

    move-result-object v1

    invoke-virtual {v1, v2}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 290
    :cond_2
    :goto_0
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 291
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->val$view:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    iget v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->addInvoicesToList(Ljava/util/List;)V

    .line 292
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->previousSize:I

    goto :goto_0

    :cond_3
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 275
    check-cast p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter$1;->call(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)V

    return-void
.end method
