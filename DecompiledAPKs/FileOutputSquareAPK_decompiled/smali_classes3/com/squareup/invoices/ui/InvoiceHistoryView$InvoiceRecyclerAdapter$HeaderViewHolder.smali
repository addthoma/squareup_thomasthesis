.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;
.super Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HeaderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "itemView",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "textView",
        "Landroid/widget/TextView;",
        "getTextView",
        "()Landroid/widget/TextView;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 686
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 687
    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;->textView:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final getTextView()Landroid/widget/TextView;
    .locals 1

    .line 687
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$HeaderViewHolder;->textView:Landroid/widget/TextView;

    return-object v0
.end method
