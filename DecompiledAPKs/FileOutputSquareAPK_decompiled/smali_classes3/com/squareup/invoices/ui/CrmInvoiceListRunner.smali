.class public interface abstract Lcom/squareup/invoices/ui/CrmInvoiceListRunner;
.super Ljava/lang/Object;
.source "CrmInvoiceListRunner.java"


# virtual methods
.method public abstract clearCurrentInvoice()V
.end method

.method public abstract getCurrentInvoiceDisplayDetails()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract setCurrentInvoiceId(Ljava/lang/String;)V
.end method
