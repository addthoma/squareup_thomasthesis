.class public abstract Lcom/squareup/invoices/editv2/service/SaveDraftResponse;
.super Ljava/lang/Object;
.source "SaveDraftResponse.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;,
        Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\t\u0082\u0001\u0002\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "",
        "()V",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "getInvoice",
        "()Lcom/squareup/protos/client/invoice/Invoice;",
        "isSuccess",
        "",
        "()Z",
        "Recurring",
        "SingleInvoice",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public final getInvoice()Lcom/squareup/protos/client/invoice/Invoice;
    .locals 5

    .line 38
    instance-of v0, p0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    const-string/jumbo v1, "when (successOrFailure) \u2026e.\"\n          )\n        }"

    const-string v2, "SaveDraftResponse was a failure - does not have an invoice."

    if-eqz v0, :cond_2

    .line 39
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v3

    .line 40
    instance-of v4, v3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 39
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 41
    :cond_0
    instance-of v0, v3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 46
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    if-eqz v0, :cond_5

    .line 47
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v3

    .line 48
    instance-of v4, v3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v4, :cond_3

    .line 49
    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->template:Lcom/squareup/protos/client/invoice/Invoice;

    .line 47
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 50
    :cond_3
    instance-of v0, v3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 47
    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public final isSuccess()Z
    .locals 1

    .line 28
    instance-of v0, p0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    goto :goto_0

    .line 29
    :cond_0
    instance-of v0, p0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    :goto_0
    return v0

    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method
