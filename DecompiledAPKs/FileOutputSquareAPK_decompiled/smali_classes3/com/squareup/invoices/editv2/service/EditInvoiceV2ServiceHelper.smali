.class public final Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;
.super Ljava/lang/Object;
.source "EditInvoiceV2ServiceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rJ\u001c\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\u000f0\u00082\u0006\u0010\u0011\u001a\u00020\rH\u0002J\u001c\u0010\u0012\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00130\u000f0\u00082\u0006\u0010\u0011\u001a\u00020\rH\u0002J\u001c\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u000f0\u00082\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\"\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u00082\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u0016\u001a\u00020\u0017J$\u0010\u001c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0\u000f0\u00082\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001bH\u0002J$\u0010\u001f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020 0\u000f0\u00082\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"H\u0002J\u001c\u0010#\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020$0\u000f0\u00082\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\"\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0\u00082\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u0016\u001a\u00020\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;",
        "",
        "clientInvoiceServiceHelper",
        "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "(Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/receiving/StandardReceiver;)V",
        "deleteDraft",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/editv2/service/DeleteDraftResponse;",
        "isRecurringDraft",
        "",
        "invoiceId",
        "Lcom/squareup/protos/client/IdPair;",
        "deleteRecurringDraft",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;",
        "idPair",
        "deleteSingleDraft",
        "Lcom/squareup/protos/client/invoice/DeleteDraftResponse;",
        "saveDraft",
        "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "rule",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "saveRecurringDraft",
        "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;",
        "recurrenceRule",
        "scheduleRecurringSeries",
        "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;",
        "schedule",
        "Lcom/squareup/protos/client/invoice/RecurringSchedule;",
        "sendOrSchedule",
        "Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/receiving/StandardReceiver;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "clientInvoiceServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "standardReceiver"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-void
.end method

.method private final deleteRecurringDraft(Lcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;",
            ">;>;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->deleteDraftSeries(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026deleteDraftSeries(idPair)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 126
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026)\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final deleteSingleDraft(Lcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/DeleteDraftResponse;",
            ">;>;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->deleteDraft(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelper.deleteDraft(idPair)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026)\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final saveDraft(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;",
            ">;>;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->saveDraftInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;

    move-result-object p1

    .line 98
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveDraft$3;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveDraft$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026il { it.status.success })"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026)\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final saveRecurringDraft(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;",
            ">;>;"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->saveRecurringDraft(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lrx/Observable;

    move-result-object p1

    .line 108
    iget-object p2, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveRecurringDraft$1;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveRecurringDraft$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v0}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    const-string p2, "clientInvoiceServiceHelp\u2026il { it.status.success })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 110
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "clientInvoiceServiceHelp\u2026)\n        .firstOrError()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final scheduleRecurringSeries(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/protos/client/invoice/RecurringSchedule;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;",
            ">;>;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->scheduleRecurringSeries(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lrx/Observable;

    move-result-object p1

    .line 91
    iget-object p2, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$scheduleRecurringSeries$1;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$scheduleRecurringSeries$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v0}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    const-string p2, "clientInvoiceServiceHelp\u2026il { it.status.success })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "clientInvoiceServiceHelp\u2026)\n        .firstOrError()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final sendOrSchedule(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;",
            ">;>;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->clientInvoiceServiceHelper:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->sendOrSchedule(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;

    move-result-object p1

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$sendOrSchedule$3;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$sendOrSchedule$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026il { it.status.success })"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 83
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "clientInvoiceServiceHelp\u2026)\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final deleteDraft(ZLcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/protos/client/IdPair;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/editv2/service/DeleteDraftResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "invoiceId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 69
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->deleteRecurringDraft(Lcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;

    move-result-object p1

    .line 70
    sget-object p2, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$deleteDraft$1;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$deleteDraft$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "deleteRecurringDraft(inv\u2026tResponse.Recurring(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 72
    :cond_0
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->deleteSingleDraft(Lcom/squareup/protos/client/IdPair;)Lio/reactivex/Single;

    move-result-object p1

    .line 73
    sget-object p2, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$deleteDraft$2;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$deleteDraft$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "deleteSingleDraft(invoic\u2026ponse.SingleInvoice(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public final saveDraft(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "rule"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoice"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->saveDraft(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object p1

    .line 57
    sget-object p2, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveDraft$1;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveDraft$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "saveDraft(invoice)\n     \u2026map { SingleInvoice(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_0
    instance-of v0, p1, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->saveRecurringDraft(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lio/reactivex/Single;

    move-result-object p1

    .line 60
    sget-object p2, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveDraft$2;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$saveDraft$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "saveRecurringDraft(invoi\u2026   .map { Recurring(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final sendOrSchedule(Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "rule"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoice"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->sendOrSchedule(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object p1

    .line 40
    sget-object p2, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$sendOrSchedule$1;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$sendOrSchedule$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "sendOrSchedule(invoice)\n\u2026ponse.SingleInvoice(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_0
    instance-of v0, p1, Lcom/squareup/util/Optional$Present;

    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->toRecurringSchedule(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object p1

    .line 45
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper;->scheduleRecurringSeries(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lio/reactivex/Single;

    move-result-object p1

    .line 46
    sget-object p2, Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$sendOrSchedule$2;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceV2ServiceHelper$sendOrSchedule$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "scheduleRecurringSeries(\u2026eResponse.Recurring(it) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
