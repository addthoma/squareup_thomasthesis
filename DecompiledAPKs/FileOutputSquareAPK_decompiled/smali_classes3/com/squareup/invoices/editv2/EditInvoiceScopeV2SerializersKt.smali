.class public final Lcom/squareup/invoices/editv2/EditInvoiceScopeV2SerializersKt;
.super Ljava/lang/Object;
.source "EditInvoiceScopeV2Serializers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeV2Serializers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeV2Serializers.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeV2SerializersKt\n*L\n1#1,34:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u001a\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t\u001a\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\n2\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0012\u0010\u000b\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007\u001a\u0012\u0010\u000c\u001a\u00020\u0004*\u00020\n2\u0006\u0010\u0006\u001a\u00020\u0007\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "WORKING_INVOICE_KEY",
        "",
        "WORKING_RECURRENCE_RULE",
        "restoreFromBundle",
        "",
        "Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;",
        "bundle",
        "Landroid/os/Bundle;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;",
        "writeInvoiceToBundle",
        "writeRuleToBundle",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final WORKING_INVOICE_KEY:Ljava/lang/String; = "workingInvoiceEditorInvoice"

.field private static final WORKING_RECURRENCE_RULE:Ljava/lang/String; = "workingRecurrenceRule"


# direct methods
.method public static final restoreFromBundle(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Landroid/os/Bundle;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 1

    const-string v0, "$this$restoreFromBundle"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bundle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingInvoiceEditorInvoice"

    .line 23
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    const-string v0, "byteArray"

    .line 24
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoiceFromByteArray([BLcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    :cond_0
    return-void
.end method

.method public static final restoreFromBundle(Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "$this$restoreFromBundle"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bundle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workingRecurrenceRule"

    .line 28
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    .line 29
    sget-object v0, Lokio/ByteString;->Companion:Lokio/ByteString$Companion;

    const-string v1, "byteArray"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    array-length v1, p1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lokio/ByteString$Companion;->of([B)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->ruleFromByteString(Lokio/ByteString;)V

    :cond_0
    return-void
.end method

.method public static final writeInvoiceToBundle(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "$this$writeInvoiceToBundle"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bundle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->workingInvoiceToByteArray()[B

    move-result-object p0

    const-string/jumbo v0, "workingInvoiceEditorInvoice"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public static final writeRuleToBundle(Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;Landroid/os/Bundle;)V
    .locals 1

    const-string v0, "$this$writeRuleToBundle"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bundle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/WorkingRecurrenceRuleEditor;->ruleToByteString()Lokio/ByteString;

    move-result-object p0

    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    const-string/jumbo v0, "workingRecurrenceRule"

    invoke-virtual {p1, v0, p0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method
