.class public final Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;
.super Ljava/lang/Object;
.source "EditInvoiceInstrumentsStore.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceInstrumentsStore.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceInstrumentsStore.kt\ncom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\t\u001a\u00020\nJ\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c2\u0006\u0010\u000f\u001a\u00020\u0010J&\u0010\u000b\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\r0\u000c2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0012J\u001c\u0010\u0014\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000e0\u00150\u000c2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012R\u001c\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;",
        "",
        "rolodex",
        "Lcom/squareup/crm/RolodexServiceHelper;",
        "(Lcom/squareup/crm/RolodexServiceHelper;)V",
        "instrumentsForContact",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;",
        "kotlin.jvm.PlatformType",
        "clear",
        "",
        "findInstrument",
        "Lio/reactivex/Single;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "contactToken",
        "",
        "instrumentToken",
        "instruments",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final instrumentsForContact:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "rolodex"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 32
    invoke-static {}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStoreKt;->access$getEMPTY$p()Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.createDefault(EMPTY)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instrumentsForContact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getInstrumentsForContact$p(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instrumentsForContact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getRolodex$p(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;)Lcom/squareup/crm/RolodexServiceHelper;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-object p0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instrumentsForContact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStoreKt;->access$getEMPTY$p()Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final findInstrument(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 103
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->findInstrument(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    sget-object p1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(Optional.empty())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public final findInstrument(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    if-nez p2, :cond_0

    .line 85
    sget-object p1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(Optional.empty())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 88
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instruments(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 89
    new-instance v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$findInstrument$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$findInstrument$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "instruments(contactToken\u2026trumentToken })\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final instruments(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instrumentsForContact:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 52
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 53
    new-instance v1, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;-><init>(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 70
    new-instance v1, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$2;-><init>(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "instrumentsForContact\n  \u2026tactToken, it))\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
