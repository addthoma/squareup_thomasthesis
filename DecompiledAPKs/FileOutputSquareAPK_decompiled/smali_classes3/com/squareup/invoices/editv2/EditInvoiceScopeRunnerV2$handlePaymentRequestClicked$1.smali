.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->handlePaymentRequestClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$PaymentRequestClicked;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $index:I

.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    iput p2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->$index:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/protos/client/invoice/Invoice;)V
    .locals 7

    .line 1656
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    .line 1657
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;

    .line 1658
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    iget v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->$index:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "invoice.payment_request[index]"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const-string v3, "invoice"

    .line 1659
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1660
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v4

    .line 1662
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getCurrentTime$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/time/CurrentTime;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v6

    const/4 v5, 0x0

    move-object v3, p1

    .line 1657
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState$Companion;->createInfo(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p1

    .line 1663
    iget v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->$index:I

    .line 1656
    invoke-interface {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startEditingPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$handlePaymentRequestClicked$1;->accept(Lcom/squareup/protos/client/invoice/Invoice;)V

    return-void
.end method
