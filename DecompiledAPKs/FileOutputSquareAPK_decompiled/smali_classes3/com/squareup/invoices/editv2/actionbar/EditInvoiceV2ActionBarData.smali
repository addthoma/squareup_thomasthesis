.class public final Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;
.super Ljava/lang/Object;
.source "EditInvoiceV2ActionBarData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u0018B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;",
        "",
        "title",
        "",
        "formattedAmount",
        "",
        "useBigHeader",
        "",
        "(Ljava/lang/String;Ljava/lang/CharSequence;Z)V",
        "getFormattedAmount",
        "()Ljava/lang/CharSequence;",
        "getTitle",
        "()Ljava/lang/String;",
        "getUseBigHeader",
        "()Z",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final formattedAmount:Ljava/lang/CharSequence;

.field private final title:Ljava/lang/String;

.field private final useBigHeader:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Z)V
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    iput-boolean p3, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/lang/CharSequence;ZILjava/lang/Object;)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->copy(Ljava/lang/String;Ljava/lang/CharSequence;Z)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    return v0
.end method

.method public final copy(Ljava/lang/String;Ljava/lang/CharSequence;Z)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;
    .locals 1

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "formattedAmount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFormattedAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final getUseBigHeader()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditInvoiceV2ActionBarData(title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", formattedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->formattedAmount:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", useBigHeader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->useBigHeader:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
