.class public final Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceV2ConfirmationCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ">;)",
            "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Lcom/squareup/hudtoaster/HudToaster;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;-><init>(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Lcom/squareup/hudtoaster/HudToaster;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/hudtoaster/HudToaster;

    invoke-static {v0, v1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;->newInstance(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$Runner;Lcom/squareup/hudtoaster/HudToaster;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator_Factory;->get()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationCoordinator;

    move-result-object v0

    return-object v0
.end method
