.class public final Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;
.super Ljava/lang/Object;
.source "EditInvoiceV2ConfirmationScreenDataFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0008\u0010\u0013\u001a\u00020\nH\u0002J\u0006\u0010\u0014\u001a\u00020\nJ\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\"\u0010\u001a\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0017\u001a\u00020\u00182\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002J\u0010\u0010\u001b\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0008\u0010\u001c\u001a\u00020\nH\u0002J\u0010\u0010\u001d\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0008\u0010\u001e\u001a\u00020\nH\u0002J\u000e\u0010\u001f\u001a\u00020\n2\u0006\u0010 \u001a\u00020!J\u000e\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020$J\"\u0010%\u001a\u00020\n2\u0006\u0010&\u001a\u00020\'2\u0006\u0010\u0017\u001a\u00020\u00182\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010(\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0010\u0010)\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0010\u0010*\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010+\u001a\u00020\nH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Res;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Clock;)V",
        "chargedInvoice",
        "Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "instrumentSummary",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "createSaveDraftFailure",
        "failureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "createSaveDraftRecurringFailure",
        "createSaveDraftRecurringSuccess",
        "createSaveDraftSuccess",
        "createScheduleSeriesFailure",
        "createScheduleSeriesSuccess",
        "isUpdating",
        "",
        "createSendSingleInvoiceFailure",
        "createSendSingleInvoiceSuccess",
        "deleteRecurringFailure",
        "deleteRecurringSuccess",
        "deleteSingleInvoiceFailure",
        "deleteSingleInvoiceSuccess",
        "fromDeleteDraftResponse",
        "deleteDraftResponse",
        "Lcom/squareup/invoices/editv2/service/DeleteDraftResponse;",
        "fromSaveDraftResponse",
        "saveDraftResponse",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "fromSendInvoiceResponse",
        "sendInvoiceResponse",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        "scheduledInvoice",
        "sentInvoice",
        "sharedInvoice",
        "updatingSingleInvoice",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private final chargedInvoice(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 8

    .line 129
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_charged:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 130
    iget-object v1, p2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v1, v1, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "brand"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 131
    iget-object p2, p2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object p2, p2, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object p2, p2, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "pan"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 132
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 133
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    iget-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/common/invoices/R$string;->invoice_charged_receipt_sent:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 137
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const-string v2, "buyer_name"

    invoke-virtual {p2, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 138
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    const-string v0, "email"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 139
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 140
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 142
    new-instance p1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 146
    iget-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_charged_title:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p1

    .line 142
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final createSaveDraftFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 248
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 249
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 252
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_save_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 248
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final createSaveDraftRecurringFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 268
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 269
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 270
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 272
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_save_recurring_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 268
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final createSaveDraftRecurringSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 257
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 258
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_saved_recurring:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 261
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_save_recurring_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v2, ""

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 257
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final createScheduleSeriesFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 228
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 229
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 230
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 232
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_schedule_recurring_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 228
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final createScheduleSeriesSuccess(Z)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 8

    if-eqz p1, :cond_0

    .line 214
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->recurring_series_updated:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 215
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_scheduled_recurring_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    move-object v1, p1

    .line 217
    new-instance p1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    const/4 v3, 0x1

    .line 221
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_schedule_recurring_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    const-string v2, ""

    move-object v0, p1

    .line 217
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final createSendSingleInvoiceFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 205
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 206
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, ""

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 205
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final createSendSingleInvoiceSuccess(Lcom/squareup/protos/client/invoice/Invoice;ZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 1

    if-eqz p2, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->updatingSingleInvoice()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 113
    :cond_0
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p2, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 114
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p2

    sget-object v0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 p3, 0x2

    if-eq p2, p3, :cond_1

    .line 117
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->sentInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 116
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->sharedInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    :cond_2
    if-nez p3, :cond_3

    .line 115
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-direct {p0, p1, p3}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->chargedInvoice(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 120
    :cond_4
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->scheduledInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final deleteRecurringFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 304
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 305
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 306
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 308
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_delete_series_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 304
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final deleteRecurringSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 295
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 296
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_deleted_series:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 299
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_delete_series_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v2, ""

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 295
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final deleteSingleInvoiceFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 286
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 287
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 288
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 290
    iget-object p1, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_delete_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 286
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method private final deleteSingleInvoiceSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 277
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 278
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_deleted:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 281
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_delete_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v2, ""

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 277
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method public static synthetic fromSendInvoiceResponse$default(Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;ZLcom/squareup/protos/client/instruments/InstrumentSummary;ILjava/lang/Object;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 34
    check-cast p3, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->fromSendInvoiceResponse(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;ZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method private final scheduledInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 10

    .line 182
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_scheduled:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 183
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v4, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    .line 185
    new-instance p1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    const-string v0, "subtitle"

    .line 187
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_scheduled_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x10

    const/4 v9, 0x0

    move-object v2, p1

    .line 185
    invoke-direct/range {v2 .. v9}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final sentInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 167
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_sent:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 168
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "recipient"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 171
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    .line 173
    new-instance p1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    const-string v0, "subtitle"

    .line 175
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_send_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x1

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, p1

    .line 173
    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final sharedInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 8

    .line 151
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_created:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 152
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_created_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 153
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "email"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 154
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 157
    new-instance p1, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 161
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->create_invoice:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x1

    const/4 v7, 0x1

    move-object v2, p1

    .line 157
    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V

    return-object p1
.end method

.method private final updatingSingleInvoice()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 194
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 195
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_updated:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->update_invoice:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v2, ""

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 194
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method


# virtual methods
.method public final createSaveDraftSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 9

    .line 237
    new-instance v8, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    .line 238
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_saved:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 241
    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_save_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v2, ""

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, v8

    .line 237
    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method public final fromDeleteDraftResponse(Lcom/squareup/invoices/editv2/service/DeleteDraftResponse;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 2

    const-string v0, "deleteDraftResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$SingleInvoice;

    if-eqz v0, :cond_2

    .line 87
    check-cast p1, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$SingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 88
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->deleteSingleInvoiceSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 89
    :cond_0
    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {p1, v0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->deleteSingleInvoiceFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 94
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$Recurring;

    if-eqz v0, :cond_5

    .line 95
    check-cast p1, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 96
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->deleteRecurringSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 97
    :cond_3
    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_4

    .line 98
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/DeleteDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {p1, v0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 97
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->deleteRecurringFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 95
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final fromSaveDraftResponse(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 2

    const-string v0, "saveDraftResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    if-eqz v0, :cond_2

    .line 64
    check-cast p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 65
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSaveDraftSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 66
    :cond_0
    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {p1, v0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSaveDraftFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 71
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    if-eqz v0, :cond_5

    .line 72
    check-cast p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 73
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSaveDraftRecurringSuccess()Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 74
    :cond_3
    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_4

    .line 75
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {p1, v0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSaveDraftRecurringFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 72
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final fromSendInvoiceResponse(Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;ZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;
    .locals 2

    const-string v0, "sendInvoiceResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    instance-of v0, p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;

    if-eqz v0, :cond_2

    .line 38
    check-cast p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    .line 39
    instance-of v1, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    const-string v0, "sendInvoiceResponse.succ\u2026.response.invoice.invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSendSingleInvoiceSuccess(Lcom/squareup/protos/client/invoice/Invoice;ZLcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 43
    :cond_0
    instance-of p2, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p2, :cond_1

    .line 44
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$SingleInvoice;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    iget-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {p1, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 43
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createSendSingleInvoiceFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 48
    :cond_2
    instance-of p3, p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;

    if-eqz p3, :cond_5

    .line 49
    check-cast p1, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p3

    .line 50
    instance-of v0, p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_3

    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createScheduleSeriesSuccess(Z)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    goto :goto_0

    .line 51
    :cond_3
    instance-of p2, p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p2, :cond_4

    .line 52
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SendInvoiceResponse$Recurring;->getSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    iget-object p2, p0, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static {p1, p2}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreenDataFactory;->createScheduleSeriesFailure(Lcom/squareup/receiving/FailureMessage;)Lcom/squareup/invoices/editv2/confirmation/EditInvoiceV2ConfirmationScreen$ScreenData;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 49
    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
