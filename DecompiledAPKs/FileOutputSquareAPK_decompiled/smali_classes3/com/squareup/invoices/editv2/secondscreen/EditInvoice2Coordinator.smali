.class public final Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditInvoice2Coordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoice2Coordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoice2Coordinator.kt\ncom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,132:1\n1103#2,7:133\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoice2Coordinator.kt\ncom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator\n*L\n55#1,7:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\r\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J \u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010#\u001a\u00020\u00162\u0006\u0010$\u001a\u00020%H\u0002J \u0010&\u001a\u00020\u00162\u0006\u0010\'\u001a\u00020\"2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020%H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;",
        "invoiceSectionContainerViewFactory",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "bigTextHeader",
        "Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;",
        "bottomButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "sectionsContainer",
        "Landroid/widget/LinearLayout;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
        "updateActionBar",
        "actionBarData",
        "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;",
        "showOverflowMenuButton",
        "",
        "updateBottomButton",
        "bottomButtonText",
        "",
        "updateHeader",
        "useBigHeader",
        "title",
        "",
        "subtitle",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private bigTextHeader:Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;

.field private bottomButton:Lcom/squareup/marketfont/MarketButton;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final invoiceSectionContainerViewFactory:Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;

.field private final runner:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;

.field private sectionsContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceSectionContainerViewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->runner:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->invoiceSectionContainerViewFactory:Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getFeatures$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/settings/server/Features;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->features:Lcom/squareup/settings/server/Features;

    return-object p0
.end method

.method public static final synthetic access$getInvoiceSectionContainerViewFactory$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->invoiceSectionContainerViewFactory:Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerViewFactory;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->runner:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getSectionsContainer$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)Landroid/widget/LinearLayout;
    .locals 1

    .line 34
    iget-object p0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->sectionsContainer:Landroid/widget/LinearLayout;

    if-nez p0, :cond_0

    const-string v0, "sectionsContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setSectionsContainer$p(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->sectionsContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->spinnerData(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateActionBar(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;ZLandroid/view/View;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->updateActionBar(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;ZLandroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateBottomButton(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Ljava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->updateBottomButton(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$updateHeader(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;ZLjava/lang/CharSequence;Ljava/lang/String;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->updateHeader(ZLjava/lang/CharSequence;Ljava/lang/String;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 125
    sget v0, Lcom/squareup/features/invoices/R$id;->big_text_header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.big_text_header)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bigTextHeader:Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;

    .line 126
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_invoice_2_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.edit_invoice_2_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->sectionsContainer:Landroid/widget/LinearLayout;

    .line 127
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 129
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_invoice_bottom_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.edit_invoice_bottom_button)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bottomButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method private final spinnerData(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 82
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->getShowProgress()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final updateActionBar(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;ZLandroid/view/View;)V
    .locals 4

    .line 121
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 106
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 108
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getUseBigHeader()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 111
    :cond_1
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;->getTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    :goto_0
    const/4 v2, 0x1

    .line 113
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 114
    new-instance v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;ZLandroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    if-eqz p2, :cond_2

    .line 117
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo p2, "view.resources"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    const-string/jumbo p3, "view.context"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p3, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$updateActionBar$1$2;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->runner:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;

    invoke-direct {p3, v2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$updateActionBar$1$2;-><init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    .line 116
    invoke-static {v1, p1, p2, p3}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogKt;->showOverflowMenu(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;Landroid/content/res/Resources;Landroid/content/Context;Lkotlin/jvm/functions/Function0;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 121
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final updateBottomButton(Ljava/lang/String;)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bottomButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "bottomButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateHeader(ZLjava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bigTextHeader:Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;

    const-string v1, "bigTextHeader"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_3

    .line 96
    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bigTextHeader:Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->setTitle(Ljava/lang/CharSequence;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bigTextHeader:Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->setSubtitle(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bindViews(Landroid/view/View;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 53
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->bottomButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "bottomButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 133
    new-instance v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->runner:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Screen$Runner;->editInvoice2ScreenData()Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$2;

    move-object v3, p0

    check-cast v3, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;

    invoke-direct {v2, v3}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$2;-><init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    const-string v2, "glassSpinner.spinnerTransform(::spinnerData)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v2, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 61
    new-instance v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator$attach$3;-><init>(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2Coordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "runner.editInvoice2Scree\u2026EW)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
