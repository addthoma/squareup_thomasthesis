.class final Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lcom/squareup/analytics/EventNamedAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "getName"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets$1;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets$1;->INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$OldWidgets$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Invoices: Creation V2 Old Widgets"

    return-object v0
.end method
