.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editReminders()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00040\u00012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0004H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "kotlin.jvm.PlatformType",
        "",
        "list",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;-><init>()V

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editReminders$3;->apply(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation

    const-string v0, "list"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 1450
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    return-object p1
.end method
