.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editInvoice1ScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u000122\u0010\u0002\u001a.\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u0007\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00050\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/util/tuple/Quartet;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/util/tuple/Quartet;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/tuple/Quartet<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;)",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component1()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component2()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component3()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Lcom/squareup/util/tuple/Quartet;->component4()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    .line 377
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoice1ScreenDataFactory$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;

    move-result-object v3

    .line 378
    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    move-object v4, p1

    check-cast v4, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v5

    const-string p1, "busy"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 379
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getInCheckout$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Z

    move-result v7

    move-object v1, v3

    move-object v3, v0

    .line 377
    invoke-virtual/range {v1 .. v7}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->create(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/client/instruments/InstrumentSummary;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZZ)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/util/tuple/Quartet;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editInvoice1ScreenData$2;->apply(Lcom/squareup/util/tuple/Quartet;)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;

    move-result-object p1

    return-object p1
.end method
