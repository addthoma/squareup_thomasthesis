.class public final Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;
.super Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;
.source "EditInvoiceOverflowDialog.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$ParentComponent;,
        Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Factory;,
        Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$Runner;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceOverflowDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceOverflowDialog.kt\ncom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,81:1\n52#2:82\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceOverflowDialog.kt\ncom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog\n*L\n41#1:82\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\u000b\u000c\rB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog;",
        "Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/MaybePersistent;",
        "editInvoiceV2Scope",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "Factory",
        "ParentComponent",
        "Runner",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V
    .locals 1

    const-string v0, "editInvoiceV2Scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    .line 32
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    const-class v0, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 41
    check-cast p1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$ParentComponent;

    .line 42
    invoke-interface {p1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialog$ParentComponent;->overflowDialogCoordinator()Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method
