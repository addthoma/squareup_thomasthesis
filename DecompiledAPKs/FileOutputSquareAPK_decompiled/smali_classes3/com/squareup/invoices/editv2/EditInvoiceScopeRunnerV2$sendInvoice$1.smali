.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->sendInvoice()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0018\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0004H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "+",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;>;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/editv2/service/SendInvoiceResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    .line 697
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getInvoicePreparer$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/editv2/service/InvoicePreparer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/invoices/editv2/service/InvoicePreparer;->prepareForSendOrSave(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 698
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1, p1, v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$sendInvoiceSingle(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/util/Optional;Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$sendInvoice$1;->apply(Lkotlin/Pair;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
