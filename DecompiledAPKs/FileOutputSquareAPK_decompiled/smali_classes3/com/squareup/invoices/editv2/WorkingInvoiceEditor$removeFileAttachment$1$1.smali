.class final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;->invoke(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1$1;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1$1;->invoke(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Z
    .locals 1

    .line 255
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1$1;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;

    iget-object v0, v0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$removeFileAttachment$1;->$attachmentToken:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
