.class public final Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;
.super Ljava/lang/Object;
.source "EditInvoiceV2BottomButtonTextFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ\u0010\u0010\u000f\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000eH\u0002J \u0010\u0010\u001a\u00020\u00082\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000eH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)V",
        "create",
        "",
        "workingInvoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "isFirstScreen",
        "",
        "textForEditing",
        "textForNewOrDraft",
        "paymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "schedulingInvoice",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method private final textForEditing(Z)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    .line 64
    iget-object p1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->edit_invoice_continue:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 66
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_edit_update_invoice:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final textForNewOrDraft(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Z)Ljava/lang/String;
    .locals 0

    if-eqz p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->edit_invoice_continue:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 48
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->res:Lcom/squareup/util/Res;

    if-eqz p3, :cond_1

    .line 50
    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_edit_schedule_invoice:I

    goto :goto_0

    .line 52
    :cond_1
    sget-object p3, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p2

    aget p2, p3, p2

    const/4 p3, 0x1

    if-eq p2, p3, :cond_3

    const/4 p3, 0x2

    if-eq p2, p3, :cond_2

    .line 55
    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_edit_send_invoice:I

    goto :goto_0

    .line 54
    :cond_2
    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_edit_create_invoice:I

    goto :goto_0

    .line 53
    :cond_3
    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_edit_charge_invoice:I

    .line 48
    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "workingInvoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    .line 36
    :cond_0
    invoke-direct {p0, p3}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->textForEditing(Z)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 32
    :cond_1
    :goto_0
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p2

    .line 33
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 30
    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->textForNewOrDraft(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Z)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method
