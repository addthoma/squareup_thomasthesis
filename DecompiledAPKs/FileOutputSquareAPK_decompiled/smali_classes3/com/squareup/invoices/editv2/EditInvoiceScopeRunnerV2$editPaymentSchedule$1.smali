.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->editPaymentSchedule()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        ">;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u000122\u0010\u0002\u001a.\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0005 \u0007*\u0016\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;>;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 1512
    iget-object v1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceWorkflowRunner$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v1

    .line 1514
    iget-object v2, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v2}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getEditInvoiceContext$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    .line 1515
    iget-object v3, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$editPaymentSchedule$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {v3}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getCurrentTime$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/time/CurrentTime;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 1513
    invoke-static {v0, v2, v3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;->createInputForEditPaymentSchedule(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    move-result-object p1

    .line 1512
    invoke-interface {v1, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startAddPaymentSchedule(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    return-void
.end method
