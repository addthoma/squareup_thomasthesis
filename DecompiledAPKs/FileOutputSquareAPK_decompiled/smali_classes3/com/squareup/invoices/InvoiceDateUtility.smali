.class public Lcom/squareup/invoices/InvoiceDateUtility;
.super Ljava/lang/Object;
.source "InvoiceDateUtility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static beforeOrEqualToday(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/util/Clock;)Z
    .locals 0

    .line 59
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceDateUtility;->convertISOtoYMD(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    .line 60
    invoke-static {p0, p1}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p0

    return p0
.end method

.method public static beforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z
    .locals 1

    .line 64
    invoke-static {p0, p1}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Lcom/squareup/invoices/InvoiceDateUtility;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static beforeToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    .line 44
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 45
    invoke-static {p1, p0}, Lcom/squareup/util/ProtoDates;->compareYmd(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result p0

    const/4 p1, 0x1

    if-ne p0, p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static convertISOtoYMD(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 53
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 54
    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->requireIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 55
    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static formatDueDateValue(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;Z)Ljava/lang/CharSequence;
    .locals 5

    if-nez p0, :cond_0

    .line 101
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_no_due_date:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 104
    :cond_0
    invoke-static {p1, p0}, Lcom/squareup/util/ProtoDates;->countDaysBetweenAsUTC(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_2

    if-eqz p2, :cond_1

    .line 109
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_upon_receipt:I

    invoke-interface {p3, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 111
    :cond_1
    invoke-static {p1, p5}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    const-string p1, "days"

    const-wide/16 v2, 0x1

    if-gez v4, :cond_4

    .line 113
    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    cmp-long p2, v0, v2

    if-nez p2, :cond_3

    .line 115
    sget p1, Lcom/squareup/common/invoices/R$string;->time_day_ago:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 117
    :cond_3
    sget p2, Lcom/squareup/common/invoices/R$string;->time_days_ago:I

    invoke-interface {p3, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 118
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-virtual {p2, p1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_4
    cmp-long p2, v0, v2

    if-nez p2, :cond_5

    .line 122
    sget p1, Lcom/squareup/utilities/R$string;->time_day:I

    invoke-interface {p3, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 124
    :cond_5
    sget p2, Lcom/squareup/utilities/R$string;->time_days:I

    invoke-interface {p3, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 125
    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-virtual {p2, p1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 125
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 129
    :goto_0
    new-instance p2, Landroid/text/SpannableStringBuilder;

    invoke-direct {p2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 130
    invoke-virtual {p2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    if-eqz p0, :cond_6

    if-eqz p6, :cond_6

    .line 133
    new-instance p1, Landroid/text/SpannableString;

    new-instance p6, Ljava/lang/StringBuilder;

    invoke-direct {p6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, " ("

    invoke-virtual {p6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p5, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {p6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 135
    sget-object p0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 136
    invoke-static {p4, p0}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object p0

    invoke-static {p1, p0}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p0

    .line 137
    new-instance p1, Landroid/text/style/ForegroundColorSpan;

    sget p4, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 138
    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p3

    invoke-direct {p1, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 137
    invoke-static {p0, p1}, Lcom/squareup/text/Spannables;->span(Landroid/text/Spannable;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p0

    .line 139
    invoke-virtual {p2, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_6
    return-object p2
.end method

.method public static formatDueDateValueWithDate(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;)Ljava/lang/CharSequence;
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 91
    invoke-static/range {v0 .. v6}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValue(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;Z)Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static getISODateString(Lcom/squareup/protos/client/ISO8601Date;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    .line 70
    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 72
    invoke-virtual {p1, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 0

    .line 29
    invoke-interface {p0}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->calendarToYmd(Ljava/util/Calendar;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method

.method public static getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;
    .locals 0

    if-eqz p0, :cond_0

    .line 81
    invoke-static {p0}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 83
    invoke-virtual {p1, p0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    return p0

    .line 36
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 37
    invoke-virtual {p1, p0}, Lcom/squareup/protos/common/time/YearMonthDay;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method
