.class public final Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;
.super Ljava/lang/Object;
.source "ClientInvoiceServiceHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/ClientInvoiceService;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/ClientInvoiceService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/invoices/ClientInvoiceService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;)",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/invoices/ClientInvoiceService;Lrx/Scheduler;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/receiving/StandardReceiver;)Lcom/squareup/invoices/ClientInvoiceServiceHelper;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;-><init>(Lcom/squareup/server/invoices/ClientInvoiceService;Lrx/Scheduler;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/receiving/StandardReceiver;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ClientInvoiceServiceHelper;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/invoices/ClientInvoiceService;

    iget-object v1, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lrx/Scheduler;

    iget-object v2, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v3, p0, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/receiving/StandardReceiver;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->newInstance(Lcom/squareup/server/invoices/ClientInvoiceService;Lrx/Scheduler;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/receiving/StandardReceiver;)Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper_Factory;->get()Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    move-result-object v0

    return-object v0
.end method
