.class final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
        "+",
        "Lcom/squareup/workflow/legacyintegration/LegacyState<",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;+",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        ">;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012<\u0010\u0004\u001a8\u0012(\u0012&\u0012\u0004\u0012\u00020\u0007\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\t\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\u00080\u0006\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u000c0\u0005H\n\u00a2\u0006\u0002\u0008\r"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "it",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/legacy/WorkflowUpdate;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "+",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    instance-of v0, p1, Lcom/squareup/workflow/legacy/Running;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    check-cast p1, Lcom/squareup/workflow/legacy/Running;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Running;->getHandle()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    .line 225
    :cond_0
    instance-of v0, p1, Lcom/squareup/workflow/legacy/Finished;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/workflow/legacy/FinishWith;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;

    check-cast p1, Lcom/squareup/workflow/legacy/Finished;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Finished;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecipientsResult;-><init>(Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowUpdate;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;->invoke(Lcom/squareup/workflow/legacy/WorkflowUpdate;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
