.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "PaymentPlanSection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentPlanSection.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentPlanSection.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n+ 4 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,194:1\n1642#2,2:195\n1163#3:197\n1103#3,7:201\n1115#4,3:198\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentPlanSection.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection\n*L\n74#1,2:195\n101#1:197\n139#1,7:201\n103#1,3:198\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 72\u00020\u0001:\u000267B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J.\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 J\u0018\u0010!\u001a\u00020\"2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010#\u001a\u00020\u00182\u0006\u0010$\u001a\u00020%H\u0002J\u008a\u0001\u0010&\u001a\u00020\u00182\u0006\u0010\'\u001a\u00020(2\u0008\u0008\u0002\u0010)\u001a\u00020%2\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020,0+2\u0006\u0010-\u001a\u00020.2\u0018\u0010/\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u0018002\u0018\u00101\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u0018002\u0012\u00102\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0018032\u0012\u00104\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\u001803J$\u00105\u001a\u00020\u00182\u0006\u0010\'\u001a\u00020(2\u0012\u00104\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\u001803H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u00128F\u00a2\u0006\u0006\u001a\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "addPaymentButton",
        "Lcom/squareup/noho/NohoButton;",
        "fixedAmountButton",
        "Lcom/squareup/widgets/ScalingRadioButton;",
        "helperTextView",
        "Lcom/squareup/widgets/MessageView;",
        "radioGroup",
        "Landroid/widget/RadioGroup;",
        "rowContainer",
        "Landroid/widget/LinearLayout;",
        "rowCount",
        "",
        "getRowCount",
        "()I",
        "sectionHeader",
        "Lcom/squareup/noho/NohoLabel;",
        "initialize",
        "",
        "rowSize",
        "headerText",
        "",
        "currencySymbol",
        "moneyKeyListenerFactory",
        "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
        "unitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "paymentRequestRow",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;",
        "setAddAnotherPaymentButton",
        "addAnotherPaymentButton",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;",
        "setData",
        "isPercentage",
        "",
        "anotherPaymentButton",
        "rowDataList",
        "",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;",
        "helperText",
        "",
        "onMoneyChanged",
        "Lkotlin/Function2;",
        "percentChanged",
        "paymentRequestClicked",
        "Lkotlin/Function1;",
        "amountTypeChanged",
        "setRadioChecked",
        "AddAnotherPaymentButton",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;


# instance fields
.field private final addPaymentButton:Lcom/squareup/noho/NohoButton;

.field private final fixedAmountButton:Lcom/squareup/widgets/ScalingRadioButton;

.field private final helperTextView:Lcom/squareup/widgets/MessageView;

.field private final radioGroup:Landroid/widget/RadioGroup;

.field private final rowContainer:Landroid/widget/LinearLayout;

.field private final sectionHeader:Lcom/squareup/noho/NohoLabel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    sget p2, Lcom/squareup/features/invoices/R$layout;->payment_plan_section_contents:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setOrientation(I)V

    .line 54
    sget p1, Lcom/squareup/features/invoices/R$id;->section_header:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->sectionHeader:Lcom/squareup/noho/NohoLabel;

    .line 55
    sget p1, Lcom/squareup/features/invoices/R$id;->radio_group:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioGroup;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->radioGroup:Landroid/widget/RadioGroup;

    .line 56
    sget p1, Lcom/squareup/features/invoices/R$id;->fixed_amount_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/ScalingRadioButton;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->fixedAmountButton:Lcom/squareup/widgets/ScalingRadioButton;

    .line 57
    sget p1, Lcom/squareup/features/invoices/R$id;->row_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->rowContainer:Landroid/widget/LinearLayout;

    .line 58
    sget p1, Lcom/squareup/features/invoices/R$id;->add_another_payment_button:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    .line 59
    sget p1, Lcom/squareup/features/invoices/R$id;->payment_plan_section_helper:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->helperTextView:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 36
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final paymentRequestRow(Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;
    .locals 2

    .line 163
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;

    .line 164
    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    .line 163
    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;->inflateWithParent(Landroid/view/ViewGroup;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;

    move-result-object p1

    return-object p1
.end method

.method private final setAddAnotherPaymentButton(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;)V
    .locals 2

    .line 130
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Hide;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Hide;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 131
    :cond_0
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Disabled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Disabled;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 133
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    goto :goto_0

    .line 136
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Show;

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 139
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->addPaymentButton:Lcom/squareup/noho/NohoButton;

    check-cast v0, Landroid/view/View;

    .line 201
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setAddAnotherPaymentButton$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setAddAnotherPaymentButton$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public static synthetic setData$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;ZLcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;Ljava/util/List;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V
    .locals 10

    and-int/lit8 v0, p9, 0x2

    if-eqz v0, :cond_0

    .line 85
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Hide;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton$Hide;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;

    move-object v3, v0

    goto :goto_0

    :cond_0
    move-object v3, p2

    :goto_0
    move-object v1, p0

    move v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setData(ZLcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;Ljava/util/List;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final setRadioChecked(ZLkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->radioGroup:Landroid/widget/RadioGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    if-eqz p1, :cond_0

    .line 151
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->radioGroup:Landroid/widget/RadioGroup;

    sget v0, Lcom/squareup/features/invoices/R$id;->percentage_button:I

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 152
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->radioGroup:Landroid/widget/RadioGroup;

    sget v0, Lcom/squareup/features/invoices/R$id;->fixed_amount_button:I

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 154
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->radioGroup:Landroid/widget/RadioGroup;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setRadioChecked$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setRadioChecked$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    return-void
.end method


# virtual methods
.method public final getRowCount()I
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final initialize(ILjava/lang/String;Ljava/lang/String;Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 3

    const-string v0, "headerText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencySymbol"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyKeyListenerFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x1

    if-gt v1, p1, :cond_0

    .line 71
    :goto_0
    invoke-direct {p0, p4, p5}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->paymentRequestRow(Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eq v1, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    :cond_0
    move-object p1, v0

    check-cast p1, Ljava/lang/Iterable;

    .line 195
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;

    .line 75
    iget-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->rowContainer:Landroid/widget/LinearLayout;

    check-cast p4, Landroid/view/View;

    invoke-virtual {p5, p4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 76
    :cond_1
    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowKt;->setBorders(Ljava/util/List;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->sectionHeader:Lcom/squareup/noho/NohoLabel;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->fixedAmountButton:Lcom/squareup/widgets/ScalingRadioButton;

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Lcom/squareup/widgets/ScalingRadioButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setData(ZLcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;Ljava/util/List;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;",
            ">;",
            "Ljava/lang/CharSequence;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p8

    const-string v2, "anotherPaymentButton"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "rowDataList"

    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "helperText"

    invoke-static {v9, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "onMoneyChanged"

    move-object/from16 v11, p5

    invoke-static {v11, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "percentChanged"

    move-object/from16 v12, p6

    invoke-static {v12, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "paymentRequestClicked"

    move-object/from16 v13, p7

    invoke-static {v13, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "amountTypeChanged"

    invoke-static {v10, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    iget-object v2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    move-object v3, v8

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v4

    if-ne v2, v4, :cond_3

    .line 101
    iget-object v2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->rowContainer:Landroid/widget/LinearLayout;

    check-cast v2, Landroid/view/ViewGroup;

    .line 197
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    invoke-static {v4, v3}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v3

    new-instance v5, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$getChildren$1;

    invoke-direct {v5, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-static {v3, v5}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    .line 102
    sget-object v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v3}, Lkotlin/sequences/SequencesKt;->mapNotNull(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    .line 199
    invoke-interface {v2}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object v14

    const/4 v15, 0x0

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    add-int/lit8 v16, v15, 0x1

    if-gez v15, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    move-object v7, v2

    check-cast v7, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;

    .line 104
    invoke-interface {v8, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v6, v2

    check-cast v6, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;

    .line 108
    invoke-virtual {v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->removeTextListeners()V

    .line 111
    invoke-static {v7, v6}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowKt;->setData(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;)V

    .line 114
    new-instance v17, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;

    move-object/from16 v2, v17

    move v3, v15

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v18, v6

    move-object/from16 v6, p6

    move-object v8, v7

    move-object/from16 v7, p7

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;-><init>(ILjava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v2, v17

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v8, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setOnMoneyChangedListener(Lkotlin/jvm/functions/Function1;)V

    .line 115
    new-instance v17, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$2;

    move-object/from16 v2, v17

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$2;-><init>(ILjava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v2, v17

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v8, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setOnPercentageChangedListener(Lkotlin/jvm/functions/Function1;)V

    .line 116
    invoke-virtual/range {v18 .. v18}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->isEditable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    new-instance v17, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$3;

    move-object/from16 v2, v17

    move v3, v15

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$3;-><init>(ILjava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)V

    move-object/from16 v2, v17

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v8, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setDescriptionRowOnClickListener(Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    .line 119
    :cond_1
    invoke-virtual {v8}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->clearDescriptionRowListener()V

    :goto_1
    move-object/from16 v8, p3

    move/from16 v15, v16

    goto :goto_0

    :cond_2
    move/from16 v2, p1

    .line 123
    invoke-direct {v0, v2, v10}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setRadioChecked(ZLkotlin/jvm/functions/Function1;)V

    .line 124
    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setAddAnotherPaymentButton(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;)V

    .line 125
    iget-object v1, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->helperTextView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v1, v9}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 95
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Section does not contain the correct number of rows. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "View currently has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->rowContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "while you want to display "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 94
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Throwable;

    throw v2
.end method
