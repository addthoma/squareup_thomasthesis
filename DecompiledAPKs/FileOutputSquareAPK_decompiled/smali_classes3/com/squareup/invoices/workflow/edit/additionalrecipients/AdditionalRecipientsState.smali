.class public abstract Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;
.super Ljava/lang/Object;
.source "AdditionalRecipientsState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;,
        Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00052\u00020\u0001:\u0002\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0001\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;",
        "",
        "()V",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Companion",
        "ShowRecipients",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$ShowRecipients;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;->Companion:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;-><init>()V

    return-void
.end method


# virtual methods
.method public final takeSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 16
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
