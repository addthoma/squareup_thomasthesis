.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;
.super Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.source "EditInvoiceState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InvoiceMessage"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0017\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\u001f\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\u0008\u00a2\u0006\u0002\u0010\u000cJ\u001b\u0010\u000f\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\u0008H\u00c6\u0003J%\u0010\u0010\u001a\u00020\u00002\u001a\u0008\u0002\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\u0008H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0003H\u00d6\u0001R#\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "message",
        "",
        "isDefaultMessageEnabled",
        "Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;",
        "(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V",
        "invoiceMessageWorkflow",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V",
        "getInvoiceMessageWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "invoiceMessageWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 117
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isDefaultMessageEnabled"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    sget-object v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;

    .line 123
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->Companion:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;

    invoke-virtual {v1, p1, p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;->startState(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    move-result-object p1

    .line 122
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$Companion;->handle(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 121
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;"
        }
    .end annotation

    const-string v0, "invoiceMessageWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInvoiceMessageWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InvoiceMessage(invoiceMessageWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->invoiceMessageWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
