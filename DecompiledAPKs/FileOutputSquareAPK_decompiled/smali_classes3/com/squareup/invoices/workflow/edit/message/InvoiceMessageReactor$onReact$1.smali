.class final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceMessageReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->onReact(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceMessageReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceMessageReactor.kt\ncom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,148:1\n85#2,2:149\n85#2,2:151\n85#2,2:153\n85#2,2:155\n125#2,3:157\n85#2,2:160\n85#2,2:162\n85#2,2:164\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceMessageReactor.kt\ncom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1\n*L\n71#1,2:149\n74#1,2:151\n84#1,2:153\n92#1,2:155\n97#1,3:157\n117#1,2:160\n122#1,2:162\n127#1,2:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

.field final synthetic $workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowPool;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    .line 70
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    if-eqz v1, :cond_0

    .line 71
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$1;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 149
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 74
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$2;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 151
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 84
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$3;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 153
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 92
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$4;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$4;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 155
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 96
    :cond_0
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    if-eqz v1, :cond_1

    .line 97
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    .line 98
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;->access$updateDefaultMessageWorker(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object v4

    sget-object v5, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;->getMessage()Ljava/lang/String;

    move-result-object v6

    .line 99
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$5;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 157
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onWorkerResult$1;

    const/4 v7, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onWorkerResult$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuspending(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 117
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$6;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$6;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 160
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 121
    :cond_1
    instance-of v0, v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    if-eqz v0, :cond_2

    .line 122
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$7;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$7;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 162
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 127
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$8;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$8;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 164
    sget-object v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$$special$$inlined$onEvent$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    :cond_2
    :goto_0
    return-void
.end method
