.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;
.super Ljava/lang/Object;
.source "EditInvoiceScreensViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final editAutomaticPaymentsCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final editReminderCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final endsDateFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final endsFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final frequencyCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final remindersListCoordinatorFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final repeatEveryFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->frequencyCoordinatorFactoryProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->endsFactoryProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->endsDateFactoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->repeatEveryFactoryProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->remindersListCoordinatorFactoryProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->editReminderCoordinatorFactoryProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->editAutomaticPaymentsCoordinatorFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;"
        }
    .end annotation

    .line 67
    new-instance v8, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;
    .locals 9

    .line 78
    new-instance v8, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;
    .locals 8

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->frequencyCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->endsFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->endsDateFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->repeatEveryFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->remindersListCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->editReminderCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->editAutomaticPaymentsCoordinatorFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;

    invoke-static/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->newInstance(Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/RemindersListCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsCoordinator$Factory;)Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory_Factory;->get()Lcom/squareup/invoices/workflow/edit/EditInvoiceScreensViewFactory;

    move-result-object v0

    return-object v0
.end method
