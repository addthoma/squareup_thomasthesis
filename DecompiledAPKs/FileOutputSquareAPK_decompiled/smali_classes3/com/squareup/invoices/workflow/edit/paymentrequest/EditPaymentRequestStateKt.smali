.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;
.super Ljava/lang/Object;
.source "EditPaymentRequestState.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestState.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt\n+ 2 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,259:1\n56#2:260\n56#2:261\n56#2:262\n61#2:263\n56#2:264\n56#2:265\n56#2:266\n56#2:267\n56#2:268\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestState.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt\n*L\n180#1:260\n181#1:261\n204#1:262\n205#1:263\n211#1:264\n212#1:265\n213#1:266\n228#1:267\n243#1:268\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u000c\u0010\u0003\u001a\u00020\u0004*\u00020\u0002H\u0002\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0002H\u0002\u001a\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\u0002H\u0002\u001a\u000c\u0010\t\u001a\u00020\n*\u00020\u0002H\u0002\u001a\u0014\u0010\u000b\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0001H\u0002\u001a\u0014\u0010\u000f\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0004H\u0002\u001a\u0014\u0010\u0010\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0006H\u0002\u001a\u0014\u0010\u0011\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0008H\u0002\u001a\u0014\u0010\u0012\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000e\u001a\u00020\nH\u0002\u00a8\u0006\u0013"
    }
    d2 = {
        "readEditPaymentRequestInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "Lokio/BufferedSource;",
        "readOverlappingDateValidation",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;",
        "readPaymentRequestType",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "readRemovalInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;",
        "readValidationErrorInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;",
        "writeEditPaymentRequestInfo",
        "",
        "Lokio/BufferedSink;",
        "data",
        "writeOverlappingDateValidation",
        "writePaymentRequestType",
        "writeRemovalInfo",
        "writeValidationErrorInfo",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$readValidationErrorInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->readValidationErrorInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$writeEditPaymentRequestInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->writeEditPaymentRequestInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V

    return-void
.end method

.method public static final synthetic access$writeValidationErrorInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->writeValidationErrorInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;)V

    return-void
.end method

.method private static final readEditPaymentRequestInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;
    .locals 4

    .line 179
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->readPaymentRequestType(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object v0

    .line 260
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 261
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 178
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-direct {v2, v0, v1, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-object v2
.end method

.method private static final readOverlappingDateValidation(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;
    .locals 3

    .line 242
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 243
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 243
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;-><init>(Lcom/squareup/protos/common/time/YearMonthDay;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    goto :goto_0

    .line 244
    :cond_0
    const-class p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$None;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$None;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$None;

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    :goto_0
    return-object v0

    .line 245
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final readPaymentRequestType(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
    .locals 4

    .line 202
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 262
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 263
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 206
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->readRemovalInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    move-result-object v2

    .line 207
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->readOverlappingDateValidation(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    move-result-object p0

    .line 203
    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-direct {v3, v0, v1, v2, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    goto :goto_1

    .line 209
    :cond_1
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 265
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 266
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/time/YearMonthDay;

    .line 210
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-direct {v2, v0, v1, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    move-object v3, v2

    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    :goto_1
    return-object v3

    .line 215
    :cond_2
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final readRemovalInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;
    .locals 3

    .line 227
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 228
    const-class v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/Money;

    .line 228
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    goto :goto_0

    .line 229
    :cond_0
    const-class p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$NonRemovable;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    sget-object p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$NonRemovable;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$NonRemovable;

    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    :goto_0
    return-object v0

    .line 230
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method private static final readValidationErrorInfo(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;
    .locals 2

    .line 255
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;

    .line 256
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p0

    .line 255
    invoke-direct {v0, v1, p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static final writeEditPaymentRequestInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)V
    .locals 1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getPaymentRequestType()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->writePaymentRequestType(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;)V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 174
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;->getInvoiceFirstSentDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    return-void
.end method

.method private static final writeOverlappingDateValidation(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V
    .locals 2

    .line 235
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "data::class.java.simpleName"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 237
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation$Validate;->getEndDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method

.method private static final writePaymentRequestType(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;)V
    .locals 2

    .line 186
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "data::class.java.simpleName"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 187
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 189
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz v0, :cond_0

    .line 190
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getCompletedAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 191
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getRemovalInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->writeRemovalInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;)V

    .line 192
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getOverlappingDateValidation()Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestStateKt;->writeOverlappingDateValidation(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V

    goto :goto_0

    .line 194
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    if-eqz v0, :cond_1

    .line 195
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getBalanceAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;->getDepositDueDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_1
    :goto_0
    return-void
.end method

.method private static final writeRemovalInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;)V
    .locals 2

    .line 220
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "data::class.java.simpleName"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 222
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo$Removable;->getInitialRequestedAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method

.method private static final writeValidationErrorInfo(Lokio/BufferedSink;Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;)V
    .locals 1

    .line 250
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 251
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/ValidationErrorInfo;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    return-void
.end method
