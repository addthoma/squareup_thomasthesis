.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$1;
.super Ljava/lang/Object;
.source "RecurringEndsDateCoordinator.kt"

# interfaces
.implements Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator;->update(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$1",
        "Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;",
        "onDateSelected",
        "",
        "date",
        "Ljava/util/Date;",
        "onDateUnselected",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSelected(Ljava/util/Date;)V
    .locals 2

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateCoordinator$update$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$DateSelected;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$DateSelected;-><init>(Ljava/util/Date;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public onDateUnselected(Ljava/util/Date;)V
    .locals 1

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
