.class public final Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;
.super Ljava/lang/Object;
.source "EditAutomaticPaymentsState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\u00032\u0008\u0010\n\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000b\u001a\u00020\u000cH\u00d6\u0001J\u0006\u0010\r\u001a\u00020\u000eJ\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;",
        "",
        "allowAutoPayments",
        "",
        "(Z)V",
        "getAllowAutoPayments",
        "()Z",
        "component1",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toString",
        "",
        "Companion",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;


# instance fields
.field private final allowAutoPayments:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->Companion:Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$Companion;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-boolean p1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->copy(Z)Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    return v0
.end method

.method public final copy(Z)Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;-><init>(Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllowAutoPayments()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final takeSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 10
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditAutomaticPaymentsState(allowAutoPayments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsState;->allowAutoPayments:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
