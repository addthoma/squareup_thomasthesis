.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;
.super Ljava/lang/Object;
.source "EditInvoiceReactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/Reactor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceReactor.kt\ncom/squareup/invoices/workflow/edit/EditInvoiceReactor\n+ 2 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,291:1\n412#2:292\n412#2:293\n412#2:294\n412#2:295\n412#2:296\n357#2:297\n402#2:298\n357#2:299\n402#2:300\n357#2:301\n402#2:302\n357#2:303\n402#2:304\n357#2:305\n402#2:306\n357#2:307\n402#2:308\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceReactor.kt\ncom/squareup/invoices/workflow/edit/EditInvoiceReactor\n*L\n254#1:292\n255#1:293\n256#1:294\n257#1:295\n258#1:296\n259#1:297\n259#1:298\n260#1:299\n260#1:300\n261#1:301\n261#1:302\n262#1:303\n262#1:304\n263#1:305\n263#1:306\n264#1:307\n264#1:308\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00d6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001B_\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u00a2\u0006\u0002\u0010\u001bJ*\u00106\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004072\u0006\u00108\u001a\u00020\u00022\u0006\u00109\u001a\u00020:H\u0016J:\u0010;\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040=0<2\u0006\u0010>\u001a\u00020\u00022\u000c\u0010?\u001a\u0008\u0012\u0004\u0012\u00020\u00030@2\u0006\u00109\u001a\u00020:H\u0016R|\u0010\u001c\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u001f\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 0\u001e\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020$0\u001dj6\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020$\u0012&\u0012$\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 j\u0008\u0012\u0004\u0012\u00020!`&`%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R|\u0010\'\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020(\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 0\u001e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020)0\u001dj6\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020)\u0012&\u0012$\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 j\u0008\u0012\u0004\u0012\u00020!`&`%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R|\u0010*\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020+\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 0\u001e\u0012\u0004\u0012\u00020+\u0012\u0004\u0012\u00020,0\u001dj6\u0012\u0004\u0012\u00020+\u0012\u0004\u0012\u00020,\u0012&\u0012$\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 j\u0008\u0012\u0004\u0012\u00020!`&`%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R|\u0010-\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020.\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 0\u001e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/0\u001dj6\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/\u0012&\u0012$\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 j\u0008\u0012\u0004\u0012\u00020!`&`%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R|\u00100\u001ap\u0012(\u0012&\u0012\u0004\u0012\u000201\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 0\u001e\u0012\u0004\u0012\u000201\u0012\u0004\u0012\u0002020\u001dj6\u0012\u0004\u0012\u000201\u0012\u0004\u0012\u000202\u0012&\u0012$\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 j\u0008\u0012\u0004\u0012\u00020!`&`%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R|\u00103\u001ap\u0012(\u0012&\u0012\u0004\u0012\u000204\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 0\u001e\u0012\u0004\u0012\u000204\u0012\u0004\u0012\u0002050\u001dj6\u0012\u0004\u0012\u000204\u0012\u0004\u0012\u000205\u0012&\u0012$\u0012\u0004\u0012\u00020!\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"j\u0002`#0 j\u0008\u0012\u0004\u0012\u00020!`&`%X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006A"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;",
        "Lcom/squareup/workflow/legacy/rx2/Reactor;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "deliveryMethodReactor",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;",
        "editRecurringReactor",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;",
        "chooseDateReactor",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
        "invoiceMessageReactor",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;",
        "invoiceAttachmentReactor",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;",
        "editInvoiceDetailsWorkflow",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;",
        "automaticRemindersWorkflow",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
        "additionalRecipientsWorkflow",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;",
        "editAutomaticPaymentsWorkflow",
        "Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;",
        "editPaymentRequestWorkflow",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;",
        "editPaymentScheduleWorflow",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;",
        "(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)V",
        "additionalRecipientsLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "editAutomaticPaymentsLauncher",
        "",
        "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
        "editPaymentRequestLauncher",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        "editPaymentScheduleWorflowLauncher",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        "invoiceDetailsLauncher",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
        "remindersLauncher",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "onReact",
        "Lio/reactivex/Single;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final additionalRecipientsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;",
            "Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final chooseDateReactor:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

.field private final deliveryMethodReactor:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

.field private final editAutomaticPaymentsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Ljava/lang/Boolean;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentRequestLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
            ">;"
        }
    .end annotation
.end field

.field private final editPaymentScheduleWorflowLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
            "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
            ">;"
        }
    .end annotation
.end field

.field private final editRecurringReactor:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;

.field private final invoiceAttachmentReactor:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

.field private final invoiceDetailsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceMessageReactor:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;

.field private final remindersLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "deliveryMethodReactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editRecurringReactor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseDateReactor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceMessageReactor"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAttachmentReactor"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceDetailsWorkflow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "automaticRemindersWorkflow"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalRecipientsWorkflow"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editAutomaticPaymentsWorkflow"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestWorkflow"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentScheduleWorflow"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->deliveryMethodReactor:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editRecurringReactor:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->chooseDateReactor:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->invoiceMessageReactor:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->invoiceAttachmentReactor:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    .line 93
    invoke-virtual {p6}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->invoiceDetailsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 94
    invoke-virtual {p7}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->remindersLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 95
    invoke-virtual {p8}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/RealAdditionalRecipientsWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->additionalRecipientsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 96
    invoke-virtual {p9}, Lcom/squareup/invoices/workflow/edit/automaticpayments/EditAutomaticPaymentsWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editAutomaticPaymentsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 97
    invoke-virtual {p10}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editPaymentRequestLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 98
    invoke-virtual {p11}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editPaymentScheduleWorflowLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    return-void
.end method


# virtual methods
.method public launch(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
            ">;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->deliveryMethodReactor:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 292
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 254
    invoke-virtual {p2, v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editRecurringReactor:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 293
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 255
    invoke-virtual {p2, v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->chooseDateReactor:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 294
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 256
    invoke-virtual {p2, v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->invoiceMessageReactor:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 295
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 257
    invoke-virtual {p2, v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->invoiceAttachmentReactor:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentReactor;

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 296
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/features/invoices/shared/edit/workflow/fileattachments/InvoiceAttachmentState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 258
    invoke-virtual {p2, v1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->invoiceDetailsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 298
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 297
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->remindersLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 300
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 299
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->additionalRecipientsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 302
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/features/invoices/shared/edit/workflow/additionalrecipients/AdditionalRecipientsInput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/AdditionalRecipientsResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 301
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editAutomaticPaymentsLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 304
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Ljava/lang/Boolean;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/EditAutomaticPaymentsResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 303
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editPaymentRequestLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 306
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 305
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->editPaymentScheduleWorflowLauncher:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    .line 308
    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v2, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-class v4, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 307
    invoke-virtual {p2, v0, v1}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    const/4 v8, 0x0

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v5, p0

    move-object v6, p1

    move-object v7, p2

    .line 265
    invoke-static/range {v5 .. v10}, Lcom/squareup/workflow/legacy/rx2/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/rx2/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    sget-object p2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->launch(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
            ">;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "events"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;

    invoke-direct {v0, p1, p3}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowPool;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->onReact(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
