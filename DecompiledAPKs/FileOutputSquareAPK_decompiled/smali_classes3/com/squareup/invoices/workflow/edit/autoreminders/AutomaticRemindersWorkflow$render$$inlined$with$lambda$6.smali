.class final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;
.super Lkotlin/jvm/internal/Lambda;
.source "AutomaticRemindersWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->render(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$1$6"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props$inlined:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

.field final synthetic $sink$inlined:Lcom/squareup/workflow/Sink;

.field final synthetic $this_with:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$this_with:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$props$inlined:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$sink$inlined:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 6

    .line 376
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$sink$inlined:Lcom/squareup/workflow/Sink;

    .line 377
    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;

    .line 379
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$this_with:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    .line 380
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$this_with:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    .line 381
    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$6;->$this_with:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v4

    const/4 v5, 0x0

    .line 377
    invoke-direct {v1, v5, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    .line 376
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
