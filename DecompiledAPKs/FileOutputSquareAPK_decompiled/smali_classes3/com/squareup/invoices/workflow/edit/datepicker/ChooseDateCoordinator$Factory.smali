.class public final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;
.super Ljava/lang/Object;
.source "ChooseDateCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0005\u001a\u00020\u00062\u001c\u0010\u0007\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u0002`\u000c0\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;",
        "",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "(Ljava/text/DateFormat;)V",
        "build",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateScreen;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;


# direct methods
.method public constructor <init>(Ljava/text/DateFormat;)V
    .locals 1
    .param p1    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/LongForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dateFormat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;>;)",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;->dateFormat:Ljava/text/DateFormat;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;-><init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
