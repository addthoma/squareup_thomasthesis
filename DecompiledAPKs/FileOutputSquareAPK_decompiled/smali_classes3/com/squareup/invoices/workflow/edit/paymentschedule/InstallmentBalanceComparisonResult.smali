.class abstract Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;
.super Ljava/lang/Object;
.source "PaymentScheduleValidator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterPercentage;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessPercentage;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00082\u0018\u00002\u00020\u0001:\u0005\u0003\u0004\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\u0008\t\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;",
        "",
        "()V",
        "Equal",
        "InstallmentGreaterMoney",
        "InstallmentGreaterPercentage",
        "InstallmentLessMoney",
        "InstallmentLessPercentage",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$Equal;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterMoney;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessMoney;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentGreaterPercentage;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult$InstallmentLessPercentage;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 226
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/InstallmentBalanceComparisonResult;-><init>()V

    return-void
.end method
