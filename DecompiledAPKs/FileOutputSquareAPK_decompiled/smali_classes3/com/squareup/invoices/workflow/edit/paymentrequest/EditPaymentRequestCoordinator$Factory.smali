.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;
.super Ljava/lang/Object;
.source "EditPaymentRequestCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0001\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ*\u0010\u0010\u001a\u00020\u00112\"\u0010\u0012\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00160\u0014j\u0008\u0012\u0004\u0012\u00020\u0015`\u00170\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "moneyFormatter",
        "Lcom/squareup/protos/common/Money;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)V",
        "build",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;",
        "screen",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceLocaleHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final build(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;

    .line 91
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory$build$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory$build$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    const-string p1, "screen.map { it.unwrapV2Screen }"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->dateFormat:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 92
    iget-object v6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v7, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 93
    iget-object v8, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator$Factory;->res:Lcom/squareup/util/Res;

    const/4 v9, 0x0

    move-object v1, v0

    .line 90
    invoke-direct/range {v1 .. v9}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/protos/common/CurrencyCode;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
