.class final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceMessageReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$1;->this$0:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;->getMessage()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v1, p1, v2, v3, v2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;->copy$default(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageReactor$onReact$1$1;->invoke(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent$UpdateMessage;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
