.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;
.super Lcom/squareup/noho/NohoConstraintLayout;
.source "PaymentRequestRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;,
        Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestRow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,299:1\n1103#2,7:300\n17#3,2:307\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentRequestRow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow\n*L\n193#1,7:300\n216#1,2:307\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u0000 L2\u00020\u0001:\u0002LMB\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010/\u001a\u000200J\u000e\u00101\u001a\u0002002\u0006\u00102\u001a\u000203J\u0015\u00104\u001a\u0002002\u0006\u00105\u001a\u000203H\u0000\u00a2\u0006\u0002\u00086J\u0006\u00107\u001a\u000200J\u0014\u00108\u001a\u0002002\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u0002000:J\u001d\u0010;\u001a\u0002002\u0006\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?H\u0000\u00a2\u0006\u0002\u0008@J\u001a\u0010A\u001a\u0002002\u0012\u0010B\u001a\u000e\u0012\u0004\u0012\u00020D\u0012\u0004\u0012\u0002000CJ\u001a\u0010E\u001a\u0002002\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u00020D\u0012\u0004\u0012\u0002000CJ\u0014\u0010G\u001a\u000200*\u00020H2\u0006\u0010I\u001a\u00020JH\u0002J\u0014\u0010K\u001a\u000200*\u00020\u00172\u0006\u0010\t\u001a\u00020\nH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR$\u0010\u0010\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0011\u0010\r\"\u0004\u0008\u0012\u0010\u000fR$\u0010\u0013\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0014\u0010\r\"\u0004\u0008\u0015\u0010\u000fR\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0019\u001a\u00020\u00182\u0006\u0010\t\u001a\u00020\u00188F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001dR\u0014\u0010\u001e\u001a\u00020\u001f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!R\u0010\u0010\"\u001a\u0004\u0018\u00010#X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010$\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008%\u0010\r\"\u0004\u0008&\u0010\u000fR\u000e\u0010\'\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010*\u001a\u0004\u0018\u00010#X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010+\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008,\u0010\r\"\u0004\u0008-\u0010\u000fR\u000e\u0010.\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;",
        "Lcom/squareup/noho/NohoConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "descriptionRow",
        "Lcom/squareup/noho/NohoRow;",
        "value",
        "",
        "descriptionRowSubtitle",
        "getDescriptionRowSubtitle",
        "()Ljava/lang/CharSequence;",
        "setDescriptionRowSubtitle",
        "(Ljava/lang/CharSequence;)V",
        "descriptionRowTitle",
        "getDescriptionRowTitle",
        "setDescriptionRowTitle",
        "disabledText",
        "getDisabledText",
        "setDisabledText",
        "disabledTextBox",
        "Lcom/squareup/noho/NohoEditText;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;",
        "inputType",
        "getInputType",
        "()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;",
        "setInputType",
        "(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;)V",
        "linkEnabledColorSpan",
        "Landroid/text/style/ForegroundColorSpan;",
        "getLinkEnabledColorSpan",
        "()Landroid/text/style/ForegroundColorSpan;",
        "moneyListener",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "moneyText",
        "getMoneyText",
        "setMoneyText",
        "moneyTextBox",
        "numberBox",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "percentageListener",
        "percentageText",
        "getPercentageText",
        "setPercentageText",
        "percentageTextBox",
        "clearDescriptionRowListener",
        "",
        "displayEdit",
        "display",
        "",
        "drawBorders",
        "isLastRowInGroup",
        "drawBorders$invoices_hairball_release",
        "removeTextListeners",
        "setDescriptionRowOnClickListener",
        "onClick",
        "Lkotlin/Function0;",
        "setMoneyKeyListener",
        "moneyKeyListenerFactory",
        "Lcom/squareup/money/MoneyDigitsKeyListenerFactory;",
        "unitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "setMoneyKeyListener$invoices_hairball_release",
        "setOnMoneyChangedListener",
        "onMoneyChanged",
        "Lkotlin/Function1;",
        "",
        "setOnPercentageChangedListener",
        "onPercentChanged",
        "setBorders",
        "Landroid/view/View;",
        "borders",
        "",
        "updateText",
        "Companion",
        "InputType",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ALLOWED_PERCENTAGE_CHARS:Ljava/lang/String; = "0123456789%\u00a0"

.field public static final Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;


# instance fields
.field private final descriptionRow:Lcom/squareup/noho/NohoRow;

.field private final disabledTextBox:Lcom/squareup/noho/NohoEditText;

.field private moneyListener:Lcom/squareup/text/EmptyTextWatcher;

.field private final moneyTextBox:Lcom/squareup/noho/NohoEditText;

.field private final numberBox:Lcom/squareup/widgets/SquareViewAnimator;

.field private percentageListener:Lcom/squareup/text/EmptyTextWatcher;

.field private final percentageTextBox:Lcom/squareup/noho/NohoEditText;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->Companion:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 61
    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/NohoConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 134
    sget p2, Lcom/squareup/features/invoices/R$layout;->payment_request_row_contents:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 136
    sget p1, Lcom/squareup/features/invoices/R$id;->number_box:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->numberBox:Lcom/squareup/widgets/SquareViewAnimator;

    .line 137
    sget p1, Lcom/squareup/features/invoices/R$id;->description_row:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    .line 138
    sget p1, Lcom/squareup/features/invoices/R$id;->fixed_text_box:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    const/4 p2, 0x0

    .line 139
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    .line 138
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    .line 141
    sget p1, Lcom/squareup/features/invoices/R$id;->percentage_text_box:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    .line 142
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    const-string v0, "0123456789%\u00a0"

    .line 143
    invoke-static {v0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    check-cast v0, Landroid/text/method/KeyListener;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 141
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    .line 147
    sget p1, Lcom/squareup/features/invoices/R$id;->disabled_text_box:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    .line 148
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    .line 149
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setInputType(I)V

    .line 150
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setEnabled(Z)V

    .line 147
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->disabledTextBox:Lcom/squareup/noho/NohoEditText;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 60
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private final getLinkEnabledColorSpan()Landroid/text/style/ForegroundColorSpan;
    .locals 3

    .line 129
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 130
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$color;->noho_text_button_link_enabled:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 129
    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    return-object v0
.end method

.method private final setBorders(Landroid/view/View;I)V
    .locals 0

    .line 231
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;

    .line 232
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoConstraintLayout$DividerConstraintLayoutParams;->setEdges(I)V

    return-void

    .line 231
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.noho.NohoConstraintLayout.DividerConstraintLayoutParams"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final updateText(Lcom/squareup/noho/NohoEditText;Ljava/lang/CharSequence;)V
    .locals 3

    .line 241
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2, p2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    :goto_0
    return-void
.end method


# virtual methods
.method public final clearDescriptionRowListener()V
    .locals 2

    .line 189
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final displayEdit(Z)V
    .locals 7

    .line 216
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    .line 217
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setEnabled(Z)V

    if-eqz p1, :cond_0

    .line 219
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/common/strings/R$string;->edit:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "resources.getString(com.\u2026on.strings.R.string.edit)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/style/CharacterStyle;

    .line 220
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->getLinkEnabledColorSpan()Landroid/text/style/ForegroundColorSpan;

    move-result-object v2

    check-cast v2, Landroid/text/style/CharacterStyle;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "context"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v5, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 308
    new-instance v6, Lcom/squareup/fonts/FontSpan;

    invoke-static {v5, v3}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result v3

    invoke-direct {v6, v4, v3}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    check-cast v6, Landroid/text/style/CharacterStyle;

    aput-object v6, v1, v2

    .line 220
    invoke-static {p1, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;[Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 218
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final drawBorders$invoices_hairball_release(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 198
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->numberBox:Lcom/squareup/widgets/SquareViewAnimator;

    check-cast p1, Landroid/view/View;

    const/16 v0, 0xf

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setBorders(Landroid/view/View;I)V

    .line 199
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    check-cast p1, Landroid/view/View;

    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setBorders(Landroid/view/View;I)V

    goto :goto_0

    .line 201
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->numberBox:Lcom/squareup/widgets/SquareViewAnimator;

    check-cast p1, Landroid/view/View;

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setBorders(Landroid/view/View;I)V

    .line 202
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    check-cast p1, Landroid/view/View;

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setBorders(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method public final getDescriptionRowSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDescriptionRowTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDisabledText()Ljava/lang/CharSequence;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->disabledTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getInputType()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->numberBox:Lcom/squareup/widgets/SquareViewAnimator;

    invoke-virtual {v0}, Lcom/squareup/widgets/SquareViewAnimator;->getDisplayedChildId()I

    move-result v0

    .line 104
    sget v1, Lcom/squareup/features/invoices/R$id;->percentage_text_box:I

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->PERCENTAGE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    goto :goto_0

    .line 105
    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$id;->fixed_text_box:I

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->MONEY:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    goto :goto_0

    .line 106
    :cond_1
    sget v1, Lcom/squareup/features/invoices/R$id;->disabled_text_box:I

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->DISABLED:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    :goto_0
    return-object v0

    .line 107
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not a child of number box."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getMoneyText()Ljava/lang/CharSequence;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getPercentageText()Ljava/lang/CharSequence;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final removeTextListeners()V
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyListener:Lcom/squareup/text/EmptyTextWatcher;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 156
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 157
    move-object v0, v1

    check-cast v0, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyListener:Lcom/squareup/text/EmptyTextWatcher;

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageListener:Lcom/squareup/text/EmptyTextWatcher;

    if-eqz v0, :cond_1

    .line 161
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 162
    check-cast v1, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageListener:Lcom/squareup/text/EmptyTextWatcher;

    :cond_1
    return-void
.end method

.method public final setDescriptionRowOnClickListener(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onClick"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    check-cast v0, Landroid/view/View;

    .line 300
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$setDescriptionRowOnClickListener$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$setDescriptionRowOnClickListener$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setDescriptionRowSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setDescriptionRowTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->descriptionRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setDisabledText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->DISABLED:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setInputType(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->disabledTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setInputType(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->numberBox:Lcom/squareup/widgets/SquareViewAnimator;

    .line 112
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 115
    sget p1, Lcom/squareup/features/invoices/R$id;->disabled_text_box:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 114
    :cond_1
    sget p1, Lcom/squareup/features/invoices/R$id;->fixed_text_box:I

    goto :goto_0

    .line 113
    :cond_2
    sget p1, Lcom/squareup/features/invoices/R$id;->percentage_text_box:I

    .line 111
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public final setMoneyKeyListener$invoices_hairball_release(Lcom/squareup/money/MoneyDigitsKeyListenerFactory;Lcom/squareup/quantity/PerUnitFormatter;)V
    .locals 2

    const-string v0, "moneyKeyListenerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    const-string v1, ""

    invoke-virtual {p2, v1}, Lcom/squareup/quantity/PerUnitFormatter;->unitSuffix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/money/MoneyDigitsKeyListenerFactory;->getDigitsKeyListener(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object p1

    check-cast p1, Landroid/text/method/KeyListener;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    return-void
.end method

.method public final setMoneyText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->MONEY:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setInputType(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->updateText(Lcom/squareup/noho/NohoEditText;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setOnMoneyChangedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onMoneyChanged"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyListener:Lcom/squareup/text/EmptyTextWatcher;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 168
    :cond_0
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$setOnMoneyChangedListener$2;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$setOnMoneyChangedListener$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyListener:Lcom/squareup/text/EmptyTextWatcher;

    .line 174
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyTextBox:Lcom/squareup/noho/NohoEditText;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->moneyListener:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public final setOnPercentageChangedListener(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onPercentChanged"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageListener:Lcom/squareup/text/EmptyTextWatcher;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 179
    :cond_0
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$setOnPercentageChangedListener$2;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$setOnPercentageChangedListener$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/text/EmptyTextWatcher;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageListener:Lcom/squareup/text/EmptyTextWatcher;

    .line 185
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageListener:Lcom/squareup/text/EmptyTextWatcher;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public final setPercentageText(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->PERCENTAGE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setInputType(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->updateText(Lcom/squareup/noho/NohoEditText;Ljava/lang/CharSequence;)V

    .line 77
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowUtilsKt;->lastDigitIndex(Ljava/lang/CharSequence;)I

    move-result p1

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->percentageTextBox:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, p1, p1}, Lcom/squareup/noho/NohoEditText;->setSelection(II)V

    return-void
.end method
