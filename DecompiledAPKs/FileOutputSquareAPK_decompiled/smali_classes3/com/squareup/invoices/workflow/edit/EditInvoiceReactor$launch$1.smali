.class final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->launch(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "kotlin.jvm.PlatformType",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto/16 :goto_0

    .line 268
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;->getEditInvoiceDetailsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 269
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto/16 :goto_0

    .line 271
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;->getAutoRemindersWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 272
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto/16 :goto_0

    .line 274
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->getAdditionalRecipientsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 275
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 277
    :cond_3
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->getEditAutomaticPaymentsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_4

    .line 278
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 280
    :cond_4
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;->getPaymentRequestWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_5

    .line 281
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 283
    :cond_5
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;->getEditPaymentScheduleWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getRendering()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_6

    .line 284
    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    goto :goto_0

    .line 286
    :cond_6
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$launch$1;->invoke(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
