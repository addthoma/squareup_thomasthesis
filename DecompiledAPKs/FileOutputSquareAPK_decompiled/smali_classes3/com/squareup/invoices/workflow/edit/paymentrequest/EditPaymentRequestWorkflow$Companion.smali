.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;
.super Ljava/lang/Object;
.source "EditPaymentRequestWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion\n+ 2 LegacyState.kt\ncom/squareup/workflow/legacyintegration/LegacyStateKt\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 4 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,325:1\n74#2,6:326\n74#2,6:334\n335#3:332\n335#3:340\n412#4:333\n412#4:341\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestWorkflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion\n*L\n278#1,6:326\n283#1,6:334\n278#1:332\n283#1:340\n278#1:333\n283#1:341\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J|\u0010\u0003\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0006\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u00070\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\u0004j6\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b\u0012&\u0012$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\r`\u000c2\u0006\u0010\u000e\u001a\u00020\u0006J|\u0010\u000f\u001ap\u0012(\u0012&\u0012\u0004\u0012\u00020\u0006\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u00070\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\u0004j6\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b\u0012&\u0012$\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\r`\u000c2\u0006\u0010\u0010\u001a\u00020\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;",
        "",
        "()V",
        "legacyHandleFromInput",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
        "Lcom/squareup/workflow/legacyintegration/LegacyHandle;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "input",
        "legacyHandleFromSnapshot",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 274
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final legacyHandleFromInput(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
            ">;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 328
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 329
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 330
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 333
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p1, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 332
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final legacyHandleFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;",
            ">;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    new-instance v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    .line 285
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;->Companion:Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Companion;->getEMPTY_BALANCE()Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Balance;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;

    .line 286
    new-instance v1, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v3, "Money.Builder().build()"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 287
    new-instance v3, Lcom/squareup/protos/common/time/YearMonthDay$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/squareup/protos/common/time/YearMonthDay$Builder;->build()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    const-string v4, "YearMonthDay.Builder().build()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)V

    .line 336
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 337
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 338
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v0

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 341
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-direct {p1, v1, v2, v3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v1, ""

    .line 340
    invoke-virtual {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v1, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v1
.end method
