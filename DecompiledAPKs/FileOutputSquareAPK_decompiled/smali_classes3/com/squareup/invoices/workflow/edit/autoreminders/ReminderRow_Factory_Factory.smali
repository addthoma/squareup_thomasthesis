.class public final Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;
.super Ljava/lang/Object;
.source "ReminderRow_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;-><init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;)Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow_Factory_Factory;->get()Lcom/squareup/invoices/workflow/edit/autoreminders/ReminderRow$Factory;

    move-result-object v0

    return-object v0
.end method
