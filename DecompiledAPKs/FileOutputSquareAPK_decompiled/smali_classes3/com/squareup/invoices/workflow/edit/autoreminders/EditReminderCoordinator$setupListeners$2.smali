.class final Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$2;
.super Ljava/lang/Object;
.source "EditReminderScreen.kt"

# interfaces
.implements Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator;->setupListeners(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "offset",
        "",
        "onQuantityChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $data:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$2;->$data:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onQuantityChanged(I)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderCoordinator$setupListeners$2;->$data:Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen;->getOffsetChanged()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
