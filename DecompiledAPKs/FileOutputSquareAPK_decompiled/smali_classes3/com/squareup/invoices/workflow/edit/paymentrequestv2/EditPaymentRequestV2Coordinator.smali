.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditPaymentRequestV2Coordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestV2Coordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestV2Coordinator.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,113:1\n1103#2,7:114\n1103#2,7:121\n1103#2,7:128\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestV2Coordinator.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator\n*L\n68#1,7:114\n70#1,7:121\n83#1,7:128\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001aB\u0015\u0008\u0002\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0018\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0004H\u0002J\u0018\u0010\u0017\u001a\u00020\u00112\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0016\u001a\u00020\u0004H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "dueRow",
        "Lcom/squareup/noho/NohoRow;",
        "headerSubtitleLabel",
        "Lcom/squareup/noho/NohoLabel;",
        "headerTitleLabel",
        "remindersRow",
        "removePaymentButton",
        "Lcom/squareup/noho/NohoButton;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "screen",
        "updateActionBar",
        "resources",
        "Landroid/content/res/Resources;",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private dueRow:Lcom/squareup/noho/NohoRow;

.field private headerSubtitleLabel:Lcom/squareup/noho/NohoLabel;

.field private headerTitleLabel:Lcom/squareup/noho/NohoLabel;

.field private remindersRow:Lcom/squareup/noho/NohoRow;

.field private removePaymentButton:Lcom/squareup/noho/NohoButton;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;-><init>(Lio/reactivex/Observable;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 104
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 106
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_payment_request_header_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->headerTitleLabel:Lcom/squareup/noho/NohoLabel;

    .line 107
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_payment_request_header_subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLabel;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->headerSubtitleLabel:Lcom/squareup/noho/NohoLabel;

    .line 108
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_payment_request_due_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->dueRow:Lcom/squareup/noho/NohoRow;

    .line 109
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_payment_request_reminder_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->remindersRow:Lcom/squareup/noho/NohoRow;

    .line 110
    sget v0, Lcom/squareup/features/invoices/R$id;->remove_payment_request_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->removePaymentButton:Lcom/squareup/noho/NohoButton;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V
    .locals 3

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v1, "view.resources"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->updateActionBar(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V

    .line 62
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$update$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->headerTitleLabel:Lcom/squareup/noho/NohoLabel;

    if-nez p1, :cond_0

    const-string v0, "headerTitleLabel"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getHeaderTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->headerSubtitleLabel:Lcom/squareup/noho/NohoLabel;

    if-nez p1, :cond_1

    const-string v0, "headerSubtitleLabel"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getHeaderSubtitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->dueRow:Lcom/squareup/noho/NohoRow;

    const-string v0, "dueRow"

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getDueDateString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->dueRow:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/view/View;

    .line 114
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getReminderRow()Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow;

    move-result-object p1

    .line 72
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Hide;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Hide;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "remindersRow"

    if-eqz v0, :cond_5

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->remindersRow:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 73
    :cond_5
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Show;

    if-eqz v0, :cond_9

    .line 74
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->remindersRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->remindersRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    move-object v2, p1

    check-cast v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Show;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow$Show;->getText()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->remindersRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    .line 121
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$$special$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$ReminderRow;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_9
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->removePaymentButton:Lcom/squareup/noho/NohoButton;

    if-nez p1, :cond_a

    const-string v0, "removePaymentButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 84
    :cond_a
    check-cast p1, Landroid/view/View;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getShowRemovePaymentButton()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 85
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getShowRemovePaymentButton()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 128
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$update$$inlined$apply$lambda$1;

    invoke-direct {v0, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$update$$inlined$apply$lambda$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    return-void
.end method

.method private final updateActionBar(Landroid/content/res/Resources;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;)V
    .locals 4

    .line 100
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 95
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 96
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getActionBarTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 97
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getCancelClicked()Lkotlin/jvm/functions/Function0;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$sam$java_lang_Runnable$0;

    invoke-direct {v3, v2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object v2, v3

    :cond_1
    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 98
    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;->getSaveClicked()Lkotlin/jvm/functions/Function0;

    move-result-object p2

    if-eqz p2, :cond_2

    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$sam$java_lang_Runnable$0;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    move-object p2, v1

    :cond_2
    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->bindViews(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;->screens:Lio/reactivex/Observable;

    .line 53
    new-instance v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Coordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
