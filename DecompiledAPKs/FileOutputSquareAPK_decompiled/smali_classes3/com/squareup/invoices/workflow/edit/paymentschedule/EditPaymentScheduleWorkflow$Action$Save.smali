.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;
.super Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;
.source "EditPaymentScheduleWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Save"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0014\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B;\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\u000eJ\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u000cH\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0008H\u00c6\u0003JK\u0010\u001e\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u001f\u001a\u00020\u00082\u0008\u0010 \u001a\u0004\u0018\u00010!H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001R\u0011\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u0013R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0013R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;",
        "paymentRequests",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "validator",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;",
        "isTippingEnabled",
        "",
        "invoiceAmount",
        "Lcom/squareup/protos/common/Money;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "isNew",
        "(Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;Z)V",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "getInvoiceAmount",
        "()Lcom/squareup/protos/common/Money;",
        "()Z",
        "getPaymentRequests",
        "()Ljava/util/List;",
        "getValidator",
        "()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final invoiceAmount:Lcom/squareup/protos/common/Money;

.field private final isNew:Z

.field private final isTippingEnabled:Z

.field private final paymentRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/analytics/Analytics;",
            "Z)V"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 152
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    iput-boolean p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    iput-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    :cond_1
    move-object p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-boolean p6, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    :cond_5
    move v3, p6

    move-object p2, p0

    move-object p3, p1

    move-object p4, p8

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    move p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->copy(Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;Z)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    return v0
.end method

.method public final component4()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component5()Lcom/squareup/analytics/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    return v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;Z)Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/analytics/Analytics;",
            "Z)",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;"
        }
    .end annotation

    const-string v0, "paymentRequests"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "validator"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceAmount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;-><init>(Ljava/util/List;Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;ZLcom/squareup/protos/common/Money;Lcom/squareup/analytics/Analytics;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final getInvoiceAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getPaymentRequests()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;"
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    return-object v0
.end method

.method public final getValidator()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    return v0
.end method

.method public final isNew()Z
    .locals 1

    .line 151
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    return v0
.end method

.method public final isTippingEnabled()Z
    .locals 1

    .line 148
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Save(paymentRequests="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->paymentRequests:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", validator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->validator:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentScheduleValidator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isTippingEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isTippingEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->invoiceAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isNew="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$Save;->isNew:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
