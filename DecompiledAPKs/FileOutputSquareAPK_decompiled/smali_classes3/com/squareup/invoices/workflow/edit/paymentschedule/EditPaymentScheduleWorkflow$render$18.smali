.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$18;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentScheduleWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
        "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$18;->$state:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;",
            "Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 474
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Removed;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Removed;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Removed;->getIndex()I

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestRemoved;-><init>(I)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 475
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Saved;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;

    .line 476
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$18;->$state:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleState$EditingPaymentRequest;->getIndex()I

    move-result v1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Saved;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result$Saved;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    .line 475
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestEdited;-><init>(ILcom/squareup/protos/client/invoice/PaymentRequest;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 478
    :cond_1
    sget-object p1, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestCanceled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$Action$PaymentRequestCanceled;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleWorkflow$render$18;->invoke(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
