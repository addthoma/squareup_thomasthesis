.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;
.super Ljava/lang/Object;
.source "EditPaymentScheduleInputFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentScheduleInputFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentScheduleInputFactory.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory\n+ 2 ProtosPure.kt\ncom/squareup/util/ProtosPure\n*L\n1#1,72:1\n132#2,3:73\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentScheduleInputFactory.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory\n*L\n37#1,3:73\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J8\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0007J0\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0006\u0010\t\u001a\u00020\n2\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;",
        "",
        "()V",
        "createInputForAddPaymentSchedule",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "displayDetails",
        "Lcom/squareup/invoices/DisplayDetails;",
        "today",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "defaultConfigs",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "createInputForEditPaymentSchedule",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final createInputForAddPaymentSchedule(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/invoices/DisplayDetails;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "invoice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "today"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultConfigs"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p0}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    if-eqz p1, :cond_1

    if-eqz p1, :cond_0

    .line 27
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.invoices.DisplayDetails.Invoice"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    const/4 p1, 0x0

    .line 28
    :goto_0
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 26
    invoke-static {p1, v0, p2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    .line 32
    sget-object p1, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 33
    iget-object p2, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p2

    .line 31
    invoke-static {p1, p3, p2, p4}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultDepositPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    .line 36
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/wire/Message;->newBuilder()Lcom/squareup/wire/Message$Builder;

    move-result-object p0

    if-eqz p0, :cond_2

    move-object p2, p0

    check-cast p2, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 38
    iget-object p3, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v5, 0x1e

    add-long/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    iput-object p3, p2, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on:Ljava/lang/Long;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/wire/Message$Builder;->build()Lcom/squareup/wire/Message;

    move-result-object p0

    .line 36
    check-cast p0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 41
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    const/4 v2, 0x1

    const/4 p3, 0x2

    new-array p3, p3, [Lcom/squareup/protos/client/invoice/PaymentRequest;

    const/4 v0, 0x0

    aput-object p1, p3, v0

    const/4 p1, 0x1

    aput-object p0, p3, p1

    .line 45
    invoke-static {p3}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move-object v1, p2

    move-object v6, p4

    .line 41
    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;-><init>(ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;)V

    return-object p2

    .line 74
    :cond_2
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type B"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final createInputForEditPaymentSchedule(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/Invoice;",
            "Lcom/squareup/invoices/DisplayDetails;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "invoice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "today"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultConfigs"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-static {p0}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    if-eqz p1, :cond_1

    if-eqz p1, :cond_0

    .line 59
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.invoices.DisplayDetails.Invoice"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    const/4 p1, 0x0

    .line 60
    :goto_0
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 58
    invoke-static {p1, v0, p2}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    .line 63
    new-instance p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    const/4 v2, 0x0

    .line 67
    iget-object v5, p0, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string p0, "invoice.payment_request"

    invoke-static {v5, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    move-object v6, p3

    .line 63
    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;-><init>(ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;Ljava/util/List;)V

    return-object p1
.end method
