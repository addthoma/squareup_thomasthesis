.class public final Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;
.super Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
.source "EditPaymentRequestInfo.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Deposit"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J3\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;",
        "paymentRequest",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "completedAmount",
        "Lcom/squareup/protos/common/Money;",
        "removalInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;",
        "overlappingDateValidation",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;",
        "(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V",
        "getCompletedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getOverlappingDateValidation",
        "()Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;",
        "getPaymentRequest",
        "()Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "getRemovalInfo",
        "()Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final completedAmount:Lcom/squareup/protos/common/Money;

.field private final overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

.field private final paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

.field private final removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V
    .locals 1

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "removalInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overlappingDateValidation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->copy(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component3()Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    return-object v0
.end method

.method public final component4()Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;
    .locals 1

    const-string v0, "paymentRequest"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "removalInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overlappingDateValidation"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCompletedAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getOverlappingDateValidation()Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    return-object v0
.end method

.method public getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->paymentRequest:Lcom/squareup/protos/client/invoice/PaymentRequest;

    return-object v0
.end method

.method public final getRemovalInfo()Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Deposit(paymentRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", completedAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->completedAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", removalInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->removalInfo:Lcom/squareup/invoices/workflow/edit/paymentrequest/RemovalInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", overlappingDateValidation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/PaymentRequestType$Deposit;->overlappingDateValidation:Lcom/squareup/invoices/workflow/edit/paymentrequest/OverlappingDateValidation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
