.class public abstract Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;
.super Ljava/lang/Object;
.source "AutomaticRemindersWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersSaved;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$AddReminder;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderCancel;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderSaved;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemoveReminder;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffset;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffsetType;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$MessageChanged;,
        Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAutomaticRemindersWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AutomaticRemindersWorkflow.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,573:1\n1360#2:574\n1429#2,3:575\n*E\n*S KotlinDebug\n*F\n+ 1 AutomaticRemindersWorkflow.kt\ncom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action\n*L\n223#1:574\n223#1,3:575\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u000b\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f !\"B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016J\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\tH\u0002J\u0014\u0010\n\u001a\u00020\u0008*\u00020\t2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u000c\u0010\r\u001a\u00020\t*\u00020\u0008H\u0002J\u000c\u0010\u000e\u001a\u00020\t*\u00020\u0008H\u0002J\u000c\u0010\u000f\u001a\u00020\t*\u00020\u0008H\u0002J\u0014\u0010\u0010\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0014\u0010\u0013\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u000cH\u0002J\u0014\u0010\u0015\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\u0016\u001a\u00020\u0017H\u0002\u0082\u0001\u000b#$%&\'()*+,-\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
        "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "editNewReminder",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;",
        "editReminder",
        "index",
        "",
        "remindersListNoChanges",
        "remindersListWithReminderRemoved",
        "remindersListWithReminderSaved",
        "setMessage",
        "message",
        "",
        "setOffset",
        "newOffset",
        "setOffsetType",
        "newOffsetType",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;",
        "AddReminder",
        "EditReminderCancel",
        "EditReminderSaved",
        "ErrorDialogClosed",
        "MessageChanged",
        "ReminderPressed",
        "RemindersSaved",
        "RemindersToggled",
        "RemoveReminder",
        "UpdateReminderOffset",
        "UpdateReminderOffsetType",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersSaved;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$AddReminder;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderCancel;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderSaved;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemoveReminder;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffset;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffsetType;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$MessageChanged;",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 82
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;-><init>()V

    return-void
.end method

.method private final editNewReminder(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 11

    .line 223
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 574
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 575
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 576
    check-cast v2, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    .line 223
    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 577
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 224
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 225
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    add-int/2addr v1, v0

    .line 228
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 229
    instance-of v2, v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    if-eqz v2, :cond_2

    invoke-static {v1}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->newReminderConfigProto(I)Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Config;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    :goto_1
    move-object v2, v0

    goto :goto_2

    .line 230
    :cond_2
    instance-of v0, v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    if-eqz v0, :cond_3

    invoke-static {v1}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->newReminderInstanceProto(I)Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminderKt;->toReminder(Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder$Instance;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    goto :goto_1

    .line 233
    :goto_2
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    const/4 v3, -0x1

    .line 236
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v4

    .line 237
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v5

    .line 238
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v6

    .line 239
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x40

    const/4 v10, 0x0

    move-object v1, v0

    .line 233
    invoke-direct/range {v1 .. v10}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0

    .line 230
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final editReminder(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;I)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 11

    .line 212
    new-instance v10, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 213
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    invoke-static {v0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->buildUponFixedMessage(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v1

    .line 215
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersEnabled()Z

    move-result v3

    .line 216
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v4

    .line 217
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v5

    .line 218
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x40

    const/4 v9, 0x0

    move-object v0, v10

    move v2, p2

    .line 212
    invoke-direct/range {v0 .. v9}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v10
.end method

.method private final remindersListNoChanges(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;
    .locals 4

    .line 244
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    .line 245
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersEnabled()Z

    move-result v1

    .line 246
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    .line 247
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    .line 248
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    .line 244
    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    return-object v0
.end method

.method private final remindersListWithReminderRemoved(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;
    .locals 4

    .line 267
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 268
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getEditingIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->removeAt(I)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 271
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 273
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    .line 274
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    .line 270
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    invoke-direct {v3, v1, v0, v2, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    return-object v3
.end method

.method private final remindersListWithReminderSaved(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;
    .locals 4

    .line 253
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 254
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getEditingIndex()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->addReminder(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    goto :goto_0

    .line 255
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getEditingIndex()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->setReminder(Lcom/squareup/invoices/workflow/edit/InvoiceReminder;I)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 259
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    .line 260
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->sortByDays()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 261
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    .line 262
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object p1

    .line 258
    new-instance v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    invoke-direct {v3, v1, v0, v2, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    return-object v3
.end method

.method private final setMessage(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 11

    .line 310
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->updateCustomMessage(Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x7e

    const/4 v10, 0x0

    move-object v1, p1

    .line 309
    invoke-static/range {v1 .. v10}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object p1

    return-object p1
.end method

.method private final setOffset(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;I)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 12

    .line 280
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v0

    if-nez p2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 284
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v1

    if-lez v1, :cond_1

    move v1, p2

    goto :goto_0

    .line 285
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v1

    if-gez v1, :cond_2

    neg-int v1, p2

    goto :goto_0

    .line 286
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->getRelativeDays()I

    move-result v1

    .line 281
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->updateRelativeDays(I)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x3e

    const/4 v11, 0x0

    move-object v2, p1

    move v9, p2

    .line 279
    invoke-static/range {v2 .. v11}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object p1

    return-object p1
.end method

.method private final setOffsetType(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;
    .locals 12

    .line 295
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getCachedDays()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 296
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    if-eq p2, v1, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    move p2, v9

    goto :goto_0

    .line 299
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const/4 p2, 0x0

    goto :goto_0

    :cond_2
    neg-int p2, v9

    .line 303
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->getTempReminderToEdit()Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/invoices/workflow/edit/InvoiceReminder;->updateRelativeDays(I)Lcom/squareup/invoices/workflow/edit/InvoiceReminder;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v10, 0x3e

    const/4 v11, 0x0

    move-object v2, p1

    .line 302
    invoke-static/range {v2 .. v11}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;->copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lcom/squareup/invoices/workflow/edit/InvoiceReminder;IZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;IILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersSaved;

    if-eqz v0, :cond_2

    .line 119
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 125
    :goto_0
    invoke-static {v0}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->containsDuplicateDays(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;

    .line 127
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersEnabled()Z

    move-result v1

    .line 128
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v2

    .line 129
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    .line 130
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v4

    .line 126
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$DuplicateDaysError;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 133
    :cond_1
    new-instance v1, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    .line 135
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p1

    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$ConfigsInfo;

    .line 133
    invoke-direct {v1, v0, p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;-><init>(Ljava/util/List;Z)V

    return-object v1

    .line 139
    :cond_2
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;

    const-string v1, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.autoreminders.AutomaticRemindersState.RemindersList"

    if-eqz v0, :cond_7

    .line 140
    move-object v0, p0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v2

    .line 141
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->getRemindersEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    goto :goto_1

    .line 142
    :cond_3
    sget-object v3, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_DISABLED:Lcom/squareup/analytics/RegisterTapName;

    .line 140
    :goto_1
    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 145
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    move-object v3, v2

    check-cast v3, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    .line 146
    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    .line 148
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->getRemindersEnabled()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 150
    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;->getReminders()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 151
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->getDefaultList()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v1

    :cond_4
    move-object v5, v1

    .line 157
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->getRemindersEnabled()Z

    move-result v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 156
    invoke-static/range {v3 .. v9}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->copy$default(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 162
    :cond_5
    new-instance v2, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;

    const/4 v4, 0x1

    .line 165
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemindersToggled;->getDefaultList()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 166
    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v3

    .line 162
    invoke-direct {v2, v4, v1, v0, v3}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$ToggleOffWarning;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 145
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 170
    :cond_7
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;

    if-eqz v0, :cond_8

    .line 171
    new-instance v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    .line 172
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;->getRemindersEnabled()Z

    move-result v2

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;->getRemindersListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;->getDefaultListInfo()Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v4

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ErrorDialogClosed;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v1

    .line 171
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;-><init>(ZLcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 175
    :cond_8
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;

    if-eqz v0, :cond_a

    .line 176
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    .line 177
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;->getIndex()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->editReminder(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;I)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 176
    :cond_9
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_a
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$AddReminder;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$AddReminder;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 180
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_b

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    .line 181
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->editNewReminder(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 180
    :cond_b
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 183
    :cond_c
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderCancel;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderCancel;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.autoreminders.AutomaticRemindersState.EditReminder"

    if-eqz v0, :cond_e

    .line 184
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 185
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->remindersListNoChanges(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 184
    :cond_d
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 187
    :cond_e
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderSaved;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$EditReminderSaved;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 188
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 189
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->remindersListWithReminderSaved(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 188
    :cond_f
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 191
    :cond_10
    sget-object v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemoveReminder;->INSTANCE:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$RemoveReminder;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 192
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_11

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 193
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->remindersListWithReminderRemoved(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$RemindersList;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 192
    :cond_11
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 195
    :cond_12
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffset;

    if-eqz v0, :cond_14

    .line 196
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_13

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 197
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffset;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffset;->getOffset()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->setOffset(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;I)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 196
    :cond_13
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 199
    :cond_14
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffsetType;

    if-eqz v0, :cond_16

    .line 200
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_15

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 201
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffsetType;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$UpdateReminderOffsetType;->getType()Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->setOffsetType(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Lcom/squareup/invoices/workflow/edit/autoreminders/EditReminderScreen$OffsetType;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 200
    :cond_15
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 203
    :cond_16
    instance-of v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$MessageChanged;

    if-eqz v0, :cond_18

    .line 204
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_17

    check-cast v0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    .line 205
    move-object v1, p0

    check-cast v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$MessageChanged;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$MessageChanged;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->setMessage(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;Ljava/lang/String;)Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState$EditReminder;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_2

    .line 204
    :cond_17
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_18
    :goto_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 82
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
