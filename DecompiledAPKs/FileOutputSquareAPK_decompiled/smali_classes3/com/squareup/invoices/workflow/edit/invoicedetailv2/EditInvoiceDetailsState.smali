.class public abstract Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;,
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;,
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;,
        Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \r2\u00020\u0001:\u0004\r\u000e\u000f\u0010B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u000b\u001a\u00020\u000cR\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0003\u0011\u0012\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;",
        "",
        "()V",
        "disableId",
        "",
        "getDisableId",
        "()Z",
        "invoiceDetailsInfo",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "getInvoiceDetailsInfo",
        "()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "takeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Companion",
        "ErrorSavingMessage",
        "SavingMessage",
        "ShowDetails",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ShowDetails;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$SavingMessage;",
        "Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$ErrorSavingMessage;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;->Companion:Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getDisableId()Z
.end method

.method public abstract getInvoiceDetailsInfo()Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;
.end method

.method public final takeSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 2

    .line 35
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$takeSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState$takeSnapshot$1;-><init>(Lcom/squareup/invoices/workflow/edit/invoicedetailv2/EditInvoiceDetailsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
