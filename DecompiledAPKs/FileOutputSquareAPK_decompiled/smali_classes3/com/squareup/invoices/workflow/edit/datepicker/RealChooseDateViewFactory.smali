.class public final Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealChooseDateViewFactory.kt"

# interfaces
.implements Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "chooseDateFactory",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;",
        "customDateFactory",
        "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;",
        "(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;)V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "chooseDateFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customDateFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 13
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 14
    sget-object v2, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDate;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDate;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDate;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$layout;->choose_date_view:I

    .line 15
    new-instance v4, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 13
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 18
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 19
    sget-object p1, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDate;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/CustomDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDate;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    sget v4, Lcom/squareup/features/invoices/R$layout;->custom_date_view:I

    .line 20
    new-instance p1, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory$2;

    invoke-direct {p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/RealChooseDateViewFactory$2;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;)V

    move-object v7, p1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x0

    const/16 v8, 0xc

    const/4 v9, 0x0

    .line 18
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
