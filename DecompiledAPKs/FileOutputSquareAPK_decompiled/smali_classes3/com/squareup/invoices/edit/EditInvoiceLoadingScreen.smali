.class public final Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EditInvoiceLoadingScreen.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$ParentComponent;,
        Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Component;,
        Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceLoadingScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceLoadingScreen.kt\ncom/squareup/invoices/edit/EditInvoiceLoadingScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,57:1\n35#2:58\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceLoadingScreen.kt\ncom/squareup/invoices/edit/EditInvoiceLoadingScreen\n*L\n39#1:58\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\r\u000e\u000fB\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/MaybePersistent;",
        "Lcom/squareup/container/RegistersInScope;",
        "nextScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getNextScreen",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "register",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "Component",
        "Factory",
        "ParentComponent",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final nextScreen:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "nextScreen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;->nextScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public final getNextScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen;->nextScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 32
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 39
    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Component;

    .line 40
    invoke-interface {v0}, Lcom/squareup/invoices/edit/EditInvoiceLoadingScreen$Component;->screenRunner()Lcom/squareup/invoices/edit/EditInvoiceLoadingScreenRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
