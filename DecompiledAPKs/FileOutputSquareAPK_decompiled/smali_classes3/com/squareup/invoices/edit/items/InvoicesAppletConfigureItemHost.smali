.class public Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;
.super Ljava/lang/Object;
.source "InvoicesAppletConfigureItemHost.java"

# interfaces
.implements Lcom/squareup/configure/item/ConfigureItemHost;


# instance fields
.field private final order:Lcom/squareup/payment/Order;

.field private final scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 32
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    return-void
.end method


# virtual methods
.method public commitCartItem(Lcom/squareup/checkout/CartItem;)V
    .locals 1

    .line 107
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 108
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToEditInvoiceScreen()V

    return-void
.end method

.method public compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 126
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Invoices should not comp items"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxRules()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAvailableTaxesAsList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    return-object v0
.end method

.method public getDiningOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/DiningOption;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getGiftCardTotal()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getGiftCardTotal()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getOrderItem(I)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    return-object p1
.end method

.method public getOrderItemCopy(I)Lcom/squareup/checkout/CartItem;
    .locals 0

    .line 112
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->getOrderItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public hasTicket()Z
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasTicket()Z

    move-result v0

    return v0
.end method

.method public isVoidCompAllowed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCancelSelected(Z)Z
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->cancelConfigureItemCard(Z)V

    const/4 p1, 0x1

    return p1
.end method

.method public onCommit(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 0

    return-void
.end method

.method public onCommitSuccess(Lcom/squareup/configure/item/ConfigureItemState;Z)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public onDeleteSelected(Lcom/squareup/configure/item/ConfigureItemState;)Z
    .locals 0

    .line 63
    invoke-virtual {p1}, Lcom/squareup/configure/item/ConfigureItemState;->delete()V

    const/4 p1, 0x1

    return p1
.end method

.method public onSelectedVariationClicked()V
    .locals 0

    return-void
.end method

.method public onStartVisualTransition(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V
    .locals 0

    return-void
.end method

.method public onVariablePriceButtonClicked()V
    .locals 0

    return-void
.end method

.method public onVariationCheckChanged()V
    .locals 0

    return-void
.end method

.method public removeItem(I)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->removeItemFromCart(I)V

    .line 69
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToEditInvoiceScreen()V

    return-void
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editItemInCart(ILcom/squareup/checkout/CartItem;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToEditInvoiceScreen()V

    return-void
.end method

.method public uncompItem(I)V
    .locals 1

    .line 130
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Invoices should not uncomp items"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public voidItem(ILjava/lang/String;Lcom/squareup/protos/client/Employee;)V
    .locals 0

    .line 135
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Invoices should not void items"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
