.class public final Lcom/squareup/invoices/edit/items/ItemSelectScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "ItemSelectScreen.kt"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/items/ItemSelectScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ComponentFactory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/items/ItemSelectScreen$ComponentFactory;",
        "Lcom/squareup/ui/WithComponent$Factory;",
        "()V",
        "create",
        "Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;",
        "parentScope",
        "Lmortar/MortarScope;",
        "path",
        "Lcom/squareup/container/ContainerTreeKey;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;
    .locals 1

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "path"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    const-class p2, Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 65
    check-cast p1, Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;

    .line 67
    invoke-interface {p1}, Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;->placeholderProvider()Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;

    move-result-object p2

    .line 68
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListModule;

    invoke-virtual {p2}, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;->getPlaceholders()Ljava/util/List;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/librarylist/SimpleLibraryListModule;-><init>(Ljava/util/List;)V

    .line 69
    invoke-interface {p1, v0}, Lcom/squareup/invoices/edit/items/ItemSelectScreen$ParentComponent;->itemSelectCard(Lcom/squareup/librarylist/SimpleLibraryListModule;)Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/edit/items/ItemSelectScreen$ComponentFactory;->create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/invoices/edit/items/ItemSelectScreen$Component;

    move-result-object p1

    return-object p1
.end method
