.class public abstract Lcom/squareup/invoices/edit/EditInvoiceScope$EditInvoiceScopeModule;
.super Ljava/lang/Object;
.source "EditInvoiceScope.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/features/invoices/widgets/V1WidgetsModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EditInvoiceScopeModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideHost(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/configure/item/ConfigureItemHost;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 101
    new-instance v0, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/items/InvoicesAppletConfigureItemHost;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    return-object v0
.end method


# virtual methods
.method abstract provider(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/orderentry/KeypadEntryScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesItemSelectOrderEditor(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesItemSelectScreenRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesItemSelectTutorialRunner(Lcom/squareup/register/tutorial/InvoiceTutorialRunner;)Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesPreviewRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesSetupPaymentsDialogRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
