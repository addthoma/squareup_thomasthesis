.class final Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;
.super Ljava/lang/Object;
.source "InvoicePreviewCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;->this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;)V
    .locals 3

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;->this$0:Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    iget-object v1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;->$view:Landroid/view/View;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator$attach$3;->accept(Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;)V

    return-void
.end method
