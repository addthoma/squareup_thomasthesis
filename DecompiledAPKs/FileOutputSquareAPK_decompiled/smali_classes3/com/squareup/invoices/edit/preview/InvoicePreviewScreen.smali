.class public final Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InvoicePreviewScreen.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$ParentComponent;,
        Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;,
        Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;,
        Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicePreviewScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicePreviewScreen.kt\ncom/squareup/invoices/edit/preview/InvoicePreviewScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,106:1\n43#2:107\n24#3,4:108\n*E\n*S KotlinDebug\n*F\n+ 1 InvoicePreviewScreen.kt\ncom/squareup/invoices/edit/preview/InvoicePreviewScreen\n*L\n35#1:107\n101#1,4:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u0012\u0013\u0014\u0015B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0014J\u0008\u0010\u000c\u001a\u00020\u0001H\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000bH\u0016R\u000e\u0010\u0004\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/LayoutScreen;",
        "parentKey",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getParentKey",
        "provideCoordinator",
        "Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "Data",
        "ParentComponent",
        "Runner",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Companion;


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->Companion:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Companion;

    .line 108
    new-instance v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 111
    sput-object v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 97
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    const-class v0, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$ParentComponent;

    .line 35
    invoke-interface {p1}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$ParentComponent;->previewCoordinator()Lcom/squareup/invoices/edit/preview/InvoicePreviewCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 32
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_preview:I

    return v0
.end method
