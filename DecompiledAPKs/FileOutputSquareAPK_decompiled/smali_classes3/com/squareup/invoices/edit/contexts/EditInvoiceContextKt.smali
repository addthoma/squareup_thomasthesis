.class public final Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;
.super Ljava/lang/Object;
.source "EditInvoiceContext.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceContext.kt\ncom/squareup/invoices/edit/contexts/EditInvoiceContextKt\n+ 2 Parcels.kt\ncom/squareup/util/Parcels\n*L\n1#1,282:1\n23#2:283\n23#2:284\n23#2:285\n23#2:286\n23#2:287\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceContext.kt\ncom/squareup/invoices/edit/contexts/EditInvoiceContextKt\n*L\n211#1:283\n214#1:284\n217#1:285\n220#1:286\n223#1:287\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0002\u001a\u0010\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\u0008H\u0002\u001a\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c\u001a\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\u0008H\u0002\u001a\n\u0010\u000f\u001a\u00020\u0010*\u00020\u0011\u001a\u0012\u0010\u0012\u001a\u00020\u0013*\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0010\u00a8\u0006\u0015"
    }
    d2 = {
        "createInvoiceTemplateFromDuplicatedInvoice",
        "Lcom/squareup/protos/client/invoice/Invoice$Builder;",
        "invoiceDisplayDetailsToDuplicate",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "creationContext",
        "Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;",
        "createInvoiceTemplateFromRecurringSeriesDisplayDetails",
        "recurringSeriesDisplayDetails",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
        "getEmptyCartProto",
        "Lcom/squareup/protos/client/bills/Cart;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getRRuleFromRecurringSeriesDisplayDetails",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "readEditInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "Landroid/os/Parcel;",
        "writeEditInvoiceContext",
        "",
        "editInvoiceContext",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$createInvoiceTemplateFromDuplicatedInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->createInvoiceTemplateFromDuplicatedInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$createInvoiceTemplateFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->createInvoiceTemplateFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRRuleFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContextKt;->getRRuleFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p0

    return-object p0
.end method

.method private static final createInvoiceTemplateFromDuplicatedInvoice(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    .line 252
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    .line 254
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    if-nez v0, :cond_0

    .line 257
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;->getCurrentTime()Lcom/squareup/time/CurrentTime;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 256
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    :cond_0
    const-string p1, "newInvoice"

    .line 261
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final createInvoiceTemplateFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->template:Lcom/squareup/protos/client/invoice/Invoice;

    .line 241
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 243
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/RecurringSeries;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/RecurringSchedule;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 242
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p0

    const-string v0, "recurringSeriesDisplayDe\u2026hedule.start_date\n      )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getEmptyCartProto(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart;
    .locals 3

    const-string v0, "currencyCode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    .line 265
    invoke-static {v0, v1, p0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p0

    .line 266
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$Builder;-><init>()V

    .line 268
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    .line 269
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object v1

    .line 270
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object v1

    .line 267
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items(Lcom/squareup/protos/client/bills/Cart$LineItems;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object v0

    .line 273
    new-instance v1, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 274
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v1

    .line 275
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v1

    .line 276
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object v1

    .line 277
    invoke-virtual {v1, p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p0

    .line 278
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p0

    .line 272
    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p0

    .line 280
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p0

    const-string v0, "Cart.Builder()\n      .li\u2026()\n      )\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final getRRuleFromRecurringSeriesDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 1

    .line 232
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/RecurringSeries;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    const-string v0, "recurringSeriesDisplayDe\u2026plate.recurrence_schedule"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    invoke-static {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRuleKt;->fromRecurringSchedule(Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p0

    return-object p0
.end method

.method public static final readEditInvoiceContext(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;
    .locals 2

    const-string v0, "$this$readEditInvoiceContext"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 208
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto/16 :goto_0

    .line 209
    :cond_0
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewRecurringSeries;

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto/16 :goto_0

    .line 210
    :cond_1
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 283
    const-class v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p0

    if-nez p0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 211
    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto/16 :goto_0

    .line 213
    :cond_3
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 284
    const-class v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p0

    if-nez p0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 214
    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    .line 216
    :cond_5
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 285
    const-class v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p0

    if-nez p0, :cond_6

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_6
    check-cast p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    .line 217
    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    .line 219
    :cond_7
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 286
    const-class v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p0

    if-nez p0, :cond_8

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_8
    check-cast p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    .line 220
    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    .line 222
    :cond_9
    const-class v1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 287
    const-class v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-static {p0, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p0

    if-nez p0, :cond_a

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_a
    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 223
    new-instance v0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    move-object p0, v0

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    goto :goto_0

    .line 225
    :cond_b
    sget-object p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;->INSTANCE:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$NewSingleInvoice;

    check-cast p0, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    :goto_0
    return-object p0
.end method

.method public static final writeEditInvoiceContext(Landroid/os/Parcel;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V
    .locals 1

    const-string v0, "$this$writeEditInvoiceContext"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 190
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 192
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 193
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 194
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    if-eqz v0, :cond_2

    .line 195
    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 197
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    if-eqz v0, :cond_3

    .line 198
    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DraftRecurringSeries;->getRecurringSeriesDisplayDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 200
    :cond_3
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    if-eqz v0, :cond_4

    .line 201
    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$DuplicatedSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    check-cast p1, Lcom/squareup/wire/Message;

    invoke-static {p0, p1}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    :cond_4
    :goto_0
    return-void
.end method
