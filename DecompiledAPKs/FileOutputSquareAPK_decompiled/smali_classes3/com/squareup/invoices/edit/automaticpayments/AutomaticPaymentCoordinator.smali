.class public Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AutomaticPaymentCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private allowAutomaticPaymentsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

.field private helperText:Lcom/squareup/widgets/MessageView;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/cnp/CnpFeesMessageHelper;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 30
    iput-object p2, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    .line 31
    iput-object p3, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->res:Lcom/squareup/util/Res;

    .line 32
    iput-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->runner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 54
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 55
    sget v0, Lcom/squareup/features/invoices/R$id;->opt_in_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->helperText:Lcom/squareup/widgets/MessageView;

    .line 56
    sget v0, Lcom/squareup/features/invoices/R$id;->allow_automatic_payment_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->allowAutomaticPaymentsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method private getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 47
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 48
    iget-object v1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->runner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/invoices/edit/automaticpayments/-$$Lambda$-77nIUnn4E0XAPGGVpKgmsfNyB8;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/edit/automaticpayments/-$$Lambda$-77nIUnn4E0XAPGGVpKgmsfNyB8;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payments:I

    .line 49
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 50
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->bindViews(Landroid/view/View;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-direct {p0}, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 39
    iget-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->helperText:Lcom/squareup/widgets/MessageView;

    iget-object v0, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-virtual {v0}, Lcom/squareup/cnp/CnpFeesMessageHelper;->invoiceAutomaticPaymentOptInMessage()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->allowAutomaticPaymentsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v0, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->runner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isAutomaticPaymentsEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 42
    iget-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->allowAutomaticPaymentsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/invoices/edit/automaticpayments/-$$Lambda$AutomaticPaymentCoordinator$9Zm12w3_UTFhoFqQMVQoCcTtLUw;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/automaticpayments/-$$Lambda$AutomaticPaymentCoordinator$9Zm12w3_UTFhoFqQMVQoCcTtLUw;-><init>(Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public synthetic lambda$attach$0$AutomaticPaymentCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 43
    iget-object p1, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;->runner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->allowAutomaticPayments(Z)V

    return-void
.end method
