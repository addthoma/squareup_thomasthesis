.class public Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;
.super Lcom/squareup/invoices/edit/InEditInvoiceScope;
.source "AutomaticPaymentScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/invoices/edit/automaticpayments/-$$Lambda$AutomaticPaymentScreen$9yXUl3M4262i8LFInOypgx3T8yQ;->INSTANCE:Lcom/squareup/invoices/edit/automaticpayments/-$$Lambda$AutomaticPaymentScreen$9yXUl3M4262i8LFInOypgx3T8yQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/InEditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;
    .locals 1

    .line 40
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 41
    new-instance v0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 35
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/edit/InEditInvoiceScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 36
    iget-object v0, p0, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;->editInvoicePath:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 25
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 27
    invoke-interface {p1}, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;->automaticPaymentCoordinator()Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 31
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_automatic_payment_view:I

    return v0
.end method
