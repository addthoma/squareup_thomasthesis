.class public final Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;
.super Ljava/lang/Object;
.source "RealEditInvoiceInTenderRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoiceGatewayProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            ">;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p3, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p4, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p5, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p7, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p8, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->editInvoiceGatewayProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            ">;)",
            "Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;"
        }
    .end annotation

    .line 68
    new-instance v9, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/PermissionGatekeeper;Lrx/Scheduler;Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;
    .locals 10

    .line 75
    new-instance v9, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;-><init>(Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/PermissionGatekeeper;Lrx/Scheduler;Lcom/squareup/invoices/edit/EditInvoiceGateway;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;
    .locals 9

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lrx/Scheduler;

    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->editInvoiceGatewayProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/invoices/edit/EditInvoiceGateway;

    invoke-static/range {v1 .. v8}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->newInstance(Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/permissions/PermissionGatekeeper;Lrx/Scheduler;Lcom/squareup/invoices/edit/EditInvoiceGateway;)Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner_Factory;->get()Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    move-result-object v0

    return-object v0
.end method
