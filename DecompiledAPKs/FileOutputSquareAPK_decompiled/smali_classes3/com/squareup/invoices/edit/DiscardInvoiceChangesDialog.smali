.class public Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;
.super Lcom/squareup/invoices/edit/InEditInvoiceScope;
.source "DiscardInvoiceChangesDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$DiscardInvoiceChangesDialog$cRHAEinhQI4Zqm3TAPAf0SYwTic;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$DiscardInvoiceChangesDialog$cRHAEinhQI4Zqm3TAPAf0SYwTic;

    .line 52
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/InEditInvoiceScope;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 54
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 55
    new-instance v0, Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 47
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/edit/InEditInvoiceScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 48
    iget-object v0, p0, Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;->editInvoicePath:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
