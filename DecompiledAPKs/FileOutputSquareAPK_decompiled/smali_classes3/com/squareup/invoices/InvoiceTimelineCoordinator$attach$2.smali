.class final Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;
.super Ljava/lang/Object;
.source "InvoiceTimelineCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/InvoiceTimelineCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/InvoiceTimelineCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;)V
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;->getActionBarTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->configureActionBar(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->access$getContent$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->access$getContent$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Landroid/widget/LinearLayout;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-static {v1}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->access$getInvoiceTimelineViewFactory$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "view.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;->getTimelineData()Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;

    move-result-object p1

    .line 45
    iget-object v3, p0, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->this$0:Lcom/squareup/invoices/InvoiceTimelineCoordinator;

    invoke-static {v3}, Lcom/squareup/invoices/InvoiceTimelineCoordinator;->access$getRunner$p(Lcom/squareup/invoices/InvoiceTimelineCoordinator;)Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;

    move-result-object v3

    check-cast v3, Lcom/squareup/features/invoices/widgets/EventHandler;

    .line 42
    invoke-virtual {v1, v2, p1, v3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView$Factory;->create(Landroid/content/Context;Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Lcom/squareup/features/invoices/widgets/EventHandler;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 41
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/InvoiceTimelineCoordinator$attach$2;->accept(Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;)V

    return-void
.end method
