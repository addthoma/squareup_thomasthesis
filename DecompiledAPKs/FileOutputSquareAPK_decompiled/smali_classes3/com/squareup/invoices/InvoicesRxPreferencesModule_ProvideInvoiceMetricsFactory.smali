.class public final Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;
.super Ljava/lang/Object;
.source "InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Ljava/util/List<",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideInvoiceMetrics(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;"
        }
    .end annotation

    .line 46
    invoke-static {p0, p1}, Lcom/squareup/invoices/InvoicesRxPreferencesModule;->provideInvoiceMetrics(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iget-object v1, p0, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;->provideInvoiceMetrics(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/squareup/protos/common/CurrencyCode;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/InvoicesRxPreferencesModule_ProvideInvoiceMetricsFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
