.class public abstract Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;
.super Ljava/lang/Object;
.source "LoyaltyBuyerBlockingCartDiffBannerFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BuyerBlockingCartDiffBanner"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$NoBanner;,
        Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$Banner;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0007\u0008B\u0011\u0008\u0002\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\t\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;",
        "",
        "blockingCartDiff",
        "Lcom/squareup/comms/protos/seller/BlockingCartDiff;",
        "(Lcom/squareup/comms/protos/seller/BlockingCartDiff;)V",
        "getBlockingCartDiff",
        "()Lcom/squareup/comms/protos/seller/BlockingCartDiff;",
        "Banner",
        "NoBanner",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$NoBanner;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner$Banner;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final blockingCartDiff:Lcom/squareup/comms/protos/seller/BlockingCartDiff;


# direct methods
.method private constructor <init>(Lcom/squareup/comms/protos/seller/BlockingCartDiff;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;->blockingCartDiff:Lcom/squareup/comms/protos/seller/BlockingCartDiff;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/comms/protos/seller/BlockingCartDiff;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;-><init>(Lcom/squareup/comms/protos/seller/BlockingCartDiff;)V

    return-void
.end method


# virtual methods
.method public getBlockingCartDiff()Lcom/squareup/comms/protos/seller/BlockingCartDiff;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/LoyaltyBuyerBlockingCartDiffBannerFormatter$BuyerBlockingCartDiffBanner;->blockingCartDiff:Lcom/squareup/comms/protos/seller/BlockingCartDiff;

    return-object v0
.end method
