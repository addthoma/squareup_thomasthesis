.class public final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RewardCarouselWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/loyaltycheckin/RewardCarouselProps;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRewardCarouselWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RewardCarouselWorkflow.kt\ncom/squareup/loyaltycheckin/RewardCarouselWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 4 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 7 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,283:1\n264#1,4:296\n266#1:300\n268#1,2:306\n264#1,4:308\n266#1:312\n268#1,2:318\n264#1,4:320\n266#1:324\n268#1,2:330\n264#1,4:336\n266#1:340\n268#1,2:346\n264#1,4:348\n266#1:352\n268#1,2:358\n264#1,4:360\n266#1:364\n268#1,2:370\n264#1,4:372\n266#1:376\n268#1,2:382\n32#2,12:284\n19#3:301\n19#3:313\n19#3:325\n19#3:341\n19#3:353\n19#3:365\n19#3:377\n19#3:387\n41#4:302\n56#4,2:303\n41#4:314\n56#4,2:315\n41#4:326\n56#4,2:327\n41#4:342\n56#4,2:343\n41#4:354\n56#4,2:355\n41#4:366\n56#4,2:367\n41#4:378\n56#4,2:379\n85#4:384\n41#4:388\n56#4,2:389\n276#5:305\n276#5:317\n276#5:329\n276#5:345\n276#5:357\n276#5:369\n276#5:381\n276#5:386\n276#5:391\n1360#6:332\n1429#6,3:333\n240#7:385\n*E\n*S KotlinDebug\n*F\n+ 1 RewardCarouselWorkflow.kt\ncom/squareup/loyaltycheckin/RewardCarouselWorkflow\n*L\n76#1,4:296\n76#1:300\n76#1,2:306\n78#1,4:308\n78#1:312\n78#1,2:318\n80#1,4:320\n80#1:324\n80#1,2:330\n95#1,4:336\n95#1:340\n95#1,2:346\n96#1,4:348\n96#1:352\n96#1,2:358\n97#1,4:360\n97#1:364\n97#1,2:370\n125#1,4:372\n125#1:376\n125#1,2:382\n63#1,12:284\n76#1:301\n78#1:313\n80#1:325\n95#1:341\n96#1:353\n97#1:365\n125#1:377\n266#1:387\n76#1:302\n76#1,2:303\n78#1:314\n78#1,2:315\n80#1:326\n80#1,2:327\n95#1:342\n95#1,2:343\n96#1:354\n96#1,2:355\n97#1:366\n97#1,2:367\n125#1:378\n125#1,2:379\n187#1:384\n267#1:388\n267#1,2:389\n76#1:305\n78#1:317\n80#1:329\n95#1:345\n96#1:357\n97#1:369\n125#1:381\n187#1:386\n267#1:391\n85#1:332\n85#1,3:333\n187#1:385\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B7\u0008\u0007\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J,\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00142\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u001a\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u00022\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u0008\u0010 \u001a\u00020!H\u0002J*\u0010\"\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00140#2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010$\u001a\u00020%H\u0002J\u001c\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00142\u0006\u0010$\u001a\u00020%H\u0002J,\u0010\'\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00032\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040)H\u0016J\u0010\u0010*\u001a\u00020+2\u0006\u0010\u0018\u001a\u00020,H\u0002J\u0010\u0010-\u001a\u00020\u001f2\u0006\u0010\u0018\u001a\u00020\u0003H\u0016J\u0014\u0010.\u001a\u00020%*\u00020\u001b2\u0006\u0010/\u001a\u000200H\u0002JG\u00101\u001a\u000202\"\n\u0008\u0000\u00103\u0018\u0001*\u000204*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040)2 \u0008\u0008\u00105\u001a\u001a\u0012\u0004\u0012\u0002H3\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001406H\u0082\u0008JH\u00107\u001a\u000202*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040)2\u0006\u0010\u0018\u001a\u00020\u00032\u0006\u00108\u001a\u0002092\u001e\u00105\u001a\u001a\u0012\u0004\u0012\u000202\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001406H\u0002J\u0018\u0010:\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014*\u00020;H\u0002J\u0014\u0010<\u001a\u000200*\u00020\u001b2\u0006\u0010/\u001a\u000200H\u0002J \u0010=\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014*\u00020\u00192\u0006\u0010>\u001a\u00020?H\u0002R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselProps;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;",
        "loyaltyServiceHelper",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "loyaltyFrontOfTransactionEvents",
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "res",
        "Lcom/squareup/util/Res;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V",
        "exitAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "goBackToRewardCarouselAction",
        "confirmAction",
        "props",
        "state",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;",
        "rewardTierState",
        "Lcom/squareup/loyalty/CartRewardState$RewardTierState;",
        "initialState",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "isApplyMultipleCouponsEnabled",
        "",
        "redeemReward",
        "Lcom/squareup/workflow/Worker;",
        "tier",
        "Lcom/squareup/loyaltycheckin/Tier;",
        "redeemRewardTier",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "rewardRedeemedScreenData",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;",
        "snapshotState",
        "format",
        "currentPoints",
        "",
        "onReceivedEvent",
        "",
        "Event",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;",
        "handler",
        "Lkotlin/Function1;",
        "onTimer",
        "durationMs",
        "",
        "toEditingAction",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;",
        "totalRedeemableAtOnce",
        "updateQuantityAction",
        "event",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final exitAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final goBackToRewardCarouselAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

.field private final loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltyServiceHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyFrontOfTransactionEvents"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    iput-object p3, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iput-object p4, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p6, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->features:Lcom/squareup/settings/server/Features;

    .line 241
    sget-object p1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x1

    const/4 p3, 0x0

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->goBackToRewardCarouselAction:Lcom/squareup/workflow/WorkflowAction;

    .line 246
    new-instance p1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$exitAction$1;

    invoke-direct {p1, p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$exitAction$1;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p3, p1, p2, p3}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$confirmAction(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyalty/CartRewardState$RewardTierState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->confirmAction(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyalty/CartRewardState$RewardTierState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getExitAction$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getGoBackToRewardCarouselAction$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->goBackToRewardCarouselAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    return-object p0
.end method

.method public static final synthetic access$redeemRewardTier(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->redeemRewardTier(Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toEditingAction(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->toEditingAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateQuantityAction(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->updateQuantityAction(Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final confirmAction(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyalty/CartRewardState$RewardTierState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/RewardCarouselProps;",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;",
            "Lcom/squareup/loyalty/CartRewardState$RewardTierState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 215
    invoke-virtual {p3}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v0

    .line 216
    invoke-virtual {p2}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->getNewQuantity()I

    move-result p2

    invoke-virtual {p3}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr p2, v1

    if-gez p2, :cond_0

    .line 220
    new-instance v1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;

    invoke-direct {v1, p2, p3, v0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$confirmAction$1;-><init>(ILcom/squareup/loyalty/CartRewardState$RewardTierState;Ljava/util/List;Lcom/squareup/loyaltycheckin/RewardCarouselProps;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    const/4 p2, 0x0

    invoke-static {p0, p2, v1, p1, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-lez p2, :cond_1

    .line 235
    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPoints()I

    move-result p1

    invoke-direct {p0, p3, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->format(Lcom/squareup/loyalty/CartRewardState$RewardTierState;I)Lcom/squareup/loyaltycheckin/Tier;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->redeemRewardTier(Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 237
    :cond_1
    iget-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->goBackToRewardCarouselAction:Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1
.end method

.method private final format(Lcom/squareup/loyalty/CartRewardState$RewardTierState;I)Lcom/squareup/loyaltycheckin/Tier;
    .locals 7

    .line 133
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/account/protos/RewardTier;->name:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "rewardTier.name!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/server/account/protos/RewardTier;->coupon_definition_token:Ljava/lang/String;

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const-string v2, "rewardTier.coupon_definition_token!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    iget-object v2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 136
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const-string v4, "rewardTier.points"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 137
    sget v3, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_redeem_card_points_text:I

    .line 135
    invoke-virtual {v2, v5, v6, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object v2

    .line 140
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    const/4 v5, 0x1

    xor-int/2addr v3, v5

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->isApplyMultipleCouponsEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 141
    new-instance p2, Lcom/squareup/loyaltycheckin/Tier$TierState$RedeemedQuantity;

    .line 142
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v3

    .line 143
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v5, :cond_2

    .line 144
    iget-object v4, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/loyaltycheckin/R$string;->x2_buyer_loyalty_check_in_tier_reward_redeemed_multiple:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v4

    .line 145
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const-string v5, "count"

    invoke-virtual {v4, v5, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 147
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 149
    :cond_2
    iget-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/loyaltycheckin/R$string;->x2_buyer_loyalty_check_in_tier_reward_redeemed:I

    invoke-interface {p1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 141
    :goto_0
    invoke-direct {p2, v3, p1}, Lcom/squareup/loyaltycheckin/Tier$TierState$RedeemedQuantity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    check-cast p2, Lcom/squareup/loyaltycheckin/Tier$TierState;

    goto :goto_1

    .line 153
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    xor-int/2addr v3, v5

    if-eqz v3, :cond_4

    sget-object p1, Lcom/squareup/loyaltycheckin/Tier$TierState$Redeemed;->INSTANCE:Lcom/squareup/loyaltycheckin/Tier$TierState$Redeemed;

    move-object p2, p1

    check-cast p2, Lcom/squareup/loyaltycheckin/Tier$TierState;

    goto :goto_1

    :cond_4
    int-to-long v5, p2

    .line 154
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    cmp-long v3, v5, p1

    if-ltz v3, :cond_5

    sget-object p1, Lcom/squareup/loyaltycheckin/Tier$TierState$Redeemable;->INSTANCE:Lcom/squareup/loyaltycheckin/Tier$TierState$Redeemable;

    move-object p2, p1

    check-cast p2, Lcom/squareup/loyaltycheckin/Tier$TierState;

    goto :goto_1

    .line 155
    :cond_5
    sget-object p1, Lcom/squareup/loyaltycheckin/Tier$TierState$NotEnoughPoints;->INSTANCE:Lcom/squareup/loyaltycheckin/Tier$TierState$NotEnoughPoints;

    move-object p2, p1

    check-cast p2, Lcom/squareup/loyaltycheckin/Tier$TierState;

    .line 132
    :goto_1
    new-instance p1, Lcom/squareup/loyaltycheckin/Tier;

    invoke-direct {p1, v0, v1, v2, p2}, Lcom/squareup/loyaltycheckin/Tier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/loyaltycheckin/Tier$TierState;)V

    return-object p1
.end method

.method private final isApplyMultipleCouponsEnabled()Z
    .locals 2

    .line 281
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final synthetic onReceivedEvent(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Event:",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;",
            ">(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-TEvent;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "+",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;)V"
        }
    .end annotation

    .line 265
    invoke-static {p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Event"

    const/4 v2, 0x4

    .line 387
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v2, "ofType(T::class.java)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 388
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    const-string/jumbo v2, "this.toFlowable(BUFFER)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 390
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    const/4 v2, 0x6

    .line 391
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const/4 v1, 0x0

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p1

    move-object v6, p2

    .line 264
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-void

    .line 390
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/RewardCarouselState;JLkotlin/jvm/functions/Function1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "J",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "+",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;)V"
        }
    .end annotation

    .line 276
    invoke-virtual {p2}, Lcom/squareup/loyaltycheckin/RewardCarouselState;->toString()Ljava/lang/String;

    move-result-object p2

    .line 277
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-wide v1, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/workflow/Worker$Companion;->timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;

    move-result-object p3

    .line 275
    invoke-interface {p1, p3, p2, p5}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final redeemReward(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/Worker;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/RewardCarouselProps;",
            "Lcom/squareup/loyaltycheckin/Tier;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;>;"
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 168
    invoke-virtual {p2}, Lcom/squareup/loyaltycheckin/Tier;->getCouponDefinitionToken()Ljava/lang/String;

    move-result-object v1

    .line 169
    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPhoneToken()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 167
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->redeemPoints(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 172
    new-instance v1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/Tier;Lcom/squareup/loyaltycheckin/RewardCarouselProps;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "loyaltyServiceHelper.red\u2026  }\n          }\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$$inlined$asWorker$1;

    invoke-direct {p2, p1, v3}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 385
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 386
    const-class p2, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method private final redeemRewardTier(Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/Tier;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 190
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemRewardTier$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemRewardTier$1;-><init>(Lcom/squareup/loyaltycheckin/Tier;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final rewardRedeemedScreenData(Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;
    .locals 3

    .line 252
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;

    .line 253
    iget-object v1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_redeemed_reward:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;->getTier()Lcom/squareup/loyaltycheckin/Tier;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/loyaltycheckin/Tier;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "reward_name"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 255
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 256
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 257
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getREDEEMED_REWARD_AUTOCLOSE_MS()J

    move-result-wide v1

    .line 252
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;-><init>(Ljava/lang/String;J)V

    return-object v0
.end method

.method private final toEditingAction(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 195
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$toEditingAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$toEditingAction$1;-><init>(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final totalRedeemableAtOnce(Lcom/squareup/loyalty/CartRewardState$RewardTierState;I)I
    .locals 2

    int-to-double v0, p2

    .line 161
    invoke-virtual {p1}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getRewardTier()Lcom/squareup/server/account/protos/RewardTier;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/server/account/protos/RewardTier;->points:Ljava/lang/Long;

    const-string p2, "rewardTier.points"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    long-to-double p1, p1

    div-double/2addr v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide p1

    double-to-int p1, p1

    return p1
.end method

.method private final updateQuantityAction(Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;"
        }
    .end annotation

    .line 204
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$updateQuantityAction$1;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->workflowAction$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 284
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 289
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 291
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 292
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 293
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 294
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 295
    :cond_3
    check-cast v1, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 63
    :cond_4
    sget-object v1, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->initialState(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltycheckin/RewardCarouselProps;",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;)",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    const-string v0, "props"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    move-object/from16 v15, p3

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of v0, v8, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;

    const-string v14, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    const-string/jumbo v13, "this.toFlowable(BUFFER)"

    const-string v12, "ofType(T::class.java)"

    if-eqz v0, :cond_5

    .line 75
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getCHECKED_IN_INACTIVITY_AUTOCLOSE_MS()J

    move-result-wide v3

    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$1;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$1;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/RewardCarouselState;JLkotlin/jvm/functions/Function1;)V

    .line 76
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$2;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$2;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 297
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 301
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_4

    .line 304
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 305
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x0

    move-object/from16 v8, p3

    move-object v5, v12

    move v12, v0

    move-object v3, v13

    move-object v13, v1

    .line 296
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 78
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$3;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$3;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 309
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 313
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$RedeemReward;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 314
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_3

    .line 316
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 317
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$RedeemReward;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    .line 308
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 79
    invoke-direct/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->isApplyMultipleCouponsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$4;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$4;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 321
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 325
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 326
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_0

    .line 328
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 329
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v2

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    .line 320
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    goto :goto_0

    .line 328
    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v14}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_1
    :goto_0
    iget-object v0, v6, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPoints()I

    move-result v1

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_balance_text:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getCartRewardState()Lcom/squareup/loyalty/CartRewardState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/loyalty/CartRewardState;->getRewardTiers()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 332
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 333
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 334
    check-cast v3, Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    .line 85
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPoints()I

    move-result v4

    invoke-direct {v6, v3, v4}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->format(Lcom/squareup/loyalty/CartRewardState$RewardTierState;I)Lcom/squareup/loyaltycheckin/Tier;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 335
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 83
    new-instance v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardCarouselScreenData;

    invoke-direct {v1, v0, v2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardCarouselScreenData;-><init>(Ljava/lang/String;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v1

    .line 316
    :cond_3
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v14}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_4
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v14}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v5, v12

    move-object v3, v13

    .line 89
    instance-of v0, v8, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;

    if-eqz v0, :cond_c

    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getCartRewardState()Lcom/squareup/loyalty/CartRewardState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyalty/CartRewardState;->getRewardTiers()Ljava/util/List;

    move-result-object v0

    move-object/from16 v16, v8

    check-cast v16, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;

    invoke-virtual/range {v16 .. v16}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->getTierIndex()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/loyalty/CartRewardState$RewardTierState;

    .line 94
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getCHECKED_IN_INACTIVITY_AUTOCLOSE_MS()J

    move-result-wide v9

    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$6;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$6;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v11, v0

    check-cast v11, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    move-object v12, v3

    move-wide v3, v9

    move-object v10, v5

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/RewardCarouselState;JLkotlin/jvm/functions/Function1;)V

    .line 95
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$7;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$7;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 337
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v1

    .line 341
    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 342
    sget-object v2, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    invoke-static {v1, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_b

    .line 344
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 345
    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v1, v3

    check-cast v1, Lcom/squareup/workflow/Worker;

    const/4 v11, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    move-object/from16 v9, p3

    move-object v5, v10

    move-object v10, v1

    move-object v4, v12

    move-object v12, v0

    move-object v0, v13

    move v13, v2

    move-object v2, v14

    move-object v14, v3

    .line 336
    invoke-static/range {v9 .. v14}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 96
    new-instance v1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$8;

    invoke-direct {v1, v6, v8}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$8;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/RewardCarouselState;)V

    move-object v12, v1

    check-cast v12, Lkotlin/jvm/functions/Function1;

    .line 349
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v1

    .line 353
    const-class v3, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 354
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_a

    .line 356
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 357
    const-class v3, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$UpdateRewardTierQuantity;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    new-instance v9, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v9, v3, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v10, v9

    check-cast v10, Lcom/squareup/workflow/Worker;

    const/4 v11, 0x0

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object/from16 v9, p3

    .line 348
    invoke-static/range {v9 .. v14}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 97
    new-instance v1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$9;

    invoke-direct {v1, v6, v7, v8, v0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$9;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState;Lcom/squareup/loyalty/CartRewardState$RewardTierState;)V

    move-object v11, v1

    check-cast v11, Lkotlin/jvm/functions/Function1;

    .line 361
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v1

    .line 365
    const-class v3, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ConfirmEditRewardTier;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    sget-object v3, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v1, v3}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v1

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lorg/reactivestreams/Publisher;

    if-eqz v1, :cond_9

    .line 368
    invoke-static {v1}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 369
    const-class v2, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ConfirmEditRewardTier;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    new-instance v3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v3, v2, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v9, v3

    check-cast v9, Lcom/squareup/workflow/Worker;

    const/4 v10, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v8, p3

    .line 360
    invoke-static/range {v8 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 101
    new-instance v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;

    .line 102
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPoints()I

    move-result v2

    invoke-direct {v6, v0, v2}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->format(Lcom/squareup/loyalty/CartRewardState$RewardTierState;I)Lcom/squareup/loyaltycheckin/Tier;

    move-result-object v18

    .line 103
    invoke-virtual {v0}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual/range {v16 .. v16}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->getNewQuantity()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v2, v3, :cond_6

    const/16 v19, 0x1

    goto :goto_2

    :cond_6
    const/16 v19, 0x0

    .line 104
    :goto_2
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->getNewQuantity()I

    move-result v20

    .line 105
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->getNewQuantity()I

    move-result v2

    if-lez v2, :cond_7

    const/16 v21, 0x1

    goto :goto_3

    :cond_7
    const/16 v21, 0x0

    .line 109
    :goto_3
    invoke-virtual/range {v16 .. v16}, Lcom/squareup/loyaltycheckin/RewardCarouselState$EditingRewardTier;->getNewQuantity()I

    move-result v2

    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getPoints()I

    move-result v3

    invoke-direct {v6, v0, v3}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->totalRedeemableAtOnce(Lcom/squareup/loyalty/CartRewardState$RewardTierState;I)I

    move-result v3

    .line 109
    invoke-virtual {v0}, Lcom/squareup/loyalty/CartRewardState$RewardTierState;->getAppliedCouponTokens()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v5

    invoke-static {v3, v0}, Lkotlin/ranges/RangesKt;->coerceAtMost(II)I

    move-result v0

    if-ge v2, v0, :cond_8

    const/16 v22, 0x1

    goto :goto_4

    :cond_8
    const/16 v22, 0x0

    :goto_4
    move-object/from16 v17, v1

    .line 101
    invoke-direct/range {v17 .. v22}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$EditRewardTierScreenData;-><init>(Lcom/squareup/loyaltycheckin/Tier;ZIZZ)V

    check-cast v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v1

    .line 368
    :cond_9
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 356
    :cond_a
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    move-object v2, v14

    .line 344
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    move-object v4, v3

    move-object v2, v14

    .line 113
    instance-of v0, v8, Lcom/squareup/loyaltycheckin/RewardCarouselState$RedeemingReward;

    if-eqz v0, :cond_d

    .line 114
    move-object v0, v8

    check-cast v0, Lcom/squareup/loyaltycheckin/RewardCarouselState$RedeemingReward;

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/RewardCarouselState$RedeemingReward;->getTier()Lcom/squareup/loyaltycheckin/Tier;

    move-result-object v0

    invoke-direct {v6, v7, v0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->redeemReward(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/Tier;)Lcom/squareup/workflow/Worker;

    move-result-object v8

    const/4 v9, 0x0

    sget-object v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$10;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$10;

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v7, p3

    invoke-static/range {v7 .. v12}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 115
    sget-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;->INSTANCE:Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$LoadingScreenData;

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 118
    :cond_d
    instance-of v0, v8, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;

    if-eqz v0, :cond_e

    .line 119
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getREDEEMED_REWARD_AUTOCLOSE_MS()J

    move-result-wide v3

    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$11;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$11;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/RewardCarouselState;JLkotlin/jvm/functions/Function1;)V

    .line 120
    move-object v0, v8

    check-cast v0, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;

    invoke-direct {v6, v0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->rewardRedeemedScreenData(Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$RewardRedeemedScreenData;

    move-result-object v0

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 123
    :cond_e
    instance-of v0, v8, Lcom/squareup/loyaltycheckin/RewardCarouselState$ErrorRedeemingReward;

    if-eqz v0, :cond_10

    .line 124
    invoke-static {}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInWorkflowKt;->getERROR_AUTOCLOSE_MS()J

    move-result-wide v9

    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$12;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$12;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object v11, v2

    move-object/from16 v2, p2

    move-object v8, v4

    move-wide v3, v9

    move-object v9, v5

    move-object v5, v7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->onTimer(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltycheckin/RewardCarouselState;JLkotlin/jvm/functions/Function1;)V

    .line 125
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$13;

    invoke-direct {v0, v6}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$render$13;-><init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function1;

    .line 373
    invoke-static/range {p0 .. p0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;->events$public_release()Lio/reactivex/Observable;

    move-result-object v0

    .line 377
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 378
    sget-object v1, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v0

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    if-eqz v0, :cond_f

    .line 380
    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 381
    const-class v1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$Done;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v8, v2

    check-cast v8, Lcom/squareup/workflow/Worker;

    const/4 v9, 0x0

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object/from16 v7, p3

    .line 372
    invoke-static/range {v7 .. v12}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 126
    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;

    iget-object v1, v6, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/loyaltycheckin/R$string;->loyalty_check_in_error_redeeming:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData$ErrorScreenData;-><init>(Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    return-object v0

    .line 380
    :cond_f
    new-instance v0, Lkotlin/TypeCastException;

    invoke-direct {v0, v11}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_10
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    check-cast p2, Lcom/squareup/loyaltycheckin/RewardCarouselState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->render(Lcom/squareup/loyaltycheckin/RewardCarouselProps;Lcom/squareup/loyaltycheckin/RewardCarouselState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInScreenData;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/loyaltycheckin/RewardCarouselState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/loyaltycheckin/RewardCarouselState;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->snapshotState(Lcom/squareup/loyaltycheckin/RewardCarouselState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
