.class public final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;
.super Ljava/lang/Object;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

.field final synthetic this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
            ")V"
        }
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    iput-object p2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->$it:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            ">;)",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->$it:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    iget-object v0, v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    invoke-static {v0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getAnalytics$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$RedeemReward;->INSTANCE:Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$RedeemReward;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 177
    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    iget-object v1, v1, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->$tier:Lcom/squareup/loyaltycheckin/Tier;

    invoke-direct {v0, v1}, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardRedeemed;-><init>(Lcom/squareup/loyaltycheckin/Tier;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    .line 178
    new-instance p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ApplyCoupon;

    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    iget-object v0, v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->$props:Lcom/squareup/loyaltycheckin/RewardCarouselProps;

    invoke-virtual {v0}, Lcom/squareup/loyaltycheckin/RewardCarouselProps;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->$it:Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    check-cast v1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const-string v2, "it.response.coupon"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    iget-object v2, v2, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->$tier:Lcom/squareup/loyaltycheckin/Tier;

    invoke-virtual {v2}, Lcom/squareup/loyaltycheckin/Tier;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput$ApplyCoupon;-><init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    return-object p1

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;

    iget-object v0, v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1;->this$0:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;

    invoke-static {v0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;->access$getAnalytics$p(Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ErrorRedeemingReward;->INSTANCE:Lcom/squareup/loyaltycheckin/CheckInAnalyticEvent$ErrorRedeemingReward;

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 181
    sget-object v0, Lcom/squareup/loyaltycheckin/RewardCarouselState$ErrorRedeemingReward;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselState$ErrorRedeemingReward;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 173
    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$redeemReward$1$1;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            "-",
            "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
