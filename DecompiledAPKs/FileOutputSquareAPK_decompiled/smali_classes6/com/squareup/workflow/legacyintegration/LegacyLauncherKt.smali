.class public final Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;
.super Ljava/lang/Object;
.source "LegacyLauncher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001az\u0010\u0000\u001a6\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\u0001j\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0004`\u0006\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0007\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0007\"\u0008\u0008\u0002\u0010\u0004*\u00020\u00072\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00040\t2\u0006\u0010\n\u001a\u00020\u000b*X\u0010\u000c\u001a\u0004\u0008\u0000\u0010\u0003\u001a\u0004\u0008\u0001\u0010\u0005\u001a\u0004\u0008\u0002\u0010\u0004\" \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\u00012 \u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\u0001\u00a8\u0006\r"
    }
    d2 = {
        "createLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "InputT",
        "RenderingT",
        "OutputT",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "LegacyLauncher",
        "v2-legacy-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TInputT;+TOutputT;+TRenderingT;>;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;

    check-cast p1, Lkotlin/coroutines/CoroutineContext;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;-><init>(Lcom/squareup/workflow/Workflow;Lkotlin/coroutines/CoroutineContext;)V

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    return-object v0
.end method
