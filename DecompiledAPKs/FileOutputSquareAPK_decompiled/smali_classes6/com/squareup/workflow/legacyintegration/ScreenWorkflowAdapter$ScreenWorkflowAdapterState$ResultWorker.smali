.class public final Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;
.super Ljava/lang/Object;
.source "ScreenWorkflowAdapter.kt"

# interfaces
.implements Lcom/squareup/workflow/Worker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ResultWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Worker<",
        "TOutputT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0004\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00030\u0001B\u0015\u0012\u000e\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00030\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\u00020\u00082\n\u0010\t\u001a\u0006\u0012\u0002\u0008\u00030\u0001H\u0016J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00028\u00030\u000bH\u0016R\u0019\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00030\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;",
        "Lcom/squareup/workflow/Worker;",
        "result",
        "Lio/reactivex/Single;",
        "(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lio/reactivex/Single;)V",
        "getResult",
        "()Lio/reactivex/Single;",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "run",
        "Lkotlinx/coroutines/flow/Flow;",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final result:Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Single<",
            "+TOutputT;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;Lio/reactivex/Single;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Single<",
            "+TOutputT;>;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;->this$0:Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;->result:Lio/reactivex/Single;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    instance-of p1, p1, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;

    return p1
.end method

.method public final getResult()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "+TOutputT;>;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;->result:Lio/reactivex/Single;

    return-object v0
.end method

.method public run()Lkotlinx/coroutines/flow/Flow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "TOutputT;>;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/ScreenWorkflowAdapter$ScreenWorkflowAdapterState$ResultWorker;->result:Lio/reactivex/Single;

    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "result.toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    invoke-static {v0}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    return-object v0
.end method
