.class public final Lcom/squareup/workflow/ScreenHint;
.super Ljava/lang/Object;
.source "ScreenHint.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ScreenHint$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScreenHint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScreenHint.kt\ncom/squareup/workflow/ScreenHint\n*L\n1#1,106:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 -2\u00020\u0001:\u0001-Bi\u0008\u0016\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0008\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0012B_\u0008\u0002\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u0012\u0006\u0010\r\u001a\u00020\u0006\u0012\u0006\u0010\u000e\u001a\u00020\u0006\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0015J\u0008\u0010&\u001a\u00020\'H\u0016J\u0018\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\'H\u0016R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u000e\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\r\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0019R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0019R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0017\u0010\u0007\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010\u001cR\u0011\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/workflow/ScreenHint;",
        "Landroid/os/Parcelable;",
        "phoneOrientation",
        "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "tabletOrientation",
        "isMaster",
        "",
        "section",
        "Ljava/lang/Class;",
        "phoneSoftInputMode",
        "Lcom/squareup/workflow/SoftInputMode;",
        "spot",
        "Lcom/squareup/container/spot/Spot;",
        "hideStatusBar",
        "hideMaster",
        "analyticsName",
        "",
        "(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V",
        "()V",
        "viewType",
        "Lcom/squareup/workflow/WorkflowViewFactory$ViewType;",
        "(Lcom/squareup/workflow/WorkflowViewFactory$ViewType;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V",
        "getAnalyticsName",
        "()Ljava/lang/String;",
        "getHideMaster",
        "()Z",
        "getHideStatusBar",
        "getPhoneOrientation",
        "()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
        "getPhoneSoftInputMode",
        "()Lcom/squareup/workflow/SoftInputMode;",
        "getSection",
        "()Ljava/lang/Class;",
        "getSpot",
        "()Lcom/squareup/container/spot/Spot;",
        "getTabletOrientation",
        "getViewType",
        "()Lcom/squareup/workflow/WorkflowViewFactory$ViewType;",
        "describeContents",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/workflow/ScreenHint;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/workflow/ScreenHint$Companion;


# instance fields
.field private final analyticsName:Ljava/lang/String;

.field private final hideMaster:Z

.field private final hideStatusBar:Z

.field private final isMaster:Z

.field private final phoneOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

.field private final phoneSoftInputMode:Lcom/squareup/workflow/SoftInputMode;

.field private final section:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private final spot:Lcom/squareup/container/spot/Spot;

.field private final tabletOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

.field private final viewType:Lcom/squareup/workflow/WorkflowViewFactory$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/ScreenHint$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ScreenHint$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/ScreenHint;->Companion:Lcom/squareup/workflow/ScreenHint$Companion;

    .line 86
    new-instance v0, Lcom/squareup/workflow/ScreenHint$Companion$CREATOR$1;

    invoke-direct {v0}, Lcom/squareup/workflow/ScreenHint$Companion$CREATOR$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/workflow/ScreenHint;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 12

    .line 60
    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x1fe

    const/4 v11, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
            "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
            "Z",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/squareup/workflow/SoftInputMode;",
            "Lcom/squareup/container/spot/Spot;",
            "ZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "phoneOrientation"

    move-object v3, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tabletOrientation"

    move-object v4, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneSoftInputMode"

    move-object/from16 v7, p5

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analyticsName"

    move-object/from16 v11, p9

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    sget-object v2, Lcom/squareup/workflow/WorkflowViewFactory$ViewType;->LAYOUT:Lcom/squareup/workflow/WorkflowViewFactory$ViewType;

    move-object v1, p0

    move v5, p3

    move-object/from16 v6, p4

    move-object/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    .line 46
    invoke-direct/range {v1 .. v11}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$ViewType;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 10

    move/from16 v0, p10

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 37
    sget-object v1, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    goto :goto_0

    :cond_0
    move-object v1, p1

    :goto_0
    and-int/lit8 v2, v0, 0x2

    if-eqz v2, :cond_1

    .line 38
    sget-object v2, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    goto :goto_1

    :cond_1
    move-object v2, p2

    :goto_1
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    move v3, p3

    :goto_2
    and-int/lit8 v5, v0, 0x8

    const/4 v6, 0x0

    if-eqz v5, :cond_3

    .line 40
    move-object v5, v6

    check-cast v5, Ljava/lang/Class;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v7, v0, 0x10

    if-eqz v7, :cond_4

    .line 41
    sget-object v7, Lcom/squareup/workflow/SoftInputMode;->RESIZE:Lcom/squareup/workflow/SoftInputMode;

    goto :goto_4

    :cond_4
    move-object v7, p5

    :goto_4
    and-int/lit8 v8, v0, 0x20

    if-eqz v8, :cond_5

    .line 42
    check-cast v6, Lcom/squareup/container/spot/Spot;

    goto :goto_5

    :cond_5
    move-object/from16 v6, p6

    :goto_5
    and-int/lit8 v8, v0, 0x40

    if-eqz v8, :cond_6

    const/4 v8, 0x0

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v0, 0x80

    if-eqz v9, :cond_7

    goto :goto_7

    :cond_7
    move/from16 v4, p8

    :goto_7
    and-int/lit16 v0, v0, 0x100

    if-eqz v0, :cond_8

    const-string v0, ""

    goto :goto_8

    :cond_8
    move-object/from16 v0, p9

    :goto_8
    move-object p1, p0

    move-object p2, v1

    move-object p3, v2

    move p4, v3

    move-object p5, v5

    move-object/from16 p6, v7

    move-object/from16 p7, v6

    move/from16 p8, v8

    move/from16 p9, v4

    move-object/from16 p10, v0

    .line 45
    invoke-direct/range {p1 .. p10}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/workflow/WorkflowViewFactory$ViewType;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowViewFactory$ViewType;",
            "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
            "Lcom/squareup/workflow/WorkflowViewFactory$Orientation;",
            "Z",
            "Ljava/lang/Class<",
            "*>;",
            "Lcom/squareup/workflow/SoftInputMode;",
            "Lcom/squareup/container/spot/Spot;",
            "ZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ScreenHint;->viewType:Lcom/squareup/workflow/WorkflowViewFactory$ViewType;

    iput-object p2, p0, Lcom/squareup/workflow/ScreenHint;->phoneOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    iput-object p3, p0, Lcom/squareup/workflow/ScreenHint;->tabletOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    iput-boolean p4, p0, Lcom/squareup/workflow/ScreenHint;->isMaster:Z

    iput-object p5, p0, Lcom/squareup/workflow/ScreenHint;->section:Ljava/lang/Class;

    iput-object p6, p0, Lcom/squareup/workflow/ScreenHint;->phoneSoftInputMode:Lcom/squareup/workflow/SoftInputMode;

    iput-object p7, p0, Lcom/squareup/workflow/ScreenHint;->spot:Lcom/squareup/container/spot/Spot;

    iput-boolean p8, p0, Lcom/squareup/workflow/ScreenHint;->hideStatusBar:Z

    iput-boolean p9, p0, Lcom/squareup/workflow/ScreenHint;->hideMaster:Z

    iput-object p10, p0, Lcom/squareup/workflow/ScreenHint;->analyticsName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/WorkflowViewFactory$ViewType;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 19
    invoke-direct/range {p0 .. p10}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$ViewType;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;)V

    return-void
.end method

.method public static final forDialog()Lcom/squareup/workflow/ScreenHint;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/ScreenHint;->Companion:Lcom/squareup/workflow/ScreenHint$Companion;

    invoke-virtual {v0}, Lcom/squareup/workflow/ScreenHint$Companion;->forDialog()Lcom/squareup/workflow/ScreenHint;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getAnalyticsName()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->analyticsName:Ljava/lang/String;

    return-object v0
.end method

.method public final getHideMaster()Z
    .locals 1

    .line 29
    iget-boolean v0, p0, Lcom/squareup/workflow/ScreenHint;->hideMaster:Z

    return v0
.end method

.method public final getHideStatusBar()Z
    .locals 1

    .line 28
    iget-boolean v0, p0, Lcom/squareup/workflow/ScreenHint;->hideStatusBar:Z

    return v0
.end method

.method public final getPhoneOrientation()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->phoneOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public final getPhoneSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->phoneSoftInputMode:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public final getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->section:Ljava/lang/Class;

    return-object v0
.end method

.method public final getSpot()Lcom/squareup/container/spot/Spot;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->spot:Lcom/squareup/container/spot/Spot;

    return-object v0
.end method

.method public final getTabletOrientation()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->tabletOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public final getViewType()Lcom/squareup/workflow/WorkflowViewFactory$ViewType;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->viewType:Lcom/squareup/workflow/WorkflowViewFactory$ViewType;

    return-object v0
.end method

.method public final isMaster()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/workflow/ScreenHint;->isMaster:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->viewType:Lcom/squareup/workflow/WorkflowViewFactory$ViewType;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowViewFactory$ViewType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->phoneOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->tabletOrientation:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    invoke-virtual {v0}, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    iget-boolean v0, p0, Lcom/squareup/workflow/ScreenHint;->isMaster:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->section:Ljava/lang/Class;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->phoneSoftInputMode:Lcom/squareup/workflow/SoftInputMode;

    invoke-virtual {v0}, Lcom/squareup/workflow/SoftInputMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    iget-object v0, p0, Lcom/squareup/workflow/ScreenHint;->spot:Lcom/squareup/container/spot/Spot;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 75
    iget-boolean p2, p0, Lcom/squareup/workflow/ScreenHint;->hideStatusBar:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    iget-boolean p2, p0, Lcom/squareup/workflow/ScreenHint;->hideMaster:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    iget-object p2, p0, Lcom/squareup/workflow/ScreenHint;->analyticsName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
