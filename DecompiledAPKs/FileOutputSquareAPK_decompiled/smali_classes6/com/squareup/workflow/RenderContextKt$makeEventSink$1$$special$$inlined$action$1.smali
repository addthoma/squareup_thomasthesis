.class public final Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;->invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "TStateT;TOutputT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowAction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt$action$2\n+ 2 RenderContext.kt\ncom/squareup/workflow/RenderContextKt$makeEventSink$1\n*L\n1#1,211:1\n183#2:212\n183#2:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0018\u0010\u0004\u001a\u00020\u0005*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006H\u0016\u00a8\u0006\u0007\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/workflow/WorkflowActionKt$action$2",
        "Lcom/squareup/workflow/WorkflowAction;",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event$inlined:Ljava/lang/Object;

.field final synthetic $event$inlined$1:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->this$0:Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;

    iput-object p2, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->$event$inlined:Ljava/lang/Object;

    iput-object p3, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->$event$inlined$1:Ljava/lang/Object;

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "TStateT;>;)TOutputT;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 199
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "TStateT;-TOutputT;>;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->this$0:Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;

    iget-object v0, v0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1;->$update:Lkotlin/jvm/functions/Function2;

    iget-object v1, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->$event$inlined:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "WorkflowAction("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eventSink("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->$event$inlined$1:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/workflow/RenderContextKt$makeEventSink$1$$special$$inlined$action$1;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
