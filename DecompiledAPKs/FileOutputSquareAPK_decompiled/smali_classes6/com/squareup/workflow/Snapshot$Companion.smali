.class public final Lcom/squareup/workflow/Snapshot$Companion;
.super Ljava/lang/Object;
.source "Snapshot.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0005\u001a\u00020\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u0007J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u0008H\u0007J\u001c\u0010\u000e\u001a\u00020\u00042\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000fH\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/workflow/Snapshot$Companion;",
        "",
        "()V",
        "EMPTY",
        "Lcom/squareup/workflow/Snapshot;",
        "of",
        "lazy",
        "Lkotlin/Function0;",
        "Lokio/ByteString;",
        "integer",
        "",
        "string",
        "",
        "byteString",
        "write",
        "Lkotlin/Function1;",
        "Lokio/BufferedSink;",
        "",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/workflow/Snapshot$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final of(I)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/workflow/Snapshot;

    new-instance v1, Lcom/squareup/workflow/Snapshot$Companion$of$3;

    invoke-direct {v1, p1}, Lcom/squareup/workflow/Snapshot$Companion$of$3;-><init>(I)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/Snapshot;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final of(Ljava/lang/String;)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "string"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/workflow/Snapshot;

    new-instance v1, Lcom/squareup/workflow/Snapshot$Companion$of$1;

    invoke-direct {v1, p1}, Lcom/squareup/workflow/Snapshot$Companion$of$1;-><init>(Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/Snapshot;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final of(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lokio/ByteString;",
            ">;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "lazy"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/squareup/workflow/Snapshot;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/workflow/Snapshot;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/workflow/Snapshot;

    new-instance v1, Lcom/squareup/workflow/Snapshot$Companion$of$2;

    invoke-direct {v1, p1}, Lcom/squareup/workflow/Snapshot$Companion$of$2;-><init>(Lokio/ByteString;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    const/4 p1, 0x0

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/Snapshot;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lokio/BufferedSink;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "lazy"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/workflow/Snapshot$Companion$write$1;

    invoke-direct {v1, p1}, Lcom/squareup/workflow/Snapshot$Companion$write$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
