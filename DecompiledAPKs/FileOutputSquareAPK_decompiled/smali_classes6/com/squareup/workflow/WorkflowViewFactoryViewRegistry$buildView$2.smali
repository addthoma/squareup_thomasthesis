.class final Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowViewFactoryViewRegistry.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;->buildView(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "TRenderingT;",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0006\u0010\u0004\u001a\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "RenderingT",
        "",
        "rendering",
        "<anonymous parameter 1>",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "invoke",
        "(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $key:Lcom/squareup/workflow/legacy/Screen$Key;

.field final synthetic $screens:Lcom/jakewharton/rxrelay2/BehaviorRelay;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen$Key;Lcom/jakewharton/rxrelay2/BehaviorRelay;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;->$key:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p2, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;->$screens:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p2, Lcom/squareup/workflow/ui/ContainerHints;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;->invoke(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")V"
        }
    .end annotation

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 1>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    new-instance p2, Lcom/squareup/workflow/legacy/Screen;

    iget-object v0, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;->$key:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    invoke-direct {p2, v0, p1, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry$buildView$2;->$screens:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
