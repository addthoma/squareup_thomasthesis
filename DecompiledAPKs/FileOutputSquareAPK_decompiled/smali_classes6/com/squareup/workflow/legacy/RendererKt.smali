.class public final Lcom/squareup/workflow/legacy/RendererKt;
.super Ljava/lang/Object;
.source "Renderer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001ak\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0001*\u00020\u0003*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00010\u00062\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00082\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "render",
        "R",
        "S",
        "",
        "E",
        "O",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;",
        "legacy-workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final render(Lcom/squareup/workflow/legacy/Renderer;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Renderer<",
            "TS;TE;TR;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;-TE;+TO;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")TR;"
        }
    .end annotation

    const-string v0, "$this$render"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, p1}, Lcom/squareup/workflow/legacy/WorkflowPool;->input(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    invoke-interface {p0, v0, p1, p2}, Lcom/squareup/workflow/legacy/Renderer;->render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method
