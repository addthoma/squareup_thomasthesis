.class public final Lcom/squareup/workflow/legacy/WorkflowPool$Companion;
.super Ljava/lang/Object;
.source "WorkflowPool.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/WorkflowPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowPool.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 2 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,422:1\n335#1:424\n335#1:426\n343#1:428\n335#1:429\n412#2:423\n412#2:425\n412#2:427\n412#2:430\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n*L\n343#1:426\n335#1:423\n343#1:427\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002Jx\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\n\u0008\u0000\u0010\u0005\u0018\u0001*\u00020\u0001\"\n\u0008\u0001\u0010\u0006\u0018\u0001*\u00020\u0001\"\n\u0008\u0002\u0010\u0007\u0018\u0001*\u00020\u00012 \u0010\u0008\u001a\u001c\u0012\u0018\u0008\u0001\u0012\u0014\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\n0\t2\u0006\u0010\u000b\u001a\u0002H\u00052\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u0086\u0008\u00a2\u0006\u0002\u0010\u000eJ_\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\n\u0008\u0000\u0010\u0006\u0018\u0001*\u00020\u0001\"\n\u0008\u0001\u0010\u0007\u0018\u0001*\u00020\u00012 \u0010\u0008\u001a\u001c\u0012\u0018\u0008\u0001\u0012\u0014\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\n0\t2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u0086\u0008\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowPool$Companion;",
        "",
        "()V",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "S",
        "E",
        "O",
        "launcherType",
        "Lkotlin/reflect/KClass;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "state",
        "name",
        "",
        "(Lkotlin/reflect/KClass;Ljava/lang/Object;Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 327
    invoke-direct {p0}, Lcom/squareup/workflow/legacy/WorkflowPool$Companion;-><init>()V

    return-void
.end method

.method public static synthetic handle$default(Lcom/squareup/workflow/legacy/WorkflowPool$Companion;Lkotlin/reflect/KClass;Ljava/lang/Object;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1

    const/4 p0, 0x4

    and-int/2addr p4, p0

    if-eqz p4, :cond_0

    const-string p3, ""

    :cond_0
    const-string p4, "launcherType"

    .line 334
    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "name"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 425
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-string p4, "S"

    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p4, Ljava/lang/Object;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p4

    const-string p5, "E"

    invoke-static {p0, p5}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p5, Ljava/lang/Object;

    invoke-static {p5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p5

    const-string v0, "O"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p0, Ljava/lang/Object;

    invoke-static {p0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p0

    invoke-direct {p1, p4, p5, p0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 424
    invoke-virtual {p1, p3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p0

    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p1, p0, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p1
.end method

.method public static synthetic handle$default(Lcom/squareup/workflow/legacy/WorkflowPool$Companion;Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 2

    and-int/lit8 p0, p3, 0x2

    if-eqz p0, :cond_0

    const-string p2, ""

    :cond_0
    const-string p0, "launcherType"

    .line 342
    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "name"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 428
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 430
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class p3, Lkotlin/Unit;

    invoke-static {p3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p3

    const/4 p4, 0x4

    const-string v0, "E"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v1, "O"

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p4, Ljava/lang/Object;

    invoke-static {p4}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p4

    invoke-direct {p1, p3, v0, p4}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 429
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance p2, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p2, p1, p0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p2
.end method


# virtual methods
.method public final synthetic handle(Lkotlin/reflect/KClass;Ljava/lang/Object;Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;-TE;+TO;>;>;TS;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "launcherType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "name"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 423
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v0, 0x4

    const-string v1, "S"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, "E"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "O"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {p1, v1, v2, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 335
    invoke-virtual {p1, p3}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance p3, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p3, p1, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object p3
.end method

.method public final synthetic handle(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lkotlin/Unit;",
            "-TE;+TO;>;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lkotlin/Unit;",
            "TE;TO;>;"
        }
    .end annotation

    const-string v0, "launcherType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "name"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 343
    move-object p1, p0

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 427
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v1, Lkotlin/Unit;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "E"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Ljava/lang/Object;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "O"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 426
    invoke-virtual {v0, p2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p2

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v0, p2, p1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v0
.end method
