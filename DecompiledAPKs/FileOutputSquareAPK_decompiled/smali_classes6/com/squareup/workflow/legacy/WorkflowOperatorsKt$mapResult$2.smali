.class public final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;
.super Ljava/lang/Object;
.source "WorkflowOperators.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Workflow;
.implements Lkotlinx/coroutines/Deferred;
.implements Lcom/squareup/workflow/legacy/WorkflowInput;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapResult(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Workflow<",
        "TS;TE;TO2;>;",
        "Lkotlinx/coroutines/Deferred<",
        "TO2;>;",
        "Lcom/squareup/workflow/legacy/WorkflowInput<",
        "TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0097\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00012\u0008\u0012\u0004\u0012\u00028\u00020\u00022\u0008\u0012\u0004\u0012\u00028\u00010\u0003J\u0011\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0097\u0001J\u0011\u0010\u001e\u001a\u00028\u0002H\u0096A\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001fJ\t\u0010 \u001a\u00020!H\u0097\u0001J\u0013\u0010 \u001a\u00020\n2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0097\u0001J\u0019\u0010 \u001a\u00020!2\u000e\u0010\"\u001a\n\u0018\u00010$j\u0004\u0018\u0001`%H\u0096\u0001J6\u0010&\u001a\u0002H\'\"\u0004\u0008\u0003\u0010\'2\u0006\u0010(\u001a\u0002H\'2\u0018\u0010)\u001a\u0014\u0012\u0004\u0012\u0002H\'\u0012\u0004\u0012\u00020+\u0012\u0004\u0012\u0002H\'0*H\u0096\u0001\u00a2\u0006\u0002\u0010,J(\u0010-\u001a\u0004\u0018\u0001H.\"\u0008\u0008\u0003\u0010.*\u00020+2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u0002H.0\u000fH\u0096\u0003\u00a2\u0006\u0002\u0010/J\r\u00100\u001a\u00060$j\u0002`%H\u0097\u0001J\u000e\u00101\u001a\u00028\u0002H\u0097\u0001\u00a2\u0006\u0002\u00102J\u000b\u00103\u001a\u0004\u0018\u00010#H\u0097\u0001JB\u00104\u001a\u0002052\u0006\u00106\u001a\u00020\n2\u0006\u00107\u001a\u00020\n2\'\u00108\u001a#\u0012\u0015\u0012\u0013\u0018\u00010#\u00a2\u0006\u000c\u0008:\u0012\u0008\u0008;\u0012\u0004\u0008\u0008(\"\u0012\u0004\u0012\u00020!09j\u0002`<H\u0097\u0001J2\u00104\u001a\u0002052\'\u00108\u001a#\u0012\u0015\u0012\u0013\u0018\u00010#\u00a2\u0006\u000c\u0008:\u0012\u0008\u0008;\u0012\u0004\u0008\u0008(\"\u0012\u0004\u0012\u00020!09j\u0002`<H\u0096\u0001J\u0011\u0010=\u001a\u00020!H\u0096A\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001fJ\u0015\u0010>\u001a\u00020?2\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u000fH\u0096\u0001J\u000e\u0010@\u001a\u0008\u0012\u0004\u0012\u00028\u00000AH\u0016J\u0011\u0010B\u001a\u00020?2\u0006\u0010C\u001a\u00020?H\u0096\u0003J\u0011\u0010B\u001a\u00020\u00062\u0006\u0010D\u001a\u00020\u0006H\u0097\u0003J\u0016\u0010E\u001a\u00020!2\u0006\u0010F\u001a\u00028\u0001H\u0096\u0001\u00a2\u0006\u0002\u0010GJ\t\u0010H\u001a\u00020\nH\u0096\u0001R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0012\u0010\t\u001a\u00020\nX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u000bR\u0012\u0010\u000c\u001a\u00020\nX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000bR\u0012\u0010\r\u001a\u00020\nX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000bR\u0016\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u000fX\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011R\u0018\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u0013X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u0012\u0010\u0016\u001a\u00020\u0017X\u0096\u0005\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006I"
    }
    d2 = {
        "com/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lkotlinx/coroutines/Deferred;",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "children",
        "Lkotlin/sequences/Sequence;",
        "Lkotlinx/coroutines/Job;",
        "getChildren",
        "()Lkotlin/sequences/Sequence;",
        "isActive",
        "",
        "()Z",
        "isCancelled",
        "isCompleted",
        "key",
        "Lkotlin/coroutines/CoroutineContext$Key;",
        "getKey",
        "()Lkotlin/coroutines/CoroutineContext$Key;",
        "onAwait",
        "Lkotlinx/coroutines/selects/SelectClause1;",
        "getOnAwait",
        "()Lkotlinx/coroutines/selects/SelectClause1;",
        "onJoin",
        "Lkotlinx/coroutines/selects/SelectClause0;",
        "getOnJoin",
        "()Lkotlinx/coroutines/selects/SelectClause0;",
        "attachChild",
        "Lkotlinx/coroutines/ChildHandle;",
        "child",
        "Lkotlinx/coroutines/ChildJob;",
        "await",
        "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "cancel",
        "",
        "cause",
        "",
        "Ljava/util/concurrent/CancellationException;",
        "Lkotlinx/coroutines/CancellationException;",
        "fold",
        "R",
        "initial",
        "operation",
        "Lkotlin/Function2;",
        "Lkotlin/coroutines/CoroutineContext$Element;",
        "(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;",
        "get",
        "E",
        "(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;",
        "getCancellationException",
        "getCompleted",
        "()Ljava/lang/Object;",
        "getCompletionExceptionOrNull",
        "invokeOnCompletion",
        "Lkotlinx/coroutines/DisposableHandle;",
        "onCancelling",
        "invokeImmediately",
        "handler",
        "Lkotlin/Function1;",
        "Lkotlin/ParameterName;",
        "name",
        "Lkotlinx/coroutines/CompletionHandler;",
        "join",
        "minusKey",
        "Lkotlin/coroutines/CoroutineContext;",
        "openSubscriptionToState",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "plus",
        "context",
        "other",
        "sendEvent",
        "event",
        "(Ljava/lang/Object;)V",
        "start",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lkotlinx/coroutines/Deferred;

.field private final synthetic $$delegate_1:Lcom/squareup/workflow/legacy/Workflow;

.field final synthetic $this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

.field final synthetic $transformedResult:Lkotlinx/coroutines/Deferred;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/Deferred;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;-TE;+TO1;>;",
            "Lkotlinx/coroutines/Deferred;",
            ")V"
        }
    .end annotation

    .line 145
    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$transformedResult:Lkotlinx/coroutines/Deferred;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    .line 147
    iget-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_1:Lcom/squareup/workflow/legacy/Workflow;

    return-void
.end method


# virtual methods
.method public attachChild(Lkotlinx/coroutines/ChildJob;)Lkotlinx/coroutines/ChildHandle;
    .locals 1

    const-string v0, "child"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->attachChild(Lkotlinx/coroutines/ChildJob;)Lkotlinx/coroutines/ChildHandle;

    move-result-object p1

    return-object p1
.end method

.method public await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-TO2;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->await(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    const-string v0, "await(...)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public synthetic cancel()V
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "Since 1.2.0, binary compatibility with versions <= 1.1.x"
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->cancel()V

    return-void
.end method

.method public cancel(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->cancel(Ljava/util/concurrent/CancellationException;)V

    return-void
.end method

.method public synthetic cancel(Ljava/lang/Throwable;)Z
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->HIDDEN:Lkotlin/DeprecationLevel;
        message = "Since 1.2.0, binary compatibility with versions <= 1.1.x"
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->cancel(Ljava/lang/Throwable;)Z

    move-result p1

    return p1
.end method

.method public fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(TR;",
            "Lkotlin/jvm/functions/Function2<",
            "-TR;-",
            "Lkotlin/coroutines/CoroutineContext$Element;",
            "+TR;>;)TR;"
        }
    .end annotation

    const-string v0, "operation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1, p2}, Lkotlinx/coroutines/Deferred;->fold(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lkotlin/coroutines/CoroutineContext$Element;",
            ">(",
            "Lkotlin/coroutines/CoroutineContext$Key<",
            "TE;>;)TE;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object p1

    return-object p1
.end method

.method public getCancellationException()Ljava/util/concurrent/CancellationException;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getCancellationException()Ljava/util/concurrent/CancellationException;

    move-result-object v0

    return-object v0
.end method

.method public getChildren()Lkotlin/sequences/Sequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/sequences/Sequence<",
            "Lkotlinx/coroutines/Job;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getChildren()Lkotlin/sequences/Sequence;

    move-result-object v0

    return-object v0
.end method

.method public getCompleted()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TO2;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getCompleted()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "getCompleted(...)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getCompletionExceptionOrNull()Ljava/lang/Throwable;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getCompletionExceptionOrNull()Ljava/lang/Throwable;

    move-result-object v0

    return-object v0
.end method

.method public getKey()Lkotlin/coroutines/CoroutineContext$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/coroutines/CoroutineContext$Key<",
            "*>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getKey()Lkotlin/coroutines/CoroutineContext$Key;

    move-result-object v0

    return-object v0
.end method

.method public getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/selects/SelectClause1<",
            "TO2;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getOnAwait()Lkotlinx/coroutines/selects/SelectClause1;

    move-result-object v0

    return-object v0
.end method

.method public getOnJoin()Lkotlinx/coroutines/selects/SelectClause0;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->getOnJoin()Lkotlinx/coroutines/selects/SelectClause0;

    move-result-object v0

    return-object v0
.end method

.method public invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlinx/coroutines/DisposableHandle;"
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    move-result-object p1

    return-object p1
.end method

.method public invokeOnCompletion(ZZLkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Throwable;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlinx/coroutines/DisposableHandle;"
        }
    .end annotation

    const-string v0, "handler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1, p2, p3}, Lkotlinx/coroutines/Deferred;->invokeOnCompletion(ZZLkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    move-result-object p1

    return-object p1
.end method

.method public isActive()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->isActive()Z

    move-result v0

    return v0
.end method

.method public isCancelled()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public isCompleted()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->isCompleted()Z

    move-result v0

    return v0
.end method

.method public join(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->join(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public minusKey(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/CoroutineContext$Key<",
            "*>;)",
            "Lkotlin/coroutines/CoroutineContext;"
        }
    .end annotation

    const-string v0, "key"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->minusKey(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    return-object p1
.end method

.method public openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "TS;>;"
        }
    .end annotation

    .line 149
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$this_mapResult:Lcom/squareup/workflow/legacy/Workflow;

    invoke-interface {v0}, Lcom/squareup/workflow/legacy/Workflow;->openSubscriptionToState()Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object v0

    return-object v0
.end method

.method public plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p1

    return-object p1
.end method

.method public plus(Lkotlinx/coroutines/Job;)Lkotlinx/coroutines/Job;
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        level = .enum Lkotlin/DeprecationLevel;->ERROR:Lkotlin/DeprecationLevel;
        message = "Operator \'+\' on two Job objects is meaningless. Job is a coroutine context element and `+` is a set-sum operator for coroutine contexts. The job to the right of `+` just replaces the job the left of `+`."
    .end annotation

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/Deferred;->plus(Lkotlinx/coroutines/Job;)Lkotlinx/coroutines/Job;

    move-result-object p1

    return-object p1
.end method

.method public sendEvent(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_1:Lcom/squareup/workflow/legacy/Workflow;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method public start()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;->$$delegate_0:Lkotlinx/coroutines/Deferred;

    invoke-interface {v0}, Lkotlinx/coroutines/Deferred;->start()Z

    move-result v0

    return v0
.end method
