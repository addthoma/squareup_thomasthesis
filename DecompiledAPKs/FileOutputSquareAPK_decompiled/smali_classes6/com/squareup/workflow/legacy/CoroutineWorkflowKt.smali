.class public final Lcom/squareup/workflow/legacy/CoroutineWorkflowKt;
.super Ljava/lang/Object;
.source "CoroutineWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u00ad\u0001\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082]\u0010\t\u001aY\u0008\u0001\u0012\u0004\u0012\u00020\u0006\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u0002H\u00020\u000b\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(\u000e\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u0002H\u00030\u000f\u00a2\u0006\u000c\u0008\u000c\u0012\u0008\u0008\r\u0012\u0004\u0008\u0008(\u0010\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00040\u0011\u0012\u0006\u0012\u0004\u0018\u00010\u00050\n\u00a2\u0006\u0002\u0008\u0012H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0014"
    }
    d2 = {
        "workflow",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "S",
        "E",
        "O",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "context",
        "Lkotlin/coroutines/CoroutineContext;",
        "block",
        "Lkotlin/Function4;",
        "Lkotlinx/coroutines/channels/SendChannel;",
        "Lkotlin/ParameterName;",
        "name",
        "state",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "events",
        "Lkotlin/coroutines/Continuation;",
        "Lkotlin/ExtensionFunctionType;",
        "(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function4;)Lcom/squareup/workflow/legacy/Workflow;",
        "legacy-workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final workflow(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function4;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/jvm/functions/Function4<",
            "-",
            "Lkotlinx/coroutines/CoroutineScope;",
            "-",
            "Lkotlinx/coroutines/channels/SendChannel<",
            "-TS;>;-",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TE;>;-",
            "Lkotlin/coroutines/Continuation<",
            "-TO;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const v0, 0x7fffffff

    .line 58
    invoke-static {v0}, Lkotlinx/coroutines/channels/ChannelKt;->Channel(I)Lkotlinx/coroutines/channels/Channel;

    move-result-object v0

    const/4 v1, -0x1

    .line 59
    invoke-static {v1}, Lkotlinx/coroutines/channels/BroadcastChannelKt;->BroadcastChannel(I)Lkotlinx/coroutines/channels/BroadcastChannel;

    move-result-object v1

    .line 60
    new-instance v2, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$result$1;

    const/4 v3, 0x0

    invoke-direct {v2, p2, v1, v0, v3}, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$result$1;-><init>(Lkotlin/jvm/functions/Function4;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlinx/coroutines/channels/Channel;Lkotlin/coroutines/Continuation;)V

    move-object v7, v2

    check-cast v7, Lkotlin/jvm/functions/Function2;

    const/4 v6, 0x0

    const/4 v8, 0x2

    const/4 v9, 0x0

    move-object v4, p0

    move-object v5, p1

    invoke-static/range {v4 .. v9}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p0

    .line 64
    new-instance p2, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;

    invoke-direct {p2, v1, v0}, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$1;-><init>(Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlinx/coroutines/channels/Channel;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p0, p2}, Lkotlinx/coroutines/Deferred;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    .line 70
    new-instance p2, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$2;

    invoke-direct {p2, v1, v0, p1, p0}, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt$workflow$2;-><init>(Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlinx/coroutines/channels/Channel;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/Deferred;)V

    check-cast p2, Lcom/squareup/workflow/legacy/Workflow;

    return-object p2
.end method

.method public static synthetic workflow$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function4;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    .line 50
    sget-object p1, Lkotlin/coroutines/EmptyCoroutineContext;->INSTANCE:Lkotlin/coroutines/EmptyCoroutineContext;

    check-cast p1, Lkotlin/coroutines/CoroutineContext;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/legacy/CoroutineWorkflowKt;->workflow(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function4;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    return-object p0
.end method
