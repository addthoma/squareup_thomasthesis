.class public final Lcom/squareup/workflow/legacy/Reactor$DefaultImpls;
.super Ljava/lang/Object;
.source "Reactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/Reactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static launch(Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Reactor<",
            "TS;TE;+TO;>;TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 210
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/legacy/ReactorKt;->doLaunch$default(Lcom/squareup/workflow/legacy/Reactor;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/CoroutineContext;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p0

    return-object p0
.end method
