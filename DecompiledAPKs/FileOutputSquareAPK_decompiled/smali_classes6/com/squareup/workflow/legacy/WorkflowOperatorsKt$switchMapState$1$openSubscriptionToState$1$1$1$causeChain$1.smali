.class final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowOperators.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0006*\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "S1",
        "",
        "S2",
        "E",
        "O",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;-><init>()V

    sput-object v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;->INSTANCE:Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 82
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1$openSubscriptionToState$1$1$1$causeChain$1;->invoke(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    return-object p1
.end method
