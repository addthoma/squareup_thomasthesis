.class public final Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;
.super Ljava/lang/Object;
.source "WorkflowOperators.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001al\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00060\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0007\"\u0008\u0008\u0001\u0010\u0005*\u00020\u0007\"\u0008\u0008\u0002\u0010\u0008*\u00020\u0007\"\u0008\u0008\u0003\u0010\u0006*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0008\u0012\u0004\u0012\u0002H\u00060\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u00080\nH\u0007\u001a\u0084\u0001\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\r0\u0003\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0007\"\u0008\u0008\u0001\u0010\u000c*\u00020\u0007\"\u0008\u0008\u0002\u0010\u000e*\u00020\u0007\"\u0008\u0008\u0003\u0010\r*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u000e0\u00032\"\u0010\t\u001a\u001e\u0008\u0001\u0012\u0004\u0012\u0002H\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\r0\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u000fH\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011\u001a\u0084\u0001\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u0002H\u0013\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00060\u0003\"\u0008\u0008\u0000\u0010\u0014*\u00020\u0007\"\u0008\u0008\u0001\u0010\u0013*\u00020\u0007\"\u0008\u0008\u0002\u0010\u000c*\u00020\u0007\"\u0008\u0008\u0003\u0010\u0006*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00060\u00032\"\u0010\t\u001a\u001e\u0008\u0001\u0012\u0004\u0012\u0002H\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00130\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u000fH\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0011\u001a\u0095\u0001\u0010\u0015\u001a\u0014\u0012\u0004\u0012\u0002H\u0013\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00060\u0003\"\u0008\u0008\u0000\u0010\u0014*\u00020\u0007\"\u0008\u0008\u0001\u0010\u0013*\u00020\u0007\"\u0008\u0008\u0002\u0010\u000c*\u00020\u0007\"\u0008\u0008\u0003\u0010\u0006*\u00020\u0007*\u0014\u0012\u0004\u0012\u0002H\u0014\u0012\u0004\u0012\u0002H\u000c\u0012\u0004\u0012\u0002H\u00060\u000323\u0010\t\u001a/\u0008\u0001\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u0002H\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00130\u00180\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0016\u00a2\u0006\u0002\u0008\u0019H\u0007\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u001a\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001b"
    }
    d2 = {
        "operatorContext",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "adaptEvents",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "S",
        "E2",
        "O",
        "",
        "E1",
        "transform",
        "Lkotlin/Function1;",
        "mapResult",
        "E",
        "O2",
        "O1",
        "Lkotlin/Function2;",
        "Lkotlin/coroutines/Continuation;",
        "(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;",
        "mapState",
        "S2",
        "S1",
        "switchMapState",
        "Lkotlin/Function3;",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "Lkotlin/ExtensionFunctionType;",
        "(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function3;)Lcom/squareup/workflow/legacy/Workflow;",
        "legacy-workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final operatorContext:Lkotlinx/coroutines/CoroutineDispatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 40
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    sput-object v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->operatorContext:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method

.method public static final synthetic access$getOperatorContext$p()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->operatorContext:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public static final adaptEvents(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E2:",
            "Ljava/lang/Object;",
            "E1:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;-TE1;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TE2;+TE1;>;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE2;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$adaptEvents"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$adaptEvents$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$adaptEvents$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Workflow;

    return-object v0
.end method

.method public static final mapResult(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O1:",
            "Ljava/lang/Object;",
            "O2:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS;-TE;+TO1;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TO1;-",
            "Lkotlin/coroutines/Continuation<",
            "-TO2;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO2;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$mapResult"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->INSTANCE:Lkotlinx/coroutines/GlobalScope;

    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->operatorContext:Lkotlinx/coroutines/CoroutineDispatcher;

    move-object v2, v0

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;

    const/4 v3, 0x0

    invoke-direct {v0, p0, p1, v3}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$transformedResult$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function2;

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p1

    .line 134
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p1, v0}, Lkotlinx/coroutines/Deferred;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    .line 145
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapResult$2;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlinx/coroutines/Deferred;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Workflow;

    return-object v0
.end method

.method public static final mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S1:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS1;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TS1;-",
            "Lkotlin/coroutines/Continuation<",
            "-TS2;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS2;TE;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$mapState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapState$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$mapState$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Workflow;

    return-object v0
.end method

.method public static final switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function3;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S1:",
            "Ljava/lang/Object;",
            "S2:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "+TS1;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Lkotlinx/coroutines/CoroutineScope;",
            "-TS1;-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TS2;>;>;+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS2;TE;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$switchMapState"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt$switchMapState$1;-><init>(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Lcom/squareup/workflow/legacy/Workflow;

    return-object v0
.end method
