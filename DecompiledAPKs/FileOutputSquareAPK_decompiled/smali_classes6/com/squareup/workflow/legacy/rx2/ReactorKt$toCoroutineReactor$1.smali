.class public final Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;
.super Ljava/lang/Object;
.source "Reactor.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Reactor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/rx2/ReactorKt;->toCoroutineReactor(Lcom/squareup/workflow/legacy/rx2/Reactor;)Lcom/squareup/workflow/legacy/Reactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Reactor<",
        "TS;TE;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00001\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J/\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a2\u0006\u0002\u0010\u0007J;\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\t2\u0006\u0010\n\u001a\u00028\u00002\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u000c2\u0006\u0010\u0005\u001a\u00020\u0006H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u0008\u0010\u000e\u001a\u00020\u000fH\u0016\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u0010"
    }
    d2 = {
        "com/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1",
        "Lcom/squareup/workflow/legacy/Reactor;",
        "launch",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "initialState",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;",
        "onReact",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "state",
        "events",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "(Ljava/lang/Object;Lkotlinx/coroutines/channels/ReceiveChannel;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "toString",
        "",
        "legacy-workflow-rx2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toCoroutineReactor:Lcom/squareup/workflow/legacy/rx2/Reactor;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/rx2/Reactor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/Reactor<",
            "TS;TE;+TO;>;)V"
        }
    .end annotation

    .line 252
    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;->$this_toCoroutineReactor:Lcom/squareup/workflow/legacy/rx2/Reactor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "initialState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;->$this_toCoroutineReactor:Lcom/squareup/workflow/legacy/rx2/Reactor;

    invoke-interface {v0, p1, p2}, Lcom/squareup/workflow/legacy/rx2/Reactor;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method public onReact(Ljava/lang/Object;Lkotlinx/coroutines/channels/ReceiveChannel;Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TE;>;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "+TS;+TO;>;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p4, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;

    iget v1, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p4, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    sub-int/2addr p4, v2

    iput p4, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;

    invoke-direct {v0, p0, p4}, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;-><init>(Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p4, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 253
    iget v2, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$3:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/workflow/legacy/WorkflowPool;

    iget-object p1, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$2:Ljava/lang/Object;

    check-cast p1, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object p1, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$1:Ljava/lang/Object;

    iget-object p1, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$0:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;

    invoke-static {p4}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-static {p4}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 257
    iget-object p4, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;->$this_toCoroutineReactor:Lcom/squareup/workflow/legacy/rx2/Reactor;

    invoke-static {p2}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt;->asEventChannel(Lkotlinx/coroutines/channels/ReceiveChannel;)Lcom/squareup/workflow/legacy/rx2/EventChannel;

    move-result-object v2

    invoke-interface {p4, p1, v2, p3}, Lcom/squareup/workflow/legacy/rx2/Reactor;->onReact(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;

    move-result-object p4

    check-cast p4, Lio/reactivex/SingleSource;

    iput-object p0, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$1:Ljava/lang/Object;

    iput-object p2, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$2:Ljava/lang/Object;

    iput-object p3, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->L$3:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1$onReact$1;->label:I

    .line 258
    invoke-static {p4, v0}, Lkotlinx/coroutines/rx2/RxAwaitKt;->await(Lio/reactivex/SingleSource;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p4

    if-ne p4, v1, :cond_3

    return-object v1

    :cond_3
    :goto_1
    const-string p1, "this@toCoroutineReactor.\u2026rkflows)\n        .await()"

    .line 253
    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p4
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "rx2.Reactor("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/ReactorKt$toCoroutineReactor$1;->$this_toCoroutineReactor:Lcom/squareup/workflow/legacy/rx2/Reactor;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
