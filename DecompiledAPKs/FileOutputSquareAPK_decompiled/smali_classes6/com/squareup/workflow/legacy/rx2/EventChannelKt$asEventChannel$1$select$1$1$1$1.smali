.class final Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EventChannel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase<",
        "TE;*TR;>;",
        "Lkotlin/jvm/functions/Function0<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n\u0012\u0004\u0012\u0002H\u0002\u0018\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00032\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u0002H\u0004\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u0002H\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lkotlin/Function0;",
        "R",
        "",
        "E",
        "it",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1$1;->$event:Ljava/lang/Object;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 66
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;)Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase<",
            "TE;*TR;>;)",
            "Lkotlin/jvm/functions/Function0<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1$1$1$1;->$event:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;->tryHandle(Ljava/lang/Object;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    return-object p1
.end method
