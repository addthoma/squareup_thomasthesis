.class public final Lcom/squareup/workflow/internal/WorkerChildNode;
.super Ljava/lang/Object;
.source "WorkerChildNode.kt"

# interfaces
.implements Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "StateT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode<",
        "Lcom/squareup/workflow/internal/WorkerChildNode<",
        "***>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0000\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u0002*\u0008\u0008\u0002\u0010\u0003*\u00020\u00042\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00000\u0005BW\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0010\u0010\n\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000c0\u000b\u0012\u0008\u0008\u0002\u0010\r\u001a\u00020\u000e\u0012\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00110\u0010\u00a2\u0006\u0002\u0010\u0012J\u001c\u0010\"\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00112\u0008\u0010#\u001a\u0004\u0018\u00010\u0004J\u001a\u0010$\u001a\u00020\u000e2\n\u0010%\u001a\u0006\u0012\u0002\u0008\u00030\u00072\u0006\u0010\u0008\u001a\u00020\tJ<\u0010&\u001a\u00020\'\"\u0004\u0008\u0003\u0010(\"\u0004\u0008\u0004\u0010)\"\u0008\u0008\u0005\u0010**\u00020\u00042\u001e\u0010+\u001a\u001a\u0012\u0004\u0012\u0002H(\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H)\u0012\u0004\u0012\u0002H*0\u00110\u0010R\u001b\u0010\n\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000c0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R&\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u00028\u0000\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u00110\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R(\u0010\u0017\u001a\u0010\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0000X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\"\u0004\u0008\u001a\u0010\u001bR\u001a\u0010\r\u001a\u00020\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001c\u0010\u001d\"\u0004\u0008\u001e\u0010\u001fR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010!\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/WorkerChildNode;",
        "T",
        "StateT",
        "OutputT",
        "",
        "Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;",
        "worker",
        "Lcom/squareup/workflow/Worker;",
        "key",
        "",
        "channel",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "Lcom/squareup/workflow/internal/ValueOrDone;",
        "tombstone",
        "",
        "handler",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/jvm/functions/Function1;)V",
        "getChannel",
        "()Lkotlinx/coroutines/channels/ReceiveChannel;",
        "getKey",
        "()Ljava/lang/String;",
        "nextListNode",
        "getNextListNode",
        "()Lcom/squareup/workflow/internal/WorkerChildNode;",
        "setNextListNode",
        "(Lcom/squareup/workflow/internal/WorkerChildNode;)V",
        "getTombstone",
        "()Z",
        "setTombstone",
        "(Z)V",
        "getWorker",
        "()Lcom/squareup/workflow/Worker;",
        "acceptUpdate",
        "value",
        "matches",
        "otherWorker",
        "setHandler",
        "",
        "T2",
        "S",
        "O",
        "newHandler",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final channel:Lkotlinx/coroutines/channels/ReceiveChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "Lcom/squareup/workflow/internal/ValueOrDone<",
            "*>;>;"
        }
    .end annotation
.end field

.field private handler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/String;

.field private nextListNode:Lcom/squareup/workflow/internal/WorkerChildNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/internal/WorkerChildNode<",
            "***>;"
        }
    .end annotation
.end field

.field private tombstone:Z

.field private final worker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "+TT;>;",
            "Ljava/lang/String;",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+",
            "Lcom/squareup/workflow/internal/ValueOrDone<",
            "*>;>;Z",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)V"
        }
    .end annotation

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "channel"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->worker:Lcom/squareup/workflow/Worker;

    iput-object p2, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->key:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->channel:Lkotlinx/coroutines/channels/ReceiveChannel;

    iput-boolean p4, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->tombstone:Z

    iput-object p5, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->handler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x8

    if-eqz p6, :cond_0

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p4

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    .line 41
    invoke-direct/range {v0 .. v5}, Lcom/squareup/workflow/internal/WorkerChildNode;-><init>(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public final acceptUpdate(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->handler:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    return-object p1
.end method

.method public final getChannel()Lkotlinx/coroutines/channels/ReceiveChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "Lcom/squareup/workflow/internal/ValueOrDone<",
            "*>;>;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->channel:Lkotlinx/coroutines/channels/ReceiveChannel;

    return-object v0
.end method

.method public final getKey()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->key:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getNextListNode()Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/squareup/workflow/internal/WorkerChildNode;->getNextListNode()Lcom/squareup/workflow/internal/WorkerChildNode;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;

    return-object v0
.end method

.method public getNextListNode()Lcom/squareup/workflow/internal/WorkerChildNode;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/internal/WorkerChildNode<",
            "***>;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->nextListNode:Lcom/squareup/workflow/internal/WorkerChildNode;

    return-object v0
.end method

.method public final getTombstone()Z
    .locals 1

    .line 41
    iget-boolean v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->tombstone:Z

    return v0
.end method

.method public final getWorker()Lcom/squareup/workflow/Worker;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Worker<",
            "TT;>;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->worker:Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public final matches(Lcom/squareup/workflow/Worker;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->worker:Lcom/squareup/workflow/Worker;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Worker;->doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->key:Ljava/lang/String;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final setHandler(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T2:",
            "Ljava/lang/Object;",
            "S:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TT2;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TS;+TO;>;>;)V"
        }
    .end annotation

    const-string v0, "newHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 52
    invoke-static {p1, v0}, Lkotlin/jvm/internal/TypeIntrinsics;->beforeCheckcastToFunctionOfArity(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->handler:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public bridge synthetic setNextListNode(Lcom/squareup/workflow/internal/InlineLinkedList$InlineListNode;)V
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/workflow/internal/WorkerChildNode;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/internal/WorkerChildNode;->setNextListNode(Lcom/squareup/workflow/internal/WorkerChildNode;)V

    return-void
.end method

.method public setNextListNode(Lcom/squareup/workflow/internal/WorkerChildNode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/internal/WorkerChildNode<",
            "***>;)V"
        }
    .end annotation

    .line 45
    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->nextListNode:Lcom/squareup/workflow/internal/WorkerChildNode;

    return-void
.end method

.method public final setTombstone(Z)V
    .locals 0

    .line 41
    iput-boolean p1, p0, Lcom/squareup/workflow/internal/WorkerChildNode;->tombstone:Z

    return-void
.end method
