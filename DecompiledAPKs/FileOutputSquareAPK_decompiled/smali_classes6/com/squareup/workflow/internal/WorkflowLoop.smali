.class public interface abstract Lcom/squareup/workflow/internal/WorkflowLoop;
.super Ljava/lang/Object;
.source "WorkflowLoop.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/internal/WorkflowLoop$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0001\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008`\u0018\u00002\u00020\u0001J\u00d5\u0001\u0010\u0002\u001a\u00020\u0003\"\u0004\u0008\u0000\u0010\u0004\"\u0004\u0008\u0001\u0010\u0005\"\u0008\u0008\u0002\u0010\u0006*\u00020\u0001\"\u0004\u0008\u0003\u0010\u00072\u001e\u0010\u0008\u001a\u001a\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\t2\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u0001H\u00052\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u00102(\u0010\u0011\u001a$\u0008\u0001\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00070\u0013\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00122\"\u0010\u0016\u001a\u001e\u0008\u0001\u0012\u0004\u0012\u0002H\u0006\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00150\u0014\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u00122\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0019\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/WorkflowLoop;",
        "",
        "runWorkflowLoop",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "RenderingT",
        "workflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "props",
        "Lkotlinx/coroutines/flow/Flow;",
        "initialSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "initialState",
        "workerContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "onRendering",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "Lkotlin/coroutines/Continuation;",
        "",
        "onOutput",
        "diagnosticListener",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "(Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract runWorkflowLoop(Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "-TPropsT;TStateT;+TOutputT;+TRenderingT;>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TPropsT;>;",
            "Lcom/squareup/workflow/Snapshot;",
            "TStateT;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "+TRenderingT;>;-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;",
            "Lkotlin/jvm/functions/Function2<",
            "-TOutputT;-",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;+",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation
.end method
