.class final Lcom/squareup/workflow/internal/WorkflowNode$2;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowNode.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/WorkflowNode;-><init>(Lcom/squareup/workflow/internal/WorkflowId;Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/Object;Lokio/ByteString;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function1;Ljava/lang/Long;Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;Lcom/squareup/workflow/diagnostic/IdCounter;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u00062\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/internal/WorkflowNode;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/internal/WorkflowNode;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/internal/WorkflowNode$2;->this$0:Lcom/squareup/workflow/internal/WorkflowNode;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/internal/WorkflowNode$2;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 2

    .line 110
    iget-object p1, p0, Lcom/squareup/workflow/internal/WorkflowNode$2;->this$0:Lcom/squareup/workflow/internal/WorkflowNode;

    invoke-static {p1}, Lcom/squareup/workflow/internal/WorkflowNode;->access$getDiagnosticListener$p(Lcom/squareup/workflow/internal/WorkflowNode;)Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode$2;->this$0:Lcom/squareup/workflow/internal/WorkflowNode;

    invoke-virtual {v0}, Lcom/squareup/workflow/internal/WorkflowNode;->getDiagnosticId$workflow_runtime()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkflowStopped(J)V

    return-void
.end method
