.class public final Lcom/squareup/workflow/rx2/RunOutputsKt;
.super Ljava/lang/Object;
.source "RunOutputs.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\u001a[\u0010\u0000\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0002*\u00020\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u00062\u0006\u0010\u0007\u001a\u0002H\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000c\u001a\\\u0010\u0000\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0002*\u00020\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u00062\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u000e2\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000b\u001aD\u0010\u0000\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00042\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u00062\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000b\u00a8\u0006\u0010"
    }
    d2 = {
        "runOutputs",
        "Lio/reactivex/flowables/ConnectableFlowable;",
        "OutputT",
        "InputT",
        "",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "context",
        "Lkotlin/coroutines/CoroutineContext;",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;",
        "inputs",
        "Lkotlinx/coroutines/flow/Flow;",
        "",
        "runtime-extensions"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final runOutputs(Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-",
            "Lkotlin/Unit;",
            "+TOutputT;*>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")",
            "Lio/reactivex/flowables/ConnectableFlowable<",
            "+TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p0, v0, p1, p2}, Lcom/squareup/workflow/rx2/RunOutputsKt;->runOutputs(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;

    move-result-object p0

    return-object p0
.end method

.method public static final runOutputs(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TInputT;+TOutputT;*>;TInputT;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")",
            "Lio/reactivex/flowables/ConnectableFlowable<",
            "+TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-static {p1}, Lkotlinx/coroutines/flow/FlowKt;->flowOf(Ljava/lang/Object;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/workflow/rx2/RunOutputsKt;->runOutputs(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;

    move-result-object p0

    return-object p0
.end method

.method public static final runOutputs(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TInputT;+TOutputT;*>;",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TInputT;>;",
            "Lcom/squareup/workflow/Snapshot;",
            "Lkotlin/coroutines/CoroutineContext;",
            ")",
            "Lio/reactivex/flowables/ConnectableFlowable<",
            "+TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "inputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;

    invoke-direct {v0, p1, p0, p2, p3}, Lcom/squareup/workflow/rx2/RunOutputsKt$runOutputs$1;-><init>(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Flowable;->defer(Ljava/util/concurrent/Callable;)Lio/reactivex/Flowable;

    move-result-object p0

    .line 43
    invoke-virtual {p0}, Lio/reactivex/Flowable;->publish()Lio/reactivex/flowables/ConnectableFlowable;

    move-result-object p0

    const-string p1, "Flowable\n    .defer {\n  \u2026   }\n    }\n    .publish()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic runOutputs$default(Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;ILjava/lang/Object;)Lio/reactivex/flowables/ConnectableFlowable;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 89
    check-cast p1, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/rx2/RunOutputsKt;->runOutputs(Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic runOutputs$default(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;ILjava/lang/Object;)Lio/reactivex/flowables/ConnectableFlowable;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 66
    check-cast p2, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/workflow/rx2/RunOutputsKt;->runOutputs(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic runOutputs$default(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;ILjava/lang/Object;)Lio/reactivex/flowables/ConnectableFlowable;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 34
    check-cast p2, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/workflow/rx2/RunOutputsKt;->runOutputs(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lio/reactivex/flowables/ConnectableFlowable;

    move-result-object p0

    return-object p0
.end method
