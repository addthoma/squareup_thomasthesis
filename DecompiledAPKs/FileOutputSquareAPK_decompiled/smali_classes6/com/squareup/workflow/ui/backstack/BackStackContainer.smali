.class public Lcom/squareup/workflow/ui/backstack/BackStackContainer;
.super Landroid/widget/FrameLayout;
.source "BackStackContainer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackStackContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackStackContainer.kt\ncom/squareup/workflow/ui/backstack/BackStackContainer\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,167:1\n1499#2,3:168\n*E\n*S KotlinDebug\n*F\n+ 1 BackStackContainer.kt\ncom/squareup/workflow/ui/backstack/BackStackContainer\n*L\n85#1,3:168\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u0000 !2\u00020\u0001:\u0001!B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0014J\u0008\u0010\u0017\u001a\u00020\u0016H\u0014J\"\u0010\u0018\u001a\u00020\u00142\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u001a\u001a\u00020\u000e2\u0006\u0010\u001b\u001a\u00020\u001cH\u0014J\u001c\u0010\u001d\u001a\u00020\u00142\n\u0010\u001e\u001a\u0006\u0012\u0002\u0008\u00030\u000b2\u0006\u0010\u001f\u001a\u00020 H\u0002R\u001a\u0010\n\u001a\u000e\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u000c\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0004\u0018\u00010\u000e8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/workflow/ui/backstack/BackStackContainer;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attributeSet",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "defStyleRes",
        "(Landroid/content/Context;Landroid/util/AttributeSet;II)V",
        "currentRendering",
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen;",
        "Lcom/squareup/workflow/ui/Named;",
        "currentView",
        "Landroid/view/View;",
        "getCurrentView",
        "()Landroid/view/View;",
        "viewStateCache",
        "Lcom/squareup/workflow/ui/backstack/ViewStateCache;",
        "onRestoreInstanceState",
        "",
        "state",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "performTransition",
        "oldViewMaybe",
        "newView",
        "popped",
        "",
        "update",
        "newRendering",
        "newContainerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "backstack-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;


# instance fields
.field private currentRendering:Lcom/squareup/workflow/ui/backstack/BackStackScreen;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "Lcom/squareup/workflow/ui/Named<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->Companion:Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 54
    new-instance p1, Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    invoke-direct {p1}, Lcom/squareup/workflow/ui/backstack/ViewStateCache;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    .line 49
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 51
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/workflow/ui/backstack/BackStackContainer;Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->update(Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method

.method private final getCurrentView()Landroid/view/View;
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final update(Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "*>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")V"
        }
    .end annotation

    .line 63
    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getBackStack()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/ui/backstack/BackStackConfig;->First:Lcom/squareup/workflow/ui/backstack/BackStackConfig;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/workflow/ui/backstack/BackStackConfig;->Other:Lcom/squareup/workflow/ui/backstack/BackStackConfig;

    .line 64
    :goto_0
    sget-object v1, Lcom/squareup/workflow/ui/backstack/BackStackConfig;->Companion:Lcom/squareup/workflow/ui/backstack/BackStackConfig$Companion;

    invoke-static {v1, v0}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/ui/ContainerHints;->plus(Lkotlin/Pair;)Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object p2

    .line 69
    sget-object v0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$update$named$1;->INSTANCE:Lcom/squareup/workflow/ui/backstack/BackStackContainer$update$named$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    move-result-object p1

    .line 71
    invoke-direct {p0}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->getCurrentView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 75
    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getTop()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->canShowRendering(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    .line 77
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getFrames()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v0, v2}, Lcom/squareup/workflow/ui/backstack/ViewStateCache;->prune(Ljava/util/Collection;)V

    .line 78
    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getTop()Ljava/lang/Object;

    move-result-object p1

    invoke-static {v1, p1, p2}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->showRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void

    .line 82
    :cond_2
    sget-object v1, Lcom/squareup/workflow/ui/ViewRegistry;->Companion:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    check-cast v1, Lcom/squareup/workflow/ui/ContainerHintKey;

    invoke-virtual {p2, v1}, Lcom/squareup/workflow/ui/ContainerHints;->get(Lcom/squareup/workflow/ui/ContainerHintKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/ui/ViewRegistry;

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getTop()Ljava/lang/Object;

    move-result-object v2

    move-object v3, p0

    check-cast v3, Landroid/view/ViewGroup;

    invoke-static {v1, v2, p2, v3}, Lcom/squareup/workflow/ui/ViewRegistryKt;->buildView(Lcom/squareup/workflow/ui/ViewRegistry;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 83
    iget-object v1, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getBackStack()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-virtual {v1, v2, v0, p2}, Lcom/squareup/workflow/ui/backstack/ViewStateCache;->update(Ljava/util/Collection;Landroid/view/View;Landroid/view/View;)V

    .line 85
    iget-object v1, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->currentRendering:Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getBackStack()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_6

    check-cast v1, Ljava/lang/Iterable;

    .line 168
    instance-of v4, v1, Ljava/util/Collection;

    if-eqz v4, :cond_4

    move-object v4, v1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 169
    :cond_4
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/workflow/ui/Named;

    .line 85
    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->getTop()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/squareup/workflow/ui/CompatibleKt;->compatible(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v1, 0x1

    :goto_2
    if-ne v1, v2, :cond_6

    goto :goto_3

    :cond_6
    const/4 v2, 0x0

    .line 87
    :goto_3
    invoke-virtual {p0, v0, p2, v2}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->performTransition(Landroid/view/View;Landroid/view/View;Z)V

    .line 88
    iput-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->currentRendering:Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    return-void
.end method


# virtual methods
.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    instance-of v0, p1, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    check-cast v0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;

    if-eqz v0, :cond_1

    .line 148
    iget-object v1, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    invoke-virtual {v0}, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->getViewStateCache()Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/workflow/ui/backstack/ViewStateCache;->restore(Lcom/squareup/workflow/ui/backstack/ViewStateCache;)V

    .line 149
    check-cast p1, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_1

    .line 151
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_1
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 142
    new-instance v0, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->viewStateCache:Lcom/squareup/workflow/ui/backstack/ViewStateCache;

    invoke-direct {v0, v1, v2}, Lcom/squareup/workflow/ui/backstack/ViewStateCache$SavedState;-><init>(Landroid/os/Parcelable;Lcom/squareup/workflow/ui/backstack/ViewStateCache;)V

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method protected performTransition(Landroid/view/View;Landroid/view/View;Z)V
    .locals 4

    const-string v0, "newView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_3

    .line 110
    sget v0, Lcom/squareup/workflow/ui/backstack/R$id;->back_stack_body:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 111
    sget v1, Lcom/squareup/workflow/ui/backstack/R$id;->back_stack_body:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    move-object p1, v0

    goto :goto_0

    :cond_0
    move-object v1, p2

    :goto_0
    const v0, 0x800005

    const v2, 0x800003

    if-nez p3, :cond_1

    .line 124
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p3, v0}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p3

    goto :goto_1

    :cond_1
    const/4 v3, 0x1

    if-ne p3, v3, :cond_2

    .line 125
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p3, v0}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p3

    :goto_1
    invoke-virtual {p3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Number;

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    .line 128
    new-instance v2, Landroidx/transition/TransitionSet;

    invoke-direct {v2}, Landroidx/transition/TransitionSet;-><init>()V

    .line 129
    new-instance v3, Landroidx/transition/Slide;

    invoke-direct {v3, v0}, Landroidx/transition/Slide;-><init>(I)V

    invoke-virtual {v3, p1}, Landroidx/transition/Slide;->addTarget(Landroid/view/View;)Landroidx/transition/Transition;

    move-result-object p1

    invoke-virtual {v2, p1}, Landroidx/transition/TransitionSet;->addTransition(Landroidx/transition/Transition;)Landroidx/transition/TransitionSet;

    move-result-object p1

    .line 130
    new-instance v0, Landroidx/transition/Slide;

    invoke-direct {v0, p3}, Landroidx/transition/Slide;-><init>(I)V

    invoke-virtual {v0, v1}, Landroidx/transition/Slide;->addTarget(Landroid/view/View;)Landroidx/transition/Transition;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroidx/transition/TransitionSet;->addTransition(Landroidx/transition/Transition;)Landroidx/transition/TransitionSet;

    move-result-object p1

    .line 131
    new-instance p3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    check-cast p3, Landroid/animation/TimeInterpolator;

    invoke-virtual {p1, p3}, Landroidx/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroidx/transition/TransitionSet;

    move-result-object p1

    const-string p3, "TransitionSet()\n        \u2026DecelerateInterpolator())"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    new-instance p3, Landroidx/transition/Scene;

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p3, v0, p2}, Landroidx/transition/Scene;-><init>(Landroid/view/ViewGroup;Landroid/view/View;)V

    check-cast p1, Landroidx/transition/Transition;

    invoke-static {p3, p1}, Landroidx/transition/TransitionManager;->go(Landroidx/transition/Scene;Landroidx/transition/Transition;)V

    return-void

    .line 125
    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 138
    :cond_3
    invoke-virtual {p0, p2}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->addView(Landroid/view/View;)V

    return-void
.end method
