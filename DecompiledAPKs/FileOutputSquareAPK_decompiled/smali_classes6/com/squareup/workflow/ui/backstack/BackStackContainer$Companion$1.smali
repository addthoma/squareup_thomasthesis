.class final Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;
.super Lkotlin/jvm/internal/Lambda;
.source "BackStackContainer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function4;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function4<",
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
        "*>;",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Landroid/content/Context;",
        "Landroid/view/ViewGroup;",
        "Lcom/squareup/workflow/ui/backstack/BackStackContainer;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackStackContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackStackContainer.kt\ncom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1\n*L\n1#1,167:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/ui/backstack/BackStackContainer;",
        "initialRendering",
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen;",
        "initialHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "context",
        "Landroid/content/Context;",
        "<anonymous parameter 3>",
        "Landroid/view/ViewGroup;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;

    invoke-direct {v0}, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;-><init>()V

    sput-object v0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;->INSTANCE:Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/squareup/workflow/ui/backstack/BackStackContainer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "*>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            ")",
            "Lcom/squareup/workflow/ui/backstack/BackStackContainer;"
        }
    .end annotation

    const-string p4, "initialRendering"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "initialHints"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "context"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    new-instance p4, Lcom/squareup/workflow/ui/backstack/BackStackContainer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p4

    move-object v1, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 160
    sget p3, Lcom/squareup/workflow/ui/backstack/R$id;->workflow_back_stack_container:I

    invoke-virtual {p4, p3}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->setId(I)V

    .line 161
    new-instance p3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {p3, v0, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p4, p3}, Lcom/squareup/workflow/ui/backstack/BackStackContainer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 162
    move-object p3, p4

    check-cast p3, Landroid/view/View;

    new-instance v0, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1$1$1;

    invoke-direct {v0, p4}, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1$1$1;-><init>(Lcom/squareup/workflow/ui/backstack/BackStackContainer;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p3, p1, p2, v0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->bindShowRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    return-object p4
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 154
    check-cast p1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    check-cast p2, Lcom/squareup/workflow/ui/ContainerHints;

    check-cast p3, Landroid/content/Context;

    check-cast p4, Landroid/view/ViewGroup;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/backstack/BackStackContainer$Companion$1;->invoke(Lcom/squareup/workflow/ui/backstack/BackStackScreen;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;)Lcom/squareup/workflow/ui/backstack/BackStackContainer;

    move-result-object p1

    return-object p1
.end method
