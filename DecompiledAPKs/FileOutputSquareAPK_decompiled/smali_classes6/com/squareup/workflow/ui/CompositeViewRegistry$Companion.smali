.class public final Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion;
.super Ljava/lang/Object;
.source "CompositeViewRegistry.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/CompositeViewRegistry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCompositeViewRegistry.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CompositeViewRegistry.kt\ncom/squareup/workflow/ui/CompositeViewRegistry$Companion\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,80:1\n10930#2:81\n10931#2:88\n1133#3,2:82\n1145#3,4:84\n*E\n*S KotlinDebug\n*F\n+ 1 CompositeViewRegistry.kt\ncom/squareup/workflow/ui/CompositeViewRegistry$Companion\n*L\n68#1:81\n68#1:88\n68#1,2:82\n68#1,4:84\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J-\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00050\u00042\u0012\u0010\u0006\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00050\u0007\"\u00020\u0005H\u0002\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion;",
        "",
        "()V",
        "mergeRegistries",
        "",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "registries",
        "",
        "([Lcom/squareup/workflow/ui/ViewRegistry;)Ljava/util/Map;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion;-><init>()V

    return-void
.end method

.method public static final varargs synthetic access$mergeRegistries(Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion;[Lcom/squareup/workflow/ui/ViewRegistry;)Ljava/util/Map;
    .locals 0

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion;->mergeRegistries([Lcom/squareup/workflow/ui/ViewRegistry;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method private final varargs mergeRegistries([Lcom/squareup/workflow/ui/ViewRegistry;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            ">;"
        }
    .end annotation

    .line 60
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 62
    new-instance v1, Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion$mergeRegistries$1;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion$mergeRegistries$1;-><init>(Ljava/util/Map;)V

    .line 81
    array-length v2, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, p1, v3

    .line 69
    instance-of v5, v4, Lcom/squareup/workflow/ui/CompositeViewRegistry;

    if-eqz v5, :cond_0

    .line 71
    check-cast v4, Lcom/squareup/workflow/ui/CompositeViewRegistry;

    invoke-static {v4}, Lcom/squareup/workflow/ui/CompositeViewRegistry;->access$getRegistriesByKey$p(Lcom/squareup/workflow/ui/CompositeViewRegistry;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion$mergeRegistries$1;->invoke(Ljava/util/Map;)V

    goto :goto_2

    .line 73
    :cond_0
    invoke-interface {v4}, Lcom/squareup/workflow/ui/ViewRegistry;->getKeys()Ljava/util/Set;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 82
    new-instance v6, Ljava/util/LinkedHashMap;

    const/16 v7, 0xa

    invoke-static {v5, v7}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-static {v7}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 84
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 85
    move-object v8, v6

    check-cast v8, Ljava/util/Map;

    .line 73
    invoke-interface {v8, v7, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 87
    :cond_1
    check-cast v6, Ljava/util/Map;

    .line 73
    invoke-virtual {v1, v6}, Lcom/squareup/workflow/ui/CompositeViewRegistry$Companion$mergeRegistries$1;->invoke(Ljava/util/Map;)V

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 76
    :cond_2
    invoke-static {v0}, Lkotlin/collections/MapsKt;->toMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
