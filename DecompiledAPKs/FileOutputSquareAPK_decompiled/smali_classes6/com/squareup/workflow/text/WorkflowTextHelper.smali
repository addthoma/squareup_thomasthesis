.class public final Lcom/squareup/workflow/text/WorkflowTextHelper;
.super Ljava/lang/Object;
.source "WorkflowTextHelper.kt"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;,
        Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowTextHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowTextHelper.kt\ncom/squareup/workflow/text/WorkflowTextHelper\n*L\n1#1,125:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u001b2\u00020\u0001:\u0002\u001b\u001cB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J*\u0010\u0012\u001a\u00020\n2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\u000cH\u0016J*\u0010\u0007\u001a\u00020\n2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0017\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u000cH\u0016J\u000e\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u001aR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0018\u00010\u0008X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/workflow/text/WorkflowTextHelper;",
        "Landroid/text/TextWatcher;",
        "textView",
        "Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;",
        "(Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;)V",
        "isUpdatingText",
        "",
        "onTextChanged",
        "Lkotlin/Function1;",
        "",
        "",
        "sequenceNum",
        "",
        "workflowToken",
        "",
        "afterTextChanged",
        "s",
        "Landroid/text/Editable;",
        "beforeTextChanged",
        "",
        "start",
        "count",
        "after",
        "before",
        "update",
        "rendering",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "Companion",
        "TextViewLike",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;


# instance fields
.field private isUpdatingText:Z

.field private onTextChanged:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private sequenceNum:I

.field private final textView:Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;

.field private workflowToken:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/text/WorkflowTextHelper;->Companion:Lcom/squareup/workflow/text/WorkflowTextHelper$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;)V
    .locals 1

    const-string v0, "textView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->textView:Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-boolean v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->isUpdatingText:Z

    if-nez v0, :cond_0

    .line 90
    iget v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->sequenceNum:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->sequenceNum:I

    .line 91
    iget-object v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->onTextChanged:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public final update(Lcom/squareup/workflow/text/WorkflowEditableText;)V
    .locals 5

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->workflowToken:Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getWorkflowToken$public_release()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getWorkflowToken$public_release()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->workflowToken:Ljava/lang/Object;

    .line 52
    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getSequenceNum()I

    move-result v0

    iput v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->sequenceNum:I

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getOnTextChanged()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->onTextChanged:Lkotlin/jvm/functions/Function1;

    .line 60
    iget v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->sequenceNum:I

    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getSequenceNum()I

    move-result v2

    const/4 v3, 0x0

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 65
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getSequenceNum()I

    move-result v2

    iget v4, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->sequenceNum:I

    if-gt v2, v4, :cond_2

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_4

    .line 71
    iget-object v2, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->textView:Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;

    invoke-interface {v2}, Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v1

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 81
    iput-boolean v1, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->isUpdatingText:Z

    .line 82
    iget-object v0, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->textView:Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;

    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;->setText(Ljava/lang/String;)V

    .line 83
    iput-boolean v3, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->isUpdatingText:Z

    :cond_3
    return-void

    .line 66
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected workflow\'s sequence number to be less than or equal to the view\'s: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "sequenceNum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    iget v1, p0, Lcom/squareup/workflow/text/WorkflowTextHelper;->sequenceNum:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", rendering.sequenceNum="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getSequenceNum()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
