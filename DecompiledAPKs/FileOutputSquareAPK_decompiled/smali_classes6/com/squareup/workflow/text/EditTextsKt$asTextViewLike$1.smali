.class public final Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;
.super Ljava/lang/Object;
.source "EditTexts.kt"

# interfaces
.implements Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/text/EditTextsKt;->asTextViewLike(Landroid/widget/TextView;)Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u0003H\u0016R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/workflow/text/EditTextsKt$asTextViewLike$1",
        "Lcom/squareup/workflow/text/WorkflowTextHelper$TextViewLike;",
        "text",
        "",
        "getText",
        "()Ljava/lang/String;",
        "textView",
        "Landroid/widget/TextView;",
        "setText",
        "",
        "newText",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_asTextViewLike:Landroid/widget/TextView;

.field private final textView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->$this_asTextViewLike:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iget-object p1, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->$this_asTextViewLike:Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->textView:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    .line 36
    iget-object p1, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->textView:Landroid/widget/TextView;

    const/4 v0, 0x0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->$this_asTextViewLike:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 42
    invoke-virtual {p0}, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->getText()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-interface {v0, v2, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_1

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/squareup/workflow/text/EditTextsKt$asTextViewLike$1;->textView:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method
