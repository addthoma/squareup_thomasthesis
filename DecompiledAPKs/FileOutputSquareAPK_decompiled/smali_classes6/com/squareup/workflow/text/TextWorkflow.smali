.class public final Lcom/squareup/workflow/text/TextWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "TextWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;,
        Lcom/squareup/workflow/text/TextWorkflow$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/text/TextWorkflow$State;",
        "Ljava/lang/String;",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u00c6\u0002\u0018\u00002\u001c\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u0012\u0013B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u00032\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00022\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016J.\u0010\n\u001a\u00020\u00042\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00022\u0006\u0010\u000b\u001a\u00020\u00032\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\u0003H\u0016J\u001c\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/workflow/text/TextWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/workflow/text/TextWorkflow$State;",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "()V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "textChangedAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "text",
        "State",
        "WorkflowToken",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/text/TextWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/workflow/text/TextWorkflow;

    invoke-direct {v0}, Lcom/squareup/workflow/text/TextWorkflow;-><init>()V

    sput-object v0, Lcom/squareup/workflow/text/TextWorkflow;->INSTANCE:Lcom/squareup/workflow/text/TextWorkflow;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    return-void
.end method

.method public static final synthetic access$textChangedAction(Lcom/squareup/workflow/text/TextWorkflow;Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/workflow/text/TextWorkflow;->textChangedAction(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final textChangedAction(Ljava/lang/String;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/workflow/text/TextWorkflow$State;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 63
    new-instance v0, Lcom/squareup/workflow/text/TextWorkflow$textChangedAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/text/TextWorkflow$textChangedAction$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Ljava/lang/String;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/text/TextWorkflow$State;
    .locals 2

    .line 46
    new-instance p1, Lcom/squareup/workflow/text/TextWorkflow$State;

    const/4 p2, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/text/TextWorkflow$State;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/text/TextWorkflow;->initialState(Ljava/lang/String;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/text/TextWorkflow$State;

    move-result-object p1

    return-object p1
.end method

.method public render(Ljava/lang/String;Lcom/squareup/workflow/text/TextWorkflow$State;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/text/TextWorkflow$State;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/workflow/text/TextWorkflow$State;",
            "-",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/workflow/text/WorkflowEditableText;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/workflow/text/WorkflowEditableText;

    .line 54
    new-instance v1, Lcom/squareup/workflow/text/TextWorkflow$render$1;

    invoke-direct {v1, p3}, Lcom/squareup/workflow/text/TextWorkflow$render$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 57
    invoke-virtual {p2}, Lcom/squareup/workflow/text/TextWorkflow$State;->getSequenceNum()I

    move-result p3

    .line 58
    invoke-virtual {p2}, Lcom/squareup/workflow/text/TextWorkflow$State;->getWorkflowToken()Lcom/squareup/workflow/text/TextWorkflow$WorkflowToken;

    move-result-object p2

    .line 52
    invoke-direct {v0, p1, v1, p3, p2}, Lcom/squareup/workflow/text/WorkflowEditableText;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Ljava/lang/String;

    check-cast p2, Lcom/squareup/workflow/text/TextWorkflow$State;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/text/TextWorkflow;->render(Ljava/lang/String;Lcom/squareup/workflow/text/TextWorkflow$State;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/workflow/text/TextWorkflow$State;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/workflow/text/TextWorkflow$State;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/text/TextWorkflow;->snapshotState(Lcom/squareup/workflow/text/TextWorkflow$State;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
