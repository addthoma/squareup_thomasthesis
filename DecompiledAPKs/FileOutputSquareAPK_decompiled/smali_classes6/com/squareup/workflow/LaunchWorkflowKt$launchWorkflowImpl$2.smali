.class final Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;
.super Lkotlin/jvm/internal/Lambda;
.source "LaunchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowImpl(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/internal/WorkflowLoop;Lcom/squareup/workflow/StatefulWorkflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Ljava/lang/Object;Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLaunchWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LaunchWorkflow.kt\ncom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2\n*L\n1#1,184:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0004\u0008\u0003\u0010\u0006\"\u0004\u0008\u0004\u0010\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "",
        "RenderingT",
        "RunnerT",
        "cause",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

.field final synthetic $renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

.field final synthetic $workflowScope:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;Lkotlinx/coroutines/channels/BroadcastChannel;Lkotlinx/coroutines/CoroutineScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->$renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    iput-object p2, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->$outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

    iput-object p3, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->$workflowScope:Lkotlinx/coroutines/CoroutineScope;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 166
    invoke-static {p1}, Lcom/squareup/workflow/internal/ThrowablesKt;->unwrapCancellationCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    .line 167
    :goto_0
    iget-object v1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->$renderingsAndSnapshots:Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;

    invoke-virtual {v1, p1}, Lkotlinx/coroutines/channels/ConflatedBroadcastChannel;->close(Ljava/lang/Throwable;)Z

    .line 168
    iget-object v1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->$outputs:Lkotlinx/coroutines/channels/BroadcastChannel;

    invoke-interface {v1, p1}, Lkotlinx/coroutines/channels/BroadcastChannel;->close(Ljava/lang/Throwable;)Z

    .line 172
    iget-object v1, p0, Lcom/squareup/workflow/LaunchWorkflowKt$launchWorkflowImpl$2;->$workflowScope:Lkotlinx/coroutines/CoroutineScope;

    invoke-interface {v1}, Lkotlinx/coroutines/CoroutineScope;->getCoroutineContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v1

    sget-object v2, Lkotlinx/coroutines/Job;->Key:Lkotlinx/coroutines/Job$Key;

    check-cast v2, Lkotlin/coroutines/CoroutineContext$Key;

    invoke-interface {v1, v2}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$Key;)Lkotlin/coroutines/CoroutineContext$Element;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v1, Lkotlinx/coroutines/Job;

    .line 173
    invoke-interface {v1}, Lkotlinx/coroutines/Job;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 175
    instance-of v2, p1, Ljava/util/concurrent/CancellationException;

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    move-object v0, p1

    :goto_1
    check-cast v0, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_3

    goto :goto_2

    :cond_3
    const-string v0, "Workflow cancelled"

    .line 176
    invoke-static {v0, p1}, Lkotlinx/coroutines/ExceptionsKt;->CancellationException(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/util/concurrent/CancellationException;

    move-result-object v0

    .line 174
    :goto_2
    invoke-interface {v1, v0}, Lkotlinx/coroutines/Job;->cancel(Ljava/util/concurrent/CancellationException;)V

    :cond_4
    return-void
.end method
