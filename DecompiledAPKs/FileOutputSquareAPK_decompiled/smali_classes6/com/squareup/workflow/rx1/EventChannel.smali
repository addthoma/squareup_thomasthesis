.class public interface abstract Lcom/squareup/workflow/rx1/EventChannel;
.super Ljava/lang/Object;
.source "EventChannel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002J?\u0010\u0003\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00050\u0004\"\u0008\u0008\u0001\u0010\u0005*\u00020\u00022#\u0010\u0006\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00050\u0008\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\nH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "E",
        "",
        "select",
        "Lrx/Single;",
        "R",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract select(Lkotlin/jvm/functions/Function1;)Lrx/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "TE;TR;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/Single<",
            "+TR;>;"
        }
    .end annotation
.end method
