.class public final Lcom/squareup/workflow/SinkKt;
.super Ljava/lang/Object;
.source "Sink.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u001a6\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00030\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "contraMap",
        "Lcom/squareup/workflow/Sink;",
        "T2",
        "T1",
        "transform",
        "Lkotlin/Function1;",
        "workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final contraMap(Lcom/squareup/workflow/Sink;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Sink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T1:",
            "Ljava/lang/Object;",
            "T2:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Sink<",
            "-TT1;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT2;+TT1;>;)",
            "Lcom/squareup/workflow/Sink<",
            "TT2;>;"
        }
    .end annotation

    const-string v0, "$this$contraMap"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/workflow/SinkKt$contraMap$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/SinkKt$contraMap$1;-><init>(Lcom/squareup/workflow/Sink;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/Sink;

    return-object v0
.end method
