.class public interface abstract Lcom/squareup/workflow/RenderContext;
.super Ljava/lang/Object;
.source "RenderContext.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/RenderContext$DefaultImpls;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\n\u0008\u0001\u0010\u0002 \u0000*\u00020\u00032\u00020\u0003J$\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0005\"\u0014\u0008\u0002\u0010\n*\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006H\u0017J>\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u00020\u000e0\u000c\"\u0008\u0008\u0002\u0010\r*\u00020\u00032\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u0002H\r\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u000cH\'Jo\u0010\u0010\u001a\u0002H\u0011\"\u0004\u0008\u0002\u0010\u0012\"\u0008\u0008\u0003\u0010\u0013*\u00020\u0003\"\u0004\u0008\u0004\u0010\u00112\u0018\u0010\u0014\u001a\u0014\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0013\u0012\u0004\u0012\u0002H\u00110\u00152\u0006\u0010\u0016\u001a\u0002H\u00122\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00182\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u0002H\u0013\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u000cH&\u00a2\u0006\u0002\u0010\u0019JF\u0010\u001a\u001a\u00020\u000e\"\u0004\u0008\u0002\u0010\u001b2\u000c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u001d2\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00182\u001e\u0010\u000f\u001a\u001a\u0012\u0004\u0012\u0002H\u001b\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u000cH&R$\u0010\u0004\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00060\u0005X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/workflow/RenderContext;",
        "StateT",
        "OutputT",
        "",
        "actionSink",
        "Lcom/squareup/workflow/Sink;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "getActionSink",
        "()Lcom/squareup/workflow/Sink;",
        "makeActionSink",
        "A",
        "onEvent",
        "Lkotlin/Function1;",
        "EventT",
        "",
        "handler",
        "renderChild",
        "ChildRenderingT",
        "ChildPropsT",
        "ChildOutputT",
        "child",
        "Lcom/squareup/workflow/Workflow;",
        "props",
        "key",
        "",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;",
        "runningWorker",
        "T",
        "worker",
        "Lcom/squareup/workflow/Worker;",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getActionSink()Lcom/squareup/workflow/Sink;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Sink<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;"
        }
    .end annotation
.end method

.method public abstract makeActionSink()Lcom/squareup/workflow/Sink;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>()",
            "Lcom/squareup/workflow/Sink<",
            "TA;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use RenderContext.actionSink."
    .end annotation
.end method

.method public abstract onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<EventT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TEventT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)",
            "Lkotlin/jvm/functions/Function1<",
            "TEventT;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use RenderContext.actionSink."
    .end annotation
.end method

.method public abstract renderChild(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ChildPropsT:",
            "Ljava/lang/Object;",
            "ChildOutputT:",
            "Ljava/lang/Object;",
            "ChildRenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TChildPropsT;+TChildOutputT;+TChildRenderingT;>;TChildPropsT;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TChildOutputT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)TChildRenderingT;"
        }
    .end annotation
.end method

.method public abstract runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Worker<",
            "+TT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)V"
        }
    .end annotation
.end method
